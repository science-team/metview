/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"

request* read_request(mstream* s) {
    request* r = empty_request(upcase(stream_read_string(s)));
    int n      = stream_read_int(s);
    int i;

    for (i = 0; i < n; i++) {
        const char* p = upcase(stream_read_string(s));
        int m         = stream_read_int(s);
        int j;
        char* q = strcache(p);

        for (j = 0; j < m; j++)
            add_value(r, q, "%s", upcase(stream_read_string(s)));

        strfree(q);
    }

    return r;
}

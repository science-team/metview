/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#ifndef ODB_H
#define ODB_H

#ifdef __cplusplus
extern "C" {
#endif

#include "mars.h"

///
/// \file odb.h
///
/// @author Piotr Kuchta, ECMWF May 2009
///

err odb_to_request_from_file(request* r, const char* fileName);
err odb_compare_attributes_of_first_request(request*, request*);
long long odb_filter(const char*, FILE* fin, FILE* fout, long long total_to_read);

#endif

#ifdef __cplusplus
}  // extern "C"
#endif

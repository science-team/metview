/*
 * © Copyright 1996-2017 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include <grib_api.h>

/* This should be c++ ... */

typedef enum field_state
{
    unknown,
    packed_mem,
    packed_file,
    expand_mem
} field_state;

typedef struct gribfile {
    struct gribfile* next;
    char* fname;
    int refcnt;
    boolean temp;
    FILE* file;
} gribfile;

typedef struct {
    int refcnt;
    request* r;
} field_request;

/* One field .. */

typedef struct field {

    int refcnt;

    field_state shape;

    grib_handle* handle;
    double* values;
    size_t value_count;


    /* if on file */

    off_t offset;
    size_t length;
    gribfile* file;

    /* missing fields/values; usually a field has a bitmap to represent
       missing values, but if the packing type is grid_complex_spatial_differencing
       then the missing values can be encoded directly in the array of values;
       the flag 'bitmap' specifically tells us if the field has a bitmap, and the
       flag 'missing_values' tells us if the field contains missing values,
       regardles of which encoding method was used. */

    boolean missing;      /* field is missing */
    boolean bitmap;       /* field has bitmap */
    boolean missing_vals; /* field has missing values */

    field_request* r;

} field;

typedef struct fieldset {

    int refcnt;

    /* if fields */

    int max;
    int count;

    field** fields;

} fieldset;


#define MISSING_VALUE(n) ((n) == mars.grib_missing_value)
#define MISSING_FIELD(f) ((f)->missing)
#define FIELD_HAS_MISSING_VALS(f) ((f)->missing_vals)

/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

/* common control code for all databases */

#define CNTL_REWIND 1
#define CNTL_HASDATA 2 /* arg is boolean true is data ready */

#define CNTL_FLUSH 3
#define CNTL_LIST 4
#define CNTL_REMOVE 5
#define CNTL_ATTACH 6
#define CNTL_STAGE 7
#define CNTL_ERROR 8

#define CNTL_REGISTER 9
#define CNTL_STORE 10
#define CNTL_FETCH 11

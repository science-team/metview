/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include <errno.h>
#include "mars.h"


static err setargs(FILE* f, int n) {
    if (n == 0)
        return 0;
    if (!(pop()))
        return -1;
    if (setargs(f, n - 1))
        return -1;
    abort();

    return 0;
}


err extern_func(math* p) {
    marslog(LOG_EROR, "Undefined function '%s' with %d arguments", p->name, p->arity);
    return -1;

#if 0
	char buf[1024];
	request *r;
	int saveerr = errno;
	char *tmp = marstmp();
	FILE *f = fopen(tmp,"w");
	err e;

	if(!f)
	{
		marslog(LOG_EROR|LOG_PERR,"%s",tmp);
		return -2;
	}
	e = setargs(f,p->arity);
	fclose(f);

	if(e == NOERR)
	{
		sprintf(buf,"env MNAME=%s MREQUEST=%s %s",p->name,tmp,p->name);
		if(system(buf) != 0) e = -2;
		else
		{
			r = read_request_file(tmp);
			abort();
			/*
			if(r && (v = request_to_fieldset(NULL,r)))
				push_fieldset(v,p);
			else e = -2;
			*/
			free_all_requests(r);
		}
	}



	errno = saveerr;
	return e;
#endif
}

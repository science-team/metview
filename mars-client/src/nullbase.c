/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"

static void null_init(void);

static err null_open(void* data, request*, request*, int);
static err null_close(void* data);
static err null_read(void* data, request* r, void* buffer, long* length);
static err null_write(void* data, request* r, void* buffer, long* length);

#ifdef VMS
/* externaldef(base_class)  */
#endif

base_class _nullbase = {

    NULL,       /* parent class */
    "nullbase", /* name         */

    false, /* inited       */

    0,    /* private size */
    0,    /* option count */
    NULL, /* options      */

    null_init, /* init         */

    null_open,  /* open         */
    null_close, /* close        */

    null_read,  /* read         */
    null_write, /* write        */

};


#ifdef VMS
/* externaldef(base_class)  */
#endif

base_class* nullbase = &_nullbase;


static void null_init(void) {
}

static err null_open(void* data, request* r, request* e, int mode) {
    return 0;
}

static err null_close(void* data) {
    return 0;
}

static err null_read(void* data, request* r, void* buffer, long* length) {
    return EOF;
}

static err null_write(void* data, request* r, void* buffer, long* length) {
    return 0;
}

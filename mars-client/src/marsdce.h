/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

/* Generated by IDL compiler version OSF DCE T1.1.0-03 */
#ifndef mars_v1_0_included
#define mars_v1_0_included
#ifndef IDLBASE_H
#include <dce/idlbase.h>
#endif
#include <dce/rpc.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef nbase_v0_0_included
#include <dce/nbase.h>
#endif
typedef struct pipe_t {
    void (*pull)(
#ifdef IDL_PROTOTYPES
        rpc_ss_pipe_state_t state,
        idl_byte* buf,
        idl_ulong_int esize,
        idl_ulong_int* ecount
#endif
    );
    void (*push)(
#ifdef IDL_PROTOTYPES
        rpc_ss_pipe_state_t state,
        idl_byte* buf,
        idl_ulong_int ecount
#endif
    );
    void (*alloc)(
#ifdef IDL_PROTOTYPES
        rpc_ss_pipe_state_t state,
        idl_ulong_int bsize,
        idl_byte** buf,
        idl_ulong_int* bcount
#endif
    );
    rpc_ss_pipe_state_t state;
} pipe_t;
typedef idl_void_p_t context_t;
typedef idl_short_int err_t;
typedef struct _param param_t;
typedef struct _value {
    struct _value* next;
    idl_char* name;
    struct _value* other_names;
    struct _value* ctx;
} value_t;
typedef struct _request {
    struct _request* next;
    struct _param* params;
    idl_char* name;
    idl_char* info;
    idl_char* kind;
    idl_long_int data;
} request_t;
typedef struct _param {
    struct _param* next;
    value_t* values;
    idl_char* name;
    idl_short_int count;
    request_t* subrequest;
    value_t* default_values;
    value_t* current_values;
    request_t* intf;
} _p;
extern err_t marsdce_open(
#ifdef IDL_PROTOTYPES
    /* [in] */ handle_t h,
    /* [in] */ request_t* r,
    /* [in] */ request_t* e,
    /* [in] */ idl_short_int mode,
    /* [out] */ context_t* ctx
#endif
);
extern err_t marsdce_read(
#ifdef IDL_PROTOTYPES
    /* [in] */ handle_t h,
    /* [in] */ context_t ctx,
    /* [in, out] */ request_t* r,
    /* [in, out] */ idl_long_int* len,
    /* [out] */ pipe_t* p
#endif
);
extern err_t marsdce_write(
#ifdef IDL_PROTOTYPES
    /* [in] */ handle_t h,
    /* [in] */ context_t ctx,
    /* [in] */ request_t* r,
    /* [in, out] */ idl_long_int* len,
    /* [in] */ pipe_t* p
#endif
);
extern err_t marsdce_close(
#ifdef IDL_PROTOTYPES
    /* [in] */ handle_t h,
    /* [in, out] */ context_t* ctx
#endif
);
extern idl_boolean marsdce_check(
#ifdef IDL_PROTOTYPES
    /* [in] */ handle_t h,
    /* [in] */ request_t* r,
    /* [in] */ context_t ctx
#endif
);
extern err_t marsdce_cntl(
#ifdef IDL_PROTOTYPES
    /* [in] */ handle_t h,
    /* [in] */ idl_short_int c
#endif
);
void context_t_rundown(
#ifdef IDL_PROTOTYPES
    rpc_ss_context_t context_handle
#endif
);
typedef struct mars_v1_0_epv_t {
    err_t (*marsdce_open)(
#ifdef IDL_PROTOTYPES
        /* [in] */ handle_t h,
        /* [in] */ request_t* r,
        /* [in] */ request_t* e,
        /* [in] */ idl_short_int mode,
        /* [out] */ context_t* ctx
#endif
    );
    err_t (*marsdce_read)(
#ifdef IDL_PROTOTYPES
        /* [in] */ handle_t h,
        /* [in] */ context_t ctx,
        /* [in, out] */ request_t* r,
        /* [in, out] */ idl_long_int* len,
        /* [out] */ pipe_t* p
#endif
    );
    err_t (*marsdce_write)(
#ifdef IDL_PROTOTYPES
        /* [in] */ handle_t h,
        /* [in] */ context_t ctx,
        /* [in] */ request_t* r,
        /* [in, out] */ idl_long_int* len,
        /* [in] */ pipe_t* p
#endif
    );
    err_t (*marsdce_close)(
#ifdef IDL_PROTOTYPES
        /* [in] */ handle_t h,
        /* [in, out] */ context_t* ctx
#endif
    );
    idl_boolean (*marsdce_check)(
#ifdef IDL_PROTOTYPES
        /* [in] */ handle_t h,
        /* [in] */ request_t* r,
        /* [in] */ context_t ctx
#endif
    );
    err_t (*marsdce_cntl)(
#ifdef IDL_PROTOTYPES
        /* [in] */ handle_t h,
        /* [in] */ idl_short_int c
#endif
    );
} mars_v1_0_epv_t;
extern rpc_if_handle_t mars_v1_0_c_ifspec;
extern rpc_if_handle_t mars_v1_0_s_ifspec;

#ifdef __cplusplus
}
#endif

#endif

/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"
#include "mars_client_version.h"

const char* pproc_name();

long marsversion() {
    static long version = -1;

    if (version == -1) {
        version = (long)(atol(mars_client_buildstamp()) / 1000000);
    }
    return version;
}

const char* marssourcebranch() {
    static char s[1024];
    snprintf(s, sizeof(s), "pproc:%s", pproc_name());
    return s;
}

long marsgribapi() {
    static long version = 20150320;
    return version;
}

long marsodbapi() {
    static long version = 20140527;
    return version;
}

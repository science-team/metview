/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

/*
** File: eccmd-request.h
**
** Purpose: Shared definitions for
**          member state MARS client and ECMWF MARS server; and
**          eccmd server and client.
**
** $Date: 1996/03/26 17:06:05 $ $Revision: 1.7 $
*/

#include <sys/types.h>
#include "ecopt.h"

#ifndef USUALS /* Assures no redefinitions of usual types...*/
#define USUALS
typedef unsigned short word16; /* values are 0-65535 */
#ifdef __alpha
typedef unsigned int word32; /* values are 0-4294967295 */
#else
typedef unsigned long word32; /* values are 0-4294967295 */
#endif
#endif /* if USUALS not already defined */

#if ECQ_MAXUSERIDLEN != 15
#error ECQ_ ## MAXUSERIDLEN must not be different from 15
#endif

typedef u_char digestType[16];
typedef char ECuser[ECQ_MAXUSERIDLEN + 1];

#define ECMARSCMDNAME "ecmars"

/* The ECMarsCertReply PROTOCOLID should never change.  Any change would
** indicate a change in the ECMarsCert Reply protocol.
**/
#if !defined(HIGHFIRST) && !defined(__alpha)
#define HIGHFIRST
#endif /* HIGHFIRST */
#ifndef HIGHFIRST
#define ECMARSCERTPROTOCOLID (ECQ_MARSPROT_MAJOR | ECQ_MARSPROT_MINOR << 8)
#else /* HIGHFIRST */
#define ECMARSCERTPROTOCOLID (ECQ_MARSPROT_MAJOR << 8 | ECQ_MARSPROT_MINOR)
#endif /* HIGHFIRST */

struct ECMarsCertReply {
    word16 protocolID; /*  must match ECMARSCERTPROTOCOLID         */
    char reserved;
    u_char returnCode;    /*  one of the return codes below           */
    ECuser msuser;        /*  userID at Member State                  */
    ECuser ecuser;        /*  userID at ECMWF                         */
    word32 certIPaddress; /*  where certificate has been made         */
    time_t certExpiry;    /*  time of expiry (= -1 infinite expiry)   */
    time_t passcodeTime;  /*  time of last valid passcode             */
};
typedef struct ECMarsCertReply eCMarsCertReply;

/* The following return codes are either
** - eccmd client exit codes, or
** - eccmd server return codes within the ECMarsCertReply structure.
**/
enum ReturnCodes
{
    RCCOK     = 0,
    RCCSYNTAX = 0x80,
    RCCFAIL,
    RCCCERTUNVERIFIED,

    RCNFAIL = 0x90,

    RCSINTERNAL = 0xa0,
    RCSFAIL,
    RCSPASSCODERETRY,
    RCSPASSCODEREJECT,

    RCSLIMREJECT = 0xb0,
    RCSLIMVERIFIED,
    RCSREQUESTVERIFIED,
    RCSCMDCALLED,

    RCSMARSOK = 0
};

#ifdef RPC_HDR
%#include "cvers.h"
#endif

#ifdef RPC_SVC
%#include "cpli.h"
%#define main()  mrpcsvc(mrs_rpc)
#endif

struct  acc {
	struct acc *next;
	char          name[8];
	long			count;
	long			maxfile;
	long			time;
	long			flags;
};


program MSPROG {
	version MSVERS {
		
		acc      MGACL(void)    = 1; /* get account list */
		int      MUACL(acc)     = 2; /* update account   */
		string   MGQUE(void)    = 3; /* update account   */
		int      MUQUE(string)  = 4; /* update account   */
		string   MGASD(void)    = 5; /* update account   */
		string   MGBLK(long)     = 6; /* update account   */

	} = 1;
} = mrs_rpc;

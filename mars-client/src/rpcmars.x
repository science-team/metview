%#include <stdio.h>
%#include <stdlib.h>
%#include <rpc/xdr.h>
#ifdef RPC_HDR
%typedef char *cache_t;
%typedef void *voidp_t;
#endif

%#include "hash.h"

#ifdef RPC_XDR
%bool_t xdr_cache_t(XDR *xdrs,char **name)
%{
%   extern char* strcache();
%	char *sp = *name;
%
%	switch (xdrs->x_op)
%	{
%
%	case XDR_FREE:
%		if(sp != NULL) strfree(sp);
%		*name = NULL;
%		break;
%
%	case XDR_ENCODE:
%		if(sp == NULL) sp = "";
%		return xdr_string(xdrs,&sp, ~0);
%		break;
%
%	case XDR_DECODE:
%
%		if(!xdr_string(xdrs,&sp, ~0))
%			return FALSE;
%
%		*name = strcache(sp);
%		free(sp);
%
%		break;
%	}
%	return TRUE;
%}

#endif

struct value {

%	/*  request part */

	struct value *next;
	cache_t         name;

%	/* language part */

	struct value     *other_names;
	struct value     *ref;

#ifdef RPC_HDR
	struct value     *expand;
#endif

};

struct request {
	struct request   *next;
	struct parameter *params;
	cache_t         name;
	cache_t         info;
	cache_t         kind;

#ifdef RPC_HDR
	voidp_t          data; /* user data */
	long             order;
#endif

};

struct parameter {

%	/*  request part */

	struct parameter *next;
	struct value     *values;
	cache_t         name;
	int              count;
	struct request   *subrequest;

%	/* language part */

	struct value     *default_values;
	struct value     *current_values;
#ifdef RPC_HDR
	struct value     *ibm_values;
	struct request   *interface;
#endif


};

struct netblk {
	int            vers;
	int            code;
	int            error;

    struct request *req;
    struct request *env;

	int            mode;
	int            check;

	long           bufsize;
	opaque         data<>;
};

struct netlock {
	string			resource<>;
	int				mode;
};

struct cache_index {
	struct request *index;
};

#ifdef RPC_XDR

%/* on hp, xdr don't use malloc.... , so let's
%override the calls */

#ifdef hpux
%char *_rpc_malloc(int size)
%{
%  return (char*)malloc(size);
%}
%
%void _rpc_free(char *p)
%{
%  free(p);
%}
#endif

#endif


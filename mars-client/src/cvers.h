/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

/* #define RPCNUM (20000) */
/* #define RPCNUM 21002 */
extern int mars_rpc_number;
#define RPCNUM mars_rpc_number
#define MARS 21001
#define MARZ 21101
#define MATS 21002

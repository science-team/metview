/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

/* Title:	MARS CACHING SYSTEM DATABASE */
/* Part:	Include file */
/* Author:	a.hofstadler */
/* Version:	2.0	93-MAY-21 */
/* Changes:	 */


#include <mscc.h>

#define MCS_DBASE "/mcs/mcs"
#define MCS_SQL "/usr/local/lib/metaps/sql"
#define MCS_CHK "/usr/local/lib/metaps/chk/mcs.chk"
#define MCS_DEFAULT_MSPATH "/usr/local/apps/empress/v6.2"

/* pattern searched in sql-script for: if there store data in compressed form */
#define MCS_COMPRESSED_PATTERN "(in compressed form)"

#define MCS_TAB_CHAR 15 /* max. # of characters of tablename */
#define MCS_MAX_LINE_CHAR 100

#define MCS_USE_TRANSACTION_AT_TABLECREATION false

typedef struct attribute {

    struct attribute* next;

    char* name;        /* mars and empress parameter name */
    addr emp_desc;     /* attribute descriptor */
    boolean qual_flag; /* flag if used in qualification or not */

} attribute;


typedef struct emp_bulk {
    long len;
    long buf;
} emp_bulk;


typedef struct mcs_private {

    /* filled outside */

    char* dbname;
    char* sql;
    char* chk;
    int maxtry;
    int sleeptime;
    boolean compress;

    /* filled inside */

    rule* _rules; /* checking rules from check file */

    char* _tab_name; /* first characters of tablename: eg.: grb_ */

    const char* _date;
    int _date_cnt;

    const char* _type;

    char _sql[MCS_MAX_LINE_CHAR];

    addr _tab; /* table descriptor */

    addr _rec;     /* record descriptor */
    addr _new_rec; /* second record descriptor for updates */

    addr _qual; /* qualification descriptor */
    boolean _qual_is_null;

    addr _ret; /* retrieval descriptor */

    attribute* _first; /* begin of linked list of attributes */

    int _dim_priv_attr; /* dimension of private attribute array */

    boolean _compress; /* flag determined from sql-script */

    request* _r; /* THE request */

} mcs_private;

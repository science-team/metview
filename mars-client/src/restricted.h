/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#define TYPE_1 (H(1) | H(2) | H(4) | H(5) | H(7) | H(8) | H(10) | H(11) | H(13) | H(14) | H(16) | H(17) | H(19) | H(20) | H(22) | H(23))
#define TYPE_2 (H(0) | H(1) | H(2) | H(3) | H(4) | H(5) | H(6) | H(7) | H(8) | H(9) | H(10) | H(11) | H(12) | H(13) | H(14) | H(15) | H(16) | H(17) | H(18) | H(19) | H(20) | H(21) | H(22) | H(23))
{
    10004,
    TYPE_1,
}, /* SNDL41 */
    {
        10005,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10007,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10015,
        TYPE_1,
    }, /* SNDL41 */
    {
        10020,
        TYPE_1,
    }, /* SNDL41 */
    {
        10022,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10026,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10035,
        TYPE_1,
    }, /* SNDL41 */
    {
        10042,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10044,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10046,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10055,
        TYPE_1,
    }, /* SNDL41 */
    {
        10091,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10093,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10113,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10124,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10129,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10130,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10131,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10139,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10147,
        TYPE_1,
    }, /* SNDL41 */
    {
        10152,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10156,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10161,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10162,
        TYPE_1,
    }, /* SNDL41 */
    {
        10168,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10170,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10177,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10180,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10184,
        TYPE_1,
    }, /* SNDL41 */
    {
        10193,
        TYPE_2,
    }, /* SIDL44  SMDL44  SNDL44 */
    {
        10200,
        TYPE_1,
    }, /* SNDL41 */
    {
        10215,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10224,
        TYPE_1,
    }, /* SNDL41 */
    {
        10235,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10249,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10253,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10261,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10264,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10267,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10268,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10270,
        TYPE_1,
    }, /* SNDL41 */
    {
        10277,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10280,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10289,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10291,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10305,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10315,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10317,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10321,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10325,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10338,
        TYPE_1,
    }, /* SNDL41 */
    {
        10348,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10356,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10359,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10361,
        TYPE_1,
    }, /* SNDL41 */
    {
        10365,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10368,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10376,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10379,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10381,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10382,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10384,
        TYPE_1,
    }, /* SNDL41 */
    {
        10385,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10389,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10393,
        TYPE_1,
    }, /* SNDL41 */
    {
        10396,
        TYPE_2,
    }, /* SIDL45  SMDL45  SNDL45 */
    {
        10400,
        TYPE_1,
    }, /* SNDL41 */
    {
        10406,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10410,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10418,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10427,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10430,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10432,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10435,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10438,
        TYPE_1,
    }, /* SNDL41 */
    {
        10444,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10449,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10452,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10453,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10454,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10458,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10460,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10466,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10469,
        TYPE_1,
    }, /* SNDL41 */
    {
        10471,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10474,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10480,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10488,
        TYPE_1,
    }, /* SNDL41 */
    {
        10490,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10496,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10499,
        TYPE_2,
    }, /* SIDL42  SMDL42  SNDL42 */
    {
        10501,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10506,
        TYPE_1,
    }, /* SNDL41 */
    {
        10513,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10515,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10517,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10526,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10532,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10535,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10542,
        TYPE_2,
    }, /* SIDL46  SMDL46  SNDL46 */
    {
        10544,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10546,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10548,
        TYPE_1,
    }, /* SNDL41 */
    {
        10552,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10554,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10555,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10557,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10558,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10564,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10565,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10567,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10569,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10574,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10575,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10577,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10578,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10579,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10582,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10591,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10609,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10615,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10616,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10635,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10637,
        TYPE_1,
    }, /* SNDL41 */
    {
        10645,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10648,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10655,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10658,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10671,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10675,
        TYPE_2,
    }, /* SIDL47  SMDL47  SNDL47 */
    {
        10685,
        TYPE_1,
    }, /* SNDL41 */
    {
        10688,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10704,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10706,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10708,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10724,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10727,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10729,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10735,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10736,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10738,
        TYPE_1,
    }, /* SNDL41 */
    {
        10739,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10742,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10761,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10763,
        TYPE_1,
    }, /* SNDL41 */
    {
        10776,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10777,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10788,
        TYPE_1,
    }, /* SNDL41 */
    {
        10791,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10796,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10803,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10805,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10815,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10818,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10836,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10838,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10850,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10852,
        TYPE_1,
    }, /* SNDL41 */
    {
        10865,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10870,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10875,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10895,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10908,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10929,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10946,
        TYPE_1,
    }, /* SNDL41 */
    {
        10948,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10961,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10962,
        TYPE_2,
    }, /* SIDL43  SMDL43  SNDL43 */
    {
        10963,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10980,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */
    {
        10982,
        TYPE_2,
    }, /* SIDL48  SMDL48  SNDL48 */

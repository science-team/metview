/*
 * © Copyright 1996-2014 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

typedef struct netcdf_field netcdf_field;

typedef struct netcdf_field_list {
    netcdf_field* first;
    netcdf_field* last;
    int cnt;
} netcdf_field_list;

typedef struct netcdf_target {
    char* path;
    int nc;
    netcdf_field_list fields;
} netcdf_target;

typedef struct netcdf_schema netcdf_schema;

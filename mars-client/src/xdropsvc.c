/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#ifdef METVIEW
#include <X11/Intrinsic.h>
#include <Xm/Xm.h>
#include "../Util/include/Drag.h"
#include "mars.h"

typedef struct drop {
    svc* s;
    svcproc proc;
    void* data;
} drop;


static void destroy_callback(Widget w, XtPointer cd, XtPointer cb) {
    XtFree(cd);
}

static void drop_callback(Widget w, XtPointer cd, DragCallbackStruct* cb) {
    drop* info = (drop*)cd;
}

void register_drop_target(svc* s, Widget w, char* name, svcproc proc, void* data) {
    drop* info = NEW_CLEAR(drop);

    info->s    = s;
    info->proc = proc;
    info->data = data;

    if (XtIsDrag(w))
        XtAddCallback(w, XmNdropCallback,
                      (XtCallbackProc)drop_callback, (XtPointer)info);
    else
        DragAcceptDropCallback(w, (XtCallbackProc)drop_callback, (XtPointer)info);

    XtAddCallback(w, XmNdestroyCallback, (XtCallbackProc)destroy_callback,
                  (XtPointer)info);

    DragSetInfo(w, s->name);
}

void get_drop_info(svc* s, DragCallbackStruct* cb, void* data) {
}

#endif
#endif

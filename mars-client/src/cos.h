/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */


/*

0123456789012345678901234567890123456789012345678901234567890123
0         1         2         3         4         5         6

Block control word (BCW)

XXXXX...........................................................  -> M
...........X....................................................  -> BDF
...............................XXXXXXXXXXXXXXXXXXXXXXXX.........  -> BDN
.......................................................XXXXXXXXX  -> FWI

Record control word (RCW)

XXXXX...........................................................  -> M
.....XXXXXX.....................................................  -> UBC
....................XXXXXXXXXXXXXXXXXXXXX.......................  -> PFI
.........................................XXXXXXXXXXXXXX.........  -> PRI
.......................................................XXXXXXXXX  -> FWI

*/

#include <stdio.h>

#define FWI(x) (((x.loword) & 0x000001FF) << 3)  /* forward index         */
#define PRI(x) (((x.loword) & 0x00FFFE00) >> 6)  /* previous record index */
#define PFI(x) (((x.loword) & 0x00FFFE00) >> 6)  /* previous record index */
#define UBC(x) (((x.loword) & 0x0FC00000) >> 20) /* unused bit count      */
#define M(x) (((x.hiword) & 0xF0000000) >> 28)   /* type of block         */
#define BDF(x) (((x.hiword) & 0x00100000) >> 28) /* bad data flag         */
#define BN(x) (((x.loword) & 0xFFFFFE00) >> 9)   /* block number          */

#define COSEOF 0xE /* COS end of file */

#define BCW 1 /* block control word  */
#define RCW 2 /* record control word */

#define COSFILE cosfile
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

/* a cray word is 64 bits = 2x32 bits = 2x1 sun word */

typedef struct {
    unsigned int hiword : 32;
    unsigned int loword : 32;
} crayword;

/* definition of a cosfile */

typedef struct {
    FILE* f;            /* current file              */
    crayword cw;        /* current control word      */
    unsigned int block; /* current BCW number        */
    int err;            /* last error                */
    char* buffer;       /* used for cos_write        */
    long cnt;           /* number of bytes in buffer */
    char* fname;        /* file name                 */
} cosfile;

/* prototypes */
/* C interface */

COSFILE* cos_open(const char* fname);
int cos_read(COSFILE* cf, char* buf, long* len);
int cos_close(COSFILE* cf);


#undef _Z

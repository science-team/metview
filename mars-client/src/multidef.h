/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#ifdef FORTRAN_UPPERCASE
#define d_def_1_ D_DEF_1
#define d_def_2_ D_DEF_2
#define d_def_3_ D_DEF_3
#define d_def_4_ D_DEF_4
#define d_def_5_ D_DEF_5
#define d_def_6_ D_DEF_6
#define d_def_7_ D_DEF_7
#define d_def_8_ D_DEF_8
#define d_def_9_ D_DEF_9
#define d_def_10_ D_DEF_10
#define d_def_11_ D_DEF_11
#define d_def_13_ D_DEF_13
#define d_def_14_ D_DEF_14
#define d_def_15_ D_DEF_15
#define d_def_16_ D_DEF_16
#define d_def_17_ D_DEF_17
#define d_def_18_ D_DEF_18
#define d_def_19_ D_DEF_19
#define d_def_20_ D_DEF_20
#define d_def_21_ D_DEF_21
#define d_def_22_ D_DEF_22
#define d_def_23_ D_DEF_23
#define d_def_24_ D_DEF_24
#define d_def_25_ D_DEF_25
#if 0 /* Move them up when they exist */
#define d_def_26_ D_DEF_26
#define d_def_27_ D_DEF_27
#define d_def_28_ D_DEF_28
#define d_def_29_ D_DEF_29
#endif
#endif

#ifdef FORTRAN_NO_UNDERSCORE
#define d_def_1_ d_def_1
#define d_def_2_ d_def_2
#define d_def_3_ d_def_3
#define d_def_4_ d_def_4
#define d_def_5_ d_def_5
#define d_def_6_ d_def_6
#define d_def_7_ d_def_7
#define d_def_8_ d_def_8
#define d_def_9_ d_def_9
#define d_def_10_ d_def_10
#define d_def_11_ d_def_11
#define d_def_13_ d_def_13
#define d_def_14_ d_def_14
#define d_def_15_ d_def_15
#define d_def_16_ d_def_16
#define d_def_17_ d_def_17
#define d_def_18_ d_def_18
#define d_def_19_ d_def_19
#define d_def_20_ d_def_20
#define d_def_21_ d_def_21
#define d_def_22_ d_def_22
#define d_def_23_ d_def_23
#define d_def_24_ d_def_24
#define d_def_25_ d_def_25
#if 0 /* Move them up when they exist */
#define d_def_26_ d_def_26
#define d_def_27_ d_def_27
#define d_def_28_ d_def_28
#define d_def_29_ d_def_29
#endif
#endif

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

extern fortint d_def_1_(fortint*, unsigned char*);
extern fortint d_def_2_(fortint*, unsigned char*);
extern fortint d_def_3_(fortint*, unsigned char*);
extern fortint d_def_4_(fortint*, unsigned char*);
extern fortint d_def_5_(fortint*, unsigned char*);
extern fortint d_def_6_(fortint*, unsigned char*);
extern fortint d_def_7_(fortint*, unsigned char*);
extern fortint d_def_8_(fortint*, unsigned char*);
extern fortint d_def_9_(fortint*, unsigned char*);
extern fortint d_def_10_(fortint*, unsigned char*);
extern fortint d_def_11_(fortint*, unsigned char*);
extern fortint d_def_13_(fortint*, unsigned char*);
extern fortint d_def_14_(fortint*, unsigned char*);
extern fortint d_def_15_(fortint*, unsigned char*);
extern fortint d_def_16_(fortint*, unsigned char*);
extern fortint d_def_17_(fortint*, unsigned char*);
extern fortint d_def_18_(fortint*, unsigned char*);
extern fortint d_def_19_(fortint*, unsigned char*);
extern fortint d_def_20_(fortint*, unsigned char*);
extern fortint d_def_21_(fortint*, unsigned char*);
extern fortint d_def_22_(fortint*, unsigned char*);
extern fortint d_def_23_(fortint*, unsigned char*);
extern fortint d_def_24_(fortint*, unsigned char*);
extern fortint d_def_25_(fortint*, unsigned char*);
#if 0 /* Move them up when they exist */
extern fortint d_def_26_(fortint *, unsigned char *);
extern fortint d_def_27_(fortint *, unsigned char *);
extern fortint d_def_28_(fortint *, unsigned char *);
extern fortint d_def_29_(fortint *, unsigned char *);
#endif

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

typedef fortint (*decodefunc)(fortint*, unsigned char*);

decodefunc decoder[50] = {
    /* 0  */ NULL,
    /* 1  */ d_def_1_,
    /* 2  */ d_def_2_,
    /* 3  */ d_def_3_,
    /* 4  */ d_def_4_,
    /* 5  */ d_def_5_,
    /* 6  */ d_def_6_,
    /* 7  */ d_def_7_,
    /* 8  */ d_def_8_,
    /* 9  */ d_def_9_,
    /* 10 */ d_def_10_,
    /* 11 */ d_def_11_,
    /* 13 */ d_def_13_,
    /* 14 */ d_def_14_,
    /* 15 */ d_def_15_,
    /* 16 */ d_def_16_,
    /* 17 */ d_def_17_,
    /* 18 */ d_def_18_,
    /* 19 */ d_def_19_,
    /* 20 */ d_def_20_,
    /* 21 */ d_def_21_,
    /* 22 */ d_def_22_,
    /* 23 */ d_def_23_,
    /* 24 */ d_def_24_,
    /* 25 */ d_def_25_,
#if 0 /* Move them up when they exist */
/* 26 */ d_def_26_,
/* 27 */ d_def_27_,
/* 28 */ d_def_28_,
/* 29 */ d_def_29_,
#endif
};

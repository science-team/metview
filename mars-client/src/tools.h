/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

err mars_compress(void* in, void* out, long inlen, long* outlen);
err mars_uncompress(void* in, void* out, long inlen, long* outlen);
#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

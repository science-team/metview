/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"

boolean in_list(marslist* l, const char* name, void* data) {
    while (l) {
        if (EQ(l->name, name))
            return true;
        l = l->next;
    }
    return false;
}


void add_to_list(marslist** l, const char* name, void* data) {
    marslist* f = NEW(marslist);
    f->name     = strcache(name);
    f->next     = *l;
    f->data     = data;
    *l          = f;
}

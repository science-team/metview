/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"

datetime date_time_2_datetime(long julian_date,
                              long hour,
                              long min,
                              long sec) {
    return julian_date * SECS_IN_DAY + hour * 3600 + min * 60 + sec;
}

datetime key_2_datetime(const packed_key* keyptr) {
    long date;

    date = KEY_YEAR(keyptr) * 10000 + KEY_MONTH(keyptr) * 100 + KEY_DAY(keyptr);
    date = mars_date_to_julian(date);
    return date * SECS_IN_DAY + KEY_HOUR(keyptr) * 3600 + KEY_MINUTE(keyptr) * 60 + KEY_SECOND(keyptr);
}

/* range is expected in seconds */
void init_time_interval(time_interval* t, datetime begin, long range) {
    t->begin = begin;
    t->end   = begin + range;
}

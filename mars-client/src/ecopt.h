/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

/*
** File: options.h
**
** Purpose: User customization file
**
** $Revision: 1.4 $    $Date: 1996/03/26 17:06:05 $
*/

/*
***********************************************************
** Part 1: You may want to change some of these definitions
***********************************************************
*/

/*
** This is part of the error message which a user will see in case the
** eccmd server encounters an internal error.  This should never happen.
** But ...
*/
#define ECQ_ADMIN_ERROR_MSG "Please inform your system administrator"

/*
***********************************************************************
** Part 2: You probably don't want to change anything beyond this point
***********************************************************************
*/

/*
** TCP host & reserved port used by the server. Do not change !
*/
#define ECQ_SERV_HOST "ecbatch.ecmwf.int"
#define ECQ_SERV_PORT 641

/*
** Version reflecting the revision level of the software. Do not change !
*/
#define ECQ_VERSION_MAJOR 0
#define ECQ_VERSION_MINOR 2

/*
** Protocol level, used to check that the client and the server run the
** same protocol level of the software.  If a protocol change is required, the
** protocol level should be set to the current revision level of the software.
** Do not change !
*/
#define ECQ_PROT_MAJOR 0
#define ECQ_PROT_MINOR 2

/*
** ECMARS certification protocol level for the verification reply
** returned by the eccmd server to the MARS server.  Do not change !
*/
#define ECQ_MARSPROT_MAJOR 0
#define ECQ_MARSPROT_MINOR 2

/*
** Timeouts in seconds for read/write and passcode wait time. Do not change !
*/
#define ECQ_IO_TIMEOUT (1 * 60)
#define ECQ_PASSC_TIMEOUT (2 * 60)

/*
** Maximum number of attempts when the user enters the Passcode before
** the command aborts.
**
** Minimum and maximum length of passcode.
** Do not change !
*/
#define ECQ_MAXPASSCODEREJECTCOUNT 3
#define ECQ_MINPASSCODELEN 6
#define ECQ_MAXPASSCODELEN 6

/*
** Maximum number of attempts when the user enters the Passcode before
** the command aborts.
**
** Minimum and maximum length of userids both for ECMWF and Member State.
** Do not change !
*/
#define ECQ_MINUSERIDLEN 3
#define ECQ_MAXUSERIDLEN 15

/*
** Maximum length of LIMITS to be imposed on new certificate by user
** or imposed by the server.  Do not change !
*/
#define ECQ_MAXLENCERTLIMITS 256

/*
** Length (in bytes) of "Request checksum" passed as trailer to ensure
** integrity.  Do not change !
*/
#define ECQ_REQUESTCHECKLEN 4

/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#define Q_ENTER 1
#define Q_LEAVE 2
#define Q_CHECK 3

#define Q_CONT 5
#define Q_WAIT 6
#define Q_DIE 7

#define Q_SLAVE 8
#define Q_SYNC 9
#define Q_FLUSH 10

/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#ifndef _H_BASE_
#define _H_BASE_

#define READ_MODE 0
#define WRITE_MODE 1
#define ADMIN_MODE 2

typedef void (*initproc)(void);
typedef err (*openproc)(void* data, request* r, request* e, int mode);
typedef err (*validproc)(void* data, request* r, request* e, int mode);
typedef err (*closeproc)(void* data);
typedef err (*readproc)(void* data, request* r, void* buffer, long* length);
typedef err (*writeproc)(void* data, request* r, void* buffer, long* length);
typedef err (*cntlproc)(void* data, int code, void* param, int size);
typedef err (*archproc)(void* data, request* r);
typedef void (*adminproc)(void*);


typedef boolean (*checkproc)(void* data, request* r);
typedef err (*queryproc)(void* data, request* r, long* length);

typedef struct base_class {

    struct base_class* parent;
    char* name;

    boolean inited;

    int private_size;
    int options_count;
    option* options;


    initproc init;
    openproc open;
    closeproc close;
    readproc read;
    writeproc write;
    cntlproc control;

    checkproc check;
    queryproc query;

    archproc archive;
    adminproc admin;

    validproc validate;

} base_class;

typedef struct database {
    base_class* driver;
    void* data;
    char* name;
    struct database* next;
} database;

extern base_class _nullbase;

#endif

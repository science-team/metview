/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

/*
 *
 * These are array offsets associated with arrays generated from
 * the MARS routine GRIBEX
 *
 * These are the limits required for return arrays of GRIBEX call
 */

#define ISECTION_0 2
#define ISECTION_1 1024 /* beware of  for ocean data */
#define ISECTION_2 3000
#define ISECTION_3 3
#define ISECTION_4 512

#define RSECTION_2 512
#define RSECTION_3 2
#define RSECTION_4 1

/*
 * This is the GRIBEX return code for Pseudo-grib data
 */
#define PSEUDO_GRIB -6

/*
#ifdef _AIX
#pragma options align=twobyte
#endif
*/

typedef struct griboffset {
    fortint section0;
    fortint section1;
    fortint section2;
    fortint section3;
    fortint section4;
    fortint edition;
} griboffset;

#define MAX_LOCALDEF 20
typedef struct multilocaldef_190 {
    fortint number;
    fortint length;
} multilocaldef_190;

typedef struct gribsec0 {
    fortint size;
    fortint edition;
} gribsec0;

typedef struct extra_spectra {
    fortint system;
    fortint method;
    fortint reference_date;
    fortint climate_from;
    fortint climate_to;
    fortint leg_base_date;
    fortint leg_base_time;
    fortint leg_number;
} extra_spectra;

typedef struct griblocsec1 {

    /*  37 */ fortint ecmwf_local_id;
    /*  38 */ fortint mars_class;
    /*  39 */ fortint mars_type;
    /*  40 */ fortint mars_stream;
    /*  41 */ fortint mars_expver;


    union extrasec1 {

        struct {

            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint reference_date;

        } hindcast;

        struct {

            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint clustering_method;
            /*  45 */ fortint clustering_start_step;
            /*  46 */ fortint clustering_end_step;
            /*  47 */ fortint mars_north;
            /*  48 */ fortint mars_west;
            /*  49 */ fortint mars_south;
            /*  50 */ fortint mars_east;
            /*  51 */ fortint clustering_operational_forecast_number;
            /*  52 */ fortint clustering_control_forecast_number;
            /*  53 */ fortint clustering_total_forecast_number;
            /*  54 */ fortint tube_reference;

        } enfo;

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint ocean_coupling;
            /*  45 */ fortint spare;
            /*  46 */ fortint leg_base_date;
            /*  47 */ fortint leg_base_time;
            /*  48 */ fortint leg_number;
            /*  49 */ fortint date_of_forecast_run; /* same as coupled_atmospheric_ocean.reference_date */
            /*  50 */ fortint climate_date_from;
            /*  51 */ fortint climate_date_to;
        } variable_resolution;

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint threshold_decimal_scale_factor;
            /*  45 */ fortint threshold_indicator;
            /*  46 */ fortint lower_threshold;
            /*  47 */ fortint upper_threshold;

        } probability;

        struct {
            /*  42 */ fortint iteration;
            /*  43 */ fortint dummy1;
            /*  44 */ fortint dummy2;
            /*  45 */ fortint diagnostic;
        } sens;

        struct {
            /*  42 */ fortint analysis_class;
            /*  43 */ fortint analysis_type;
            /*  44 */ fortint analysis_stream;
            /*  45 */ fortint analysis_version;
            /*  46 */ fortint analysis_year;
            /*  47 */ fortint analysis_month;
            /*  48 */ fortint analysis_day;
            /*  49 */ fortint analysis_hour;
            /*  50 */ fortint analysis_minute;
            /*  51 */ fortint analysis_century;
            /*  52 */ fortint analysis_center;
            /*  53 */ fortint analysis_subcenter;
        } sst;

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint direction;
            /*  45 */ fortint frequency;
            /*  46 */ fortint n_directions;
            /*  47 */ fortint n_frequencies;
            /*  48 */ fortint d_scale_factor;
            /*  49 */ fortint f_scale_factor;
            /*  .. */ fortint extra[1];
        } spectra;

        struct {
            /*  42 */ fortint type;
            /*  43 */ fortint function_code;
            /*  44 */ fortint spare;
        } image;

        struct {
            /*  42 */ fortint satellite_id;
            /*  43 */ fortint instrument;
            /*  44 */ fortint channel;
            /*  45 */ fortint function_code;
        } satellite_image; /* including pseudo-images */

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint channel;
            /*  45 */ fortint frequency_factor;
            /*  46 */ fortint nb_frequency;
            /*  47...   list of scaled frequencies */
        } brigthness_temperature;

        struct {

            /*  42 */ fortint mars_number;
            /*  43 */ fortint iterations;
            /*  44 */ fortint sv_computed;
            /*  45 */ fortint norm_initial_time;
            /*  46 */ fortint norm_final_time;
            /*  47 */ fortint ll_factor;
            /*  48 */ fortint mars_north;
            /*  49 */ fortint mars_west;
            /*  50 */ fortint mars_south;
            /*  51 */ fortint mars_east;
            /*  52 */ fortint accuracy;
            /*  53 */ fortint sv_evolved;
            /*  54 */ fortint ritz_1;
            /*  55 */ fortint ritz_2;

        } singular_vector;

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint system;
            /*  45 */ fortint method;
            /*  46 */ fortint space_unit;
            /*  47 */ fortint vertical_coordinate;
            /*  48 */ fortint horizontal_coordinates;
            /*  49 */ fortint time_unit;
            /*  50 */ fortint time_coordinate;
            /*  51 */ fortint mixed_coordinate;
            /*  52 */ fortint coordinate_1_locating;
            /*  53 */ fortint coordinate_1_averaging;
            /*  54 */ fortint coordinate_1_start_position;
            /*  55 */ fortint coordinate_1_end_position;
            /*  56 */ fortint coordinate_2_locating;
            /*  57 */ fortint coordinate_2_averaging;
            /*  58 */ fortint coordinate_2_start_position;
            /*  59 */ fortint coordinate_2_end_position;
            /*  60 */ fortint coordinate_3_axis;
            /*  61 */ fortint coordinate_4_axis;
            /*  62 */ fortint coordinate_4_start_grid_point;
            /*  63 */ fortint coordinate_3_start_grid_point;
            /*  64 */ fortint coordinate_4_end_grid_point;
            /*  65 */ fortint coordinate_3_end_grid_point;
            /*  66 */ fortint i_increment;
            /*  67 */ fortint j_increment;
            /*  68 */ fortint irregular_grid_coordinates;
            /*  69 */ fortint staggered_grid;
            /*  70 */ fortint further_information;
            /*  71 */ fortint dimension_horizontal_coordinates;
            /*  72 */ fortint dimension_mixed_coordinates;
            /*  73 */ fortint dimension_grid_coordinates;
            /*  74 */ fortint dimension_auxiliary_array;
        } ocean;

        struct {

            /*  42 */ fortint mars_number; /* 2 bytes */
            /*  43 */ fortint extra;       /* 0 */
            /*  44 */ fortint system;
            /*  45 */ fortint method;

        } seasonal;

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint system;
            /*  45 */ fortint method;
            /*  46 */ fortint verifying_month; /* yyyymm */
            /*  47 */ fortint averaging_period;
            /*  48 */ fortint forecast_month;
        } seasonal_means;

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint system;
            /*  45 */ fortint method;
            /*  46 */ fortint forecast_period; /* dddddd day1-day2*/
            /*  47 */ fortint averaging_period;
        } monthly_forecast_means;

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint system;
            /*  45 */ fortint method;
            /*  46 */ fortint verifying_month; /* Seasonal Forecast: yyyymm or Monthly Forecast: day1-day2 */
            /*  47 */ fortint averaging_period;
            /*  48 */ fortint forecast_month;
            /*  49 */ fortint reference_date; /* same as variable_resolution.date_of_forecast_run */
            /*  50 */ fortint climateDateFrom;
            /*  51 */ fortint climateDateTo;
            /*  52 */ fortint spareProbabilities[10];
        } coupled_atmospheric_ocean;

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint origin;
            /*  45 */ fortint model;
            /*  46 */ fortint count;
        } multi_analysis;

        struct {
            /*  42 */ fortint component_index;
            /*  43 */ fortint number_of_components;
            /*  44 */ fortint model_error_type;
        } model_error;

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint version_major_number;
            /*  45 */ fortint version_minor_number;
            /*  46 */ fortint original_subcenter;
            /*  47 */ fortint spare1;
            /*  48 */ fortint spare2;
            /*  49 */ fortint spare3;
            /*  50 */ fortint spare4;
            /*  51 */ fortint length;
            /*  -- */ char data[4096];
        } free_format;

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint number;
            /*  45 */ multilocaldef_190 def[MAX_LOCALDEF];
            /*  .. */
        } multi_definition_190;

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint number;
            /*  45 */ fortint localdef[2]; /* This is used as a pointer to memory */

        } multi_definition;

        struct {

            /*  42 */ fortint iteration;
            /*  43 */ fortint total_iterations;

        } increments;

        struct {

            /*  42 */ fortint mars_number;
            /*  43 */ fortint iterations;
            /*  44 */ fortint sv_computed;
            /*  45 */ fortint norm_initial_time;
            /*  46 */ fortint norm_final_time;
            /*  47 */ fortint ll_factor;
            /*  48 */ fortint mars_north;
            /*  49 */ fortint mars_west;
            /*  50 */ fortint mars_south;
            /*  51 */ fortint mars_east;
            /*  52 */ fortint accuracy;
            /*  53 */ fortint sv_evolved;
            /*  54 */ fortint ritz_1;
            /*  55 */ fortint ritz_2;
            /*  56 */ fortint optimisation_time;
            /*  57 */ fortint lead_time;
            /*  58 */ fortint domain;
            /*  59 */ fortint method;
            /*  60 */ fortint mars_total;
            /*  61 */ fortint shape;
            /*  .. */
        } sensitive_area_prediction;

        struct {

            /*  42 */ fortint mars_number; /* Zero */
            /*  43 */ fortint mars_total;
            /*  44 */ fortint scale_climate_weight;
            /*  45 */ fortint weight;
            /*  46 */ fortint first_month_climate_1;
            /*  47 */ fortint last_month_climate_1;
            /*  48 */ fortint first_month_climate_2;
            /*  49 */ fortint last_month_climate_2;
            /*  50 */ fortint order;
            /*  .. */

        } extreme_forecast_index;

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint forecast_month;
            /*  45 */ fortint date_of_forecast_run;
            /*  46 */ fortint number_of_models;
        } eurosip;

        struct {
            /*  42 */ fortint mars_number;
            /*  43 */ fortint mars_total;
            /*  44 */ fortint EPS_base_date;
            /*  45 */ fortint EPS_base_time;
            /*  46 */ fortint representative_member_number;
            /*  47 */ fortint members_in_cluster;
            /*  48 */ fortint total_EPS_members;
            /*  52 */ fortint spare[10];
        } cosmo;

    } u;

} griblocsec1;


typedef struct gribsec1 {
    /*   1 */ fortint version;
    /*   2 */ fortint center;
    /*   3 */ fortint generation;
    /*   4 */ fortint grid_definition;
    /*   5 */ fortint flags;
    /*   6 */ fortint parameter;
    /*   7 */ fortint level_type;
    /*   8 */ fortint top_level;
    /*   9 */ fortint bottom_level;
    /*  10 */ fortint year;
    /*  11 */ fortint month;
    /*  12 */ fortint day;
    /*  13 */ fortint hour;
    /*  14 */ fortint minute;
    /*  15 */ fortint time_unit;
    /*  16 */ fortint p1;
    /*  17 */ fortint p2;
    /*  18 */ fortint range;
    /*  19 */ fortint range_include;
    /*  20 */ fortint range_missing;
    /*  21 */ fortint century;
    /*  22 */ fortint sub_centre;
    /*  23 */ fortint scale_factor;
    /*  24 */ fortint local_use;
    /*  25 */ fortint reserved_25;
    /*  26 */ fortint reserved_26;
    /*  27 */ fortint reserved_27;
    /*  28 */ fortint reserved_28;
    /*  29 */ fortint reserved_29;
    /*  30 */ fortint reserved_30;
    /*  31 */ fortint reserved_31;
    /*  32 */ fortint reserved_32;
    /*  33 */ fortint reserved_33;
    /*  34 */ fortint reserved_34;
    /*  35 */ fortint reserved_35;
    /*  36 */ fortint reserved_36;

    /*  37...*/ griblocsec1 local;

    fortint stuff[ISECTION_1];

} gribsec1;

/* To point to any group of atributes which are not
   at fixed offset on section 1 */
typedef struct gribsec1_extra {

    union {

        struct {
            /* n   */ fortint count;
            /* n+1 */ fortint base_date;
            /* n+2 */ fortint base_time;
            /* n+3 */ fortint step;
        } ocean;

    } u;

} gribsec1_extra;

typedef struct stepRange {
    long from;
    long to;
} stepRange;

#define GRIB_ENSEMBLE_FORECAST 1
#define GRIB_CLUSTER_MEANS 2
#define GRIB_SATELLITE_IMAGE 3
#define GRIB_OCEAN_MODEL_DATA 4
#define GRIB_FORECAST_PROBABILITY 5
#define GRIB_SURFACE_TEMPERATURE 6
#define GRIB_SENSITIVITY 7
#define GRIB_REANALYSIS 8
#define GRIB_SINGULAR_VECTOR 9
#define GRIB_TUBE 10
#define GRIB_SST 11
#define GRIB_WAVE2D 13
#define GRIB_BRIGHTNESS_TEMPERATURE 14
#define GRIB_SEASONAL_FORECAST 15
#define GRIB_SEASONAL_FORECAST_MEAN 16
#define GRIB_SEA_ICE_TEMPERATURE 17
#define GRIB_MULTI_ANALYSIS 18
#define GRIB_EFI 19
#define GRIB_4V_INCREMENTS 20
#define GRIB_SENSITIVE_AREA_PREDICTION 21
#define GRIB_MULTI_DEFINITION_190 190
#define GRIB_FREE_FORMAT 191
#define GRIB_MULTI_DEFINITION 192

#define GRIB_COUPLED_ATMOSPHERIC_OCEAN 23
#define GRIB_NEW_SATELLITE_IMAGE 24
#define GRIB_MODEL_ERROR 25

#define GRIB_HINDCAST 26
#define GRIB_VARIABLE_RESOLUTION_OBSOLETE 27
#define GRIB_COSMO 28
#define GRIB_COSMO_CLUSTER_INFORMATION 29
#define GRIB_VARIABLE_RESOLUTION 30
#define GRIB_EUROSIP 31
#define GRIB_EXTERNAL_DATA 50
#define GRIB_SREPS 244

#define GRIB_CLASS_OD 1
#define GRIB_CLASS_RD 2
#define GRIB_CLASS_ER 3
#define GRIB_CLASS_CS 4
#define GRIB_CLASS_ERA_40 5
#define GRIB_CLASS_DEMETER 6
#define GRIB_CLASS_PROVOST 7
#define GRIB_CLASS_ELDAS 8
#define GRIB_CLASS_TOST 9
#define GRIB_CLASS_COSMO 10
#define GRIB_CLASS_ENSEMBLES 11
#define GRIB_CLASS_TIGGE 12
#define GRIB_CLASS_MERSEA 13
#define GRIB_CLASS_ERA_INTERIM 14
#define GRIB_CLASS_SREPS 15
#define GRIB_CLASS_DTS 16
#define GRIB_CLASS_LACE 17
#define GRIB_CLASS_YOTC 18
#define GRIB_CLASS_MS 121

#define GRIB_CLASS_TEST 99
#define GRIB_CLASS_METAPS 199

#define GRIB_STREAM_OPER 1025
#define GRIB_STREAM_SHORT_CUT_OFF 1026
#define GRIB_STREAM_SHORT_CUT_OFF_WAVE 1027
#define GRIB_STREAM_DELAYED_CUT_OFF 1028
#define GRIB_STREAM_DELAYED_CUT_OFF_WAVE 1029
#define GRIB_STREAM_ENFO 1035
#define GRIB_STREAM_ENFO_OVERLAP 1034
#define GRIB_STREAM_TOGA 1041
#define GRIB_STREAM_SUPD 1044
#define GRIB_STREAM_WAVE 1045
#define GRIB_STREAM_OCEA 1046
#define GRIB_STREAM_FGGE 1047
#define GRIB_STREAM_SENS 1036
#define GRIB_STREAM_MNTH 1043
#define GRIB_STREAM_WAMO 1080
#define GRIB_STREAM_WAEF 1081
#define GRIB_STREAM_WASF 1082
#define GRIB_STREAM_WAEF_OVERLAP 1086
#define GRIB_STREAM_WAVE_STANDALONE_MODEL 1087
#define GRIB_STREAM_EGRR 1050 /* Bracknell */
#define GRIB_STREAM_KWBC 1051 /* Washington */
#define GRIB_STREAM_EDZW 1052 /* Offenbach */
#define GRIB_STREAM_LFPW 1053 /* Paris or Toulouse */
#define GRIB_STREAM_RJTD 1054 /* Tokyo */
#define GRIB_STREAM_CWAO 1055 /* Montreal */
#define GRIB_STREAM_AMMC 1056 /* Melbourne */
#define GRIB_STREAM_CNRM 2231
#define GRIB_STREAM_MPIC 2232
#define GRIB_STREAM_UKMO 2233
#define GRIB_STREAM_CHER 1042


/* Hindcasts */
#define GRIB_STREAM_OPER_HINDCAST 1024
#define GRIB_STREAM_ENFO_HINDCAST_OBSOLETE 1039
#define GRIB_STREAM_WAEF_HINDCAST_OBSOLETE 1084
#define GRIB_STREAM_WAVE_HINDCAST 1085

#define GRIB_STREAM_ENFH_HINDCAST 1033
#define GRIB_STREAM_ENWH_HINDCAST 1079
#define GRIB_STREAM_EFHO_HINDCAST 1032
#define GRIB_STREAM_EWHO_HINDCAST 1078
#define GRIB_STREAM_ENSEMBLE_HINDCAST_STATISTICS 1040
#define GRIB_STREAM_WAVE_ENSEMBLE_HINDCAST_STATISTICS 1077


#define GRIB_STREAM_MV 1070
#define GRIB_STREAM_MODA 1071
#define GRIB_STREAM_MOR 1072
#define GRIB_STREAM_MVR 1073
#define GRIB_STREAM_MSDA 1074
#define GRIB_STREAM_MDFA 1075
#define GRIB_STREAM_DACL 1076
#define GRIB_STREAM_DACW 1089

#define GRIB_STREAM_SEAS 1090
#define GRIB_STREAM_SEAS_MM 1091
#define GRIB_STREAM_SEAS_MMA 1097
#define GRIB_STREAM_WAVE_SEAS_MM 1092

#define GRIB_STREAM_MONTHLY_FC 1093
#define GRIB_STREAM_MONTHLY_FC_MEANS 1094
#define GRIB_STREAM_WV_MONTHLY_FC 1095
#define GRIB_STREAM_WV_MONTHLY_FC_MEANS 1096

#define GRIB_STREAM_MAED 1037
#define GRIB_STREAM_AMAP 1038
#define GRIB_STREAM_MAWV 1083

#define GRIB_STREAM_SENS_AREA_PREDICTION 1110

/* Definition of new Monthly Forecasts */
#define GRIB_STREAM_NEW_MONTHLY_FORECAST 1200
#define GRIB_STREAM_NEW_MONTHLY_FORECAST_HINDCAST 1201
#define GRIB_STREAM_NEW_MONTHLY_FORECAST_ANOMALY 1202

#define GRIB_STREAM_NEW_MONTHLY_FORECAST_WAVE 1203
#define GRIB_STREAM_NEW_MONTHLY_FORECAST_HINDCAST_WAVE 1204
#define GRIB_STREAM_NEW_MONTHLY_FORECAST_ANOMALY_WAVE 1205

#define GRIB_STREAM_NEW_MONTHLY_FORECAST_MEANS 1206
#define GRIB_STREAM_NEW_MONTHLY_FORECAST_HINDCAST_MEANS 1207
#define GRIB_STREAM_NEW_MONTHLY_FORECAST_ANOMALY_MEANS 1208

#define GRIB_STREAM_NEW_MONTHLY_FORECAST_WAVE_MEANS 1209
#define GRIB_STREAM_NEW_MONTHLY_FORECAST_HINDCAST_WAVE_MEANS 1210
#define GRIB_STREAM_NEW_MONTHLY_FORECAST_ANOMALY_WAVE_MEANS 1211

/* Definition of Multi-model Seasonal Forecasts */
#define GRIB_STREAM_MULTIMODEL_SEASONAL_FORECAST 1220
#define GRIB_STREAM_MULTIMODEL_SEASONAL_FORECAST_MEANS 1221
#define GRIB_STREAM_MULTIMODEL_SEASONAL_FORECAST_WAVE 1222
#define GRIB_STREAM_MULTIMODEL_SEASONAL_FORECAST_WAVE_MEANS 1223
#define GRIB_STREAM_MULTIMODEL_SEASONAL_FORECAST_MEAN_ANOMALIES 1224

/* Definition of EUROSIP */
#define GRIB_STREAM_EUROSIP_MONTHLY_MEANS 1240
#define GRIB_STREAM_EUROSIP_HINDCAST_MONTHLY_MEANS 1241

/* Definition of Multi-model Multi-Annual Forecasts */
#define GRIB_STREAM_MULTIMODEL_MULTIANNUAL_FORECAST 1230
#define GRIB_STREAM_MULTIMODEL_MULTIANNUAL_FORECAST_MEANS 1231
#define GRIB_STREAM_MULTIMODEL_MULTIANNUAL_FORECAST_WAVE 1232
#define GRIB_STREAM_MULTIMODEL_MULTIANNUAL_FORECAST_WAVE_MEANS 1233

/* Definition of Ensemble Data Assimilation */
#define GRIB_STREAM_ENSEMBLE_DATA_ASSIMILATION 1030
#define GRIB_STREAM_ENSEMBLE_WAVE_DATA_ASSIMILATION 1088


typedef struct gribsec2_ll {
    /*   1 */ fortint data_rep;
    /*   2 */ fortint points_parallel;
    /*   3 */ fortint points_meridian;
    /*   4 */ fortint limit_north;
    /*   5 */ fortint limit_west;
    /*   6 */ fortint resolution;
    /*   7 */ fortint limit_south;
    /*   8 */ fortint limit_east;
    /*   9 */ fortint grid_ew;
    /*  10 */ fortint grid_ns;
    /*  11 */ fortint scan_mode;
    /*  12 */ fortint vertical;
    /*  13 */ fortint rotation_lat;
    /*  14 */ fortint rotation_lon;
    /*  15 */ fortint stretch_lat;
    /*  16 */ fortint stretch_lon;
    /*  17 */ fortint quasi_regular;
    /*  18 */ fortint stuff[ISECTION_2];
} gribsec2_ll;

typedef struct gribsec2_og {
    /*   1 */ fortint data_rep;
    /*   2 */ fortint points_parallel;
    /*   3 */ fortint points_meridian;
    /*   4 */ fortint reserved_1;
    /*   5 */ fortint reserved_2;
    /*   6 */ fortint reserved_3;
    /*   7 */ fortint reserved_4;
    /*   8 */ fortint reserved_5;
    /*   9 */ fortint reserved_6;
    /*  10 */ fortint reserved_7;
    /*  11 */ fortint scan_mode;
    /*  12 */ fortint reserved_8;
    /*  13 */ fortint reserved_9;
    /*  14 */ fortint reserved_10;
    /*  15 */ fortint reserved_11;
    /*  16 */ fortint reserved_12;
    /*  17 */ fortint reserved_13;
    /*  18 */ fortint stuff[ISECTION_2];
} gribsec2_og;

typedef struct gribsec2_gg {
    /*   1 */ fortint data_rep;
    /*   2 */ fortint points_parallel;
    /*   3 */ fortint points_meridian;
    /*   4 */ fortint limit_north;
    /*   5 */ fortint limit_west;
    /*   6 */ fortint resolution;
    /*   7 */ fortint limit_south;
    /*   8 */ fortint limit_east;
    /*   9 */ fortint grid_ew;
    /*  10 */ fortint gauss_trunc;
    /*  11 */ fortint scan_mode;
    /*  12 */ fortint vertical;
    /*  13 */ fortint rotation_lat;
    /*  14 */ fortint rotation_lon;
    /*  15 */ fortint stretch_lat;
    /*  16 */ fortint stretch_lon;
    /*  17 */ fortint quasi_regular;
    /*  18 */ fortint earth_flag;
    /*  19 */ fortint components_flags;
    /*  20 */ fortint reserved_20;
    /*  21 */ fortint reserved_21;
    /*  22 */ fortint reserved_22;
    /*  23 */ fortint stuff[ISECTION_2];
} gribsec2_gg;

typedef struct gribsec2_sh {
    /*   1 */ fortint data_rep;
    /*   2 */ fortint j_resolution;
    /*   3 */ fortint k_resolution;
    /*   4 */ fortint m_resolution;
    /*   5 */ fortint rep_type;
    /*   6 */ fortint rep_mode;
    /*   7 */ fortint reserved_1;
    /*   8 */ fortint reserved_2;
    /*   9 */ fortint reserved_3;
    /*  10 */ fortint reserved_4;
    /*  11 */ fortint reserved_5;
    /*  12 */ fortint reserved_6;
    /*  13 */ fortint reserved_7;
    /*  14 */ fortint rotation_lat;
    /*  15 */ fortint rotation_lon;
    /*  16 */ fortint stuff[ISECTION_2];
} gribsec2_sh;

typedef struct gribsec2_sv {
    /*   1 */ fortint data_rep;
    /*   2 */ fortint nx;
    /*   3 */ fortint ny;
    /*   4 */ fortint latitude_point;
    /*   5 */ fortint longitude_point;
    /*   6 */ fortint resolution;
    /*   7 */ fortint x_diameter;
    /*   8 */ fortint y_diameter;
    /*   9 */ fortint x_coordinate;
    /*  10 */ fortint y_coordinate;
    /*  11 */ fortint scan_mode;
    /*  12 */ fortint vertical_coordinates;
    /*  13 */ fortint orientation;
    /*  14 */ fortint altitude;
    /*  15 */ fortint x_origin;
    /*  16 */ fortint y_origin;
    /*  17 */ fortint stuff[ISECTION_2];
} gribsec2_sv;

typedef union gribsec2 {
    gribsec2_ll ll;
    gribsec2_og og;
    gribsec2_gg gg;
    gribsec2_sh sh;
    gribsec2_sv sv;
} gribsec2;

/*
 * These are the offset of the data representation type in GRIB sections 2
 * plus the values used for further analysis
 */


#define GRIB_LAT_LONG 0
#define GRIB_ROTATED_LAT_LONG 10
#define GRIB_OCEAN_GRID 192
#define GRIB_GAUSSIAN 4
#define GRIB_ROTATED_GAUSSIAN 14
#define GRIB_STRETCHED_GAUSSIAN 24
#define GRIB_SPHERICAL_HARMONIC 50
#define GRIB_ROTATED_SPHERICAL_HARMONIC 60
#define GRIB_STRETCHED_SPHERICAL_HARMONIC 70
#define GRIB_SPACE_VIEW 90

/*
 *	The first value is the scale factor for the degree values stored in grib
 *
 *	GRIB_DEGREE = int (REAL_DEGREE * GRIB_FACTOR)
 *
 *	The second factor is the additional factor used for calculating the
 *	correct degree values. This has to be used for 640 by 320 grids
 */

#define GRIB_FACTOR 1000
#define GRIB_EXTRA_FACTOR 10
#define WORK_FACTOR (GRIB_FACTOR * GRIB_EXTRA_FACTOR)
#define GLOBE360 (360 * WORK_FACTOR)
#define POLE90 (90 * GRIB_FACTOR)

/*
 * Definitions for Gaussian grids
 *
 * Values 1 through 8 are identical to latitude/longitude grids
 */

#define GRIB_QUASI_REGULAR 16

/*
 * These are the GRIB_LEVEL_TYPE_1 values in use at ECMWF
 */

#define GRIB_SURFACE 1
#define GRIB_CLOUD_BASE 2
#define GRIB_CLOUD_TOP 3
#define GRIB_ISO_0 4

#define GRIB_TOP_OF_ATMOSPHERE 8
#define GRIB_PRESSURE_LEVEL 100
#define GRIB_ABOVE_1HPA 210
#define GRIB_MEAN_SEA_LEVEL 102
#define GRIB_HEIGHT_ABOVE_GROUND 105
#define GRIB_SIGMA_LEVEL 108
#define GRIB_MODEL_LEVEL 109
#define GRIB_MODEL_LEVEL_LAYER 110
#define GRIB_DEPTH_BELOW_LAND 111
#define GRIB_SUB_SURFACE 112
#define GRIB_POTENTIAL_TEMPERATURE 113
#define GRIB_POTENTIAL_VORTICITY 117
#define GRIB_DEPTH_BELOW_SEA_LEVEL 160
#define GRIB_ATMOSPHERE_AS_SINGLE_LAYER 200

#define GRIB_OCEAN_WAVE_LEVEL 211

#define GRIB_WMO_TABLE_1 1
#define GRIB_WMO_TABLE_2 2
#define GRIB_WMO_TABLE_3 3
#define GRIB_ATMOSPHERE_TABLE 128
#define GRIB_ATMOSPHERE_TABLE_GRADIENTS 129
#define GRIB_ASTEX_TABLE 130
#define GRIB_PROBABILITY_TABLE 131
#define GRIB_EFI_TABLE 132
#define GRIB_WAVE_TABLE 140
#define GRIB_PRELIMINARY_OCEAN_TABLE 150
#define GRIB_OPERATIONAL_OCEAN_TABLE 151
#define GRIB_REANALYSIS_STATISTICS_TABLE 160
#define GRIB_VERTICAL_INTEGRALS_TABLE 162
#define GRIB_SEASONAL_FORECAST_TABLE 170
#define GRIB_CLIMATE_SIMULATION_TABLE 180
#define GRIB_DEMETER_TABLE 190
#define GRIB_INCREMENTS_TABLE 200

/* Tables for DWD and Italy */
#define GRIB_TABLE_201 201
#define GRIB_TABLE_202 202

#define GRIB_MODEL_OD 40
#define GRIB_MODEL_WAVE_G 104
#define GRIB_MODEL_WAVE_M 204

#define GRIB_DEPTH 2
#define GRIB_LONGITUDE 3
#define GRIB_LATITUDE 4


/* WMO Centre identifier */

#define WMO_CENTRE_ID_ECMF 98             /* ecmwf */
#define WMO_CENTRE_ID_AMMC 1              /* melbourne */
#define WMO_CENTRE_ID_KWBC 7              /* washington */
#define WMO_CENTRE_ID_CWAO 54             /* montreal */
#define WMO_CENTRE_ID_PARIS 84            /* toulouse */
#define WMO_CENTRE_ID_LFPW 85             /* toulouse */
#define WMO_CENTRE_ID_EGRR 74             /* bracknell */
#define WMO_CENTRE_ID_EDZW 78             /* offenbach */
#define WMO_CENTRE_ID_RJTD 34             /* tokyo */
#define WMO_CENTRE_ID_FNMO 58             /* Fleet Numerical Meteorology and Oceanography Centre */
#define WMO_CENTRE_ID_RSMC 80             /* roma */
#define WMO_CENTRE_ID_EHDB 99             /* De Bilt */
#define WMO_CENTRE_ID_LEMM 214            /* Spanish INM */
#define WMO_CENTRE_ID_LACE 224            /* LACE or Vienna? */
#define WMO_CENTRE_ID_INGV 235            /* INGV-Bologna */
#define WMO_CENTRE_ID_CRFC 239            /* CERFACS */
#define WMO_CENTRE_ID_VUWIEN 244          /* University of Veterinary Medicine Vienna */
#define WMO_CENTRE_ID_KNMI 245 /* KNMI */ /* THIS IS WRONG. SHOULD BE 99, De Bilt, CCCC=EHDB */
#define WMO_CENTRE_ID_IFMK 246            /* Institut fuer Meererskunde-Kiel */

#ifdef _AIX
#pragma options align = reset
#endif

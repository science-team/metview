/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

/*

M.Fuentes
ECMWF Jul-97

Exactly the same as 'handle_control', but it does not
fail if one of the databases succeded

*/

#include <errno.h>
#include "mars.h"

err handle_remove(request* r, void* data) {
    const char* s = get_value(r, "DATABASE", 0);

    if (s == 0) {
        marslog(LOG_EROR, "%s needs DATABASE set", request_verb(r));
        return -5;
    }
    else {
        err e       = 0;
        int i       = 0;
        int success = 0;

        while ((s = get_value(r, "DATABASE", i++)) != NULL) {
            const char* name = 0;
            request* cache   = 0;
            database* bout   = 0;

            const request* b = findbase(s, r);
            if (!b)
                return -2;

            bout = openbase(b, r, &name, &cache, READ_MODE);
            if (!bout)
                e = -1;
            else {
                e = database_control(bout, CNTL_REMOVE, r, 0);
                database_close(bout);
                if (e)
                    marslog(LOG_WARN, "REMOVE failed for database '%s'", name);
                else {
                    marslog(LOG_INFO, "REMOVE performed on database '%s'", name);
                    success++;
                }
            }
        }

        return (success > 0) ? NOERR : -2;
    }

    /*NOTREACHED*/
    return 0;
}

/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

typedef struct hypercube {
    request* cube;
    request* r;
    request* iterator;
    char* set;
    int count;
    int size;
    int max;
    int* index_cache;
    int index_cache_size;
    namecmp* compare;
} hypercube;


typedef struct axis_t {
    const char* name;
    namecmp compare;
} axis_t;

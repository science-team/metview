/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */


typedef void (*msgproc)(int, const char*);
typedef void (*cplproc)(request*, int, err);
typedef int (*memproc)(void*);

typedef void (*progress_proc)(void);

typedef struct globals {

    char* appl; /* name of appl: mars, xmars, ... */

    boolean debug;             /* debug mode */
    boolean echo;              /* echo user request */
    boolean verbose;           /* echo compiled request */
    boolean casecomp;          /* fielset names are case sensitive */
    boolean nofork;            /* no forks */
    boolean restriction;       /* restrict observation access */
    boolean quiet;             /* quiet */
    boolean infomissing;       /* inform missing fields from database */
    boolean dont_trap_signals; /* dont trap any signal */
    boolean logrequest;        /* activate logging of the raw mars request  */

    request* setup;

    /* files */

    char* mars_home;

    char* config;
    char* config_dev;
    char* langfile;
    char* testfile;
    char* logreqfile;

    /* User authentication */
    char* authfile;
    char* authmail;
    char* validate;
    char* emsfile;

    /* Mailer */
    char* mailer;

    char* monhost;
    int monport;

    msgproc outproc;  /* where go the messages    */
    cplproc complete; /* completion proc          */

    /* progress */
    progress_proc progress;
    long64 todo;
    long64 done;

    long expflags; /* expand flags             */
    int maxforks;  /* maximun forks            */

    int computeflg;            /* value set to computed gribs */
    double grib_missing_value; /* Missing value for gribex */

    long request_id; /* current request id */

    int certlen;   /* certificate length */
    char* certstr; /* certificate string */
    boolean certify;

    char* dhsmail; /* Mail if DHS archive failed */

    /* For archive */

    boolean fields_are_ok;         /* For new ibm */
    boolean fields_in_order;       /* For old ibm */
    boolean fields_have_same_size; /* For ensemble */
    boolean pseudogrib;            /* for BUDG/TIDE */
    boolean dont_check_pseudogrib; /* for BUDG/TIDE */

    boolean y2k;
    boolean y2k_fail; /* Fail if dates entered are not 4 digit years */
    boolean crc;
    boolean ignore_7777; /* to ignore missing 7777 error */
    boolean autoresol;   /* use AUTO RESOLUTION */
    long autoarch;       /* use AUTO RESOLUTION */

    long y2k_problem; /* Y2K problems */
    char* sms_label;  /* MARS is under sms control */

    int number_of_requests;
    int current_request;

    boolean patch_sst; /* Patch request to use new SST */

    /* For compute */
    int accuracy;

    boolean warning; /* display warnings */

    int so_sndbuf;        /* for setsockopt */
    int so_rcvbuf;        /* for setsockopt */
    int tcp_maxseg;       /* for setsockopt */
    int tcp_window_clamp; /* for setsockopt */
    char* tcp_congestion; /* for setsockopt */


    /* In server mode, number of clients to accept */
    int clients;

    int show_pid; /* Will output pid on output */

    char* statfile; /* File for statistics */

    char* udp_server;     /* UDP server for statistics */
    long udp_server_port; /* UDP server port for statistics */

    long mail_frequency; /* How frequent to send e-mail in seconds */
    char* home;          /* User home directory for MARS: usually ~/.marsrc */

    long64 max_filesize; /* Maximum file size client can archive */

    int maxretries;     /* Maximum number of retries */
    boolean paramtable; /* Set table for parameter in grib2request */

    boolean grib_postproc; /* Whether to allow GRIB post-processing (default true) */

    boolean bufr_empty_target; /* Whether a retrieval of BUFR for no data should create an empty target */

    boolean mm_firstofmonth; /* If true, monthly means date is 1st of month */

    boolean use_intuvp; /* Whether to use INTUVP for wind interpolation */

    boolean debug_ems; /* Whether to print EMS information */

    boolean can_do_vector_postproc; /* Whether to call intvect for winds, etc... */

    boolean info;                          /* display info */
    boolean gridded_observations_postproc; /* Whether we can post-process gridded observations */
    int dissemination_schedule;            /* Whether MARS should fail, inform or just log when requests have
                                              been submitted earlier than the dissemination schedule */
    char* dissemination_schedule_file;     /* File containing dissemination schedule */
    boolean daily_climatology;             /* in order to be able to compare two dates: ????mmdd with yyyymmdd */

    boolean no_special_fp;               /* whether Forecast probabilities are p1-p2 (true) or d1d2 with p1+24 for rain (false) in FDB. */
    int year_for_daily_climatology;      /* What year to use in grib2request for Daily Climatology */
    boolean notimers;                    /* Set to 1 in order to disable timers */
    boolean valid_data_includes_fcmonth; /* Set to != 0 in order to consider FCMONTH when deciding */
                                         /* if it is valid data or not */

    boolean exit_on_failed_expand; /* Set to 1 in order to force MARS to exit when expand of language file */
                                   /* doesn't work, for example, if language file is not found */
    char* emsaccountsfile;
    boolean force_setting_ecaccount; /* Set to 1 in order to force MARS to set the value of ECACCOUNT */
    boolean enable_ecaccount_email;  /* Set to 0 in order to avoid receiving emails when ECACCOUNT */
                                     /* is not in the list of valid ECACCOUNTs for the user */

    char* timers_file; /* Filename were to print timers */

    boolean wind_requested_by_server;

    int readdisk_buffer; /* Size of buffer to read from disk. Calls setvbuf */

    char* ecflow_label; /* MARS is under ecflow control */
    char* private_key;  /* PATH to private key, e.g. ~/.ssh/id_rsa */

    boolean build_grib_index;   /* prepare index to send to server */
    long64 readany_buffer_size; /* Size of buffer to pass to readany */

    char* webmars_target; /* Used by mars when serving web requests */
    boolean show_hosts;   /* Show hosts in logfiles */

    boolean marslite_mode;        /* Whether this client is running on behalf of a marslite client */
    boolean enforce_ems_category; /* Whether we have to fail if no EMS category is found. Default true: Aug 2013 */

    int expand_timeout;          /* maximum time to expand mars request (used to prevent silly requests) */
    boolean auto_split_by_dates; /* if set, split retrieve requests in monthly parts */
    boolean auto_split_by_day;   /* if set, split retrieve requests in monthly parts */

    boolean keep_database;    /* by default, webmars removes the database provided by user */
    char* webmars_request_id; /* Contains the Web API id, so a request can be traced */

    long64 max_retrieve_size; /* maximum retrieval size  (0= no limit) */
    long64 retrieve_size;     /* retrieved so far */

    boolean privileged; /* access to restricted observations */


    boolean remove_bufr_duplicates; /* Used by multibase to force removing of duplicate */

    char* proxies; /* File of TCP proxies */

} globals;

extern globals mars;

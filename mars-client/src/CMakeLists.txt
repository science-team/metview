### config headers

# if we are not building inside the mars-client-bundle we won't have this defined
if(NOT DEFINED MARSCLIENT_VERSION_STR )
  set(MARSCLIENT_VERSION_STR ${MARS_CLIENT_VERSION_STR})
endif()

configure_file( mars_client_version.h.in  mars_client_version.h )
configure_file( mars_client_version.c.in  mars_client_version.c )

# compile definitions

ecbuild_generate_config_headers()

configure_file( mars_client_config.h.in    mars_client_config.h   )

# install generated headers

install(FILES
			${CMAKE_CURRENT_BINARY_DIR}/mars_client_version.h
			${CMAKE_CURRENT_BINARY_DIR}/mars_client_config.h
		DESTINATION
			${INSTALL_INCLUDE_DIR}/marsclient )

add_definitions( -DR64 )

if( ECMWF )
    add_definitions( -DECMWF )
endif()

test_big_endian( _BIG_ENDIAN )

if( _BIG_ENDIAN )
	message( STATUS "System is big endian" )
else()
	message( STATUS "System is little endian" )
	add_definitions(-DLITTLE_END)
endif()

if( NOT HAVE_FDB )
    add_definitions(-DNOFDB)
endif()

if( NOT HAVE_FDB5 )
    add_definitions(-DNOFDB5)
endif()

if( NOT HAVE_ODB )
	add_definitions(-DNOODB)
else()
	add_definitions(-DODB_SUPPORT )
	include_directories( ${ODB_API_INCLUDE_DIRS} ${ECKIT_INCLUDE_DIRS} )
endif()

if( HAVE_PPROC_EMOS )
    list( APPEND mars_client_src_files pproc_emos.cc )
endif()

if( HAVE_PPROC_MIR )
    list( APPEND mars_client_src_files pproc_mir.cc )
endif()

if( NOT HAVE_CURL )
	add_definitions(-DNOCURL)
endif()

if(HAVE_ECMWF_DEVELOPMENT)
  add_definitions(-DECMWF)
  add_definitions(-DNOEMS)
  add_definitions(-DNOAUTH)
  add_definitions(-DNOSCHEDULE)
endif()

# sources

list( APPEND mars_client_src_files
account.c
archive.c
authenticate.c
base.c
bufr.c
calc.c
certify.c
check.c
control.c
cos.c
ecaccess.c
eccert.c
environ.c
expand.c
extargs.c
externf.c
fdb5base.cc
forwardbase.c
feedtask.c
field.c
filebase.c
files.c
flatfilebase.c
free.c
gribbase.c
grib.c
guess.c
handler.c
hash.c
hidden.c
hypercube.c
ibmblk.c
index.c
langy.c
list.c
lock.c
logfile.c
mcs.c
memory.c
dhsbase.c
metadata.c
msbase.c
multibase.c
netbase.c
netcdf.c
hdf5.c
nfdbbase.c
nullbase.c
options.c
pproc.h
pproc.cc
pproc_none.cc
queue.c
rdb.c
remove.c
request.c
restricted.c
retrieve.c
schedule.c
server.c
service.c
sh2ll.c
statistics.c
stream.c
target.c
chunked.c
tcp.c
proxies.c
time.c
timer.c
tools.c
udp.c
variable.c
version.c
json.c
api.c
apibase.c
wind.c)

if(HAVE_UDP_STATS)
  list(APPEND mars_client_src_files udp_statistics.cc)
endif()

if(HAVE_NETCDF)

  list(APPEND mars_client_src_files
    nctarget.c
    nctypes.c
    ncschema.c
    ncmerge.c
    )

endif()

if(HAVE_ODB)
    list(APPEND mars_client_src_files
        odb.cc
    )
endif()

ecbuild_find_lexyacc()
if(FLEX_FOUND)
    set(_lex ${FLEX_EXECUTABLE})
elseif(LEX_FOUND)
    set(_lex ${LEX_EXECUTABLE})
else()
    ecbuild_critical("Found neither flex nor lex")
endif()

if(YACC_FOUND)
    set(_yacc ${YACC_EXECUTABLE})
elseif(BISON_FOUND)
    set(_yacc ${BISON_EXECUTABLE} -y)
else()
    ecbuild_critical("Found neither yacc nor bison")
endif()

if(mars_client_HAVE_RPC)

  list( APPEND mars_client_src_files marsxdr.c )
  list( APPEND mars_client_deps marsxdr )

  if( DEFINED RPCGEN_PATH )
      find_program( RPCGEN_EXECUTABLE NAMES rpcgen PATHS ${RPCGEN_PATH} PATH_SUFFIXES bin NO_DEFAULT_PATH )
  endif()
  find_program( RPCGEN_EXECUTABLE NAMES rpcgen )
  if( NOT RPCGEN_EXECUTABLE )
    ecbuild_critical("Could not find rpcgen. Please provide RPCGEN_PATH.")
  endif()

  # targets

  configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/rpcmars.x ${CMAKE_CURRENT_BINARY_DIR}/rpcmars.x COPYONLY )

  add_custom_command(
      OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/rpcmars.h
      COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_CURRENT_BINARY_DIR}/rpcmars.h
      COMMAND ${RPCGEN_EXECUTABLE} -h -o ${CMAKE_CURRENT_BINARY_DIR}/rpcmars.h ${CMAKE_CURRENT_BINARY_DIR}/rpcmars.x
      DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/rpcmars.x )

  add_custom_command(
      OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/marsxdr.c
      COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_CURRENT_BINARY_DIR}/marsxdr.c
      COMMAND ${RPCGEN_EXECUTABLE} -c -o ${CMAKE_CURRENT_BINARY_DIR}/marsxdr.c ${CMAKE_CURRENT_BINARY_DIR}/rpcmars.x
      DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/rpcmars.x )

  add_custom_target(marsxdr DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/rpcmars.h ${CMAKE_CURRENT_BINARY_DIR}/marsxdr.c)

  list(APPEND _make_cleans rpcmars.h marsxdr.c )

endif()

add_custom_command(
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/langl.c
	  COMMAND ${_lex} -l ${CMAKE_CURRENT_SOURCE_DIR}/langl.l\; sed -e s/yy/yy_mars/g < lex.yy.c > ${CMAKE_CURRENT_BINARY_DIR}/langl.c
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/langl.l)

add_custom_command(
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/langy.c
	  COMMAND ${_yacc} -t ${CMAKE_CURRENT_SOURCE_DIR}/langy.y\; sed -e s/yy/yy_mars/g < y.tab.c > ${CMAKE_CURRENT_BINARY_DIR}/langy.c
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/langy.y)

list(APPEND _make_cleans langl.c langy.c )

add_custom_target(marslex DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/langy.c ${CMAKE_CURRENT_BINARY_DIR}/langl.c)
list( APPEND mars_client_deps marslex)

set_directory_properties( ADDITIONAL_MAKE_CLEAN_FILES  ${_make_cleans} )

set( grib_handling_pkg eccodes )

set(MARSC_HEADER_FILES_TO_INSTALL
        api.h
        base.h
        bufr.h
        control.h
        cos.h
        field.h
        globals.h
        grib.h
        hypercube.h
        index.h
        json.h
        lang.h
        mars.h
        nctarget.h
        proto.h
        hash.h
        tools.h
        variable.h
)

if(mars_client_HAVE_RPC)
  list(APPEND MARSC_HEADER_FILES_TO_INSTALL ${CMAKE_CURRENT_BINARY_DIR}/rpcmars.h)
endif()

ecbuild_add_library(
  TARGET      marsclient
  GENERATED   mars_client_version.c
  INSTALL_HEADERS_LIST ${MARSC_HEADER_FILES_TO_INSTALL}
  HEADER_DESTINATION ${INSTALL_INCLUDE_DIR}/marsclient

	SOURCES
      ${mars_client_src_files}
      mars_client_version.c
	DEPENDS
      ${mars_client_deps}

  PUBLIC_INCLUDES
       $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/src>
       $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src>
       ${RPC_INCLUDE_DIRS}

  PRIVATE_INCLUDES
        ${ECKIT_INCLUDE_DIRS}
        ${MARS_SERVER_INCLUDE_DIRS}
        ${FDB5_INCLUDE_DIRS}
        ${MIR_INCLUDE_DIRS}
        ${CURL_INCLUDE_DIRS}

  PUBLIC_LIBS
        eccodes
        ${RPC_LIBRARIES}

  PRIVATE_LIBS
        ${FDB5_LIBRARIES}
        ${MARS_FEATURE_LIBRARIES}
        ${CURL_LIBRARIES}
        ${CMATH_LIBRARIES}
        ${CMAKE_THREAD_LIBS_INIT}
)

set(_marsbin_noinstall NOINSTALL) # still needed for the tests
if(HAVE_BUILD_TOOLS)
  set(_marsbin_noinstall)
endif()
ecbuild_add_executable(
    TARGET    mars.bin
    INCLUDES  ${MIR_INCLUDE_DIRS} ${LIBEMOS_INCLUDE_DIRS}
    SOURCES   mars.c
    LIBS      marsclient ${MARS_FEATURE_LIBRARIES}
    ${_marsbin_noinstall}
)

ecbuild_add_executable(
    NOINSTALL
    TARGET    test-udp-stats
    SOURCES   test-udp-stats.c
    CONDITION HAVE_UDP_STATS
    LIBS      marsclient
)

if(HAVE_BUILD_TOOLS)
  add_subdirectory( tools )
endif()

/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"
#include "mars_client_config.h"
#include "mars_client_version.h"

int main(int argc, char* argv[]) {
    argv[0] = "mars";

    marsinit(&argc, argv, NULL, 0, NULL);

    mars.verbose = true;

    char sdate[24];
    char stime[24];
    char fname[256];
    char buf[1024000];
    time_t now = 0;
    char* p    = buf;
    size_t l   = sizeof(buf);

    request* statistics = empty_request("statistics");
    request* env        = get_environ();
    request* r          = empty_request("retrieve");

    marslog(LOG_INFO, "test_statistics()");
    /* Get time stamp */
    time(&now);
    strftime(sdate, sizeof(sdate), "%Y%m%d", gmtime(&now));
    strftime(stime, sizeof(stime), "%H:%M:%S", gmtime(&now));

    set_value(statistics, "stopdate", "%s", sdate);
    set_value(statistics, "stoptime", "%s", stime);

    add_value_int(r, "number", 3);
    add_value_int(r, "number", 1);
    add_value_int(r, "number", 2);

    print_all_requests(statistics);
    print_all_requests(env);
    print_all_requests(r);

    send_udp_statistics(statistics, env, r);

    free_all_requests(statistics);
    free_all_requests(env);
    free_all_requests(r);

    return 0;
}

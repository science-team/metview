/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "mars.h"

char* marstmp(void) {
    static char name[1024];
    char* p;

    p = tempnam(getenv("TMPDIR"), "mars");
    strcpy(name, p);
    free(p);

    close(creat(name, 0777));
    return name;
}

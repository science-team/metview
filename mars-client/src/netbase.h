/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#define NET_ERROR -1
#define NET_OK 0

#define NET_OPEN 1
#define NET_CLOSE 2
#define NET_READ 3
#define NET_WRITE 4
#define NET_CHECK 5
#define NET_QUERY 6
#define NET_WAIT 7
#define NET_RAW 8

#define NET_COMPRESS 8
#define NET_CALLBACK 16
#define NET_MISSING 32

#define NET_ABORT 255

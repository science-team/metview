/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

/*=======================================================================*/
/* Fortran types                                                         */

#if !defined(I32) && !defined(I64) && !defined(I128)
#define I32
#endif

#if !defined(R32) && !defined(R64) && !defined(R128)
#define R32
#endif

#define INT_MISSING_VALUE INT_MAX
#define FLOAT_MISSING_VALUE FLT_MAX

#ifdef I32
typedef int fortint; /* fortran integer */
#define D "ld"
#endif

#ifdef I64
typedef long long fortint;
#define D "lld"
#endif

#ifdef R32
typedef float fortfloat; /* fortran single precision float */
#define PFLOAT "f"
#endif

#ifdef R64
typedef double fortfloat;
#define FLOAT "lf"
#endif

/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"

int main(int argc, char* argv[]) {
    request *r, *p;
    json_value* j;

    marsinit(&argc, argv, NULL, 0, NULL);

    if (argc > 2) {
        printf("Usage: %s [file]\n", argv[0]);
        marsexit(1);
    }

    char* f = NULL;
    if (argc == 2)
        f = argv[1];

    mars.verbose = true;
    mars.echo    = false;
    r            = read_mars_request(f);

    if (r == NULL)
        marslog(LOG_EXIT, "No request found in input");

    p = r;
    while (p) {
        j = request2json(p);
        json_dump(j);
        json_free(j);
        p = p->next;
    }

    free_all_requests(r);

    marsexit(0);
}

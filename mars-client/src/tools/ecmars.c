/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"

extern base_class* msbase;

typedef struct data {
    int port;
} data;

option opts[] = {
    {"port", "MS_MARS_PORT", "-port", "9100", t_int, sizeof(int), OFFSET(data, port)},
};


int main(int argc, char* argv[]) {
    data setup;

    /* Assume the same setup as mars client */
    argv[0] = "msserver";


    marsinit(&argc, argv, &setup, NUMBER(opts), opts);
    mars.show_pid = getpid();
    marslog(LOG_INFO, "MARS Client code version in use: %ld", marsversion());
    server_run(setup.port, basetask, (void*)msbase);
    marsexit(1);
}

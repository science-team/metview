/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"

typedef struct data {
    char* file;
    int one;
    int archive;
} data;

static data setup;

static option opts[] = {
    {
        NULL,
        "BUFRTOREQUEST_FILE",
        "-f",
        NULL,
        t_str,
        sizeof(char*),
        OFFSET(data, file),
    },
    {
        NULL,
        "BUFRTOREQUEST_ONE",
        "-1",
        "0",
        t_boolean,
        sizeof(boolean),
        OFFSET(data, one),
    },
    {
        NULL,
        "BUFRTOREQUEST_ARCHIVE",
        "-1",
        "0",
        t_boolean,
        sizeof(boolean),
        OFFSET(data, archive),
    },
};

int main(int argc, char** argv) {
    FILE* fd     = 0;
    int e        = 0;
    int cnt      = 0;
    long buflen  = 1024 * 1024 * 20;
    long length  = buflen;
    char* buffer = reserve_mem(length);
    request* a   = empty_request("BUFR");
    request* g   = empty_request("BUFR");
    char buf[80];

    marsinit(&argc, argv, &setup, NUMBER(opts), opts);
    if (!setup.file) {
        printf("Usage: %s -1 -f <file>\n", argv[0]);
        printf("          -1 : print a single request\n");
        printf("          -f : file to process\n");
        marsexit(1);
    }


    start_timer();
    if ((fd = fopen(setup.file, "r")) == NULL) {
        marslog(LOG_EROR | LOG_PERR, "fopen: %s", setup.file);
        marsexit(1);
    }

    while ((e = _readany(fd, buffer, &length)) == NOERR) {
        cnt++;
        bufr_to_request(g, buffer, length);


        if(setup.archive) {
            unset_value(g, "IDENT");
            set_value(g, "TIME", "%d",0);
            set_value(g, "RANGE", "%d",1439);
        }

        reqmerge(a, g);

        if (!setup.one) {
            marslog(LOG_INFO, "BUFR number %d", cnt);
            print_all_requests(g);
        }
        /* free_all_requests(g); */
        length = buflen;
        /* break; */
    }

    switch (e) {
        case EOF:
            e = NOERR;
        case NOERR:
            if (setup.one)
                print_all_requests(a);
            marslog(LOG_INFO, "Decoded %d BUFR messages", cnt);
            break;
        case BUF_TO_SMALL:
            marslog(LOG_WARN, "Buffer is too small (%d bytes, %d needed.)", buflen, length);
            break;
        case NOT_FOUND_7777:
            marslog(LOG_EROR, "Group 7777 not found at the end of BUFR message");
            break;
        default:
            marslog(LOG_EROR, "readany: got unknown error %d", e);
            break;
    }

    if (buffer)
        release_mem(buffer);
    free_all_requests(a);
    free_all_requests(g);

    stop_timer(buf);
    timer_cpu();
    if (*buf)
        marslog(LOG_INFO, "Request time: %s", buf);

    print_memory_usage(NULL);
    marsexit(e);
}

/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"

int main(int argc, char* argv[]) {
    request *r, *p;
    err e = 0;

    argv[0] = "mars";
    marsinit(&argc, argv, NULL, 0, NULL);

    mars.verbose = true;

    mars.echo = false;
    p = r     = read_mars_request(getenv("MARS_REQUEST"));
    mars.echo = false;


    if (r == NULL)
        marslog(LOG_EXIT, "No request");

    while (r && e == NOERR) {
        request* s = r->next;
        r->next    = NULL;

        add_hidden_parameters(r);

        r->next = s;
        r       = s;
    }

    print_all_requests(p);

    free_all_requests(p);
    marsexit(e);
}

/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"

#define SIZE 2048000

char buf1[SIZE];
char buf2[SIZE];

int main(int argc, char** argv) {
    FILE* f1 = fopen(argv[1], "r");
    FILE* f2 = fopen(argv[2], "r");

    int e1;
    int e2;
    int rec = 0;

    long len1 = sizeof(buf1);
    long len2 = sizeof(buf2);


    for (;;) {
        e1 = _readany(f1, buf1, &len1);
        e2 = _readany(f2, buf2, &len2);

        if (e1 || e2)
            break;

        rec++;

        if (len1 != len2)
            marslog(LOG_EXIT,
                    "Files are different rec:%d (len1 = %d/len2 = %d)",
                    rec, len1, len2);

        if (memcmp(buf1, buf2, len1) != 0) {
            int i;
            marslog(LOG_INFO, "Files are different rec:%d", rec);
            for (i = 0; i < len1; i++)
                if (buf1[i] != buf2[i])
                    marslog(LOG_EXIT, "Byte is %d %x %x", i, buf1[i], buf2[i]);
        }

        len1 = sizeof(buf1);
        len2 = sizeof(buf2);
    }

    if (e1 == e2 && e1 == -1)
        e1 = e2 = 0;

    if (e1 == -3)
        marslog(LOG_EXIT, "Buffer too small, should be %d", len1);
    if (e2 == -3)
        marslog(LOG_EXIT, "Buffer too small, should be %d", len1);

    if (e1 || e2)
        marslog(LOG_EXIT, "Got some error  %d %d", e1, e2);

    marslog(LOG_INFO, "Got %d similar rec.", rec);
    exit(0);
}

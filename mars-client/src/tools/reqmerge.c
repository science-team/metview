/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"


int main(int argc, char** argv) {
    int i;

    request* s = 0;

    for (i = 1; i < argc; i++) {
        request* r = read_request_file(argv[i]);

        if (r == 0)
            marslog(LOG_EXIT | LOG_PERR, "%s", argv[i]);

        if (s == NULL)
            s = r;
        else {

            request* a = s;
            request* b = r;


            while (a && b) {
                reqmerge(a, b);
                a = a->next;
                b = b->next;
            }

            if (a || b)
                marslog(LOG_EXIT, "Requests of different length");
        }
    }

    print_all_requests(s);

    marsexit(0);
}

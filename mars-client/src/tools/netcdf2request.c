/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"

typedef struct data {
    char* file;
    int one;
    int expect;
} data;

static data setup;

static option opts[] = {
    {
        NULL,
        "NETCDFTOREQUEST_FILE",
        "-f",
        NULL,
        t_str,
        sizeof(char*),
        OFFSET(data, file),
    },
    {
        NULL,
        "NETCDFTOREQUEST_ONE",
        "-1",
        "0",
        t_boolean,
        sizeof(boolean),
        OFFSET(data, one),
    },
    {
        NULL,
        "NETCDFTOREQUEST_EXPECT",
        "-e",
        "0",
        t_boolean,
        sizeof(boolean),
        OFFSET(data, expect),
    },
};

int main(int argc, char** argv) {
    int e = 0;

    marsinit(&argc, argv, &setup, NUMBER(opts), opts);
    if (!setup.file) {
        printf("Usage: %s -1 -f <file>\n", argv[0]);
        printf("          -1 : print a single request\n");
        printf("          -f : file to process\n");
        marsexit(1);
    }

    request* g = netcdf_to_request(setup.file, setup.one, NULL, &e);

    if (e) {
        marslog(LOG_EROR | LOG_PERR, "ncdf_to_request: %s error %d", setup.file, e);
        marsexit(1);
    }

    print_all_requests(g);

    marsexit(0);
}

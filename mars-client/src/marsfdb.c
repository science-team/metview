/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "base.h"
#include "mars.h"

extern void basetask();
extern base_class* nfdbbase;

typedef struct data {
    int port;
    boolean feed;
} data;

option opts[] = {
    {"port", "MARS_FDB_PORT", "-port", "7000",
     t_int, sizeof(int), OFFSET(data, port)},
};


int main(int argc, char* argv[]) {
    data setup;

    marsinit(&argc, argv, &setup, NUMBER(opts), opts);
    server_run(setup.port, (taskproc)basetask, (void*)nfdbbase);
    marsexit(1);
}

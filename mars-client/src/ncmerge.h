#ifndef NCMERGE_H
#define NCMERGE_H

#include "ncschema.h"
#include "nctypes.h"


err netcdf_merge(const char* target_path, netcdf_field_list* result, netcdf_field_list* fields);


#endif

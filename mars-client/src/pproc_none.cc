/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"

#include "pproc.h"

namespace marsclient {

class PProcNone : public PProc {
public:  // methods
    PProcNone(const char* name) :
        name_(name) {}

    virtual ~PProcNone() {}

    virtual void print_version() {
        marslog(LOG_INFO, "PProc NONE version: 0.0.0");
    }

    const std::string& name() const { return name_; }

    virtual err initialise(int argc, char** argv) {
        marslog(LOG_DBUG, "Post processing backend is %s", name().c_str());
        return 0;
    }

    virtual err ppinit(const request* r, postproc* proc) { return -1; }
    virtual err ppdone(void) { return -1; }
    virtual err ppcount(int* in, int* out) { return -1; }
    virtual err pparea(request* r) { return -1; }
    virtual fieldset* pp_fieldset(const char* file, request* filter) { return NULL; }
    virtual err ppstyle(const request* r) { return -1; }
    virtual err pprotation(const request* r) { return -1; }
    virtual long ppestimate() { return -1; }
    virtual err makeuv(char* vo, char* d, long inlen, char* u, char* v, long* outlen) { return -1; }

    std::string name_;
};

static PProcBuilderT<PProcNone> noneBuilder("None");

}  // namespace marsclient

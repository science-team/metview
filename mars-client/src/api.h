/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */
#ifndef api_H
#define api_H
#include <stdlib.h>
#include "json.h"

typedef struct _ecmwf_api ecmwf_api;

typedef void (*typeproc)(const char* type, void* data);
typedef void (*messageproc)(const char* msg, void* data);

ecmwf_api* ecmwf_api_create(const char* url, const char* key, const char* email);

long long ecmwf_api_transfer(ecmwf_api* api, const char* url, const char* target);
const json_value* ecmwf_api_call(ecmwf_api* api, const char* method, const char* url, const char* json);
const char* ecmwf_api_full(ecmwf_api* api, const char* url);

void ecmwf_api_set_msg_callback(ecmwf_api* api, messageproc msgcb, void* data);

void ecmwf_api_destroy(ecmwf_api* api);
int ecmwf_api_in_progress(ecmwf_api* api);
int ecmwf_api_transfer_ready(ecmwf_api* api);
void ecmwf_api_in_wait(ecmwf_api* api);
const char* ecmwf_api_location(ecmwf_api* api);
const char* ecmwf_api_content_type(ecmwf_api* api);

long long ecmwf_api_transfer_start(ecmwf_api* api, const char* url, typeproc typecb, void* typecb_data);
int ecmwf_api_transfer_end(ecmwf_api* api);
size_t ecmwf_api_transfer_read(ecmwf_api* api, void*, size_t);
const char* ecmwf_api_must_retry(ecmwf_api* api);

void ecmwf_api_verbose(ecmwf_api* api, int on);

#endif

/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#include "mars.h"

#define H(x) (1 << (x))

typedef struct station {
    int ident;
    int time_restricted;
} station;

static station restrictions[] = {
#include "restricted.h"
};


static int compare(const void* x1, const void* x2) {
    return ((station*)x1)->ident - ((station*)x2)->ident;
}

int restricted(void* p) {
    packed_key* k = (packed_key*)p;
    station st;
    station* s;
    int i;

    if (KEY_SUBTYPE(k) > 4)
        return 0;

    st.ident = 0;

    for (i = 0; i < 5; i++)
        st.ident = st.ident * 10 + (KEY_IDENT(k)[i] - '0');

    if ((s = bsearch(&st, restrictions, NUMBER(restrictions), sizeof(station), compare))) {
        if ((s->time_restricted & (1 << KEY_HOUR(k))) != 0) {
            /* marslog(LOG_INFO,"RESTRICTED ident: %d, rest time: %04x, obs time: %04x, real obs key time: %d",st.ident,s->time_restricted,1<<KEY_HOUR(k),KEY_HOUR(k)); */
            return 1;
        }
        /* marslog(LOG_INFO,"INCLUDED ident: %d, rest time: %04x, obs time: %04x, real obs key time: %d",st.ident,s->time_restricted,1<<KEY_HOUR(k),KEY_HOUR(k)); */
    }
    return 0;
}

/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

typedef struct mars_grib_map {
    struct mars_grib_map* next;

    char* name;

    boolean s_ok;
    char* s_value;

    boolean d_ok;
    double d_value;

    boolean l_ok;
    long l_value;

} mars_grib_map;

typedef struct mars_field_index {

    struct mars_field_index* next;

    off_t offset;
    long length;

    mars_grib_map* head;
    mars_grib_map* tail;


} mars_field_index;

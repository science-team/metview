/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#ifndef mars_pproc_h
#define mars_pproc_h

#include "mars.h"

#include <map>
#include <string>

namespace marsclient {

//----------------------------------------------------------------------------------------------------------------------

class PProc {
public:  // methods
    PProc();

    virtual ~PProc();

    virtual const std::string& name() const = 0;

    virtual void print_version() = 0;

    virtual err initialise(int argc, char** argv) = 0;

    virtual err ppinit(const request* r, postproc* proc)                              = 0;
    virtual err ppdone(void)                                                          = 0;
    virtual err ppcount(int* in, int* out)                                            = 0;
    virtual err pparea(request* r)                                                    = 0;
    virtual fieldset* pp_fieldset(const char* file, request* filter)                  = 0;
    virtual err ppstyle(const request* r)                                             = 0;
    virtual err pprotation(const request* r)                                          = 0;
    virtual long ppestimate()                                                         = 0;
    virtual err makeuv(char* vo, char* d, long inlen, char* u, char* v, long* outlen) = 0;
};

//----------------------------------------------------------------------------------------------------------------------

struct PProcBuilder {
    virtual PProc* build() = 0;
};

class PProcFactory {
public:
    static PProcFactory& instance();

    PProc* build(const std::string& name) const;

    void insert(const std::string& name, PProcBuilder* b);

private:
    PProcFactory();
    std::map<std::string, PProcBuilder*> m_;
};

//----------------------------------------------------------------------------------------------------------------------

template <typename T>
struct PProcBuilderT : public PProcBuilder {

    const char* name_;

    PProcBuilderT(const char* name) :
        name_(name) {
        PProcFactory::instance().insert(name, this);
    }

    PProc* build() { return new T(name_); }
};


//----------------------------------------------------------------------------------------------------------------------

}  // namespace marsclient

#endif

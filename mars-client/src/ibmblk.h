/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */
#include "mars.h"
#define CHR_TYPE 1
#define NUM_TYPE 2
#define STR_TYPE 3

#define CHECK_ALIGN

#define MAX_NET_BLOCK 32768

typedef struct {

    char hdrlen[2];

    char userid[4];
    char compid[4];
    char sendid[7];
    char recid[7];

    char subdate[6];
    char subtime[6];

    char recdate[6];
    char rectime[6];

    char nihdr[2];
    char noshdr[2];

    char suberr[2];
    char status[1];

    char actcls[1];
    char msglen[5];

    char actmod[1];
    char account[8];
    char userjob[7];
    char pswitch[1];
    char hdrvers[2];

#ifdef CHECK_ALIGN
    char __dummy[2];
#endif

} _prmhdr;


typedef struct {

    char shdrlen[2];
    char action[8];
    char mode[1];
    char blklen[5];
    char reser1[4];
    char ccode[4];
    char escode[4];
    char reser2[2];
    char nparms[3];

#ifdef CHECK_ALIGN
    char __dummy[2];
#endif

} _subhdr;

typedef struct {
    char nvalues[2];
    char plen[2];
    char param[256];
} _bparam;

typedef struct {
    char ptype[1];
    char vlen[2];
    char value[256];
} _bvalue;

#ifdef CHECK_ALIGN
#define PRM_SIZE OFFSET(_prmhdr, __dummy)
#define SUB_SIZE OFFSET(_subhdr, __dummy)
#else
#define PRM_SIZE sizeof(_prmhdr)
#define SUB_SIZE sizeof(_subhdr)
#endif

request* procces_reply(char* block);
char* buildblock(request* cmds, int);

/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#ifdef METVIEW_MOTIF
#include <X11/Intrinsic.h>
#include "mars.h"

static void svcinput(void* data, int* soc, XtInputId* id) {
    svc* s = (svc*)data;
    process_service(s);
}

static Boolean install_input(void* data) {
    svc* s = (svc*)data;

    svc_connect(s);

    XtAppAddInput(
        (XtAppContext)s->context,
        s->soc,
        (XtPointer)XtInputReadMask,
        (XtInputCallbackProc)svcinput,
        (XtPointer)s);
    return True;
}


void ListenToService(XtAppContext app_context, svc* s) {
    s->context = (long)app_context;
    XtAppAddWorkProc(app_context, (XtWorkProc)install_input,
                     (XtPointer)s);
}

svc* RegisterService(XtAppContext app_context, const char* name) {
    svc* s = create_service(name);

    if (s) {
        svc_connect(s);
        s->context = (long)app_context;
        XtAppAddWorkProc(app_context,
                         (XtWorkProc)install_input,
                         (XtPointer)s);
    }
    return s;
}

#endif

/*
 * © Copyright 1996-2012 ECMWF.
 *
 * This software is licensed under the terms of the Apache Licence Version 2.0
 * which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
 * In applying this licence, ECMWF does not waive the privileges and immunities
 * granted to it by virtue of its status as an intergovernmental organisation nor
 * does it submit to any jurisdiction.
 */

#ifndef _H_LANG_
#define _H_LANG_

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif
// typedef struct request request;
// typedef struct parameter parameter;

// request* expand(request*);


// request* new_request(char*, parameter*);
// parameter* new_parameter(char*, value*);
// value* new_value(char*);
#if defined(__cplusplus) || defined(c_plusplus)
}
#endif


typedef enum
{
    t_val,
    t_or,
    t_and,
    t_not,
    t_eq,
    t_lt,
    t_gt,
    t_ne,
    t_ge,
    t_le,
    t_in,
    t_func
} testop;

typedef struct condition {
    testop op;
    struct condition* left;
    struct condition* right;
} condition;

typedef enum
{
    a_set,
    a_unset,
    a_warning,
    a_error,
    a_exit,
    a_fail,
    a_info,
    a_mail,
    a_mailuser
} actop;

typedef struct action {
    struct action* next;
    actop op;
    void* param;
} action;


typedef struct rule {
    struct rule* next;
    condition* test;
    action* doit;
} rule;

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

// rule* new_rule(condition*, action*);
// condition* new_condition(testop, condition*, condition*);
// action* new_action(actop, void*);

// rule* read_chk(char*);
// boolean chkrequest(rule*, request*);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif

#!/bin/bash
set -xu

for pid in $(ps -fu $USER | grep /vscode-server/ | grep -v grep | awk '{print $2;}')
do
    tmp=$(tr '\0' '\n' < /proc/$pid/environ | grep -E ^TMPDIR | sed 's/.*=//' | sed 's/\n.*//g')
    # echo $tmp | hexdump -C
    mkdir -p $tmp
done

set( MARS_HOME $ENV{INSTALL_PREFIX}/mars-home CACHE STRING "Where the files go" )

set( INSTALL_MARS_SCRIPT ON CACHE BOOL "Install main script" )
set( ENABLE_ECCODES      ON CACHE BOOL "Use ecCodes" FORCE )

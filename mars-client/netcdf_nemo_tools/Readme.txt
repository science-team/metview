This software consists of a number of files written to process NEMO netcdf output and make it ready 
for archival in MARS.


The files are: 

a) Split the input NEMO file into 1 file per variable:

1. convert_nemo_to_1_variable_all_levels_and_timesteps.py 
2. convert_nemo_to_1_variable_all_levels_and_timesteps.cfg  

b) Split the files produced in a) to 1 file per level per timestep.

3. split_nemo_1_field_per_level_per_timestep_nco.py
4. split_nemo_1_field_per_level_per_timestep_nco.cfg

c) Finally check that the output data is CF compliant:

5. cf_checks.py  
6. cf_checks.cfg            
7. cf-standard-name-table.xml 
8. area-type-table.xml


In detail: 

To run: convert_nemo_to_1_variable_all_levels_and_timesteps.py -c <config file>
********************************************************************************
Program to read NEMO ocean model output temperature field file (netCDF3),
Requirements:
 1)convert it to netcdf4 and write one output variable per file.
 2)Output files should also pass CF checker and CMIP conventions (and possibly OGC).
 3) Mars attributes should be clearly identified 
 4) time should be handled (forecast time, assimilation window etc)
 5) Use an approach whereby various attributes/additional variables can be added to output NetCDF files by calls to functions

To run: split_nemo_1_field_per_level_per_timestep_nco.py -c <config file>
********************************************************************************
Program to read NEMO output temperature field file (netCDF3),
Initial Requirements:
 1) convert it to netcdf4 and write one output variable per file.
 2) Output files should also pass CF checker + CMIP conventions (possibly OGC).
 3) Mars attributes should be clearly identified 
 4) time should be handled (forecast time, assimilation window etc)

Modified program to alter the split level/time fields created from cdo on the output of convert_nemo_to_1_variable_all_levels_and_timesteps.py.
these files are 1 field/level/timestep per netcdf4 file. Uses CDO because it already has the timestep/level splitting capability built in.
 Dependencies:
  netCDF4 python module
  nco tools (ncks,ncatted),cdo

To run: python cf_checks.py -c cf_checks.cfg
********************************************************************************
(Expands to: cf_checks  -a area-type-table.xml  -s cf-standard-name-table.xml -v 1.6 )

Python script to run the cf-checker from the command line
 Only requires:
  1  location of area-type-table.xml
  2  location of standard-name-table.xml
  3  python netCDF4 module to be installed
  4  LD_LIBRARY_PATH to be set in local ENV *before* running script, i.e.:
 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"/usr/local/apps/udunits/2.1.21/LP64/lib"
  5 path of data directory 
  6 glob pattern to match

 Input : Configuration file  cf_checks.cfg (in same directory as script)
 Output: cf check output/err results to stdout
 Dependencies:
  cfchecks (cf checker) is installed on the system

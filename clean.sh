#!/bin/ksh

# **************************** LICENSE START ***********************************
#
# Copyright 2018 ECMWF and INPE. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

if [ -e configure.sh ]
then
    rm -rf atlas eckit mars-client metview mir metview-ecmwf-configs fdb
else
    echo "Should only run this from its own diredctory"
    exit 1
fi

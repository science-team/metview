
# the root name of the qrc resources
set (QRC_NAMES
    debug
    desktop
    edit
    editor
    examiner
    find
    geoSelect
    keyDialog
    MacroEdit
    mail
    MvQOgcClient
    scmEditor
    uPlot
    window
    XServ
    )

if (ENABLE_CODES_UI)
    list(APPEND QRC_NAMES codes_ui)
endif()

# the styles. There is a directory for each style containing all the images belonging to it.
set(STYLE_NAMES
    light
    dark
)

ecbuild_info("Generate QRC/RCC files")
ecbuild_debug("CMAKE_CURRENT_BINARY_DIR=${CMAKE_CURRENT_BINARY_DIR}")
ecbuild_debug("CMAKE_CURRENT_SOURCE_DIR=${CMAKE_CURRENT_SOURCE_DIR}")
ecbuild_debug("CMAKE_SOURCE_DIR=${CMAKE_SOURCE_DIR}")
ecbuild_debug("CMAKE_BINARY_DIR=${CMAKE_BINARY_DIR}")

# the rcc files will be generated in this dir
make_directory(${CMAKE_BINARY_DIR}/share/metview/app-defaults/images)

# copy images to build dir
foreach( sn ${style_NAMES} )
    file(COPY ${sn} DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/)
endforeach()

# generate rcc binary files form image (svg+png) resources to be loaded at
# runtime by the Qt-based applications. There will one set of rcc files for each style.
set(RCC_FILES "")

# iterate through the styles
foreach( STYLE ${STYLE_NAMES} )

    # generate a qrc + rcc file
    foreach( _N ${QRC_NAMES} )

        # create a qrc file from the template. A qrc file contains paths to
        # a set of images
        set(_QRC_TEMPLATE "${CMAKE_CURRENT_SOURCE_DIR}/${_N}.qrc.in")
        set(_QRC_NAME ${_N}_${STYLE})
        set(_QRC_FILENAME ${_QRC_NAME}.qrc)
        set(_QRC_FILE "${CMAKE_CURRENT_BINARY_DIR}/${_QRC_NAME}.qrc")
        set(_CURRENT_STYLE "${CMAKE_CURRENT_SOURCE_DIR}/${STYLE}")
        configure_file("${_QRC_TEMPLATE}" "${_QRC_FILE}"  @ONLY)

        # generate the rcc file from the qrc file. The Qt command will automatically create an
        # rcc target (first arg) depending of the qrc file and the images it points to
        set(_RCC_FILENAME "${_QRC_NAME}.rcc")
        set(_RCC_FILE "${CMAKE_BINARY_DIR}/share/metview/app-defaults/images/${_RCC_FILENAME}")
        ecbuild_debug("QRC_NAME=${_QRC_NAME} QRC_FILENAME=${_QRC_FILENAME} RCC_FILENAME=${_RCC_FILENAME}")
        if(METVIEW_QT6)
            QT6_ADD_BINARY_RESOURCES(${_QRC_NAME} "${_QRC_FILE}" DESTINATION "${_RCC_FILE}")
        elseif(METVIEW_QT5)
            QT5_ADD_BINARY_RESOURCES(${_QRC_NAME} "${_QRC_FILE}" DESTINATION "${_RCC_FILE}")
        endif()

        # for the light style we create a qrc file in the source dir so that qt
        # designes show the icons. These qrc files have to be added to gitignore
        # and should not be part of the repo
        if(ENABLE_QRC_LOCAL_DEV AND STYLE STREQUAL "light")
            set(_QRC_FILE_SRC "${CMAKE_CURRENT_SOURCE_DIR}/${_QRC_NAME}.qrc")
            configure_file("${_QRC_TEMPLATE}" "${_QRC_FILE_SRC}" @ONLY)
        endif()

        list(APPEND RCC_FILES ${_RCC_FILE})
    endforeach()
endforeach()


# ensure rcc files are installed at install time
foreach( _F ${RCC_FILES})
#    get_filename_component(fname ${f} NAME)
    install( FILES "${_F}"
             DESTINATION share/metview/app-defaults/images
             PERMISSIONS OWNER_WRITE OWNER_READ GROUP_READ WORLD_READ)

endforeach()

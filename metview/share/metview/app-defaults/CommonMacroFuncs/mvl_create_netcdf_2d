# Metview Macro

# **************************** LICENSE START ***********************************
#
# Copyright 2012 ECMWF. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

# **************************************************************************
# Function      : mvl_create_netcdf_2d
#
# Syntax        : netcdf mvl_create_netcdf_2d (dimension_name_1:string, dimension_vals_1:list,
#                                              dimension_name_2:string, dimension_vals_2:list,
#                                              var_name:string, var_values:list)
#
# Author (date) : Fernando Ii / Iain Russell (02/04/2007)
#
# Category      : NETCDF
#
# OneLineDesc   : Creates a 2-dimensional NetCDF file from lists of data
#
# Description   : Creates a 2-dimensional NetCDF file from lists of data
#
# Parameters    : dimension_name_1:string - the name to assign to the first dimension 
#                 dimension_vals_1:list   - the first dimension values
#                 dimension_name_2:string - the name to assign to the second dimension 
#                 dimension_vals_2:list   - the second dimension values
#                 var_name:string         - the name to assign to the variable
#                 dimension_vals:list     - the variable values
#
#
# Return Value  : A newly-created netcdf variable
#
# Dependencies  : None
#
# Example Usage : 
#                 levels     = [1000, 925, 850]
#                 geocoords  = [0, 5, 10, 15]
#                 values     = [5, 3, 8, 6, 1, 4, 2, 3, 6, 7, 8, 6]
#
#                 net = mvl_create_netcdf_2d ('Levels', levels, 'Latitude', geocoords, 'AverageXSection', values)
#
#                 write (getenv ('SCRATCH') & '/result.nc', net)
#
# **************************************************************************

function mvl_create_netcdf_2d (dimension_name_1:string, dimension_vals_1:list,
                               dimension_name_2:string, dimension_vals_2:list,
                               var_name:string, var_values:list)

	# Open temporary file
	fn = tmpfile()
	fh = file(fn)

	# Append global values
	write(fh,"netcdf test {",newline)

	# Append dimensions
	write(fh,"dimensions:",newline)
	write(fh,tab,dimension_name_1," = ",count(dimension_vals_1)," ;",newline)
	write(fh,tab,dimension_name_2," = ",count(dimension_vals_2)," ;",newline)

	# Append variables
	write(fh,"variables:",newline)
	write(fh,tab,"double ",dimension_name_1,"(",dimension_name_1,") ;",newline)
	write(fh,tab,"double ",dimension_name_2,"(",dimension_name_2,") ;",newline)
	write(fh,tab,"double ",var_name,"(",dimension_name_1,",",dimension_name_2,") ;",newline)

	# Apend data
	# First dimension
	write(fh,"data:",newline)
	write(fh,tab,dimension_name_1," = ")
	for i = 1 to count(dimension_vals_1)-1 do
		write(fh,dimension_vals_1[i],", ")
	end for
	write(fh,dimension_vals_1[count(dimension_vals_1)], " ;",newline)

	# Second dimension
	write(fh,tab,dimension_name_2," = ")
	for i = 1 to count(dimension_vals_2)-1 do
		write(fh,dimension_vals_2[i],", ")
	end for
	write(fh,dimension_vals_2[count(dimension_vals_2)], " ;",newline)

	# Matrix
	write(fh,tab,var_name," = ")
	ind = 1
	for i = 1 to count(dimension_vals_1)-1 do
		for j = 1 to count(dimension_vals_2) do
			write(fh,var_values[ind],", ")
			ind = ind + 1
		end for
		write(fh,newline)
	end for
	for j = 1 to count(dimension_vals_2)-1 do  #last line
		write(fh,var_values[ind],", ")
		ind = ind + 1
	end for
	write(fh,var_values[ind], " ;",newline)

	# Close file
	write(fh,"}",newline)
	fh = 0

	# Convert text file to a netcdf file
	fnet = tmpfile()
	shell("ncgen -o " & fnet &" " & fn)
	net = read(fnet)

	return net

end mvl_create_netcdf_2d

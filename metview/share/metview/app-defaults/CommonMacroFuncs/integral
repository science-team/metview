#Metview Macro

#  **************************** LICENSE START ***********************************
# 
#  Copyright 2019 ECMWF. This software is distributed under the terms
#  of the Apache License version 2.0. In applying this license, ECMWF does not
#  waive the privileges and immunities granted to it by virtue of its status as
#  an Intergovernmental Organization or submit itself to any jurisdiction.
# 
#  ***************************** LICENSE END ************************************
# 

# **************************************************************************
# Function      : integral
#
# Syntax        : number/list integral(fs:fieldset)
#                                          
# Category      : INTEGRAL
#
# OneLineDesc   : Computes surface integral for fields
#
# Description   : Computes surface integral for fields
#
# Parameters    : fieldset
#                 
# Return Value  : number or list of numbers
#
# Dependencies  : none
#
# Example Usage : 
#                 
#
# **************************************************************************

function integral(fs:fieldset)

    if count(fs) = 0 then
        fail('integrate: invalid input fieldset')
    end if

    res = accumulate(fs*grid_cell_area(fs)) 
    
    return res
    
end integral   

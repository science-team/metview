#Metview Macro

#  **************************** LICENSE START ***********************************
# 
#  Copyright 2020 ECMWF. This software is distributed under the terms
#  of the Apache License version 2.0. In applying this license, ECMWF does not
#  waive the privileges and immunities granted to it by virtue of its status as
#  an Intergovernmental Organization or submit itself to any jurisdiction.
# 
#  ***************************** LICENSE END ************************************
# 

#=============================================================================
# Function      : xs_build_orog
#
# Syntax        : 
#                                          
# Category      : 
#
# OneLineDesc   : returns an orography area curve for the given cross section data
#
# Description   : Returns an orography area curve for the given cross section data
#
# Parameters    : 
# 
# Return Value  : 
#
# Dependencies  : none
#
#==============================================================================

function xs_build_orog(xs_d, orog_fs, bottom_level, colour_area)

    __DEBUG_BUILD_OROG=0

    # read cross section line points from the
    # cross section dataobject
    setcurrent(xs_d, "lat")
    lats = values(xs_d)
    setcurrent(xs_d, "lon")
    lons = values(xs_d)

    if __DEBUG_BUILD_OROG then
        # print("lats=", lats)
        # print("lons=", lons)
    end if

    # figure out the horizontal interpolation method used
    # to generate the cross section data
    attr = global_attributes(xs_d)
    hm = "interpolate"
    if attr.xsHorizontalMethod <> nil then
        hm = attr.xsHorizontalMethod
    end if
    
    if __DEBUG_BUILD_OROG then
        print("hm=", hm)
    end if

    # sample surface height field along the line with the right method
    if hm = "nearest" then
        vals = nearest_gridpoint(orog_fs, lats, lons)
    else
        vals = vector(count(lats))
        for i=1 to count(lats) do
            v = interpolate(orog_fs, lats[i], lons[i])
            if v =  nil then
                v = vector_missing_value
            end if
            vals[i] = v
       end for        
    end if
    
    # the x variable selection has to match the xs view algorithm, 
    # otherwise the area is not plotted! 
    # See CrossS.cc CreateOuputRequest()    
    d_lons = abs(lons[1] - lons[count(lons)])
    eps = 1E-6
    if d_lons < eps then
        # NETCDF_X_GEOLINE_CONVENTION=lonlat
        xp_in = lats
    else
        # NETCDF_X_GEOLINE_CONVENTION=latlon
        xp_in = lons
    end if
    
    # construct the orography polygon
    xp = vector(count(xp_in) + 2)
    xp[1] = xp_in[1]
    xp[count(xp)] = xp_in[count(xp_in)]
    for i=2 to count(xp)-1 do
        xp[i] = xp_in[i-1]
    end for

    yp = vector(count(xp_in) + 2)
    yp[1] = bottom_level
    yp[count(yp)] = bottom_level
    for i=2 to count(yp)-1 do
        yp[i] = vals[i-1]
    end for

    if __DEBUG_BUILD_OROG then
        print(" xp=", xp)
        print(" yp=", yp)
    end if

    return xy_area(xp, yp, colour_area)
    
end xs_build_orog
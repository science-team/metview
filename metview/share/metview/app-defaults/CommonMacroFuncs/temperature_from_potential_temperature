#Metview Macro

#  **************************** LICENSE START ***********************************
# 
#  Copyright 2019 ECMWF. This software is distributed under the terms
#  of the Apache License version 2.0. In applying this license, ECMWF does not
#  waive the privileges and immunities granted to it by virtue of its status as
#  an Intergovernmental Organization or submit itself to any jurisdiction.
# 
#  ***************************** LICENSE END ************************************
# 

#=============================================================================
# Function      : temperature_from_potential_temperature
#
# Syntax        : number t_from_potential_temperature(pt: number, p: number)
#                                          
# Category      : THERMODYNAMICS
#
# OneLineDesc   : Computes the temperature from potential tempearture and pressure
#
# Description   : Computes the temperature from a given potential tempearture and pressure
#
# Parameters    : pt - the potential temperature (K)
#		 		  p - the pressure (Pa)
#           
# Return Value  : the temperature (K)
#
# Dependencies  : none
#
#==============================================================================

function temperature_from_potential_temperature(th: number, p: number)

    KAPPA=0.285611 #Rd/cp
    return th *(p/100000.)^KAPPA

end temperature_from_potential_temperature	

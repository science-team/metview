#Metview Macro

#  **************************** LICENSE START ***********************************
# 
#  Copyright 2021 ECMWF. This software is distributed under the terms
#  of the Apache License version 2.0. In applying this license, ECMWF does not
#  waive the privileges and immunities granted to it by virtue of its status as
#  an Intergovernmental Organization or submit itself to any jurisdiction.
# 
#  ***************************** LICENSE END ************************************
# 

# **************************************************************************
# Function      : second_derivative_y
#
# Syntax        : fieldset second_derivative_y(fs:fieldset, args)
#                                          
# Category      : DERIVATIVES
#
# OneLineDesc   : Computes the second South-North derivative of a fieldset
#
# Description   :
#
# Parameters    : 
#                 
# Return Value  : 
#
# Dependencies  : none
#
# Example Usage : 
#                 
#
# **************************************************************************

function second_derivative_y
    _fn_name = "second_derivative_y"
    
    _args = arguments()
    _v = __prepare_gradient_arg(_fn_name, 1, _args)
    if count(_v) <> 4 then
        fail(_fn_name & ": invalid arguments=" & _args)
    end if
    _fs = _v[1]
    _mode = _v[2]
    _pole_missing = _v[3]
    _vector_mode = _v[4]
    
    # finite difference
    if _mode = "fdiff" then
         return _cpp_second_derivative_y(_fs)
    
    # finite element
    else if _mode = "felem" then
        _nabla_mode = "scalar_gradient"
        _r = regrid(data: _fs, nabla: _nabla_mode, nabla_poles_missing_values: _pole_missing)
        _r = _r[2, count(_r), 2] + 0.
        _r = regrid(data: _r, nabla: _nabla_mode, nabla_poles_missing_values: _pole_missing)
        return _r[2, count(_r), 2]
    end if
    
    fail(_fn_name & ": invalid mode=" & _mode)
end second_derivative_x

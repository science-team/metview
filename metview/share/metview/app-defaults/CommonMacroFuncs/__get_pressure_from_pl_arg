#Metview Macro

#  **************************** LICENSE START ***********************************
# 
#  Copyright 2020 ECMWF. This software is distributed under the terms
#  of the Apache License version 2.0. In applying this license, ECMWF does not
#  waive the privileges and immunities granted to it by virtue of its status as
#  an Intergovernmental Organization or submit itself to any jurisdiction.
# 
#  ***************************** LICENSE END ************************************
# 

function __get_pressure_from_pl_arg(fs, fs_name, fn_name)
    
    if type(fs) <> "fieldset" then
        fail(fn_name & ": " & fs_name & " argument must be a fieldset!")
    end if   
    
    if count(fs) = 0 then 
        fail(fn_name & ": " & fs_name & " argument is an empty fieldset!")
    end if
    
    pres = vector(count(fs))
    keys = grib_get(fs, ["typeOfLevel","level:l"])
    for i=1 to count(keys) do
        if keys[i][1] = "isobaricInhPa" then
            pres[i] = keys[i][2]*100
        else if keys[i][1] = "isobaricInPa" then
            pres[i] = keys[i][2]
        else
            fail(fn_name & ": non-pressure level field! [" &
                 fs_name & "-field=]" & i)
        end if
    end for
    
    return pres
       
end __get_pressure_from_pl_arg
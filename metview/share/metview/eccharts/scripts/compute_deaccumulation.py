def compute_deaccumulation(params, data, target_shortname):

    if len(params) == 0 or len(data) == 0 :
        return None

    param = params[0]
    fs = mv.read(data = data, param = param)

    if len(fs) == 0 :
        return None

    # set values in first timestep to 0 and add to result
    res = fs[0] * 0

    # deaccumulate fieldset
    num = len(fs)
    res.append(fs[1:num-1] - fs[0:num-2])

    # set correct shortname for result
    res = mv.grib_set_string(res, ["shortName", target_shortname])

	# mark as non-derived field - so that the correct scaling could be applied in contouring
    res = mv.grib_set_long(res, ["generatingProcessIdentifier", 149])
    
    return res

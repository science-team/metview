def compute_speed(params, data, target_shortname):
	
	uParam = params[0]
	vParam = params[1]
	
	u = mv.read(data=data, param=uParam)
	v = mv.read(data=data, param=vParam)
	res = mv.sqrt(u*u+v*v)
	
	return res.grib_set_string(["shortName", target_shortname])
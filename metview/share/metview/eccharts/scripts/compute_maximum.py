def compute_maximum(params, data, target_shortname, interval_steps, interval):

    if len(params) == 0 or len(data) == 0 :
        return None

    param = params[0]
    fs = mv.read(data = data, param = param)

    if len(fs) == 0 :
        return None
   
    # get the steps from the input fieldse
    steps = mv.grib_get_long(fs, "step")
    if not isinstance(steps, list):
        steps = [steps]
        
    # initialise result
    res = mv.Fieldset()

    # loop for the required interval steps
    for ts in interval_steps:

        # define start and end step for interval
        step_start = ts - interval + 1
        step_end = ts

        # collect fields in the given interval. We go in reverse order
		# so that the first field collected should be valid at the end
		# of the interval. 
        f = mv.Fieldset()
        for i in range(len(fs)-1,-1,-1):
            if steps[i] in range(step_start, step_end+1):
                f.append(fs[i])

        # compute the maximum for the given interval. The metadata (step etc.)
        # will be copied from the first field of fieldset f!
        if f:
            res.append(mv.max(f))
        else:
            return None

    # set correct shortname for result
    res = mv.grib_set_string(res, ["shortName", target_shortname])

    # mark as non-derived field - so that the correct scaling could be applied in contouring
    res = mv.grib_set_long(res, ["generatingProcessIdentifier", 149])
    
    return res
set(files
    compute_speed.mv compute_speed.py
    compute_deaccumulation.mv compute_deaccumulation.py
    compute_maximum.mv compute_maximum.py
    compute_minimum.mv compute_minimum.py
    compute_sum.mv compute_sum.py
)

foreach( f ${files} )

    # copy to the build 'bin' directory
    configure_file(${f} ${CMAKE_BINARY_DIR}/share/metview/eccharts/scripts/${f} COPYONLY)

endforeach()

foreach( f ${files} )

    # ensure file is installed at install time
    install( FILES ${CMAKE_BINARY_DIR}/share/metview/eccharts/scripts/${f}
             DESTINATION share/metview/eccharts/scripts/
             PERMISSIONS OWNER_WRITE OWNER_READ GROUP_READ WORLD_READ)

endforeach()

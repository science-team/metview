function compute_minimum(params: list, data: fieldset, target_shortname: string, interval_steps: list, interval: number)

    if count(params) = 0 or count(data) = 0 then
        return nil
    end if

    param = params[1]
    fs = read(data: data, param: param)

    if count(fs) = 0 then
        return nil
    end if

	# get the steps from the input fieldset
	steps = grib_get_long(fs, "step")
	if type(steps) = "number" then
		steps = [steps]
	end if	
	
    # initialise result
    res = nil

    # loop for the required interval steps
	loop ts in interval_steps

		# define start and end step for interval
		step_start = ts - interval + 1
		step_end = ts
		
		# collect fields in the given interval. We go in reverse order
		# so that the first field collected should be valid at the end of the
		# interval. 
		f = nil
		for i=count(fs) to 1 by -1 do
			if steps[i] >= step_start and steps[i] <= step_end then
				f = f & fs[i]
			end if
		end for			
		
		# compute the minimum for the given interval. The metadata (step etc.) will
		# be copied from the first field of fieldset f!
		if f <> nil then
			res = res & min(f)
		else 
			return nil
		end if	
		
	end loop	
	
    # set correct shortname for result
    res = grib_set_string(res, ["shortName", target_shortname])
    
    # mark as non-derived field - so that the correct scaling could be applied in contouring
    res = grib_set_long(res, ["generatingProcessIdentifier",149 ])
    
    return res

end compute_minimum

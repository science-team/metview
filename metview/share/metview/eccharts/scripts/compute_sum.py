def compute_sum(params, data, target_shortname):

	if len(params) == 0 or len(data) == 0 :
		return None
	
	res = []
	
	for param in params:	

		fs = mv.read(data = data, param = param)

		if len(fs) == 0:
			return None

		if len(res) == 0:
			res = fs
		else:
			res += fs

	# set correct shortname for result
	res = mv.grib_set_string(res, ["shortName", target_shortname])

	# mark as non-drived field - so that the correct scaling could be applied in contouring
	res = mv.grib_set_long(res, ["generatingProcessIdentifier", 149])
    
	return res

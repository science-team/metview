# Metview Macro

#  **************************** LICENSE START ***********************************
# 
#  Copyright 2019 ECMWF. This software is distributed under the terms
#  of the Apache License version 2.0. In applying this license, ECMWF does not
#  waive the privileges and immunities granted to it by virtue of its status as
#  an Intergovernmental Organization or submit itself to any jurisdiction.
# 
#  ***************************** LICENSE END ************************************

include 'test_utils.mv'

test_ew_pl()

#------------------------------------------
# Tests
#------------------------------------------

function test_ew_pl()

    PI = acos(-1)

    fs = read("tuv_pl.grib")
    fs = fs + 0  # change bitspervalue to 24!
    f = read(data: fs, param: ['t'])
    
    # create artificial field with known values   
    ft = nil  
    for i=1 to count(f) do   
        lev = grib_get_long(f[i],'level')
        lons = longitudes(f[i])
        for k=1 to count(lons) do
            if lons[k] > 180 then
                lons[k] = lons[k] - 360
            end if
        end for    
        v = values(coslat(f[i])) + lons + 10*lev
        ft = ft & (set_values(f[i],v))                
    end for

    areaLst = nil
   
    # north
    north = 70
    west = 10
    south = 40
    east = 50
    areaLst = areaLst & [[north, west, south, east]]
   
    # north, crossing prime meridian
    north = 70
    west = -10
    south = 40
    east = 50
    areaLst = areaLst & [[north, west, south, east]]

    # north, crossing dateline
    north = 70
    west = 140
    south = 40
    east = -160
    areaLst = areaLst & [[north, west, south, east]]

    # south
    north = -30
    west = 10
    south = -60
    east = 50
    areaLst = areaLst & [[north, west, south, east]]

    # south, crossing prime meridian
    north = -30
    west = -10
    south = -60
    east = 50
    areaLst = areaLst & [[north, west, south, east]]

    # south, crossing dateline
    north = -30
    west = 140
    south = -60
    east = -160
    areaLst = areaLst & [[north, west, south, east]]

    for k=1 to count(areaLst) do
      
        area = areaLst[k]        
        test_name = 'ew_pl area=[' & area[1] & "," & area[2] & "," & area[3] & "," & area[4] & "]"
        
        nc = mxs_average(
            area           : area,
            direction      : "ew",
            data           : ft
        )
         
        setcurrent(nc, 't')   
        v = values(nc)
   
        # get number of points and levels along the line
        nlev = 6
        v_lev = |300, 400, 500, 700, 850, 1000|
        nlat = 7
        nlon = 7
    
        v_lat = vector(nlat)
        d_ns = (area[1] - area[3])/(nlat-1)        
        for j=1 to nlat do           
           v_lat[j] = area[1] - (j-1)*d_ns
        end for       
    
        ref = (var_name: 't',
            dim_names: ["time","nlev","lat"],
            dim_values: [1,nlev,nlat])   
        check_xs_var(nc, ref, test_name,1)
    
        ref = (var_name: 't_lev',
            dim_names: ["time","nlev"],
            dim_values: [1,nlev],
            values: v_lev)   
        check_xs_var(nc, ref, test_name,1)
             
        ref = (var_name: 'lat',
            dim_names: ["lat"],
            dim_values: [nlat],
            values: v_lat)   
        check_xs_var(nc, ref, test_name,1)
           
        ref = (var_name: 'lon',
            dim_names: ["lon"],
            dim_values: [nlon])  
        check_xs_var(nc, ref, test_name,1)   
             
        # build reference
        v_ref = v*0
    
        avg_ew = (area[4] + area[2])/2 
      
        for i=1 to nlev do
            for j=1 to nlat do           
                lat = area[1] - (j-1)*d_ns
                v_ref[(i-1)*nlat + j] = cos(lat * PI/180) + avg_ew + 10*v_lev[i]                          
            end for
        end for
 
        assert(vec_same(v, v_ref,1E-4), test_name & " values",1)
    end for
    
end test_ew_pl  
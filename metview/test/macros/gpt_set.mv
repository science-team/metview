# Metview Macro

# **************************** LICENSE START ***********************************
#
# Copyright 2018 ECMWF. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

include "test_utils.mv"

set1 = create_geo_set()
assert_equal(type(set1), 'geopointset', 'type returned from create_geo_set(1)')
assert_equal(count(set1), 0, 'count of empty set(1)')

set2 = create_geo_set()
assert_equal(type(set2), 'geopointset', 'type returned from create_geo_set(2)')
assert_equal(count(set2), 0, 'count of empty set(2)')

merged_set = set1 & set2
assert_equal(type(merged_set), 'geopointset', 'type returned from merging set1 and set2')
assert_equal(count(merged_set), 0, 'count of empty set(merged)')

gpt = read('gpt_to_grib_input_1.gpt')
merged_set = merged_set & gpt
assert_equal(type(merged_set), 'geopointset', 'type returned from merging merged_set and gpt')
assert_equal(count(merged_set), 1, 'count of empy set merged with gpt')
g1 = merged_set[1]
assert_equal(type(g1), 'geopoints', 'type of merged_set[1]')
assert_equal(count(g1), count(gpt), 'count of merged_set[1]')
compare_geopoints(gpt, g1, 'merged_set[1]')

gpt2 = read('bufr_picker_reference.gpt')
merged_set2 = merged_set & gpt2
assert_equal(type(merged_set2), 'geopointset', 'type returned from merging merged_set2 and gpt1')
assert_equal(count(merged_set2), 2, 'count of 1-set merged_set2 with gpt')
g1 = merged_set2[1]
compare_geopoints(gpt, g1, 'merged_set2[1]')
g2 = merged_set2[2]
compare_geopoints(gpt2, g2, 'merged_set2[2]')


# create a copy
merged_set_copy = merged_set2
assert_equal(type(merged_set_copy), 'geopointset', 'type of merged_set_copy')
assert_equal(count(merged_set_copy), 2, 'count of 1-set merged_set_copy')


# construct from multiple geopoints files
g1 = read('bufr_obs_filter_area_reference.gpt')
g2 = read('bufr_obs_filter_custom_reference.gpt')
g3 = read('nearest_gridpoint_gpt_ref_1.gpt')
set3 = create_geo_set(g1, g2, g3)
assert_equal(type(set3), 'geopointset', 'type of set3')
assert_equal(count(set3), 3, 'count of set3')
compare_geopoints(set3[1], g1, 'set3[1]')
compare_geopoints(set3[2], g2, 'set3[2]')
compare_geopoints(set3[3], g3, 'set3[3]')


# concatentate empty geopointset with non-empty geopointset
gempty = create_geo_set()
gmerge_empty_non = gempty & set3
compare_geopointsets(gmerge_empty_non, set3, 'gmerge_empty_non')


# concatentate non-empty geopointset with non-empty geopointset
gmerge_non_non = merged_set2 & set3
assert_equal(type(gmerge_non_non), 'geopointset', 'type ofgmerge_non_nonnegset')
assert_equal(count(gmerge_non_non), 5, 'count of gmerge_non_non')
compare_geopoints(gmerge_non_non[1], merged_set2[1], 'gmerge_non_non[1] gpt compare')
compare_geopoints(gmerge_non_non[2], merged_set2[2], 'gmerge_non_non[2] gpt compare')
compare_geopoints(gmerge_non_non[3], set3[1],        'gmerge_non_non[3] gpt compare')
compare_geopoints(gmerge_non_non[4], set3[2],        'gmerge_non_non[4] gpt compare')
compare_geopoints(gmerge_non_non[5], set3[3],        'gmerge_non_non[5] gpt compare')


# try a unary operator
# - also checks that the original gset has not been modified by the operation
negset = neg(merged_set2)
assert_equal(type(negset), 'geopointset', 'type of negset')
assert_equal(count(negset), 2, 'count of negset')
assert_equal(minvalue(negset[1]), -maxvalue(merged_set2[1]), 'minval of negset1')
assert_equal(minvalue(negset[1]), -20, 'minval of negset1 (2) ')
assert_equal(minvalue(negset[2]), -maxvalue(merged_set2[2]), 'minval of negset2 (1)')
assert_equal(minvalue(negset[2]), -300.6, 'minval of negset2 (2) ')
compare_geopoints(negset[1], -merged_set2[1], 'negset[1] gpt compare')
compare_geopoints(negset[2], -merged_set2[2], 'negset[1] gpt compare')

# try number + geopointset
gsetplus1 = merged_set2 + 1
assert_equal(minvalue(gsetplus1[1]), minvalue(gpt)  + 1, 'minval of gsetplus1 (1)')
assert_equal(minvalue(gsetplus1[2]), minvalue(gpt2) + 1, 'minval of gsetplus1 (2)')
assert_equal(maxvalue(gsetplus1[1]), maxvalue(gpt)  + 1, 'maxval of gsetplus1 (1)')
assert_equal(maxvalue(gsetplus1[2]), maxvalue(gpt2) + 1, 'maxval of gsetplus1 (2)')
compare_geopoints(gsetplus1[1], gpt  + 1, 'gsetplus1[1] gpt compare')
compare_geopoints(gsetplus1[2], gpt2 + 1, 'gsetplus1[2] gpt compare')

# try geopointset * number
gsetmul2 = 2 * merged_set2
assert_equal(minvalue(gsetmul2[1]), minvalue(gpt) * 2,            'minval of gsetmul2 (1)')
assert_equal(maxvalue(gsetmul2[2]), maxvalue(merged_set2[2] * 2), 'maxval of gsetmul2 (2)')
compare_geopoints(gsetmul2[1], gpt  * 2, 'gsetmul2[1] gpt compare')
compare_geopoints(gsetmul2[2], gpt2 * 2, 'gsetmul2[2] gpt compare')

# try single fieldset - multi geopointset
fs = read('t1000_LL_1x1.grb')
fs_minus_gps = fs - merged_set2
assert_equal(type(fs_minus_gps), 'geopointset', 'type of fs_minus_gps')
assert_equal(count(fs_minus_gps), 2, 'count of fs_minus_gps')
compare_geopoints(fs_minus_gps[1], fs[1] - merged_set2[1], 'fs_minus_gps[1] gpt compare')
compare_geopoints(fs_minus_gps[2], fs[1] - merged_set2[2], 'fs_minus_gps[2] gpt compare')

# try multi geopointset - single fieldset
gps_minus_fs = merged_set2 - fs
assert_equal(type(gps_minus_fs), 'geopointset', 'type of gps_minus_fs')
assert_equal(count(gps_minus_fs), 2, 'count of gps_minus_fs')
compare_geopoints(gps_minus_fs[1], merged_set2[1] - fs[1], 'gps_minus_fs[1] gpt compare')
compare_geopoints(gps_minus_fs[2], merged_set2[2] - fs[1], 'gps_minus_fs[2] gpt compare')

# try multi fieldset - single geopointset
fs_multi = read('hindcast.grib')
multi_fs_minus_single_gps = fs_multi - merged_set
assert_equal(type(multi_fs_minus_single_gps), 'geopointset', 'type of multi_fs_minus_single_gps')
assert_equal(count(multi_fs_minus_single_gps), 6, 'count of multi_fs_minus_single_gps')
compare_geopoints(multi_fs_minus_single_gps[1], fs_multi[1] - gpt, 'multi_fs_minus_single_gps[1]')
compare_geopoints(multi_fs_minus_single_gps[2], fs_multi[2] - gpt, 'multi_fs_minus_single_gps[2]')
compare_geopoints(multi_fs_minus_single_gps[3], fs_multi[3] - gpt, 'multi_fs_minus_single_gps[3]')
compare_geopoints(multi_fs_minus_single_gps[4], fs_multi[4] - gpt, 'multi_fs_minus_single_gps[4]')
compare_geopoints(multi_fs_minus_single_gps[5], fs_multi[5] - gpt, 'multi_fs_minus_single_gps[5]')
compare_geopoints(multi_fs_minus_single_gps[6], fs_multi[6] - gpt, 'multi_fs_minus_single_gps[6]')


# multi fieldset and multi geopoints
gpts6 = create_geo_set(g1, g2, g3, g1*2, g2*2, g3*2)
multi_fs_minus_multi_gps = fs_multi - gpts6
assert_equal(type(multi_fs_minus_multi_gps), 'geopointset', 'type of multi_fs_minus_multi_gps')
assert_equal(count(multi_fs_minus_multi_gps), 6, 'count of multi_fs_minus_multi_gps')
compare_geopoints(multi_fs_minus_multi_gps[1], fs_multi[1] - gpts6[1], 'multi_fs_minus_multi_gps[1]')
compare_geopoints(multi_fs_minus_multi_gps[2], fs_multi[2] - gpts6[2], 'multi_fs_minus_multi_gps[2]')
compare_geopoints(multi_fs_minus_multi_gps[3], fs_multi[3] - gpts6[3], 'multi_fs_minus_multi_gps[3]')
compare_geopoints(multi_fs_minus_multi_gps[4], fs_multi[4] - gpts6[4], 'multi_fs_minus_multi_gps[4]')
compare_geopoints(multi_fs_minus_multi_gps[5], fs_multi[5] - gpts6[5], 'multi_fs_minus_multi_gps[5]')
compare_geopoints(multi_fs_minus_multi_gps[6], fs_multi[6] - gpts6[6], 'multi_fs_minus_multi_gps[6]')


# write to disk + read it back to compare
write('geopointset_1.gpts', multi_fs_minus_multi_gps)
assert(exist('geopointset_1.gpts'), 'geopointset_1.gpts exists')
read_gptset = read('geopointset_1.gpts')
assert_equal(type(read_gptset), 'geopointset', 'type of read_gptset')
assert_equal(count(read_gptset), 6, 'count of read_gptset')
compare_geopointsets(read_gptset, multi_fs_minus_multi_gps, 'read_gptset equals multi_fs_minus_multi_gps')

# write it again, just to make sure we're not losing anything!
write('geopointset_2.gpts', read_gptset)
read_gptset_2 = read('geopointset_2.gpts')
assert_equal(count(read_gptset_2), 6, 'count of read_gptset_2')
compare_geopointsets(read_gptset_2, read_gptset, 'read_gptset_2 equals read_gptset')

# try number + geopointset on the one we just read in, just to make
# sure it behaves as a 'normal' set with all the data loaded
readplus1 = read_gptset_2 + 1
compare_geopoints(readplus1[1], read_gptset_2[1] + 1, 'readplus1[1] gpt compare')
compare_geopoints(readplus1[2], read_gptset_2[2] + 1, 'readplus1[2] gpt compare')


# write an empty geopointset to disk and see what it looks like
empty_gpset = create_geo_set()
write('empty_set.gpts', empty_gpset)
read_empty_gpset = read('empty_set.gpts')
assert_equal(type(read_empty_gpset), 'geopointset', 'type of read_empty_gpset')


# write an ncols-based geopointset and read it back in again
gpn = create_geo(
        type       : 'ncols',
        latitudes  : |50.,80.,60.,vector_missing_value,-40.|,
        longitudes : |-30.,-0.0065,15.,vector_missing_value,296.|,
        levels     : |500., 500, 500|,
        stnids     : ['11520','0734',nil,'82202','abcd'],
        vals_0     : |10.,20.,30.,40.,50.|
)
gptsn = create_geo_set(gpn, gpn, gpn)
write('geopointset_n.gpts', gptsn)
read_gptset_n = read('geopointset_n.gpts')
assert_equal(count(read_gptset_n), 3, 'count of read_gptset_n')
compare_geopointsets(read_gptset_n, gptsn, 'read_gptset_n equals gptsn')
write('geopointset_n_read.gpts', read_gptset_n)
read2_gptset_n = read('geopointset_n_read.gpts')
assert_equal(count(read2_gptset_n), 3, 'count of read2_gptset_n')
compare_geopointsets(read2_gptset_n, read_gptset_n, 'read2_gptset_n equals read_gptset_n')



# test the filtering of geopointsets

# test with an empty set and an empty filter - it should return the empty set unchanged
gf_empty_no_filter = filter(create_geo_set(), ())
compare_geopointsets(gf_empty_no_filter, create_geo_set(), 'gf_empty_no_filter should be empty gset')

gf_gpts_with_no_metadata = create_geo_set(create_geo(5))
gf_empty_gpts_no_filter = filter(gf_gpts_with_no_metadata, ())
compare_geopointsets(gf_empty_gpts_no_filter, gf_gpts_with_no_metadata, 'gf_empty_gpts_no_filter should be same as input gset')

gf_empty_gpts_with_filter = filter(gf_gpts_with_no_metadata, (key:111))
assert(gf_empty_gpts_with_filter = nil, 'gf_empty_gpts_with_filter should be nil')


#  create a set of geopoints with values we can use to easily
#  confirm they are the ones we're looking for

g1   = create_dummy_gpts((date:20010101, level:10))
g2   = create_dummy_gpts((date:20010102, level:20))
g3   = create_dummy_gpts((date:20010103, level:30))
g4   = create_dummy_gpts((date:20010104, level:40))
g5   = create_dummy_gpts((date:20010105, level:50))
g56  = create_dummy_gpts((date:20010105, level:60))
gset = create_geo_set(g1, g2, g3, g4, g5, g56)

# test with an empty filter - it should return the set unchanged
gf_no_filter = filter(gset, ())
compare_geopointsets(gf_no_filter, gset, 'gf_no_filter should equal gset')

# test with a bad filter - should get nil back
gf_bad_filter = filter(gset, (bad_key:7777))
assert(gf_bad_filter = nil, 'gf_bad_filter should be nil')

# test with an empty set and a non-empty filter - it should return nil
gf_empty_with_filter = filter(create_geo_set(), (arbitrary:'val'))
assert(gf_empty_with_filter = nil, 'gf_empty_with_filter should be nil')

# filter on single keys
gf_filter_level_2 = filter(gset, (level:20))
compare_geopointsets(gf_filter_level_2, create_geo_set(g2), 'gf_filter_level_2 should contain exactly g2')

gf_filter_level_5 = filter(gset, (level:50))
compare_geopointsets(gf_filter_level_5, create_geo_set(g5), 'gf_filter_level_5 should contain exactly g5')

gf_filter_level_5x = filter(gset, ('level':50))
compare_geopointsets(gf_filter_level_5x, create_geo_set(g5), 'gf_filter_level_5x should contain exactly g5')

gf_filter_date_3 = filter(gset, (date:20010103))
compare_geopointsets(gf_filter_date_3, create_geo_set(g3), 'gf_filter_date_3 should contain exactly g3')

# single matching key plus a bad key
gf_filter_level_2_and_bad = filter(gset, (level:20, bad_key:8776))
assert(gf_filter_level_2_and_bad = nil, 'gf_filter_level_2_and_bad should be nil')

# two matching keys
gf_filter_level_and_date_4 = filter(gset, (level:40, date:20010104))
compare_geopointsets(gf_filter_level_and_date_4, create_geo_set(g4), 'gf_filter_level_and_date_4 should contain exactly g4')

# one key matches, one does not
gf_filter_level_4_and_bad_date = filter(gset, (level:40, date:20090909))
assert(gf_filter_level_4_and_bad_date = nil, 'gf_filter_level_4_and_bad_date should be nil')

# one gpt matches both, one matches one key
gf_filter_level_5_and_date_5 = filter(gset, (level:50, date:20010105))
compare_geopointsets(gf_filter_level_5_and_date_5, create_geo_set(g5), 'gf_filter_level_5_and_date_5 should contain exactly g5')

# matches both g5 and g56
gf_filter_date_5 = filter(gset, (date:20010105))
compare_geopointsets(gf_filter_date_5, create_geo_set(g5,g56), 'gf_filter_date_5 should contain exactly g5,g56')

# contains 2xg2 and 3xg3
gset2 = create_geo_set(g1, g2, g3, g4, g5, g2, g3, g3)
gf2_filter_level_2 = filter(gset2, (level:20))
compare_geopointsets(gf2_filter_level_2, create_geo_set(g2,g2), 'gf2_filter_level_2 should contain exactly g2,g2')

# match one of a list of values from a key
gf_filter_level_4_or_5 = filter(gset2, (level:[40, 50]))
compare_geopointsets(gf_filter_level_4_or_5, create_geo_set(g4,g5), 'gf_filter_level_4_or_5 should contain exactly g4,g5')

# same, but different order of key values - should not affect the result
gf_filter_level_4_or_5x = filter(gset2, (level:[50, 40]))
compare_geopointsets(gf_filter_level_4_or_5x, create_geo_set(g4,g5), 'gf_filter_level_4_or_5x should contain exactly g4,g5')

gf_filter_level_3_or_4 = filter(gset2, (level:[30, 40]))
compare_geopointsets(gf_filter_level_3_or_4, create_geo_set(g3,g4,g3,g3), 'gf_filter_level_3_or_4 should contain exactly g3,g4,g3,g3')

gf_filter_level_3_or_4_and_date_3 = filter(gset2, (level:[30, 40], date:20010103))
compare_geopointsets(gf_filter_level_3_or_4_and_date_3, create_geo_set(g3,g3,g3), 'gf_filter_level_3_or_4_and_date_3 should contain exactly g3,g3,g3')


# -----------------------------------------------------------
# Utility functions
# -----------------------------------------------------------

# -----------------------------------------------------------
# compare_geopoints
# will fail if any element of the geopoints variables differs
# -----------------------------------------------------------

function compare_geopoints(g1: geopoints, g2: geopoints,fname: string)

    if count(g1) <> count(g2) then
        print('Function ', fname, ': different number of points: ', count(g1), ' vs ', count(g2))
        fail()
    end if

    diff = g1 - g2

    if maxvalue(abs(diff)) > 0.1 then
        print('Function ', fname, ': difference in values. Please inspect the results vs the reference')
        fail()
    end if

    if maxvalue(abs(latitudes(g1)-latitudes(g2))) > 0.05 then
        print('Function ', fname, ': difference in latitudes. Please inspect the results vs the reference')
        fail()
    end if

    if maxvalue(abs(longitudes(g1)-longitudes(g2))) > 0.05 then
        print('Function ', fname, ': difference in longitudes. Please inspect the results vs the reference')
        fail()
    end if

    if maxvalue(abs(levels(g1)-levels(g2))) > 0.05 then
        print('Function ', fname, ': difference in levels. Please inspect the results vs the reference')
        fail()
    end if

    if maxvalue(abs(vector(dates(g1)-dates(g2)))) > 0.05 then
        print('Function ', fname, ': difference in dates. Please inspect the results vs the reference')
        fail()
    end if

    md1 = metadata(g1)
    md2 = metadata(g2)
    if md1 <> nil or md2 <> nil then
        if not is_equal_any(metadata(g1), metadata(g2)) then
            print('Function ', fname, ':Metadata not equal')
            fail()
        end if
    end if

end compare_geopoints


# -----------------------------------------------------------
# compare_geopointsets
# will fail if anything differs
# -----------------------------------------------------------

function compare_geopointsets(g1, g2, fname: string)

    assert(type(g1) = 'geopointset', 'cmpgsets: type 1 is ' & type(g1) & ' : ' & fname)
    assert(type(g2) = 'geopointset', 'cmpgsets: type 2 is ' & type(g2) & ' : ' & fname)

    if count(g1) <> count(g2) then
        print('Function ', fname, ': different number of gpts: ', count(g1), ' vs ', count(g2))
        fail()
    end if

    for i = 1 to count(g1) do
        compare_geopoints(g1[i], g2[i], 'cmpgsets[' & i & ']: ' & fname)
    end for

end compare_geopointsets


function create_dummy_gpts(md)

    g1 = create_geo(1)
    g1 = set_dates (g1, |md.date|)
    g1 = set_levels(g1, |md.level|)
    g1 = set_metadata(g1, md)
    return g1

end create_dummy_gpts

# Metview Macro

#  **************************** LICENSE START ***********************************
# 
#  Copyright 2017 ECMWF. This software is distributed under the terms
#  of the Apache License version 2.0. In applying this license, ECMWF does not
#  waive the privileges and immunities granted to it by virtue of its status as
#  an Intergovernmental Organization or submit itself to any jurisdiction.
# 
#  ***************************** LICENSE END ************************************
#

include "test_utils.mv"

gribsetbits(16)

test_basic(-1,-1, "base")
test_basic(133, 135, "param")

function test_basic(vor_par, div_par, test_id)
    # Read input fieldset and ref
    fs = read("vorticity_divergence_500_sh.grb")
    fs_ref = read("divrot_500_sh_ref.grib")

    # rotational wind
    vo = read(
        param : "vo",
        data  : fs
        )

    d_core = (truncation : 48,
              data       : vo)

    if vor_par <> -1 then
        vo = grib_set_long(vo, ["paramId", vor_par])
        d_core.vorticity_param = vor_par
        d_core.data = vo       
    end if
   
    rw = divrot(d_core)
  
    assert_equal(grib_get_string(rw[1],'paramId'),"131","divrot paramId", test_id) 
    assert_equal(grib_get_string(rw[2],'paramId'),"132","divrot paramId", test_id) 
    for i=1 to count(rw) do   
        assert_equal(grib_get_string(rw[1],'dataRepresentationType'),"sh","divrot representation - " & i, test_id)   
        assert_equal(grib_get_string(rw[1],'J'),"48","divrot truncation - " & i, test_id)
    end for
    
    rw_ref = fs_ref[1] & fs_ref[2]
    for i=1 to count(rw) do
        v = values(rw[i])
        v_ref = values(rw_ref[i])
        assert(vec_same(v,v_ref,1E-8), "divrot value-" & i, test_id)
    end for
   
    # divergent wind
    d = read(
        param : "d",
        data  : fs
        )
    
    d_core = (truncation : 48,
              data       : d)

    if div_par <> -1 then
        d = grib_set_long(d, ["paramId", div_par])
        d_core.divergence_param = div_par
        d_core.data = d
    end if

    dw = divwind(d_core)
    assert_equal(grib_get_string(dw[1],'paramId'),"131","divwind paramId", test_id) 
    assert_equal(grib_get_string(dw[2],'paramId'),"132","divwind paramId", test_id) 
    for i=1 to count(rw) do   
        assert_equal(grib_get_string(dw[1],'dataRepresentationType'),"sh","divwind representation - " & i, test_id)   
        assert_equal(grib_get_string(dw[1],'J'),"48","divwind truncation - " & i, test_id)
    end for

    dw_ref = fs_ref[3] & fs_ref[4]
    for i=1 to count(dw) do
        v = values(dw[i])
        v_ref = values(dw_ref[i])
        assert(vec_same(v,v_ref,1E-8), "divwind value-" & i,test_id)
    end for
    
    # wind from div and vor
    d_core = (truncation : 48,
              data       : d & vo)

    if div_par <> -1 then
        d_core.divergence_param = div_par
    end if
    if vor_par <> -1 then
        d_core.vorticity_param = vor_par
    end if
 
    uv = uvwind(d_core)
    assert_equal(grib_get_string(uv[1],'paramId'),"131","uvwind paramId", test_id) 
    assert_equal(grib_get_string(uv[2],'paramId'),"132","uvwind paramId", test_id) 
    for i=1 to count(uv) do   
        assert_equal(grib_get_string(uv[1],'dataRepresentationType'),"sh","uvwind representation - " & i, test_id)   
        assert_equal(grib_get_string(uv[1],'J'),"48","uvwind truncation - " & i, test_id)
    end for

    uv_ref = fs_ref[5] & fs_ref[6]
    for i=1 to count(uv) do
        v = values(uv[i])
        v_ref = values(uv_ref[i])
        assert(vec_same(v,v_ref,1E-8), "uvwind value - " & i,test_id)
    end for

end test_basic

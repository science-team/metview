# ------------------------------------------------------------------------
# MACRO metview_etc_files
# Ensures the files listed in ETC_FILES are copied to the build directory
# and that they will be installed at install time
# ------------------------------------------------------------------------

set(POST_INSTALL_TEST_SCRIPT ${CMAKE_CURRENT_BINARY_DIR}/metview_post_install_tests.sh)
set(MVRUN "${INSTALLED_METVIEW_STARTUP_SCRIPT}")

macro(metview_macro_test)
    set(options POST_INSTALL)
    set(single_value_args MACRO)
    set(multi_value_args RESOURCES MACRO_ARGS)
    cmake_parse_arguments(_PAR "${options}" "${single_value_args}" "${multi_value_args}" ${_FIRST_ARG} ${ARGN})

    ecbuild_add_test(TARGET ${_PAR_MACRO}.test
        COMMAND ${FULL_STARTUP_SCRIPT_PATH}
        TYPE SCRIPT
        ARGS -nocreatehome -b ${_PAR_MACRO} ${_PAR_MACRO_ARGS}
        RESOURCES ${_PAR_MACRO} ${_PAR_RESOURCES} test_utils.mv hov_utils.mv grib_nearest_utils.mv thermo_parcel_utils.mv )

    # add to the post-install tests too
    if(_PAR_POST_INSTALL)
        string(REPLACE ";" " " MACRO_ARGS "${_PAR_MACRO_ARGS}") # ';' -> ' '
        file(APPEND ${POST_INSTALL_TEST_SCRIPT} "${MVRUN} -b ${_PAR_MACRO} ${MACRO_ARGS}\n")
    endif()

    set(all_tests "${all_tests}${_PAR_MACRO} ") # note the spacing
endmacro()

# write the start of the post-install test script
file(WRITE ${POST_INSTALL_TEST_SCRIPT} "#!/bin/sh\n")
file(APPEND ${POST_INSTALL_TEST_SCRIPT} "# Run the tests on the installed version of Metview\n")
file(APPEND ${POST_INSTALL_TEST_SCRIPT} "# -------------------------------------------------\n")
file(APPEND ${POST_INSTALL_TEST_SCRIPT} "\n")
file(APPEND ${POST_INSTALL_TEST_SCRIPT} "set -eax\n")
file(APPEND ${POST_INSTALL_TEST_SCRIPT} "\n")

set(all_tests) # empty to start with

if(ENABLE_PLOTTING)
    metview_macro_test(MACRO HelloWorld.mv
        RESOURCES set_output.mv ../data/z500.grb
        POST_INSTALL)

    metview_macro_test(MACRO layoutx3.mv
        RESOURCES set_output.mv ../data/z500.grb)

    metview_macro_test(MACRO plot_from_read_grib.mv
        RESOURCES set_output.mv ../data/fc_data.grib)

    metview_macro_test(MACRO plot_bufr.mv
        RESOURCES set_output.mv ../data/temp_10.bufr)
endif()

if(NOT ENABLE_PLOTTING)
    metview_macro_test(MACRO no_plotting.mv
        POST_INSTALL)
endif()

metview_macro_test(MACRO grib_dates.mv
    RESOURCES ../data/fc_data.grib
    ../data/hindcast.grib
    ../data/fc_long_step.grib
    ../data/monthly_avg.grib
    ../data/flexpart_minute_step.grib
    ../data/step_60m.grib
    POST_INSTALL)

metview_macro_test(MACRO grib_keys.mv
    RESOURCES ../data/fc_data.grib
    ../data/lnsp.grib
    ../data/xs_rgg_input.grib
    POST_INSTALL)

metview_macro_test(MACRO grib_gradient.mv
    RESOURCES ../data/grad_input_global.grib
    ../data/grad_ref_global.grib
    ../data/grad_input_global_sn.grib
    ../data/grad_ref_global_sn.grib
    ../data/u_sn_reg_ll.grib
    ../data/grad_ref_u_sn.grib
    ../data/second_deriv_input_global.grib
    ../data/second_deriv_ref_global.grib
    ../data/div_vor_ref.grib
    ../data/laplace_ref.grib
    POST_INSTALL)

metview_macro_test(MACRO grib_gradient_analytic.mv
    RESOURCES ../data/grad_input_global.grib)

metview_macro_test(MACRO qg_meteo.mv
    RESOURCES ../data/geostrophic.grib
    ../data/tz_1x1_na.grib
    ../data/static_stability_ref.grib
    ../data/q_vector_ref.grib)

metview_macro_test(MACRO grib_integral.mv
    RESOURCES ../data/t1000_LL_2x2.grb
    ../data/LL_2x2_cellarea_ref.grib
    ../data/t1000_reg_gg_N48.grib
    ../data/t1000_red_gg_N48.grib
    ../data/rgg_small_subarea.grib
    ../data/reg_gg_N48_cellarea_ref.grib
    ../data/red_gg_N48_cellarea_ref.grib
    ../data/rgg_O1280_subarea.grib
    ../data/rgg_small_subarea_cellarea_ref.grib)

metview_macro_test(MACRO grib_nearest_ll.mv
    RESOURCES ../data/bufr_picker_reference.gpt
    ../data/nearest_gridpoint_gpt_ref_1.gpt
    ../data/t1000_LL_1x1.grb
    ../data/t1000_LL_1x1_subarea.grb
    ../data/t1000_LL_1x1_subarea_g2.grb
    ../data/t1000_LL_2x2.grb
    ../data/t1000_LL_2x2_with_missing.grb
    ../data/nearest_gridpoint_gpt_ref_missing_1.gpt
    ../data/nearest_gridpoint_gpt_ref_missing_1.gpt
    ../data/nearest_gridpoint_gpt_ref_missing_2.gpt
    ../data/nearest_gridpoint_gpt_ref_missing_2.gpt
    ../data/ll_no_pole.grib
    ../data/z500.grb
    ../data/z500_g2.grb)

metview_macro_test(MACRO grib_nearest_reduced_ll.mv
    RESOURCES ../data/reduced_ll.grib
    )

metview_macro_test(MACRO grib_nearest_reduced_gg.mv
    RESOURCES
    ../data/t1000_SH_to_RGG_reference.grb
    ../data/t1000_SH_to_RGG_reference_g2.grb
    ../data/t1000_red_gg_N48.grib
    ../data/rgg_western_subarea.grib
    ../data/rgg_western_subarea_g2.grib
    ../data/rgg_small_subarea.grib
    ../data/rgg_zero_lon_subarea.grib
    ../data/rgg_zero_lon_subarea_g2.grib
    )

metview_macro_test(MACRO grib_nearest_regular_gg.mv
    RESOURCES
    ../data/regular_gaussian_subarea.grib
    ../data/regular_gaussian_subarea_g2.grib
    ../data/t1000_reg_gg_N48.grib
)

metview_macro_test(MACRO grib_nearest_healpix.mv
    RESOURCES ../data/t2m_healpix_4.grib
    ../data/ml_to_hl_agr_log_geop_h_ref.grib
    )

metview_macro_test(MACRO grib_vertical_geopot_on_ml.mv
    RESOURCES ../data/tq_ml137.grib
    ../data/z_ml137_ref.grib
    )

metview_macro_test(MACRO grib_vertical_integral.mv
    RESOURCES ../data/t_lnsp_ml137.grb
    ../data/univertint_ml_ref.grib
    ../data/univertint_pl_ref.grib
    ../data/t_80Pa.grib
    ../data/vertint_ref.grib
    ../data/tuv_pl.grib
     )

metview_macro_test(MACRO grib_vertical_interpolate_to_pl.mv
     RESOURCES ../data/tq_ml137.grib
     ../data/ml2pl_ref.grib
     ../data/xs_pl_input.grib
     ../data/pl_to_pl_ref.grib
     ../data/pl_to_pl_log_ref.grib
     ../data/pl_pa_hpa.grib
     )

metview_macro_test(MACRO grib_vertical_interpolate_ml_to_hl_geom_h_1.mv
     RESOURCES ../data/tq_ml137.grib
      ../data/z_ml137_ref.grib
      ../data/ml_to_hl_asl_geom_h_ref.grib
      ../data/ml_to_hl_agr_geom_h_ref.grib
      ../data/ml_to_hl_asl_log_geom_h_ref.grib
      ../data/ml_to_hl_agr_log_geom_h_ref.grib
      ../data/ml_to_hl_asl_geom_h_ref_sub_surface.grib
     )


metview_macro_test(MACRO grib_vertical_interpolate_ml_to_hl_geom_h_2.mv
     RESOURCES ../data/tq_ml137.grib
     ../data/z_ml137_ref.grib
     ../data/ml_to_hl_asl_geom_h_ref.grib
     ../data/ml_to_hl_agr_geom_h_ref.grib
     ../data/ml_to_hl_agr_surf_geom_h_ref.grib
     ../data/ml_to_hl_agr_surf_log_geom_h_ref.grib
     )

metview_macro_test(MACRO grib_vertical_interpolate_ml_to_hl_geop_h_1.mv
     RESOURCES ../data/tq_ml137.grib
      ../data/z_ml137_ref.grib
      ../data/ml_to_hl_asl_geop_h_ref.grib
      ../data/ml_to_hl_agr_geop_h_ref.grib
      ../data/ml_to_hl_asl_log_geop_h_ref.grib
      ../data/ml_to_hl_agr_log_geop_h_ref.grib
     )


metview_macro_test(MACRO grib_vertical_interpolate_ml_to_hl_geop_h_2.mv
     RESOURCES ../data/tq_ml137.grib
     ../data/z_ml137_ref.grib
     ../data/ml_to_hl_asl_geop_h_ref.grib
     ../data/ml_to_hl_agr_geop_h_ref.grib
     ../data/ml_to_hl_agr_surf_geop_h_ref.grib
     ../data/ml_to_hl_agr_surf_log_geop_h_ref.grib
     )


metview_macro_test(MACRO grib_vertical_interpolate_pl_to_hl_geom_h_1.mv
     RESOURCES ../data/pl_to_hl_input.grib
      ../data/pl_to_hl_asl_geom_h_ref.grib
      ../data/pl_to_hl_agr_geom_h_ref.grib
      ../data/pl_to_hl_asl_log_geom_h_ref.grib
      ../data/pl_to_hl_agr_log_geom_h_ref.grib
      ../data/pl_to_hl_asl_geom_h_ref_sub_surface.grib
      ../data/pl_to_hl_agr_geom_h_ref_sub_surface.grib
     )


metview_macro_test(MACRO grib_vertical_interpolate_pl_to_hl_geom_h_2.mv
     RESOURCES ../data/pl_to_hl_input.grib
     ../data/pl_to_hl_asl_geom_h_ref.grib
     ../data/pl_to_hl_agr_geom_h_ref.grib
     ../data/pl_to_hl_agr_geom_h_non_const_ref.grib
     )

metview_macro_test(MACRO grib_vertical_pressure.mv
    RESOURCES ../data/t_lnsp_ml137.grb
    ../data/tuv_pl.grib
    ../data/thickness_ml137.grib
    ../data/pres_ml137.grib
    ../data/dt_dp_ref.grib
    ../data/omega_ml.grib
    ../data/omega_pl.grib
    )

metview_macro_test(MACRO polygon_mask.mv
    RESOURCES ../data/poly_ref.grib
    ../data/poly_multi_ref.grib)

metview_macro_test(MACRO interpolation_sh_to_ll.mv
    RESOURCES ../data/t1000_SH.grb
    POST_INSTALL)

metview_macro_test(MACRO interpolation_sh_to_rgg.mv
    RESOURCES ../data/t1000_SH.grb)

metview_macro_test(MACRO interpolation_rgg_to_ll.mv
    RESOURCES ../data/t1000_SH_to_RGG_reference.grb)

metview_macro_test(MACRO interp_default.mv
    RESOURCES ../data/t1000_SH_to_RGG_reference.grb)

if(HAVE_PPROC_EMOS AND HAVE_PPROC_MIR)
    metview_macro_test(MACRO interp_emos.mv
        RESOURCES ../data/t1000_SH_to_RGG_reference.grb)
endif()

if(HAVE_PPROC_MIR)
    metview_macro_test(MACRO interp_mir.mv
        RESOURCES ../data/t1000_SH_to_RGG_reference.grb)
endif()

metview_macro_test(MACRO grib_to_gpt.mv
    RESOURCES ../data/t1000_LL_2x2.grb
    ../data/gpt_to_grib_nearest_sum_reference.grb
    ../data/mercator.grib
    POST_INSTALL)

metview_macro_test(MACRO gpt_to_grib_core.mv
    RESOURCES ../data/gpt_to_grib_input_1.gpt
    ../data/gpt_to_grib_reciprocal_reference.grb
    ../data/gpt_to_grib_exp_mean_reference.grb
    ../data/gpt_to_grib_exp_sum_reference.grb
    POST_INSTALL)

metview_macro_test(MACRO gpt_to_grib_with_template.mv
    RESOURCES ../data/t1000_LL_2x2.grb ../data/10U_GG.grb)

metview_macro_test(MACRO gpt_to_grib_nearest.mv
    RESOURCES ../data/t1000_LL_7x7.grb
    ../data/example_XYLDTZ.geo
    ../data/example_XYLDTZ_with_missing.geo
    ../data/gpt_to_grib_nearest_sum_reference.grb
    ../data/gpt_to_grib_nearest_sum_missing_reference.grb
    ../data/gpt_to_grib_nearest_mean_reference.grb
    ../data/gpt_to_grib_nearest_mean_missing_reference.grb
    ../data/gpt_to_grib_nearest_count_reference.grb
    ../data/nearest_gridpoint_gpt_ref_1.gpt)

metview_macro_test(MACRO geopoint_types.mv
    RESOURCES ../data/bufr_obs_filter_basic_reference.gpt
    ../data/example_POLAR_VECTOR.geo
    ../data/example_XYZ.geo
    ../data/example_XY_VECTOR.geo)

metview_macro_test(MACRO gpt_set.mv
    RESOURCES ../data/gpt_to_grib_input_1.gpt
    ../data/bufr_picker_reference.gpt
    ../data/bufr_obs_filter_area_reference.gpt
    ../data/bufr_obs_filter_custom_reference.gpt
    ../data/nearest_gridpoint_gpt_ref_1.gpt
    ../data/t1000_LL_1x1.grb
    ../data/hindcast.grib)

# metview_macro_test(MACRO     interpolation_gg_to_subarea.mv
# RESOURCES  ../data/10U_GG.grb)
metview_macro_test(MACRO bufr_obs_filter.mv
    RESOURCES ../data/bufr-3.bufr
    ../data/compress_3.bufr
    ../data/bufr_obs_filter_basic_reference.gpt
    ../data/temp_10.bufr
    ../data/bufr_obs_filter_level_reference.gpt
    ../data/synop_multi_subset_uncompressed.bufr
    ../data/bufr_obs_filter_area_reference.gpt
    ../data/bufr_obs_filter_missvals_1_reference.gpt
    ../data/bufr_obs_filter_missvals_2_reference.gpt
    ../data/amsua_13.bufr
    ../data/bufr_obs_filter_time_reference.gpt
    ../data/bufr_obs_filter_custom_reference.gpt
    ../data/bufr_obs_filter_blockstation_reference.gpt
    ../data/aircraft_10.bufr
    ../data/trop1.bufr
    ../data/bufr_obs_filter_descriptor_string_reference.gpt
    ../data/bufr_obs_filter_subtype_reference.gpt
    ../data/temp_small.bufr
    ../data/bufr_obs_filter_ncols_reference.gpt
    ../data/bufr_obs_filter_ncols_miss_reference.gpt
    ../data/missing_ident.bufr
    ../data/stnid_with_internal_spaces.bufr
    ../data/bufr_obs_filter_ncols_stnids_spaces_reference.gpt
    ../data/ssd_missing_location.bufr
    ../data/ssd_missing_location_ref.gpt
)

metview_macro_test(MACRO bufr_picker.mv
    RESOURCES ../data/temp_10.bufr ../data/bufr_picker_reference_ws.gpt
    ../data/cape-ens.bufr ../data/bufr_picker_two_coordinate_descriptors_compressed_reference.gpt
    ../data/2t-ens.bufr ../data/bufr_picker_two_coordinate_descriptors_uncompressed_reference.gpt
    ../data/temp_small.bufr ../data/bufr_picker_ncols_reference.gpt
    POST_INSTALL)

if(ENABLE_UI)
    metview_macro_test(MACRO bufr_filter.mv
        RESOURCES ../data/temp_small.bufr ../data/aircraft_small.bufr
        ../data/ens_multi_subset_uncompressed.bufr
        ../data/ens_multi_subset_compressed.bufr
        ../data/synop_multi_subset_uncompressed.bufr
        ../data/tropical_cyclone.bufr
        ../data/wave_uncompressed.bufr
        ../data/compress_3.bufr
        ../data/stnid_with_internal_spaces.bufr
        ../data/syn_new.bufr)
endif()

metview_macro_test(MACRO inline_c.mv
    RESOURCES ../data/z500.grb
    POST_INSTALL)

if(HAVE_METVIEW_FORTRAN)
    metview_macro_test(MACRO inline_fortran.mv
        RESOURCES ../data/z500.grb
        POST_INSTALL)

    metview_macro_test(MACRO spectra.mv
        RESOURCES ../data/z_for_spectra.grib
        POST_INSTALL)

    metview_macro_test(MACRO pottf.mv
        RESOURCES ../data/thermo_profile.grib
        ../data/theta_input_pl.grib
        ../data/theta_pl_ref.grib
        ../data/theta_ml_ref.grib)
endif()

metview_macro_test(MACRO station.mv
    POST_INSTALL)

metview_macro_test(MACRO unix.mv)

metview_macro_test(MACRO vectors.mv
    RESOURCES ../data/gpt_to_grib_nearest_sum_reference.grb
    ../data/bufr_picker_reference.gpt
    POST_INSTALL)

metview_macro_test(MACRO tables.mv
    RESOURCES ../data/table1.csv
    ../data/table2.csv
    ../data/example_XYLDTZ.geo
    ../data/csv_from_windows_1.csv)

metview_macro_test(MACRO lists.mv)

metview_macro_test(MACRO strings.mv)

metview_macro_test(MACRO dates.mv)

metview_macro_test(MACRO version.mv
    POST_INSTALL)

metview_macro_test(MACRO fieldset_bitmap.mv
    RESOURCES ../data/t1000_LL_2x2.grb
    ../data/t1000_LL_2x2_with_missing.grb
    ../data/monthly_avg.grib
    )

metview_macro_test(MACRO fieldset_compute.mv
    RESOURCES ../data/tuv_pl.grib
    ../data/surface_wind.grib
    ../data/abs_vort.grib
    )

metview_macro_test(MACRO fieldset_geo.mv
    RESOURCES ../data/t1000_LL_2x2.grb
    ../data/t1000_LL_7x7.grb
    ../data/fc_data.grib
    ../data/t1000_reg_gg_N48.grib
    ../data/regular_gaussian_subarea.grib
    ../data/t1000_red_gg_N48.grib
    ../data/rgg_small_subarea.grib
    ../data/rgg_western_subarea.grib
    ../data/reduced_ll.grib
    ../data/ll_dateline.grib
    ../data/solar_zenith_angle.grib
    ../data/rmask_ref.grib
    ../data/t2m_healpix_4.grib
    )

metview_macro_test(MACRO fieldset_maths.mv
    RESOURCES ../data/t1000_LL_2x2.grb
    ../data/t1000_LL_7x7.grb
    )

metview_macro_test(MACRO fieldset_stats.mv
    RESOURCES ../data/t1000_LL_2x2.grb
    ../data/t1000_LL_7x7.grb
    ../data/monthly_avg.grib
    )

metview_macro_test(MACRO fieldset_util.mv
    RESOURCES ../data/hindcast.grib
    ../data/sort.grib
    ../data/t1000_LL_2x2.grb
    ../data/t1000_LL_2x2_with_missing.grb
    ../data/t1000_LL_7x7.grb
    ../data/surface_wind.grib
    ../data/xs_rgg_input.grib
    ../data/xs_ml_input.grib
    ../data/tuv_pl.grib
    ../data/daily_clims.grib
    ../data/ll_steps_168.grib
    )

metview_macro_test(MACRO fieldset_read_sort.mv
    RESOURCES ../data/daily_clims.grib
    ../data/ll_steps_168.grib
    ../data/ml2pl_ref.grib
    )

metview_macro_test(MACRO fieldset_sort_default.mv
    RESOURCES ../data/sort.grib
    )

metview_macro_test(MACRO fieldset_sort_custom_1.mv
    RESOURCES ../data/sort.grib
    )

metview_macro_test(MACRO fieldset_sort_custom_2.mv
    RESOURCES ../data/sort.grib
    )

metview_macro_test(MACRO llmatrix.mv
    RESOURCES ../data/myllmat.llmat)

metview_macro_test(MACRO geopoints.mv
    RESOURCES ../data/geopoints_with_metadata.gpt
    ../data/bufr_picker_reference.gpt
    ../data/geopoints_for_subsample.gpt
    ../data/example_XYLDTZ.geo
    ../data/example_XY_VECTOR.geo
    ../data/example_XYZ.geo
    ../data/example_POLAR_VECTOR.geo
    ../data/bufr_obs_filter_level_reference.gpt
    ../data/example_POLAR_VECTOR.geo
    ../data/bufr_obs_filter_time_reference.gpt
    ../data/geo_ncols_1.gpt
    ../data/geo_ncols_2.gpt
    ../data/geo_ncols_3.gpt
    ../data/geo_ncols_4.gpt
    ../data/geo_ncols_5.gpt
    ../data/geo_ncols_6.gpt
    ../data/geo_ncols_7.gpt
    ../data/geo_ncols_8.gpt
    ../data/geo_ncols_9.gpt
    ../data/geo_ncols_10.gpt
    ../data/geo_ncols_11.gpt
    ../data/geo_ncols_stvl_1.gpt
    ../data/t1000_LL_2x2.grb
    ../data/empty_ncols.gpt
    ../data/geopoints_with_metadata.gpt
    ../data/geopoints_with_missing_latlons.gpt
    ../data/invalid_location.gpt
    ../data/invalid_geo_nearest_ll_ref.gpt
    ../data/invalid_geo_nearest_gg_ref.gpt
    ../data/invalid_geo_interpolate_ll_ref.gpt
    ../data/invalid_geo_interpolate_gg_ref.gpt
    POST_INSTALL)

metview_macro_test(MACRO netcdf_computations.mv
    RESOURCES ../data/xs_with_missing_vals.nc
    ../data/xs_without_missing_vals.nc
    ../data/nc_from_grib.nc
    ../data/ushort_data.nc
    ../data/uint_data.nc
    ../data/attrs.nc
    ../data/xs_date_mv5.nc
    ../data/xs_byte_levs.nc
    ../data/z500.nc
    POST_INSTALL)

# metview_macro_test(MACRO     "../../bin/mv_fl_prep.mv 1 mv_flextra_prepare_test01.txt"
# RESOURCES  ../data/mv_flextra_prepare_test01.txt
# ../data/hindcast.grib)
metview_macro_test(MACRO divrot.mv
    RESOURCES ../data/vorticity_divergence_500_sh.grb
    ../data/divrot_500_sh_ref.grib)

metview_macro_test(MACRO input_visualiser_missing_value.mv)

metview_macro_test(MACRO velpot.mv
    RESOURCES ../data/d_vo_sh.grib
    ../data/velpot_ref.grib)

# metview_macro_test(MACRO      mv_flextra_prep.mv
# MACRO_ARGS 1 mv_flextra_prepare_test01.txt
# RESOURCES  ../../scripts/mv_flextra_prep.mv
# ../data/mv_flextra_prepare_test01.txt)
metview_macro_test(MACRO thermo_data.mv
    RESOURCES ../data/thermo_profile.grib
    ../data/temp_10.bufr
    ../data/thermo_bufr_ref.nc
    ../data/temp_wigos.bufr
    ../data/thermo_bufr_wigos_ref.nc
    ../data/thermo_grib_ref.nc
    ../data/thermo_grib_area_ref.nc
    ../data/thermo_profile_baltic.grib
    POST_INSTALL)

metview_macro_test(MACRO thermo_humidity.mv
    RESOURCES ../data/thermo_profile.grib
    ../data/theta_input_pl.grib
    ../data/td_2m.grib
    ../data/tq_ml137.grib
    POST_INSTALL)

metview_macro_test(MACRO thermo_parcel_grib_amazon_1.mv
    RESOURCES ../data/thermo_profile.grib
    POST_INSTALL)

metview_macro_test(MACRO thermo_parcel_grib_amazon_2.mv
    RESOURCES ../data/thermo_profile.grib
    POST_INSTALL)

metview_macro_test(MACRO thermo_parcel_grib_cin.mv
    RESOURCES ../data/thermo_profile_east.grib
    ../data/thermo_profile_capping_cin.grib
    POST_INSTALL)

metview_macro_test(MACRO thermo_parcel_grib_himalayas.mv
    RESOURCES ../data/thermo_profile_east.grib
    POST_INSTALL)

metview_macro_test(MACRO thermo_parcel_grib_inversion_warm_sea.mv
    RESOURCES ../data/thermo_profile_east.grib
    POST_INSTALL)

metview_macro_test(MACRO thermo_parcel_grib_inversion_cold.mv
    RESOURCES ../data/thermo_profile_east.grib
    POST_INSTALL)

metview_macro_test(MACRO thermo_parcel_grib_inversion_unstable.mv
    RESOURCES ../data/thermo_profile_baltic.grib
    POST_INSTALL)

metview_macro_test(MACRO thermo_parcel_grib_sahara.mv
    RESOURCES ../data/thermo_profile.grib
    POST_INSTALL)

metview_macro_test(MACRO thermo_parcel_grib_unstable.mv
    RESOURCES ../data/thermo_profile_east.grib
    ../data/thermo_profile_pl_east.grib
    POST_INSTALL)

metview_macro_test(MACRO thermo_parcel_grib_warm_atl.mv
    RESOURCES ../data/thermo_profile_east.grib
    POST_INSTALL)

metview_macro_test(MACRO thermo_parcel_bufr.mv
    RESOURCES ../data/thermo_profile_obs.bufr
    POST_INSTALL)

metview_macro_test(MACRO thermo_parcel_vector.mv
    POST_INSTALL)

metview_macro_test(MACRO xsection_ml_scalar.mv
    RESOURCES ../data/t_lnsp_ml137.grb
    ../data/t_lnsp_ml137_xs_ref.nc
    ../data/xs_ml_input.grib
    POST_INSTALL)

metview_macro_test(MACRO xsection_ml_wind.mv
    RESOURCES ../data/xs_ml_input.grib
    POST_INSTALL)

metview_macro_test(MACRO xsection_ml_ghbc.mv
    RESOURCES ../data/xs_ml_input.grib
    ../data/xs_tz_ml.grib
    ../data/xs_ml_ghbc_wind_2d_intensity.nc
    ../data/xs_ml_ghbc_wind_3d_intensity_1.nc
    ../data/xs_ml_ghbc_wind_3d_intensity_2.nc
    ../data/xs_ml_ghbc_wind_3d_parallel_1.nc
    ../data/xs_ml_ghbc_wind_3d_parallel_2.nc
    ../data/xs_ml_ghbc_from_data_ref.nc
    ../data/xs_ml_ghbc_adaptive_ref.nc
    ../data/xs_ml_ghbc_adaptive_extra_linear_ref.nc
    ../data/xs_ml_ghbc_adaptive_extra_const_ref.nc
    ../data/xs_ml_ghbc_from_data_extra_linear_ref.nc
    ../data/xs_ml_ghbc_from_data_extra_constant_ref.nc
    POST_INSTALL)


metview_macro_test(MACRO xsection_ml_orog.mv
    RESOURCES ../data/t_lnsp_ml137.grb
    ../data/t_lnsp_ml137_xs_ref.nc
    POST_INSTALL)


metview_macro_test(MACRO xsection_ml_curve.mv
    RESOURCES ../data/t_lnsp_ml137.grb
    ../data/t_lnsp_ml137_xs_ref.nc
    POST_INSTALL)

metview_macro_test(MACRO xsection_pl.mv
    RESOURCES ../data/xs_pl_input.grib
    ../data/xs_wind_3d_w_default_pl_ref.nc
    ../data/tuv_pl.grib)

metview_macro_test(MACRO xsection_hl.mv
    RESOURCES ../data/hl_data.grib
    ../data/xs_hl_level_count_ref.nc
    ../data/xs_hl_level_list_ref.nc
    ../data/xs_hl_inter_ref.nc
    ../data/xs_hl_nearest_ref.nc
    ../data/xs_hl_ghbc_from_data_ref.nc
    ../data/xs_hl_ghbc_adaptive_ref.nc)

metview_macro_test(MACRO xsection_grid.mv
    RESOURCES ../data/xs_pl_input.grib
    ../data/xs_rgg_input.grib
    ../data/xs_rgg_ref.nc
    ../data/xs_rgg_2_ref.nc
    ../data/xs_ll_ref.nc)

metview_macro_test(MACRO average_data.mv
    RESOURCES ../data/tuv_pl.grib)

metview_macro_test(MACRO vertprof.mv
    RESOURCES ../data/t_lnsp_ml137.grb)

metview_macro_test(MACRO hovline.mv
    RESOURCES ../data/t_1000_timeseries.grb
    ../data/hovline_ll_ref_1.nc
    ../data/hovline_ll_ref_2.nc)

metview_macro_test(MACRO hovarea.mv
    RESOURCES ../data/t_1000_timeseries.grb
    ../data/hovarea_ll_ew_mean_ref.nc
    ../data/hovarea_ll_ew_maximum_ref.nc
    ../data/hovarea_ll_ew_median_ref.nc
    ../data/hovarea_ll_ew_minimum_ref.nc
    ../data/hovarea_ll_ew_stdev_ref.nc
    ../data/hovarea_ll_ew_variance_ref.nc
    ../data/hovarea_ll_ns_mean_ref.nc
    ../data/hovarea_ll_ns_maximum_ref.nc
    ../data/hovarea_ll_ns_median_ref.nc
    ../data/hovarea_ll_ns_minimum_ref.nc
    ../data/hovarea_ll_ns_stdev_ref.nc
    ../data/hovarea_ll_ns_variance_ref.nc
    )

metview_macro_test(MACRO hovvert_pl.mv
    RESOURCES ../data/t_timeseries.grb
    ../data/hovvert_pl_ref.nc
    )

metview_macro_test(MACRO hovvert_ml_to_pl.mv
    RESOURCES ../data/hov_ml_input.grib
    ../data/hovvert_ml_pl_ref.nc
    ../data/hovvert_ml_pl_fixed_lnsp_ref.nc
    ../data/hovvert_ml_pl_extra_ref_1.nc
    ../data/hovvert_ml_pl_extra_ref_2.nc
    ../data/hovvert_ml_pl_extra_ref_3.nc
    ../data/hovvert_ml_pl_missing_ref_1.nc
    ../data/hovvert_ml_pl_missing_ref_2.nc
    ../data/hovvert_ml_pl_range_ref.nc
    ../data/hovvert_ml_pl_subset_ref.nc
    )

metview_macro_test(MACRO hovvert_ml_to_hl.mv
    RESOURCES ../data/hov_ml_input.grib
    ../data/hovvert_ml_hl_ref.nc
    ../data/hovvert_ml_hl_extra_ref_1.nc
    ../data/hovvert_ml_hl_extra_ref_2.nc
    ../data/hovvert_ml_hl_extra_ref_3.nc)


metview_macro_test(MACRO hovexp.mv
    RESOURCES ../data/t_1000_timeseries.grb
    ../data/hovexp_area_ref_1.nc
    ../data/hovexp_line_ref_1.nc
    ../data/hovexp_vert_area.nc
    ../data/hovexp_vert_nearest_gridpoint.nc
    ../data/hovexp_vert_point.nc
    )

metview_macro_test(MACRO memory.mv
    RESOURCES ../data/t1000_LL_2x2.grb
    ../data/tuv_pl.grib
    ../data/hindcast.grib
    ../data/example_XYLDTZ.geo)

metview_macro_test(MACRO request.mv
    RESOURCES ../data/request_empty.req
    ../data/request_one_message.req
    ../data/request_two_messages.req
    ../data/mcont.req)

metview_macro_test(MACRO request_with_data.mv
    RESOURCES set_output.mv
    ../data/xs_pl_input.grib)

metview_macro_test(MACRO file.mv
    RESOURCES ../data/z500.grb
    ../data/fc_data.grib
    ../data/bufr_obs_filter_area_reference.gpt
    ../data/example_POLAR_VECTOR.geo
    ../data/example_XY_VECTOR.geo
    ../data/example_XYLDTZ.geo
    ../data/geo_ncols_1.gpt
    ../data/temp_small.bufr
    ../data/z500.nc
    ../data/table1.csv)

metview_macro_test(MACRO uv_grid.mv
    RESOURCES ../data/uv_spectral.grib
    ../data/uv_grid_reference.grib)

if(HAVE_PPROC_MIR)
    metview_macro_test(MACRO regrid.mv
        RESOURCES ../data/rgg_small_subarea.grib
        ../data/tuv_pl.grib
        ../data/uv_spectral.grib
        ../data/vorticity_divergence_500_sh.grb
        ../data/t1000_LL_2x2.grb
        ../data/z_for_spectra.grib
        ../data/globalise_input.grib
        ../data/z500.grb
        POST_INSTALL)
endif()

if(ENABLE_MARS)
    metview_macro_test(MACRO mars_retrieve_default.mv
        POST_INSTALL)
    metview_macro_test(MACRO mars_retrieve_uv.mv
        POST_INSTALL)
endif()

if(METVIEW_ODB)
    metview_macro_test(MACRO odb_read.mv
        RESOURCES ../data/odb_test_set.odb)

    metview_macro_test(MACRO odb_filter.mv
        RESOURCES ../data/odb_test_set.odb
        POST_INSTALL)

    if(ENABLE_PLOTTING)
        metview_macro_test(MACRO odb_visualiser.mv
            RESOURCES ../data/odb_test_set.odb
            POST_INSTALL)
    endif()
endif()

if(ENABLE_METEOGRAM)
    metview_macro_test(MACRO meteogram.mv
        POST_INSTALL)
endif()

if(ENABLE_STVL)
    metview_macro_test(MACRO stvl.mv)
endif()

if(ENABLE_FLEXTRA_TEST)
    metview_macro_test(MACRO flextra_run.mv)
endif()

metview_macro_test(MACRO flextra_output.mv
        RESOURCES ../data/flextra_res_normal.txt
        ../data/flextra_res_multi.txt)

if(ENABLE_FLEXPART_TEST)
    metview_macro_test(MACRO flexpart_run.mv)
endif()

metview_macro_test(MACRO flexpart_output.mv
        RESOURCES ../data/flexpart_conc_fwd.grib
        ../data/flexpart_flux_fwd.grib
        ../data/flexpart_bwd.grib)

# finish the post-install test script
file(APPEND ${POST_INSTALL_TEST_SCRIPT} "\n")
file(APPEND ${POST_INSTALL_TEST_SCRIPT} "# if we got this far, then all tests must have succeeded\n")
file(APPEND ${POST_INSTALL_TEST_SCRIPT} "echo \"ALL TESTS PASSED\"\n")
file(APPEND ${POST_INSTALL_TEST_SCRIPT} "echo \"\"\n")

# generate a script which can be used to run the tests on the installed version
set(METVIEW_COMMAND "${CMAKE_INSTALL_PREFIX}/bin/${METVIEW_SCRIPT}")
set(TESTS_DIR "${CMAKE_CURRENT_BINARY_DIR}")

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/metview_post_install_tests.sh
    DESTINATION ${MV_BIN_DIR}
    PERMISSIONS OWNER_WRITE OWNER_READ GROUP_READ WORLD_READ OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE)

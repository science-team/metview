# Metview Macro

#  **************************** LICENSE START ***********************************
# 
#  Copyright 2022 ECMWF. This software is distributed under the terms
#  of the Apache License version 2.0. In applying this license, ECMWF does not
#  waive the privileges and immunities granted to it by virtue of its status as
#  an Intergovernmental Organization or submit itself to any jurisdiction.
# 
#  ***************************** LICENSE END ************************************

include "test_utils.mv"
include "hov_utils.mv"

# model level to height (param)
test_scalar_ml_to_hl()
test_scalar_ml_to_hl_extrapolate()


function test_scalar_ml_to_hl()

    fn_name = "scalar_ml_to_hl"
    
    # read input grib data
    data = read("hov_ml_input.grib")
    t = read(data: data, param: "t")
    z = read(data: data, param: "z")/9.81
    lnsp = read(data: data, param: "lnsp")
    
    # read reference data
    nc_ref = read("hovvert_ml_hl_ref.nc")

    lat = 30
    lon = -60
    delta = 1

    # -----------------------------
    # 1. original field ordering
    # -----------------------------
    
    # compute hovmoeller and save it in a netcdf file
    nc = mhovmoeller_vertical(
        area                : [lat-delta,lon-delta,lat+delta,lon+delta],
        vertical_level_type : "user",
        vertical_coordinate_param: 129,
        data                : t & z,
        top_level: 50000,
        bottom_level: 0
        )
    
    # variables
    vars_ref = ["time", "vertical", "t"]
    vars = variables(nc)
    assert_equal(vars, vars_ref,fn_name & ": vars",1)

    # global attributes
    _check_hov_global_attr_vert(fn_name, 1, nc, 
                "AREA", "  31.00/ -61.00/  29.00/ -59.00", "USER", "MEAN")

    _check_hov_data(nc, nc_ref, "vertical", 126, 3, 0, fn_name, 1)
    
    # -----------------------------------
    # 2. Change order of fields in input
    # -----------------------------------
    
    data1 = sort(data, ["levelist", "step"], [">", ">"]) + 0
    
    # compute hovmoeller and save it in a netcdf file
    nc = mhovmoeller_vertical(
        area                : [lat-delta,lon-delta,lat+delta,lon+delta],
        vertical_level_type : "user",
        vertical_coordinate_param: 129,
        data                : t & z,
        top_level: 50000,
        bottom_level: 0
        )
      
    # global attributes
    _check_hov_global_attr_vert(fn_name, 2, nc, 
                "AREA", "  31.00/ -61.00/  29.00/ -59.00", "USER", "MEAN")

    _check_hov_data(nc, nc_ref, "vertical", 126, 3, 0, fn_name, 2)
        
end test_scalar_ml_to_hl

function test_scalar_ml_to_hl_extrapolate()

    fn_name = "scalar_ml_to_hl_extrapolate"
    
    # read input grib data
    data = read("hov_ml_input.grib")
    t = read(data: data, param: "t")
    z = read(data: data, param: "z")/9.81
    lnsp = read(data: data, param: "lnsp")
    
    # read reference data
    nc_ref = read("hovvert_ml_hl_extra_ref_1.nc")

    lat = 30
    lon = 60
    delta = 1

    # -----------------------------
    # 1. No extrapolation
    # -----------------------------
    
    # compute hovmoeller and save it in a netcdf file
    nc = mhovmoeller_vertical(
        area                : [lat-delta,lon-delta,lat+delta,lon+delta],
        vertical_level_type : "user",
        vertical_coordinate_param: 129,
        data                : t & z,
        vertical_coordinate_extrapolate: "off",
        top_level: 20000,
        bottom_level: 0
        )
   
    # variables
    vars_ref = ["time", "vertical", "t"]
    vars = variables(nc)
    assert_equal(vars, vars_ref,fn_name & ": vars",1)

    # global attributes
    _check_hov_global_attr_vert(fn_name, 1, nc, 
                "AREA", "  31.00/  59.00/  29.00/  61.00", "USER", "MEAN")

    _check_hov_data(nc, nc_ref, "vertical", 89, 3, 0, fn_name, 1)
    
    # 
#     
#     # global attributes
#     _check_global_attr_area(fn_name, 1, nc, 
#                 "  31.00/  59.00/  29.00/  61.00", "USER", "MEAN")
#              
#     # define variable to be checked
#     setcurrent(nc,"t")
# 
#     # attributes of a variable
#     attr = attributes(nc)
#     assert_equal(count(keywords(attr)),6,fn_name & ": count t attributes",1)
#     assert_equal(attr.long_name,"Temperature",fn_name & ": attribute t long_name",1)
# 
#     _check_hovvert(nc, nc_ref, 89, fn_name, 1)
   
    # ------------------------------------
    # 2. Extrapolate: linear, top+bottom
    # ------------------------------------
    
    # read reference data
    nc_ref = read("hovvert_ml_hl_extra_ref_2.nc")
    
    # compute hovmoeller and save it in a netcdf file
    nc = mhovmoeller_vertical(
        area                : [lat-delta,lon-delta,lat+delta,lon+delta],
        vertical_level_type : "user",
        vertical_coordinate_param: 129,
        data                : t & z,
        vertical_coordinate_extrapolate: "on",
        vertical_coordinate_extrapolate_mode: "linear",
        vertical_coordinate_extrapolate_fixed_sign: "on",
        top_level: 100000,
        bottom_level: 0
        )
    
    # variables
    vars_ref = ["time", "vertical", "t"]
    vars = variables(nc)
    assert_equal(vars, vars_ref,fn_name & ": vars",2)

    # global attributes
    _check_hov_global_attr_vert(fn_name, 2, nc, 
                "AREA", "  31.00/  59.00/  29.00/  61.00", "USER", "MEAN")

    _check_hov_data(nc, nc_ref, "vertical", 139, 3, 0, fn_name, 2)
    
    # ------------------------------------
    # 3. Extrapolate: constant, top+bottom
    # ------------------------------------
    
    # read reference data
    nc_ref = read("hovvert_ml_hl_extra_ref_3.nc")
    
    # compute hovmoeller and save it in a netcdf file
    nc = mhovmoeller_vertical(
        area                : [lat-delta,lon-delta,lat+delta,lon+delta],
        vertical_level_type : "user",
        vertical_coordinate_param: 129,
        data                : t & z,
        vertical_coordinate_extrapolate: "on",
        vertical_coordinate_extrapolate_mode: "constant",
        top_level: 100000,
        bottom_level: 0
        )
   
    # variables
    vars_ref = ["time", "vertical", "t"]
    vars = variables(nc)
    assert_equal(vars, vars_ref,fn_name & ": vars",3)

    # global attributes
    _check_hov_global_attr_vert(fn_name, 3, nc, 
                "AREA", "  31.00/  59.00/  29.00/  61.00", "USER", "MEAN")

    _check_hov_data(nc, nc_ref, "vertical", 139, 3, 0, fn_name, 3)
    
end test_scalar_ml_to_hl_extrapolate

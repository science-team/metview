# Metview Macro

#  **************************** LICENSE START ***********************************
# 
#  Copyright 2016 ECMWF. This software is distributed under the terms
#  of the Apache License version 2.0. In applying this license, ECMWF does not
#  waive the privileges and immunities granted to it by virtue of its status as
#  an Intergovernmental Organization or submit itself to any jurisdiction.
# 
#  ***************************** LICENSE END ************************************

include "test_utils.mv"
include "hov_utils.mv"

test_ll_default()
test_ll_ew()
test_ll_ns()

function test_ll_default()

    statsLst = ["mean", "minimum", "maximum", "stdev", "variance", "median"]

    fn_name = "ll_ew_default"
    
    # read input grib data
    data = read("t_1000_timeseries.grb")

    # read reference data
    nc_ref = read("hovarea_ll_ew_mean_ref.nc")
    
    # compute hovmoeller and save it in a netcdf file
    nc = mhovmoeller_area(
        area : [33,-27,75,45],
        data : data
    )
   
    # variables
    vars_ref = ["time", "lat", "t"]
    vars = variables(nc)
    assert_equal(vars, vars_ref,fn_name & ": vars",1)
    
    # global attributes
    _check_hov_global_attr_area(fn_name, 1, nc, 
        "EAST_WEST", "  75.00/ -27.00/  33.00/  45.00", 1, "MEAN")
   
    _check_hov_data(nc, nc_ref, "lat", 43, 5, 0, fn_name, 1)
    
end test_ll_default    
    
    
function test_ll_ew()

    statsLst = ["mean", "minimum", "maximum", "stdev", "variance", "median"]

    fn_name = "ll_ew"
    
    # read input grib data
    data = read("t_1000_timeseries.grb")

    for i=1 to count(statsLst) do

        # read reference data
        nc_ref = read("hovarea_ll_ew_" & statsLst[i] & "_ref.nc")
        #_plot_hov_nc(nc_ref)
        #plot(mhovmoellerview(type: "area_hovm", area: [75,-27,33,45]), nc_ref)
        #stop
    
        # compute hovmoeller and save it in a netcdf file
        nc = mhovmoeller_area(
            area : [33,-27,75,45],
            data : data,
            area_statistics: statsLst[i]
        )
        #write("hovarea_ll_ew_" & statsLst[i] & "_ref.nc", nc)
     
        # variables
        vars_ref = ["time", "lat", "t"]
        vars = variables(nc)
        assert_equal(vars, vars_ref,fn_name & ": vars",i)
    
        # global attributes
        _check_hov_global_attr_area(fn_name, i, nc, 
            "EAST_WEST", "  75.00/ -27.00/  33.00/  45.00", 1, uppercase(statsLst[i]))
        
        _check_hov_data(nc, nc_ref, "lat", 43, 5, 0, fn_name, i)
        
    end for
    
end test_ll_ew

function test_ll_ns()
    
    statsLst = ["mean", "minimum", "maximum", "stdev", "variance", "median"]

    fn_name = "ll_ns"
    
    # read input grib data
    data = read("t_1000_timeseries.grb")

    for i=1 to count(statsLst) do

        # read reference data
        nc_ref = read("hovarea_ll_ns_" & statsLst[i] & "_ref.nc")
        #_plot_hov_nc("lon", -27, 45, [nc_ref])
        #plot(mhovmoellerview(type: "area_hovm", area: [75,-27,33,45]), nc_ref)
        #stop
    
        # compute hovmoeller and save it in a netcdf file
        nc = mhovmoeller_area(
            area : [33,-27,75,45],
            data : data,
            area_statistics: statsLst[i],
            average_direction: "north_south"
        )
        #write("hovarea_ll_ns_" & statsLst[i] & "_ref.nc", nc)
   
        # variables
        vars_ref = ["time", "lon", "t"]
        vars = variables(nc)
        assert_equal(vars, vars_ref,fn_name & ": vars",i)
    
        # global attributes
         _check_hov_global_attr_area(fn_name, i, nc, 
            "NORTH_SOUTH", "  75.00/ -27.00/  33.00/  45.00", 0, uppercase(statsLst[i]))
         
        _check_hov_data(nc, nc_ref, "lon", 73, 5, 1, fn_name, i)
   
    end for
   
end test_ll_ns

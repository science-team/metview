#Metview Macro

#  **************************** LICENSE START ***********************************
#
#  Copyright 2019 ECMWF. This software is distributed under the terms
#  of the Apache License version 2.0. In applying this license, ECMWF does not
#  waive the privileges and immunities granted to it by virtue of its status as
#  an Intergovernmental Organization or submit itself to any jurisdiction.
#
#  ***************************** LICENSE END ************************************
#

include "test_utils.mv"
 
test_theta_field()


global ASSERT_EQUAL_EPS = 1E-4 # wil be used by assert_equal()

function test_theta_field()

    # ------------------
    # Pressure levels
    # ------------------

    f = read("theta_input_pl.grib")
    t = read(data:f, param: "t")
    q = read(data:f, param: "q")
    rhu = read(data:f, param: "r")
    
    # pott_p
    r=pott_p(temperature: t)
    assert_equal(count(r), count(t), "theta_pl count",1)
    
    pl =[850, 700]
    c_kappa=0.285491218
    for i=1 to count(pl) do
        
        lev = grib_get_long(r[i],"level")
        assert_equal(lev,pl[i],"theta_pl level [" & i & "]",1)
        id = grib_get_long(r[i],"paramId")
        assert_equal(id,3,"theta_pl paramId [" & i & "]",1)
             
        v = values(r[i])
        pv = pl[i]*100
        th = t[i]*(100000/pv)^c_kappa
        assert(vec_same(v, values(th),1E-3),"theta_pl values [" & i & "]",1)
    end for    

    f_ref = read("theta_pl_ref.grib")
    f_eq_ref  = f_ref[1,2]
    f_eq_rhu_ref = f_ref[3,4]
    f_seq_ref = f_ref[5,6]
    
    # eqpott_p
    r=eqpott_p(temperature: t, humidity: q)
    assert_equal(count(r), count(t), "theta_eq_pl count",1)
   
    for i=1 to count(pl) do
        
        lev = grib_get_long(r[i],"level")
        assert_equal(lev,pl[i],"theta_eq_pl level [" & i & "]",1)
        id = grib_get_long(r[i],"paramId")
        assert_equal(id,4,"theta_eq_pl paramId [" & i & "]",1)
        
        v = values(r[i])       
        th = f_eq_ref[i]
        assert(vec_same(v, values(th),1E-3),"theta_eq_pl values [" & i & "]",1)
    end for    
     
    # eqpott_p with relhum
    r=eqpott_p(temperature: t, humidity: rhu)
    assert_equal(count(r), count(t), "theta_eq_pl count",2)
   
    for i=1 to count(pl) do
        
        lev = grib_get_long(r[i],"level")
        assert_equal(lev,pl[i],"theta_eq_pl level [" & i & "]",2)
        id = grib_get_long(r[i],"paramId")
        assert_equal(id,4,"theta_eq_pl paramId [" & i & "]",2)
        
        v = values(r[i])       
        th = f_eq_rhu_ref[i]
        assert(vec_same(v, values(th),1E-3),"theta_eq_pl values [" & i & "]",2)
    end for
    
    # seqpott_p
    r=seqpott_p(temperature: t)
    assert_equal(count(r), count(t), "theta_sqe_pl count",1)
   
    for i=1 to count(pl) do
        lev = grib_get_long(r[i],"level")
        assert_equal(lev,pl[i],"theta_seq_pl level [" & i & "]",1)
        id = grib_get_long(r[i],"paramId")
        assert_equal(id,5,"theta_seq_pl paramId [" & i & "]",1)
        
        v = values(r[i])       
        th = f_seq_ref[i]
        assert(vec_same(v, values(th),0.005),"theta_seq_pl values [" & i & "]",1)
    end for    
    
    # ------------------
    # Model levels
    # ------------------

    ml = [135, 110]
    f = read("thermo_profile.grib")
    t = read(data:f, param: "t", levelist: ml)
    q = read(data:f, param: "q", levelist: ml)
    lnsp = read(data:f, param: "lnsp")
    rhu = relhum(data: lnsp & t & q)
    
    f_ref = read("theta_ml_ref.grib")
    f_th_ref  = f_ref[1,2]
    f_eq_ref  = f_ref[3,4]
    f_eq_rhu_ref  = f_ref[5,6]
    f_seq_ref = f_ref[7,8]
    
    # pott_m
    r=pott_m(temperature: t, lnsp: lnsp)
    assert_equal(count(r), count(t), "theta_ml count",1)
   
    for i=1 to count(r) do
        
        lev = grib_get_long(r[i],"level")
        assert_equal(lev,ml[i],"theta_ml level [" & i & "]",1)
        id = grib_get_long(r[i],"paramId")
        assert_equal(id,3,"theta_ml paramId [" & i & "]",1)
             
        v = values(r[i])
        v_ref = values(f_th_ref[i])
        assert(vec_same(v, v_ref,1E-3),"theta_ml values [" & i & "]",1)
    end for    

    # eqpott_m
    r=eqpott_m(temperature: t, humidity: q, lnsp: lnsp)
    assert_equal(count(r), count(t), "theta_eq_ml count",1)
   
    for i=1 to count(r) do
        
        lev = grib_get_long(r[i],"level")
        assert_equal(lev,ml[i],"theta_eq_ml level [" & i & "]",1)
        id = grib_get_long(r[i],"paramId")
        assert_equal(id,4,"theta_eq_ml paramId [" & i & "]",1)
        
        v = values(r[i])       
        v_ref = values(f_eq_ref[i]) 
        assert(vec_same(v, v_ref ,1E-3),"theta_eq_ml values [" & i & "]",1)
    end for    
    
    # eqpott_m rhu
    r=eqpott_m(temperature: t, humidity: rhu, lnsp: lnsp)
    assert_equal(count(r), count(t), "theta_eq_ml count",2)
   
    for i=1 to count(r) do
        
        lev = grib_get_long(r[i],"level")
        assert_equal(lev,ml[i],"theta_eq_ml level [" & i & "]",2)
        id = grib_get_long(r[i],"paramId")
        assert_equal(id,4,"theta_eq_ml paramId [" & i & "]",2)
        
        v = values(r[i])       
        v_ref = values(f_eq_rhu_ref[i]) 
        assert(vec_same(v, v_ref ,1E-3),"theta_eq_ml values [" & i & "]",2)
    end for  
    
    # seqpott_m
    r=seqpott_m(temperature: t, lnsp: lnsp)
    assert_equal(count(r), count(t), "theta_seq_ml count",1)
   
    for i=1 to count(r) do
        lev = grib_get_long(r[i],"level")
        assert_equal(lev,ml[i],"theta_seq_ml level [" & i & "]",1)
        id = grib_get_long(r[i],"paramId")
        assert_equal(id,5,"theta_seq_ml paramId [" & i & "]",1)
        
        v = values(r[i])       
        v_ref = values(f_seq_ref[i]) 
        assert(vec_same(v, v_ref,1E-3),"theta_seq_ml values [" & i & "]",1)
    end for        
    
end test_theta_field

#====================================================
#
# Functions to generate test reference data
#
#====================================================

function generate_test_theta_pl_field_reference()

    f = read("theta_input_pl.grib")
    t = read(data:f, param: "t")
    q = read(data:f, param: "q")
    
    r1=eqpott_p(temperature: t, humidity: q)
    r2=seqpott_p(temperature: t)
    
    write("theta_pl_ref.grib",r1 & r2)
 
    
end generate_test_theta_pl_field_reference

function generate_test_theta_ml_field_reference()

    lev = [135, 110]
    f = read("thermo_profile.grib")
    t = read(data:f, param: "t", levelist: lev)
    q = read(data:f, param: "q", levelist: lev)
    lnsp = read(data:f, param: "lnsp")
    
    # visually checked
    r1=pott_m(temperature: t, lnsp: lnsp)
    r2=eqpott_m(temperature: t, humidity: q, lnsp: lnsp)
    r3=seqpott_m(temperature: t, lnsp: lnsp)
    
    write("theta_ml_ref.grib",r1 & r2 & r3)
 
end generate_test_theta_ml_field_reference

# Metview Macro

# **************************** LICENSE START ***********************************
#
# Copyright 2017 ECMWF. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

include "test_utils.mv"

test_grib_get()
test_grib_set()
test_grib_set_step()
test_repack()

function test_grib_get()

    # read the original data file
    data  = read('fc_data.grib')
    d1 = data[1]

    data_rgg = read('xs_rgg_input.grib')
    d_rgg1 = data_rgg[1]

    # read some keys whose values we know
    ip = grib_get_long(d1, 'indicatorOfParameter')
    assert_equal(ip, 129, 'indicatorOfParameter')

    cl = grib_get_long(d1, 'centre')
    assert_equal(cl, 98, 'centre long')

    cs = grib_get_string(d1, 'centre')
    assert_equal(cs, 'ecmf', 'centre string')

    lat = grib_get_double(d1, 'latitudeOfFirstGridPointInDegrees')
    assert_equal(lat, 80, 'lat double')

    steps = grib_get_long(data, 'step')
    assert_equal(steps, [0,6,12,120], 'all steps')

    pl = grib_get_long_array(d_rgg1, 'pl')
    expected_pl = |20,25,36,40,45,50,60,60,72,75,80,90,96,100,108,
                   120,120,120,128,135,144,144,160,160,160,160,160,
                   180,180,180,180,180,192,192,192,192,192,192,192,
                   192,192,192,192,192,192,192,192,192,192,192,192,
                   192,192,192,192,192,192,192,192,192,192,192,192,
                   192,180,180,180,180,180,160,160,160,160,160,144,
                   144,135,128,120,120,120,108,100,96,90,80,75,72,
                   60,60,50,45,40,36,25,20|
    assert(vec_same(pl, expected_pl))

    # read keys that do not exist
    nolong = grib_get_long(d1, 'doesnotexist')
    assert_equal(nolong, nil, 'doesnotexist long')

    nodouble = grib_get_double(d1, 'doesnotexist')
    assert_equal(nodouble, nil, 'doesnotexist double')

    nostring = grib_get_string(d1, 'doesnotexist')
    assert_equal(nostring, nil, 'doesnotexist string')

    nolongarray = grib_get_long_array(d1, 'doesnotexist')
    assert_equal(nolongarray, nil, 'doesnotexist long array')

    nodoublearray = grib_get_double_array(d1, 'doesnotexist')
    assert_equal(nodoublearray, nil, 'doesnotexist double array')

    nolongall = grib_get_long(data, 'doesnotexist')
    assert_equal(nolongall, [nil, nil, nil, nil], 'doesnotexist long all')

    # grib_get
    r = grib_get(data,['centre','centre:l','notakey','indicatorOfParameter','step'])
    v_ref = [["ecmf",98,nil,"z","0"],
             ["ecmf",98,nil,"z","6"],
             ["ecmf",98,nil,"z","12"],
             ["ecmf",98,nil,"z","120"]]
           
    assert_equal(count(r),count(v_ref),"grib_get count",1)
    for i=1 to count(v_ref) do   
        assert_equal(r[i],v_ref[i],"grib_get +" & i,1)  
    end for           
                
    r = grib_get(data,['centre','centre:l','notakey','indicatorOfParameter','step'],'field')
    v_ref = [["ecmf",98,nil,"z","0"],
             ["ecmf",98,nil,"z","6"],
             ["ecmf",98,nil,"z","12"],
             ["ecmf",98,nil,"z","120"]]
           
    assert_equal(count(r),count(v_ref),"grib_get count",2)
    for i=1 to count(v_ref) do   
        assert_equal(r[i],v_ref[i],"grib_get +" & i,2)  
    end for           
                 
    r = grib_get(data,['centre','centre:l','notakey','indicatorOfParameter','step'],'key')
    v_ref = [["ecmf","ecmf","ecmf","ecmf"],
              [98,98,98,98],
              [nil,nil,nil,nil],
              ["z","z","z","z"],
              ["0","6","12","120"]]
           
    assert_equal(count(r),count(v_ref),"grib_get count",3)
    for i=1 to count(v_ref) do   
        assert_equal(r[i],v_ref[i],"grib_get +" & i,3)  
    end for 

end test_grib_get

function test_grib_set()

    data  = read('fc_data.grib')
    d1 = data[1]

    data_rgg = read('xs_rgg_input.grib')
    d_rgg1 = data_rgg[1]

    #grib set  
    r = grib_set_double(d1, ['latitudeOfFirstGridPointInDegrees',82.5])
    lat = grib_get_double(r, 'latitudeOfFirstGridPointInDegrees')
    assert_equal(lat, 82.5, 'grib_set_double',1)

    r = grib_set_long(data[2], ['step',24])
    step = grib_get_long(r, 'step')
    assert_equal(step, 24, 'grib_set_long',1)

    r = grib_set_string(data[2], ['unitOfTimeRange','D'])
    utr = grib_get_string(r, 'unitOfTimeRange')
    assert_equal(utr, 'D','grib_set_string',1)

    r = grib_set(d1, ['latitudeOfFirstGridPointInDegrees',82.5, 'step',24])
    lat = grib_get_double(r, 'latitudeOfFirstGridPointInDegrees')
    step = grib_get_long(r, 'step')
    assert_equal(lat, 82.5, 'grib_set lat',1)
    assert_equal(step, 24, 'grib_set step',1)

    new_pl = |18,35,84,40,45,50,60,60,72,75,80,90,96,100,108,
              120,120,120,128,135,144,144,160,160,160,160,160,
              180,180,180,180,180,192,192,192,192,192,192,192,
              192,192,192,192,192,192,192,192,192,192,192,192,
              192,192,192,192,192,192,192,192,192,192,192,192,
              192,180,180,180,180,180,160,160,160,160,160,144,
              144,135,128,120,120,120,108,100,96,90,80,75,72,
              60,60,50,45,40,36,27,19|
    r = grib_set_long_array(d_rgg1, ['pl', new_pl])
    pl = grib_get_long_array(r, 'pl')
    assert(vec_same(pl, new_pl))

end test_grib_set

function test_grib_set_step()

    data  = read('fc_data.grib')
    f = data[1]
    
    r = grib_get(f,[ "date", "time", "stepType","startStep", "endStep", "unitOfTimeRange"])
    r_ref = ["20160415","1200","instant","0","0","h"]
    assert_equal(r[1], r_ref, "grib_set_date before",1)
   
    f = grib_set(f,
        ["date", 20150601,    
        "time", 0600,         
        "stepType", "avg", 
        #"unitOfTimeRange", "D",    
        "startStep", 0 ,       
        "endStep", 31        
     ])
     
     r = grib_get(f,[ "date", "time", "stepType","startStep", "endStep", "unitOfTimeRange"])
     r_ref = ["20150601","0600","avg","0","31","h"]
     assert_equal(r[1], r_ref, "grib_set_date after",1)
     
end test_grib_set_step   


function test_repack()

    fs = read("lnsp.grib")
    f = fs[1]
    v_min_ref = 11.1920347214
    v_max_ref = 11.5454053879
  
    # when we do not use "repack" min=0, max=46317 and totalLength=4146
    
    # macro syntax
    r1 = grib_set(f, ["packingType", "grid_second_order"], "repack")
    
    # python syntax
    r2 = grib_set(f, ["packingType", "grid_second_order"], "repack", 1)
    
    #precision(16)
    
    rLst = [r1, r2] 
    for i=1 to count(rLst) do
        r = rLst[i]
        assert_equal(count(r), 1, 'repack count', i)
        v_min = grib_get_double(r, "min")
        v_max = grib_get_double(r, "max")
        tl = grib_get_long(r, "totalLength")
        assert_equal(v_min, v_min_ref, 1E-6, 'repack min',i)
        assert_equal(v_max, v_max_ref, 1E-6, 'repack max',i)
        assert_equal(tl, 5861, 'repack totalLength',i)
    end for
       
end test_repack    


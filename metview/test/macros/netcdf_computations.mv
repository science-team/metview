# Metview Macro

#  **************************** LICENSE START ***********************************
# 
#  Copyright 2016 ECMWF. This software is distributed under the terms
#  of the Apache License version 2.0. In applying this license, ECMWF does not
#  waive the privileges and immunities granted to it by virtue of its status as
#  an Intergovernmental Organization or submit itself to any jurisdiction.
# 
#  ***************************** LICENSE END ************************************

include "test_utils.mv"

ASSERT_EQUAL_EPS = 0.000001

# read our sample netCDF file and get its values
xs = read("xs_with_missing_vals.nc")
setcurrent(xs, 't')
v = values(xs)
n = count(v)

# check we can read the original values ok
assert_equal(n, 931,'count values')
assert_equal(v[1], vector_missing_value, 'v_1 missing')
assert_equal(v[18], 249.377731548334, 'v_18 val')
assert_equal(v[n], 251.891082763672, 'v_n val')

# same check, but with missing values not preserved
netcdf_preserve_missing_values(0)
v = values(xs)
n = count(v)
assert_equal(n, 931, 'count values')
assert_equal(v[1],  1e+22,                'vnm_1 missing')
assert_equal(v[18], 249.377731548334,     'vnm_18 val')
assert_equal(v[n],  251.891082763672,     'vnm_n val')
netcdf_preserve_missing_values(1)


# unary operator
xs2 = neg(xs)
v2  = values(xs2)
assert_equal(v2[1],  vector_missing_value, 'v2_1 missing')
assert_equal(v2[18], -249.377731548334,    'v2_18 val')
assert_equal(v2[n],  -251.891082763672,    'v2_n val')
assert_equal(value(xs2,1),  vector_missing_value, 'v2_1s missing') # single value
assert_equal(value(xs2,18), -249.377731548334,    'v2_18s val')    # single value
assert_equal(value(xs2,n),  -251.891082763672,    'v2_ns val')     # single value


# num/netCDF operator
xs3 = xs + 20
v3  = values(xs3)
assert_equal(v3[1],  vector_missing_value, 'v3_1 missing')
assert_equal(v3[18], 269.377731548334,     'v3_18 val')
assert_equal(v3[n],  271.891082763672,     'v3_n val')

nc = xs * 2
v3  = values(nc)
assert_equal(v3[1],  vector_missing_value, 'v3*_1 missing')
assert_equal(v3[18], 498.755463097, 'v3*_18 val')
assert_equal(v3[n],  503.782165527, 'v3*_n val')


# netCDF/netCDF operator
xs4 = xs3 - xs
v4  = values(xs4)
assert_equal(v4[1],  vector_missing_value, 'v4_1 missing')
assert_equal(v4[18], 20,                   'v4_18 val')
assert_equal(v4[n],  20,                   'v4_n val')
assert_equal(minvalue(v4), 20,             'v4_minvalue')
assert_equal(maxvalue(v4), 20,             'v4_maxvalue')

xs4 = xs3 / xs
v4  = values(xs4)
assert_equal(v4[1],  vector_missing_value, 'v4/_1 missing')
assert_equal(v4[18], 1.08019962278, 'v4/_n val')


# another check - re-read our original file and make sure it has not been altered
xs = nil
xs9 = read("xs_with_missing_vals.nc")
setcurrent(xs9, 't')
v9 = values(xs9)
n = count(v)
assert_equal(n, 931, 'count values')
assert_equal(v9[1],  vector_missing_value, 'v9_1 missing')
assert_equal(v9[18], 249.377731548334,     'v9_18 val')
assert_equal(v9[n],  251.891082763672,     'v9_n val')


# computations between netCDF files with and without missing values
# first - LHS has missing values, RHS does not
xs_nomiss = read("xs_without_missing_vals.nc")
setcurrent(xs_nomiss, 't')

xs_diff1 = xs9 - xs_nomiss
vd1 = values(xs_diff1)
n = count(vd1)
assert_equal(n, 931, 'count values')
assert_equal(vd1[1],  vector_missing_value, 'vd1_1 missing')
assert_equal(vd1[18], 5.92851304103021,     'vd1_18 val')
assert_equal(vd1[n], -0.398426757812501 ,   'vd1_n val')


# second - RHS has missing values, LHS does not
xs_diff2 = xs_nomiss - xs9
vd2 = values(xs_diff2)
n = count(vd2)
assert_equal(n, 931, 'count values')
assert_equal(vd2[1],  vector_missing_value, 'vd2_1 missing')
assert_equal(vd2[18],-5.92851304103021,     'vd2_18 val')
assert_equal(vd2[n],  0.398426757812501 ,   'vd2_n val')



# open a netCDF file without preserving missing values, then change the behaviour
xs  = 0  # release previous references to the netCDF file to avoid warning message
xs9 = 0  # release previous references to the netCDF file to avoid warning message
netcdf_preserve_missing_values(0)
xs1 = read("xs_with_missing_vals.nc")
setcurrent(xs1, 't')
v = values(xs1)
assert_equal(v[1],  1e+22,                'vpm0v_1 missing')
netcdf_preserve_missing_values(1)
v = values(xs1)
assert_equal(v[1],  vector_missing_value, 'vpm1v_1 missing')
xs1 = 0


# do we handle scale_factor and add_offset correctly?
gn = read('nc_from_grib.nc')
setcurrent(gn, 't')
gv = values(gn)
n = count(gv)
assert_equal(n, 1080,                     'gv count values')
assert_equal(gv[1], 273.044658214,        'gv_1 val')
assert_equal(gv[2], vector_missing_value, 'gv_2 missing')
assert_equal(gv[n], 244.940783544,        'gv_n missing')
assert_equal(value(gn,1), 273.044658214,        'gv_1s val')     # single value
assert_equal(value(gn,2), vector_missing_value, 'gv_2s missing') # single value
assert_equal(value(gn,n), 244.940783544,        'gv_ns missing') # single value

# 'null' computation and compare the result - should be the same
gn2 = gn + 0
setcurrent(gn2, 't')
gv2 = values(gn2)
n = count(gv2)
assert_equal(n, 1080,                      'gv2 count values')
assert_equal(gv2[1], 273.044658214,        'gv2_1 val')
assert_equal(gv2[2], vector_missing_value, 'gv2_2 missing')
assert_equal(gv2[n], 244.940783544,        'gv2_n missing')


# check the different auto_scale behaviours
netcdf_auto_scale_values(0)
gn = 0
gn = read('nc_from_grib.nc')
setcurrent(gn, 't')
gv = values(gn)
n = count(gv)
assert_equal(n, 1080,                     'gvns count values')
assert_equal(gv[1], 7069,                 'gvns_1 val')
netcdf_auto_scale_values(1)
gv = values(gn)
assert_equal(gv[1], 273.044658214,        'gvns_2 val')


# check the rescaling capabilities
netcdf_auto_rescale_values_to_fit_packed_type(1)
gn = 0
ncrs = read('nc_from_grib.nc')
setcurrent(ncrs, 't')
v1 = values(ncrs)
ncrsd1 = ncrs / 1
vd1 = values(ncrsd1)
assert_equal(v1[1], vd1[1], 'rs_vd1')
ncrsd1 = 0

ncrsp1 = ncrs + 1
vp1 = values(ncrsp1)
assert_equal(v1[1], vp1[1]-1, 'rs_vp1')

# check that the changes are preserved when written to file
write('ncrsp1.nc', ncrsp1)
ncrsp1 = read('ncrsp1.nc')
setcurrent(ncrsp1, 't')
vp1 = values(ncrsp1)
assert_equal(v1[1], vp1[1]-1, 'rs_vp1')
ncrsp1 = 0

ncrsp1000 = ncrs + 1000
vp1000 = values(ncrsp1000)
assert_equal(v1[1], vp1000[1]-1000, 'rs_vp1000')
ncrsp1000 = 0

ncrsd2 = ncrs / 2
vd2 = values(ncrsd2)
assert_equal(v1[1], vd2[1]*2, 'rs_vd2')
ncrsd2 = 0
ncrs = 0


# check that we can interpret the time variable correctly
netcdf_auto_translate_times(1)
gnt = read('nc_from_grib.nc')
setcurrent(gnt, 'time')
gv = values(gnt)
n = count(gv)
assert_equal(n, 1, 'gnt count values')
assert_equal(gv[1], 2016-08-11 12:00:00,  'gnt_1 val')


netcdf_auto_translate_times(0)
gv = values(gnt)
n = count(gv)
assert_equal(n,             1,       'gntn count values')
assert_equal(gv[1],         1022196, 'gntn_1 vals')
assert_equal(value(gnt, 1), 1022196, 'gntn_1 val') # single value


netcdf_auto_translate_times(1)
xst = read('xs_with_missing_vals.nc')
setcurrent(xst, 'time')
xsv = values(xst)
n = count(xsv)
assert_equal(n, 1,'xst count values')
assert_equal(xsv[1],              2016-08-11 00:00:00, 'xst_1   vals')
assert_equal(value(xst, 1),       2016-08-11 00:00:00, 'xst_1   val') # single value
assert_equal(values(xst, [1])[1], 2016-08-11 00:00:00, 'xst_[1] val') # single value, dim array notation

# Metview 5 changes the format of the date attributes slightly, so test them here
xsmv5 = read('xs_date_mv5.nc')
setcurrent(xsmv5, 'time')
xsv = values(xsmv5)
n = count(xsmv5)
assert_equal(n, 1, 'xsmv5 count values')
assert_equal(xsv[1],                2013-02-06 00:00:00, 'xsmv5_1   vals')
assert_equal(value(xsmv5, 1),       2013-02-06 00:00:00, 'xsmv5_1 xst_1   val') # single value
assert_equal(values(xsmv5, [1])[1], 2013-02-06 00:00:00, 'xsmv5_1 xst_[1] val') # single value, dim array notation


# check that setcurrent(netcdf, index) works
setcurrent(xsmv5, 1)
assert_equal(attributes(xsmv5).long_name, "time", "long_name of xsmv5[1]")
setcurrent(xsmv5, 2)
assert_equal(attributes(xsmv5).long_name, "latitude", "long_name of xsmv5[2]")
setcurrent(xsmv5, 5)
assert_equal(attributes(xsmv5).long_name, "Temperature", "long_name of xsmv5[5]")


# check the extraction of values using indexes
setcurrent(xsmv5, 't')
assert_equal(values(xsmv5, [1, 1, 1])[1], 234.714411279, 'values(xsmv5, [1, 1, 1])')
assert_equal(values(xsmv5, [1, 1, 5])[1], 237.437757375, 'values(xsmv5, [1, 1, 5])')
assert_equal(values(xsmv5, [1, 2, 1])[1], 248.722030086, 'values(xsmv5, [1, 2, 1])')
assert_equal(values(xsmv5, [1, 2, 2])[1], 249.303098469, 'values(xsmv5, [1, 2, 2])')
check_same_vector(values(xsmv5, [1, 1, 'all']), 64, 234.714, 258.979, "values(xsmv5, [1, 1, 'all']")
check_same_vector(values(xsmv5, [1, 'all', 1]), 5, 234.714,  260.484, "values(xsmv5, [1, 'all', 1]")


# check function variables
var_list = variables(xsmv5)
assert_equal(count(var_list), 5, "count variables")
assert_equal(var_list[1], "time", "first variable error")
assert_equal(var_list[count(var_list)], "t", "last variable error")


# check function mod
netcdf_preserve_missing_values(1)
setcurrent(xst, 't')
setcurrent(xs_nomiss, 't')
nc = mod(xst,xs_nomiss)
setcurrent(nc, 't')
v = values(nc)
assert_equal(v[1],  vector_missing_value, 'mod: v_1 missing')
assert_equal(v[18],  6, 'mod: v_18 val')
assert_equal(v[21],  243, 'mod: v_20 val')


# check functions min/max
setcurrent(xst, 't_lev')
nc = min(xst,50)
setcurrent(nc, 't_lev')
check_same_vector(values(nc), 7, 10, 50, 'min function error')

nc = max(xst,50)
check_same_vector(values(nc), 7, 50, 91, 'max function error')


# check function global_attributes
lval = global_attributes(xst)
assert_equal(lval.type, 'MXSECTION', 'global_attributes type')


# check functions dimensions and dimension_names
setcurrent(xst, 't_lev')
lval = dimensions(xst)
assert_equal(lval[1], 1, 'dimensions function: time')
assert_equal(lval[2], 7, 'dimensions function: nlev')

lval = dimension_names(xst)
assert_equal(lval[1], 'time', 'dimension_names function: time')
assert_equal(lval[2], 'nlev', 'dimension_names function: nlev')


# check functions 'and' and 'or'
setcurrent(xs_nomiss, 't')
setcurrent(xst, 't')
nc = xst and xs_nomiss
check_same_vector(values(nc), 931, vector_missing_value, 1, '"and" function')

nc = xst or xs_nomiss
check_same_vector(values(nc), 931, vector_missing_value, 1, '"or" function')

# check different attributes
nc = read('z500.nc')
setcurrent(nc, 'z')
assert_equal(attributes(nc).scale_factor, 0.161463340199893, "scale_factor of nc")
ASSERT_EQUAL_EPS = 0.01
#print(attributes(nc).scale_factor)
#print(attributes(nc).add_offset)
assert_equal(attributes(nc).add_offset, 52807.5572941701,    "add_offset of nc")
assert_equal(attributes(nc).units,     "m**2 s**-2",    "units of nc")
assert_equal(attributes(nc).long_name, "Geopotential",  "long_name of nc")

# check all global attributes
ga = global_attributes(nc)
assert_equal(ga.Conventions, "MARS",  "Conventions of nc")
assert_equal(ga.stream,      "da",    "stream of nc")
assert_equal(ga.date,        20030529,"date of nc")
assert_equal(ga.step,        0,       "step of nc")
assert_equal(ga.expver,      1,       "expver of nc")
assert_equal(ga.class,       "od",    "class of nc")
assert_equal(ga.type,        "an",    "type of nc")
assert_equal(ga.levtype,     "pl",    "levtype of nc")
assert_equal(ga.levelist,    500,     "levelist of nc")
assert_equal(ga.repres,      "ll",    "repres of nc")
assert_equal(ga.domain,      "g",     "domain of nc")


# check that we can read byte-encoded variables ok
xs = read("xs_byte_levs.nc")
setcurrent(xs, 't_lev')
v = values(xs)
vm = vector_missing_value
# note that the file does not have _FillValue, hence -127 is read as-is
assert(vec_same_missing(v, |10, -20, 40, 55, vm, 80, 91|), 'nc byte values')


b = xs+2
v2 = values(b)
assert(vec_same_missing(v2, |12, -18, 42, 57, vm, 82, 93|), 'nc byte values +2')
c = abs(xs/30)
v3 = values(c)
assert(vec_same_missing(v3, |0, 0, 1, 1, vm, 2, 3|), 'nc byte values /30')
d = c+xs
v4 = values(d)
assert(vec_same_missing(v4, |10, -20, 41, 56, vm, 82, 94|), 'nc byte values +2 +xs')


# check that we can read ushort-encoded variables ok
ASSERT_EQUAL_EPS = 0.000001

us = read("ushort_data.nc")
setcurrent(us, 't')
netcdf_auto_scale_values(0)
v = values(us)
vm = vector_missing_value
assert_equal(count(v), 1080,  'nc ushort count')
assert_equal(v[1]    , 7710,  'nc ushort [1]')
assert_equal(v[2]    , vm,    'nc ushort [2]')
assert_equal(v[3]    , 6747,  'nc ushort [3]')
assert_equal(v[1080] , 20238, 'nc ushort [1080]')
netcdf_auto_scale_values(1)
v = values(us)
vm = vector_missing_value
assert_equal(count(v), 1080,           'nc ushort scaled count')
assert_equal(v[1]    , 272.999725329,  'nc ushort scaled [1]')
assert_equal(v[2]    , vm,             'nc ushort scaled [2]')
assert_equal(v[3]    , 272.000473044,  'nc ushort scaled [3]')
assert_equal(v[1080] , 285.999343842,  'nc ushort scaled [1080]')

rr = us + 2
v = values(rr)
assert_equal(count(v), 1080,          'nc ushort scaled +2 count')
assert_equal(v[1]    , 274.999267545, 'nc ushort scaled +2 [1]')
assert_equal(v[2]    , vm,            'nc ushort scaled +2 [2]')
assert_equal(v[3]    , 274.000015259, 'nc ushort scaled +2 [3]')
assert_equal(v[1080] , 287.998886057, 'nc ushort scaled +2 [1080]')


# check that we can read uint-encoded variables ok
ASSERT_EQUAL_EPS = 0.000001

us = read("uint_data.nc")
setcurrent(us, 't')
netcdf_auto_scale_values(0)
v = values(us)
vm = vector_missing_value
assert_equal(count(v), 1080,  'nc uint count')
assert_equal(v[1]    , 7710,  'nc uint [1]')
assert_equal(v[2]    , vm,    'nc uint [2]')
assert_equal(v[3]    , 6747,  'nc uint [3]')
assert_equal(v[1080] , 20238, 'nc uint [1080]')
netcdf_auto_scale_values(1)
v = values(us)
vm = vector_missing_value
assert_equal(count(v), 1080,           'nc uint scaled count')
assert_equal(v[1]    , 272.999725329,  'nc uint scaled [1]')
assert_equal(v[2]    , vm,             'nc uint scaled [2]')
assert_equal(v[3]    , 272.000473044,  'nc uint scaled [3]')
assert_equal(v[1080] , 285.999343842,  'nc uint scaled [1080]')

rr = us + 2
v = values(rr)
assert_equal(count(v), 1080,          'nc uint scaled +2 count')
assert_equal(v[1]    , 274.999267545, 'nc uint scaled +2 [1]')
assert_equal(v[2]    , vm,            'nc uint scaled +2 [2]')
assert_equal(v[3]    , 274.000015259, 'nc uint scaled +2 [3]')
assert_equal(v[1080] , 287.998886057, 'nc uint scaled +2 [1080]')


# check that we can read uint and int64 attributes
nc = read("attrs.nc")
setcurrent(nc, 't')
a = attributes(nc)
assert_equal(a['level'], 1000000, 'nc attrs int64')
assert_equal(a['expver'], 1, 'nc attrs uint')


# stress test: ensure that we don't hold file handles on temporary netcdf files
# the trick here is to retain the netcdf results in a list so that the variables
# are not destroyed; the file handles should be closed nonetheless.

# part 1: netcdf + number
xs = read("xs_with_missing_vals.nc")
setcurrent(xs, 't')
a = nil
for i = 1 to 2000 do
    a = a & [xs + i]
end for

assert_equal(minvalue(values(a[1])),    196.804047152,  'nc + 1 handles 1')
assert_equal(minvalue(values(a[70])),   265.804047152,  'nc + 1 handles 70')
assert_equal(minvalue(values(a[2000])), 2195.804047152, 'nc + 1 handles 2000')


# part 2: netcdf unary operator
xs = read("xs_with_missing_vals.nc")
setcurrent(xs, 't')
a = nil
for i = 1 to 2000 do
    a = a & [sin(xs)]
end for

assert_equal(minvalue(values(a[1])),    -0.999997144869, 'nc sin handles 1')
assert_equal(minvalue(values(a[70])),   -0.999997144869, 'nc sin handles 70')
assert_equal(minvalue(values(a[2000])), -0.999997144869, 'nc sin handles 2000')


# part 3: netcdf + netcdf
xs = read("xs_with_missing_vals.nc")
setcurrent(xs, 't')
xs2 = xs * 10
setcurrent(xs2, 't')
a = nil
for i = 1 to 2000 do
    a = a & [xs + xs2]
end for

assert_equal(minvalue(values(a[1])),    2153.84451867, 'nc + nc handles 1')
assert_equal(minvalue(values(a[70])),   2153.84451867, 'nc + nc handles 70')
assert_equal(minvalue(values(a[2000])), 2153.84451867, 'nc + nc handles 2000')


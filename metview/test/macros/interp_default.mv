# Metview Macro

# **************************** LICENSE START ***********************************
#
# Copyright 2019 ECMWF. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

include "test_utils.mv"

# check an emoslib-based interpolation at a point where we know that it returns
# a different result to mir, and therefore we can be fairly certain that emoslib
# was actually used
env = getenv('METVIEW_MARS_INTERP')
print('METVIEW_MARS_INTERP=' & env)

a = read('t1000_SH_to_RGG_reference.grb')
b = read(data: a, grid:[5,5])
n = nearest_gridpoint(b, 23, -100)
vi = version_info()

# from the value we're given, we can figure out whether emoslib or mir performed
# the interpolation; the correct one is defined by env var METVIEW_MARS_INTERP

if env = 'MIR' then
    print('Testing mir-based interpolation as the default')
    assert(is_close(n, 304.423), 'mir(default) point value is not close enough: ' & n)
    assert(vi.mir_version <> nil, 'version_info().mir_version is ' & vi.mir_version)
else if env = 'EMOS' then
    print('Testing emoslib-based interpolation as the default')
    assert(is_close(n, 303.423), 'emos(default) point value is not close enough: ' & n)
    assert(vi.emos_version <> nil, 'version_info().emos_version is ' & vi.emos_version)
end if


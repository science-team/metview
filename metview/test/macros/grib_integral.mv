# Metview Macro

# **************************** LICENSE START ***********************************
#
# Copyright 2019 ECMWF. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

include "test_utils.mv"

# the area of the Earth is in the 1E14 range! We need a
# high grib precision to correctly reproduce it in the 
# computations!!!
gribsetbits(48)
precision(25)

global ASSERT_EQUAL_EPS = 1E2  # used by assert_equal()
global VECTOR_DIFF_THRESHOLD = 0.0000001
global FIELD_DIFF_THRESHOLD = 0.0000001
global R = 6371200
global PI = acos(-1)
global E_AREA = 4*PI*R*R # area of the Earth

cell_area_reg_ll()
cell_area_reg_gg()
cell_area_red_gg()
integral_reg_ll()

# Regular latlon grid
function cell_area_reg_ll()
 
    d_eps=1E-8
 
    fs_22 = read('t1000_LL_2x2.grb')
    fs_22_ref = read('LL_2x2_cellarea_ref.grib')
 
    # global fully aligned to poles
    f = fs_22
    a = grid_cell_area(f)
    v = accumulate(a)
    v_ref = E_AREA
    assert_equal(v, v_ref, 'cell_area_reg_ll',1)  
    
    v = values(a)
    v_ref = values(fs_22_ref)
    assert(vec_same(v, v_ref, 1E-3), 'cell_area_reg_ll values',1) 
  
    # subarea but not aligned to the poles
    f = read(data: fs_22, grid: [2,2], area: [-89,-180,89, 180])
    a = grid_cell_area(f)
    v = accumulate(a)
    v_ref = E_AREA
    assert_equal(v, v_ref, 'cell_area_reg_ll',2)
    
    # subarea - Northern hemisphere
    f = read(data: fs_22, grid: [2,2], area: [0,-180,90, 180])
    a = grid_cell_area(f)
    v = accumulate(a)
    # the area goes below the Equator by 1 degree
    v_ref = E_AREA/2 + area_spherical_segment(0,-1)
    assert_equal(v, v_ref, 'cell_area_reg_ll',3)
    
    # subarea - Southern hemisphere
    f = read(data: fs_22, grid: [2,2], area: [-90,-180,0, 180])
    a = grid_cell_area(f)
    v = accumulate(a)
    # the area goes above the Equator by 1 degree
    v_ref = E_AREA/2 + area_spherical_segment(1,-0)
    assert_equal(v, v_ref, 'cell_area_reg_ll',4)
    
    # subarea - half of Northern hemisphere
    f = read(data: fs_22, grid: [2,2], area: [0,0,90,178])
    a = grid_cell_area(f)
    v = accumulate(a)
    # the area goes below the Equator by 1 degree
    v_ref = (E_AREA/2 + area_spherical_segment(0,-1))/2
    assert_equal(v, v_ref, 'cell_area_reg_ll',5)
    
    # subarea - half of Northern hemisphere
    f = read(data: fs_22, grid: [2,2], area: [0,-20,90,158])
    a = grid_cell_area(f)
    v = accumulate(a)
    # the area goes below the Equator by 1 degree
    v_ref = (E_AREA/2 + area_spherical_segment(0,-1))/2
    assert_equal(v, v_ref, 'cell_area_reg_ll',6)
    
end cell_area_reg_latlon

# Regular Gaussian grid
function cell_area_reg_gg()
 
    d_eps=1E-8
 
    fs = read('t1000_reg_gg_N48.grib')
    fs_ref = read('reg_gg_N48_cellarea_ref.grib')
    
    # global - fully aligned to poles
    a = grid_cell_area(fs)
    v = accumulate(a)
    v_ref = E_AREA
    assert_equal(v, v_ref, 'cell_area_reg_gg',1)
    
    v = values(a)
    v_ref = values(fs_ref)
    assert(vec_same(v, v_ref, 1E-3), 'cell_area_reg_gg values',1) 
    
end cell_area_reg_gg

# Reduced Gaussian grid
function cell_area_red_gg()
 
    d_eps=1E-8
 
    # global 
    fs = read('t1000_red_gg_N48.grib')
    fs_ref = read('red_gg_N48_cellarea_ref.grib')
    
    a = grid_cell_area(fs)   
    v = accumulate(a)
    v_ref = E_AREA
    assert_equal(v, v_ref, 'cell_area_red_gg',1)
    
    v = values(a)
    v_ref = values(fs_ref)
    assert(vec_same(v, v_ref, 1E-3), 'cell_area_red_gg values',1) 
  
    # subarea   
    fs = read('rgg_small_subarea.grib')
    fs_ref = read('rgg_small_subarea_cellarea_ref.grib')
    
    a = grid_cell_area(fs)
    v = accumulate(a)
    v_ref = 30010715877.06256485
    if 1 = 0 then
    #assert_equal(v, v_ref, 'cell_area_red_gg',2)
    
    v = values(a)
    v_ref = values(fs_ref)
    assert(vec_same(v, v_ref, 1E-3), 'cell_area_red_gg values',2) 
    
    #assert(field_same(a, fs_ref),"cell_area_red_gg values",2)
    end if
    
end cell_area_red_gg

# Regular latlon grid
function integral_reg_ll()
 
    d_eps=1E-8
 
    fs = read('t1000_LL_2x2.grb')
    
    # global - fully aligned to poles
    
    # const
    v = integral(fs*0+2)
    v_ref = E_AREA*2
    assert_equal(v, v_ref, 'integral_reg_ll',1)   
  
    # real field
    v = integral(fs)/1E6
    v_ref = 147380275213.3952941894531
    assert_equal(v, v_ref, 'integral_reg_ll',2) 
    
    # multiple fields
    v = vector(integral(fs & fs*0.5)/1E6)
    v_ref = |147380275213.3952941894531,147380275213.3952941894531*0.5|
    assert(vec_same(v, v_ref, 0.01), 'integral_reg_ll',3) 

end integral_reg_latlon



# lat1  > lat2
function area_spherical_segment(lat1, lat2)
    return 4*PI*R*R*cos(((lat1+lat2)/2)*PI/180.)*sin(((lat1-lat2)/2)*PI/180)
end area_spherical_segment    


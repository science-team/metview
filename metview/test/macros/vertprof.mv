# Metview Macro

#  **************************** LICENSE START ***********************************
# 
#  Copyright 2019 ECMWF. This software is distributed under the terms
#  of the Apache License version 2.0. In applying this license, ECMWF does not
#  waive the privileges and immunities granted to it by virtue of its status as
#  an Intergovernmental Organization or submit itself to any jurisdiction.
# 
#  ***************************** LICENSE END ************************************

include "test_utils.mv"

# model levels
test_vprof_ml("point",[50, 20], 1)
test_vprof_ml("point",[50, -20], 2)
test_vprof_ml("nearest_gridpoint",[50, 20], 3)
test_vprof_ml("nearest_gridpoint",[50, -20], 4)
test_vprof_ml("area",[40, -15, 55, 20], 5)
test_vprof_ml("area",[40, 4, 55, 20], 6)

# pressure levelss
test_vprof_pl("point",[50, 20], 1)
test_vprof_pl("point",[50, -20], 2)
test_vprof_pl("nearest_gridpoint",[50, 20], 3)
test_vprof_pl("nearest_gridpoint",[50, -20], 4)
test_vprof_pl("area",[20, -45, 55, 50], 5)
test_vprof_pl("area",[-40, 20, 55, 50], 6)

function test_vprof_ml(input_mode, point_or_area, id)

    fs = read("t_lnsp_ml137.grb")

    t = read(data: fs, param: "t")
    lnsp = read(data: fs, param: "lnsp")
    pres = unipressure(lnsp)
   
    fName = "vprof_ml_" & input_mode
   
    if input_mode = "point" or input_mode = "nearest_gridpoint" then
      
      eps = 0.03
      
      nc = mvert_prof(
         input_mode: input_mode,
         point : point_or_area,
         data : fs
        )
    else if input_mode = "area" then
        
        eps = 0.8
       
        nc = mvert_prof(
         input_mode: input_mode,
         area : point_or_area,
         data : fs
        )
    else
     fail(fName & " invalid input_mode=" & input_mode)
    end if
    
    # global attributes
    gattr = global_attributes(nc)
    assert_equal(count(keywords(gattr)),4,fName & ' - global attributes count', id)
    assert_equal(gattr.type,"MVPROFILE",fName & ' - global attribute type', id)
   
    # variables
    vars_ref = ["nlev","time","lat","lon","t","lnsp"]

    vars = variables(nc)
    assert_equal(vars, vars_ref,fName & " - vars",id)

    # build reference
    v_lev = vector(137)
    v_t = vector(137)
    
    if input_mode = "point" then
        for i=1 to count(pres) do
            v_lev[i] = interpolate(pres[i], point_or_area)/100
            v_t[i] = interpolate(t[i], point_or_area)
        end for    
    else if input_mode = "nearest_gridpoint" then
        for i=1 to count(pres) do
            v_lev[i] = nearest_gridpoint(pres[i], point_or_area)/100
            v_t[i] = nearest_gridpoint(t[i], point_or_area)
        end for    
    else if input_mode = "area" then
    
        # TODO: check how the area average is computed in the C++ module
        for i=1 to count(pres) do
            v_lev[i] = integrate(pres[i], point_or_area)/100
            v_t[i] = integrate(t[i], point_or_area)
        end for 
    end if 
    nlev = count(v_lev)
   
    setcurrent(nc, 't')   
    v = values(nc)
    
    ref = (var_name: 't',
        dim_names: ["time","nlev"],
        dim_values: [1,nlev],
        values: v_t)   
    check_xs_var(nc, ref, eps, fName & " - t",id)
    
    ref = (var_name: 'nlev',
        dim_names: ["nlev"],
        dim_values: [nlev],
        values: v_lev)   
    check_xs_var(nc, ref, eps, fName & " - nlev",id)

end test_vprof_ml

function test_vprof_pl(input_mode, point_or_area, id)

    fs = read("tuv_pl.grib")
    t = read(data: fs, param: "t")
     
    fName = "vprof_pl_" & input_mode
   
    if input_mode = "point" or input_mode = "nearest_gridpoint" then
      
      eps = 0.001
      
      nc = mvert_prof(
         input_mode: input_mode,
         point : point_or_area,
         data : fs
        )
    else if input_mode = "area" then
        
        eps = 0.001
       
        nc = mvert_prof(
         input_mode: input_mode,
         area : point_or_area,
         data : fs
        )
    else
     fail(fName & " invalid input_mode=" & input_mode)
    end if
    
    # global attributes
    gattr = global_attributes(nc)
    assert_equal(count(keywords(gattr)),4,fName & ' - global attributes count', id)
    assert_equal(gattr.type,"MVPROFILE",fName & ' - global attribute type', id)
   
    # variables
    vars_ref = ["nlev","time","lat","lon","t","u","v"]

    vars = variables(nc)
    assert_equal(vars, vars_ref,fName & " - vars",id)

    # build reference
    v_lev = |300, 400, 500, 700, 850, 1000|
    nlev = count(v_lev)
    v_t = vector(nlev)
    
    if input_mode = "point" then
        for i=1 to count(t) do
            v_t[i] = interpolate(t[nlev-i+1], point_or_area)
        end for    
    else if input_mode = "nearest_gridpoint" then
        for i=1 to count(t) do
            v_t[i] = nearest_gridpoint(t[nlev-i+1], point_or_area)
        end for    
    else if input_mode = "area" then
    
        # TODO: check how the area average is computed in the C++ module
        for i=1 to count(t) do
            v_t[i] = integrate(t[nlev-i+1], point_or_area)
        end for 
    end if 
    
    nlev = count(v_lev)
   
    setcurrent(nc, 't')   
    v = values(nc)
    
    ref = (var_name: 't',
        dim_names: ["time","nlev"],
        dim_values: [1,nlev],
        values: v_t)   
    check_xs_var(nc, ref, eps, fName & " - t",id)
    
    ref = (var_name: 'nlev',
        dim_names: ["nlev"],
        dim_values: [nlev],
        values: v_lev)   
    check_xs_var(nc, ref, eps, fName & " - nlev",id)

end test_vprof_pl
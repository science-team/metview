#Metview Macro
# Metview Macro

# **************************** LICENSE START ***********************************
#
# Copyright 2019 ECMWF. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

include "test_utils.mv"

global ASSERT_EQUAL_EPS = 1E-4  # used by assert_equal()
global VECTOR_DIFF_THRESHOLD = 0.0000001

global fs_22 = read('t1000_LL_2x2.grb')

test_bitmap()
test_nobitmap()

function test_bitmap()
	fs = fs_22 
	
	#-- const field
	f = fs*0+1
	
	# non missing
	r = bitmap(f,0)
	v = values(r)
	v_ref = values(f)
	assert(vec_same(v, v_ref,1E-4), "bitmap const",1)
	
	# all missing
	r = bitmap(f,1)
	v = values(r)
	v_ref = values(f) * vector_missing_value	
	assert(vec_same_missing(v, v_ref,1E-4), "bitmap const",2)
	
	#-- non const field
	f = fs
	
	# bitmap with value
	f_mask = f > 300
	r = bitmap(f_mask, 1)
	v = values(r)
	v_ref = values(f_mask)
	v_ref = bitmap(v_ref, 1)
	assert(vec_same_missing(v, v_ref,1E-4), "bitmap value",1)
	
	f_mask = f > 300
	r = bitmap(f_mask*2 , 2)
	v = values(r)
	v_ref = values(f_mask*2)
	v_ref = bitmap(v_ref, 2)
	assert(vec_same_missing(v, v_ref,1E-4), "bitmap value",1)
	
	# bitmap with field
	r = bitmap( f > 300, 0)
	r = bitmap(f, r)
	v = values(r)
	v_ref = values(f)
	v_ref = v_ref * bitmap(v_ref > 300, 0)	
	assert(vec_same_missing(v, v_ref,1E-4), "bitmap field",2)
	
	# multiple fields
	f = read('monthly_avg.grib')
	f = f[1,2]
	
	# with value
	f_mask = f > 300
	r = bitmap(f_mask, 1)
	assert(count(r) = count(f), "bitmap multi count", 1)
	for i=1 to count(r) do
	    v = values(r[i])
	    v_ref = values(f_mask[i])
	    v_ref = bitmap(v_ref, 1)
		assert(vec_same_missing(v, v_ref,1E-3), "bitmap multi value",1)
	end for
	
	# with field
	r = bitmap( f > 300, 0)
	r = bitmap(f, r)
	assert(count(r) = count(f), "bitmap multi count", 2)
	for i=1 to count(r) do
	    v = values(r[i])
	    v_ref = values(f[i])
	    v_ref = v_ref * bitmap(v_ref > 300, 0)
		assert(vec_same_missing(v, v_ref,1E-3), "bitmap multi value",2)
	end for	
	
end test_bitmap


function test_nobitmap()
	
	fs = read('t1000_LL_2x2_with_missing.grb')

    # single field
	f = fs
	r = nobitmap(f,1)
	v = values(r)
	vf = values(f)
	assert_equal(count(r),count(f), "nobitmap count",1)
	assert_equal(count(v),count(vf), "nobitmap value_count",1)
	
	for i=1 to count(v) do
	
	   if vf[i] = vector_missing_value then
	       assert_equal(v[i],1,"nobitmap value [" & i & "]",1)
	   else
	       assert_equal(v[i],vf[i],"nobitmap value [" & i & "]",1)   
	   end if
    end for	   
	
	# multiple fields
	f = fs & 2*fs
	r = nobitmap(f,1)
	assert_equal(count(r),count(f), "nobitmap count",2)
	
	for k=1 to count(r) do	
	   v = values(r[k])
	   vf = values(f[k])
	
	   assert_equal(count(v),count(vf), "nobitmap value_count f" & k,2)
	
	   for i=1 to count(v) do	
	       if vf[i] = vector_missing_value then
	           assert_equal(v[i],1,"nobitmap value f" & k & " [" & i & "]",2)
	       else
	           assert_equal(v[i],vf[i],"nobitmap value f" & k & " [" & i & "]",2)   
	       end if
        end for	   
	end for
	
end test_nobitmap

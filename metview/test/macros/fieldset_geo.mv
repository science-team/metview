# Metview Macro

# **************************** LICENSE START ***********************************
#
# Copyright 2019 ECMWF. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

include "test_utils.mv"

global ASSERT_EQUAL_EPS = 1E-4  # used by assert_equal()
global VECTOR_DIFF_THRESHOLD = 0.0000001

global fs_22 = read('t1000_LL_2x2.grb')
global fs_77 = read('t1000_LL_7x7.grb')

test_bearing()
test_bounding_box()
test_coslat()
test_distance()
test_latitudes()
test_longitudes()
test_mask()
test_mask_missing()
test_rmask()
test_rmask_missing()
test_sinlat()
test_solar_zenith_angle()
test_tanlat()


function test_bearing()
	fs = fs_22 
    lat_ref = 46
	lon_ref = 20 
	b = bearing(fs[1],lat_ref,lon_ref)

	eps =1E-4
	assert_equal(nearest_gridpoint(b,[50,20]),0,"bearing_basic",1)
	assert_equal(nearest_gridpoint(b,[46,24]),90,"bearing_basic",2)
	assert_equal(nearest_gridpoint(b,[40,20]),180,"bearing_basic",3)
	assert_equal(nearest_gridpoint(b,[46,16]),270,"bearing_basic",4)
	assert_equal(nearest_gridpoint(b,[46,-80]),270,"bearing_basic",5)
	assert_equal(nearest_gridpoint(b,[46,100]),90,"bearing_basic",6)
	assert_equal(nearest_gridpoint(b,[-40,20]),180,"bearing_basic",7)
	assert_equal(nearest_gridpoint(b,[80,20]),0,"bearing_basic",8)
	assert(nearest_gridpoint(b,[46,20]) = nil,"bearing_basic_9")

	assert_equal(nearest_gridpoint(b,[50,22]),19.092956543,"bearing",1)
	assert_equal(nearest_gridpoint(b,[50,18]),340.907043457,"bearing",2)
	assert_equal(nearest_gridpoint(b,[40,18]),193.098327637,"bearing",3)
	assert_equal(nearest_gridpoint(b,[40,22]),166.901672363,"bearing",4)

	b = bearing(fs[1],[lat_ref,lon_ref]) # coords as list
	assert_equal(nearest_gridpoint(b,[50,20]),0,"bearing_basic_l",1)
	assert_equal(nearest_gridpoint(b,[46,24]),90,"bearing_basic_l",2)
end test_bearing

function test_bounding_box()
    fn_name = "test_bounding_box"
  
	# lam
    fs = read("fc_data.grib")
	v = bounding_box(fs)
    v_ref = [|70,10,80,20|,|70,10,80,20|,|70,10,80,20|,|70,10,80,20|]
    assert(count(v) = count(v_ref),  fn_name & " count", 1)
	for i=1 to count(v_ref) do
	   assert(vec_same(v[i], v_ref[i],1E-4), fn_name & " vals-" & i,1)
	end for

	# global
    fs = fs_22
	v = bounding_box(fs)
    v_ref = |-90,-180,90,180|
    assert(count(v) = count(v_ref),  fn_name & " count", 2)
    assert(vec_same(v, v_ref,1E-4), fn_name & " vals",2)

    # regular Gaussian global
    fs = read("t1000_reg_gg_N48.grib")
    v = bounding_box(fs)
    v_ref = |-90,-180,90,180|
    assert(count(v) = count(v_ref),  fn_name & " count", 3)
    assert(vec_same(v, v_ref,1E-4), fn_name & " vals",3)
   
    # regular Gaussian subarea
    fs = read("regular_gaussian_subarea.grib")
    v = bounding_box(fs)
    v_ref = |15.0351,-101.25,56.9086,-38.812|
    assert(count(v) = count(v_ref),  fn_name & " count", 4)
    assert(vec_same(v, v_ref,1E-4), fn_name & " vals",4)
    
    # reduced Gaussian global
    fs = read("t1000_red_gg_N48.grib")
    v = bounding_box(fs)
    v_ref = |-90,-180,90,180|
    assert(count(v) = count(v_ref),  fn_name & " count", 5)
    assert(vec_same(v, v_ref,1E-4), fn_name & " vals",5)
   
    # reduced Gaussian subarea
    fs = read("rgg_small_subarea.grib")
    v = bounding_box(fs)
    v_ref = |84.8155,36.233,89.8765,46.185|
    assert(count(v) = count(v_ref),  fn_name & " count", 6)
    assert(vec_same(v, v_ref,1E-4), fn_name & " vals",6)

    fs = read("rgg_western_subarea.grib")
    v = bounding_box(fs)
    v_ref = |-9.97658,-80,11.9438,-70|
    assert(count(v) = count(v_ref),  fn_name & " count", 7)
    assert(vec_same(v, v_ref,1E-4), fn_name & " vals",7)

    # reduced latlon
    fs = read("reduced_ll.grib")
    v = bounding_box(fs)
    v_ref = |-90,-180,90,180|
    assert(count(v) = count(v_ref),  fn_name & " count", 8)
    assert(vec_same(v, v_ref,1E-4), fn_name & " vals",8)
    
    # ll dateline
    fs = read("ll_dateline.grib")
    v = bounding_box(fs)
    v_ref = |-90,120,90,240|
    assert(count(v) = count(v_ref),  fn_name & " count", 9)
    assert(vec_same(v, v_ref,1E-4), fn_name & " vals",9)
    
end	test_bounding_box

function test_coslat()
	fs = fs_22
    v = values(coslat(fs))
	v_ref = cos(pi * latitudes(fs) / 180.)
	assert(vec_same(v, v_ref), "coslat", 1)
end test_coslat

function test_distance()
	fs = fs_22
    eps =1E-4
	
    lat_ref = 0
	lon_ref = 0 
	b = distance(fs[1],lat_ref,lon_ref)
	assert_equal(nearest_gridpoint(b,[0,0]),0,"distance_eq",1)
	assert_equal(nearest_gridpoint(b,[0,90]),10000800,"distance_eq",2)
    assert_equal(nearest_gridpoint(b,[0,-90]),10000800,"distance_eq",3)
    assert_equal(nearest_gridpoint(b,[0,180]),2*10000800,"distance_eq",4)
	assert_equal(nearest_gridpoint(b,[90,0]),10000800,"distance_eq",5)
    assert_equal(nearest_gridpoint(b,[-90,0]),10000800,"distance_eq",6)
	assert_equal(nearest_gridpoint(b,[48,20]),5671570,"distance_eq",7)
    assert_equal(nearest_gridpoint(b,[48,-20]),5671570,"distance_eq",8)
    assert_equal(nearest_gridpoint(b,[-48,-20]),5671570,"distance_eq",9)
	assert_equal(nearest_gridpoint(b,[-48,20]),5671570,"distance_eq",10)

	lat_ref = 90
	lon_ref = 0 
	b = distance(fs[1],lat_ref,lon_ref)
	assert_equal(nearest_gridpoint(b,[0,0]),10000800,"distance_np",1)
	assert_equal(nearest_gridpoint(b,[0,90]),10000800,"distance_np",2)
    assert_equal(nearest_gridpoint(b,[0,-90]),10000800,"distance_np",3)
    assert_equal(nearest_gridpoint(b,[0,180]),10000800,"distance_np",4)
	assert_equal(nearest_gridpoint(b,[90,0]),0,"distance_np",5)
    assert_equal(nearest_gridpoint(b,[-90,0]),2*10000800,"distance_np",6)
	assert_equal(nearest_gridpoint(b,[48,20]),4667040,"distance_np",7)
    assert_equal(nearest_gridpoint(b,[48,-20]),4667040,"distance_np",8)
    assert_equal(nearest_gridpoint(b,[-48,-20]),15334560,"distance_np",9)
	assert_equal(nearest_gridpoint(b,[-48,20]),15334560,"distance_np",10)

	lat_ref = -90
	lon_ref = 0 
	b = distance(fs[1],lat_ref,lon_ref)
	assert_equal(nearest_gridpoint(b,[0,0]),10000800,"distance_sp",1)
	assert_equal(nearest_gridpoint(b,[0,90]),10000800,"distance_sp",2)
    assert_equal(nearest_gridpoint(b,[0,-90]),10000800,"distance_sp",3)
    assert_equal(nearest_gridpoint(b,[0,180]),10000800,"distance_sp",4)
	assert_equal(nearest_gridpoint(b,[90,0]),2*10000800,"distance_sp",5)
    assert_equal(nearest_gridpoint(b,[-90,0]),0,"distance_sp",6)
	assert_equal(nearest_gridpoint(b,[48,20]),15334560,"distance_sp",7)
    assert_equal(nearest_gridpoint(b,[48,-20]),15334560,"distance_sp",8)
    assert_equal(nearest_gridpoint(b,[-48,-20]),4667040,"distance_sp",9)
	assert_equal(nearest_gridpoint(b,[-48,20]),4667040,"distance_sp",10)

	lat_ref = 48
	lon_ref = 20
	b = distance(fs[1],lat_ref,lon_ref)
	assert_equal(nearest_gridpoint(b,[0,0]),5671570,"distance_hu",1)
	assert_equal(nearest_gridpoint(b,[0,90]),8530712,"distance_hu",2)
    assert_equal(nearest_gridpoint(b,[0,-90]),11470888,"distance_hu",3)
    assert_equal(nearest_gridpoint(b,[0,180]),14330030,"distance_hu",4)
	assert_equal(nearest_gridpoint(b,[90,0]),4667040,"distance_hu",5)
    assert_equal(nearest_gridpoint(b,[-90,0]),15334560,"distance_hu",6)
	assert_equal(nearest_gridpoint(b,[48,20]),0,"distance_hu",7)
    assert_equal(nearest_gridpoint(b,[48,-20]),2940176,"distance_hu",8)
    assert_equal(nearest_gridpoint(b,[-48,-20]),11343140,"distance_hu",9)
	assert_equal(nearest_gridpoint(b,[-48,20]),10667520,"distance_hu",10)


	b = distance(fs[1],[lat_ref,lon_ref]) # target as list
	assert_equal(nearest_gridpoint(b,[0,0]),5671570,"distance_hu_l",1)
	assert_equal(nearest_gridpoint(b,[0,90]),8530712,"distance_hu_l",2)
    assert_equal(nearest_gridpoint(b,[0,-90]),11470888,"distance_hu_l",3)
    assert_equal(nearest_gridpoint(b,[0,180]),14330030,"distance_hu_l",4)
	assert_equal(nearest_gridpoint(b,[90,0]),4667040,"distance_hu_l",5)
    assert_equal(nearest_gridpoint(b,[-90,0]),15334560,"distance_hu_l",6)
	assert_equal(nearest_gridpoint(b,[48,20]),0,"distance_hu_l",7)
    assert_equal(nearest_gridpoint(b,[48,-20]),2940176,"distance_hu_l",8)
    assert_equal(nearest_gridpoint(b,[-48,-20]),11343140,"distance_hu_l",9)
	assert_equal(nearest_gridpoint(b,[-48,20]),10667520,"distance_hu_l",10)
	
end test_distance

function test_latitudes()

	fs = fs_22
	
	v = latitudes(fs)
	assert_equal(count(v),16380,"latitudes_count",1)
	assert_equal(v[1],90,"latitudes_1",1)
	assert_equal(v[2],90,"latitudes_2",1)
	assert_equal(v[8104],0,"latitudes_3",1)
	assert_equal(v[11336],-34,"latitudes_4",1)
	assert_equal(v[16380],-90,"latitudes_5",1)

    fs = fs_22 & fs_22
    lst = latitudes(fs)
	assert_equal(count(lst),2,"latitudes_list_count",2)
	for i=1 to count(lst) do
		v = lst[i]
		assert_equal(count(v),16380,"latitudes_count",1+i)
		assert_equal(v[1],90,"latitudes_1",1+i)
		assert_equal(v[2],90,"latitudes_2",1+i)
		assert_equal(v[8104],0,"latitudes_3",1+i)
		assert_equal(v[11336],-34,"latitudes_4",1+i)
		assert_equal(v[16380],-90,"latitudes_5",1+i)
	end for 

    if does_eccodes_support_healpix() then
        print("Testing healpix")
        h = read("t2m_healpix_4.grib")
        v = latitudes(h)
		assert_equal(count(v),192,"latitudes_healpix_count",1)
        assert_equal(v[1], 78.2841, "latitudes_healpix_1", 1)
        assert_equal(v[117], -9.59407, "latitudes_healpix_2", 1)
        assert_equal(v[192], -78.2841, "latitudes_healpix_3", 1)
    end if

end test_latitudes

function test_longitudes()

	fs = fs_22
	
	v = longitudes(fs)
	assert_equal(count(v),16380,"longitudes_count",1)
	assert_equal(v[1],0,"longitudes_1",1)
	assert_equal(v[2],2,"longitudes_2",1)
	assert_equal(v[8104],6,"longitudes_3",1)
	assert_equal(v[11336],350,"longitudes_4",1)
	assert_equal(v[16380],358,"longitudes_5",1)

	fs = fs_22 & fs_22
    lst = longitudes(fs)
	assert_equal(count(lst),2,"longitudes_list_count",2)
	for i=1 to count(lst) do
		v = lst[i]
		assert_equal(count(v),16380,"longitudes_count",1+i)
		assert_equal(v[1],0,"longitudes_1",1+i)
		assert_equal(v[2],2,"longitudes_2",1+i)
		assert_equal(v[8104],6,"longitudes_3",1+i)
		assert_equal(v[11336],350,"longitudes_4",1+i)
		assert_equal(v[16380],358,"longitudes_5",1+i)
	end for 

    if does_eccodes_support_healpix() then
        print("Testing healpix")
        h = read("t2m_healpix_4.grib")
        v = longitudes(h)
		assert_equal(count(v),192,"longitudes_healpix_count",1)
        assert_equal(v[1], 45, "longitudes_healpix_1", 1)
        assert_equal(v[117], 270, "longitudes_healpix_2", 1)
        assert_equal(v[192], 315.0, "longitudes_healpix_3", 1)
    end if


end test_longitudes

function test_mask()

	fs = fs_22 

	# global area
	area = [90,-180,-90,180]
	
	r = mask(fs,area)
	v = values(r)
	v_ref = values(fs)*0+1
	assert(vec_same(v, v_ref,1E-4), "mask global",1)
		
	# limited area
	area = [80,-100,-60, 80]
	
	r = mask(fs,area)
	v = values(r)
	
	# build ref
	lat = latitudes(fs)
	lon = longitudes(fs)
	v_ref = values(fs)*0
	for i=1 to count(lat) do
	   
	   if lon[i] > 180 then
	       lon[i] = lon [i] - 360
	   end if    
	   
	   if lat[i] <= area[1] and lat[i] >= area[3] and
	      lon[i] <= area[4] and lon[i] >= area[2] then
	       v_ref[i] = 1
	   end if
	end for 
	
	assert(vec_same(v, v_ref,1E-4), "mask lam",1)


	r = mask(fs,80,-100,-60,80) # same as above, but area as numbers
	v = values(r)
	assert(vec_same(v, v_ref,1E-4), "mask lam_area_as_nums",1)


	r = mask(fs,80,-100,-60,80, 'missing', 'off') # same as above, but Pythonesque way missing=False
	v = values(r)
	assert(vec_same(v, v_ref,1E-4), "mask lam_area_as_nums_py",1)


	# multiple fields - limited area
	f = fs & 2*fs
	
	r = mask(f, area)
	assert(count(r) = count(f), "mask multi count", 1)
	# v_ref is the same as before
	for i=1 to count(r) do	
	    v = values(r[i])    
		assert(vec_same_missing(v, v_ref,1E-3), "mask multi value",1)
	end for

    # area that straddles the date line
	area = [50,150,30,-130]
	r = mask(fs,area)
	ones = find(r, 1)
	assert(list_same(ones[1], [50, 150]), "mask dateline first 1")
	assert(list_same(ones[count(ones)], [30, 230]), "mask dateline last 1")
	assert_equal(count(ones), 451, "mask dateline count")
	
end test_mask

function test_mask_missing()

	fs = fs_22 

	# global area
	area = [90,-180,-90,180]
	
	r = mask(fs,area,'missing')
	v = values(r)
	v_ref = values(fs)
	assert(vec_same(v, v_ref,1E-4), "mask_missing global",1)
		
	# limited area
	area = [80,-100,-60, 80]
	
	r = mask(fs,area,'missing')
	v = values(r)
	
	# build ref
	lat = latitudes(fs)
	lon = longitudes(fs)
    src_vals = values(fs)
	v_ref = src_vals + vector_missing_value
	for i=1 to count(lat) do
	   
	   if lon[i] > 180 then
	       lon[i] = lon [i] - 360
	   end if    
	   
	   if lat[i] <= area[1] and lat[i] >= area[3] and
	      lon[i] <= area[4] and lon[i] >= area[2] then
	       v_ref[i] = src_vals[i]
	   end if
	end for 
	
	assert(vec_same(v, v_ref,1E-4), "mask_missing lam",1)


	r = mask(fs,80,-100,-60,80,'missing') # same as above, but area as numbers
	v = values(r)
	assert(vec_same(v, v_ref,1E-4), "mask_missing lam_area_as_nums",1)

	r = mask(fs,80,-100,-60,80,'missing', 'on') # same as above, but Pythonesque missing=True
	v = values(r)
	assert(vec_same(v, v_ref,1E-4), "mask_missing lam_area_as_nums_py",1)

	# multiple fields - limited area
	f = fs & 2*fs
	
	r = mask(f, area, 'missing')
	assert(count(r) = count(f), "mask_missing multi count", 1)
	# v_ref is the same as before
	for i=1 to count(r) do
	    v = values(r[i])    
		assert(vec_same_missing(v, v_ref*i,1E-3), "mask_missing multi value",1)
	end for

end test_mask_missing

function test_rmask()
    fs = fs_22
    f_ref = read('rmask_ref.grib')
    
    lat_c = [48, -40, 90]
    lon_c = [19,-60, 0]
    rad   = [1000000, 7000000, 6000000]
    
    for i=1 to count(f_ref) do         
        r = rmask(fs, lat_c[i], lon_c[i], rad[i])
        v = values(r)
        v_ref = values(f_ref[i])
        assert(vec_same(v,v_ref),"rmask",i) 
    end for
       
    # list as argument    
    r = rmask(fs, [lat_c[1], lon_c[1], rad[1]])
    v = values(r)
    v_ref = values(f_ref[1])
    assert(vec_same(v,v_ref),"rmask",4) 

    # same, but Pythonesque way of supplying missing=False
    r = rmask(fs, [lat_c[1], lon_c[1], rad[1]], 'missing', 'off')
    v = values(r)
    v_ref = values(f_ref[1])
    assert(vec_same(v,v_ref),"rmask_py",1) 

end test_rmask

function test_rmask_missing()
    fs = fs_22
    f_ref = read('rmask_ref.grib')
    f_ref = bitmap(f_ref, bitmap(f_ref, 0))
    
    lat_c = [48, -40, 90]
    lon_c = [19,-60, 0]
    rad   = [1000000, 7000000, 6000000]
    
    for i=1 to count(f_ref) do         
        r = rmask(fs, lat_c[i], lon_c[i], rad[i], 'missing')
        v = values(r)
        v_ref = values(f_ref[i])-1 + values(fs)
        assert(vec_same(v,v_ref),"rmask_missing",i) 
    end for
       
    # list as argument    
    r = rmask(fs, [lat_c[1], lon_c[1], rad[1]], 'missing')
    v = values(r)
    v_ref = values(f_ref[1])-1 + values(fs)
    assert(vec_same(v,v_ref),"rmask_missing",4) 

    # same, but Pythonesque way of supplying 'missing'
    r = rmask(fs, [lat_c[1], lon_c[1], rad[1]], 'missing', 'on')
    v = values(r)
    v_ref = values(f_ref[1])-1 + values(fs)
    assert(vec_same(v,v_ref),"rmask_missing_py",1) 

end test_rmask_missing

function test_sinlat()
	fs = fs_77
	eps = 1E-6
	v = values(sinlat(fs))
	v_ref = sin(pi * latitudes(fs) / 180.)
	assert(vec_same(v, v_ref, eps), "sinlat", 1)
end test_sinlat

function test_tanlat()
	fs = fs_77
	eps = 1E-6
	v = values(tanlat(fs))
	lats = latitudes(fs)
	lats = lats + bitmap(abs(lats) > 90-1E-7, 1)
	v_ref = tan(pi * lats / 180.)	
	assert(vec_same_missing(v, v_ref, eps), "tanlat", 1)
end test_tanlat

function test_solar_zenith_angle()
	fn_name = "solar_zenith_angle"
	
	fs = read("solar_zenith_angle.grib")
	f_in = fs[1,6]
	f_ref = fs[7,12]
	
	r = solar_zenith_angle(f_in)
	
	# fs =0 
    # write("solar_zenith_angle.grib", f_in, r)
 	assert_equal(count(r),count(f_ref),fn_name & " count",1)
 	for i=1 to count(r) do
 	   v = values(r[i])
 	   v_ref = values(f_ref[i])
 	   assert(vec_same(v, v_ref, 1E-6), fn_name & " value-" & i, 1)
 	end for
	
	# some obvious cases
	
	# get data on 0.5x0.5 grid
	hr = read(data: f_in[1],  grid:[0.5, 0.5])
	lat = |0,23.5,-23.5|
	lon = |0,0,0|
	
	# spring equinox
	ft = grib_set(hr, ["dataDate", 20180321, "dataTime",0, "step", 12])
	r = solar_zenith_angle(ft)
    v_ref  = |0.0301904268563, 23.4698113985, 23.5301904269|
    v = nearest_gridpoint(r, lat, lon)
    assert(vec_same(v, v_ref, 1E-5), fn_name & " spring", 2)

	# summer solstice
	ft = grib_set(hr, ["dataDate", 20180621, "dataTime",0, "step", 12])
	r = solar_zenith_angle(ft)
    v_ref  = |23.4382684194, 0.0617272816598, 46.9382684194|
    v = nearest_gridpoint(r, lat, lon)
    assert(vec_same(v, v_ref, 1E-5), fn_name & " summer", 2)
	
	# autumn equinox
	ft = grib_set(hr, ["dataDate", 20180923, "dataTime",0, "step", 12])
	r = solar_zenith_angle(ft)
    v_ref = |0.133450090885,23.3665433526,23.6334500909|
    v = nearest_gridpoint(r, lat, lon)
    assert(vec_same(v, v_ref, 1E-5), fn_name & " autumn", 2)
	
	# winter solstice
	ft = grib_set(hr, ["dataDate", 20181221, "dataTime",0, "step", 12])
	r = solar_zenith_angle(ft)
    v_ref  = |23.4350353479, 46.9350353479, 0.0649639368057|
    v = nearest_gridpoint(r, lat, lon)
    assert(vec_same(v, v_ref, 1E-5), fn_name & " winter", 2)
	
	# test cosine option
	r = solar_zenith_angle(ft, to_cosine:"on")
	v_ref = cos(v_ref * acos(-1)/180)
	v = nearest_gridpoint(r, lat, lon)
	assert(vec_same(v, v_ref, 1E-5), fn_name & " cosine", 3)
	
	assert_equal(grib_get_long(r, "paramId"), 214001, 
	   fn_name & " cosine paramId", 3)
	
end test_solar_zenith_angle


#----------------------------

function step_equal(fs:fieldset, refstep:number)
    s = grib_get_long(fs, 'step')
    return (s = refstep)
end steps_equal

function steps_equal(fs:fieldset, refsteps:list)
    s = grib_get_long(fs, 'step')
    return is_equal_any(s, refsteps)
end steps_equal

function levels_equal(fs:fieldset, reflevs:list)
    s = grib_get_long(fs, 'level')
    eq = is_equal_any(s, reflevs)
    if not eq then
        print('levels not equal: ', s, ' vs ref ', reflevs)
    end if
    return eq
end levels_equal

function params_equal(fs:fieldset, refparams:list)
    s = grib_get_string(fs, 'shortName')
    return is_equal_any(s, refparams)
end params_equal

#====================================================
#
# Functions to generate test reference data
#
#====================================================

function generate_test_rmask_reference()
    fs = fs_22
    
    lat_c = 48
    lon_c = 19
    rad = 1000000
    r1 = rmask(fs, lat_c, lon_c, rad)
      
    lat_c = -40
    lon_c = -60
    rad = 7000000
    r2 = rmask(fs, lat_c, lon_c, rad)

    lat_c = 90
    lon_c = 0
    rad = 6000000
    r3 = rmask(fs, lat_c, lon_c, rad)
    
    #write('rmask_ref.grib',r1 & r2 & r3)

end generate_test_rmask_reference

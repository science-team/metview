#Metview Macro

# **************************** LICENSE START ***********************************
#
# Copyright 2019 ECMWF. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

include "test_utils.mv"

global ASSERT_EQUAL_EPS = 1E-4  # used by assert_equal()
global VECTOR_DIFF_THRESHOLD = 0.0000001

test_ml_to_hl_field_geop_h()
test_ml_to_hl_surface_geop_h()

    
function test_ml_to_hl_field_geop_h()
    
    # The target height now is defined by a field!

    fs = read('tq_ml137.grib')   
    t = read(data: fs, param: 't')
    zs = read(data: fs, param: 'zs')
    lnsp = read(data: fs, param: 'lnsp')
    z = read("z_ml137_ref.grib")
    
    z_bottom = read(data: z, levelist: 137)
    
    method = "linear"
    hLst = [1000, 500, 2500, 10000]
    gEarth = 9.80665
    
    # Construct a height field with contant values
    h_fs = nil
    for i=1 to count(hLst) do
        h_fs = h_fs & (z[1]*0 + hLst[i])
    end for    
       
    # 1. interpolate height field above sea to height levels above sea
    #f_ref = read("ml_to_hl_field_geop_h_asl_ref.grib")
    
    r = ml_to_hl(z/gEarth, z, nil, h_fs, "sea", method, nil, "geopotential")
    assert_equal(count(r),count(h_fs),"ml_to_hl_field_geop_h count",1)
   
    for i=1 to count(r) do        
        v = values(r[i])
         
        # we should get back the height field 
        v_ref = values(bitmap(h_fs[i], bitmap(z_bottom > h_fs[i]*gEarth, 1)))
       
        assert(vec_same_missing(v,v_ref,1E-3),"ml_to_hl_field_geop_h value lev" & i,1)
    end for    
    
    
    # 2. interpolate height field above ground to height levels above ground
    h_agr = nil
    for i=1 to count(z) do
        h_agr = h_agr & (z[i] - zs[1])/gEarth
    end for  
  
    r = ml_to_hl(h_agr, z, zs, h_fs, "ground", method, nil, "geopotential")
    assert_equal(count(r),count(h_fs),"ml_to_hl_field_geop_h count",2)
    
    for i=1 to count(r) do        
        v = values(r[i])
         
        v_ref = values(h_fs[i])
        assert(vec_same_missing(v,v_ref,2),"ml_to_hl_field_geop_h value lev" & i,2)
        
    end for    
    
    # 3. interpolate pressure to height levels above sea    
    f_ref =read("ml_to_hl_asl_geop_h_ref.grib")

    pres = unipressure(lnsp)
    r = ml_to_hl(pres, z, nil, h_fs, "sea", method, nil, "geopotential")
    assert_equal(count(r),count(h_fs),"ml_to_hl_field_geop_h count",3)
    
    for i=1 to count(r) do        
        v = values(r[i])
         
        v_ref = values(f_ref[i])
        assert(vec_same_missing(v,v_ref,1E-3),"ml_to_hl_field_geop_h value lev" & i,3)
    end for 
    
    
    # 4. interpolate pressure to height levels above ground   
    f_ref =read("ml_to_hl_agr_geop_h_ref.grib")

    pres = unipressure(lnsp)
    r = ml_to_hl(pres, z, zs, h_fs, "ground", method, nil, "geopotential")
    assert_equal(count(r),count(h_fs),"ml_to_hl_field_geop_h count",4)
    
    for i=1 to count(r) do        
        v = values(r[i])
         
        v_ref = values(f_ref[i])
        assert(vec_same_missing(v,v_ref,1E-3),"ml_to_hl_field_geop_h value lev" & i,4)
    end for    
    
    # 5. interpolate height above sea to non constant height 
    #  fields above sea 
    h_fs_noconst = nil
    # the target height field = surface orography + a constant value
    delta_h_lst = [1200, 5000, 10000]
    for i=1 to count(delta_h_lst) do
        h_fs_noconst = h_fs_noconst & (zs/gEarth + delta_h_lst[i])
    end for
   
    # the results should be very close to the target heights themselves
    r = ml_to_hl(z/gEarth, z, nil, h_fs_noconst, "sea", method, nil, "geopotential")
    assert_equal(count(r),count(h_fs_noconst),"ml_to_hl_field_geop_h count",5)
    
    for i=1 to count(r) do        
        v = values(r[i])        
        v_ref = values(h_fs_noconst[i])
#       plot(r[i]-h_fs_noconst[i])         
        assert(vec_same_missing(v,v_ref,1E-3),"ml_to_hl_field_geop_h value lev" & i,5)
    end for  
     
    # 6. interpolate height above sea to non constant height 
    #  fields above ground
   
    # the results should be very close to the target heights + zs/9.81
    r = ml_to_hl(z/gEarth, z, zs, h_fs_noconst, "ground", method, nil, "geopotential")
    assert_equal(count(r),count(h_fs_noconst),"ml_to_hl_field_geop_h count",6)
    
    for i=1 to count(r) do        
        v = values(r[i])   
        # we should get back the target height field + surf orography    
        v_ref = values(h_fs_noconst[i] + zs/gEarth)      
        assert(vec_same_missing(v,v_ref,1E-3),"ml_to_hl_field_geop_h value lev" & i,6)
    end for
    
    # 7. interpolate height above sea to non constant height 
    #  fields above sea
    # - the input field and the input z field contain the fields in a different
    # order
    z_fs_input = sort(z,"levelist", ">")
    z_fs = sort(z,"levelist", "<")
    #print(grib_get(h_fs,["level"]))
    #print(grib_get(z_fs,["level"]))
    r = ml_to_hl(z_fs_input/gEarth, z_fs, nil, h_fs_noconst, "sea", method, nil, "geopotential")
    assert_equal(count(r),count(h_fs_noconst),"ml_to_hl_field_geop_h count",7)
    
    for i=1 to count(r) do        
        v = values(r[i])
         
        # we should get back the target height field 
        v_ref = values(h_fs_noconst[i])
        assert(vec_same_missing(v,v_ref,1E-3),"ml_to_hl_field_geop_h value lev" & i,7)
    end for    
    
    # 8. interpolate height above sea to non constant height 
    #  fields above ground
    # - the input field and the input z field contain the fields in a different
    # order
    r = ml_to_hl(z_fs_input/gEarth, z_fs, zs, h_fs_noconst, "ground", method, nil, "geopotential")
    assert_equal(count(r),count(h_fs_noconst),"ml_to_hl_field_geop_h count",8)
    
    for i=1 to count(r) do        
        v = values(r[i])
         
        # we should get back the target height field + surf orography 
        v_ref = values(h_fs_noconst[i] + zs/gEarth)
        assert(vec_same_missing(v,v_ref,1E-2),"ml_to_hl_field_geop_h value lev" & i,8)
    end for   
           
end test_ml_to_hl_field_geop_h

function test_ml_to_hl_surface_geop_h()

    fn_name = "ml_to_hl_surface_geop_h"

    fs = read('tq_ml137.grib')   
    t = read(data: fs, param: 't')
    zs = read(data: fs, param: 'zs')
    lnsp = read(data: fs, param: 'lnsp')
    z = read("z_ml137_ref.grib")
    
    hLst = [1, 5, 120, 10]
    gEarth = 9.80665
   
    # 1. interpolate height field above ground to height levels above ground
    h_agr = nil
    for i=1 to count(z) do
        h_agr = h_agr & (z[i] - zs[1])/gEarth
    end for  
  
    r = ml_to_hl(h_agr, z, zs, hLst, "ground", "linear", 0, "geopotential")
    assert_equal(count(r),count(hLst),fn_name & " count",1)
    
    for i=1 to count(r) do        
        v = values(r[i])
         
        v_ref = v*0 + hLst[i]
        assert(vec_same_missing(v,v_ref,1E-2),fn_name & " value lev" & i,1)
        
        r_level=grib_get_long(r[i],"level")
        assert_equal(r_level,hLst[i],fn_name & " level lev" & i,1)
       
        r_level=grib_get_string(r[i],"typeOfLevel")
        assert_equal(r_level,"heightAboveGround",fn_name & " typeOfLevel lev" & i,1)
        
        r_name=grib_get_string(r[i],"shortName")
        assert_equal(r_name,"z",fn_name & " shortName lev" & i,1)
    end for    
    
    # 2. interpolate height field above ground to height levels above ground
    f_ref =read("ml_to_hl_agr_surf_log_geop_h_ref.grib")
    
    hLst = [1, 5, 120, 212, 10]
    r = ml_to_hl(h_agr, z, zs, hLst, "ground", "log", 0, "geopotential")
    assert_equal(count(r),count(hLst),fn_name & " count",2)
    
    for i=1 to count(r) do        
        v = values(r[i])   
        v_ref = values(f_ref[i])  
        
        #print(maxvalue(v))
        #print(maxvalue(abs(v - v_ref)))
        #print(v[1,10])
        #print(v_ref[1,10])
        assert(vec_same_missing(v,v_ref,2),fn_name & " value lev" & i,2)
         
        r_level=grib_get_long(r[i],"level")
        assert_equal(r_level,hLst[i],fn_name & " level lev" & i,2)
         
        r_level=grib_get_string(r[i],"typeOfLevel")
        assert_equal(r_level,"heightAboveGround",fn_name & " typeOfLevel lev" & i,2)
         
        r_name=grib_get_string(r[i],"shortName")
        assert_equal(r_name,"z",fn_name &  " shortName lev" & i,2)
    end for     
    
    # 3. interpolate pressure to height levels above ground   
    f_ref =read("ml_to_hl_agr_surf_geop_h_ref.grib")

    hLst = [1, 5, 120, 10]
    pres = unipressure(lnsp)
    r = ml_to_hl(pres, z, zs, hLst, "ground", "linear", exp(lnsp), "geopotential")
    assert_equal(count(r),count(hLst), fn_name & " count",3)
   
    for i=1 to count(r) do        
        v = values(r[i])
        v_ref = values(f_ref[i])
        
        assert(vec_same_missing(v,v_ref,1E-3),fn_name & " value lev" & i,3)
        
        r_level=grib_get_long(r[i],"level")
        assert_equal(r_level,hLst[i],fn_name & " level lev" & i,3)
        
        r_level=grib_get_string(r[i],"typeOfLevel")
        assert_equal(r_level,"heightAboveGround",fn_name & " typeOfLevel lev" & i,3)
        
        r_name=grib_get_string(r[i],"shortName")
        assert_equal(r_name,"pres",fn_name & " shortName lev" & i,3)
        
    end for    
        
    
end test_ml_to_hl_surface_geop_h


function diag_ml_to_hl_compare_log_to_linear()

    fs = read('tq_ml137.grib')   
    t = read(data: fs, param: 't')
    zs = read(data: fs, param: 'zs')
    lnsp = read(data: fs, param: 'lnsp')
    z = read("z_ml137_ref.grib")
   
    hLst = [1000, 500, 2500, 10000]
    
    print('ml_to_hl_compare_log_to_linear -->')
    
    # field changing by log height
    r = ml_to_hl(1000*log(z/9.81+1000), z, nil, hLst, "sea", "log")
    r1 = ml_to_hl(1000*log(z/9.81+1000), z, nil, hLst, "sea", "linear")    
    v = interpolate(r, [50,20])
    v1 = interpolate(r1, [50,20])
    print('field changing by log height')
    print('log',v)
    print('lin',v1)
    ref = 1000*log(vector(hLst) + 1000)
    print('ref',ref)
    print('err_log', abs(vector(v)-ref))
    print('err_lin', abs(vector(v1)-ref))
   
    # field changing by height
    r = ml_to_hl(z/9.81, z, nil, hLst, "sea", "log")
    r1 = ml_to_hl(z/9.81, z, nil, hLst, "sea", "linear") 
    v = interpolate(r, [50,20])
    v1 = interpolate(r1, [50,20])
    print(' ')
    print('field changing by height')
    print('log',v)
    print('lin',v1)
    ref = vector(hLst)
    print('ref',ref)
    print('err_log', abs(vector(v)-ref))
    print('err_lin', abs(vector(v1)-ref))
    
end diag_ml_to_hl_compare_log_to_linear

#====================================================
#
# Functions to generate test reference data
#
#====================================================

function generate_ml_to_h_reference()
    
    hLst = [1000, 500, 2500, 10000] 
    fs = read('tq_ml137.grib')       
    lnsp = read(data: fs, param: 'lnsp')
    zs = read(data: fs, param: 'z')
    z = read("z_ml137_ref.grib")
    
    # asl
    pres = unipressure(lnsp)
    r1 = ml_to_hl(pres, z, nil, hLst, "sea", "linear")
    
    # visually checked!!!
    #plot(r1)
    #write("ml_to_hl_asl_ref.grib", r1)
    
    # agr
    pres = unipressure(lnsp)
    r2 = ml_to_hl(pres, z, zs, hLst, "ground", "linear")
    
    # visually checked!!!
    #plot(r1-r2)
    #write("ml_to_hl_agr_ref.grib", r2)
    
end generate_ml_to_h_reference()   
#Metview Macro

# **************************** LICENSE START ***********************************
#
# Copyright 2015 ECMWF. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

include "test_utils.mv"

test_db()
test_no_db()

function test_db()

    # First test: ident does not exist
    st1 = stations(
            search_key    : "ident",
            ident         : 12344321,
    		fail_on_error : "no"
            )

    if st1 <> nil then
       fail("Should return nil because ident does not exist")
    end if

    # Second test: should not failure
    st = stations(
        search_key    : "ident",
        ident         : 3772, #Heathrow
        fail_on_error : "no"
        )

    if st = nil then
       fail("Should not return nil because ident does exist")
    end if

    if st.LATITUDE <> 51.48 or st.LONGITUDE <> -0.45 or st.NAME <> "Heathrow" then
       fail("The returned station is not correct: ", st)
    end if
    
 end test_db


function test_no_db()

    fn_name = "no_db"

    st = stations(
            search_stations_database : "no",
            position: [-2.1816, -59.0217]
            )
    assert_equal(st.LATITUDE, -2.1816, fn_name & " lat",1)
    assert_equal(st.LONGITUDE, -59.0217, fn_name & " lon",1)
    assert_equal(st.NAME, "Heathrow", fn_name & " name",1)
    assert_equal(st.THRESHOLD, 0.1, fn_name & " threshold",1)
    
    st = stations(
            search_stations_database : "no",
            name: "ATTO",
            position: [-2.1816, -59.0217],
            threshold: 0.01,
            height: 2
            )
    assert_equal(st.LATITUDE, -2.1816, fn_name & " lat",2)
    assert_equal(st.LONGITUDE, -59.0217, fn_name & " lon",2)
    assert_equal(st.NAME, "ATTO", fn_name & " name",2)
    assert_equal(st.THRESHOLD, 0.01, fn_name & " threshold",2)
    assert_equal(st.HEIGHT, 2, fn_name & " height",2)
    
 end test_db

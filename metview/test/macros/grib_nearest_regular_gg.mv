# Metview Macro

# **************************** LICENSE START ***********************************
#
# Copyright 2019 ECMWF. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

include "test_utils.mv"
include "grib_nearest_utils.mv"

global USE_NEAREST_NON_MISSING = 0

# regular Gaussian
test_reg_gg_latlons()
test_reg_gg_subarea()
test_reg_gg_subarea_latlons()

loop ctl in [0, 1]
	
    USE_NEAREST_NON_MISSING = ctl
    
    test_reg_gg_small()
  
end loop


#-------------------------------------------------
# Testing small regular Gaussian grid
#-------------------------------------------------
function test_reg_gg_small()
	print(' ')
	print('Test: test_reg_gg_small USE_NEAREST_NON_MISSING=' & USE_NEAREST_NON_MISSING)
	test_interpolate_on_original_gridpoints('t1000_reg_gg_N48.grib', 0.000000001, 'vector')
end test_reg_gg_small

function test_reg_gg_latlons()
    print(' ')
    print('Test: test_reg_gg_latlons')
    fnName = "test_reg_gg_latlons"
  
    #precision(12)
  
    files = ["t1000_reg_gg_N48.grib"] 
    loop fname in files
        data = read(fname)
        print("fname=", fname)
        lon = longitudes(data)
        lat = latitudes(data)
        
        lat_index = |1,2,3,193,385|
        lat_ref = |88.572168514, 88.572168514, 88.572168514, 86.7225309547, 84.861970292|
        #loop idx in lat_index
        #    print(lat[idx])
        #end loop
        assert(vec_same(lat[lat_index], lat_ref, 1E-4), fnName & " lat", 1)
        
        lon_index = |1,2,3,193,385|
        lon_ref = |0,1.875,3.75,0,0|

        #loop idx in lon_index
        #    print(lon[idx])
        #end loop
        assert(vec_same(lon[lon_index], lon_ref, 1E-4), fnName & " lon", 1)
        
    end loop
end test_reg_gg_latlons

#-------------------------------------------------
# Testing subarea of regular Gaussian grid
#-------------------------------------------------
function test_reg_gg_subarea()
	print(' ')
	print('Testing subarea regular Gaussian grid')
	data = read('regular_gaussian_subarea.grib')

    # test all points with longitude values in both 0-360 range and -180-+180 range
    loop lon_offset in [0, 360]
	   # point in the middle with lon in 0-360 range
	   print('nearest_gridpoint lon_offset=', lon_offset)
	   test_nearest_gridpoint_value(data, 37.0,  -75    + lon_offset, -1.9101) # in the middle of the area
	   test_nearest_gridpoint_value(data, 15.09, -100.9 + lon_offset, -3.69)   # towards South-West corner
	   test_nearest_gridpoint_value(data, 15.40, -101.2 + lon_offset, -3.04)   # towards South-West corner
	   test_nearest_gridpoint_value(data, 56.87, -38.87 + lon_offset,  4.28)   # towards North-East corner
    
       print('interpolate lon_offset=', lon_offset)
       test_interpolate(data, 37.0,  -75    + lon_offset, -2.75669094004) # in the middle of the area
	   test_interpolate(data, 15.09, -100.9 + lon_offset, -3.61352386547)   # towards South-West corner
	   test_interpolate(data, 15.40, -101.2 + lon_offset, -2.93612764983)   # towards South-West corner
	   test_interpolate(data, 56.87, -38.87 + lon_offset,  4.49194312115)   # towards North-East corner
       if lon_offset = 0 then
            test_interpolate(data, 56.87, 28 + lon_offset,  nil) # point outside domain 
       end if
    end loop

    print(' ')
    print('Testing subarea regular Gaussian grid GRIB2')
    data = read('regular_gaussian_subarea_g2.grib')
    # test all points with longitude values in both 0-360 range and -180-+180 range
    loop lon_offset in [0, 360]
       print('nearest_gridpoint lon_offset=', lon_offset)
       test_nearest_gridpoint_value(data, 37.0,  -75    + lon_offset, -1.9101) # in the middle of the area
	   test_nearest_gridpoint_value(data, 15.09, -100.9 + lon_offset, -3.69)   # towards South-West corner
	   test_nearest_gridpoint_value(data, 15.40, -101.2 + lon_offset, -3.04)   # towards South-West corner
	   test_nearest_gridpoint_value(data, 56.87, -38.87 + lon_offset,  4.28)   # towards North-East corner
	  
       print('interpolate lon_offset=', lon_offset)
       test_interpolate(data, 37.0,  -75    + lon_offset, -2.75669094004) # in the middle of the area
	   test_interpolate(data, 15.09, -100.9 + lon_offset, -3.61352386547)   # towards South-West corner
	   test_interpolate(data, 15.40, -101.2 + lon_offset, -2.93612764983)   # towards South-West corner
	   test_interpolate(data, 56.87, -38.87 + lon_offset,  4.49194312115)   # towards North-East corner
       if lon_offset = 0 then
          test_interpolate(data, 56.87, 28 + lon_offset,  nil) # point outside domain
       end if
    end loop

	#test_interpolate_on_original_gridpoints('regular_gaussian_subarea.grib', 0.000000001, 'vector')
end test_reg_gg_subarea

function test_reg_gg_subarea_latlons()
    print(' ')
    print('Test: test_reg_gg_subarea_latlons')
    fnName = "test_reg_gg_subarea_latlons"
  
    #precision(12)
  
    files = ["regular_gaussian_subarea.grib", "regular_gaussian_subarea_g2.grib"] 
    loop fname in files
        grib2 = (fname = "regular_gaussian_subarea_g2.grib")
        data = read(fname)
        print("fname=", fname)
        lon = longitudes(data)
        lat = latitudes(data)
        
        lat_index = |1,2,3,224,447|
        lat_ref = |56.9086383162, 56.9086383162, 56.9086383162, 56.6276081563, 56.3465779923|
        #loop idx in lat_index
        #    print(lat[idx])
        #end loop
        assert(vec_same(lat[lat_index], lat_ref, 1E-4), fnName & " lat", 1)
        
        lon_index = |1,2,3,224,447|
        lon_ref = |258.75, 259.031, 259.312, 258.75, 258.75|

        #loop idx in lon_index
        #    print(lon[idx])
        #end loop
        assert(vec_same(lon[lon_index], lon_ref, 1E-4), fnName & " lon", 1)
        
    end loop
end test_reg_gg_subarea_latlons


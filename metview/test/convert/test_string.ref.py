# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_string.mv
# at: 2023-09-21 20:34:46
# using version: Metview 5.19.2


import metview as mv


# case 1
print("case_1")
v = "12a"
print(v)
print(" ")

# case 2
print("case_2")
v = str("ab12_")
print(v)
print(" ")

# case 3
print("case_3")
v = "abc"
print("\t", v)
print(" ")

# case 4
print("case_4")
v = "abc"
print(v, "\n", "nnn")
print(" ")

# case 5
print("case_5")
v = "abc"
print(len(v))
print(" ")


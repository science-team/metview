# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_icon.mv
# at: 2023-09-22 14:25:05
# using version: Metview 5.19.2


import metview as mv


cont_1 = mv.mcont(
    legend="on",
    contour_line_style="dash",
    contour_line_thickness=2,
    contour_line_colour="lavender",
    contour_max_level=200,
    contour_label_height=0.4,
    contour_label_colour="greenish_blue",
    contour_shade="on",
    contour_shade_colour_method="list",
    contour_shade_method="area_fill",
    contour_shade_colour_list=["purplish_blue", "RGBA(1,0.9647,0,0.6392)", "orange"],
    contour_legend_text="My legend"
    )

cont_2 = mv.mcont(legend="on")

cont_3 = mv.mcont()

res = mv.read("tuv_pl.grib")

fname = "tuv_pl.grib"

res1 = mv.read(fname)
print(len(res1))

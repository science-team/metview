# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_parenthesis.mv
# at: 2023-09-21 20:34:45
# using version: Metview 5.19.2


import numpy as np

import metview as mv

def fn(v):
    return v + 2


# case 1
print("case_1")
v = 1 + 3 * (4 - 2)
print(v)
print(" ")

# case 2
print("case_2")
a = 8
v = 2 * (3 / (4 - 2) + 5) + 6
print(v)
print(" ")

# case 3
print("case_3")
a = "text"
v = mv.compat.concat(a, ((1 + (3 * 2 - 8) * 2) * 3 - 1))
print(v)
print(" ")

# case 4
print("case_4")
v = fn(fn(1) * 2 + fn(fn(fn(4))))
print(v)
print(" ")


ecbuild_configure_file(macro_convert_test.sh.in macro_convert_test.sh @ONLY)

set(MACRO_FILES
    test_case.mv
    test_comment.mv
    test_concat.mv
    test_date.mv
    test_definition.mv
    test_do_while.mv
    test_for_int.mv
    test_for_float.mv
    test_for_function.mv
    test_globals.mv
    test_icon.mv
    test_icon_def.mv
    test_icon_embed.mv
    test_if.mv
    test_if_function.mv
    test_include.mv
    test_indices.mv
    test_inline_c.mv
    test_list.mv
    test_loop.mv
    test_nil.mv
    test_parenthesis.mv
    test_repeat_until.mv
    test_string.mv
    test_ui.mv
    test_user_function.mv
    test_vector.mv
    test_when.mv
)

set( _TEST_RUNNER "convert_test_runner.mv" )
set( _TEST_SCRIPT "macro_convert_test.sh")
set( _TEST_RESOURCES myfunc_include.mv myfunc_include.py ../data/tuv_pl.grib)

foreach(_MV ${MACRO_FILES})

    string( REGEX REPLACE "\.mv$" ".ref.py" _PY_REF ${_MV} )
    string( REGEX REPLACE "\.mv$" ".py" _PY_RES ${_MV} )

    ecbuild_add_test(TARGET ${_MV}.test
        COMMAND ${FULL_STARTUP_SCRIPT_PATH}
        TYPE SCRIPT
        ARGS -nocreatehome -b  ${_TEST_RUNNER} ${_TEST_SCRIPT} ${CMAKE_CURRENT_BINARY_DIR} ${_MV} ${_PY_RES} ${_PY_REF}
        RESOURCES ${_TEST_RUNNER} ${_TEST_RESOURCES} ${_MV} ${_PY_REF})

endforeach()

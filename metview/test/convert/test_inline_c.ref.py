# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_inline_c.mv
# at: 2023-09-21 20:34:45
# using version: Metview 5.19.2


import numpy as np

import metview as mv

def vector_test(v):

    inline_C = """
#include <stdio.h>
#include <string.h>
#include "macro_api.h"
int main()
{
    int n = mci_arg_count();
   
    if (n != 1)
    {
        mci_fail("mci_arg_count should be 1");
        return 0;
    }
   
    double* vec;
    int     len;
    mci_get_vector(&vec, &len);
    if (len != 5)
    {
        mci_fail("mci_get_vector len should be 5");
        return 0;
    }
    if (vec[0] != 11 || vec[1] != 12 || vec[2] != 13 || vec[3] != 14 || vec[4] != 15)
    {
        int i;
        for (i = 0; i < len; i++)
            printf("%f ", vec[i]);
        mci_fail("mci_get_vector vector contents are wrong");
    }
    /* multiply all values by 2 and return this as the result*/
    int i;
    for (i = 0; i < len; i++)
        vec[i] *= 2;

    mci_return_vector( vec, 5 );
}
    """


# inline 'C' function which returns the number of values in the given field.
# Note that this is only a test - it is not necessary to use inline 'C' for
# this - the following line of Macro code will also do it:
#  numvals = count(values(field))

# -------------------------------------------------------------

fs = mv.read("z500.grb")
#r = grib_test(fs)
#print (r)

#if r <> 29040 then
#    fail('Inline C function failed - should have returned 29040; got ' & r)
#end if

vec1 = np.array([11, 12, 13, 14, 15])
vect2 = vector_test(vec1)
print(vect2)


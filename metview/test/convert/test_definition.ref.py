# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_definition.mv
# at: 2023-09-22 10:57:46
# using version: Metview 5.19.2


import metview as mv


a = {"data": 1, "num": 2}

print(a["data"])
print(a["num"])

print(a["data"])
print(a["num"])

print(a["data"] + 1)
print(a["num"] + 1)

print(a["data"] + 1)
print(a["num"] + 1)

a = {"data": 0, "num": 1}
print(a["data"])
print(a["num"])

a["data"] = 2
print(a["data"])

n = 2
page_logo = {"TOP": 0, "BOTTOM": 100, "LEFT": 0, "RIGHT": 100, "ID": n}

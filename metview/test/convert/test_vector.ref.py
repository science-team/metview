# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_vector.mv
# at: 2023-09-21 21:38:42
# using version: Metview 5.19.2


import numpy as np

import metview as mv


# case 1
print("case_1")
v = np.array([1, 2.5, 3])
print(len(v))
print(v[1])
print(" ")

# case 2
print("case_2")
v[1] = 5
print(v[1])
print(" ")

# case 3
print("case_3")
v = np.zeros(10)
v[1] = 12.3
print(len(v))
print(v[1])
print(" ")

# case 4
print("case_4")
n = 5
v = np.zeros(n)
v[1] = 1.3
print(len(v))
print(v[1])
print(" ")

# case 5
print("case_5")
v = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
print(v[0:2])
print(v[0:6:2])
print(v[mv.compat.index4(v, 0, 10, 4, 2)])
print(" ")

# case 6
print("case_6")
a = [1, 2, 3]
v = np.zeros(a)
print(len(v))
print(v[0:2])
print(" ")

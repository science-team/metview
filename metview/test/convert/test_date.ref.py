# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_date.mv
# at: 2023-09-21 20:34:44
# using version: Metview 5.19.2


import datetime

import metview as mv


# case 1
print("case_1")
d = datetime.date(2019, 3, 21)
print(d)
print(" ")

# case 2
print("case_2")
t = datetime.time(3, 11, 32)
print(t)
print(" ")

# case 3
print("case_3")
d = datetime.datetime(2021, 3, 21, 5, 11, 32)
print(d)
print(" ")

# case 4
print("case_4")
d = mv.date(20220321)
print(d)
print(" ")

# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_case.mv
# at: 2023-09-21 20:34:44
# using version: Metview 5.19.2


import metview as mv


# case 1
print("case_1")
k = 10
if k == 8:
    print("8")
elif k == "alma":
    print("fruit")
else:
    print("otherwise")

print(" ")

# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_icon_def.mv
# at: 2023-10-23 18:00:46
# using version: Metview 5.19.2


import datetime

import metview as mv


# part of the icon is defined by a definition

base_cont_1 = {"legend": "on", "contour_line_style": "dash"}

base_cont_2 = {"contour_label_height": 0.4}

cont_1 = mv.mcont(
    **base_cont_1,
    contour_line_thickness=2,
    contour_line_colour="lavender",
    contour_max_level=200,
    **base_cont_2,
    contour_label_colour="greenish_blue"
    )

cont_2 = mv.mcont(base_cont_1)
print(cont_2)

cont_3 = mv.mcont(
    **base_cont_1,
    **base_cont_2
    )

side_text = {"text_colour": "black", "text_font_size": 0.5}
annotation_left_box = {"text_mode": "positional", "text_box_x_position": 0, "text_box_y_position": 0, "text_box_x_length": 1, "text_box_y_length": 4, "text_justification": "right", "text_orientation": "top_bottom", "text_border": "off"}
anno_left = mv.mtext(
    **side_text,
    **annotation_left_box,
    text_line_count=1,
    text_line_1="Date created: " + str(mv.string(datetime.datetime.now(), "yyyy-mm-dd"))
    )

print(anno_left)

# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_loop.mv
# at: 2023-09-21 20:34:45
# using version: Metview 5.19.2


import metview as mv


# case 1
print("case_1")
for i in [1, 2, 3]:
    print(i)

print(" ")

# case 2
print("case_2")
v = [1, 2, 3]
for i in v:
    print(i)


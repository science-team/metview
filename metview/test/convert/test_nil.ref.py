# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_nil.mv
# at: 2023-09-21 20:34:45
# using version: Metview 5.19.2


import metview as mv


# case 1
print("case_1")
v = None
if v is None:
    print("v is nil")

print(" ")

# case 2
print("case_2")
v = 2
if v is not None:
    print("v is not nil")

print(" ")

# case 3
print("case_3")
v = None
print(v)
print(" ")

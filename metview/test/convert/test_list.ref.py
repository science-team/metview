# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_list.mv
# at: 2023-09-21 20:34:45
# using version: Metview 5.19.2


import metview as mv


v = [1, 2, "a", 1.34]
print(len(v))
print(v[1])
print(v[2])

v[1] = "metview"
print(v[1])

v = [1, 5, 6, "c", 12, 18]
print(len(v))
print(v[1])

print(v[0:2])
print(v[0:6:2])

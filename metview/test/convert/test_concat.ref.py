# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_concat.mv
# at: 2023-09-21 20:34:44
# using version: Metview 5.19.2


import numpy as np

import metview as mv


# case 1
print("case_1")
v = "my" + " long " + "text"
print(v)
print(" ")

# case 2
print("case_2")
a = "12"
v = "19" + str(a) + "23"
print(v)
print(" ")

# case 3
print("case_3")
v = None
v = mv.compat.concat(v, [1])
v = mv.compat.concat(v, [2])
print(v)
print(" ")

# case 4
print("case_4")
v = None
v = mv.compat.concat(v, ["a"])
print(v)
v = mv.compat.concat(v, ["b", "c"])
print(v)
v = mv.compat.concat(v, [["d", "e"]])
print(v)
v = mv.compat.concat(v, [mv.compat.concat(["f", "g"], ["h"])])
print(v)
print(" ")

# case 5
print("case_5")
v = []
v = mv.compat.concat(v, ["abc"])
print(v)
print(" ")

# case_6
print("case_6")
f = mv.read("tuv_pl.grib")
g = mv.compat.concat(f[0], f[1], f[2])
print(len(g))
print(" ")

# case_7a
print("case_7a")
f = mv.read("tuv_pl.grib")
g = None
g = mv.compat.concat(g, f[0])
print(len(g))
print(" ")

# case_7b
print("case_7b")
f = mv.read("tuv_pl.grib")
g = None
g = mv.compat.concat(g, f[0], f[1])
print(len(g))
print(" ")

# case_7c
print("case_7c")
f = mv.read("tuv_pl.grib")
g = None
g = mv.compat.concat(g, f[0], f[1:4])
print(len(g))
print(" ")

# case_8
print("case_8")
v1 = np.array([1, 3, 5, 7])
v2 = np.array([9, 10])
v = mv.compat.concat(v1, v2)
print(v)
v3 = np.array([14, 15])
v = mv.compat.concat(v1, v2, v3)
print(v)
print(" ")

# case_9
print("case_9")
v1 = None
v2 = np.array([9, 10])
v = mv.compat.concat(v1, v2)
print(v)
print(" ")

# case_10
print("case_10")
v1 = None
v2 = np.array([9, 10])
v3 = np.array([14, 15])
v = mv.compat.concat(v1, v2, v3)
print(v)
print(" ")

# case_11
print("case_11")
v = "a" + str(1) + "11" + str(mv.round(1.374, 2))
print(v)
print(" ")

# case_12
print("case_12")
v = "" + "a" + str(1) + "12"
print(v)
print(" ")

# case_13
print("case_13")
v = "a" + "" + str(1) + "13"
print(v)
print(" ")

# case_14
print("case_14")
v = "a" + str(1) + "14"
print(v)
print(" ")

# case_15
print("case_14")
v = "a" + str(1) + "15"
print(v)
print(" ")

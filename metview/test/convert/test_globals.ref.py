# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_globals.mv
# at: 2023-09-21 20:34:44
# using version: Metview 5.19.2


import metview as mv

A = 1
MY_VAR = "something"

def fn3():
    b = A + 1
    return b

def fn2(b):
    def _inner():
        global A
        A = A + 2
        return A
    

    return b + _inner()

def fn1():
    global A
    A = A + 7
    return A


print("A=", A)
print("MY_VAR=", MY_VAR)

MY_VAR = "value"

print(fn1())
print(fn2(10))
print(fn3())

A = 5
print(fn1())
print(fn2(10))
print(fn3())


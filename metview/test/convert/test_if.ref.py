# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_if.mv
# at: 2023-09-21 20:34:45
# using version: Metview 5.19.2


import metview as mv


# case 1
i = 0
k = 0
if i == 1:
    k = 1
elif i == 2:
    k = i * 2
elif i <= 0:
    k = -1
elif i >= 10:
    k = 10
else:
    k = 100

print("k=", k)

# case 2
i = 0
k = 0
if i != 1:
    k = 1
else:
    k = 100

print("k=", k)

# case 3
i = 0
k = 0
j = 10
if i == 1:
    if j <= 10:
        k = 10
    else:
        k = -10
    
elif i > 10:
    k = 100
else:
    k = -1

print("k=", k)

# case 4
i = 0
k = 0

if i <= 0 and i > -10:
    k = -1
else:
    k = 10

print("k=", k)


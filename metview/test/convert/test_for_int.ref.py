# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_for_int.mv
# at: 2023-10-23 15:44:46
# using version: Metview 5.19.2


import metview as mv


# case 1
print("case_1")
k = 0
for i in range(1, 9 + 1):
    print(" ", i)
    k = k + i

print("k=", k)
print(" ")

# case 2
print("case_2")
k = 0
for i in range(-1, 9 + 1):
    print(" ", i)
    k = k + i

print("k=", k)
print(" ")

# case 3
print("case_3")
k = 0
for i in range(1, 10 + 2, 2):
    print(" ", i)
    k = k + i

print("k=", k)
print(" ")

# case 4
print("case_4")
k = 0
for i in range(10, -2 + (-2), -2):
    print(" ", i)
    k = k + i

print("k=", k)
print(" ")

# case 5
print("case_5")
s_idx = 1
e_idx = 9
k = 0
# Warning: for()
# The following for loop might contain float values and need manual adjustment.
# See the following page for details:
#   https://metview.readthedocs.io/en/latest/metview/macro/convert/convert_for.html
for i in range(s_idx, e_idx + 1):
    print(" ", i)
    k = k + i

print("k=", k)
print(" ")

# case 6
print("case_6")
s_idx = 1
e_idx = 9
k = 0
# Warning: for()
# The following for loop might contain float values and need manual adjustment.
# See the following page for details:
#   https://metview.readthedocs.io/en/latest/metview/macro/convert/convert_for.html
for i in range(s_idx, e_idx + 1):
    print(" ", i)
    k = k + i

print("k=", k)
print(" ")

# case 7
print("case_7")
s_idx = 9
e_idx = 1
k = 0
# Warning: for()
# The following for loop might contain float values and need manual adjustment.
# See the following page for details:
#   https://metview.readthedocs.io/en/latest/metview/macro/convert/convert_for.html
for i in range(s_idx, e_idx + (-1), -1):
    print(" ", i)
    k = k + i

print("k=", k)
print(" ")

# case 8
print("case_8")
s_idx = 9
e_idx = 1
step = -1
k = 0
# Warning: for()
# The following for loop might contain float values and need manual adjustment.
# See the following page for details:
#   https://metview.readthedocs.io/en/latest/metview/macro/convert/convert_for.html
for i in range(s_idx, e_idx + step, step):
    print(" ", i)
    k = k + i

print("k=", k)
print(" ")

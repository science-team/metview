# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_do_while.mv
# at: 2023-09-21 20:34:44
# using version: Metview 5.19.2


import metview as mv


# case 1
print("case_1")
i = 1
while i < 3:
    print(i)
    i = i + 1

print(" ")

# case 2
print("case_2")
i = 1
while i < 3:
    print(i)
    i = i + 1

print(" ")

# case 3
print("case_3")
i = 0
while i < 3 and i >= 0:
    print(i)
    i = i + 1

print(" ")

# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_repeat_until.mv
# at: 2023-09-21 20:34:46
# using version: Metview 5.19.2


import metview as mv


# case 1
print("case_1")
i = 0
while True:
    print(i)
    i = i + 1
    if i > 2:
        break

print(" ")

# case 1
print("case_1")
i = 0
while True:
    print(i)
    i = i + 1
    if i > 2 and i > 3:
        break

print(" ")

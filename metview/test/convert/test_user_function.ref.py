# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_user_function.mv
# at: 2023-09-21 20:34:46
# using version: Metview 5.19.2


import metview as mv

def f4(a):
    
    b = mv.sin(a)
    return b
    

def f3(a, b, c):
    
    c = c + a + b + 1
    return [a, c]
    

def f2(a):
    def _inner(b):
        b = b + 2
        return b
    

    
    b = a + 1
    return _inner(b)
    

def f1(a):
    b = a + 1
    return b + 1
    


# case 1
print("case_1")
v = f1(1)
print(v)
print(f1(1))
print(" ")

print("case_2")
v = f2(1)
print(v)
print(f2(1))
print(" ")

print("case_3")
v = [1, f1(0), "a", f2(f1(0)), f2(0) + f1(0)]
print(v[0])
print(v[1])
print(v[2])
print(v[3])
print(v[4])
print(" ")

print("case_4")
v = f3(1, 2, 5)
print(v[0])
print(v[1])
print(" ")

print("case_5")
c = 7
v = f3(1, 2, c)
print(v[0])
print(v[1])
print(c)
print(" ")

print("case_6")
c = 3.14 / 2
v = f4(c)
print(v)
print(" ")

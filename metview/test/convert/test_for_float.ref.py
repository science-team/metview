# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_for_float.mv
# at: 2023-10-23 15:44:51
# using version: Metview 5.19.2


import numpy as np

import metview as mv


# case 1
print("case_1")
k = 0
for i in np.arange(float(-2), float(4) + 0.5 / 2., 0.5):
    k = k + i
    print(" ", i)

print("k=", k)
print(" ")

# case 2
print("case_2")
k = 0
for i in np.arange(float(4), float(-2) + (-0.5) / 2., -0.5):
    k = k + i
    print(" ", i)

print("k=", k)
print(" ")

# case 3
print("case_3")
k = 0
for i in np.arange(-2.5, float(4) + float(1) / 2., float(1)):
    k = k + i
    print(" ", i)

print("k=", k)
print(" ")

# case 4
print("case_4")
k = 0
for i in np.arange(float(-2), 3.5 + float(1) / 2., float(1)):
    k = k + i
    print(" ", i)

print("k=", k)
print(" ")

# case 5
print("case_5")
k = 0
a = 2.5
for i in np.arange(float(a), float(4) + 0.5 / 2., 0.5):
    k = k + i
    print(" ", i)

print("k=", k)
print(" ")

# case 6
print("case_6")
delta = 0.5
k = 0
# Warning: for()
# The following for loop might contain float values and need manual adjustment.
# See the following page for details:
#   https://metview.readthedocs.io/en/latest/metview/macro/convert/convert_for.html
for i in range(4, -2 + mv.neg(delta), mv.neg(delta)):
    k = k + i
    print(" ", i)

print("k=", k)
print(" ")

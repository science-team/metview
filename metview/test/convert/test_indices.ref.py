# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_indices.mv
# at: 2023-10-23 15:43:35
# using version: Metview 5.19.2


import metview as mv


print("Case 1")
for i in range(1, 5 + 1):
    k = i + 1
    print("k=", k)

print(" ")

print("Case 2")
v = [1, 5, 8, 2, 12, 4]
# Warning: for()
# The following for loop might contain float values and need manual adjustment.
# See the following page for details:
#   https://metview.readthedocs.io/en/latest/metview/macro/convert/convert_for.html
for i in range(1, len(v) + 1):
    k = v[i - 1] + 1
    print("k=", k)

print(" ")

print("Case 3")
v1 = 1
v2 = len(v)
# Warning: for()
# The following for loop might contain float values and need manual adjustment.
# See the following page for details:
#   https://metview.readthedocs.io/en/latest/metview/macro/convert/convert_for.html
for i in range(v1, v2 + 1):
    k = v[i - 1] + 1
    print("k=", k)

print(" ")

print("Case 4")
i = 4
j = v[i - 1]
print("j=", j)
k = v[j - 1]
print("k=", k)
k = v[v[i - 1] - 1]
print("k=", k)
k = v[v[i - 3 - 1] + 2 - 1]
print("k=", k)
k = v[i + j - 1]
print("k=", k)
print(" ")


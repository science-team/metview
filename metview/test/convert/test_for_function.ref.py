# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_for_function.mv
# at: 2023-10-23 15:44:57
# using version: Metview 5.19.2


import metview as mv

def test_3(k):
    
    # Warning: for()
    # The following for loop might contain float values and need manual adjustment.
    # See the following page for details:
    #   https://metview.readthedocs.io/en/latest/metview/macro/convert/convert_for.html
    for i in range(1, add_2(k) + 1):
        print(i)
    
    

def test_2(k):
    
    # Warning: for()
    # The following for loop might contain float values and need manual adjustment.
    # See the following page for details:
    #   https://metview.readthedocs.io/en/latest/metview/macro/convert/convert_for.html
    for i in range(1, k + 1):
        print(i)
    
    

def add_2(i):
    return i + 2


# case 1
print("case_1")
# Warning: for()
# The following for loop might contain float values and need manual adjustment.
# See the following page for details:
#   https://metview.readthedocs.io/en/latest/metview/macro/convert/convert_for.html
for i in range(1, add_2(2) + 1):
    print(i)

print(" ")

# case 2
print("case_2")
test_2(2)
print(" ")

# case 3
print("case_3")
test_3(2)
print(" ")


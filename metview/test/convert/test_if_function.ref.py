# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_if_function.mv
# at: 2023-09-21 20:34:45
# using version: Metview 5.19.2


import metview as mv

def test_4(i):
    
    if i <= 0 and i > -10:
        k = -1
    else:
        k = 10
    
    
    return k

def test_3(i, j):
    
    if i == 1:
        if j <= 10:
            k = 10
        else:
            k = -10
        
    elif i > 10:
        k = 100
    else:
        k = -1
    
    
    return k

def test_2(i):
    
    if i != 1:
        k = -1
    else:
        k = 100
    
    
    return k
    

def test_1(i):
    
    if i == 1:
        k = 1
    elif i == 2:
        k = i * 2
    elif i <= 0:
        k = -1
    elif i >= 10:
        k = 10
    else:
        k = -100
    
    
    return k
    


# case 1
print("case 1")
for i in [-1, 0, 1, 2, 10, 100]:
    print(i, " -> ", test_1(i))

print(" ")

# case 2
print("case 2")
for i in [-1, 0, 1, 10]:
    print(i, " -> ", test_2(i))

print(" ")

# case 3
print("case 3")
for i in [-1, 0, 1, 10]:
    for j in [5, 10, 12]:
        print(i, " ", j, " -> ", test_3(i, j))
    

print(" ")

# case 4
print("case 4")
for i in [-20, -1, 0, 1, 10]:
    print(i, " -> ", test_4(i))

print(" ")

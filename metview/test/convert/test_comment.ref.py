# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_comment.mv
# at: 2023-12-06 09:03:31
# using version: Metview 5.20.0


import numpy as np

import metview as mv

def f4():
    print("f4")

def f3(x, a, b, e):
    print(a)

def f3(x, b, c, d, e):
    print(x)

def f2(b, c, d):
    print(b)

def f1(b, c, d, e):
    print(b)

# function definitions
def f(a, b, c, d, e):
    print(a)


# first
#second
a = 1
# third

if a == 2:
    # another
    a = 3

# definitions
a = {"a": 1, "b": 2}

n = 2
page_logo = {"TOP": 0, "BOTTOM": 100, "LEFT": 0, "RIGHT": 100, "ID": n}

page_logo = {"BOTTOM": 100, "LEFT": 0, "RIGHT": 100, "ID": n}

page_logo = {"LEFT": 0, "RIGHT": 100, "ID": n}

page_logo = {"TOP": 0, "LEFT": 0, "RIGHT": 100, "ID": n}

page_logo = {"TOP": 0, "BOTTOM": 100, "LEFT": 0, "RIGHT": 100}

# function calls
f1(1, 2, 3, 4)

f1(1, 2, 3, 4)

f1(1, 2, 3, 4)

f1(1, 2, 3, 4)

# vector

v = np.array([1, 2, 3])

v = np.array([1, 2, 3])

v = np.array([1, 2, 3])

# list

v = [1, 2, 3]

v = [1, 2, 3]

v = [1, 2, 3]

# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_ui.mv
# at: 2023-12-07 12:08:44
# using version: Metview 5.20.0


import metview as mv


a = mv.ui.icon(
    name="input data",
    class_="GRIB"
    )

b = mv.ui.any(
    name="verif date (yyyy-mm-dd)",
    default="2012-03-01",
    help="help_script",
    help_script_command="echo Dates must be in YYYY-MM-DD format"
    )

c = mv.ui.slider(
    name="days",
    min=1,
    max=10,
    default=5
    )

d = mv.ui.option_menu(
    name="parameter",
    values=["UV", "t"],
    default="t"
    )

e = mv.ui.any(
    name="area",
    help="help_input",
    input_type="area",
    default=[30, -25, 50, 65]
    )

f = mv.ui.colour(
    name="isoline_colour",
    default="green"
    )

g = mv.ui.toggle(
    name="show_title",
    default="on"
    )

# retrieve the user input values
x = mv.ui.dialog([a, b, c, d, e, f, g])

print("area=", x["area"])

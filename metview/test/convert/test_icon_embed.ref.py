# This Python code was automatically generated from the following Metview Macro:
# /Users/cgr/metview/ctest/convert/test_icon_embed.mv
# at: 2023-09-21 20:34:45
# using version: Metview 5.19.2


import metview as mv


data_for_gribthermo = mv.retrieve(
    param=["t", "q"],
    grid=[1, 1]
    )

thermo_data = mv.thermo_grib(
    coordinates=[10, 10],
    point_extraction="nearest_gridpoint",
    data=data_for_gribthermo
    )


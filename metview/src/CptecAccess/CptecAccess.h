/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

/************************************************************
  Application CptecAccess

  Access GRIB/BUFR data from CPTEC's storage directories
************************************************************/
#include <Metview.h>

class CptecAccess : public MvService
{
public:
    CptecAccess(const char* name) :
        MvService(name){};

    void serve(MvRequest&, MvRequest&);

    // Get some input arguments
    bool get_common_input_info(const MvRequest&);

    // Build path
    bool build_path(MvRequest&);

    // Build filenames
    bool build_filenames(MvRequest&, std::vector<std::string>&);
    bool build_model_filenames(MvRequest&, std::vector<std::string>&);
    bool build_observation_filenames(MvRequest&, std::vector<std::string>&);
    bool build_bam_filenames(std::vector<std::string>&);

    // Build output file
    bool build_output_file(MvRequest&, std::vector<std::string>&, MvRequest&);
    bool build_model_output_file(MvRequest&, std::vector<std::string>&, std::string&);
    bool build_observation_output_file(MvRequest&, std::vector<std::string>&, std::string&);
    std::string build_bam_output_file(std::vector<std::string>&);

private:
    std::vector<std::string> sdates_;
    std::vector<std::string> sparams_;
    std::vector<int> ilevels_;
    std::vector<int> itimes_;
    std::vector<int> isteps_;
    std::string spath_;
    std::string slevel_type_;
    std::string model_res_;
    // float resX_{0.};
    // float resY_{0.};
};

#
#          CPTEC ACCESS EDITOR: definition file
#
CPTEC_ACCESS
{
  TYPE
  {
  MODEL ; MODEL
  OBSERVATION ; OBSERVATION
  } =  MODEL

  OBSERVATION 
  {
  SYNOP ; SYNOP
  ACARS ; ACARS
  AIREP ; AIREP
  AMDAR ; AMDAR
  AWS ; AWS
  BUOY ; BUOY
  METAR ; METAR
  SHIP ; SHIP
  TEMP ; TEMP
  SHIP ; SHIP
  UPPERAIR ; UPPERAIR
  AIRCRAFT ; AIRCRAFT
  PREPBUFR ; PREPBUFR
  EMA_INMET ; EMA_INMET
  1bamua ; 1bamua
  1bamub ; 1bamub
  1bhrs3 ; 1bhrs3
  airsev ; airsev
  gpsro ; gpsro
  mtiasi ; mtiasi
  satwnd ; satwnd
  SAnl ; SAnl	
  } = SYNOP

  MODEL
  {
    BAM ; BAM
    BAM_HIBRIDO ; BAM_HIBRIDO
    Eta_15Km_1h    ; ETA151
    BRAMS 05 	   ; BRAMS05
    MCGA - 3Dvar  ;  MCGA3DVAR
    GLOBAL T299       ; GLOBAL299
    GLOBAL T213       ; GLOBAL213
    Eta Nordeste	; ETANOR
    Eta Vale		; ETAVALE

  } = BAM

  LEVEL_TYPE
  {
    SURFACE        ; sfc # 01
    10_METER       ; 105
    PRESSURE_LEVEL ; pl #100
    MODEL_LEVEL    ; ml #107
  } = pl

  LEVEL_LIST
  {
    *
    /
  } = 1000/850/700/500/300

  PARAMETER
[ help = help_multiple_selection,  # For Metview
			java_control = LIST_2, 
			marsdoc      = field/all,
			mars_interface         = list,
			java_section = METEO, java_hidden = FALSE ]
  {


# Variaveis em comum
TOPOGRAPHY                                    ;  topo  ;132
SURFACE PRESSURE                              ;  pslc  ;135
LAND SEA MASK                                 ;  lsmk  ;81
2 METRE TEMPERATURE                           ;  tp2m  ;128
10 METRE U-WIND COMPONENT                     ;  u10m  ;130
10 METRE V-WIND COMPONENT                     ;  v10m  ;131
TOTAL PRECIPITATION                           ;  prec  ;61
CONVECTIVE PRECIPITATION                      ;  prcv  ;63
LATENT HEAT FLUX FROM SURFACE                 ;  clsf  ;121
SENSIBLE HEAT FLUX FROM SURFACE               ;  cssf  ;122
SURFACE TEMPERATURE                           ;  tsfc  ;187
GROUND/SURFACE COVER TEMPERATURE              ;  tgsc  ;191
SURFACE ZONAL WIND STRESS                     ;  usst  ;193
SURFACE MERIDIONAL WIND STRESS                ;  vsst  ;195
DOWNWARD SHORT WAVE AT GROUND                 ;  ocis  ;209
DOWNWARD LONG WAVE AT BOTTOM                  ;  olis  ;207
UPWARD LONG WAVE AT BOTTOM                    ;  oles  ;211
UPWARD SHORT WAVE AT TOP                      ;  roce  ;214
OUTGOING LONG WAVE AT TOP                     ;  role  ;114
CONVECTIVE AVAIL. POT.ENERGY                  ;  cape  ;140
CONVECTIVE INHIB. ENERGY                      ;  cine  ;141
GEOPOTENTIAL HEIGHT                           ;  zgeo  ;7
ZONAL WIND - U                                ;  uvel  ;33
MERIDIONAL WIND - V                           ;  vvel  ;34
ABSOLUTE TEMPERATURE                          ;  temp  ;11
RELATIVE HUMIDITY                             ;  umrl  ;52
OMEGA                                         ;  omeg  ;39
SPECIFIC HUMIDITY                             ;  umes  ;51

#Variaveis Global
SURFACE ZONAL WIND - U                        ;  UVES    ;192
SURFACE MERIDIONAL WIND - V                   ;  VVES    ;194
VORTICITY                                     ;  VORT   ;43
STREAM FUNCTION                               ;  FCOR   ;35
VELOCITY POTENTIAL                            ;  POTV   ;36
SEA LEVEL PRESSURE                            ;  PSNM    ;2
SURFACE ABSOLUTE TEMPERATURE                  ;  TEMS    ;188
ABSOLUTE TEMPERATURE                          ;  TEMP   ;11
SURFACE RELATIVE HUMIDITY                     ;  UMRS    ;226
INST. PRECIPITABLE WATER                      ;  AGPL    ;54
SPECIFIC HUMIDITY AT 2-M FROMSURFACE          ;  Q02M    ;199
CLOUD COVER                                   ;  CBNV    ;71
UPWARD SHORT WAVE AT GROUND                   ;  OCES    ;212
SHORT WAVE ABSORBED AT GROUND                 ;  OCAS    ;111
GROUND/SURFACE COVER TEMPERATURE              ;  TGSC    ;191
OZONE MIXING RATIO                            ;  O3MR   ;166
VERTICALLY INTEGRATED OZONECONTENT            ;  TOZN    ;10
DEW POINT TEMPERATURE                         ;  TPOR   ;17
SEVERE WEATHER THREAT                         ;  SWET    ;154

#Variaveis Eta

M S L PRESSURE - MESINGER METHOD              ;  pslm  ;136
MAXIMUM TEMPERATURE                           ;  mxtp  ;15
MINIMUM TEMPERATURE                           ;  mntp  ;16
2 METRE DEWPOINT TEMPERATURE                  ;  dp2m  ;129
100 METRE U-WIND COMPONENT                    ;  u100  ;216
100 METRE V-WIND COMPONENT                    ;  v100  ;217
LARGE SCALE PRECIPITATION                     ;  prge  ;62
SNOWFALL                                      ;  neve  ;64
TIME AVE GROUND HT FLX                        ;  ghfl  ;198
SURFACE SPEC HUMIDITY                         ;  qsfc  ;181
SOIL TEMPERATURE OF ROOT ZONE                 ;  tgrz  ;175
SOIL WETNESS OF SURFACE                       ;  ussl  ;182
SOIL WETNESS OF ROOT ZONE                     ;  uzrs  ;183
SOIL MOISTURE AVAILABILITY                    ;  smav  ;174
RUNOFF                                        ;  rnof  ;178
ROUGHNESS LENGTH                              ;  zorl  ;83
POTENTIAL SFC EVAPORATION                     ;  evpp  ;177
LOW CLOUD COVER                               ;  lwnv  ;73
MEDIUM CLOUD COVER                            ;  mdnv  ;74
HIGH CLOUD COVER                              ;  hinv  ;75
MEAN CLOUD COVER                              ;  cbnt  ;149
ALBEDO                                        ;  albe  ;84
BEST LIFTED INDEX                             ;  bli   ;77
INST. PRECIPITABLE WATER                      ;  agpl  ;54
TROPOPAUSE PRESSURE                           ;  tppp  ;157
FREEZING LEVEL HEIGHT                         ;  fzht  ;152
FREEZING LEVEL RELATIVE HUMIDITY              ;  fzrh  ;153
MAXIMUM WIND PRESS. LVL                       ;  mxwp  ;146
MAXIMUM U-WIND                                ;  mxwu  ;138
MAXIMUM V-WIND                                ;  mxwv  ;139
PRESSURE AT CLOUD BASE                        ;  pcbs  ;150
PRESSURE AT CLOUD TOP                         ;  pctp  ;151
PSEUDO-ADIABATIC POTENTIAL TEMPERATURE        ;  psat  ;14
CLOUD WATER                                   ;  wtnv  ;76
CLOUD ICE                                     ;  itnv  ;58

  } = zgeo

  DATE
  {
     JANUARY      ;   JAN
     FEBRUARY     ;   FEB
     MARCH        ;   MAR
     APRIL        ;   APR
     MAY          ;   MAY
     JUNE         ;   JUN
     JULY         ;   JUL
     AUGUST       ;   AUG
     SEPTEMBER    ;   SEP
     OCTOBER      ;   OCT
     NOVEMBER     ;   NOV
     DECEMBER     ;   DEC
     *   
     CURRENT DATE ; 0
     YESTERDAY    ; -1
     TO           ; TO
     BY           ; BY
     /
     OFF
  } = CURRENT DATE

  TIME
  {
    *
    /
  } = 0

  STEP
  {
    TO  ;   TO
    BY  ;   BY    
    /
    *   
    } = 00

}

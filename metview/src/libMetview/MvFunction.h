/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

#ifndef DOXYGEN_SHOULD_SKIP_THIS

// it seems that this class is not used anywhere (2008-04-10/vk)

class MvFunction : public MvTransaction
{
    static MvFunction* First;

    const argdef* Arguments;
    int Count;
    const char* Name;
    MvFunction* Next;

protected:
    virtual const char* getInfo(void) { return "No help available"; }
    MvFunction(MvTransaction* f) :
        MvTransaction(f) {}

public:
    MvFunction(const char* name, const argdef* args);
    static void record(const char*, ...);
};
#endif

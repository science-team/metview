/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

class MvSci
{
public:
    static const double cEarthRadiusInKm;
    static const double cEarthRadiusInM;
    static const double cEarthG;
    static const double cRadian;
    static const double cDegree;
    static const double cMetreToRadian;
    static const double cRadianToMetre;

    static double degToRad(double d) { return d * cDegree; }
    static double radToDeg(double r) { return r * cRadian; }
    static double metresToRadians(double m) { return m * cMetreToRadian; }
    static double radiansToMetres(double r) { return r * cRadianToMetre; }
    static double geoDistanceInKm(double fi1, double la1, double fi2, double la2);
    // static void   latLonToXyz(double lat, double lon, double &x, double &y, double &z);
    static double saturationMixingRatio(double t, double p);
    static double relativeHumidity(double t, double p, double q);
    static double relativeHumidityFromTd(double t, double td);
    static double specificHumidity(double t, double p, double r);
    static double specificHumidityFromTd(double t, double p, double td);
    static double saturationVapourPressure(double t);
    static double vapourPressure(double p, double q);
    static double vapourPPMV(double p, double q);
    static double ozonePPMV(double r);
    static double dewPointFromQ(double p, double t, double q, const std::string& formula);
    static double speed(double u, double v);
    static double direction(double u, double v);
    static double saturationSpecHumidity(double t, double p, bool flag);
    static bool saturationLevel(double t, double p, double q, double& tSat, double& pSat, bool flag);
    static double potentialTemperature(double t, double p);
    static double potentialTemperaturePressureTerm(double p);
    static double equivalentPotentialTemperature(double t, double p, double q, bool flag);
    static double saturatedEquivalentPotentialTemperature(double t, double p, bool flag);
    static double saturatedEquivalentPotentialTemperatureWithPTerm(double t, double p, double pTerm, bool flag);
    static double height_from_geopotential(double z);
    static double geopotential_from_height(double h);
    static double geopotential_height_from_geopotential(double z);
    static double geopotential_from_geopotential_height(double h);
#ifdef METVIEW
    static double solarDeclinationInRad(double dateTime);
    static double cosineSolarZenithAngle(double lat, double lon, double declInRad, double hourAngleGM);
#endif
};

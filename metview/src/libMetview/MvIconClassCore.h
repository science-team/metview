/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include "Metview.h"
#include "Path.h"

class MvIconLanguage;

class MvIconClassCore
{
public:
    MvIconClassCore(const std::string&, request*);
    virtual ~MvIconClassCore();

    virtual const std::string& name() const;
    virtual std::string editor() const;
    virtual std::string type() const;
    virtual std::string defaultName() const;
    virtual std::string iconBox() const;
    virtual std::string helpPage() const;
    virtual std::string doubleClickMethod() const;
    virtual std::string defaultMethod() const;
    virtual std::string macroFunction() const;
    virtual bool skipDepandancies(const std::string& action) const;
    virtual std::string pixmap() const;
    virtual bool canBeCreated() const;
    virtual bool canHaveLog() const;
    virtual Path definitionFile() const;
    virtual Path rulesFile() const;
    virtual long expandFlags() const;
    virtual bool isObsolete() const;
    virtual bool isFamilyMember() const;

    // virtual bool isSubClassOf(const MvIconClass&) const;
    // virtual bool canBecome(const MvIconClass&) const;

    virtual MvIconLanguage& language() const;

    // static bool isValid(const std::string& name);
    // static const MvIconClass& find(const std::string&);
    // static const MvIconClass& find(const std::string& name, const std::string& defaultType, bool);
    // static void find(const std::string&, std::vector<const MvIconClass*>&, bool onlyCreatable = true);
    // static void load(request*);

    // static void scan(ClassScanner&);
protected:
    std::string name_;
    request* request_;

private:
    // No copy allowed
    MvIconClassCore(const MvIconClassCore&);
    MvIconClassCore& operator=(const MvIconClassCore&);
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <stdio.h>

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/* UtilitiesC.c */
char* UtRandomName(const char* path, const char* name);
char* UtProgressiveName(const char* path, const char* name);
char* UtProgressiveName2(const char* path, const char* name);
char* UtPath2Name(const char* path);
int UtTouch(const char* dir, const char* fname);
int UtIsValidVisDef(const char* pathname);
void UtWriteCmd(FILE* fp, const char* cmd);
void UtWritePar(FILE* fp, const char* par, const char* value);
void UtWriteLiteralPar(FILE* fp, const char* par, const char* value);
void UtWriteIntPar(FILE* fp, const char* par, int value);
void UtWriteFloatPar(FILE* fp, const char* par, float value);
void UtWriteIconDescriptionFile(const char* fileName, const char* iconClass);
void UtWritePrinterCmd(char* cmd, const char* user_cmd, const char* printer,
                       const char* ncopies, const char* fname);
void UtWritePreviewCmd(char* previewCmd, const char* previewerName,
                       const char* outFileName);

double t2d(double value);
int localWmoSiteNumber(void);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

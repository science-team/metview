/***************************** LICENSE START ***********************************

 Copyright 2017 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvVariant.h"


MvVariant::MvVariant() :
    type_(NoType)
{
    init();
}

MvVariant::MvVariant(int v) :
    type_(IntType),
    longVal_(v)
{
    init();
}

MvVariant::MvVariant(long v) :
    type_(LongType),
    longVal_(v)
{
    init();
}

MvVariant::MvVariant(float v) :
    type_(FloatType),
    doubleVal_(v)
{
    init();
}

MvVariant::MvVariant(double v) :
    type_(DoubleType),
    doubleVal_(v)
{
    init();
}

MvVariant::MvVariant(const std::string& v) :
    type_(StringType),
    strVal_(v)
{
    init();
}

MvVariant::MvVariant(Type type) :
    type_(type)
{
    init();
}

void MvVariant::init()
{
    if (type_ != IntType && type_ != LongType)
        longVal_ = 0;
    if (type_ != DoubleType && type_ != FloatType)
        doubleVal_ = 0.;
}

const std::string& MvVariant::toString() const
{
    if (type_ != StringType && strVal_.empty()) {
        try {
            if (type_ == IntType || type_ == LongType) {
                strVal_ = std::to_string(longVal_);
            }
            else if (type_ == DoubleType || type_ == FloatType) {
                strVal_ = std::to_string(doubleVal_);
            }
        }
        catch (...) {
        }
    }
    return strVal_;
}

bool MvVariant::operator==(const MvVariant& o) const
{
    if (type_ != o.type_)
        return false;

    switch (type_) {
        case LongType:
        case IntType:
            return longVal_ == o.longVal_;

        case DoubleType:
        case FloatType: {
            return doubleVal_ == o.doubleVal_;
        }
        case StringType:
            return strVal_ == o.strVal_;
        default:
            break;
    }
    return false;
}

bool MvVariant::operator!=(const MvVariant& o) const
{
    if (type_ != o.type_)
        return false;

    return !(*this == o);
}

bool MvVariant::operator<(const MvVariant& o) const
{
    if (type_ != o.type_)
        return false;

    switch (type_) {
        case LongType:
        case IntType:
            return longVal_ < o.longVal_;
        case DoubleType:
        case FloatType:
            return doubleVal_ < o.doubleVal_;
        case StringType:
            return strVal_ < o.strVal_;
        default:
            break;
    }
    return false;
}

bool MvVariant::operator>(const MvVariant& o) const
{
    if (type_ != o.type_)
        return false;
    switch (type_) {
        case LongType:
        case IntType:
            return longVal_ > o.longVal_;
        case FloatType:
        case DoubleType:
            return doubleVal_ > o.doubleVal_;
        case StringType:
            return strVal_ > o.strVal_;
        default:
            break;
    }
    return false;
}

bool MvVariant::operator<=(const MvVariant& o) const
{
    if (type_ != o.type_)
        return false;

    return !(*this > o);
}

bool MvVariant::operator>=(const MvVariant& o) const
{
    if (type_ != o.type_)
        return false;

    return !(*this < o);
}

std::vector<MvVariant> MvVariant::makeVector(const std::vector<int>& v)
{
    std::vector<MvVariant> vv;
    for (int i : v)
        vv.emplace_back(i);
    return vv;
}

std::vector<MvVariant> MvVariant::makeVector(const std::vector<long>& v)
{
    std::vector<MvVariant> vv;
    for (long i : v)
        vv.emplace_back(i);
    return vv;
}

std::vector<MvVariant> MvVariant::makeVector(const std::vector<float>& v)
{
    std::vector<MvVariant> vv;
    for (float i : v)
        vv.emplace_back(i);
    return vv;
}


std::vector<MvVariant> MvVariant::makeVector(const std::vector<double>& v)
{
    std::vector<MvVariant> vv;
    for (double i : v)
        vv.emplace_back(i);
    return vv;
}

std::vector<MvVariant> MvVariant::makeVector(const std::vector<std::string>& v)
{
    std::vector<MvVariant> vv;
    for (const auto& i : v)
        vv.emplace_back(i);
    return vv;
}

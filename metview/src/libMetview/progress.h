/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/* progress.c */

void progress_init(Widget parent);
void progress_pixmap(Pixmap pixmap);
void progress_start(const char* newtitle, int max);
void progress_value(int value);
void progress_stop();
int progress_ok();

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

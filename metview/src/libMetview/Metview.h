/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <mars.h>
#include <Cached.h>
#include <MvRequest.h>
#include <MvLanguage.h>
#include <MvFilter.h>
#include <MvApplication.h>
#include <MvProtocol.h>
#include <MvFunction.h>
#include <MvFieldSet.h>
#include <MvService.h>
#include <MvSerie.h>
#include "MvDebugPrintControl.h"
// for observations:
//#include <MvObs.h>
//#include <MvBufr.h>
//#include <MvLocation.h>

/*! \mainpage Documentation of libMetview library
 *
 * \section intro_sec Introduction
 *
 * This is libMetview documentation, part of Metview Techical
 * Documentation, documenting the classes whose source code is
 * stored in directory 'src/libMetview'. Note that this document
 * does not include classes whose source code is outside
 * 'src/libMetview', such as 'src/libUtil', 'src/libMars', etc.
 *
 * This documentation is created by 'Doxygen'.
 *
 * \section build_sec How to update this documentation
 *
 * Whenever you edit such files in libMetview that are still
 * missing the documentation (or having misleading documentation),
 * you are invited to add and/or correct also the documentation
 * comments.
 *
 * To (re)create this documentation simply run command 'doxygen'
 * in directory 'src/libMetview'. The documentation is created
 * according to 'doxygen' configuration file
 * 'src/libMetview/Doxyfile'.
 *
 * The HTML index file (this page) will be written to
 * 'src/libMetview/doxy/html/index.html'.
 *
 * \section pdf_sec PDF documentation
 *
 * Doxygen can be configured to create also PDF documentation by
 * setting USE_PDFLATEX to YES. Currently (Dec 2008) some packages
 * are missing and the PDF version cannot be created...
 * (needs: pdflatex,... )
 *
 * \section doxy_sec About Doxygen
 *
 * To learn about Doxygen have a look at:
 * http://www.stack.nl/~dimitri/doxygen/ \n \n
 *
 * Good Luck! \n \n
 * Graphics Section \n
 * ECMWF \n \n \n
 *
 * and continuous Happy Metviewing to everybody! \n
 * Vesa Karhila, 2008-12-23, retiring from ECMWF
 *
 */

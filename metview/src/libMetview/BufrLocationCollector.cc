/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "BufrLocationCollector.h"

#include "MvKeyProfile.h"
#include <cassert>

BufrLocationCollector::BufrLocationCollector(MvKeyProfile* prof, int maxNum) :
    prof_(prof),
    maxNum_(maxNum),
    totalNum_(0)
{
    assert(prof_);

    msgKey_ = prof->key(MvKey::MessageIndexRole);
    subsetKey_ = prof->key(MvKey::SubsetIndexRole);
    rankKey_ = prof->key(MvKey::RankRole);
    latKey_ = prof->key(MvKey::LatitudeRole);
    lonKey_ = prof->key(MvKey::LongitudeRole);

    assert(msgKey_);
    assert(subsetKey_);
    assert(rankKey_);
    assert(latKey_);
    assert(lonKey_);
}

void BufrLocationCollector::add(int msg, int subset, int rank, double lat, double lon)
{
    if (totalNum_ < maxNum_) {
        msgKey_->addIntValue(msg);
        subsetKey_->addIntValue(subset);
        rankKey_->addIntValue(rank);
        latKey_->addDoubleValue(lat);
        lonKey_->addDoubleValue(lon);
    }
    totalNum_++;
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

//! (obsolete class)
/*! This class dates back to 1994 when it was used in modules
 *  \c src/Average/Average.cc and \c src/Profile/Profile.cc.
 *  Both these modules are now obsolete and currently
 *  MvSerie is not used anywhere in Metview.
 */
class MvSerie
{
protected:
    MvRequest Header;
    MvRequest Data;
    MvRequest HAxis;
    MvRequest VAxis;
    MvRequest Magics;

    double Xmin;
    double Xmax;
    double Ymin;
    double Ymax;

    int NbPoints;

    virtual void init();
    virtual void addX(double v) { Data("X") += v; }
    virtual void addY(double v) { Data("Y") += v; }

public:
    virtual void setXMinMax(double);
    virtual void setYMinMax(double);

    virtual void setYAutoScale(int nticks = 10);
    virtual void setXAutoScale(int nticks = 10);
    virtual void setYPressure();
    virtual void setYModel();
    MvSerie(char*);

    MvSerie();
    virtual ~MvSerie();
    void addPoint(double x, double y);
    void setTitle(char*, ...);
    void modifyMagics(MvRequest m) { Magics = m; }
    virtual MvRequest getRequest();
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <vector>

class MvKeyProfile;

class MvKeyManager : public std::vector<MvKeyProfile*>
{
public:
    MvKeyManager(std::string fprof) :
        fprof_(fprof) {}
    ~MvKeyManager();
    MvKeyProfile* addProfile(std::string);
    void deleteProfile(int);
    void loadProfiles();
    void saveProfiles();
    void clear();

private:
    std::string fprof_;
};


#pragma once

typedef struct
{
    char id[13];  // grid identifier

    short year,   // grid year >1900 xxxx
        month,    // grid mouth 1-12 xxxx
        day,      // grid day 1-31 xxxx
        hour,     // grid hour 0-23 xxxx
        minutes,  // grid minutes 0-59 xxxx
        seconds,  // grid seconds 0-59 xxxx
        nx,       // number of columns
        ny,       // number of lines
        row,      // grid number xxxx
        plevel,   // processing level
        vc,       // current volume
        ac;       // currente file

    float rx,  // horizontal resolution in meters
        ry,    // vertical resolution in meters
        xmin,  // bounding box minimum x coordinate
        xmax,  // bounding box maximum x coordinate
        ymin,  // bounding box minimum y coordinate
        ymax,  // bounding box maximum y coordinate
        zmin,  // minimum z coordinate
        zmax;  // maximum z coordinate
} GridDoc;


void fill_griddoc(GridDoc& grd, short wid, short hg, float xmin, float xmax, float ymin, float ymax, float zmin, float zmax);

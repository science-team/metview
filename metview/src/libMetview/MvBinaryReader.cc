/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvBinaryReader.h"

#include <fstream>
#include <iostream>

#include "fstream_mars_fix.h"

#include <cassert>

MvBinaryReader::MvBinaryReader(const std::string& fname)
{
    std::ifstream file(fname.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
    if (file.is_open()) {
        size_ = file.tellg();
        data_ = new char[size_];
        dataStart_ = data_;
        std::cout << "size"
                  << " " << size_ << std::endl;
        file.seekg(0, std::ios::beg);
        file.read(data_, size_);
        file.close();
    }
}

MvBinaryReader::~MvBinaryReader()
{
    if (dataStart_)
        delete[] dataStart_;
}

bool MvBinaryReader::isValid() const
{
    return (data_ != nullptr);
}

void MvBinaryReader::skipRecMarker()
{
    if (data_)
        data_ += recordMarker_;
}

bool MvBinaryReader::hasData() const
{
    return data_ - dataStart_ < size_;
}

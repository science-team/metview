/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

struct parameter;
struct value;

#include "Request.h"

class MvIconParameter;

class ParameterScanner
{
public:
    virtual ~ParameterScanner() {}
    virtual void next(const MvIconParameter&, const char*, const char*) = 0;
};

class MvIconLanguage;

class MvIconParameter
{
public:
    MvIconParameter(MvIconLanguage&, parameter*);
    ~MvIconParameter();

    const char* name() const;
    const char* help() const;
    const char* help_text() const;
    const char* help_icon() const;
    const char* interface() const;
    const char* warning_text() const;
    const char* ribbon_interface() const;
    const char* ribbon_label() const;
    const char* ribbon_control() const;
    const char* ribbon_separator() const;
    const char* ribbon_size_range() const;
    bool hiddenInTemporary() const;

    Request interfaceRequest() const;

    const std::string& beautifiedName(const std::string&) const;
    const std::string& beautifiedName() const;

    void scan(ParameterScanner&) const;

    bool multiple() const;
    bool hasDefaults() const;

    const std::vector<std::string>& defaults() const;

private:
    // No copy allowed

    MvIconParameter(const MvIconParameter&);
    MvIconParameter& operator=(const MvIconParameter&);

    MvIconLanguage& lang_;
    parameter* param_;
    bool multiple_;
    bool beautify_;
    std::map<std::string, std::string> beau_;
    std::string name_;
    std::vector<std::string> defaults_;

    const std::string& beautify(const std::string&) const;
    void scan(ParameterScanner&, const value*) const;
};

inline void destroy(MvIconParameter**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(Parameter);
//#endif

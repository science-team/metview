/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvKeyManager.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "fstream_mars_fix.h"

#include "MvKeyProfile.h"

/*key_.push_back(MvKey("count","Count",true));
    key_.push_back(MvKey("mars.date","Date",true));
    key_.push_back(MvKey("mars.time","Time",true));
    key_.push_back(MvKey("mars.step","Step",true));
    key_.push_back(MvKey("mars.param","Param",true));
    key_.push_back(MvKey("dataRepresentationType","Rep",true));
    key_.push_back(MvKey("mars.levelist","Level",true));
    key_.push_back(MvKey("mars.levtype","Levtype",true));
    key_.push_back(MvKey("mars.class","Class",false));
    key_.push_back(MvKey("mars.type","Type",false));
    key_.push_back(MvKey("mars.stream","Stream",false));
    key_.push_back(MvKey("editionNumber","Edition",false));*/


MvKeyManager::~MvKeyManager()
{
    clear();
}

void MvKeyManager::clear()
{
    for (auto& it : *this) {
        delete it;
    }

    std::vector<MvKeyProfile*>::clear();
}

MvKeyProfile* MvKeyManager::addProfile(std::string name)
{
    auto* profile = new MvKeyProfile(name);
    push_back(profile);
    return profile;
}

void MvKeyManager::deleteProfile(int index)
{
    auto it = begin() + index;
    delete (*it);
    erase(it);
}

void MvKeyManager::loadProfiles()
{
    std::ifstream in(fprof_.c_str(), std::ifstream::in);

    if (!in) {
        auto* prof = new MvKeyProfile("Metview default");
        push_back(prof);

        prof->addKey(new MvKey("count", "Index"));
        prof->addKey(new MvKey("mars.date", "Date"));
        prof->addKey(new MvKey("mars.time", "Time"));
        prof->addKey(new MvKey("mars.step", "Step"));
        prof->addKey(new MvKey("mars.param", "Param"));
        prof->addKey(new MvKey("dataRepresentationType", "Rep"));
        prof->addKey(new MvKey("mars.levelist", "Level"));
        prof->addKey(new MvKey("mars.levtype", "Levtype"));

        saveProfiles();
    }
    else {
        // Create a deafult profile
        char c[256];
        std::string name, sname;

        MvKeyProfile* prof = nullptr;
        while (in.getline(c, 256)) {
            // cout << c << std::endl;

            std::string s = c;

            if (s.find("#PROFILE") != std::string::npos) {
                in.getline(c, 256);

                if (!prof) {
                    prof = new MvKeyProfile(c);
                    push_back(prof);
                }
                else
                    break;
            }
            else {
                name = c;
                in.getline(c, 256);
                sname = c;

                if (prof)
                    prof->addKey(new MvKey(name, sname));
                else
                    break;
            }
        }
    }
}

void MvKeyManager::saveProfiles()
{
    std::ofstream out(fprof_.c_str(), std::ofstream::out);

    for (auto prof : *this) {
        out << "#PROFILE" << std::endl;
        out << prof->name() << std::endl;
        // out  << prof->size() << std::endl;
        for (auto key : *prof) {
            out << key->name() << std::endl;
            out << key->shortName() << std::endl;
        }
    }

    out.close();
}

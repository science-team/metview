/***************************** LICENSE START ***********************************

 Copyright 2023 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

class MvExitStatus
{
public:
    MvExitStatus(int ret);
    bool isWarning() const;
    bool isError() const;
    bool isSuccess() const;
    int exitCode() const {return wRet_;}
private:
    int ret_{0};
    int wRet_{0};
};

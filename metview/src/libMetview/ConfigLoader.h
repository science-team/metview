/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

#include <string>

#include "Metview.h"

class ConfigLoader
{
public:
    ConfigLoader(const std::string& name, int order);
    virtual ~ConfigLoader() = default;
    ConfigLoader(const ConfigLoader&) = delete;
    ConfigLoader& operator=(const ConfigLoader&) = delete;

    virtual void load(request*) = 0;
    static void init();

private:
    static bool process(request*, int);
};

template <class T>
class SimpleLoader : public ConfigLoader
{
    void load(request* r) { T::load(r); }

public:
    SimpleLoader(const std::string& name, int order) :
        ConfigLoader(name, order) {}
};

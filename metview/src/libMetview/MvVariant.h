/***************************** LICENSE START ***********************************

 Copyright 2017 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <vector>

class MvVariant
{
public:
    enum Type
    {
        IntType,
        LongType,
        FloatType,
        DoubleType,
        StringType,
        NoType
    };

    MvVariant();
    MvVariant(int v);
    MvVariant(long v);
    MvVariant(float v);
    MvVariant(double v);
    MvVariant(const std::string& v);
    MvVariant(Type);

    // MvVariant(const MvVariant&);

    // void operator =(int);
    // void operator =(double);
    // void operator =(const std::string&);

    bool operator==(const MvVariant&) const;
    bool operator!=(const MvVariant&) const;
    bool operator<=(const MvVariant&) const;
    bool operator<(const MvVariant&) const;
    bool operator>=(const MvVariant&) const;
    bool operator>(const MvVariant&) const;

    Type type() const { return type_; }
    int toInt() const { return static_cast<int>(longVal_); }
    long toLong() const { return longVal_; }
    float toFloat() const { return static_cast<float>(doubleVal_); }
    double toDouble() const { return doubleVal_; }
    const std::string& toString() const;
    bool isNull() const { return type_ == NoType; }

    static std::vector<MvVariant> makeVector(const std::vector<int>&);
    static std::vector<MvVariant> makeVector(const std::vector<long>&);
    static std::vector<MvVariant> makeVector(const std::vector<float>&);
    static std::vector<MvVariant> makeVector(const std::vector<double>&);
    static std::vector<MvVariant> makeVector(const std::vector<std::string>&);

protected:
    void init();

    Type type_;
    long longVal_;
    double doubleVal_;
    mutable std::string strVal_;
};

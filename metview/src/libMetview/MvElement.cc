/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <MvElement.hpp>
#include <mars.h>

MvElement ::MvElement(MvElement& dl)
{
    setElemName(dl._elem_name);
    _elem_code = dl._elem_code;
    _count = 0;
}

// Operator =

MvElement MvElement ::operator=(MvElement& dl)
{
    setElemName(dl._elem_name);
    _elem_code = dl._elem_code;
    return *this;
}

// Set link name

void MvElement ::setElemName(const char* name)
{
    _elem_name = name;
}

// Set both link name and code

void MvElement ::setElemBoth(const char* name, int code)
{
    setElemName(name);
    _elem_code = code;
}

MvElement::~MvElement() = default;

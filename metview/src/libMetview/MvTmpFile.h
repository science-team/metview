/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

class MvTmpFile
{
public:
    MvTmpFile(bool autoRemove = true);
    MvTmpFile(const std::string& name, bool autoRemove = true);
    ~MvTmpFile();

    const std::string& path() const { return path_; }

protected:
    static const std::string& tmpDir();
    bool exists() const;

    std::string path_;
    bool autoRemove_;
};

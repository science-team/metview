#define Types_H_

// Definitions for Macro Device

enum MacroType
{
    PRINT_JOB,      // Print he output of the screen
    PRINT_MACRO,    //
    PRINT_PREVIEW,  //  Preview the output
    SCREEN_MACRO,   //  Reproduce the output of the screen
    ZOOM_AREA
};  //  Zoom into a single page


enum MacroConversion
{
    NO_LAST_COMMA,   // omits the comma on the last line
    PUT_LAST_COMMA,  // puts a comma on all lines
    PUT_END
};  // generate closing paranthesis.

// Definitions for MenuTypes

enum MenuType
{
    BUTTON,              // supported
    LABEL,               //    "
    SEPARATOR,           //    "
    PULLDOWN,            //    "
    PIXMAPPULLDOWN,      //    "
    END,                 //    "
    TOGGLE,              //    "
    RADIOPULLDOWN,       //    "
    CONFIRMFIRSTACTION,  // the following are
    BAR,                 //  not supported yet
    OPTION,
    POPUP,
    CASCADE,
    OBJECT
};

// Definitions for CursorTypes
enum CursorType
{
    POINTERCURSOR,
    ZOOMCURSOR,
    MAGNIFYCURSOR,
    SCROLLCURSOR,
    POINTCURSOR,
    TOOLPLUSPOINTCURSOR,
    TOOLPLUSAREACURSOR
};

// Definitions for InputTypes
enum InputType
{
    NOINPUT,
    POINTINPUT,
    LINEINPUT,
    AREAINPUT,
    MAPINPUT
};

// Definitions for FORTRAN Interface

#ifdef __alpha
typedef int Int; /* fortran integer */
#else
typedef long Int; /* fortran integer */
#pragma once
#ifdef R64
typedef double Real;
#else
typedef float Real; /* fortran single precision float */
#endif


#endif

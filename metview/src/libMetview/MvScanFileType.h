/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//--  MvScanFileType.h  --  Jun05
//
// Code moved from MetviewUI in order to reuse it in Macro!
//

#pragma once

#include <string>

#include "MvRequest.h"

//! \file MvScanFileType.h
//! \brief Utilities to scan for the type of a file

//! \fn ScanFileType
//! \brief A function to examine file type
//!
//! This function is used to find out what is the type
//! of the file given as a parameter.
std::string ScanFileType(const char* fileName);

//-- check if file is missing a binary file --
bool IsBinaryOrMissingFile(const char* fileName);

int MvGribVersion(const char* fileName);
MvRequest MvConvertToGrib1(const MvRequest& gribRequest);
std::string MvOdbType(const char* fileName, bool scanBeforeCheck = false);

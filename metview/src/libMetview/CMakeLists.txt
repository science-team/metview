set(libMetview_srcs
      ConfigLoader.cc
      GribVertical.cc
      MacroToPythonConvert.cc
      MvAbstractApplication.cc
      MvService.cc
      MvProtocol.cc
      MvFunction.cc
      MvApplication.cc
      MvRequest.cc
      MvLanguage.cc
      MvIconLanguage.cc
      MvIconParameter.cc
      MvGeoPoints.cc
      MvGeoPointSet.cc
      MvGrid.cc
      MvFieldSet.cc
      MvFilter.cc
      MvFlextra.cc
      MvFlexpart.cc
      MvIconClassCore.cc
      MvSerie.cc
      MvTable.cc
      MvFortran.cc
      MvDate.cc
      MvTimeSerie.cc
      MvLocation.cc
      MvLog.cc
      MvObsSet.cc
      MvObs.cc
      MvPrepBufrPrep.cc
      MvSci.cc
      MvTask.cc
      MvServiceTask.cc
      MvShellTask.cc
      MvVisTool.cc
      LLMatrixToGRIB.cc
      MvMatrix.cc
      MvScanFileType.cc
      MvDebugPrintControl.cc
      Path.cc
      BufrFilterDef.cc
      BufrFilterEngine.cc
      BufrLocationCollector.cc
      BufrMetaData.cc
      Cached.cc
      GribMetaData.cc
      LogHandler.cc
      MtInputEvent.cc
      MvAlmostObsoleteRequest.cc
      MvBinaryReader.cc
      MvBufrEdition.cc
      MvBufrElementTable.cc
      MvEccBufr.cc
      MvElement.cc
      MvException.cc
      MvExitStatus.cc
      MvKeyCondition.cc
      MvKeyProfile.cc
      MvKeyManager.cc
      MvList.cc
      MvMessageMetaData.cc
      MvMiscellaneous.cc
      MvPath.cc
      MvProfileData.cc
      MvRequestUtil.cc
      MvStopWatch.cc
      MvTmpFile.cc
      MvVariant.cc
      MvVersionInfo.cc
      Point.cc
      proj_braz.cc
      StatsCompute.cc
      Request.cc
      TableReader.cc
      Tokenizer.cc
      UtilitiesC.c
      VectorUtils.c
      MvNetCDF.cc)

if (ENABLE_URL_DOWNLOAD)
    list (APPEND libMetview_srcs MvNetwork.cc)
endif()

list (APPEND libMetview_srcs MvRttov.cc MvScm.cc)


if (odc_FOUND)
    list(APPEND libMetview_srcs MvOdb.cc)
endif()


ecbuild_add_library( TARGET           Metview
                     TYPE             SHARED
                     SOURCES          ${libMetview_srcs}
                     TEMPLATES        ${common_templates}
                     PRIVATE_INCLUDES ${METVIEW_STANDARD_INCLUDES}
                     DEFINITIONS      ${METVIEW_EXTRA_DEFINITIONS}
                     PUBLIC_LIBS      ${MARSCLIENT_LIBS} MvFTimeUtil ${METVIEW_EXTRA_LIBRARIES} ${METVIEW_ODB_API_LIBRARIES}
                    )

#target_link_libraries(Metview MvMars MvFTimeUtil)  # 'a' depends on 'b' and 'c'


ecbuild_dont_pack(FILES "Demo.cc;DemoFort.cc;Doxyfile;SampleModule.cc;SampleObs.cc;demofort.f")

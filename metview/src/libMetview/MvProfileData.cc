/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvProfileData.h"

void MvProfileData::valueRange(int startIndex, int endIndex, float& minv, float& maxv)
{
    minv = 9999999999;
    maxv = -9999999999;

    if (startIndex < 0 || startIndex > endIndex || endIndex >= count())
        return;

    for (int i = startIndex; i <= endIndex; i++) {
        float val = value(i);
        if (val >= maxv)
            maxv = val;
        if (val < minv)
            minv = val;
    }
}

void MvProfileData::valueRange(float& minv, float& maxv)
{
    valueRange(0, count() - 1, minv, maxv);
}


bool MvProfileData::acceptChange(const MvProfileChange& ch) const
{
    return ch.checkData(*this);
}

bool MvProfileData::fitToRange(float& val)
{
    if (rangeSet_) {
        if (val < rangeMin_) {
            val = rangeMin_;
            return false;
        }
        else if (val > rangeMax_) {
            val = rangeMax_;
            return false;
        }
    }

    return true;
}

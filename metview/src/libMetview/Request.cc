/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#ifndef Request_H
#include "Request.h"
#endif

#include "Path.h"

Request::Request() :
    MvRequest((request*)nullptr)
{
}

Request::Request(const char* verb) :
    MvRequest(verb)
{
}
Request::Request(const std::string& verb) :
    MvRequest(verb.c_str())
{
}

Request::Request(const MvRequest& other) :
    MvRequest(other)
{
}

Request::Request(const Path& path) :
    MvRequest(path.loadRequest())
{
}

Request::~Request() = default;

void Request::save(const Path& path) const
{
    path.saveRequest(*this);
}

std::vector<std::string> Request::get(const char* name) const
{
    int i = 0;
    const char* p;

    std::vector<std::string> result;
    result.reserve(count_values(CurrentRequest, name));

    while ((p = get_value(CurrentRequest, name, i++)))
        result.push_back(p);

    return result;
}


void Request::set(const char* name, const std::vector<std::string>& v)
{
    unset_value(CurrentRequest, name);
    for (const auto& j : v)
        add_value(CurrentRequest, name, "%s", j.c_str());
}

std::vector<std::string> Request::get(const std::string& name) const
{
    return get(name.c_str());
}

void Request::set(const std::string& name, const std::vector<std::string>& v)
{
    set(name.c_str(), v);
}

Request Request::fromText(const std::string& txt)
{
    // request* r=string2request(txt.c_str());
    MvRequest r(string2request(txt.c_str()));
    return Request(r);
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <cstdio>
#include <cstdlib>

#include "MvMiscellaneous.h"
#include "MvVersionInfo.h"

#include "eccodes.h"

#ifdef METVIEW
#include "netcdf.h"
#endif

MvVersionInfo::MvVersionInfo()
{
    FILE* fp = nullptr;
    const char* user = getenv("METVIEW_DIR_SHARE");
    const char* dir = getenv("METVIEW_DIR");
    char version_path[256];
    char mv_name[64];
    char mv_label[64];
    char mv_period[64];
    char mv_release_date[64];

    install_dir_ = dir;

    sprintf(version_path, "%s/app-defaults/MvVersionDetails", user);

    fp = fopen(version_path, "rt");

    if (fp) {
        /* e.g.
            1 Metview 4 0 0 -alpha-7 2010 1990-2010 01-February-2010

        Note that the file version is just there in case we want to change the fomat
        of the file in the future. */
        fscanf(fp, "%d %63s %d %d %d %63s %d %63s %63s", &file_version_, mv_name, &major_,
               &minor_, &revision_,
               mv_label, &year_, mv_period,
               mv_release_date);

        version_ = (major_ * 10000) + (minor_ * 100) + revision_;

        name_ = mv_name;
        label_ = mv_label;
        copyright_period_ = mv_period;
        release_date_ = mv_release_date;

        char charNameAndVersion[64];
        sprintf(charNameAndVersion, "%s %d.%d.%d", name_.c_str(), major_, minor_, revision_);
        nameAndVersion_ = charNameAndVersion;

        fclose(fp);
        info_found_ = true;
    }

    else {
        error_message_ = "Cannot open Metview version file ";
        error_message_ += version_path;
        info_found_ = false;
    }
}


MvGribApiVersionInfo::MvGribApiVersionInfo()
{
    version_ = codes_get_api_version();
    major_ = version_ / 10000;
    minor_ = (version_ - (major_ * 10000)) / 100;
    revision_ = version_ % 100;

    info_found_ = true;
}

#ifdef METVIEW
MvNetcdfVersionInfo::MvNetcdfVersionInfo()
{
    // get version info
    std::string str = nc_inq_libvers();

    // get substring "x.x.x"
    size_t pos1 = str.find('"');
    size_t pos2 = str.find('"', ++pos1);
    std::string sver = str.substr(pos1, pos2 - pos1);

    // decode the version number
    pos1 = sver.find('.');
    std::string ss = sver.substr(0, pos1);
    major_ = atoi(ss.c_str());

    pos2 = sver.find('.', ++pos1);
    ss = sver.substr(pos1, pos2 - pos1);
    minor_ = atoi(ss.c_str());

    ss = sver.substr(++pos2);
    revision_ = atoi(ss.c_str());

    version_ = major_ * 10000 + minor_ * 100 + revision_;

    info_found_ = true;
}
#endif


#ifdef ECCODES_UI
CodesUiVersionInfo::CodesUiVersionInfo()
{
    FILE* fp;
    std::string version_path = metview::appDefDirFile("codes_ui_version_details");

    // const char *dir  = getenv("METVIEW_DIR");
    char mv_name[64];
    char mv_label[64];
    char mv_period[64];
    char mv_release_date[64];

    // install_dir_ = dir;

    // sprintf (version_path, "%s/app-defaults/MvVersionDetails", user);

    fp = fopen(version_path.c_str(), "rt");

    if (fp) {
        /* e.g.
            1 Metview 4 0 0 -alpha-7 2010 1990-2010 01-February-2010

        Note that the file version is just there in case we want to change the format
        of the file in the future. */
        fscanf(fp, "%d %63s %d %d %d %d %63s %63s", &file_version_, mv_name, &major_,
               &minor_, &revision_,
               &year_, mv_period,
               mv_release_date);

        version_ = (major_ * 10000) + (minor_ * 100) + revision_;

        name_ = mv_name;
        label_ = mv_label;
        copyright_period_ = mv_period;
        release_date_ = mv_release_date;

        char charNameAndVersion[64];
        sprintf(charNameAndVersion, "%s %d.%d.%d", name_.c_str(), major_, minor_, revision_);
        nameAndVersion_ = charNameAndVersion;

        fclose(fp);
        info_found_ = true;
    }

    else {
        error_message_ = "Cannot open CodesUI version file ";
        error_message_ += version_path;
        info_found_ = false;
    }
}
#endif

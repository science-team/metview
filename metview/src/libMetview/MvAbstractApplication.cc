/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvAbstractApplication.h"

#include "MvFwd.h"
#include "MvLog.h"

MvAbstractApplication* MvAbstractApplication::abcApp_ = nullptr;
std::string MvAbstractApplication::name_;

MvAbstractApplication::MvAbstractApplication(const char* name)
{
    abcApp_ = this;
    if (name) {
        name_ = std::string(name);
    }
    // init application logger
    MvLog::registerApp(this);
}

void MvAbstractApplication::toLog(const std::string& msg, const std::string& details, MvLogLevel level, bool popup)
{
    abcApp_->writeToLog(msg + details, level);
    abcApp_->writeToUiLog(msg, details, level, popup);
}

void MvAbstractApplication::abortWithPopup(const std::string& text)
{
    MvLog().popup().err() << text;
}

// ==================================================
//
// MvRequestUtil - Utilities for Dealing with Requests
//
//
//
// ===================================================
#pragma once

#include <MvRequest.h>

namespace metview
{

void copyParam(const MvRequest& inRequest, MvRequest& outRequest,
               const std::string& oldName, const std::string& newName);

// Copy a request, removing parameters with acertain prefix
void CopyAndRemoveParameters(const MvRequest& inRequest,
                             MvRequest& outRequest,
                             const char* prefix);

// Copy a request the parameters with a certain prefix from one request to another
void CopySomeParameters(const MvRequest& inRequest,
                        MvRequest& outRequest,
                        const char* prefix,
                        bool overwrite = true);

// Copy the Parameters which start with a certain prefix
// from one request to another changing prefix by a new prefix
void CopySomeParameters(const MvRequest& inRequest,
                        MvRequest& outRequest,
                        const char* prefix,
                        const char* newPrefix);


// Copy the Parameters which finish with a certain sufix
// from one request to another
void CopySomeParameters(const MvRequest& inRequest,
                        MvRequest& outRequest,
                        const std::string& sufix);

// Merge two TEXT parameters
void MergeTextParameters(MvRequest& inRequest,
                         MvRequest& outRequest,
                         bool overwrite);

// Compare two requests
bool AreEqualRequests(MvRequest& req1, MvRequest& req2);

// Find if a parameter has been set
bool IsParameterSet(MvRequest&, const char*);

// Copy a value to a parameter
bool SetIfValue(MvRequest& req, const char* param, const char* val);

// Remove parameters with a certain prefix
void RemoveParameters(MvRequest& req, const char* prefix);

// Copy a request taking into consideration the Drawing Order parameter
void CopyParametersAndStackingOrder(const MvRequest& inRequestOld, const MvRequest& inRequestNew, int index, MvRequest& outRequest);

}  // namespace metview

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/
//  MvPath
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Utilities used in PlotMod for
//  accessing the system and user default paths
//
//
#pragma once

#include <stdio.h>

#include <string>

#include <Cached.h>

std::string MakeUserPath(const std::string& name);
std::string MakeUserDefPath(const char* name, bool absolutePath = true);
std::string MakeUserPrefPath(const char* name);
std::string MakeSystemEtcPath(const std::string& name);
std::string MakeTmpPath(const char* name);
std::string CreateTmpPath(const char*);
Cached MakeFilePath(const char* reqName);
const char* MakeIconName(const char* path, const char* iconPrefix);
std::string MakeIconNameFromPath(const std::string&);
Cached MakePrinterFileName(const Cached& path, const Cached& fileName);
Cached MakeTmpName(const Cached& iconPrefix);
Cached MakeIconPath(const char* iconName);
std::string MakeProcessName(const char* name);
bool FileCanBeOpened(const char* filename, const char* mode);
bool FileHasValidSize(const char* filename);

//! Prepends the current path to filename unless it is absolute path
std::string FullPathName(const char* filename);

std::string GetUserDirectory();
std::string UserTempCachePath();

std::string MakeAbsolutePath(const char*, const char* = 0);

bool DeletePath(const char*);

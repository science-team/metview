#pragma once

#include <stdio.h>
#include <string.h>
#include <Cached.h>

class MvList;

class MvElement
{
    Cached _elem_name;
    int _elem_code;
    int _count;

public:
    MvElement() :
        _elem_name()
    {
        _elem_code = -1;
        _count = 0;
    }
    MvElement(MvElement& dl);

    virtual ~MvElement();
    //	Destructor is virtual to enable the real component to
    //	be deleted when MvList or MvLink is deleted

    virtual void callback(const char*, void* = nullptr) {}
    //	Define virtual callback.

    void increment() { _count++; }
    void decrement() { _count--; }

    //	Inquiries

    inline const char* getElemName() { return _elem_name; }
    inline int getElemCode() { return _elem_code; }
    int count() { return _count; }

    //	Set

    MvElement operator=(MvElement& dl);
    void setElemName(const char*);
    inline void setElemCode(int code) { _elem_code = code; }
    void setElemBoth(const char* name, int code);

    //<*teste*>
    void print()
    {
        if (_elem_name)
            printf("%s : ", (const char*)_elem_name);
        printf("%d\n", _elem_code);
    }

    //	Friends

    friend class MvList;
};

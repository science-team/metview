/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

class MacroToPythonConvert
{
public:
    MacroToPythonConvert() =  default;
    static std::string resolveMacroPath(const std::string& inPath);
    int  convert(const std::string& inPath, bool resolveInPath, std::string& outPath);
    void setNeedHeader(bool b) {needHeader_=b;}
    void setNeedWarnings(bool b) {needWarnings_=b;}
    void setNeedLicense(bool b) {needLicense_=b;}
    void setNeedBlackFormatter(bool b) {needBlack_=b;}
    const std::string errText() const {return errText_;}
    const std::string outText() const {return outText_;}

private:
    bool needHeader_{true};
    bool needWarnings_{true};
    bool needLicense_{true};
    bool needBlack_{true};
    std::string errText_;
    std::string outText_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvAlmostObsoleteRequest.hpp"

static MvList Requests;

void DataGenNewStatus(svcid* id, request* r, void* data);
void DataGenServe(svcid* id, request* r, void* data);
void DataGenReply(svcid* id, request* r, void* data);


//======================================================================
//==== (file ./src/Util/MvRequest appended here to prevent messing up
//==== with class MvRequest in ./src/Metview!)
//==== Then later 010508 original part of the file was moved to
//==== src/AppMod (because of IBM compilation problems) and
//==== this remaining was renamed as MvAlmostObsoleteRequest.cc.


MvDataGen ::MvDataGen(svcid* i, request* r, const char* typ)
{
    datatype = strdup(typ);
    req = clone_all_requests(r);
    setElemCode((long)req);  //-- 'hpcd' C++ fails with 'int' cast
    id = i;
    tasks = 0;

    // Generate the temporary file name where result will be written

    datafile = tempnam(nullptr, request_verb(req));
    unlink(datafile);
    set_value(req, "PATH", "%s", datafile);

    const char* tmp;
    if ((tmp = get_value(req, "_NAME", 0)))
        setElemName(tmp);
    else
        setElemName(datafile);
}

MvDataGen::MvDataGen(svcid* i, MvDataGen* mdg)
{
    datatype = strdup(mdg->datatype);
    datafile = strdup(mdg->datafile);

    req = nullptr;
    setElemCode(mdg->getElemCode());
    id = i;
    tasks = 0;

    setElemName(mdg->getElemName());
}

int MvDataGen ::Ready()
{
    if (datafile) {
        if (access(datafile, F_OK)) {
            marslog(LOG_INFO, "File %s has not been created yet !", datafile);
            return 0;
        }
        else
            return 1;
    }

    return 0;
}

void MvDataGen ::DeleteFile()
{
    if (datafile) {
        unlink(datafile);
        marslog(LOG_INFO, "~MvDataVis deleting %s", datafile);
    }
}

MvDataGen ::~MvDataGen()
{
    if (req)
        free_all_requests(req);
    delete datatype;
    delete datafile;
}

void MvDataGen ::SendStatus(const char* st)
{
    request* status = empty_request("STATUS");
    const char* tmp;

    set_value(status, "STATUS", "%s", st);
    if ((tmp = get_value(req, "_NAME", 0)))
        set_value(status, "NAME", "%s", tmp);

    if ((tmp = get_value(req, "_ICON_CLASS", 0)))
        set_value(status, "ICON_CLASS", "%s", tmp);

    send_message(id->s, status);
    free_all_requests(status);
}

void MvDataGen ::SendReply(err status)
{
    set_svc_err(id, status);
    request* reply = nullptr;

    if (status == 0) {
        // Build the reply request.

        reply = empty_request(datatype);
        set_value(reply, "PATH", "%s", datafile);
        set_value(reply, "TEMPORARY", "1");
        pool_store(id->s, getElemName(), "DATA", reply);

        char name1[132];
        sprintf(name1, "%sINP", getElemName());
        pool_link(id->s, name1, getElemName());

        print_all_requests(reply);
    }

    send_reply(id, reply);
    free_all_requests(reply);
}

MvDataVis ::MvDataVis(svcid* i, request* r)
{
    req = clone_all_requests(r);
    setElemCode((long)req);  //-- 'hpcd' C++ fails with 'int' cast
    id = i;
    tasks = 0;

    // Generate the temporary file name where result will be written

    char* tmpfile = tempnam(nullptr, request_verb(req));
    set_value(req, "PATH", "%s", tmpfile);
    unlink(tmpfile);

    const char* tmp;
    if ((tmp = get_value(req, "_LOT_NAME", 0)))
        setElemName(tmp);
    else
        setElemName(tmpfile);

    if ((tmp = get_value(req, "_LOT_COUNT", 0)))
        tasks = atoi(tmp);

    // Build the reply request.

    reply = empty_request("");

    if ((tmp = get_value(req, "_DROP_ID", 0)))
        set_value(reply, "_DROP_ID", "%s", tmp);

    delete tmpfile;
}

MvDataVis ::~MvDataVis()
{
    const char* tmpfile = get_value(req, "PATH", 0);

    if (tmpfile) {
        unlink(tmpfile);
        marslog(LOG_INFO, "~MvDataVis deleting %s", tmpfile);
    }

    free_all_requests(req);
    free_all_requests(reply);
}

void MvDataVis ::SendStatus(const char* st)
{
    request* status = empty_request("STATUS");
    const char* tmp;

    set_value(status, "STATUS", "%s", st);
    if ((tmp = get_value(req, "_NAME", 0)))
        set_value(status, "NAME", "%s", tmp);

    if ((tmp = get_value(req, "_ICON_CLASS", 0)))
        set_value(status, "ICON_CLASS", "%s", tmp);

    send_message(id->s, status);
    free_all_requests(status);
}

void MvDataVis ::SendReply(err status)
{
    set_svc_err(id, status);

    if (status == 0) {
        // Send data visualisation request to corresponding service
    }

    send_reply(id, reply->next);
}

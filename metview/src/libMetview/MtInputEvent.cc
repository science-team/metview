/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <MtInputEvent.h>

MtInputEvent::MtInputEvent() :
    type_(Mt::NoEvent)
{
    // empty
}


MtMouseEvent::MtMouseEvent() :
    button_(Mt::NoButton),
    x_(0),
    y_(0)
{
    // empty
}

MtKeyEvent::MtKeyEvent() :
    key_(Mt::Key_Unknown)
{
    // empty
}
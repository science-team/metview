/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

/* Microsoft Windows Visual Studio support */
#if defined(_WIN32) && defined(_MSC_VER)
#define METVIEW_ON_WINDOWS

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define popen(cmd, mode) _popen(cmd, mode)
#define pclose(stream) _pclose(stream)

#define WEXITSTATUS(status) (status)

#endif

/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/
#pragma once


#include <iosfwd>
#include <string>
#include <vector>

class MvRttov
{
public:
    MvRttov();
    ~MvRttov();

    bool parseFile(std::string& fileName);
    std::string errorMessage() { return errorMsg_; }
    std::vector<int>& channels() { return channels_; }
    std::vector<double>& tbs() { return tbs_; }
    std::vector<double>& jacobianPressures() { return jacobians_[0]; }
    std::vector<double>& jacobianValues(size_t i) { return jacobians_[i + 1]; }


private:
    bool searchForLineContainingString(std::ifstream& ifs, std::string& str);

    int currentLine_;
    std::string fileName_;
    std::string errorMsg_;
    std::vector<int> channels_;
    std::vector<double> tbs_;
    std::vector<int> jacobianChannels_;
    std::vector<std::vector<double> > jacobians_;
};

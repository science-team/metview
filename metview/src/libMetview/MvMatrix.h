/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// MvMatrix class provides tools to manipulate a matrix of elements.
// The type of the elements are double.
// The template facility could be used to define a general type for
// the elements. However, compilation problems was found in SGI
//  platforms (probably in Alpha too). IBM and Linux work fine.

#pragma once

#include <vector>

//! Class to handle a matrix of elements
/*! MvMatrix class provides tools to manipulate a matrix of elements.
 *  The type of the elements are double.
 *  The template facility could be used to define a general type for
 *  the elements. However, compilation problems was found in SGI
 *  platforms (probably in Alpha too). IBM and Linux work fine.
 */
class MvMatrix
{
    int nlin_;                 // number of lines
    int ncol_;                 // number of columns
    std::vector<double> vec_;  // matrix

    MvMatrix() = default;

public:
    //! Constructor for \c nlin times \c ncol matrix
    MvMatrix(int nlin, int ncol);

    //! Copy constructor
    MvMatrix(const MvMatrix& m);

    //! Destructor
    ~MvMatrix() = default;

    //! Assignment operator
    void operator=(const MvMatrix& m);

    //! Returns the number of lines in the matrix
    int Nlin() const { return nlin_; }

    //! Returns the number of columns in the matrix
    int Ncol() const { return ncol_; }

    //! Assign value \c val into matrix element [ \c lin , \c col ]
    /*! Returns \c false if [ \c lin , \c col ] is outside the matrix
     */
    bool Mput(int lin, int col, double val);

    //! Get the value from matrix cell [ \c lin , \c col ]
    /*! Returns \c DBL_MAX if [ \c lin , \c col ] is outside the matrix
     */
    double Mget(int lin, int col);
};

/***************************** LICENSE START ***********************************

 Copyright 2023 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvExitStatus.h"

#include <sys/wait.h>

MvExitStatus::MvExitStatus(int ret) : ret_(ret), wRet_(WEXITSTATUS(ret))
{
}

bool MvExitStatus::isWarning() const
{
    return (ret_ == -1 || wRet_ != 0) && wRet_ ==  255;
}

bool MvExitStatus::isError() const
{
    return (ret_ == -1 || wRet_ != 0) && wRet_ !=  255;
}

bool MvExitStatus::isSuccess() const
{
    return !isWarning() && !isError();
}


#pragma once

typedef struct
{
    char id[13];  // image identifier

    short satellite,  // satellite identifier xxxx
        band,         // spectral band
        year,         // image year >1900 xxxx
        month,        // image mouth 1-12 xxxx
        day,          // image day 1-31 xxxx
        hour,         // image hour 0-23 xxxx
        minutes,      // image minutes 0-59 xxxx
        seconds,      // image seconds 0-59 xxxx
        nx,           // number of columns
        ny,           // number of lines
        xp,           // column of sub-satellite point
        yp,           // line of sub-satellite point
        x0,           // image origin column
        y0,           // image origin line
        path,         // orbit number xxxx
        row,          // image number xxxx
        plevel,       // processing level
        vc,           // current volume
        ac;           // currente file

    float lap,  // latitude of sub-satellite point
        lop,    // longitude of sub-satellite point
        yaw,    // orientation angle ... definir grau ou rad??????
        nr,     // altitude or radius of satellite in meters ????????
        rx,     // horizontal resolution in meters
        ry;     // vertical resolution in meters
} ImageDoc;

typedef struct
{
    char quadrant[2];

    short pvol,  // first tape
        uvol,    // last tape
        band,    // banda
        numarq,  // file number
        sta;     // 0 no load
                 // 1 load
                 // 2 already load
} Descritption;

void fill_imagedoc(ImageDoc& im, short wid, short hg, float rx, float ry);

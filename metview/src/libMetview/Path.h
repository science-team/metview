/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <sys/types.h>
#include <time.h>

#include <set>
#include <string>
#include <ostream>

#include "Metview.h"

class MvRequest;

class Path
{
public:
    Path(const std::string&);
    ~Path();

    const std::string& str() const;

    std::string relativePath(const Path&) const;
    Path add(const std::string&) const;

    Path directory() const;
    std::string name() const;
    Path dot() const;

    void rename(const Path&) const;
    void copy(const Path&) const;
    bool copyData(const Path&) const;
    void touch() const;
    void remove() const;
    void mkdir() const;
    void symlink(const Path&) const;
    void changeSymlink(const Path& other) const;
    std::string symlinkTarget() const;

    std::set<std::string> files() const;
    std::set<std::string> directories() const;

    bool exists() const;
    bool locked() const;
    bool isSymLink() const;

    std::string loadText() const;
    void saveText(const std::string&) const;

    MvRequest loadRequest() const;
    void saveRequest(const MvRequest&) const;

    time_t lastModified(bool symLink = false) const;
    off_t sizeInBytes() const;

    void makeWritableByUser() const;
    std::string permissions(bool symLink = false) const;
    std::string owner(bool symLink = false) const;
    std::string group(bool symLink = false) const;
    void nameAndSuffix(std::string& namePart, std::string& suffixPart) const;
    std::string uniqueNameInDir() const;

protected:
    void print(std::ostream&) const;  // Change to virtual if base class

private:
    std::string path_;

    friend std::ostream& operator<<(std::ostream& s, const Path& p)
    {
        p.print(s);
        return s;
    }
};

inline void destroy(Path**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(Path);
//#endif

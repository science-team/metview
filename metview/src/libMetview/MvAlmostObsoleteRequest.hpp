#pragma once


//========================================================================
//== following stuff used to in a file 'MvRequest.hpp'.  It was appended
//== into file DataGen.hpp because ./src/Metview/include contains a file
//== with same name and MvRequest class!  Then 010508 original part of
//== the file was moved to src/AppMod (because of IBM compilation problems)
//== and this remaining was renamed as MvAlmostObsoleteRequest.hpp.

#include "mars.h"
#include "MvList.hpp"

class MvDataGen : public MvElement
{
public:
    MvDataGen() = default;
    MvDataGen(svcid*, request*, const char*);
    MvDataGen(svcid*, MvDataGen*);
    ~MvDataGen();
    MvDataGen(const MvDataGen&) = delete;
    MvDataGen& operator=(const MvDataGen&) = delete;

    request* req{nullptr};
    svcid* id{nullptr};
    int tasks{0};
    char* datatype{nullptr};
    char* datafile{nullptr};
    void SendStatus(const char*);
    void SendReply(err);
    void DeleteFile();
    const char* DataFile();
    int Ready();
    void IncTasks()
    {
        tasks++;
        printf("Tasks = %d\n", tasks);
    }
    int DecTasks()
    {
        tasks--;
        printf("Tasks = %d\n", tasks);
        return tasks;
    }
};

class MvDataVis : public MvElement
{
public:
    request* req;
    request* reply;
    svcid* id;
    int tasks;

    MvDataVis() {}
    MvDataVis(svcid*, request*);
    ~MvDataVis();
    void SendStatus(const char*);
    void SendReply(err);
    void Tasks(int c) { tasks = c; }
    void IncTasks()
    {
        tasks++;
        printf("Tasks = %d\n", tasks);
    }
    int DecTasks()
    {
        tasks--;
        printf("Tasks = %d\n", tasks);
        return tasks;
    }
};

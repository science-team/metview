/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#if defined(__APPLE__) && defined(__MACH__)
#include <limits.h>
#else
#include <values.h>
#endif

#ifndef mv_abs
#define mv_abs(x) (((x) >= 0) ? (x) : -(x))
#endif

#ifndef BIGFLOAT
const float BIGFLOAT = 3.4E35;
#endif

#define K_PATH_SIZE 200

#ifndef PI
const float PI = 3.14159265358979323846;
#endif

#ifndef CDR
const float CDR = 0.017453293;  // Conversion factor: degrees to radians
#endif

#ifndef CRD
const float CRD = 57.295779513;  // Conversion factor: radians to degrees
#endif

#ifndef EARTH_RADIUS
const float EARTH_RADIUS = 6378160.;
#endif

#ifndef FLATTENING
const float FLATTENING = 0.0033528;
#endif

#define NSTEPS 16.

/* define icon classes */

#define ICON_FIELD "RETRIEVE"
#define ICON_IMAGE "RETRIEVE"
#define ICON_OBS "RETRIEVE"
#define ICON_DRAW "Draw"
#define ICON_OTHER_DATA "Otherdata"
#define ICON_MAP "Map"
#define ICON_AXIS "Axis"
#define ICON_KILL "Kill"
#define ICON_VISDEF "Visdef"
#define ICON_WARNING "WarningIcon"

#define ICON_DATA(x)                                     \
    (!strcmp(x, ICON_FIELD) || !strcmp(x, ICON_IMAGE) || \
     !strcmp(x, ICON_OBS) || !strcmp(x, ICON_DRAW) ||    \
     !strcmp(x, ICON_OTHER_DATA))

#define ICON_CHART(x) (!strcmp(x, ICON_MAP) || !strcmp(x, ICON_AXIS))

/* define data types
   Note: As a temporary solution the data type is been used
   to define the icon class
*/

#define DATA_FIELD "Field"
#define DATA_IMAGE "Image"
#define DATA_OBS "Obs"
#define DATA_PHYSICAL "Physical"

/* Visualisation Data State */

typedef enum
{
    K_NO_SEG,
    K_REQUESTED,
    K_CREATED,
    K_SEG_DISPLAYED,
    K_PIX_DISPLAYED
} SegState;

/* define window names */

#define WINDOW_KILL "kill"

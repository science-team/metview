/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <vector>
#include <string>

#include <TableReader.h>
#include <mars.h>

using namespace metview;

// ---------------------------
// --- class MvTableColumn ---
// ---------------------------

class MvTableColumn
{
public:
    enum eTableColumnType
    {
        COL_NUMBER,
        COL_STRING
    };

    MvTableColumn() :
        type_(COL_NUMBER) {}

    std::string& name() { return name_; }
    eTableColumnType type() { return type_; }
    int count() { return (type() == COL_NUMBER) ? dvals_.size() : svals_.size(); }


    void setName(std::string s) { name_ = s; }
    void setType(eTableColumnType t) { type_ = t; }

    std::vector<double>& dVals() { return dvals_; }
    std::vector<std::string>& sVals() { return svals_; }


private:
    // void clear();

    std::string name_;       // each column has an optional name
    eTableColumnType type_;  // what data type is each element in the column

    std::vector<double> dvals_;       // only populated if type is number
    std::vector<std::string> svals_;  // only populated if type is string
};


// ---------------------
// --- class MvTable ---
// ---------------------

class MvTable
{
public:
    MvTable();  // constructor
                // MvTable(const char *name);                                        // constructor with path

    void setPath(std::string& path) { reader_.setPath(path); }
    void setDoubleMissingValue(double d) { doubleMissingValue_ = d; }
    void setStringMissingValue(std::string s) { stringMissingValue_ = s; }
    bool setReaderParameters(request* r);  // set the reader params from a request
    bool read();                           // read the table into memory

    int numColumns() { return numColumns_; }
    MvTableColumn* column(int n) { return &columns_[n]; }  // return pointer to indexed column
    MvTableColumn* column(std::string& name);              // return pointer to named column
    double doubleMissingValue() { return doubleMissingValue_; }
    std::string stringMissingValue() { return stringMissingValue_; }
    std::map<std::string, std::string> userMetaData() { return userMetaData_; }


private:
    int numColumns_;                      // the number of columns
    std::vector<MvTableColumn> columns_;  // the columns themselves
    TableReader reader_;                  // the object to perform the table-reading
    double doubleMissingValue_;           // missing values in the input file will be translated into this
    std::string stringMissingValue_;      // missing values in the input file will be translated into this

    std::vector<int> userColumnIndexes;                               // did the user select certain column indexes to parse?
    std::vector<TableReader::eTableReaderFieldType> userColumnTypes;  // did the user select certain column types?
    std::map<std::string, std::string> userMetaData_;                 // optional user meta-data, e.g. PARAM1=VALUE1
};

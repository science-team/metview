/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once
#include <Metview.h>

//! Implements a wrapper around Fortran programs
/*! This class is used for porting existing Fortran programs
 *  to be used as Metview modules, like the ones in directories
 *  \c src/Divrot, \c src/PottF , \c src/Spectra and \c src/Velstr. \n \n
 *  The main program, normally written using Metview Application Framework
 *  (C++), receives the input request, makes a Fortran readable version
 *  of it using methods addParameter() and then calls the main
 *  Fortran routine. Fortran routine uses MGET* and MLOADG functions to read
 *  the input values (these are the very same "M*" functions that
 *  are documented in Metview User Manual, in chapter 'Using
 *  FORTRAN in Macro'), and MSET* and MNEWG functions to build
 *  a reply request. Method getResult() is used in C++ code
 *  to read the reply request.\n \n
 *  There is a simple example program in \c src/libMetview,
 *  files \c DemoFort.cc and \c demofort.f
 */
class MvFortran
{
    Cached Name;
    Cached Temp;

    MvRequest Param;
    MvRequest Result;

public:
    //! Constructor
    /*! Argument \c name is used in messages for identification.\n \n
     *  Constructor reserves an empty file for the "Fortranized" request
     *  and assigns environment variable \c MREQUEST to point to this file.
     */
    MvFortran(const char* name);

    //! Destructor
    ~MvFortran();

    //! Add a numeric value \c val to the list of Fortran parameters
    void addParameter(double val);

    //! Add a string value \c str to the list of Fortran parameters
    void addParameter(const char* str);

    //! Add a fieldset \c fs to the list of Fortran parameters
    void addParameter(MvFieldSet& fs);

    //! Execute an external command \c cmd (rare)
    int external(const char* cmd);

    //! Get the replay request from the Fortran routine
    MvRequest getResult();
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Tokenizer.h"

Tokenizer::Tokenizer(const std::string& separators)
{
    for (char separator : separators)
        separator_.insert(separator);
}

Tokenizer::~Tokenizer() = default;

void Tokenizer::operator()(const std::string& raw, std::vector<std::string>& v)
{
    int index = 0;
    int length = raw.length();
    std::string token = "";

    while (index < length) {
        char c = raw[index];
        if (separator_.find(c) != separator_.end()) {
            if (token.length() > 0)
                v.push_back(token);
            token = "";
        }
        else
            token += c;

        index++;
    }

    if (token.length() > 0)
        v.push_back(token);
}

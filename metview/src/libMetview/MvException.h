/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

class MvException
{
public:
    MvException(const char* why) :
        what_(why) {}
    MvException(const std::string& why) :
        what_(why) {}
    const char* what() const { return what_.c_str(); }

protected:
    std::string what_;
};

// MVEXCEPTION_H_

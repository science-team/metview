/***************************** LICENSE START ***********************************

 Copyright 2017 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <map>

class BufrFilterDef
{
public:
    void add(const std::string&, const std::string&);
    const std::string& value(const std::string& key) const;
    void clear();

    // bool messageFilter(std::string& res,std::string& err) const;

    // void run();

    std::map<std::string, std::string> vals_;
#if 0
protected:    
    void buildQueryWithId(std::string& q,const std::string& id,const std::string& key) const;
    void buildQueryWithValue(std::string& q,const std::string& id,const std::string& key) const;
    void replaceBlock(std::string& q,const std::string& block) const;
    void addKeyBlock(std::string& q,const std::string& key,const std::string& value) const;
    bool buildIntQuery(std::string& q,const std::string& key,
                                      const std::string& keyValue,
                                      const std::string& param,std::string& err,
                                      int minVal) const;

    bool buildIndexQuery(std::string& q,std::string& err) const;
    bool buildEditionQuery(std::string& q,std::string& err) const;
    bool buildTypeQuery(std::string& q,std::string& err) const;
    bool buildIdentQuery(std::string& q,std::string& err,bool& headerOnly) const;
    bool buildAreaQuery(std::string& q,std::string& err,bool& headerOnly) const;
    bool buildDateQuery(std::string& q,std::string& err,bool& headerOnly) const;

    bool parseDate(std::string& val,std::string& err) const;
    bool parseTime(std::string& val,std::string& hour,std::string& minute,
                                  std::string& second,std::string& err) const;
    bool checkHour(int h,std::string& err) const;
    bool checkMinute(int m,std::string& err) const;
    bool checkSecond(int s,std::string& err) const;
    bool parseTimeWindow(std::string& winVal,int& winSec,std::string& err) const;

    std::string periodQuery(const std::string& startDate,const std::string& startTime,
                                           const std::string& endDate,const std::string& endTime) const;
    std::string periodTimeQuery(const std::string& startTime,const std::string& endTime) const;
    std::string periodDateQuery(const std::string& startDate,const std::string& endDate) const;

    bool checkLon(float lon1,float lon2,std::string& err) const;
    bool checkLat(float lon1,float lon2,std::string& err) const;

    std::string toBold(int v) const;
    std::string toBold(float v) const;
    std::string toBold(const std::string&) const;

#endif
};

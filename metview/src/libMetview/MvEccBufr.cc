/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvEccBufr.h"

#include "MvBufrEdition.h"
#include "LogHandler.h"
#include "MvMiscellaneous.h"
#include "BufrMetaData.h"
#include "Tokenizer.h"

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <vector>

#include "fstream_mars_fix.h"

#include <cstdlib>
#include <cassert>
#include <cstdio>

std::vector<std::string> MvEccBufrMessage::definitionsDir_;

#define MV_CODES_CHECK_HEADER(a, msg)                              \
    {                                                              \
        if (codesCheck(#a, __FILE__, __LINE__, a, msg) == false) { \
            headerValid_ = false;                                  \
        }                                                          \
    }
#define MV_CODES_CHECK(a, msg) codesCheck(#a, __FILE__, __LINE__, a, msg)


//=========================================================
//
// MvEccBufrMessage
//
//=========================================================

MvEccBufrMessage::MvEccBufrMessage(codes_bufr_header* bh, int index) :
    index_(index),
    headerValid_(true),
    dataValid_(true),
    bufrEditionNumber_(-1),
    dataCategory_(-1),
    dataSubCategory_(-1),
    rdbType_(-1),
    subsetNum_(-1),
    compressed_(false),
    hasSection2_(false),
    edition_(nullptr),
    fileInfo_(nullptr),
    offset_(0),
    totalLenght_(0)
{
    if (!bh || bh->message_size == 0) {
        headerValid_ = false;
        dataValid_ = false;
        return;
    }

    offset_ = bh->message_offset;
    totalLenght_ = bh->message_size;

    dataCategory_ = bh->dataCategory;
    dataSubCategory_ = bh->dataSubCategory;

    // Edition
    long masterNumber = 0, masterVersion = 0, localVersion = 0, centre = 0, subCentre = 0;
    masterVersion = bh->masterTablesVersionNumber;
    masterNumber = bh->masterTableNumber;
    localVersion = bh->localTablesVersionNumber;
    centre = bh->bufrHeaderCentre;
    subCentre = bh->bufrHeaderSubCentre;

    edition_ = MvBufrEdition::find(masterNumber, masterVersion, localVersion, centre, subCentre);
    assert(edition_);
    if (edition_->centreAsStr().empty()) {
        size_t vlen = 0;
        char charVal[512] = {
            0,
        };
        charVal[0] = '\n';
        if (codes_bufr_header_get_string(bh, "centre", charVal, &vlen) == CODES_SUCCESS) {
            if (vlen > 0)
                edition_->setCentreAsStr(std::string(charVal));
        }
    }

    // Subsets
    subsetNum_ = bh->numberOfSubsets;
    if (bh->compressedData == 1)
        compressed_ = true;

    if (centre == 98) {
        rdbType_ = bh->rdbType;
    }
}

MvEccBufrMessage::MvEccBufrMessage(codes_handle* h, int index) :
    index_(index),
    headerValid_(true),
    dataValid_(true),
    bufrEditionNumber_(-1),
    dataCategory_(-1),
    dataSubCategory_(-1),
    rdbType_(-1),
    subsetNum_(-1),
    compressed_(false),
    hasSection2_(false),
    edition_(nullptr),
    fileInfo_(nullptr),
    offset_(0),
    totalLenght_(0)
{
    if (!h)
        return;

    long longVal = 0;
    size_t vlen = 0;

    MV_CODES_CHECK_HEADER(codes_get_long(h, "offset", &offset_), nullptr);
    MV_CODES_CHECK_HEADER(codes_get_long(h, "editionNumber", &longVal), nullptr);
    bufrEditionNumber_ = longVal;

    MV_CODES_CHECK_HEADER(codes_get_long(h, "dataCategory", &dataCategory_), nullptr);
    MV_CODES_CHECK_HEADER(codes_get_long(h, "dataSubCategory", &dataSubCategory_), nullptr);

    // Edition
    long masterNumber = 0, masterVersion = 0, localVersion = 0, centre = 0, subCentre = 0;
    MV_CODES_CHECK_HEADER(codes_get_long(h, "masterTableNumber", &masterNumber), nullptr);
    MV_CODES_CHECK_HEADER(codes_get_long(h, "masterTablesVersionNumber", &masterVersion), nullptr);
    MV_CODES_CHECK_HEADER(codes_get_long(h, "localTablesVersionNumber", &localVersion), nullptr);
    MV_CODES_CHECK_HEADER(codes_get_long(h, "bufrHeaderCentre", &centre), nullptr);
    MV_CODES_CHECK_HEADER(codes_get_long(h, "bufrHeaderSubCentre", &subCentre), nullptr);
    edition_ = MvBufrEdition::find(masterNumber, masterVersion, localVersion, centre, subCentre);
    assert(edition_);
    if (edition_->centreAsStr().empty()) {
        const int MAX_CHAR_LEN = 1024;
        char charVal[MAX_CHAR_LEN];
        vlen = MAX_CHAR_LEN;
        MV_CODES_CHECK_HEADER(codes_get_string(h, "bufrHeaderCentre", charVal, &vlen), nullptr);
        if (vlen > 0)
            edition_->setCentreAsStr(std::string(charVal));
    }

    // Subsets
    MV_CODES_CHECK_HEADER(codes_get_long(h, "numberOfSubsets", &subsetNum_), nullptr);
    MV_CODES_CHECK_HEADER(codes_get_long(h, "compressedData", &longVal), nullptr);
    if (longVal == 1)
        compressed_ = true;

    // Unexpanded descriptors
    MV_CODES_CHECK_HEADER(codes_get_size(h, "unexpandedDescriptors", &vlen), nullptr);
    if (vlen > 0) {
        long* descriptors = (long*)malloc(vlen * sizeof(long));
        MV_CODES_CHECK_HEADER(codes_get_long_array(h, "unexpandedDescriptors", descriptors, &vlen), nullptr);
        for (size_t i = 0; i < vlen; i++) {
            unexpanded_.push_back(descriptors[i]);
        }
        free(descriptors);
    }

    if (centre == 98) {
        codes_get_long(h, "rdbType", &rdbType_);
    }
}

MvEccBufrMessage::~MvEccBufrMessage() = default;

int MvEccBufrMessage::centre() const
{
    return (edition_) ? edition_->centre() : 0;
}

int MvEccBufrMessage::subCentre() const
{
    return (edition_) ? edition_->subCentre() : 0;
}

int MvEccBufrMessage::masterTablesNumber() const
{
    return (edition_) ? edition_->masterTablesNumber() : 0;
}

int MvEccBufrMessage::masterTablesVersionNumber() const
{
    return (edition_) ? edition_->masterTablesVersionNumber() : 0;
}

int MvEccBufrMessage::localTablesVersionNumber() const
{
    return (edition_) ? edition_->localTablesVersionNumber() : 0;
}

const std::string& MvEccBufrMessage::centreAsStr() const
{
    static std::string empty;
    return (edition_) ? edition_->centreAsStr() : empty;
}

const std::vector<int>& MvEccBufrMessage::unexpandedDesc()
{
    if (unexpanded_.empty()) {
        getUnexpanded();
    }
    return unexpanded_;
}

void MvEccBufrMessage::getUnexpanded()
{
    if (!fileInfo_)
        return;

    size_t vlen = 0;
    int err = 0;

    FILE* fp = nullptr;
    fp = fopen(fileInfo_->fileName().c_str(), "rb");
    if (!fp) {
        return;
    }

    if (fseek(fp, offset_, 0) != 0) {
        fclose(fp);
        return;
    }

    if (codes_handle* ch = codes_handle_new_from_file(nullptr, fp, PRODUCT_BUFR, &err)) {
        MV_CODES_CHECK_HEADER(codes_get_size(ch, "unexpandedDescriptors", &vlen), nullptr);
        if (vlen > 0) {
            long* descriptors = (long*)malloc(vlen * sizeof(long));
            MV_CODES_CHECK_HEADER(codes_get_long_array(ch, "unexpandedDescriptors", descriptors, &vlen), nullptr);
            for (size_t i = 0; i < vlen; i++) {
                unexpanded_.push_back(descriptors[i]);
            }
            free(descriptors);
        }
        codes_handle_delete(ch);
    }

    fclose(fp);
}

// We do not want to store the dir path in the class to reduce memory usage. Instead we construct these
// strings on demand!
void MvEccBufrMessage::tablesDirs(std::vector<std::string>& tablesMasterDir, std::vector<std::string>& tablesLocalDir)
{
    if (!fileInfo_)
        return;

    const int MAX_CHAR_LEN = 1024;
    char charVal[MAX_CHAR_LEN];
    size_t vlen = 0;
    int err = 0;

    FILE* fp = nullptr;
    fp = fopen(fileInfo_->fileName().c_str(), "rb");
    if (!fp) {
        // log->error("GribMetaData::readMessages() ---> Cannot open grib file: \n        " + fileName_ + "\n");
        return;
    }

    if (fseek(fp, offset_, 0) != 0) {
        fclose(fp);
        return;
    }

    std::string masterDirPattern, localDirPattern;
    if (codes_handle* ch = codes_handle_new_from_file(nullptr, fp, PRODUCT_BUFR, &err)) {
        // tables
        vlen = MAX_CHAR_LEN;
        if (MV_CODES_CHECK(codes_get_string(ch, "tablesMasterDir", charVal, &vlen), nullptr)) {
            masterDirPattern = std::string(charVal);
        }

        vlen = MAX_CHAR_LEN;
        if (MV_CODES_CHECK(codes_get_string(ch, "tablesLocalDir", charVal, &vlen), nullptr)) {
            localDirPattern = std::string(charVal);
        }

        replace(masterDirPattern, "[masterTableNumber]", masterTablesNumber());
        replace(masterDirPattern, "[masterTablesVersionNumber]", masterTablesVersionNumber());
        replace(localDirPattern, "[masterTableNumber]", masterTablesNumber());
        replace(localDirPattern, "[localTablesVersionNumber]", localTablesVersionNumber());
        replace(localDirPattern, "[bufrHeaderCentre:l]", centre());
        replace(localDirPattern, "[bufrHeaderSubCentre]", subCentre());

        if (definitionsDir_.empty()) {
            if (char* defPath = codes_definition_path(nullptr)) {
                std::string dStr(defPath);
                Tokenizer parse(":");
                parse(dStr, definitionsDir_);
            }
        }

        for (const auto& defDir : definitionsDir_) {
            tablesMasterDir.push_back(defDir + "/" + masterDirPattern);
            tablesLocalDir.push_back(defDir + "/" + localDirPattern);
        }

        codes_handle_delete(ch);
    }

    fclose(fp);
}

void MvEccBufrMessage::replace(std::string& str, const std::string& pattern, const std::string& newPattern)
{
    std::size_t pos = str.find(pattern);
    if (pos != std::string::npos) {
        str.replace(pos, pattern.size(), newPattern);
    }
}

void MvEccBufrMessage::replace(std::string& str, const std::string& pattern, int newPattern)
{
    std::stringstream ss;
    ss << newPattern;
    replace(str, pattern, ss.str());
}

void MvEccBufrMessage::checkPar(int err, const std::string& name)
{
    if (err != CODES_SUCCESS) {
        GuiLog().error() << "Could not read key " << name << " for message " << index_
                         << ": " << std::string(grib_get_error_message(err)) << "\n";
    }
}

bool MvEccBufrMessage::codesCheck(const char* call, const char* /*file*/, int /*line*/, int e, const char* /*msg*/)
{
    if (e) {
        const char* m = grib_get_error_message(e);
        GuiLog().error() << call << m;
        headerErrors_.push_back(std::string(call) + " " + std::string(m));
        return false;
    }
    return true;
}


//=========================================================
//
// MvEccBufr
//
//=========================================================

MvEccBufr::MvEccBufr(const std::string& path) :
    fileName_(path),
    messageNum_(computeMessageNum())
{
}

int MvEccBufr::computeMessageNum()
{
    FILE* fp = nullptr;
    fp = fopen(fileName_.c_str(), "rb");
    if (!fp) {
        // log->error("GribMetaData::readMessages() ---> Cannot open grib file: \n        " + fileName_ + "\n");
        return 0;
    }

    int res = 0;
    if (codes_count_in_file(nullptr, fp, &res) != CODES_SUCCESS)
        res = 0;

    fclose(fp);
    return res;
}

int MvEccBufr::scan()
{
    FILE* in = nullptr;

    codes_handle* h = nullptr;
    int err = 0, cnt = 0;

    in = fopen(fileName_.c_str(), "rb");
    if (!in) {
        printf("ERROR: unable to open file %s\n", fileName_.c_str());
        return 1;
    }

    while ((h = codes_handle_new_from_file(nullptr, in, PRODUCT_BUFR, &err)) != nullptr || err != CODES_SUCCESS) {
        auto* m = new MvEccBufrMessage(h, cnt);
        msg_.push_back(m);

        if (h == nullptr) {
            printf("Error: unable to create handle for message %d\n", cnt);
        }
        else {
            codes_handle_delete(h);
        }
        cnt++;
    }

    fclose(in);
    return 0;
}

MvEccBufrMessage* MvEccBufr::message(int /*cnt*/)
{
    return nullptr;
}

MvEccBufr::ElementDefType MvEccBufr::elementDefType(int elem)
{
    if (elem > 0 && elem < 64255) {
        int cat = elem / 1000;
        int code = elem % 1000;
        if (cat >= 0 && cat <= 63 && code >= 1 && code <= 255) {
            if (cat < 48 && code < 192)
                return WmoElement;
            else
                return LocalElement;
        }
    }
    return UnknownElement;
}

bool MvEccBufr::codesCheck(const char* call, const char* /*file*/, int /*line*/, int e, const char* /*msg*/)
{
    if (e) {
        GuiLog().error() << call << grib_get_error_message(e);
        return false;
    }
    return true;
}

//=========================================================
//
// MvBufrCodeTable
//
//=========================================================

std::vector<MvBufrCodeTable*> MvBufrCodeTable::tables_;

MvBufrCodeTable::MvBufrCodeTable(int element, MvBufrEdition* edition, const std::string& path) :
    element_(element),
    edition_(edition)
{
    load(path);
    tables_.push_back(this);
}

const std::string& MvBufrCodeTable::value(int code) const
{
    static std::string es;
    auto it = items_.find(code);
    return (it != items_.end()) ? it->second : es;
}

MvBufrCodeTable* MvBufrCodeTable::find(int element, MvEccBufrMessage* msg)
{
    for (auto table : tables_) {
        if (table->element_ == element && table->edition_ == msg->edition())
            return table;
    }

    return make(element, msg);
}

std::string MvBufrCodeTable::buildFileName(int element, const std::string& path)
{
    return path + "/codetables/" + std::to_string(element) + ".table";
}

MvBufrCodeTable* MvBufrCodeTable::make(int element, MvEccBufrMessage* msg)
{
    // Try
    std::vector<std::string> path;
    std::vector<std::string> masterDir, localDir;
    msg->tablesDirs(masterDir, localDir);

    switch (MvEccBufr::elementDefType(element)) {
        case MvEccBufr::WmoElement:
            path = masterDir;
            break;
        case MvEccBufr::LocalElement:
            path = localDir;
            break;
        default:
            return nullptr;
            break;
    }

    for (const auto& p : path) {
        // Check if the file exists and can be open for reading
        std::string fname = buildFileName(element, p);
        std::ifstream in(fname.c_str(), std::ios::in);
        if (in.is_open()) {
            in.close();
            auto* t = new MvBufrCodeTable(element, msg->edition(), p);
            return t;
        }
    }

    return nullptr;
}


void MvBufrCodeTable::load(const std::string& path)
{
    std::string fname = buildFileName(element_, path);

    std::ifstream in(fname.c_str(), std::ios::in);
    if (!in.is_open()) {
        std::cout << "Cannot open input file: " << fname << std::endl;
        return;
    }

    std::string line;
    while (getline(in, line)) {
        int code = 0;
        std::stringstream sst(line);
        sst >> code >> code;
        std::string val;
        getline(sst, val);
        items_[code] = val;
    }
}


const std::string& MvBufrFlagInfo::bitPosition(int i) const
{
    if (i >= 0 && i < static_cast<int>(flags_.size()))
        return flags_[i].first;

    static std::string es;
    return es;
}

const std::string& MvBufrFlagInfo::description(int i) const
{
    if (i >= 0 && i < static_cast<int>(flags_.size()))
        return flags_[i].second;

    static std::string es;
    return es;
}

//=========================================================
//
// MvBufrFlagTable
//
//=========================================================

std::vector<MvBufrFlagTable*> MvBufrFlagTable::tables_;

MvBufrFlagTable::MvBufrFlagTable(int element, MvBufrEdition* edition, const std::string& path) :
    element_(element),
    edition_(edition)
{
    load(path);
    tables_.push_back(this);
}

void MvBufrFlagTable::values(int code, int width, MvBufrFlagInfo& v) const  // std::vector<std::parstd::string>& vals) const
{
    std::vector<int> bits;
    getBits(code, width, bits);

    std::string bitStr;
    size_t n = bits.size();
    for (size_t i = 0; i < n; i++)
        bitStr += (bits[i] == 0) ? "0" : "1";

    v.bits_ = bitStr;

    for (size_t i = 0; i < n; i++) {
        if (bits[i] == 1) {
            auto it = items_.find(i + 1);
            std::string desc;
            if (it != items_.end()) {
                desc = it->second;
            }
            v.flags_.emplace_back(std::to_string(i + 1), desc);
        }
    }
}

void MvBufrFlagTable::getBits(unsigned int code, int width, std::vector<int>& v)
{
    for (unsigned int mask = 0x1; mask; mask <<= 1) {
        v.push_back((code & mask) ? 1 : 0);
        if (static_cast<int>(v.size()) == width)
            break;
    }

    std::reverse(v.begin(), v.end());
}


MvBufrFlagTable* MvBufrFlagTable::find(int element, MvEccBufrMessage* msg)
{
    for (auto table : tables_) {
        if (table->element_ == element && table->edition_ == msg->edition())
            return table;
    }

    return MvBufrFlagTable::make(element, msg);
}

std::string MvBufrFlagTable::buildFileName(int element, const std::string& path)
{
    return path + "/codetables/" + std::to_string(element) + ".table";
}

MvBufrFlagTable* MvBufrFlagTable::make(int element, MvEccBufrMessage* msg)
{
    // Try
    std::vector<std::string> path;
    std::vector<std::string> masterDir, localDir;
    msg->tablesDirs(masterDir, localDir);

    switch (MvEccBufr::elementDefType(element)) {
        case MvEccBufr::WmoElement:
            path = masterDir;
            break;
        case MvEccBufr::LocalElement:
            path = localDir;
            break;
        default:
            return nullptr;
            break;
    }

    for (const auto& p : path) {
        // Check if the file exists and can be open for read!
        std::string fname = buildFileName(element, p);
        std::ifstream in(fname.c_str(), std::ios::in);
        if (in.is_open()) {
            in.close();
            auto* t = new MvBufrFlagTable(element, msg->edition(), p);
            return t;
        }
    }

    return nullptr;
}

void MvBufrFlagTable::load(const std::string& path)
{
    std::string fname = buildFileName(element_, path);

    std::ifstream in(fname.c_str(), std::ios::in);
    if (!in.is_open()) {
        std::cout << "Cannot open input file: " << fname << std::endl;
        return;
    }

    std::string line;
    while (getline(in, line)) {
        int code = 0;
        std::stringstream sst(line);
        sst >> code >> code;
        std::string val;
        getline(sst, val);
        items_[code] = val;
    }
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

/* Messages */

#define MSG_NOT_IMPL "VisMod.001"

/* System Messages */

#define SYSTEM_GKS_WS "VisMod.200"
#define SYSTEM_UNEXPECTED "VisMod.201"
#define SYSTEM_PW_IO "VisMod.202"
#define SYSTEM_PW_NOT_FOUND "VisMod.203"
#define SYSTEM_GERROR "VisMod.204"
#define SYSTEM_FILE_ERROR "VisMod.205"
#define SYSTEM_VISDEF_FILE_ERROR "VisMod.206"
#define SYSTEM_FILE_NOT "VisMod.207"
#define SYSTEM_PW_INVALID "VisMod.208"
#define SYSTEM_MAGPROC_ERROR "VisMod.209"

/* User Error */

#define DATA_UNIT_EMPTY "VisMod.300"
#define PW_JOIN_ERROR "VisMod.301"

/* MagProc Errors */

#define MAGPROC_OK -1
#define MAGPROC_ERR_MAGICS 0
#define MAGPROC_ERR_VISDEF 1
#define MAGPROC_ERR_FILE 2
#define MAGPROC_ERR_GKS 3
#define MAGPROC_ERR_SLIDE 4
#define MAGPROC_ERR_PHYSICAL 5

#ifdef _MV_DECLARE
/*
 *deleteme*char* MAGPROC_ERRLIST[] =
 *deleteme*{
 *deleteme*	"MagProc.MAG",		* 0 *
 *deleteme*	"MagProc.VISDEF",	* 1 *
 *deleteme*	"MagProc.FILE",		* 2 *
 *deleteme*	"MagProc.GKS",		* 3 *
 *deleteme*	"MagProc.SLIDE",	* 4 *
 *deleteme*	"MagProc.PHYSICAL"	* 5 *
 *deleteme*};
 */

const char* MAGPROC_ERRLIST[] =
    {
        "Magics error",
        "VisDef not found or invalid",
        "Cannot open description file",
        "GKS error",
        "Slide error",
        "Physical error"};

#else
extern const char* MAGPROC_ERRLIST[];
#endif

#ifdef _MV_DECLARE
#undef _MV_DECLARE
#endif

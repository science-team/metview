/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include "MvFwd.h"

class MvAbstractApplication
{
    friend class MvLog;

public:
    MvAbstractApplication(const char* name);
    virtual ~MvAbstractApplication() = default;

    static const std::string& name() { return name_; }
    static void toLog(const std::string& msg, const std::string& details, MvLogLevel level, bool popup);
    static void abortWithPopup(const std::string& text);

protected:
    virtual void writeToLog(const std::string& msg, MvLogLevel level) = 0;
    virtual void writeToUiLog(const std::string& msg, const std::string& details, MvLogLevel level, bool popup) = 0;
    virtual void exitWithError() = 0;

private:
    static MvAbstractApplication* abcApp_;
    static std::string name_;
};

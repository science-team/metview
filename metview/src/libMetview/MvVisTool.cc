/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <MvVisTool.h>

static Cached VISTOOL = "VISTOOL";
static Cached CHANGE = "CHANGE_WINDOW";
static Cached CLOSE = "CLOSE_WINDOW";
static Cached INFO = "WINDOW_INFO";

/*===
REGISTER,
    WINDOW=...
    SERVICE=...
===*/

/*===
REGISTER,
    ID=...
===*/

/*
VISTOOL,
    WINDOW=....
*/

#ifndef DOXYGEN_SHOULD_SKIP_THIS

class RegisterReply : public MvReply
{
    void callback(MvRequest&);

public:
    RegisterReply() :
        MvReply("REGISTER") {}
};
#endif


static RegisterReply* reply = nullptr;

MvVisTool::MvVisTool() :
    MvProtocol(VISTOOL)
{
    VistoolTarget = (Cached) nullptr;

    if (reply == nullptr)
        reply = new RegisterReply;

    addCallbackKeyword(CHANGE);
    addCallbackKeyword(INFO);
    addCallbackKeyword(CLOSE);
}

MvVisTool::~MvVisTool()
{
    disconnect();
}

void RegisterReply::callback(MvRequest& r)
{
    printf("RegisterReply::callback got reply\n");
    r.print();

    auto* tool = (MvVisTool*)getReference();
    if (tool)
        tool->SetID(r("VISTOOL_ID"));
}

void MvVisTool::disconnect()
{
    if (!VistoolId)
        return;

    MvRequest r = "EXIT";
    r("VISTOOL_ID") = VistoolId;

    printf("MvVisTool::disconnect sending\n");
    r.print();

    if (!VistoolTarget)
        VistoolTarget = (Cached) "VisMod";

    MvApplication::callService(VistoolTarget, r, nullptr);
    VistoolId = (Cached) nullptr;
    VistoolTarget = (Cached) nullptr;
}

void MvVisTool::startUp(MvRequest&)
{
}

void MvVisTool::connect()
{
    MvRequest r = "REGISTER";
    r("SERVICE") = progname();
    r("WINDOW") = Window;

    // Copy interest
    int i = 0;
    const char* p;
    while ((p = Setup("interest", i++)))
        r("interest") += p;

    printf("MvVisTool::connect sending\n");
    r.print();

    if (!VistoolTarget)
        VistoolTarget = (Cached) "VisMod";

    MvApplication::callService(VistoolTarget, r, this);
}

void MvVisTool::SetID(const char* name)
{
    VistoolId = name;
    windowChanged();
}

void MvVisTool::callback(MvRequest& r)
{
    const char* v = r.getVerb();

    printf("MvVisTool::callback got request\n");

    r.print();

    if (v == INFO)
        info(r);
    else if (v == CLOSE) {
        disconnect();
        windowClosed();
    }
    else if (v == CHANGE) {
        Window = r("WINDOW");
        windowChanged();
    }
    else if (v == VISTOOL) {
        Window = r("WINDOW");
        Setup = r("SETUP");
        disconnect();  // Exit from previous window

        // For compatibility with VisMod
        VistoolTarget = r("SOURCE");
        connect();  // Register to new window
        startUp(r);
    }

    MvRequest nil;
    sendReply(nil);
}

void MvVisTool::info(MvRequest& r)
{
    printf("MvVisTool::info\n");
    r.print();
}

void MvVisTool::windowClosed()
{
    printf("MvVisTool::windowClosed\n");
}

void MvVisTool::windowIconified()
{
    printf("MvVisTool::windowIconified\n");
}

void MvVisTool::windowChanged()
{
    printf("MvVisTool::windowChanged\n");
}

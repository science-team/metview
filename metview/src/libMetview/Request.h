/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <vector>

#ifndef Metview_H
#include "Metview.h"
#endif

class Path;

class Request : public MvRequest
{
public:
    // -- Exceptions
    // None

    // -- Contructors

    Request();
    Request(const char*);
    Request(const std::string&);

    Request(const Path&);
    Request(const MvRequest&);

    // -- Destructor

    ~Request();  // Change to virtual if base class

    // -- Convertors
    // None

    // -- Operators
    // None

    // -- Methods

    void load(const Path&);
    void save(const Path&) const;

    std::vector<std::string> get(const std::string&) const;
    std::vector<std::string> get(const char*) const;

    void set(const std::string&, const std::vector<std::string>&);
    void set(const char*, const std::vector<std::string>&);

    static Request fromText(const std::string&);

    // -- Overridden methods
    // None

    // -- Class members
    // None

    // -- Class methods
    // None

    // Uncomment for persistent, remove otherwise
    // static os_typespec* get_os_typespec();

protected:
    // -- Members
    // None

    // -- Methods

    // void print(std::ostream&) const; // Change to virtual if base class

    // -- Overridden methods
    // None

    // -- Class members
    // None

    // -- Class methods
    // None

private:
    // No copy allowed

    //	Request(const Request&);
    //	Request& operator=(const Request&);

    // -- Members
    // None

    // -- Methods
    // None

    // -- Overridden methods
    // None

    // -- Class members
    // None

    // -- Class methods
    // None

    // -- Friends

    // friend std::ostream& operator<<(std::ostream& s,const Request& p)
    //	{ p.print(s); return s; }
};

inline void destroy(Request**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(Request);
//#endif

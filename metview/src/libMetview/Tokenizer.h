/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// File Tokenizer.h
// Manuel Fuentes - ECMWF Jan 97

#pragma once

#include <string>
#include <set>
#include <vector>

class Tokenizer
{
public:
    // -- Contructors

    Tokenizer(const std::string&);

    // -- Destructor

    ~Tokenizer();  // Change to virtual if base class

    // -- Methods

    void operator()(const std::string&, std::vector<std::string>&);
    void operator()(std::istream&, std::vector<std::string>&);

private:
    // No copy allowed

    Tokenizer(const Tokenizer&);
    Tokenizer& operator=(const Tokenizer&);

    // -- Members

    std::set<char> separator_;  // To make searching faster

    // -- Methods

    void print(std::ostream&) const;

    friend std::ostream& operator<<(std::ostream& s, const Tokenizer& p)
    {
        p.print(s);
        return s;
    }
};

inline void destroy(Tokenizer**) {}

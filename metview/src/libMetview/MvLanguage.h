/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

//! Class to handle Metview language syntax files
/*! This is a C++ wrapper around libMars C functions in \c src/libMars/expand.c.\n \n
 *  NOTE: class has no explicit copy constructors or operator=,
 *  so copies share the same pointers!
 */
class MvLanguage
{
    long Flags;
    request* Lang;
    rule* Rule;

    // Note : no destructors, copy construtors or operator=,
    // so copies share the same pointers

public:
    //! Constructor
    /*! Argument \c langFile is the file name of the language syntax file,
     *  \c rulesFile is the name of the rules file (use \c 0 if no rules file)
     *  and \c expandFlags is a combination of bits defining how requests
     *  should be expanded (see file \c src/libMars/mars.h for
     *  defined mnemonics and their values).
     */
    MvLanguage(const char* langFile, const char* rulesFile, long expandFlags = EXPAND_2ND_NAME);

    //! Destructor
    ~MvLanguage();

    //

    //! Removes non-matching parameters
    /*! Removes parameters that are not defined in the language file
     *  from all requests in \c req
     */
    MvRequest trimAll(const MvRequest& req);

    //! Expands all parameters in all requests in \c req
    MvRequest expandAll(const MvRequest& req);

    //! Removes non-matching parameters
    /*! Removes parameters that are not defined in the language file
     *  from the current request in \c req
     */
    MvRequest trimOne(const MvRequest& req);

    //! Expands all parameters in the current request in \c req
    MvRequest expandOne(const MvRequest& req);

    //
    //! Set request expansion flags
    /*! For values and mnemonics, see \c EXPAND_* defines in file \c src/libMars/mars.h
     */
    void setFlags(long f) { Flags = f; }

    //! Resets the language file
    void reset(void);
};

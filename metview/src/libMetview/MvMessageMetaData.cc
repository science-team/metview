/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvMessageMetaData.h"

#include <algorithm>

void MvMessageMetaData::registerObserver(MvMessageMetaDataObserver* o)
{
    auto it = std::find(observers_.begin(), observers_.end(), o);
    if (it == observers_.end())
        observers_.push_back(o);
}


void MvMessageMetaData::broadcast(IntMethod proc, int n)
{
    for (auto observer : observers_) {
        (observer->*proc)(n);
    }
}

void MvMessageMetaData::broadcast(VoidMethod proc)
{
    for (auto observer : observers_) {
        (observer->*proc)();
    }
}

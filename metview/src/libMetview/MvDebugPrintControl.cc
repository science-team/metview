/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//-- MvDebugPrintControl.cc                       Jan04/vk
//
//  This function should be used to allow external control
//  over the amount of information that Metview modules
//  print for development purposes and/or into log files.
//
//  NOTE 041012: you can as well test 'mars.debug' before
//               printing, or 'mars.info' if appropriate.
//               Or even 'mars.warning'!
//
//  Usage:
//
//    //if( mvDebugPrint() ) //-- semi-obsolete
//    if( mars.debug )
//    {
//       std::cout << "bla bla bla..." << std::endl;
//       myRequest.print();
//       // std::cerr << "an error..."   // NO! Always show error msgs!!
//    }
//


#include <cstdlib>
#include "MvDebugPrintControl.h"
#include "mars.h"


bool mvDebugPrint()
{
    if (getenv("MV_DEBUG_PRINT")) {
        return atoi(getenv("MV_DEBUG_PRINT")) != 0;
    }

    return false;
}


void mvSetMarslogLevel()
{
    const char* myEnv = getenv("MV_QLOG");  //-- "quiet log" - only errors!

    if (myEnv && strcmp(myEnv, "yes") == 0) {
        mars.debug = false;
        mars.info = false;
        mars.warning = false;
    }
}

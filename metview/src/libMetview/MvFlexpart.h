/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <vector>

class MvFlexpartRelease
{
public:
    int num;
    int start;
    int end;
    int levType;
    float west;
    float east;
    float north;
    float south;
    float top;
    float bottom;
    std::string name;
    std::vector<float> massForSpec;
};


class MvFlexpartHeader
{
public:
    MvFlexpartHeader(const std::string&);

    void print();

    int runDate;
    int runTime;
    int interval;
    int averagingInterval;
    int samplingInterval;
    float west;
    float south;
    float north;
    float east;
    int nx;
    int ny;
    float dx;
    float dy;
    std::vector<float> levels;
    int specNum;
    std::vector<std::string> specNames;
    int specPointNum;
    std::vector<MvFlexpartRelease> release;
    int method;
    bool hasSubGrid;
    bool hasConvection;
    int indSource;
    int indReceptor;
    std::vector<int> ageClass;
};

class MvFlexpartDates
{
public:
    MvFlexpartDates(const std::string&);

    void print();

    int num;
    std::vector<std::string> date;
    std::vector<std::string> time;
};

#pragma once

#include <cassert>

// The require and ensure macros implement
// the pre-conditions and post-conditions
// for each module.
//
// The "invariant" macro verifies is an
// invariant condition is true.
//
// The keywords "require", "invariant" and "ensure"
// come from the Eiffel language. For more discussion
// on these issues, please see:
//
// - Struostrup, "The C++ Programming Language", page 417.
//
// - Eilens, "Principles of Object-Oriented Programming"


// extern "C" {void assert(int)}


#define require(statement) assert(statement)
#define ensure(statement) assert(statement)

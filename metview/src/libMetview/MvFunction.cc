/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <stdarg.h>
#include <Metview.h>

#ifndef DOXYGEN_SHOULD_SKIP_THIS

// it seems that this class is not used anywhere (2008-04-10/vk)

MvFunction* MvFunction::First = nullptr;

MvFunction::MvFunction(const char* name, const argdef* args)
{
    Arguments = args;
    Count = 0;
    Name = strcache(name);

    while (args->name) {
        Count++;
        args++;
    }


    if (First == nullptr)
        support_recording(MvApplication::getService());
    Next = First;
    First = this;

    add_function_callback(MvApplication::getService(), Name, _serve,
                          getInfo(), (argdef*)Arguments, (void*)this);
}

void MvFunction::record(const char* name, ...)
{
    MvFunction* f = First;

    while (f) {
        if (strcmp(name, f->Name) == 0) {
            va_list list;
            va_start(list, name);
            record_function(MvApplication::getService(), name,
                            (argdef*)f->Arguments, list);
            va_end(list);
            return;
        }
        f = f->Next;
    }
    marslog(LOG_WARN, "Function %s not found", name);
}
#endif

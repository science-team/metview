/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvException.h"

#ifdef FORTRAN_NO_UNDERSCORE
#define mvabort_ mvabort
#endif

extern "C" {
//-- Magics calls this one if serious error --//
void mvabort_(const char* msg);
}

//******************i*******
void mvabort_(const char* msg)
{
    throw MvException(msg);
}
//**************************

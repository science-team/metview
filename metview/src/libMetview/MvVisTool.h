/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once
#include <Metview.h>

//! \brief (semi-obsolete class)
//!
//! Used only by GribTooll

class MvVisTool : public MvProtocol
{
    Cached VistoolId;
    Cached VistoolTarget;
    MvRequest Setup;

    virtual void callback(MvRequest&);

    void connect(void);
    void disconnect(void);

protected:
    MvRequest Window;

    /* To override ... */

    virtual void info(MvRequest&);
    virtual void startUp(MvRequest&);
    virtual void windowClosed();
    virtual void windowChanged();
    virtual void windowIconified();

public:
    MvVisTool();
    ~MvVisTool();

    void SetID(const char*);
};

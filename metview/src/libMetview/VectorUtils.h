/***************************** LICENSE START ***********************************

 Copyright 2020- ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#define MV_VECTOR_READ_OK 0
#define MV_VECTOR_READ_ERROR 1

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif
int read_vector_from_request(request* r, double** vec, int* length);
#if defined(__cplusplus) || defined(c_plusplus)
};
#endif

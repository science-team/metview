/***************************** LICENSE START ***********************************

 Copyright 2022 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <memory>

enum class MvLogLevel
{
    INFO,
    WARN,
    ERROR,
    DBG
};
class MvApplication;

namespace metview
{
class StatsCompute;
using StatsComputePtr = std::shared_ptr<StatsCompute>;
}  // namespace metview

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <float.h>
#include "MvMatrix.h"

MvMatrix::MvMatrix(int nlin, int ncol) :
    nlin_(nlin),
    ncol_(ncol)
//      vec_  (nlin*ncol) //Unfortunately, SGI compiler gives an error.
// Solution: the use of the resize command.
{
    vec_.resize(nlin * ncol);
}

MvMatrix::MvMatrix(const MvMatrix& m) = default;

void MvMatrix::operator=(const MvMatrix& m)
{
    if (&m == this)
        return;

    nlin_ = m.nlin_;
    ncol_ = m.ncol_;
    vec_ = m.vec_;
}

bool MvMatrix::Mput(int lin, int col, double val)
{
    if (lin < 0 || lin >= nlin_ || col < 0 || col >= ncol_)
        return false;

    vec_[lin * ncol_ + col] = val;

    return true;
}

double MvMatrix::Mget(int lin, int col)
{
    if (lin < 0 || lin >= nlin_ || col < 0 || col >= ncol_)
        return DBL_MAX;

    return vec_[lin * ncol_ + col];
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

class MvKeyProfile;
class MvKey;

class BufrLocationCollector
{
public:
    BufrLocationCollector(MvKeyProfile* prof, int maxNum);
    void add(int msg, int subset, int rank, double lat, double lon);
    MvKeyProfile* profile() const { return prof_; }
    int totalNum() const { return totalNum_; }
    int maxNum() const { return maxNum_; }

protected:
    MvKeyProfile* prof_;
    int maxNum_;
    int totalNum_;
    MvKey* msgKey_;
    MvKey* subsetKey_;
    MvKey* rankKey_;
    MvKey* latKey_;
    MvKey* lonKey_;
};

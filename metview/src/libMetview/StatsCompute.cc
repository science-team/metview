/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "StatsCompute.h"

#include <algorithm>
#include <cmath>

namespace metview
{

void AggregateStatsCompute::reset()
{
    aggr_ = 0.;
    aggr2_ = 0.;
    weight_ = 0.;
    cnt_ = 0;
}

//====================================
//
//   AverageStatsCompute
//
//====================================

void MeanStatsCompute::add(double v)
{
    aggr_ += v;
    cnt_++;
}

void MeanStatsCompute::add(double v, double weight)
{
    aggr_ += v * weight;
    weight_ += weight;
    cnt_++;
}

double MeanStatsCompute::compute(bool weighted)
{
    if (cnt_ > 0) {
        if (weighted) {
            return aggr_ / weight_;
        }
        return aggr_ / static_cast<double>(cnt_);
    }
    return invalidVal_;
}

//====================================
//
//   VarianceStatsCompute
//
//====================================

void VarianceStatsCompute::add(double v)
{
    aggr_ += v;
    aggr2_ += v * v;
    cnt_++;
}

void VarianceStatsCompute::add(double v, double weight)
{
    double wv = v * weight;
    aggr_ += wv;
    aggr2_ += wv * v;
    weight_ += weight;
    cnt_++;
}

double VarianceStatsCompute::compute(bool weighted)
{
    if (cnt_ > 0) {
        if (weighted) {
            return aggr2_ / weight_ - (aggr_ * aggr_) / (weight_ * weight_);
        }
        auto n = static_cast<double>(cnt_);
        return aggr2_ / n - (aggr_ * aggr_) / (n * n);
    }
    return invalidVal_;
}

//====================================
//
//   StdevStatsCompute
//
//====================================

double StdevStatsCompute::compute(bool weighted)
{
    if (cnt_ > 0) {
        return std::sqrt(VarianceStatsCompute::compute(weighted));
    }
    return invalidVal_;
}

//====================================
//
//   MaxStatsCompute
//
//====================================

void MaxStatsCompute::reset()
{
    val_ = startVal_;
}

void MaxStatsCompute::add(double v)
{
    if (v > val_) {
        val_ = v;
    }
}

void MaxStatsCompute::add(double v, double /*weight*/)
{
    MaxStatsCompute::add(v);
}

double MaxStatsCompute::compute(bool /*weighted*/)
{
    if (val_ == startVal_) {
        return invalidVal_;
    }
    return val_;
}

//====================================
//
//   MinStatsCompute
//
//====================================

void MinStatsCompute::reset()
{
    val_ = startVal_;
}

void MinStatsCompute::add(double v)
{
    if (v < val_) {
        val_ = v;
    }
}

void MinStatsCompute::add(double v, double /*weight*/)
{
    MinStatsCompute::add(v);
}

double MinStatsCompute::compute(bool /*weighted*/)
{
    if (val_ == startVal_) {
        return invalidVal_;
    }
    return val_;
}

//====================================
//
//   MedianStatsCompute
//
//====================================

void ArrayStatsCompute::reset()
{
    vals_.clear();
}

double MedianStatsCompute::compute(bool /*weighted*/)
{
    if (!vals_.empty()) {
        using difference_type = std::vector<double>::difference_type;

        if (vals_.size() == 1) {
            return vals_[0];
        }
        std::size_t n = vals_.size() / 2;
        std::nth_element(vals_.begin(), vals_.begin() + static_cast<difference_type>(n), vals_.end());
        if (vals_.size() % 2 == 1) {
            return vals_[n];
        }
        auto vn = vals_[n];
        std::nth_element(vals_.begin(), vals_.begin() + static_cast<difference_type>(n - 1), vals_.end());
        const double c2 = 2.;
        return (vn + vals_[n - 1]) / c2;
    }
    return invalidVal_;
}

}  // namespace metview

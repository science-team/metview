/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

/*
    class MvVersionInfo
    purpose: to provide information about Metview's version numbers, date, etc.
    Note: the constructor loads the relevant information.
*/
class MvAbstractVersionInfo
{
public:
    virtual ~MvAbstractVersionInfo() = default;

    bool infoFound() const { return info_found_; }
    int fileVersion() const { return file_version_; }
    int majorVersion() const { return major_; }
    int minorVersion() const { return minor_; }
    int revision() const { return revision_; }
    int version() const { return version_; }
    int year() const { return year_; }
    const std::string& name() const { return name_; }
    const std::string& nameAndVersion() const { return nameAndVersion_; }
    const std::string& label() const { return label_; }
    const std::string& period() const { return copyright_period_; }
    const std::string& releaseDate() const { return release_date_; }
    const std::string& errorMessage() const { return error_message_; }
    const std::string& installDir() const { return install_dir_; }

protected:
    MvAbstractVersionInfo() = default;

    bool info_found_;
    int file_version_;
    int major_;
    int minor_;
    int revision_;
    int version_;
    int year_;
    std::string name_;
    std::string nameAndVersion_;
    std::string label_;
    std::string copyright_period_;
    std::string release_date_;
    std::string error_message_;
    std::string install_dir_;
};

class MvVersionInfo : public MvAbstractVersionInfo
{
public:
    MvVersionInfo();
    ~MvVersionInfo() override = default;

protected:
    void init();
};

class MvGribApiVersionInfo : public MvAbstractVersionInfo
{
public:
    MvGribApiVersionInfo();
    ~MvGribApiVersionInfo() override = default;
};

#ifdef METVIEW
class MvNetcdfVersionInfo : public MvAbstractVersionInfo
{
public:
    MvNetcdfVersionInfo();
    ~MvNetcdfVersionInfo() {}
};
#endif

#ifdef ECCODES_UI
class CodesUiVersionInfo : public MvAbstractVersionInfo
{
public:
    CodesUiVersionInfo();
    ~CodesUiVersionInfo(){};
};
#endif

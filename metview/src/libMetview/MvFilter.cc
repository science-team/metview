/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <Metview.h>

void MvFilter::_init(const char* name)
{
    value* v = new_value(strcache(name));
    Condition = new_condition(t_val, (condition*)v, nullptr);
}

MvFilter::MvFilter(condition* c)
{
    Condition = c;
}

MvFilter::MvFilter(const char* c)
{
    _init(c);
}

MvFilter::MvFilter(const MvFilter& f)
{
    Condition = clone_condition(f.Condition);
}


MvFilter::~MvFilter()
{
    free_condition(Condition);
}


MvFilter& MvFilter::operator=(const MvFilter& f)
{
    free_condition(Condition);
    Condition = nullptr;
    Condition = clone_condition(f.Condition);
    return *this;
}

// Test


int MvFilter::operator()(const MvRequest& r)
{
    return condition_check(r, Condition);
}

// Operators


MvFilter MvFilter::operator&&(const MvFilter& f)
{
    condition* c = new_condition(t_and,
                                 clone_condition(Condition),
                                 clone_condition(f.Condition));
    return {c};
}

MvFilter MvFilter::operator||(const MvFilter& f)
{
    condition* c = new_condition(t_or,
                                 clone_condition(Condition),
                                 clone_condition(f.Condition));
    return {c};
}

MvFilter MvFilter::operator!()
{
    condition* c = new_condition(t_not,
                                 clone_condition(Condition), nullptr);
    return {c};
}

//------------------------------------------------------------------------

MvFilter MvFilter::_newop(testop op, char* x)
{
    MvFilter f(x);
    condition* c = new_condition(op, clone_condition(Condition),
                                 clone_condition(f.Condition));
    return {c};
}

MvFilter MvFilter::_newop(testop op, double x)
{
    char buf[80];
    sprintf(buf, "%g", x);
    return _newop(op, buf);
}

MvFilter MvFilter::_newop(testop op, int x)
{
    char buf[80];
    sprintf(buf, "%d", x);
    return _newop(op, buf);
}

MvFilter MvFilter::operator==(char* x)
{
    return _newop(t_eq, x);
}
MvFilter MvFilter::operator==(double x)
{
    return _newop(t_eq, x);
}
MvFilter MvFilter::operator==(int x)
{
    return _newop(t_eq, x);
}
MvFilter MvFilter::operator!=(char* x)
{
    return _newop(t_ne, x);
}
MvFilter MvFilter::operator!=(double x)
{
    return _newop(t_ne, x);
}
MvFilter MvFilter::operator!=(int x)
{
    return _newop(t_ne, x);
}
MvFilter MvFilter::operator<=(char* x)
{
    return _newop(t_le, x);
}
MvFilter MvFilter::operator<=(double x)
{
    return _newop(t_le, x);
}
MvFilter MvFilter::operator<=(int x)
{
    return _newop(t_le, x);
}
MvFilter MvFilter::operator>=(char* x)
{
    return _newop(t_ge, x);
}
MvFilter MvFilter::operator>=(double x)
{
    return _newop(t_ge, x);
}
MvFilter MvFilter::operator>=(int x)
{
    return _newop(t_ge, x);
}
MvFilter MvFilter::operator>(char* x)
{
    return _newop(t_gt, x);
}
MvFilter MvFilter::operator>(double x)
{
    return _newop(t_gt, x);
}
MvFilter MvFilter::operator>(int x)
{
    return _newop(t_gt, x);
}
MvFilter MvFilter::operator<(char* x)
{
    return _newop(t_lt, x);
}
MvFilter MvFilter::operator<(double x)
{
    return _newop(t_lt, x);
}
MvFilter MvFilter::operator<(int x)
{
    return _newop(t_lt, x);
}

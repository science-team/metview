/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <iostream>
#include <string>

namespace Mt
{
enum InputEvent
{
    NoEvent,
    MousePressEvent,
    MouseMoveEvent,
    MouseReleaseEvent,
    KeyPressEvent,
    KeyReleaseEvent
};
enum MouseButton
{
    NoButton = 0,
    LeftButton = 1,
    RightButton = 2,
    MidButton = 4
};
enum Key
{
    Key_Unknown,
    Key_Left,
    Key_Up,
    Key_Right,
    Key_Down
};

};  // namespace Mt

class MtInputEvent
{
public:
    MtInputEvent();
    // MtInputEvent(MtInputEvent&);

    void setType(Mt::InputEvent t) { type_ = t; }
    Mt::InputEvent type() const { return type_; }

protected:
    Mt::InputEvent type_;
};


class MtMouseEvent : public MtInputEvent
{
public:
    MtMouseEvent();
    // MtMouseEvent(MtMouseEvent&);

    int x() { return x_; }
    int y() { return y_; }
    int button() { return button_; }

    void setX(int i) { x_ = i; }
    void setY(int i) { y_ = i; }
    void setButton(Mt::MouseButton i) { button_ = button_ | i; }

private:
    int button_;
    int x_;
    int y_;
};


class MtKeyEvent : public MtInputEvent
{
public:
    MtKeyEvent();
    // MtKeyEvent(MtKeyEvent&);

    Mt::Key key() const { return key_; }
    void setKey(Mt::Key k) { key_ = k; }

    std::string text() const { return text_; }
    void setText(std::string s) { text_ = s; }

private:
    Mt::Key key_;
    std::string text_;
};

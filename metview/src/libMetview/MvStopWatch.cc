/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// rev vk 031125 --------------- (vk Jun92) ---------------- MvStopWatch.cc
//

#include <sys/time.h>
#include <unistd.h>

#include <iostream>

#include "MvStopWatch.h"

//_________________________________________________________ Constructors

MvStopWatch::MvStopWatch() :
    explanationString_("stopwatch")
{
    startTimer();
}
//_________________________________________________________

MvStopWatch::MvStopWatch(const char* anExplanationString) :
    explanationString_(anExplanationString)
{
    startTimer();
}
//_________________________________________________________ Destructor

MvStopWatch::~MvStopWatch()
{
    printHead("stop - final total");
    printTimes(startingTimeStamp_, startingClockStamp_);

    printHead("stop-watchRemoved!");
    printDateTime();
}
//_________________________________________________________ reset

void MvStopWatch::reset()
{
    reset("\0");
}
//_________________________________________________________

void MvStopWatch::reset(const char* anExplanationString)
{
    printHead("reset - total time until now");
    printTimes(startingTimeStamp_, startingClockStamp_);

    printHead("reset - start new stopwatch!");
    std::cout << anExplanationString << " - ";
    printDateTime();

    explanationString_ = anExplanationString;

    startingTimeStamp_ = previousTimeStamp_ = currentTimeStamp_;
    startingClockStamp_ = previousClockStamp_ = currentClockStamp_;
}
//_________________________________________________________ lapTime

void MvStopWatch::lapTime()
{
    lapTime("\0");
}
//_________________________________________________________

void MvStopWatch::lapTime(const char* anExplanationString)
{
    printHead("laptime");
    std::cout << anExplanationString << ": ";
    printTimes(previousTimeStamp_, previousClockStamp_);

    previousTimeStamp_ = currentTimeStamp_;
    previousClockStamp_ = currentClockStamp_;
}
//_________________________________________________________ startTimer

void MvStopWatch::startTimer()
{
    printHead("start stopwatch");
    printDateTime();

    gettimeofday(&startingClockStamp_, nullptr);
    previousClockStamp_ = currentClockStamp_ = startingClockStamp_;

    times(&currentTimeStamp_);
    startingTimeStamp_ = previousTimeStamp_ = currentTimeStamp_;
}

//_________________________________________________________ printHead

void MvStopWatch::printHead(const char* string)
{
    std::cout << "["
              << explanationString_.c_str()
              << "\\"
              << string
              << "] ";
}
//_________________________________________________________ printTimes

void MvStopWatch::printTimes(struct tms& beginTimeStamp, struct timeval& beginClockStamp)
{
    static double sClockTicks = sysconf(_SC_CLK_TCK);

    times(&currentTimeStamp_);
    gettimeofday(&currentClockStamp_, nullptr);

    auto userTicks = (double)(currentTimeStamp_.tms_utime - beginTimeStamp.tms_utime);
    auto sysTicks = (double)(currentTimeStamp_.tms_stime - beginTimeStamp.tms_stime);

    std::cout << userTicks / sClockTicks
              << "u/"
              << sysTicks / sClockTicks
              << "s CPU ["
              << (currentClockStamp_.tv_sec - beginClockStamp.tv_sec)
              << " sec wall clock]"
              << std::endl;
    times(&currentTimeStamp_);
}
//_________________________________________________________ printDateTime

void MvStopWatch::printDateTime()
{
    time_t t = 0;
    time(&t);
    std::cout << ctime(&t);  //-- ctime ends with a '\n'!!!
}

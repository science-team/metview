/***************************** LICENSE START ***********************************

 Copyright 2022 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <sstream>
#include <vector>

#include "MvFwd.h"

class MvAbstractApplication;

class MvLog
{
public:
    MvLog();
    virtual ~MvLog();
    MvLog(const MvLog&) = delete;
    MvLog& operator=(const MvLog&) = delete;

    MvLog& popup();
    std::ostringstream& info();
    std::ostringstream& err();
    std::ostringstream& errNoExit();
    std::ostringstream& warn();
    std::ostringstream& dbg();
    static const std::string& detailsBegin();
    static const std::string& detailsEnd();

    static void registerApp(MvAbstractApplication*);
    static std::string formatFuncInfo(const std::string&);
    static std::string formatFuncInfoBasic(const std::string&);

private:
    void output(const std::string&);
    void parseDetails(const std::string& ori, std::string& msg, std::string& details);

    MvLogLevel level_{MvLogLevel::INFO};
    bool popup_{false};
    bool exitOnError_{true};
    std::ostringstream os_;
    static MvAbstractApplication* app_;
    static std::string detailsBeginId_;
    static std::string detailsEndId_;
};

// Overload ostringstream for various objects
template <typename T>
std::ostream& operator<<(std::ostream& stream, const std::vector<T>& vec)
{
    stream << "[";
    for (std::size_t i = 0; i < vec.size(); i++) {
        if (i > 0)
            stream << ",";
        stream << vec[i];
    }
    stream << "]";

    return stream;
}

#if defined(__GNUC__) || defined(__clang__)
#define MV_FN_INFO MvLog::formatFuncInfo(__PRETTY_FUNCTION__)
#else
#define MV_FN_INFO MvLog::formatFuncInfoBasic(__func__)
#endif

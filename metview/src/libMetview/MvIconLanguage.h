/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Path.h"
#include "Request.h"

class MvIconClassCore;
class MvIconParameter;

class MvIconLanguageScanner
{
public:
    virtual ~MvIconLanguageScanner() {}
    virtual void next(const MvIconParameter&) = 0;
};


class MvIconLanguage
{
public:
    MvIconLanguage(const MvIconClassCore*);
    MvIconLanguage(const MvIconClassCore*, const Request&);
    ~MvIconLanguage();  // Change to virtual if base class

    const char* getInfo() const;
    const char* getKind() const;

    Request expand(const Request&);
    Request expand(const Request&, long, bool = true);
    void scan(MvIconLanguageScanner&);
    std::vector<std::string> interfaces(const char*);
    Request interfaceRequest(const char*);

    static MvIconLanguage& find(const MvIconClassCore*);

private:
    // No copy allowed
    MvIconLanguage(const MvIconLanguage&);
    MvIconLanguage& operator=(const MvIconLanguage&);

    void init();

    bool inited_;
    request* lang_;
    rule* rule_;
    long flags_;
    const MvIconClassCore* class_;
    std::vector<MvIconParameter*> params_;
};

inline void destroy(MvIconLanguage**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(MvIconLanguage);
//#endif

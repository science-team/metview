/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

#include "MvList.hpp"
#include "MvDate.h"

//! \enum eTsReqFilter Timeseries request filtering levels
enum eTsReqFilter  //-- timeseries request filter
{
    eTsReqsAll /**< -  header + VAxis + HAxis + Curve + PGraph */
    ,
    eTsReqsCurveAndGraph /**< -  HAxis + Curve + PGraph */
    ,
    eTsReqsCurveOnly /**< -  HAxis + Curve */
};

//! Class to create a time series as a request
/*! Currently used by \c src/Metgram/TimeSerie.cc and \c src/Metgram/Scores.cc
 */
class MvTimeSerie : public MvElement
{
    static MvRequest empty;

protected:
    MvRequest Header;
    MvRequest HAxis;
    MvRequest VAxis;
    MvRequest PGraph;
    MvRequest Data;

    MvDate BaseDate;
    MvDate Xmin;
    MvDate Xmax;
    double Ymin;
    double Ymax;
    int Offset{0};

    eTsReqFilter reqOutFilterLevel;  //-- for Timeserie app

    int NbPoints{0};
    int user_axis{0};

    void init();


public:
    MvTimeSerie(MvRequest& vaxis = MvTimeSerie::empty,
                MvRequest& graph = MvTimeSerie::empty);
    ~MvTimeSerie();

    void createNewGroup(const Cached& refId, const char* legend = nullptr);
    void addIconInfo(const Cached& refId, const Cached& param, MvRequest& req);
    void addPoint(const Cached& refId, double x, double y);
    void setDate(int date, int hour);
    void setDate(const MvDate& date);
    void setTitle(const Cached& refId, const char* fmt, ...);
    void setReqFilter(eTsReqFilter lvl) { reqOutFilterLevel = lvl; }
    virtual MvRequest getRequest();
};

//-- MVTIMESERIE_H

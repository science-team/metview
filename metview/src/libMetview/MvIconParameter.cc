/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvIconParameter.h"

#include <ctype.h>
#include <iostream>

#include <mars.h>

#include "Request.h"

#include "MvIconLanguage.h"

//======================================================================
class GuessInterface : public ParameterScanner
{
    std::map<std::string, int> choices_;

    void next(const MvIconParameter&, const char* name, const char*);

public:
    GuessInterface(MvIconParameter&);
    std::string choice();
};

GuessInterface::GuessInterface(MvIconParameter& /*p*/)
{
    choices_["on_off"] = 4;
    choices_["menu"] = 3;    // combobox
    choices_["string"] = 2;  // textedit line
    choices_["text"] = 1;    // textedit line with lists
}

void GuessInterface::next(const MvIconParameter&, const char* name, const char*)
{
    if (*name == '/' ||
        *name == '@' ||
        *name == '"' ||
        *name == '*' ||
        *name == '\'')
        choices_["menu"] = 0;

    if (*name == '/')
        choices_["string"] = 0;

    if (*name != 'O' || (strcmp(name, "ON") != 0 && strcmp(name, "OFF") != 0))
        choices_["on_off"] = 0;
}

std::string GuessInterface::choice()
{
    int max = 0;
    std::string result = "text";

    for (auto& choice : choices_) {
        if (choice.second > max) {
            max = choice.second;
            result = choice.first;
        }
    }

    return result;
}

//======================================================================

MvIconParameter::MvIconParameter(MvIconLanguage& l, parameter* p) :
    lang_(l),
    param_(p),
    multiple_(false)
{
//    std::cout << "param:" << param_->name << std::endl;

    if (param_->interface == 0)
        param_->interface = empty_request(0);

    if (get_value(param_->interface, "interface", 0) == 0) {
        // Try to guess

        GuessInterface guess(*this);
        scan(guess);
        std::string choice = guess.choice();

        set_value(param_->interface, "interface", choice.c_str());
        if (get_value(param_->interface, "help", 0) == 0)
            set_value(param_->interface, "help", choice.c_str());
    }
    else if (strcmp(get_value(param_->interface, "interface", 0), "colour") == 0) {
        // We need to see if it is a list
        GuessInterface guess(*this);
        scan(guess);
        std::string choice = guess.choice();

        // If it can hold a list it is a colourlist
        if (choice == "text") {
            set_value(param_->interface, "interface", "colourlist");
            if (get_value(param_->interface, "help", 0) == 0 ||
                strcmp(get_value(param_->interface, "help", 0), "help_colour") == 0)
                set_value(param_->interface, "help", "help_colourlist");
        }
        // otherwise a single colour
        else {
            if (get_value(param_->interface, "help", 0) == 0)
                set_value(param_->interface, "help", "help_colour");
        }
    }

    const char* h;
    if ((h = get_value(param_->interface, "hidden", 0)) != 0 && (*h == 'T' || *h == 't')) {
        set_value(param_->interface, "interface", "none");
    }

    if ((h = get_value(param_->interface, "visible", 0)) != 0 && (*h == 'f' || *h == 'F')) {
        set_value(param_->interface, "interface", "none");
    }

    // if( ( h= get_value(param_->interface,"exclusive",0)) !=0  && (*h == 'T' || *h == 't'))
    //{
    //	multiple_=false;
    // }

    h = interface();

    if (h != 0) {
        // std::cout << "   interface:" << h << std::endl;

        if (strcmp(h, "text") == 0) {
            multiple_ = true;
        }

        else if (strcmp(h, "icon") == 0) {
            const char* ev = get_value(param_->interface, "exclusive", 0);
            if (ev != 0 && (*ev == 'F' || *ev == 'f'))
                multiple_ = true;
        }
        else if (strcmp(h, "any") == 0) {
            // if 'exclusive' is set, then do not split elements into a list
            // - interface=text does this splitting; string does not
            const char* ev = get_value(param_->interface, "exclusive", 0);
            if (ev != 0 && (*ev == 'F' || *ev == 'f'))
                multiple_ = true;

            if (multiple_)
                set_value(param_->interface, "interface", "text");
            else
                set_value(param_->interface, "interface", "string");
        }
    }

    // No helper if the interface is "none"
    if (h == 0 || strcmp(h, "none") == 0) {
        set_value(param_->interface, "help", "none");
    }

    // no helper for menus (aka combo box)
    if (h && strcmp(h, "menu") == 0) {
        set_value(param_->interface, "help", "none");
    }

    const char* b = get_value(param_->interface, "beautify", 0);
    beautify_ = (b == 0) || (*b != 'f');

    name_ = beautify(param_->name);

//    std::cout << "name=" << param_->name << " interface" << param_->interface << std::endl;

    value* v = param_->default_values;
    while (v) {
        defaults_.push_back(v->name);
        v = v->next;
    }
}

MvIconParameter::~MvIconParameter() = default;

const char* MvIconParameter::name() const
{
    return param_->name;
}

void MvIconParameter::scan(ParameterScanner& s) const
{
    scan(s, param_->values);
}

void MvIconParameter::scan(ParameterScanner& s, const value* v) const
{
    while (v) {
        if (v->ref)
            scan(s, v->ref);
        else
            s.next(*this, v->name, v->other_names ? v->other_names->name : 0);
        v = v->next;
    }
}

Request MvIconParameter::interfaceRequest() const
{
    return Request(param_->interface);
}

const char* MvIconParameter::help() const
{
    return get_value(param_->interface, "help", 0);
}

const char* MvIconParameter::help_text() const
{
    return get_value(param_->interface, "help_text", 0);
}

const char* MvIconParameter::help_icon() const
{
    const char* p = get_value(param_->interface, "help_icon", 0);
    return p ? p : "";
}

const char* MvIconParameter::interface() const
{
    return get_value(param_->interface, "interface", 0);
}

const char* MvIconParameter::warning_text() const
{
    return get_value(param_->interface, "warning_text", 0);
}

const char* MvIconParameter::ribbon_interface() const
{
    return get_value(param_->interface, "ribbon_interface", 0);
}

const char* MvIconParameter::ribbon_label() const
{
    return get_value(param_->interface, "ribbon_label", 0);
}

const char* MvIconParameter::ribbon_control() const
{
    return get_value(param_->interface, "ribbon_control", 0);
}

const char* MvIconParameter::ribbon_separator() const
{
    return get_value(param_->interface, "ribbon_separator", 0);
}

const char* MvIconParameter::ribbon_size_range() const
{
    return get_value(param_->interface, "ribbon_size_range", 0);
}

bool MvIconParameter::hiddenInTemporary() const
{
    if (auto ch = get_value(param_->interface, "hidden_in_tmp", 0)) {
        return strcmp(ch, "true") == 0;
    }
    return false;
}

const std::string& MvIconParameter::beautify(const std::string& name) const
{
    auto j = beau_.find(name);
    if (j != beau_.end())
        return (*j).second;

    std::string& result = const_cast<MvIconParameter*>(this)->beau_[name];
    bool up = true;
    result = name;

    for (char& it : result) {
        char j = it;
        if (j == '_' || j == ' ') {
            up = true;
            it = ' ';
        }
        else {
            it = up ? toupper(j) : tolower(j);
            up = false;
        }
    }

    return result;
}

const std::string& MvIconParameter::beautifiedName() const
{
    return name_;
}

const std::string& MvIconParameter::beautifiedName(const std::string& name) const
{
    if (beautify_)
        return beautify(name);
    else
        return name;
}

bool MvIconParameter::multiple() const
{
    return multiple_;
}

bool MvIconParameter::hasDefaults() const
{
    return param_->default_values != 0;
}

const std::vector<std::string>& MvIconParameter::defaults() const
{
    return defaults_;
}

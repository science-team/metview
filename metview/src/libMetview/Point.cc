/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Point.hpp"

#include <algorithm>

short Point ::get(FILE* fp)
{
    if (fread((char*)&_x, sizeof(float), 1, fp) != 1)
        return 0;
    if (fread((char*)&_y, sizeof(float), 1, fp) != 1)
        return 0;
    return 1;
}


short Point ::put(FILE* fp)
{
    if (fwrite((char*)&_x, sizeof(float), 1, fp) != 1)
        return 0;
    if (fwrite((char*)&_y, sizeof(float), 1, fp) != 1)
        return 0;
    return 1;
}


Point Point ::operator=(Point* p)
{
    _x = p->_x;
    _y = p->_y;
    return *this;
}

Point Point ::operator=(const Point& p)
{
    _x = p._x;
    _y = p._y;
    return *this;
}

Point Point ::pmax(const Point& p) const
{
    return {std::max(_x, p._x), std::max(_y, p._y)};
}

Point Point ::pmin(const Point& p) const
{
    return {std::min(_x, p._x), std::min(_y, p._y)};
}

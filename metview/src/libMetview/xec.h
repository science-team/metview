/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

#include <stdio.h>
#define ARG_DEF(n) \
    Arg al[n];     \
    int ac = 0
#define SetArg(arg, val)        \
    XtSetArg(al[ac], arg, val); \
    ac++;
#define SetValues(w)        \
    XtSetValues(w, al, ac); \
    ac = 0;
#define GetValues(w)        \
    XtGetValues(w, al, ac); \
    ac = 0;

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif
/* xec_Cursor.c */
void xec_SetWatchCursor(Widget w);
void xec_ResetCursor(Widget w);

/* xec_Label.c */
void xec_SetLabel(Widget w, const char* title);
void xec_VaSetLabel(Widget w, const char* fmt, ...);

/* xec_List.c */
void xec_ClearList(Widget w);
void xec_RemoveListItem(Widget w, const char* p);
void xec_AddListMax(Widget w, int max, const char* p);
void xec_VaAddListMax(Widget w, int max, char* fmt, ...);
void xec_AddListItem(Widget w, const char* p);
void xec_VaAddListItem(Widget w, const char* fmt, ...);
void xec_SetListItems(Widget w, XmString* list, int count);
int xec_DumpList(FILE* f, const char* fmt, Widget w);
Boolean xec_ListSearch(Widget w, char* word, Boolean nocase, Boolean fromstart, Boolean wrap);
void xec_AddFontListItem(Widget list, const char* buffer, Boolean bold);

/* xec_Regexp.c */
void xec_compile(const char* w);
int xec_step(const char* p);

/* xec_Strings.c */
XmString xec_NewString(const char* s);
void xec_BuildXmStringList(XmString** list, const char* p, int* count);
void xec_FreeXmStringList(XmString* list, int count);
char* xec_GetString(XmString string);

/* xec_Text.c */
int regexp_find(char* word, char* buffer, Boolean nocase, int* from, int* to);
Boolean xec_TextSearch(Widget w, char* word, Boolean nocase, Boolean fromstart, Boolean wrap);
char* xec_GetText(Widget w, char* buf);
int xec_LoadText(Widget Text, const char* fname, Boolean include);
int xec_SaveText(Widget w, const char* fname);
int xec_DumpText(FILE* fp, Widget w);
void xec_PrintText(Widget w, const char* cmd);
void xec_ReplaceTextSelection(Widget w, const char* p, Boolean sel);

/* xec_Widget.c */
void* xec_GetUserData(Widget w);
void xec_SetUserData(Widget w, void* p);
void xec_SetColor(Widget w, Pixel p, const char* which);
void xec_ShowWidget(Widget w);
void xec_Invert(Widget w);
void xec_ManageAll(Widget w);
void xec_UnmanageAll(Widget w);
#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

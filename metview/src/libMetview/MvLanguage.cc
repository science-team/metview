/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#include "Metview.h"

//===================================================================
// MvLanguage

MvLanguage::MvLanguage(const char* lang, const char* rule, long flags)
{
    Lang = lang ? read_language_file(lang) : nullptr;
    Rule = rule ? read_check_file(rule) : nullptr;
    Flags = flags;
}

MvLanguage::~MvLanguage()
{
    free_all_requests(Lang);
    free_rule(Rule);
}

MvRequest MvLanguage::trimAll(const MvRequest& r)
{
    return MvRequest(trim_all_requests(Lang, r), false);
}

MvRequest MvLanguage::expandAll(const MvRequest& r)
{
    reset();
    expand_flags(Flags);
    return MvRequest(expand_all_requests(Lang, Rule, r), false);
}

MvRequest MvLanguage::trimOne(const MvRequest& r)
{
    MvRequest one = r.justOneRequest();
    return MvRequest(trim_all_requests(Lang, one), false);
}

MvRequest MvLanguage::expandOne(const MvRequest& r)
{
    reset();
    long oldFlags = expand_flags(Flags);
    MvRequest one = r.justOneRequest();
    request* req = expand_all_requests(Lang, Rule, one);
    expand_flags(oldFlags);  // restore the expand flags to their previous value
    return MvRequest(req, false);
}

void MvLanguage::reset()
{
    reset_language(Lang);
}

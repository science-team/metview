/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <cstdlib>
#include <fstream>
#include <map>
#include <string>
#include <vector>
#include "fstream_mars_fix.h"

#ifdef MAGICS
namespace magics
{
#else
namespace metview
{
#endif

/*! -----------------------------------------------------------------
    \class TableFormatAttributes
    Base lass to handle the different ways of specifying how a table
    file is formatted.
   ----------------------------------------------------------------- */

class TableFormatAttributes
{
public:
    TableFormatAttributes() :
        delimiter_(','), headerRow_(1), consecutiveDelimitersAsOne_(false), dataRowOffset_(0) {}

    void setPath(std::string& path) { path_ = path; }
    void setPath(const char* path) { path_ = std::string(path); }
    void setDelimiter(char delimiter) { delimiter_ = delimiter; }
    void setHeaderRow(int headerRow) { headerRow_ = headerRow; }
    void setDataRowOffset(int offset) { dataRowOffset_ = offset; }
    void setConsecutiveDelimitersAsOne(bool consecutive) { consecutiveDelimitersAsOne_ = consecutive; }
    void setUserMetaDataRows(std::vector<int>& rows) { userMetaDataRows_ = rows; }

    std::string path() { return path_; }
    int headerRow() { return headerRow_; }
    int dataRowOffset() { return dataRowOffset_; }
    std::vector<int> userMetaDataRows() { return userMetaDataRows_; }


protected:
    std::string path_;
    char delimiter_;
    int headerRow_;
    bool consecutiveDelimitersAsOne_;
    int dataRowOffset_;
    std::vector<int> userMetaDataRows_;
};


/*! -----------------------------------------------------------------
    \class TableElementDecoder
    Base class to handle the decoding of a single element in a table
    (e.g. CSV). Derived classes should handle the decoding of
    particular data types (e.g. double, string).
   ----------------------------------------------------------------- */

class TableElementDecoder
{
public:
    TableElementDecoder() :
        currentIndex_(0) {}
    virtual ~TableElementDecoder() = default;

    virtual void initialise(int numValues) = 0;
    virtual void addValue(char* value) = 0;
    // virtual void decodeElement (char *element) = 0;

protected:
    int currentIndex_;
};


/*! -----------------------------------------------------------------
    \class TableDoubleVectorElementDecoder
    Derived class to handle the decoding of numeric data from a
    table file. The data will be put into a vector of doubles.
   ----------------------------------------------------------------- */

class TableDoubleVectorElementDecoder : public TableElementDecoder
{
public:
    TableDoubleVectorElementDecoder(std::vector<double>& target, double outMiss) :
        target_(target), outputMissingIndicator_(outMiss) {}

    void initialise(int numValues) override { target_.reserve(numValues); }
    void addValue(char* value) override { target_.push_back((value[0] != '\0') ? atof(value) : outputMissingIndicator_); }

private:
    std::vector<double>& target_;
    double outputMissingIndicator_;
};


/*! -----------------------------------------------------------------
    \class TableStringVectorElementDecoder
    Derived class to handle the decoding of string data from a
    table file. The data will be put into a vector of strings.
   ----------------------------------------------------------------- */

class TableStringVectorElementDecoder : public TableElementDecoder
{
public:
    TableStringVectorElementDecoder(std::vector<std::string>& target, std::string outMiss) :
        target_(target), outputMissingIndicator_(outMiss) {}

    void initialise(int numValues) override { target_.reserve(numValues); }
    void addValue(char* value) override { target_.push_back((value[0] != '\0') ? value : outputMissingIndicator_); }

private:
    std::vector<std::string>& target_;
    std::string outputMissingIndicator_;
};


using TableElementDecoders = std::vector<TableElementDecoder*>;  // for shorthand


/*! -----------------------------------------------------------------
    \class TableReader
    Handles the reading of a table file, such as CSV.
    To use it, set its path and any other formatting attributes
    as available in the TableFormatAttributes class, and then
    for each field (column) you wish to store, supply a container
    such as a vector of numbers. Only those fields for which you
    supply a container will be read. Fields can be specified by
    index (0-based) or by name.

   ----------------------------------------------------------------- */

class TableReader : public TableFormatAttributes
{
public:
    enum eTableReaderFieldType
    {
        TABFIELD_NUMBER,
        TABFIELD_STRING
    };

    TableReader()
    {
        gotMetaData_ = false;
        errorCode_ = 0;
    }
    TableReader(std::string& path)
    {
        setPath(path);
        gotMetaData_ = false;
        errorCode_ = 0;
    }

    void setFieldContainer(int index, std::string& name, std::vector<double>& container, double outputMissingIndicator);
    void setFieldContainer(int index, std::string& name, std::vector<std::string>& container,
                           std::string outputMissingIndicator);

    // setFieldContainer (std::string &name, std::vector<double>);
    // setFieldContainer (std::string &name, std::vector<std::string>);

    bool getMetaData(std::string& errorMessage);
    bool read(std::string& errorMessage);  // read and parse the file; the supplied cointainers will be filled

    std::vector<eTableReaderFieldType>& fieldTypes();
    std::vector<std::string>& fieldNames();
    std::map<std::string, std::string>& userMetaData();
    int numRecords();

private:
    void resizeDecoders(unsigned int numNeeded);
    int nextLineTokens(char* line, size_t sizeOfLine,
                       std::vector<char*>& tokens);          // reads the next line and splits into tokens
    void splitLine(char* line, std::vector<char*>& tokens);  // splits a line into tokens based on the current settings
    void splitLineConsecutiveDelimiters(
        char* line, std::vector<char*>& tokens);                                      // splits a line into tokens based on the current settings
    bool readUserMetaData(char* line, size_t sizeOfLine, std::string& errorMessage);  // reads user meta-data into a map
    int indexOfField(std::string& name);                                              // returns the index of the field with a given name (or -1)
    void setError(std::string msg)
    {
        errorCode_ = 1;
        errorMsg_ = msg;
    }                                                                // set an error message to be used later
    void clearError() { errorCode_ = 0; }                            // clear error message and code to be used later
    void ensureHaveMetaData();                                       // loads the meta-data if not already there
    void skipLines(int linesToSkip, char* line, size_t sizeOfLine);  // skips a user-defined number of rows
    TableReader::eTableReaderFieldType guessFieldType(
        char* str);  // tries to determine whether the field is, for example, string or number

    std::vector<TableElementDecoders> decoderSets_;  // each column can have multiple decoders attached to it
    std::vector<std::string*> names_;
    std::vector<std::string> namesAll_;
    std::vector<eTableReaderFieldType> types_;
    bool gotMetaData_;
    std::ifstream f_;
    std::streampos dataStart_;
    int numRecords_;
    std::map<std::string, std::string> userMetaData_;
    int errorCode_;         // some functions will set these to be used later
    std::string errorMsg_;  // some functions will set these to be used later
};
}
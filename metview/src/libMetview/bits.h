/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "default.16.icon"
#include "default.16.mask"
#include "default.16.open"
#include "default.32.icon"
#include "default.32.mask"
#include "default.32.open"

#define LARGEBITS "big\nicon"
#define LARGEOPEN "big\nopen"
#define LARGEMASK "big\nmask"
#define SMALLBITS "small\nicon"
#define SMALLOPEN "small\nopen"
#define SMALLMASK "small\nmask"

static int defpix_width[2] = {pix_32_width, pix_16_width};
static int defpix_height[2] = {pix_32_height, pix_16_height};


static unsigned char* defpix_bits[2] = {pix_32_bits, pix_16_bits};
static unsigned char* defmsk_bits[2] = {msk_32_bits, msk_16_bits};
static unsigned char* defopn_bits[2] = {opn_32_bits, opn_16_bits};

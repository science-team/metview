/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvIconClassCore.h"

//#include "ConfigLoader.h"
#include "MvIconLanguage.h"
//#include "MvRequestUtil.hpp"
#include "MvMiscellaneous.h"

#include <algorithm>

// static std::map<std::string, const MvIconClassCore*> classes;

// All icon classed subclass from this one
// static MvIconClassCore super("*", nullptr);


MvIconClassCore::MvIconClassCore(const std::string& name, request* r) :
    // super_(sup),
    name_(name),
    request_(r)
{
    /*classes[name] = this;
    if (!super_ && this != &super)
        super_ = &super;*/
}

MvIconClassCore::~MvIconClassCore()
{
    free_all_requests(request_);
}

#if 0
void MvIconClassCore::scan(ClassScanner& s)
{
    std::map<std::string, const MvIconClassCore*>::iterator j;
    for (j = classes.begin(); j != classes.end(); ++j)
        s.next(*(*j).second);
}
#endif

const std::string& MvIconClassCore::name() const
{
    return name_;
}

#if 0
bool MvIconClassCore::isValid(const std::string& name)
{
    auto j = classes.find(name);
    return (j != classes.end());
}

const MvIconClassCore& MvIconClassCore::find(const std::string& name)
{
    auto j = classes.find(name);
    if (j != classes.end())
        return *(*j).second;

    return *(new MvIconClassCore(name, empty_request(0)));
}

void MvIconClassCore::find(const std::string& str, std::vector<const MvIconClassCore*>& res, bool onlyCreatable)
{
    for (auto it = classes.begin(); it != classes.end(); it++) {
        const MvIconClassCore* kind = it->second;

        if (onlyCreatable && kind->canBeCreated() == false)
            continue;

        std::string s;
        std::string strTerm = str;
        std::transform(strTerm.begin(), strTerm.end(), strTerm.begin(), ::tolower);

        //Default name
        s = kind->defaultName();
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);
        if (s.find(strTerm) != std::string::npos) {
            res.push_back(kind);
            continue;
        }

        //Name
        s = kind->name();
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);
        if (s.find(strTerm) != std::string::npos) {
            res.push_back(kind);
            continue;
        }

        //Type
        s = kind->type();
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);
        if (s.find(strTerm) != std::string::npos) {
            res.push_back(kind);
            continue;
        }

        //icon box
        s = kind->iconBox();
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);
        if (s.find(strTerm) != std::string::npos) {
            res.push_back(kind);
            continue;
        }
    }
}
#endif

#if 0
void MvIconClassCore::load(request* r)
{
    new MvIconClassCore(get_value(r, "class", 0), r);
}
#endif

std::string MvIconClassCore::editor() const
{
    const char* e = get_value(request_, "editor_type", 0);
    return e ? e : "NoEditor";
}

std::string MvIconClassCore::defaultName() const
{
    const char* def = get_value(request_, "default_name", 0);
    return def ? std::string(def) : name_;
}

std::string MvIconClassCore::iconBox() const
{
    const char* def = get_value(request_, "icon_box", 0);
    return def ? std::string(def) : std::string();
}

std::string MvIconClassCore::helpPage() const
{
    const char* def = get_value(request_, "help_page", 0);
    return def ? std::string(def) : name_;
}

std::string MvIconClassCore::doubleClickMethod() const
{
    const char* def = get_value(request_, "doubleclick_method", 0);
    return def ? std::string(def) : std::string();
}

std::string MvIconClassCore::defaultMethod() const
{
    const char* def = get_value(request_, "default_method", 0);
    return def ? std::string(def) : "edit";
}

std::string MvIconClassCore::macroFunction() const
{
    const char* def = get_value(request_, "macro", 0);
    return def ? std::string(def) : std::string();
}

bool MvIconClassCore::skipDepandancies(const std::string& action) const
{
    const char* def = get_value(request_, "skip_dependancies", 0);
    return def ? (strcmp(def, action.c_str()) == 0) : false;
}

std::string MvIconClassCore::pixmap() const
{
    const char* def = get_value(request_, "pixmap", 0);
    if (def) {
        return {def};
    }
    return {};

    //        Path p(def);
    //        std::string fname          = p.name();
    //        std::string::size_type pos = fname.rfind(".icon");
    //        if (pos != std::string::npos) {
    //            fname = fname.substr(0, pos);
    //        }

    //        Path psvg(metview::iconDirFile(fname + ".svg"));
    //        if (psvg.exists()) {
    //            return psvg;
    //        }
    //        else {
    //            Path pxpm(metview::iconDirFile(fname + ".xpm"));
    //            if (pxpm.exists()) {
    //                return pxpm;
    //            }
    //        }

    //        return Path("");
    //    }

    //    return Path("");
}

bool MvIconClassCore::canBeCreated() const
{
    const char* def = get_value(request_, "can_be_created", 0);
    if (def) {
        return (strcmp(def, "True") == 0 || strcmp(def, "true") == 0) ? true : false;
    }

    return false;
}

bool MvIconClassCore::canHaveLog() const
{
    const char* def = get_value(request_, "can_have_log", 0);
    if (def) {
        return (strcmp(def, "True") == 0 || strcmp(def, "true") == 0) ? true : false;
    }

    return true;
}

std::string MvIconClassCore::type() const
{
    const char* def = get_value(request_, "type", 0);
    return def ? std::string(def) : name_;
}

Path MvIconClassCore::definitionFile() const
{
    const char* def = get_value(request_, "definition_file", 0);
    if (def == 0)
        def = "/dev/null";
    return Path(def);
}

Path MvIconClassCore::rulesFile() const
{
    const char* def = get_value(request_, "rules_file", 0);
    if (def == 0)
        def = "/dev/null";
    return Path(def);
}

long MvIconClassCore::expandFlags() const
{
    const char* def = get_value(request_, "expand", 0);
    return def ? atol(def) : EXPAND_MARS;
}

bool MvIconClassCore::isObsolete() const
{
    const char* def = get_value(request_, "obsolete", 0);
    if (def) {
        return (strcmp(def, "True") == 0 || strcmp(def, "true") == 0) ? true : false;
    }

    return false;
}

bool MvIconClassCore::isFamilyMember() const
{
    const char* def = get_value(request_, "family_member", 0);
    if (def) {
        return (strcmp(def, "True") == 0 || strcmp(def, "true") == 0) ? true : false;
    }

    return false;
}

MvIconLanguage& MvIconClassCore::language() const
{
    return MvIconLanguage::find(this);
}

// static SimpleLoader<MvIconClassCore> loadClasses("object", 0);

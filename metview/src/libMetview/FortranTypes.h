/***************************** LICENSE START ***********************************

 Copyright 2017 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

// This definition was taken from mars.h
// We only need this when mars.h cannot be used (e.g. inW codes_ui)
#ifndef METVIEW

#if !defined(I32) && !defined(I64) && !defined(I128)
#define I32
#endif

#if !defined(R32) && !defined(R64) && !defined(R128)
#define R32
#endif

#define INT_MISSING_VALUE INT_MAX
#ifdef hpux
#define FLOAT_MISSING_VALUE 3.40282346638528859812E+38F
#else
#define FLOAT_MISSING_VALUE FLT_MAX
#endif


#ifdef I32
typedef int fortint; /* fortran integer */
#endif

#ifdef I64
typedef long long fortint;
#endif

#ifdef R32
typedef float fortfloat; /* fortran single precision float */
#define PFLOAT "f"
#endif

#ifdef R64
typedef double fortfloat;
#define FLOAT "lf"
#endif

#endif

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once
#include <MvApplication.h>
#include <MvProtocol.h>
#include <MvTask.h>
#include <MvRequest.h>

//! Utility class used by MvServiceTask
class MvServiceReply : public MvReply
{
    friend class MvServiceTask;
    virtual void callback(MvRequest&);
    // Make getMessage visible for MvServiceTask
    const char* getMessage(int n) { return MvReply::getMessage(n); }
};

//! Class to send requests to other Metview modules
/*! Currently used by PlotMod (and uPlot).
 *  For a simple example, see file \c src/libMetview/Demo.cc
 */
class MvServiceTask : public MvTask
{
    friend class MvServiceReply;

    Cached Service;
    MvRequest Request;
    MvRequest Reply;

    static MvServiceReply* ReplyHandler;

    void gotReply(const MvRequest&, err);

public:
    //! Constructor
    /*! Here \c req is the request that will be sent to
     *  the module that provides service \c service.
     */
    MvServiceTask(MvClient* client, const Cached& service, const MvRequest& req, const char* taskName = 0);

    //! Sends the given request to the given service
    /*! Service and the request are set in the constructor
     */
    virtual void run(void);

    MvRequest run(int&);

    //! Returns the reply request from the other Metview module
    MvRequest& getReply() { return Reply; }
};

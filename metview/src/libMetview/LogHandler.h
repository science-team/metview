/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <iostream>
#include <map>
#include <sstream>
#include <vector>

class LogHandlerObserver;
class LogItemObserver;

namespace metview
{
enum Type
{
    ErrorType,
    WarningType,
    TaskType,
    InfoType
};
enum LogKey
{
    CommandKey,
    RequestKey,
    MethodKey
};
}  // namespace metview

template <typename OutputPolicy>
class BaseLog
{
public:
    BaseLog();
    virtual ~BaseLog();

    std::ostringstream& task();
    std::ostringstream& error();
    std::ostringstream& msg();

    static const std::string& keyToStr(metview::LogKey);
    static const std::string& commandKey();
    static const std::string& methodKey();
    static const std::string& requestKey();

protected:
    metview::Type type_;
    std::ostringstream os_;

    static std::map<metview::LogKey, std::string> logKeys_;

private:
    BaseLog(const BaseLog&);
    BaseLog& operator=(const BaseLog&);
};

template <typename OutputPolicy>
std::map<metview::LogKey, std::string> BaseLog<OutputPolicy>::logKeys_;

template <typename OutputPolicy>
BaseLog<OutputPolicy>::BaseLog() = default;

template <typename OutputPolicy>
const std::string& BaseLog<OutputPolicy>::keyToStr(metview::LogKey key)
{
    if (logKeys_.empty()) {
        logKeys_[metview::CommandKey] = "--CMD";
        logKeys_[metview::MethodKey] = "--MTD";
        logKeys_[metview::RequestKey] = "--REQ";
    }

    auto it = logKeys_.find(key);
    if (it != logKeys_.end())
        return it->second;

    static std::string emptyStr("");
    return emptyStr;
}

template <typename OutputPolicy>
const std::string& BaseLog<OutputPolicy>::commandKey()
{
    return keyToStr(metview::CommandKey);
}

template <typename OutputPolicy>
const std::string& BaseLog<OutputPolicy>::requestKey()
{
    return keyToStr(metview::RequestKey);
}

template <typename OutputPolicy>
const std::string& BaseLog<OutputPolicy>::methodKey()
{
    return keyToStr(metview::MethodKey);
}


template <typename OutputPolicy>
std::ostringstream& BaseLog<OutputPolicy>::task()
{
    type_ = metview::TaskType;
    return os_;
}

template <typename OutputPolicy>
std::ostringstream& BaseLog<OutputPolicy>::msg()
{
    type_ = metview::InfoType;
    return os_;
}

template <typename OutputPolicy>
std::ostringstream& BaseLog<OutputPolicy>::error()
{
    type_ = metview::ErrorType;
    return os_;
}

template <typename OutputPolicy>
BaseLog<OutputPolicy>::~BaseLog()
{
    OutputPolicy::output(os_.str(), type_);
}

class LogObserver
{
public:
    virtual void update(const std::string&, metview::Type) = 0;
};

class OutputToGui
{
public:
    static void output(const std::string& msg, metview::Type type);
    static void setObserver(LogObserver* o) { observer_ = o; }

protected:
    static LogObserver* observer_;
};

using GuiLog = BaseLog<OutputToGui>;

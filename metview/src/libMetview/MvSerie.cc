/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <Metview.h>
#include <stdarg.h>
#include <math.h>


MvSerie::MvSerie() :
    Header("MAGICS_PARAMETERS"),
    Data("CURVE"),
    HAxis("PAXIS"),
    VAxis("PAXIS"),
    Magics("PGRAPH")
{
    init();
}

MvSerie::MvSerie(char* t) :
    Header("MAGICS_PARAMETERS"),
    Data(t),
    HAxis("PAXIS"),
    VAxis("PAXIS"),
    Magics("PGRAPH")
{
    init();
}

void MvSerie::init()
{
    Xmin = Ymin = 1e300;
    Xmax = Ymax = -1e300;
    NbPoints = 0;

    HAxis("AXIS_ORIENTATION") = "HORIZONTAL";
    HAxis("AXIS_TICK_INTERVAL") = 1.;
    HAxis("AXIS_GRID") = "ON";
    HAxis("AXIS_GRID_COLOUR") = "BLACK";

    VAxis("AXIS_ORIENTATION") = "VERTICAL";

    Data("NB_POINTS") = NbPoints;

    Magics("GRAPH_TYPE") = "CURVE";
}
MvRequest MvSerie::getRequest()
{
    return Header + VAxis + HAxis + Data + Magics;
}

void MvSerie::setYMinMax(double y)
{
    if (y > Ymax) {
        Ymax = y;
        VAxis("AXIS_MAX_VALUE") = Ymax;
    }
    if (y < Ymin) {
        Ymin = y;
        VAxis("AXIS_MIN_VALUE") = Ymin;
    }
}
void MvSerie::setXMinMax(double x)
{
    if (x > Xmax) {
        Xmax = x;
        HAxis("AXIS_MAX_VALUE") = Xmax;
    }
    if (x < Xmin) {
        Xmin = x;
        HAxis("AXIS_MIN_VALUE") = Xmin;
    }
}
void MvSerie::setYAutoScale(int nticks)
{
    double interval = (Ymax - Ymin) / double(nticks);
    int exponent = (int)log10(interval);

    if (log10(interval) < 0.)
        exponent -= 1;

    if (interval / pow(10., (double)exponent) < 1.5)
        interval = pow(10., (double)exponent);
    else if (interval / pow(10., (double)exponent) < 3.5)
        interval = 2. * pow(10., (double)exponent);
    else if (interval / pow(10., (double)exponent) < 7.5)
        interval = 5. * pow(10., (double)exponent);
    else
        interval = 10. * pow(10., (double)exponent);

    double scale = 1.0;
    if (exponent < -1 || exponent > 2) {
        scale = pow(10., (double)exponent);
        MvRequest rtemp(Data);
        Data.unsetParam("Y");
        double val;
        for (int i = 0; i < NbPoints; i++) {
            rtemp.getValue(val, "Y", i);
            if (i)
                Data("Y") += val / scale;
            else
                Data("Y") = val / scale;
        }
        char text[132];
        sprintf(text, "x%g", scale);
        VAxis("AXIS_TIP_TITLE") = "ON";
        VAxis("AXIS_TIP_TITLE_QUALITY") = "LOW";
        VAxis("AXIS_TIP_TITLE_TEXT") = text;
        VAxis("AXIS_MAX_VALUE") = Ymax / scale;
        VAxis("AXIS_MIN_VALUE") = Ymin / scale;
        interval /= scale;
    }

    VAxis("AXIS_TICK_INTERVAL") = interval;
    VAxis("AXIS_GRID_LINE_STYLE") = "DOT";
}

void MvSerie::setXAutoScale(int nticks)
{
    double interval = (Xmax - Xmin) / double(nticks);
    int exponent = (int)log10(interval);

    if (log10(interval) < 0.)
        exponent -= 1;

    if (interval / pow(10., (double)exponent) < 1.5)
        interval = pow(10., (double)exponent);
    else if (interval / pow(10., (double)exponent) < 3.5)
        interval = 2. * pow(10., (double)exponent);
    else if (interval / pow(10., (double)exponent) < 7.5)
        interval = 5. * pow(10., (double)exponent);
    else
        interval = 10. * pow(10., (double)exponent);

    double scale = 1.0;
    if (exponent < -1 || exponent > 2) {
        scale = pow(10., (double)exponent + 1.);
        MvRequest rtemp(Data);
        Data.unsetParam("X");
        double val;
        for (int i = 0; i < NbPoints; i++) {
            rtemp.getValue(val, "X", i);
            if (i)
                Data("X") += val / scale;
            else
                Data("X") = val / scale;
        }
        char text[132];
        sprintf(text, "x%g", scale);
        HAxis("AXIS_TIP_TITLE") = "ON";
        HAxis("AXIS_TIP_TITLE_QUALITY") = "LOW";
        HAxis("AXIS_TIP_TITLE_TEXT") = text;
        HAxis("AXIS_MAX_VALUE") = Xmax / scale;
        HAxis("AXIS_MIN_VALUE") = Xmin / scale;
        interval /= scale;
    }

    HAxis("AXIS_TICK_INTERVAL") = interval;
    HAxis("AXIS_GRID_LINE_STYLE") = "DOT";
}

void MvSerie::setYPressure()
{
    int yint = (int)(ABS(Ymax - Ymin) / 10.);
    if (yint <= 15)
        yint = 10;
    else if (yint <= 30)
        yint = 20;
    else if (yint <= 60)
        yint = 50;
    else
        yint = 100;

    VAxis("AXIS_GRID") = "ON";
    VAxis("AXIS_GRID_COLOUR") = "BLACK";
    VAxis("AXIS_MIN_VALUE") = Ymax;
    VAxis("AXIS_MAX_VALUE") = Ymin;
    VAxis("AXIS_TITLE_QUALITY") = "LOW";
    VAxis("AXIS_TITLE_HEIGHT") = .8;
    VAxis("AXIS_TICK_INTERVAL") = yint;
    VAxis("AXIS_TICK_LABEL_HEIGHT") = .4;
    VAxis("AXIS_GRID_LINE_STYLE") = "DOT";
    VAxis("AXIS_TICK_LABEL_QUALITY") = "LOW";
    VAxis("AXIS_TIP_TITLE_QUALITY") = "LOW";
    VAxis("AXIS_TIP_TITLE") = "ON";
    VAxis("AXIS_TICK_LABEL_TYPE") = "NUMBER";
    VAxis("AXIS_TITLE_TEXT") = "Pressure";
    VAxis("AXIS_TIP_TITLE_TEXT") = "hPa";
}

void MvSerie::setYModel()
{
    int yint = (int)(ABS(Ymax - Ymin) / 10.);
    if (yint <= 2)
        yint = 1;
    else
        yint = 5;
    VAxis("AXIS_GRID") = "ON";
    VAxis("AXIS_GRID_COLOUR") = "BLACK";
    VAxis("AXIS_MIN_VALUE") = Ymax;
    VAxis("AXIS_MAX_VALUE") = Ymin;
    VAxis("AXIS_TITLE_QUALITY") = "LOW";
    VAxis("AXIS_TITLE_HEIGHT") = .8;
    VAxis("AXIS_TICK_INTERVAL") = yint;
    VAxis("AXIS_TICK_LABEL_QUALITY") = "LOW";
    VAxis("AXIS_TICK_LABEL_HEIGHT") = .4;
    VAxis("AXIS_GRID_LINE_STYLE") = "DOT";
    VAxis("AXIS_TIP_TITLE_QUALITY") = "LOW";
    VAxis("AXIS_TICK_LABEL_TYPE") = "NUMBER";
    VAxis("AXIS_TITLE_TEXT") = "Model Level";
    VAxis("AXIS_TIP_TITLE_TEXT") = "";
}

void MvSerie::addPoint(double x, double y)
{
    addX(x);
    addY(y);

    setXMinMax(x);
    setYMinMax(y);

    NbPoints++;
    Data("NB_POINTS") = NbPoints;
}

MvSerie::~MvSerie() = default;

void MvSerie::setTitle(char* fmt, ...)
{
    char buffer[1024];
    va_list list;

    va_start(list, fmt);
    vsprintf(buffer, fmt, list);
    va_end(list);

    Data("TITLE") = buffer;
}

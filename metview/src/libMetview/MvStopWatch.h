/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// rev vk 031125 ---------------- (vk Jun92) ---------------- MvStopWatch.h
//

#pragma once

#include <ctime>
#include <sys/times.h>

#include <string>

class MvStopWatch
{
public:
    MvStopWatch();
    MvStopWatch(const char* anExplanationString);
    ~MvStopWatch();

    void lapTime();
    void lapTime(const char* anExplanationString);
    void reset();
    void reset(const char* anExplanationString);

protected:
    void startTimer();
    void printHead(const char* string);
    void printTimes(struct tms& beginTimeStamp, struct timeval& beginClockStamp);
    void printDateTime();

private:
    struct tms startingTimeStamp_;
    struct tms previousTimeStamp_;
    struct tms currentTimeStamp_;

    struct timeval startingClockStamp_;
    struct timeval previousClockStamp_;
    struct timeval currentClockStamp_;

    std::string explanationString_;
};

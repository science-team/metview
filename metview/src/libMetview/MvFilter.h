/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

//! Define request filtering conditions
/*! MvFilter defines filtering conditions on a general MvRequest.
 *  For instance, MvFilter can be used with MvFieldSetIterator
 *  to access - from a MvFieldSet - only those MvField objects
 *  that match the given conditions.\n \n
 *  This class encapsulates libMars structure \c condition, found in
 *  files \c lang.h and \c check.c in directory src/libMars.\n \n
 *  Example:
 * <PRE>
 *     MvFilter date ("DATE");
 *     MvFilter param ("PARAM");
 *
 *     MvFilter clause = param==127 && date<20080101 && date>=20070101;
 * </PRE>
 */
class MvFilter
{
    condition* Condition;

    // private

    void _init(const char*);
    MvFilter _newop(testop, char*);
    MvFilter _newop(testop, double);
    MvFilter _newop(testop, int);

    MvFilter(char*, testop, char*);

public:
    MvFilter(const MvFilter&);  //-- originally private

    //! Constructor, defines the parameter that will be used in a filtering condition
    MvFilter(const char* param);

    //! Constructor, may take an external condition to build the MvFilter or builds an empty MvFilter
    MvFilter(condition* cond = nullptr);

    //! Destructor
    ~MvFilter();

    // Copy
    //! Operator to copy one MvFilter to another in an assignment operation
    MvFilter& operator=(const MvFilter&);

    //  Convertors
    //! Operator that returns the pointer to the internal condition structure
    /*! It may be used to check if a condition is defined.
     */
    operator condition*() { return Condition; }

    //  Utilities
    //! Tests \c req against the this filter
    int operator()(const MvRequest& req);  // Test MvRequest against this

    //! Equality test for a string
    /*! These three operators build atomic comparison conditions
     *  that do an equality test between the parameter defined
     *  in the constructor and the argument of the operator
     */
    MvFilter operator==(char* str);

    //! Equality test for a double
    MvFilter operator==(double val);

    //! Equality test for an int
    MvFilter operator==(int val);

    //! Non-equality test for a string
    /*! These three operators build atomic comparison conditions
     *  that do a non-equality test between the parameter defined
     *  in the constructor and the argument of the operator
     */
    MvFilter operator!=(char* str);

    //! Non-equality test for a double
    MvFilter operator!=(double val);

    //! Non-equality test for an int
    MvFilter operator!=(int val);

    //! Less-or-equal test for a string
    /*! These three operators build atomic comparison conditions
     *  that do a less-or-equal test between the parameter defined
     *  in the constructor and the argument of the operator
     */
    MvFilter operator<=(char* str);

    //! Less-or-equal test for a double
    MvFilter operator<=(double val);

    //! Less-or-equal test for an int
    MvFilter operator<=(int val);

    //! Greater-or-equal test for a string
    /*! These three operators build atomic comparison conditions
     *  that do a greater-or-equal test between the parameter defined
     *  in the constructor and the argument of the operator
     */
    MvFilter operator>=(char* str);

    //! Greater-or-equal test for a double
    MvFilter operator>=(double val);

    //! Greater-or-equal test for an int
    MvFilter operator>=(int val);

    //! Greater-than test for a string
    /*! These three operators build atomic comparison conditions
     *  that do a greater-than test between the parameter defined
     *  in the constructor and the argument of the operator
     */
    MvFilter operator>(char* str);

    //! Greater-than test for a double
    MvFilter operator>(double val);

    //! Greater-than test for an int
    MvFilter operator>(int val);

    //! Less-than test for a string
    /*! These three operators build atomic comparison conditions
     *  that do a less-than test between the parameter defined
     *  in the constructor and the argument of the operator
     */
    MvFilter operator<(char* str);

    //! Less-than test for a double
    MvFilter operator<(double val);

    //! Less-than test for an int
    MvFilter operator<(int val);

    //! Operator that performs the logical \c AND operation on two MvFilter and returns the compound MvFilter
    MvFilter operator&&(const MvFilter& f);

    //! Operator that performs the logical \c OR operation on two MvFilter and returns the compound MvFilter
    MvFilter operator||(const MvFilter& f);

    //! Operator that negates the condition in a MvFilter and returns the new condition
    MvFilter operator!();
};

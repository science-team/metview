//  MvParTable
//
// .AUTHOR:
//  Lubia Vinhas
//
// .SUMMARY:
//  Utilities used in PlotMod (wind plot) for
//  accessing the parameter tables in METVIEW_DIR/sys
//
//
#pragma once
#include <stdio.h>
#include <Cached.h>
#include <MvPath.h>

Cached CompanionWindField(int parTableNumer, int parNumber);

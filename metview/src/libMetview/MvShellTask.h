/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once
#include <MvApplication.h>
#include <MvTask.h>

//! Class to execute shell commands and scripts
class MvShellTask : public MvTask
{
    static boolean _inputCB(FILE*, void*);

    Cached Command;

    virtual void input(char*);  // Called for each lines

public:
    //! Constructor
    MvShellTask(MvClient*, const char* command, const char* taskName = 0);

    //! Method to run a shell task
    virtual void run(void);
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

//*****************************************************************
//  Application Gaia
//
//  Access GRIB/BUFR/NETCDF data from the Navy's storage directories
//*****************************************************************

#include <Metview.h>

class Gaia : public MvService
{
public:
    Gaia(char* name) :
        MvService(name) {}

    void serve(MvRequest&, MvRequest&);

    // Get some input arguments
    bool get_common_input_info(MvRequest&);

    // Convert integer numbers to strings with '0's on the left
    void left_pad_string(const std::vector<int>&, int, std::vector<std::string>&);

    // Compute data's minimum date to be stored in the main repository
    long get_minimum_date_mr();

    // Build the paths to Gaia's data repositories
    void build_path(MvRequest&);

    // Add full path to all filenames
    void add_full_path(MvRequest&, std::vector<std::string>&);

    // Build filenames
    bool build_filenames(MvRequest&, std::vector<std::string>&);
    bool build_filenames_model(MvRequest&, std::vector<std::string>&);
    bool build_filenames_observation(MvRequest&, std::vector<std::string>&);
    bool build_filenames_ww3(MvRequest&, std::vector<std::string>&, std::vector<std::string>&);
    bool build_filenames_hycom(MvRequest&, std::vector<std::string>&);
    bool build_filenames_icon13km(std::vector<std::string>&, std::vector<std::string>&, std::string&,
                              std::vector<std::string>&);
    //F bool build_filenames_icon_single(const std::vector<std::string>&, std::vector<std::string>&);
    bool build_filenames_cosmo(std::string&, std::vector<std::string>&, std::vector<std::string>&,
                               std::vector<std::string>&);
    bool build_filenames_iconlam(std::string&, std::vector<std::string>&, std::string&, 
		    		std::vector<std::string>&, std::vector<std::string>&);
    bool build_filenames_wrf(const std::string&, const std::string&,
                             const std::vector<std::string>&, const std::vector<std::string>&,
                             std::vector<std::string>&);
    bool build_filenames_adcirc(MvRequest&, std::vector<std::string>&);
    bool build_filenames_gfs(std::vector<std::string>&, std::vector<std::string>&,
                             std::vector<std::string>&);

    bool get_filenames(const std::string&, const std::string&, const std::string&,
                       std::vector<std::string>&);
    bool get_filenames(const std::string&, const std::string&, const std::string&,
                       const std::string&, const long, const long, std::vector<std::string>&);

    // Build output data file
    std::string bufr_obs(long, long, const std::vector<std::string>&);
    std::string netcdf_hycom_model(const std::vector<std::string>&);
    std::string grib_icon13km_model(const std::vector<std::string>&);
    //F std::string grib_icon_single_model(const std::vector<std::string>&);
    //F29 std::string grib_cosmo_wrf_gfs_model(const std::vector<std::string>&);
    std::string grib_get_messages(const std::vector<std::string>&);  //F29

    // Add messages to a grib file
    // bool add_messages(const std::string&, const std::string&);
    bool add_messages_tpl(const std::string&, const std::string&);

    // Split string according to a single caracter
    std::vector<std::string> split(const std::string&, char);

    // Pos-process data: regrid and/or crop area
    bool pos_processing_data(const MvRequest&, std::string&);

private:
    void date_plus_ndays(struct tm*, int);

    bool expect_{FALSE};
    bool bignore_level_type_{FALSE};  // by default level_type is to be tested //F29
    std::vector<std::string> dates_;
    std::vector<std::string> sparams_;
    std::vector<std::string> directory_;
    std::vector<int> ilevels_;
    std::vector<int> times_;
    std::vector<int> isteps_;
    std::string spath_;
    std::string spath_s_;
    std::string slevel_type_;
    long min_date_;
    // float resX_{0.};
    // float resY_{0.};
};

#
#          GAIA EDITOR: definition file
#
GAIA
{
  TYPE
  {
    MODEL       ; MODEL
    OBSERVATION ; OBSERVATION
    GEONETCAST  ; GEONETCAST
  } = MODEL

  DATA
  {
    GOES-16
    SEA ICE
  } = GOES-16

  OBSTYPE 
  {
    SYNOP     ; SYNOP
    ASCAT     ; ASCAT
  } = ASCAT

  MODEL
  {
    COSMO      ; COSMO
    COSMO_ANT  ; COSMO_ANT
    ICONLAM SAM6.5KM  ; ICONLAM_SAM65KM
    ICONLAM ANT6.5KM  ; ICONLAM_ANT65KM
    ICONLAM SSE2.1KM  ; ICONLAM_SSE21KM
    ICON_13KM  ; ICON_13KM
    WRF        ; WRF
    GFS        ; GFS
    ECMWF      ; ECMWF
    WW3_ICON   ; WW3_ICON
    WW3_ICLM   ; WW3_ICLM
    WW3_GFS    ; WW3_GFS
    HYCOM_112  ; HYCOM_112
    HYCOM_124  ; HYCOM_124
    ADCIRC     ; ADCIRC
  } = ICONLAM SAM6.5KM


  LEVEL_TYPE
  {
    SURFACE        ; sfc
    PRESSURE_LEVEL ; pl
    MODEL_LEVEL    ; ml
  } = PRESSURE_LEVEL

  LEVEL_LIST
  [ help = help_script, help_script_command = 'echo "Valid formats are: Single level as n ; All levels as ALL ; List of levels as n1/n2/... or n1/TO/n2 or n1/TO/n2/BY/number_of_levels"' ]
  {
    TO ; TO
    BY ; BY
    OFF ; OFF
    ALL ; ALL
    *
    /
  } = ALL

  PARAMETER #[ help = help_multiple_selection, exclusive = False ]
   [ help = help_multiple_selection, exclusive = False ]
  #[ help = help_script, help_script_command = 'echo "Valid formats are: Single parameter as string or number ; List of parameters as param1/param2/..."' ]
  {
     #GEOPOTENTIAL                                  ;  FI       ;129
     #U COMPONENT OF WIND                           ;  U        ;131
     #V COMPONENT OF WIND                           ;  V        ;132
     #10 METRE U WIND COMPONENT                     ;  U_10M    ;165
     #10 METRE V WIND COMPONENT                     ;  V_10M    ;166
     #2 METRE TEMPERATURE                           ;  T_2M     ;167
     #2 METRE DEWPOINT TEMPERATURE                  ;  TD_2M    ;168
     #PRESSURE REDUCED TO MSL                       ;  PMSL     ;260074
     #LARGE SCALE RAIN                              ;  RAIN_GSP ;500134
     #CONVECTIVE RAIN                               ;  RAIN_CON ;500137
     #CONVECTIVE SNOWFALL WATER EQUIVALENT          ;  SNOW_CON ;500052
     #LARGE SCALE SNOWFALL                          ;  SNOW_GSP ;500053
     #MAXIMUM WIND 10M                              ;  VMAX_10M ;500164
     Albedo for diffusive radiation ; alb_rad
     Surface latent heat flux mean ; alhfl_s
     Surface net solar radiation mean ; asob_s
     TOA net solar radiation mean     ; asob_t
     Surface down solar diff. rad     ; aswdifd_s
     Surface up solar diff. rad     ; aswdifu_s
     Surface down solar direct rad     ; aswdir_s
     Conv avail pot energy     ; cape_con
     Surface layer parcel     ; cape_ml
     latitude of the midpoints of triangle circumcenters ; clat
     Cloud cover ; clc
     High level clouds ; clch
     Low level clouds ; clcl
     Mid level clouds ; clcm
     Total cloud cover ; clct
     Modified total cloud cover for media ; clct_mod
     Modified cloud depth for media ; cldepth
     longitude of the midpoints of triangle circumcenters ;clon
     latitude of the edge midpoints ; elat
     longitude of the edge midpoints ; elon
     Geopotential at cell centre ; fi
     Fraction of sea ice ; fr_ice
     Fraction lake ; fr_lake
     Fraction land ; fr_land
     Snow depth ; h_snow
     Height of convective cloud base ; hbas_con
     Geometric height at half level center ; hhl
     Topographic height at cell centers ; hsurf
     Height of convective cloud top ; htop_con
     Height of top of dry convection ; htop_dc
     Height of 0 deg C leve ; hzerocl
     Pressure at half level ; p
     Plant covering degree  ; plcov
     Mean sea level pressure ; pmsl
     Surface pressure ; ps
     Specific humidity at the surface ; qv
     Specific humidity ; qv_s
     Convective rain ; rain_con
     Gridscale rain ; rain_gsp
     Relative humidity ; relhum
     Relative humidity in 2m ; relhum_2m
     Snow density ; rho_snow
     Convective snow ; snow_con
     Gridscale snow ; snow_gsp
     Soil type ; soiltyp
     Temperature ; t
     Temperature in 2m ; t_2m
     Weighted surface temperature ; t_g
     Temperature of the snow-surface ; t_snow
     Weighted soil temperature ; t_so
     Dew-point in 2m ; td_wm
     Turbulent kinetic energy  ; tke
     Max 2m temperature ; tmax_2m
     Min 2m temperature ; tmin_2m
     Total precip ; tot_prec
     Zonal wind ; u
     Zonal wind in 10m ; u_10m
     Meridional wind  ; v
     Meridional wind in 10m ; v_10m
     Max meridional wind in 10m ; vmax_10m
     Vertical velocity ; w
     Water equivalent of snow ; w_snow
     Total water content ; w_so
     Significant weather ; ww
     Roughness length ; z0
     @
     /
  } = fi

  DATE
  [ help = help_script, help_script_command = 'echo "Valid formats are: Absolute as YYYYMMDD ; Relative as -n (i.e., 0 = today ; -1 = yesterday) ; List of dates as date1/date2/... or date1/TO/date2 or date1/TO/date2/BY/number_of_days"' ]
  {
     JANUARY      ;   JAN
     FEBRUARY     ;   FEB
     MARCH        ;   MAR
     APRIL        ;   APR
     MAY          ;   MAY
     JUNE         ;   JUN
     JULY         ;   JUL
     AUGUST       ;   AUG
     SEPTEMBER    ;   SEP
     OCTOBER      ;   OCT
     NOVEMBER     ;   NOV
     DECEMBER     ;   DEC
     *   
     CURRENT DATE ; 0
     YESTERDAY    ; -1
     TO           ; TO
     BY           ; BY
     /
     OFF
  } = CURRENT DATE

  SATELLITE_TYPE
  {
       METOPA ; metopa
       METOPB ; metopb
       METOPC ; metopc
  } = METOPC

  RESOLUTION_SAT
  [ help = help_script, help_script_command = 'echo "Coastal Winds at 12.5 or 25 km Swath Grid"' ]
  {
       12.5 ; 12.5
       25   ; 25
  } = 12.5

  TIME
  {
    00
    06
    12
    18
  } = 00

  INITIAL_TIME
  [ help = help_script, help_script_command = 'echo "Format: HHMMSS"' ]
  {
    *
  } = 000000

  FINAL_TIME
  [ help = help_script, help_script_command = 'echo "Format: HHMMSS"' ]
  {
    *
  } = 235900

  STEP
  [ help = help_script, help_script_command = 'echo "Valid formats are: Single number as n ;  All steps as ALL ; List of steps as n1/n2/... or n1/TO/n2 or n1/TO/n2/BY/number_of_steps"' ]
  {
    TO  ; TO
    BY  ; BY
    ALL ; ALL
    /
    *   
    } = 0

 # LEVELS
 # {
 #   ONE_LEVEL
 #   MULTI_LEVELS
 # } = ONE_LEVEL

  #Param_OneLevel
  #[ help = help_script, help_script_command = 'echo "Opcoes: u_velocity e v_velocity (apenas HYCOM_124) / mixed_layer_thickness / ssh"' ]
  #{
  #  u_velocity ; u_velocity
  #  v_velocity ; v_velocity
  #  mixed_layer_thickness ; mixed_layer_thickness
  #  ssh ; ssh
  #  /
  #} = u_velocity

  #Param_MultiLevel
  #[ help = help_script, help_script_command = 'echo "Opcoes: salinity / temperature / u / v / bathymetry"' ]
  #{
  #  salinity ; salinity
  #  temperature ; temperature
   # u ; u
   # v ; v
   # bathymetry ; bathymetry
   # /
  #} = salinity

  REGION_WW3
  {
    ANTARCTICA ; ant5
    METAREA5   ; met5
    GLOBAL     ; glo20
    BRCOAST    ; brcoast
  } = METAREA5

  REGION_WRF
  {
    ANTARCTICA
    METAREA5
    SPECIAL
  } = METAREA5

  REGION_ADC
  {
    BAIA_GUANABARA ; BG
    SEPETIBA       ; SIG
    SAO_SEBASTIAO  ; SSIB
  } = BAIA_GUANABARA

  RESOLUTION_WRF
  {
    5_KM
    10_KM
    20_KM
  } = 20_KM

  GRID
  [ help = help_script, help_script_command = 'echo "Format: nlat/nlon (regular lat/lon grid with nlat x nlon degree point spacing)"' ]
  {
    OFF
    /
    @
  } = OFF

  # cropping area: north/west/south/east
	AREA
		[ help         = help_input,
		  help_icon    = 'help_map',
		  help_tooltip = 'Define area on a map',
		  input_type   = map,
		  input_window = '/Metview/System/Input Window' ]
	{
		@
		/
	} = OFF #-90./0./90./360.

  EXPECT
  {
    OFF
    ON
  } = OFF
}

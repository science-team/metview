/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <string>

#include "FlextraExaminer.h"
#include "Metview.h"
#include "MvQApplication.h"
#include "MvQMethods.h"


int main(int argc, char** argv)
{
    if (argc < 2) {
        marslog(LOG_EROR, "No arguments are specified");
        exit(1);
    }

    MvRequest in;
    in.read(argv[1], false, true);

    marslog(LOG_INFO, "Request:");
    in.print();

    if (!in) {
        marslog(LOG_EROR, "No request could be read from request file=%s", argv[1]);
        exit(1);
    }

    // Get flextra file name
    std::string fPath;
    if (const char* tmpc = in("PATH")) {
        fPath = std::string(tmpc);
    }
    else {
        marslog(LOG_EROR, "No PATH is specified!");
        exit(1);
    }

    std::string iconPath;
    if (const char* iconName = in("_NAME")) {
        iconPath = MvQ::iconPixmapPath(QString(iconName)).toStdString();
    }
    //    char* mvdir = getenv("METVIEW_USER_DIRECTORY");
    //    if (mvdir) {
    //        iconPath             = std::string(mvdir);
    //        const char* iconName = in("_NAME");
    //        if (iconName) {
    //            iconPath = std::string(mvdir) + "/" + iconName;
    //        }
    //    }

    // Create the qt application. The appname must be unique!
    std::string appName = MvApplication::buildAppName("FlextraExaminer");
    MvQApplication app(argc, argv, appName.c_str(), QStringList{"examiner", "window", "find"});

    // Create the flextra browser, initialize it and show it
    auto* browser = new FlextraExaminer;
    browser->setAppIcon("FLEXTRA_FILE");
    browser->initData(fPath, iconPath);
    browser->show();

    // Enter the app loop
    app.exec();
}

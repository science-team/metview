/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QAction>
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QMenu>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QSettings>
#include <QSplitter>
#include <QStatusBar>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QVBoxLayout>

#include "FlextraExaminer.h"
#include "LogHandler.h"
#include "PlainTextWidget.h"
#include "StatusMsgHandler.h"

#include "mars.h"

#include "MvFlextra.h"
#include "MvQAbout.h"
#include "MvQFileInfoLabel.h"
#include "MvQHighlighter.h"
#include "MvQLogPanel.h"
#include "MvQMethods.h"

FlextraExaminer::FlextraExaminer(QWidget* parent) :
    MvQMainWindow(parent),
    data_(nullptr),
    actionFileInfo_(nullptr),
    actionLog_(nullptr)
{
    // Initializations
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle(tr("Metview - FLEXTRA Examiner"));

    // Initial size
    setInitialSize(1100, 800);

    setupBlockPanel();
    setupDumpPanel();

    // Main splitter
    mainSplitter_ = new QSplitter;
    mainSplitter_->setOrientation(Qt::Vertical);
    mainSplitter_->setOpaqueResize(false);
    setCentralWidget(mainSplitter_);

    // The main layout (the upper part of mainSplitter)
    auto* mainLayout = new QVBoxLayout;
    mainLayout->setObjectName(QString::fromUtf8("vboxLayout"));
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(1);

    auto* w = new QWidget;
    w->setLayout(mainLayout);
    mainSplitter_->addWidget(w);

    // File info area
    fileInfoLabel_ = new MvQFileInfoLabel;
    mainLayout->addWidget(fileInfoLabel_);

    // Central splitter
    centralSplitter_ = new QSplitter;
    centralSplitter_->setOrientation(Qt::Horizontal);
    centralSplitter_->setOpaqueResize(false);
    mainLayout->addWidget(centralSplitter_);

    centralSplitter_->addWidget(blockPanel_);
    centralSplitter_->addWidget(dumpPanel_);
    centralSplitter_->setCollapsible(1, false);
    QList<int> splitterSize;
    splitterSize << 200 << 900;
    centralSplitter_->setSizes(splitterSize);

    // Log
    logPanel_ = new MvQLogPanel(this);
    mainSplitter_->addWidget(logPanel_);

    //--------------------------
    // Status bar
    //--------------------------

    statusMessageLabel_ = new QLabel("");
    statusMessageLabel_->setFrameShape(QFrame::NoFrame);
    statusBar()->addPermanentWidget(statusMessageLabel_, 1);  // '1' means 'please stretch me when resized'

    StatusMsgHandler::instance()->init(statusMessageLabel_);

    //----------------------------
    // Setup menus and toolbars
    //----------------------------

    // Initialize user interface actions
    setupViewActions();
    setupHelpActions();

    setupMenus(menuItems_);

    readSettings();
}

FlextraExaminer::~FlextraExaminer()
{
    // Save settings
    writeSettings();

    // Clean memory
    if (data_)
        delete data_;
}

void FlextraExaminer::setupViewActions()
{
    actionFileInfo_ = new QAction(this);
    actionFileInfo_->setObjectName(QString::fromUtf8("actionFileInfo"));
    actionFileInfo_->setText(tr("File info"));
    actionFileInfo_->setCheckable(true);
    actionFileInfo_->setChecked(true);
    actionFileInfo_->setToolTip(tr("View file info"));
    QIcon icon;
    icon.addPixmap(QPixmap(QString::fromUtf8(":/examiner/info_light.svg")), QIcon::Normal, QIcon::Off);
    actionFileInfo_->setIcon(icon);

    actionLog_ = MvQMainWindow::createAction(MvQMainWindow::LogAction, this);

    logPanel_->hide();  // hide log area

    // Signals and slots
    connect(actionFileInfo_, SIGNAL(toggled(bool)),
            fileInfoLabel_, SLOT(setVisible(bool)));

    connect(actionLog_, SIGNAL(toggled(bool)),
            logPanel_, SLOT(setVisible(bool)));

    MvQMainWindow::MenuType menuType = MvQMainWindow::ViewMenu;
    menuItems_[menuType].push_back(new MvQMenuItem(actionFileInfo_));
    menuItems_[menuType].push_back(new MvQMenuItem(actionLog_));
}

void FlextraExaminer::setupHelpActions()
{
    // About
    QAction* actionAbout = MvQMainWindow::createAction(MvQMainWindow::AboutAction, this);
    actionAbout->setText(tr("&About FLEXTRA Examiner"));
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(slotShowAboutBox()));

    MvQMainWindow::MenuType menuType = MvQMainWindow::HelpMenu;
    menuItems_[menuType].push_back(new MvQMenuItem(actionAbout, MvQMenuItem::MenuTarget));
}

void FlextraExaminer::setupDumpPanel()
{
    // Build a layout
    dumpPanel_ = new QWidget(this);
    auto* dumpLayout = new QVBoxLayout(dumpPanel_);
    dumpLayout->setContentsMargins(0, 0, 0, 0);

    dumpText_ = new PlainTextWidget(this);
    dumpText_->editor()->setReadOnly(true);
    dumpText_->editor()->setLineWrapMode(QPlainTextEdit::NoWrap);

    new MvQHighlighter(dumpText_->editor()->document());

    dumpLayout->addWidget(dumpText_);
}

void FlextraExaminer::setupBlockPanel()
{
    auto* blockLayout = new QVBoxLayout;
    blockLayout->setContentsMargins(0, 0, 0, 0);
    blockPanel_ = new QWidget;
    blockPanel_->setLayout(blockLayout);

    // Build a tree to show the data representation
    blockTree_ = new QTreeWidget(this);
    blockTree_->setAlternatingRowColors(true);
    blockTree_->setAllColumnsShowFocus(true);
    blockTree_->setColumnCount(4);
    blockTree_->setUniformRowHeights(true);
    blockTree_->setRootIsDecorated(false);

    QStringList hdr;
    hdr << tr("Index") << tr("Name") << tr("Step type") << tr("U");
    blockTree_->setHeaderLabels(hdr);

    QTreeWidgetItem* header = blockTree_->headerItem();
    header->setData(3, Qt::ToolTipRole, tr("Uncertainty trajectory"));

    blockTree_->setContextMenuPolicy(Qt::CustomContextMenu);

    blockLayout->addWidget(blockTree_);

    // connect(blockTree_,SIGNAL(itemActivated(QTreeWidgetItem*,int)),
    //	this,SLOT(slotBlockSelected(QTreeWidgetItem*,int)));

    connect(blockTree_, SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(slotContextMenu(const QPoint&)));
}


bool FlextraExaminer::initData(const std::string& filename, const std::string& icon)
{
    iconPath_ = icon;

    // Clean data if already exists
    if (data_) {
        delete data_;
        data_ = nullptr;
    }

    // Write initial message in the log area
    GuiLog().task() << "Loading FLEXTRA output file";

    // Initialize Flextra object
    data_ = new MvFlextra(filename);

    // Update File info label
    updateFileInfoLabel();

    if (data_->blockNum() == 0) {
        delete data_;
        data_ = nullptr;
        GuiLog().error() << "Could not open FLEXTRA file";
        StatusMsgHandler::instance()->failed();
        return false;
    }
    else if (data_->blockNum() == 1) {
        QFile file(QString::fromStdString(filename));
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QString str, line;
            QTextStream in(&file);
            while (!in.atEnd()) {
                line = in.readLine();
                str += line + '\n';
            }
            dumpText_->editor()->setPlainText(str);
        }
        file.close();
        blockPanel_->hide();
    }
    else {
        QTreeWidgetItem* firstItem = nullptr;

        for (int i = 0; i < data_->blockNum(); i++) {
            auto* item = new QTreeWidgetItem(blockTree_);
            QString comment = QString::fromStdString(data_->blocks().at(i)->comment());
            QString step = (data_->blocks().at(i)->constantStep()) ? tr("Constant") : tr("Flexible");
            QString utr = (data_->blocks().at(i)->uncertaintyTr()) ? tr("y") : tr("n");

            item->setData(0, Qt::DisplayRole, QString::number(i + 1));
            item->setData(1, Qt::DisplayRole, comment);
            item->setData(2, Qt::DisplayRole, step);
            item->setData(3, Qt::DisplayRole, utr);

            if (!firstItem)
                firstItem = item;
        }
        blockTree_->resizeColumnToContents(0);
        blockTree_->resizeColumnToContents(1);
        blockTree_->resizeColumnToContents(2);

        connect(blockTree_, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)),
                this, SLOT(slotBlockSelected(QTreeWidgetItem*, QTreeWidgetItem*)));

        if (firstItem) {
            blockTree_->setCurrentItem(firstItem);
        }
    }

    StatusMsgHandler::instance()->done();

    return true;
}

void FlextraExaminer::updateFileInfoLabel()
{
    fileInfoLabel_->setFlextraTextLabel(QString::fromStdString(data_->fileName()), QString::fromStdString(iconPath_), data_->blockNum());
}

void FlextraExaminer::slotShowAboutBox()
{
    MvQAbout about("FLEXTRA Examiner", "", MvQAbout::MetviewVersion);
    about.exec();
}

void FlextraExaminer::slotBlockSelected(QTreeWidgetItem* item, QTreeWidgetItem*)
{
    int row = item->data(0, Qt::DisplayRole).toString().toInt();
    row--;
    if (row >= 0 && row < data_->blockNum()) {
        if (tmpFilePath_.empty()) {
            tmpFilePath_ = std::string(marstmp());
        }
        data_->write(tmpFilePath_, row);

        QFile file(QString::fromStdString(tmpFilePath_));
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QString str, line;
            QTextStream in(&file);
            while (!in.atEnd()) {
                line = in.readLine();
                str += line + '\n';
            }
            dumpText_->editor()->setPlainText(str);
        }
        file.close();
    }
    else {
        dumpText_->clear();
    }
}


void FlextraExaminer::slotContextMenu(const QPoint& position)
{
    QTreeWidgetItem* item = blockTree_->itemAt(position);
    int row = item->data(0, Qt::DisplayRole).toString().toInt();
    row--;
    if (row < 0 || row >= data_->blockNum())
        return;

    QList<QAction*> actions;

    // Inser new key
    auto* actionSave = new QAction(this);
    actionSave->setObjectName(QString::fromUtf8("actionSave"));
    actionSave->setText(tr("Save"));
    actions.append(actionSave);

    if (QMenu::exec(actions, blockTree_->mapToGlobal(position)) == actionSave) {
        QString offeredName = QString::fromStdString(iconPath_).section('/', 0, -2) +
                              "/res_" + item->data(1, Qt::DisplayRole).toString() + ".txt";
        QString fileName = QFileDialog::getSaveFileName(this,
                                                        tr("Save Data To File"), offeredName, QString());

        data_->write(fileName.toStdString(), row);
    }

    delete actionSave;
}

void FlextraExaminer::writeSettings()
{
    QSettings settings(MVQ_QSETTINGS_DEFAULT_ARGS, "MV4-FlextraExaminer");

    settings.beginGroup("mainWindow");
    settings.setValue("geometry", saveGeometry());
    settings.setValue("centralSplitter", centralSplitter_->saveState());
    settings.setValue("mainSplitter", mainSplitter_->saveState());
    settings.setValue("actionFileInfoStatus", actionFileInfo_->isChecked());
    settings.endGroup();
}

void FlextraExaminer::readSettings()
{
    QSettings settings(MVQ_QSETTINGS_DEFAULT_ARGS, "MV4-FlextraExaminer");

    settings.beginGroup("mainWindow");
    restoreGeometry(settings.value("geometry").toByteArray());
    centralSplitter_->restoreState(settings.value("centralSplitter").toByteArray());
    mainSplitter_->restoreState(settings.value("mainSplitter").toByteArray());

    if (settings.value("actionFileInfoStatus").isNull())
        actionFileInfo_->setChecked(true);
    else
        actionFileInfo_->setChecked(settings.value("actionFileInfoStatus").toBool());

    settings.endGroup();
}

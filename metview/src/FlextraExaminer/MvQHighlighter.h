/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QtGlobal>
#include <QSyntaxHighlighter>
#include <QHash>
#include <QTextCharFormat>

#if QT_VERSION < QT_VERSION_CHECK(5, 12, 0)
#include <QRegExp>
#endif
#include <QRegularExpression>

class QTextDocument;

class MvQHighlighter : public QSyntaxHighlighter
{
public:
    MvQHighlighter(QTextDocument* parent = nullptr);

protected:
    void highlightBlock(const QString& text) override;

private:
    struct HighlightingRule
    {
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        QRegularExpression pattern;
#else
        QRegExp pattern;
#endif
        QTextCharFormat format;
    };

    QVector<HighlightingRule> highlightingRules;
};

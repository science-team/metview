/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQHighlighter.h"

#include <QStringList>
#include <QRegularExpressionMatch>


MvQHighlighter::MvQHighlighter(QTextDocument* parent) :
    QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    // Keywords
    QTextCharFormat keywordFormat;

    keywordFormat.setForeground(Qt::darkBlue);
    keywordFormat.setFontWeight(QFont::Bold);

    QStringList keywordPatterns;
    keywordPatterns << "DATE"
                    << "TIME"
                    << "STOP INDEX"
                    << "# OF POINTS";

    foreach (QString pattern, keywordPatterns) {
        QString s = pattern + "(?=:)";
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        rule.pattern = QRegularExpression(s);
        rule.pattern.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
#else
        rule.pattern = QRegExp(s, Qt::CaseInsensitive);
#endif
        rule.format = keywordFormat;
        highlightingRules.append(rule);
    }

    // Data keywords
    QTextCharFormat dataKeyFormat;

    dataKeyFormat.setForeground(QColor(224, 138, 33));
    // dataKeyFormat.setForeground(QColor(255,119,119));
    dataKeyFormat.setFontWeight(QFont::Bold);

    QStringList dataKeyPatterns;
    dataKeyPatterns << " SECS "
                    << " LONGIT "
                    << " LATIT "
                    << " ETA "
                    << " PRESS "
                    << "  Z "
                    << " Z-ORO "
                    << " PV "
                    << " THETA "
                    << " Q";

    foreach (QString pattern, dataKeyPatterns) {
        // rule.pattern = QRegExp(pattern,Qt::CaseInsensitive);
        // QString s=pattern + "(?==)";
        QString s = pattern;
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        rule.pattern = QRegularExpression(s);
        rule.pattern.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
#else
        rule.pattern = QRegExp(s, Qt::CaseInsensitive);
#endif
        rule.format = dataKeyFormat;
        highlightingRules.append(rule);
    }

    // Comments
    QTextCharFormat singleLineCommentFormat;

    singleLineCommentFormat.setFontItalic(true);
    singleLineCommentFormat.setForeground(Qt::darkGreen);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
    QRegularExpression rx;
    rx.setPattern(QRegularExpression::wildcardToRegularExpression("\\*[^\n]*"));
#else
    QRegExp rx("\\*[^\n]*");
    rx.setPatternSyntax(QRegExp::WildcardUnix);
#endif
    rule.pattern = rx;
    rule.format = singleLineCommentFormat;
    highlightingRules.append(rule);
}

void MvQHighlighter::highlightBlock(const QString& text)
{
    foreach (HighlightingRule rule, highlightingRules) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        QRegularExpression expression(rule.pattern);
        QRegularExpressionMatch rmatch;
        int index = text.indexOf(expression, 0, &rmatch);
        while (index >= 0) {
            int length = rmatch.capturedLength();
            setFormat(index, length, rule.format);
            index = text.indexOf(expression, index + length, &rmatch);
        }
#else
        QRegExp expression(rule.pattern);
        int index = text.indexOf(expression);
        while (index >= 0) {
            int length = expression.matchedLength();
            setFormat(index, length, rule.format);
            index = text.indexOf(expression, index + length);
        }
#endif
    }
    setCurrentBlockState(0);
}

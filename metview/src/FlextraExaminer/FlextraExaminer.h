/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QMainWindow>
#include <QModelIndex>

#include "MvQMainWindow.h"
#include "MvQMenuItem.h"

class QPlainTextEdit;
class QSplitter;
class QTreeWidget;
class QTreeWidgetItem;

class MvFlextra;
class MvQFileInfoLabel;
class MvQLogPanel;
class PlainTextWidget;


class FlextraExaminer : public MvQMainWindow
{
    Q_OBJECT

public:
    FlextraExaminer(QWidget* parent = nullptr);
    ~FlextraExaminer() override;
    bool initData(const std::string&, const std::string&);
    void updateFileInfoLabel();

public slots:
    void slotShowAboutBox();
    void slotBlockSelected(QTreeWidgetItem*, QTreeWidgetItem*);
    void slotContextMenu(const QPoint&);

private:
    void setupViewActions();
    void setupHelpActions();
    void setupBlockPanel();
    void setupDumpPanel();
    void setupFindPanel();

    void readSettings();
    void writeSettings();

    MvFlextra* data_;
    std::string tmpFilePath_;
    std::string iconPath_;

    MvQMainWindow::MenuItemMap menuItems_;
    MvQFileInfoLabel* fileInfoLabel_;

    QAction* actionFileInfo_;
    QSplitter* mainSplitter_;
    QSplitter* centralSplitter_;
    MvQLogPanel* logPanel_;
    QAction* actionLog_;
    QLabel* statusMessageLabel_;
    QWidget* blockPanel_;
    QWidget* dumpPanel_;
    PlainTextWidget* dumpText_;
    QTreeWidget* blockTree_;
};

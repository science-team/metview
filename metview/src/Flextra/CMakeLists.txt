
ecbuild_add_executable( TARGET       FlextraPrepare
                        SOURCES      FlextraPrepare.cc                                    
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )

ecbuild_add_executable( TARGET       FlextraRun
                        SOURCES      FlextraRun.cc                                    
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )

ecbuild_add_executable( TARGET       FlextraVisualiser
                        SOURCES      FlextraVisualiser.cc
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )

ecbuild_add_executable( TARGET       FlexpartRun
                        SOURCES      FlexpartRun.cc                                     
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )

ecbuild_add_executable( TARGET       flexpartField
                        SOURCES      flexpartField.cc
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )

ecbuild_add_executable( TARGET       flexpartReceptor
                        SOURCES      flexpartReceptor.cc
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )

ecbuild_add_executable( TARGET       flexpartTrajectory
                        SOURCES      flexpartTrajectory.cc
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )

# the list of files to be copied to etc
metview_module_files(ETC_FILES ObjectSpec.Flextra
                               FlexpartPrepareDef
                               FlexpartPrepareRules
                               FlextraPrepareDef
                               FlextraPrepareRules
                               FlextraRunDef
                               FlextraRunRules
                               FlextraVisualiserDef
                               FlextraVisualiserRules
                               ObjectSpec.Flexpart
                               FlexpartReleaseDef
                               FlexpartRunDef
                               FlexpartRunRules
                     SVG_FILES FLEXPART_PREPARE.svg
                               FLEXPART_RUN.svg
                               FLEXPART_RELEASE.svg)

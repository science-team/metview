/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// MvThermoParams.h - Nov2003/vk  - original file
// ParamsThermo.h   - Jan2014/fii - updated to the new Thermo application

#pragma once

#include <string>

enum EPointType
{
    eAreaAverage,
    eGridPoint,
    eInterpolated
};

struct ThermoParams
{
    std::string dewPointFormulaType_;
    std::string errorMsg_;
    int uFieldId_{0};
    int vFieldId_{0};
    int tempId_{0};
    int dewId_{0};
    int lnPressId_{0};
    double lat_{0.};
    double lon_{0.};
    double lat2_{0.};
    double lon2_{0};
    EPointType pointType_{eGridPoint};
};

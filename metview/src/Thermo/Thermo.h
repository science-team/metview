/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Metview.h"

// Constant variable names/sizes
static const float K2C = 273.15;
static const float MissingValue = 1.7e38;
static const std::string sTIME_STR = "str_length";
static const std::string sTIME = "time";
static const std::string sTEMP = "t";
static const std::string sDEWPOINT = "td";
static const std::string sPRESSURE = "pres";
static const std::string sGEOPOTENTIAL = "z";
static const std::string sWIND = "wind";
static const std::string sU = "u";
static const std::string sV = "v";
static const std::string sXWIND = "xwind";
static const std::string sPWIND = "pwind";
static const std::string sNLEV = "nlev";
static const std::string sNLEVWIND = "nlevwind";

// Type of input request to be processed
enum
{
    THERMO_DATA_DROPPED,
    THERMO_DATA_MODULE,
    THERMO_DATA_MODULE_DROPPED
};

class Thermo : public MvService
{
public:
    Thermo(const char*);
    virtual ~Thermo() {}

    // Entry point routine
    void serve(MvRequest&, MvRequest&);

    // Get input parameters
    virtual bool getInputParameters(MvRequest&);

    // Process data and build all data structures
    virtual bool processData() = 0;

    // Create output request
    virtual bool createOutputRequest(MvRequest&) = 0;

    // Handle variable members
    void actionMode(std::string action) { actionMode_ = action; }
    std::string actionMode() { return actionMode_; }
    // void processType( int type )     { procType_ = type; }
    // int  processType( )              { return procType_; }
    void ncFileName(std::string name) { ncFileName_ = name; }
    std::string ncFileName() { return ncFileName_; }

    // Check if processing is related to visualisation
    bool isVisualise();

    // Check if input parameter is a data module
    bool isDataModule(const char*);

    // Get the application (original) view request
    MvRequest getAppView(MvRequest&);

    // Add information to help the Macro Converter to translate
    // the output request to a Macro code
    MvRequest buildMacroConverterRequest();

protected:
    // Get application specific parameters
    virtual bool getAppParameters(MvRequest&) = 0;

    // Check if parameters between two requests are consistent
    // It seems that the application should overlay diagrams independently how
    // they were computed.
    virtual bool consistencyCheck(MvRequest&, MvRequest&) { return true; }

    void createProcessedOutputRequest(MvRequest&);

    // Variable members
    std::string actionMode_;  // e.g. "examine"/"save"/"execute"/...
    std::string ncFileName_;  // netcdf file name
    int procType_;            // processing type, e.g.,THERMO_DATA_MODULE,...
    MvRequest dataRequest_;   // data request
    MvRequest origReq_;       // input request
    std::string moduleLabel_;
};

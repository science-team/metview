/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "ParamsThermo.h"
#include "Thermo.h"

class MvNetCDF;

class GribThermo : public Thermo
{
public:
    // Constructors
    GribThermo();
    GribThermo(const char*);

    // Destructor
    ~GribThermo();

    // Process data and build all data structures
    bool processData();

    // Generate output request
    bool createOutputRequest(MvRequest&);

private:
    // Get application specific parameters
    virtual bool getAppParameters(MvRequest&);

    // Variables
    bool windFlag_{false};  // T - there is wind data
    std::string title_;     // title information
    ThermoParams pars_;     // parameters from the user interface
};

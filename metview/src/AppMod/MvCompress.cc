/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <unistd.h>
#include <pwd.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include "fstream_mars_fix.h"

#include "Metview.h"
#include "MvService.h"
#include "MvCompressRun.h"

std::string ftmp;

// Globals
int ac;
char** av;

class MvCompress : public MvService
{
public:
    MvCompress(const char* name) :
        MvService(name) {}
    void serve(MvRequest&, MvRequest&) override;
};

void MvCompress::serve(MvRequest& in, MvRequest& out)
{
    std::cout << "--------------MvCompress::serve()--------------" << std::endl;
    in.print();

    metview::compress::MvCompressRun compressor;
    int ret = compressor.run(in, out);

    if (ret != 0) {
        std::string st;
        out.getValue("STATUS", st, false);
        const char *errMsg = out("TEXT");
        std::string errMsgS = (errMsg!=nullptr)?errMsg:"MvCompress failed";

        if (st == "ERROR") {
            setError(1, errMsgS.c_str());
        }  else if (st == "WARNING") {
            setError(0, errMsgS.c_str());
        }
    }
}

int main(int argc, char** argv)
{
    ac = argc;
    av = argv;

    MvApplication theApp(argc, argv, "MvCompress");

    MvCompress arch("ARCHIVE");
    MvCompress cmp("COMPRESS");

    theApp.run();
}

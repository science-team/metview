/***************************** LICENSE START ***********************************

 Copyright 2020 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQSavePool.h"

#include <QApplication>
#include "MvQFileDialog.h"

MvQSavePool::MvQSavePool(QString iconDir)
{
    fDialog_ = new MvQFileDialog(iconDir, tr("Save as"));
    fDialog_->setAcceptMode(QFileDialog::AcceptSave);

    connect(fDialog_, SIGNAL(accepted()),
            this, SLOT(slotAccept()));

    fDialog_->show();
}

MvQSavePool::~MvQSavePool()
{
    delete fDialog_;
}

void MvQSavePool::slotAccept()
{
    QStringList lst = fDialog_->selectedFiles();
    if (lst.count() > 0) {
        selectedFile_ = lst[0];
    }

    // We leave the event loop
    QApplication::quit();
}

void MvQSavePool::slotCancel()
{
    // We leave the event loop
    QApplication::quit();
}

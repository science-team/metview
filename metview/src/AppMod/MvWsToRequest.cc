/***************************** LICENSE START ***********************************

 Copyright 2022 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <string>
#include <iostream>

#include "ConfigLoader.h"
#include "MvRequest.h"

// this is based on Qt!
#include "WsDecoder.h"
#include "WsType.h"

int main(int argc, char** argv)
{
    if (argc != 3) {
        std::cerr << "MvWsToRequest: number of arguments must be 2! (called with " << argc - 1 << ")" << std::endl;
        exit(1);
    }

    std::string fIn(argv[1]);
    std::string fOut(argv[2]);

    //    std::cout << "MvWsToRequest: fIn=" << fIn << std::endl;
    //    std::cout << "MvWsToRequest: fOut=" << fOut << std::endl;

    marsinit(&argc, argv, nullptr, 0, nullptr);
    ConfigLoader::init();

    MvRequest r;
    WsType::init(WsType::CodeGenerateMode);
    WsDecoder::toRequest(fIn, r, EXPAND_NO_DEFAULT | EXPAND_2ND_NAME);
    //    r.print();

    r.save(fOut.c_str());
}

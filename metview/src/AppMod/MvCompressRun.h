/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvRequest.h"

namespace metview {
namespace compress {

class MvCompressRun
{
public:
    MvCompressRun() = default;
    void enableMailMode(const std::string& compressFile, const std::string& dumpFile);
    int run(MvRequest& in, MvRequest& out);

private:
    bool mailMode_{false};
    std::string compressFile_;
    std::string dumpFile_;
};

}
}

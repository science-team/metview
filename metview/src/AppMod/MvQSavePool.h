/***************************** LICENSE START ***********************************

 Copyright 2020 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QObject>
#include <QString>

class MvQFileDialog;

class MvQSavePool : public QObject
{
    Q_OBJECT

public:
    MvQSavePool(QString);
    ~MvQSavePool() override;
    QString selectedFile() const { return selectedFile_; }

public slots:
    void slotAccept();
    void slotCancel();

private:
    MvQFileDialog* fDialog_{nullptr};
    QString selectedFile_;
};

/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQWmsClient.h"
#include "MvQWmsClientEditor.h"
#include "Metview.h"
#include "MvQApplication.h"


int main(int argc, char** argv)
{
    if (argc < 2) {
        marslog(LOG_EROR, "No arguments are specified");
        exit(1);
    }

    MvRequest in;
    in.read(argv[1], false, true);

    marslog(LOG_INFO, "Request:");
    in.print();

    if (!in) {
        marslog(LOG_EROR, "No request could be read from request file=%s", argv[1]);
        exit(1);
    }

    // Get input file name
    std::string fPath;
    if (const char* fPathCh = in("_NAME")) {
        fPath = std::string(fPathCh);
    }
    else {
        marslog(LOG_EROR, "No PATH is specified!");
        exit(1);
    }

    MvQApplication::writePidToFile(in);

    // Create the qt application. The appname must be unique!
    std::string appName = MvApplication::buildAppName("WmsClientEditor");
    MvQApplication app(argc, argv, appName.c_str(), {"MvQOgcClient", "examiner", "find", "window"});

    // Initialize client
    auto* wmsClient = new MvQWmsUiClient(fPath, in);

    // Define the ogc service manager configuration file name
    char* mvhome = getenv("METVIEW_USER_DIRECTORY");
    if (mvhome == nullptr) {
        marslog(LOG_EROR, "Environment variable METVIEW_USER_DIRECTORY is not defined!");
        exit(1);
    }

    // Read the icon file and initialize the dialog
    auto* editor = new MvQWmsClientEditor(wmsClient);
    editor->setAppIcon("WMS_CLIENT");
    editor->show();

    // register for callbacks from Desktop
    app.registerToDesktop(editor);

    // Enter the app loop
    app.exec();
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QGridLayout>
#include <QLineEdit>
#include <QComboBox>
#include <QMainWindow>
#include <QStringList>

#include "MvQMainWindow.h"
#include "MvQMenuItem.h"
#include "MvQWmsClient.h"

class QAbstractButton;
class QCheckBox;
class QComboBox;
class QDialogButtonBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QListWidgetItem;
class QListWidget;
class QProgressBar;
class QPushButton;
class QScrollArea;
class QSplitter;
class QTabWidget;
class QTextBrowser;
class QTextEdit;
class QToolButton;
class QTreeView;
class QVBoxLayout;

class MvQLogPanel;
class MvQOgcTreeModel;
class MvQTextEditSearchLine;
class MvQTreeView;
class MvQWmsUiClient;
class PlainTextWidget;

class MvQWmsDimensionGuiItem : public QObject
{
    Q_OBJECT
public:
    MvQWmsDimensionGuiItem(MvQOgcDimension*, QString, QGridLayout*, int);
    ~MvQWmsDimensionGuiItem() override;
    QString currentValue();
    void addValue(QString);

public slots:
    void slotClearText(bool);
    void slotSetVisibleList(bool);
    void slotListItemChanged(QListWidgetItem*);
    void slotLineEditChanged(QString);
    void slotAddItem(QListWidgetItem*);

signals:
    void textChanged(MvQOgcDimension*, QString);

private:
    MvQOgcDimension* dim_;
    QLabel* label_;
    QLineEdit* lineEdit_;
    QStringList values_;
    QToolButton* expandTb_;
    QListWidget* list_;
    int row_;
};


class MvQWmsClientEditor : public MvQMainWindow
{
    Q_OBJECT

public:
    MvQWmsClientEditor(MvQWmsUiClient*, QWidget* w = nullptr);
    ~MvQWmsClientEditor() override;

public slots:
    void slotClientModeChanged(QString);
    // void slotSaveInfo();
    void slotSelectUrl(const QString&);
    void slotSelectVersion(const QString&);
    void slotRunGetCapabilities();
    void slotGetCapabilityLoaded();
    void slotSelectLayer(const QModelIndex&);
    void slotSelectStyle(int);
    void slotSelectCrs(QString);
    void slotDimensionChanged(MvQOgcDimension*, QString);
    void slotExtraGetCapChanged(QString);
    void slotExtraGetMapChanged(QString);
    void slotUpdateLayerInfo();
    void slotPreview();
    void slotPreviewLoaded(QString);
    void slotLegendLoaded(QString);
    void slotLogoLoaded(QString);
    void slotSetGetMapInfo(QString, QStringList);
    void slotReqEdited(const QString&);
    void slotImportRequest();
    void slotClientReset();
    void slotUpdateServiceInfo();
    void slotButtonClicked(QAbstractButton*);

signals:
    void clientModeChanged();
    void clientModeIsAboutToChange();

protected:
    void setupFileActions();
    void setupSettingsActions();
    void setupViewActions();
    void setupLayerPanel(QVBoxLayout*);
    void setupLayerInfoTab();
    void setupLayerCombo();
    void setupLayerList();
    void setupDimension();
    void setupReqEditPanel();

    void initClientMode();
    void initVersionCombo();
    void initExtraParEdit();
    void initFormatCombo();
    void initStyleCombo();
    void initCrsCombo();

    void clearServerDependentData();
    void clearLayerDependentData();
    void clearDimension();

    void checkUrlComboItems();

    void loadStarted();
    void loadFinished();
    void writeSettings();
    void readSettings();

private:
    MvQWmsUiClient* client_{nullptr};

    QVBoxLayout* mainLayout_{nullptr};
    QSplitter* mainSplitter_{nullptr};
    QSplitter* requestSplitter_{nullptr};

    // QCheckBox* openCb_;

    QLabel* loadLabel_{nullptr};
    QLabel* statusMessageLabel_{nullptr};
    QList<QWidget*> advancedModeOnlyWidgets_;

    MvQMainWindow::MenuItemMap menuItems_;
    QAction* actionCapability_{nullptr};
    QAction* actionStopLoad_{nullptr};
    QAction* actionLog_{nullptr};
    QComboBox* urlCombo_{nullptr};
    QComboBox* versionCombo_{nullptr};
    QComboBox* clientModeCombo_{nullptr};
    QMap<QString, MvQWmsUiClient::ClientMode> clientModeMap_;

    QComboBox* formatCombo_{nullptr};
    QLineEdit* extraGetCapEdit_{nullptr};
    QLineEdit* extraGetMapEdit_{nullptr};

    QTabWidget* layerTab_{nullptr};
    QWidget* layerPanel_{nullptr};
    QGridLayout* layerGrid_{nullptr};
    QHBoxLayout* layerLayout_{nullptr};
    MvQTreeView* layerTree_{nullptr};
    MvQOgcTreeModel* layerModel_{nullptr};
    QComboBox* crsCombo_{nullptr};
    QComboBox* styleCombo_{nullptr};
    QPushButton* previewPb_{nullptr};
    int dimensionNum_{0};
    int layerParamRowNum_{0};
    QMap<QString, MvQWmsDimensionGuiItem*> dim_;
    QLabel* crsLabel_{nullptr};

    QTabWidget* layerInfoTab_{nullptr};
    PlainTextWidget* getCapInfo_{nullptr};
    QTextBrowser* serviceInfo_{nullptr};
    QTextBrowser* layerPreview_{nullptr};
    QTextBrowser* getMapInfoSplit_{nullptr};
    //    MvQHighlighter* highlighterSplit_;
    QPushButton* plainPreviewPb_{nullptr};
    QLabel* plainPreviewLabel_{nullptr};
    QWidget* plainPreviewPanel_{nullptr};

    MvQLogPanel* logPanel_{nullptr};

#ifdef UI_TODO
    MvQTextEditSearchLine* getCapSearch_;
#endif

    //    MvQWmsGetCapHighlighter* highlighterGetCap_;

    QProgressBar* loadProgress_{nullptr};

    QMap<QString, QLineEdit*> reqLe_;
    QMap<QString, QTextEdit*> reqTe_;
    QSplitter* reqSplitter_{nullptr};
    QWidget* reqEditPanel_{nullptr};
    QScrollArea* reqEditSa_{nullptr};

    QDialogButtonBox* buttonBox_{nullptr};
    QPushButton* savePb_{nullptr};
    QPushButton* okPb_{nullptr};
    QPushButton* cancelPb_{nullptr};
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QDialog>
#include <QStringList>

class QListWidget;
class QListWidgetItem;
class QTreeWidget;
class QTreeWidgetItem;
class QVBoxLayout;

class MvQWmsClient;
class MvQOgcServer;
class MvQOgcServiceManager;

class MvQOgcServiceManagerDialog : public QDialog
{
    Q_OBJECT

public:
    MvQOgcServiceManagerDialog(QWidget* w = 0);
    ~MvQOgcServiceManagerDialog(){};
    void init(MvQOgcServiceManager*);

signals:
    void dataChanged();

public slots:
    void accept();
    void reject();
    void slotAddServer();
    void slotDeleteServer();
    void slotSelectServer(QTreeWidgetItem*, QTreeWidgetItem*);
    void slotVersionStatusChanged(QListWidgetItem*);

protected:
    QVBoxLayout* layout_;
    QTreeWidget* serverTree_;
    QListWidget* versionList_;
    QStringList versions_;
    MvQOgcServiceManager* manager_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQOgcTreeModel.h"

#include "MvQOgcParam.h"

MvQOgcTreeModel::MvQOgcTreeModel(QObject* parent) :
    QAbstractItemModel(parent)
{
}

void MvQOgcTreeModel::dataIsAboutToChange()
{
    beginResetModel();
}

void MvQOgcTreeModel::setRootNode(MvQOgcNode* root)
{
    root_ = root;

    // Reset the model (views will be notified)
    endResetModel();
}


int MvQOgcTreeModel::columnCount(const QModelIndex& /* parent */) const
{
    return 1;
}

int MvQOgcTreeModel::rowCount(const QModelIndex& parent) const
{
    if (!root_)
        return 0;

    if (parent.column() > 0) {
        return 0;
    }

    MvQOgcNode* parentNode = nodeFromIndex(parent);
    if (!parentNode)
        return 0;

    return parentNode->children().count();
}


QVariant MvQOgcTreeModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid() || (role != Qt::DisplayRole && role != Qt::ToolTipRole)) {
        return {};
    }

    MvQOgcNode* node = nodeFromIndex(index);
    if (!node)
        return {};

    if (role == Qt::ToolTipRole) {
        QString s = "Title: " + node->displayValue() + "<br>" +
                    "Name:  " + node->value();

        return s;
    }
    else {
        return node->displayValue();
    }
}


QVariant MvQOgcTreeModel::headerData(const int section, const Qt::Orientation orient, const int role) const
{
    if (orient != Qt::Horizontal || role != Qt::DisplayRole)
        return QAbstractItemModel::headerData(section, orient, role);

    return section == 0 ? "Layer title" : QVariant();
}

QModelIndex MvQOgcTreeModel::index(int row, int column, const QModelIndex& parent) const
{
    if (!root_ || row < 0 || column < 0) {
        return {};
    }

    MvQOgcNode* parentNode = nodeFromIndex(parent);
    MvQOgcNode* childNode = parentNode->children().at(row);

    if (!childNode)
        return {};

    return createIndex(row, column, childNode);
}

MvQOgcNode* MvQOgcTreeModel::nodeFromIndex(const QModelIndex& index) const
{
    if (index.isValid()) {
        return static_cast<MvQOgcNode*>(index.internalPointer());
    }
    else {
        return root_;
    }
}

QModelIndex MvQOgcTreeModel::parent(const QModelIndex& child) const
{
    MvQOgcNode* node = nodeFromIndex(child);
    if (!node)
        return {};

    MvQOgcNode* parentNode = node->parent();
    if (!parentNode)
        return {};

    MvQOgcNode* grandParentNode = parentNode->parent();
    if (!grandParentNode)
        return {};

    int row = grandParentNode->children().indexOf(parentNode);
    return createIndex(row, 0, parentNode);
}

QModelIndex MvQOgcTreeModel::indexFromNode(MvQOgcNode* node) const
{
    if (node != nullptr && node->parent() != nullptr) {
        int row = node->parent()->children().indexOf(node);
        if (row != -1) {
            return createIndex(row, 0, node);
        }
    }

    return {};
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QString>
#include <QStringList>
#include <QXmlResultItems>

class MvQOgcServer
{
public:
    MvQOgcServer(const QString& shortName, const QString& url, const QStringList& versions) :
        shortName_(shortName),
        url_(url),
        versions_(versions){};
    const QString& url() { return url_; }
    const QString& shortName() { return shortName_; }
    const QStringList& versions() { return versions_; }

protected:
    QString shortName_;
    QString url_;
    QStringList versions_;
};

class MvQOgcServiceManager
{
public:
    MvQOgcServiceManager(QString);
    ~MvQOgcServiceManager();
    QList<MvQOgcServer*> wms() { return server_; }
    QList<MvQOgcServer*> server() { return server_; }

    void addServer(MvQOgcServer* s) { addServer(s); };
    void addWms(MvQOgcServer*);
    void clearWms();
    void clear() { clearWms(); }
    int findServerByUrl(QString);
    int serverNum() { return server_.count(); }

protected:
    void readConfig();
    void writeConfig();
    void getAtomicValues(QXmlResultItems&, QStringList&);

    QString configFile_;
    QList<MvQOgcServer*> server_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QDateTime>
#include <QMap>
#include <QStringList>

class MvQOgcRequest
{
public:
    MvQOgcRequest() = default;
    virtual ~MvQOgcRequest() = default;

    const QString& request() const { return request_; }
    void setRequest(QString);
    virtual QString item(QString) const;
    void setItem(QString, QString);
    virtual void removeItem(QString);
    virtual void clear();
    QString guessUrl() const;

    static QString timeToString(QString);
    static QString correctDateTime(QString);

protected:
    virtual void parse();
    QString getStoredId(QString) const;
    static void decodePeriod(const QString&, QMap<QString, int>&);
    static void addPeriod(QDateTime&, const QMap<QString, int>&);
    static int periodToSeconds(QString);
    static QString timeFormat(QString);

    QString request_;
    QMap<QString, QString> requestMap_;
};


class MvQGetMapRequest : public MvQOgcRequest
{
public:
    MvQGetMapRequest();

    void setRequest(QString, QStringList lst = QStringList());
    void editRequest(QString);

    void clear() override;
    int requestFrameNum() { return requestFrame_.count(); }
    QString requestFrame(int);
    QStringList requestFrame() { return requestFrame_; }
    QStringList outFile() { return outFile_; }
    QString outFile(int);
    void setOutFile(int, QString);
    bool isTimePresent() { return (timeDimName_.size() > 0) ? true : false; }

    void setTimeDimName(QStringList s) { timeDimName_ = s; }
    static int maxFrame() { return maxFrame_; }

    QString crs() const;
    QString minX();
    QString maxX();
    QString minY();
    QString maxY();
    QString layers();
    void dimValuesForFrame(QMap<QString, QString>&, int);
    // void timeDimValuesForFrame(QMap<QString,QString>&,int);
    // void nonTimeDimValues(QMap<QString,QString>&);
    bool isTimeDim(QString);
    QString item(QString) const override;
    void setItem(QString, QString);
    void removeItem(QString) override;
    void setDims(QString, bool);
    QStringList dims() const;

private:
    void parse() override;
    bool getNextVariation(int&, QList<int>, QList<int>&, bool);
    void decodeTimeDimItem(QString, QStringList&);
    void decodeDimItem(QString, QStringList&);
    void setExtraPar(QString);

    QStringList outFile_;
    QStringList requestFrame_;
    QStringList timeDimName_;
    static int maxFrame_;
    QStringList stdParams_;
};

class MvQGetCoverageRequest : public MvQOgcRequest
{
public:
    MvQGetCoverageRequest() = default;
    QString outFile() { return outFile_; }
    void setOutFile(QString s) { outFile_ = s; }

private:
    QString outFile_;
};

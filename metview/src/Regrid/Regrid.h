/***************************** LICENSE START ***********************************

 Copyright 2020 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include "mars.h"

#include "MvRequest.h"
#include "MvProtocol.h"
#include "MvService.h"


namespace mir
{
namespace api
{
class MIRJob;
}
namespace input
{
class MIRInput;
}
namespace output
{
class MIROutput;
}
}  // namespace mir


class Regrid : public MvService
{
public:
    // -- Constructors

    using MvService::MvService;

    // Destructor

    ~Regrid() = default;

    // -- Convertors
    // None

    // -- Operators
    // None

    // -- Methods
    // None

    // -- Overridden methods
    // None

    // -- Class members
    // None

    // -- Class methods
    // None

private:
    // -- Members
    // None

    // -- Methods

    static void process(const mir::api::MIRJob&, mir::input::MIRInput&, mir::output::MIROutput&,
                        const std::string& what);

    // -- Overridden methods

    void serve(MvRequest& in, MvRequest& out);

    // -- Friends
    // None
};

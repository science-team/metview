
ecbuild_add_executable(
    TARGET       Regrid
    SOURCES      Regrid.cc
    DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
    INCLUDES     ${METVIEW_STANDARD_INCLUDES}
    LIBS         ${STANDARD_METVIEW_LIBS} )


metview_module_files(
    ETC_FILES  ObjectSpec.Regrid RegridDef RegridRules
    SVG_FILES  REGRID.svg )

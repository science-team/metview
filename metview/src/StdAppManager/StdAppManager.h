/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

/************************************
  Application StdAppManager

  Manager a standard Metview module
************************************/

#include "Metview.h"

class StdAppManager : public MvService
{
public:
    // Constructor
    StdAppManager(const char* kw);

    // Destructor
    ~StdAppManager() override = default;

    void serve(MvRequest&, MvRequest&) override;

protected:
    // Handle missing value. For example:
    // Macro code:
    //     yvals = interpolate(fieldset,lat,lon)  # [8, nil, 9]
    //     iv = input_visualiser(input_y_values : yvals, ...)
    // Macro converts nil to DBL_MAX
    // This function replaces DBL_MAX to a missing value indicator
    void missingValue(MvRequest&, std::string, std::string);
};

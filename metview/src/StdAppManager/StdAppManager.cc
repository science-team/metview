/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "StdAppManager.h"

#include <iostream>


StdAppManager::StdAppManager(const char* kw) :
    MvService(kw)
{
}

void StdAppManager::serve(MvRequest& in, MvRequest& out)
{
    std::cout << "StdAppManager::serve in" << std::endl;
    in.print();

    // Get prefix
    std::string verb = in.getVerb();
    size_t found = verb.find('_');
    if (found == std::string::npos) {
        setError(1, "StdAppManager-> Visualiser verb not found");
        return;
    }
    std::string prefix = verb.substr(0, found + 1);

    // Build the application verb
    std::string plotType = prefix + "PLOT_TYPE";
    const char* type = in(plotType.c_str());
    prefix += type;

    // special case - INPUT_XY_AREA only exists in Metview -
    // Magics should see it as INPUT_XY_POINTS
    if (prefix == "INPUT_XY_AREA")
        prefix = "INPUT_XY_POINTS";


    // Create output request
    out = in;
    out.setVerb(prefix.c_str());
    out.unsetParam(plotType.c_str());
    out("_VERB") = prefix.c_str();
    out("_CLASS") = prefix.c_str();

    // Check missing values
    if ((const char*)out("INPUT_X_VALUES"))
        missingValue(out, "INPUT_X_VALUES", "INPUT_X_MISSING_VALUE");

    if ((const char*)out("INPUT_Y_VALUES"))
        missingValue(out, "INPUT_Y_VALUES", "INPUT_Y_MISSING_VALUE");

    // another special case - Magics does not like having the default
    // value of NETCDF_POSITION_TYPE sent to it
    const char* netcdfPosType = in("NETCDF_POSITION_TYPE");
    if (netcdfPosType && !strcmp(netcdfPosType, "ARRAY")) {
        out.unsetParam("NETCDF_POSITION_TYPE");
    }

    std::cout << "StdAppManager::serve out" << std::endl;
    out.print();
}

void StdAppManager::missingValue(MvRequest& out, std::string param, std::string smissing_value)
{
    // Get missing value indicator
    double missingValue = DBL_MAX;
    if ((const char*)out(smissing_value.c_str()))
        missingValue = double(out(smissing_value.c_str()));

    // Replace nil by missing value
    bool found = false;
    const char* cval = nullptr;
    std::vector<double> dval;
    out.iterInit(param.c_str());
    while (out.iterGetNextValue(cval)) {
        if (atof(cval) >= DBL_MAX / 10.) {
            dval.push_back(missingValue);
            found = true;
        }
        else
            dval.push_back(atof(cval));
    }

    // Update request
    if (found) {
        auto it = dval.begin();
        out.setValue(param.c_str(), *it++);
        while (it != dval.end())
            out.addValue(param.c_str(), *it++);
    }
}

//--------------------------------------------------------

int main(int argc, char** argv)
{
    MvApplication theApp(argc, argv);

    StdAppManager input("INPUT_VISUALISER");
    StdAppManager table("TABLE_VISUALISER");
    StdAppManager netcd("NETCDF_VISUALISER");

    theApp.run();
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "AbstractTextEditSearchInterface.h"
#include "TextPagerCursor.hpp"

class TextPagerEdit;

class TextPagerSearchInterface : public AbstractTextEditSearchInterface
{
public:
    TextPagerSearchInterface() :
        editor_(nullptr) {}
    void setEditor(TextPagerEdit* e) { editor_ = e; }

    bool findString(QString str, bool highlightAll, QTextDocument::FindFlags findFlags,
                    QTextCursor::MoveOperation move, int iteration, StringMatchMode::Mode matchMode);

    void automaticSearchForKeywords(bool);
    void refreshSearch();
    void clearHighlights();
    void disableHighlights();
    void enableHighlights();
    bool highlightsNeedSearch() { return false; }
    void gotoLastLine();

protected:
    TextPagerCursor::MoveOperation translateCursorMoveOp(QTextCursor::MoveOperation move);
    TextPagerEdit* editor_;
};

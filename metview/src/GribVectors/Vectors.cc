/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Vectors.h"

#include <iostream>

GribVectors::GribVectors(const char* kw) :
    MvService(kw)
{
}

void GribVectors::serve(MvRequest& in, MvRequest& out)
{
    std::cout << "GribVectors::serve in" << std::endl;
    in.print();

    // Get input info
    // Get plotting type info
    bool polar = false;
    if ((const char*)in("TYPE") && strcmp((const char*)in("TYPE"), "POLAR_FIELD") == 0)
        polar = true;

    // Get the two field requests
    MvRequest grib_u;
    MvRequest grib_v;
    if (polar) {
        in.getValue(grib_u, "INTENSITY");
        in.getValue(grib_v, "DIRECTION");
    }
    else {
        in.getValue(grib_u, "U_COMPONENT");
        in.getValue(grib_v, "V_COMPONENT");
    }

    // Get the colouring field request, if it exists
    MvRequest grib_t;
    in.getValue(grib_t, "COLOURING_FIELD");

    // Associate field requests to fieldsets
    MvFieldSet fs_t(grib_t);
    MvFieldSetIterator iter_t(fs_t);
    MvFieldSet fs_u(grib_u);
    MvFieldSetIterator iter_u(fs_u);
    MvFieldSet fs_v(grib_v);
    MvFieldSetIterator iter_v(fs_v);

    // Check consistencies
    if (fs_u.countFields() != fs_v.countFields()) {
        setError(1, "GribVectors-> Error: Incompatible Number of U and V Fields....%d %d", fs_u.countFields(), fs_v.countFields());
        return;
    }

    int nColourFields = fs_t.countFields();
    if (nColourFields > 1 && nColourFields != fs_v.countFields())
        setError(1, "GribVectors-> Error: Incompatible Number of Colour Fields...");

    // Get the colouring field data, if it exists
    const char* strDim = nColourFields ? "3" : "2";
    MvField f_t = iter_t();

    // Main loop
    // Concatenate the field requests in triples: u[i]/v[i]/t[i]
    out.setVerb("GRIB_VECTORS");
    MvFieldSet fs_out;
    MvField f_u, f_v;
    while ((f_u = iter_u()) && (f_v = iter_v())) {
        // Concatenate fields
        fs_out += f_u;
        fs_out += f_v;
        if (f_t)
            fs_out += f_t;

        // Get the next colouring field, if it exists
        if (nColourFields > 1)
            f_t = iter_t();

        // Add dimension info
        out.addValue("GRIB_DIMENSION", strDim);
    }

    out("GRIB_WIND_MODE") = polar ? "SD" : "UV";
    out("GRIB") = fs_out.getRequest();
    out("_CLASS") = "GRIB_VECTORS";
    out("_VERB") = "GRIB_VECTORS";

    std::cout << "GribVectors::serve out" << std::endl;
    out.print();
    return;
}

//--------------------------------------------------------

int main(int argc, char** argv)
{
    MvApplication theApp(argc, argv);

    GribVectors gv("GRIB_VECTORS_APP");

    theApp.run();
}

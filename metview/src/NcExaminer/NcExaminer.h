/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <vector>

#include "MvQMainWindow.h"

class QPlainTextEdit;
class QSplitter;
class QTabWidget;
class QTreeWidget;

class MvNetCDF;
class MvNcVar;
class MvQFileInfoLabel;
class MvQLogPanel;
class PlainTextWidget;

class NcExaminer : public MvQMainWindow
{
    Q_OBJECT

public:
    NcExaminer(QWidget* parent = 0);
    ~NcExaminer();
    bool initMetaData(const std::string&);
    void updateFileInfoLabel();

public slots:
    void slotTabIndexChanged(int);
    void slotShowAboutBox();

private:
    void setupViewActions();
    void setupHelpActions();
    void setupParamBox();
    void setupDumpBox();
    void setupLogArea();

    void showDumpBox();
    void showVariables();
    void getDataValues(MvNcVar*, QStringList&, long&);
    long getNumberValues(MvNcVar*);

    void readSettings();
    void writeSettings();

    template <class T>
    void print_data(std::vector<T>&, QStringList&, long);

    MvNetCDF* data_;
    MvQMainWindow::MenuItemMap menuItems_;
    MvQFileInfoLabel* fileInfoLabel_;

    QAction* actionFileInfo_;
    QSplitter* mainSplitter_;
    QTabWidget* metaTab_;
    QTreeWidget* treeParam_;
    MvQLogPanel* logPanel_;
    QAction* actionLog_;
    QLabel* statusMessageLabel_;
    PlainTextWidget* dumpText_;

    bool initDump_;
};

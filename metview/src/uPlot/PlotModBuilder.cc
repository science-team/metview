/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "PlotModBuilder.h"
#include "PlotPageBuilder.h"
#include "PmContext.h"
#include "ObjectList.h"

//
//  Methods for the PlotModBuilder class
//
PlotModBuilder&
BuilderTraits::DefaultObject()
{
    return PlotPageBuilder::Instance();
}

Cached PlotModBuilder::GetView(PmContext& context)
{
    Cached viewName = context.InRequest()("_VIEW");
    if ((const char*)viewName)
        return viewName;

    viewName = context.FirstDataView();
    if ((const char*)viewName)
        return viewName;

    const char* verb = context.InRequest()("_VERB");
    if (!verb)
        verb = context.InRequest().getVerb();

    viewName = ObjectList::Find("request", verb, "view");
    if ((const char*)viewName) {
        int slen = (const char*)viewName ? strlen(viewName) : 0;
        char* upName = new char[slen + 1];

        for (int i = 0; i < slen; i++)
            upName[i] = toupper(viewName[i]);

        upName[slen] = 0;
        viewName = upName;
        delete[] upName;
    }

    return viewName;
}

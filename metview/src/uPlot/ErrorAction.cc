/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "ErrorAction.h"
#include "PlotMod.h"
#include "MvLog.h"

// This is the exemplar object for the Error Action class
static ErrorAction errorActionInstance("Error");

PlotModAction&
ErrorAction::Instance()
{
    return errorActionInstance;
}

// METHOD : Execute
//
// PURPOSE: Get the error message and forward it to the error handler class
void ErrorAction::Execute(PmContext& context)
{
    // Get error request and message
    MvRequest req = context.InRequest().justOneRequest();
    std::string error_message = (const char*)req("MESSAGE");

    // Send the error message
    MvLog().popup().err() << error_message;

    context.Advance();
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <vector>

#include <QBrush>
#include <QGraphicsItem>
#include <QPen>

#include "MvQPlotItem.h"

#include "Layer.h"

class QTextDocument;

class MgQLayoutItem;
class MgQSceneItem;
class CursorCoordinate;

class MvQCursorData : public MvQPlotItem, public ValuesCollectorVisitor
{
    Q_OBJECT

public:
    enum PositionMode
    {
        Anchored,
        FollowCursor
    };

    MvQCursorData(MgQPlotScene*, MvQPlotView*, QGraphicsItem* parent = nullptr);
    ~MvQCursorData() override = default;

    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;
    QRectF boundingRect() const override;

    void setActivated(bool) override;
    void prepareForReset() override;
    void reset() override;

    void setPositionMode(PositionMode);
    void changePositionMode(const QPointF&);

    void setMagnifierInfo(bool, QPointF magScenePos = QPointF(), float magZoomFactor = 1.);
    void resetPosition();
    void setTrackCoordinates(bool b) {trackCoordinates_=b;}

    void mousePressEventFromView(QMouseEvent*) override;
    void mouseMoveEventFromView(QMouseEvent*) override;
    void mouseReleaseEventFromView(QMouseEvent*) override;

    void setZoomWasPerformedAfterMouseRelease(bool b) { zoomWasPerformedAfterMouseRelease_ = b; }

    void visit(const ValuesCollectorData&) override;
    void visit(const ValuesCollectorUVData&) override;
    void visit(const ValuesCollectorSDData&) override;

signals:
    void coordinatesChanged(const CursorCoordinate&);

private:
    void setCursorPos(const QPointF&, bool forced=false);
    void setText();
    void setText(QString);
    void setText(const CursorCoordinate& coord, QList<ValuesCollector> layerData);
    QString formatNumber(double) const;
    void setData(const CursorCoordinate& coord, MgQLayoutItem* dataLayout);   
    QString unitsString(QString) const;
    bool getCoordinates(const QPointF& scPos, QPointF& coord, MgQLayoutItem** dataLayout);
    void adjustToScenePos(const QPointF& pos, bool changeItemPos);

    QString text_;

    PositionMode positionMode_{FollowCursor};
    QPointF offsetFromCursor_{20, 20};
    QPointF cursorPos_;

    QRectF boundingRect_;
    QTextDocument* textDoc_{nullptr};
    MgQLayoutItem* dataLayout_{nullptr};
    MgQSceneItem* sceneItem_{nullptr};
    bool pointInMagnifier_{false};
    QPointF magnifierScenePos_;
    float magnifierZoomFactor_{1.};
    bool zoomWasPerformedAfterMouseRelease_{true};
    std::vector<float> currentValues_;

    bool trackCoordinates_{false};
};

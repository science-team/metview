/***************************** LICENSE START ***********************************

 Copyright 2021 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QGraphicsItem>
#include <QIcon>
#include <QPainter>
#include <QPixmap>
#include <QPen>

#include "MvQFeatureItem.h"
#include "MvRequest.h"

//---------------------------------
// Marker shapes
//---------------------------------

class MvQFeatureShapeItem : public MvQFeatureItem
{
public:
    MvQFeatureShapeItem(WsType* feature);
    void init(const QJsonObject& d) override;
    //    void toJson(QJsonObject& d) override;
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;
    void resize(QRectF rect) override;
    void resizeTo(const MvQFeatureGeometry& geom) override;
    void moveBy(QPointF delta) override;
    void moveTo(const MvQFeatureGeometry& g) override;
    QString describe() const override;

protected:
    void initAtGeoCoord(QPointF co) override;
    void initAtScenePos(const QPointF&) override;
    void updateRect();
    void adjustBRect() override;
    bool getRequestParameters() override;
    double halo() const override;
    virtual void buildShape() = 0;
    void adjustSizeInReq();
    void initGeometry() override;

    float width_{30.};
    float height_{30.};
    QBrush brush_;
    QPen pen_;
    QPolygonF shape_;
    QPainterPath shapePath_;
};

class MvQFeatureRectItem : public MvQFeatureShapeItem
{
    friend class MvQFeatureMaker<MvQFeatureRectItem>;

protected:
    using MvQFeatureShapeItem::MvQFeatureShapeItem;
    void buildShape() override;
};


class MvQFeatureTriangleItem : public MvQFeatureShapeItem
{
    friend class MvQFeatureMaker<MvQFeatureTriangleItem>;

protected:
    using MvQFeatureShapeItem::MvQFeatureShapeItem;
    void buildShape() override;
};

class MvQFeatureDiamondItem : public MvQFeatureShapeItem
{
    friend class MvQFeatureMaker<MvQFeatureDiamondItem>;

protected:
    using MvQFeatureShapeItem::MvQFeatureShapeItem;
    void buildShape() override;
};

class MvQFeatureStarItem : public MvQFeatureShapeItem
{
    friend class MvQFeatureMaker<MvQFeatureStarItem>;

protected:
    MvQFeatureStarItem(WsType* feature);
    void buildShape() override;
};

class MvQFeatureEllipseItem : public MvQFeatureShapeItem
{
    friend class MvQFeatureMaker<MvQFeatureEllipseItem>;

protected:
    using MvQFeatureShapeItem::MvQFeatureShapeItem;
    void buildShape() override;
};

class MvQFeaturePlacemarkerItem : public MvQFeatureShapeItem
{
    friend class MvQFeatureMaker<MvQFeaturePlacemarkerItem>;

public:
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;

protected:
    MvQFeaturePlacemarkerItem(WsType*);
    void buildShape() override;
    bool getRequestParameters() override;

    bool fillHole_{true};
    QBrush holeBrush_;
    QPainterPath holePath_;
};


//-------------------------------------------------
// Geo shapes (the edges are straight lines,
// not sampled in lat-lon)
//-------------------------------------------------

class MvQFeatureGeoShapeItem : public MvQFeatureItem
{
public:
    void init(const QJsonObject& d) override;
    void toJson(QJsonObject& d) override;
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;
    void resize(QRectF rect) override;
    void resizeTo(const MvQFeatureGeometry& geom) override;
    void moveBy(QPointF delta) override;
    void moveTo(const MvQFeatureGeometry& g) override;
    void reproject(bool init) override;
    bool canBeCreatedOutOfView() const override { return false; }
    bool canBePastedOutOfView() const override { return false; }
    QString describe() const override;

protected:
    MvQFeatureGeoShapeItem(WsType* feature);
    void initAtGeoCoord(QPointF co) override;
    void updateRect();
    void adjustBRect() override;
    bool getRequestParameters() override;
    double halo() const override;
    virtual void reprojectShape() {}
    virtual void buildShape() = 0;
    virtual void adjustShape() {}
    virtual QRectF shapeBoundingRect();

    float width_{100.};
    float height_{100.};
    QBrush brush_;
    QPen pen_;
    QPolygonF shape_;
    QPainterPath shapePath_;
    QList<QPointF> geoCoords_;
};

class MvQFeatureGeoRectItem : public MvQFeatureGeoShapeItem
{
    friend class MvQFeatureMaker<MvQFeatureGeoRectItem>;

protected:
    MvQFeatureGeoRectItem(WsType* feature);
    void reprojectShape() override;
    void buildShape() override;
    void adjustShape() override;
    void updateGeoCoordsFromShape();
};

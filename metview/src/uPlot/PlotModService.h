/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  PlotModService
//
// .AUTHOR:
//  Gilberto Camara, Baudouin Raoult and Fernando Ii
//
// .SUMMARY:
//  Describes the ServiceView class, which deals with
//  application modules
//
// .CLIENTS:
//  DropAction
//
// .RESPONSIBILITY:
//
//  - When receiving a drop or a request in a page associated
//    to a service view, call the service to process the request
//
//  - When the application is finished, pass the request sent
//    by the application to the presentable, which then should
//    perform the data matching
//
//
// .COLLABORATORS:
//  MvTask - communication with METVIEW modules
//
// .ASCENDENT:
//  PlotModView, MvClient
//
// .DESCENDENT:
//  MapView
//
//
#pragma once

#include "PlotModView.h"
#include <MvTask.h>

class PlotModService : public MvClient
{
public:
    // -- Constructors
    PlotModService(const char*, PlotModView*);
    PlotModService(PlotModView*);
    PlotModService(const PlotModService&);

    // -- Destructor
    ~PlotModService() override;

    void ServiceName(const char* name)
    {
        serviceName_ = name;
    }

    void progress(const char* msg) override
    {
        msg_.append(msg);
    }

    // Perform actions after receiving response from service
    void endOfTask(MvTask*) override;

protected:
    std::string serviceName_;
    PlotModView* view_;
    std::string msg_;

    // No copy allowed
    PlotModService& operator=(const PlotModService&);
};

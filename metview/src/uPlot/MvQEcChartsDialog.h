/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include <QDialog>
#include <QLineEdit>

class MvQEcChartsDialog : public QDialog
{
    Q_OBJECT

public:
    MvQEcChartsDialog(int, int, QWidget* parent = nullptr);
    ~MvQEcChartsDialog() override;

    // Get info from the user interface
    std::string getLayerName()
    {
        return leLayerNameSelection_->text().toStdString();
    }

    std::string getOutputFileName()
    {
        return leOutputFileNameSelection_->text().toStdString();
    }

public slots:
    void accept() override;

protected:
    void setVisible(bool b) override
    {
        QDialog::setVisible(b);
    }

    void readSettings();
    void writeSettings();

private:
    QLineEdit* leLayerNameSelection_;
    QLineEdit* leOutputFileNameSelection_;

    std::string layerName_;
    std::string outputFileName_;
};

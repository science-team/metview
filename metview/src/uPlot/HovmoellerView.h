/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  HovmoellerView
//
// .AUTHOR:
//  Fernando Ii
//
// .SUMMARY:
//  Describes the HovmoellerView class, which handles the
//  matching issues related to the Hovmoeller View
//
// .CLIENTS:
//  DropAction
//
// .RESPONSIBILITY:
//
//  - When receiving a drop or a request in a page associated
//    to a service view, call the service to process the request
//
//  - When the application is finished, pass the request sent
//    by the application to the presentable, which then should
//    perform the data matching
//
// .COLLABORATORS:
//  MvRequest - extracts information from the request
//  MvTask - communication with METVIEW modules
//
// .DESCENDENT:
//
// .RELATED:
//  Presentable, SuperPage, Page, DataObject
//
// .ASCENDENT:
// CommonXSectView
//

#pragma once

#include "CommonXSectView.h"

//---------------------------------------------------------------------
// HovmoellerView factory definition
class HovmoellerViewFactory : public PlotModViewFactory
{
    // --  Virtual Constructor - Builds a new HovmoellerView
    PlotModView* Build(Page&, const MvRequest&, const MvRequest&) override;

public:
    // Constructors
    HovmoellerViewFactory() :
        PlotModViewFactory("HovmoellerView") {}
};

//---------------------------------------------------------------------
// HovmoellerView factory definition to handle translation from Metview 3 to 4.
// This should be delete in the future. It is defined here for backwards compatibility.
class HovmoellerViewM3Factory : public PlotModViewFactory
{
    // --  Virtual Constructor - Builds a new HovmoellerView
    PlotModView* Build(Page&, const MvRequest&, const MvRequest&) override;

    // Translate Metview 3 view request to Metview 4
    MvRequest Translate(const MvRequest&);

public:
    // Constructors
    HovmoellerViewM3Factory() :
        PlotModViewFactory("HovmoellerM3View") {}
};

//---------------------------------------------------------------------
// HovmoellerView class definition
class HovmoellerView : public CommonXSectView
{
public:
    // -- Constructors
    HovmoellerView(Page&, const MvRequest&, const MvRequest&);
    HovmoellerView(const HovmoellerView&);
    PlotModView* Clone() const override { return new HovmoellerView(*this); }

    // -- Destructor
    ~HovmoellerView() override = default;

    // -- Methods
    // -- Overriden from CommonXSectView class
    std::string Name() override;

    // Draw the background (axis )
    void DrawBackground() override {}

    // Describe the contents of the view
    void DescribeYourself(ObjectInfo&) override;

    // Initialize some variable members
    virtual void SetVariables(const MvRequest&, bool);

    // Update view
    bool UpdateView() override;
    void UpdateViewWithReq(MvRequest&);

private:
    // No assignment
    HovmoellerView& operator=(const HovmoellerView&);

    void ApplicationInfo(const MvRequest&) override;

    void updateDates(const MvRequest& in);

    // Update View according to the Hovmoeller type
    void UpdateViewLine(MvRequest&);
    void UpdateViewArea(MvRequest&);
    void UpdateViewVert(MvRequest&);

    // Variable members
    std::string type_;     // Line, Area, Vertical
    std::string dateMin_;  // date 1 current used in the axis
    std::string dateMax_;  // date 2 current used in the axis
    bool bAutoDate_;       // true: dates min&max have a default value 'automatic'
    bool swapAxes_;        // true: vertical axis is geoline
    bool bDataUnit_;       // true: there is a DU to be plotted (e.g. plot is not
                           //       empty). If there is no DU, default Date values
                           //       must be assigned to dateMin_/dateMax_ because
                           //       Magics do not accept value AUTOMATIC.
    bool bUpdateDate_;     // true: parameter Date needs to be updated
};

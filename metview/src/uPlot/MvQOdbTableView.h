/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QGraphicsView>
#include <QGraphicsScene>

class MvQOdbMetaData;

class MvQOdbTableView : public QGraphicsView
{
    Q_OBJECT

public:
    MvQOdbTableView(QWidget* parent = nullptr);
    void init(MvQOdbMetaData*, QString);
    void renderTableGraph();
    void centreTableGraph();
    void setSelectedTable(QString);

signals:
    void tableSelection(QString);

public slots:
    void mousePressedInTableGraph(int, int);

protected:
    void getTablePos(int, int, int&, int&);
    bool setSelectedTable(int, int);
    void resizeEvent(QResizeEvent*) override;
    void adjustPixmapSizeToView(int, int, int&, int&);

    MvQOdbMetaData* odb_{nullptr};

    int tableOffsetX_{15};
    int tableOffsetY_{15};
    int tableWidth_{70};
    int tableHeight_{15};
    int tableGapX_{12};
    int tableGapY_{12};
    int graphWidth_{70};
    int graphHeight_{15};

    QString selectedTable_;

    QPixmap* tablePixmap_{nullptr};
};


class OdbTableScene : public QGraphicsScene
{
    Q_OBJECT

public:
    OdbTableScene(QGraphicsScene* parent = nullptr);

signals:
    void mousePressed(int, int);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent*) override;
};

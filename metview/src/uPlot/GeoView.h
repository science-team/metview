/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  GeoView
//
// .AUTHOR:
//  Fernando Ii
//
// .SUMMARY:
//  Describes the GeoView class, which handles the
//  matching issues related to the Geo View. This class is based on the
//  Map View class.
//
//
// .CLIENTS:
//  DropAction
//
// .RESPONSIBILITY:
//
//  - Process the drop, asking the
//
//
// .COLLABORATORS:
//  MvRequest - extracts information from the request
//
// .DESCENDENT:
//
// .RELATED:
//  Presentable, SuperPage, Page, DataObject
//
// .ASCENDENT:
// PlotModView, PlotModService
//

#pragma once

#include "PlotModView.h"
#include "PlotModService.h"

class GeoView : public PlotModView, public PlotModService
{
public:
    // Constructors
    GeoView(Page&, const MvRequest&, const MvRequest&);
    GeoView(const GeoView&);
    PlotModView* Clone() const override { return new GeoView(*this); }

    // Destructor
    ~GeoView() {}

    // Get the object name
    std::string Name() override;

    //  static void SaveRequest ( const Cached& path, MvRequest& viewRequest );
    // save a request for later use

    // Methods Overriden from PlotModView class
    void Drop(PmContext&) override;

    // Insert a new data request into the page hierarchy;
    void InsertDataRequest(MvRequest& dropRequest) override;

    // Updates the current view
    void UpdateViewWithReq(MvRequest& viewRequest);

    // Replaces the current geographical area
    //  virtual void ReplaceArea ( const Location& coordinates, int izoom=-1 );

    // Draw the background (coastlines)
    void DrawBackground() override;

    // Draw the foreground (coastlines with land-sea shade)
    void DrawForeground() override;

    // Draw all layers
    void Draw(SubPage*) override;

    // Describe the contents of the view
    void DescribeYourself(ObjectInfo&) override;

    // Decode the data Unit
    void DecodeDataUnit(MvIcon& dataUnit) override;

    // Call an application
    bool CallService(const MvRequest&, PmContext&) override;

    //  virtual Cached InputMode () { return Cached ("MAP"); }
    // Input mode is MAP

    //  virtual bool CanChangeGeography() { return true; }
    // Allow to change geography coordinates

protected:
    //  virtual int CheckPSymb(MvRequest&);
    // Find, and if necessary, add a psymb. Return it's id.

    //  virtual void DescribeSubrequest ( ObjectInfo& description, MvRequest& request, const Cached& name, const Cached& verb);

    //  void ConvertToSatellite ( MvRequest& decoderRequest );

private:
    // No assignment
    GeoView& operator=(const GeoView&);

    // Update view request
    void UpdateView(const MvRequest&);

    // Functions to handle GeoLayers

    // Update Coastlines request
    void UpdateCoastlines(MvRequest&);

    // Update View GeoLayers request, making sure that the _STACKING_ORDER parameter is set.
    void UpdateViewRequestGeoLayers(MvRequest&);

    // Check if all the mandatory GeoLayers are defined. If not, add them.
    bool CheckAddGeoLayers(MvRequest&);

    // Get geolayers as a list of requests. The input geolayers is taken either from
    // the View or is given as a parameter in the function.
    bool GetGeoLayersList(MvRequest&);
    bool GetGeoLayersList(MvRequest&, MvRequest&);

    // Get GeoLayers user default request. The GeoCoast and GeoGrid subrequests are provided,
    // if they are not customised by the user.
    void GeoLayersDefaultRequest(MvRequest&);

    // Insert/replace GeoLayers to the View and DB
    // bool InsertGeoLayers ( MvRequest&, int = -1 );

    // Insert GeoLayers to the View and DB
    // void InsertGeoLayers ( int = -1 );

    // Insert GeoLayers to the databasse
    // void InsertGeoLayersDB ( int = -1 );
    // void InsertGeoLayersDB ( MvRequest&, int = -1 );

    // Delete GeoLayers from the database
    // void RemoveGeoLayersDB();

    // Retrieve GeoLayers from the DB
    // bool RetrieveGeoLayersDB ( MvIconList& );

    // Decode GeoLayers default requests. The requests were obtained from either the
    // GeoLayers automatic default icon generation or the icon instance stored at the
    // System/Defaults directory. In the first case, the request is empty, so the
    // Coastlines and Grid default requests need to be created. In the second case,
    // Metview uses the MARS function 'read_request_file' to read the icon info from disk.
    // Because this function does not expand the requests, it only points to the icon
    // filename, an extra code was added.
    void DecodeGeoLayers(const MvRequest&, MvRequest&);
};

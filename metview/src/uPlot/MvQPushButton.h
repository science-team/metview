/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QPushButton>
#include <QStyleOptionButton>

class MvQPushButton : public QPushButton
{
public:
    MvQPushButton(QWidget* parent = nullptr);
    MvQPushButton(const QString& text, QWidget* parent = nullptr);
    MvQPushButton(const QIcon& icon, const QString& text, QWidget* parent = nullptr);
    ~MvQPushButton() override;

    Qt::Orientation orientation() const;
    void setOrientation(Qt::Orientation orientation);

    bool mirrored() const;
    void setMirrored(bool mirrored);

    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;

protected:
    void paintEvent(QPaintEvent* event) override;

private:
    QStyleOptionButton getStyleOption() const;
    void init();

    Qt::Orientation orientation_;
    bool mirrored_;
};

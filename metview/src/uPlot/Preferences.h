/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  Preferences
//
// .AUTHOR:
//  Gilberto Camara, Baudouin Raoult and Fernando Ii
//
// .SUMMARY:
//  Defines a class for handling interfaces preferences
//
//
// .CLIENTS:
//  SuperPage
//
// .RESPONSABILITIES:
//  - Keep track of users' preferences
//  - Inform these preferences to the user
//
//
// .COLLABORATORS:
//
//
//
// .BASE CLASS:
//
//
// .DERIVED CLASSES:
//
//
// .REFERENCES:
//
//
#pragma once

#include "Presentable.h"

class Preferences
{
public:
    // Constructors
    Preferences(Presentable& owner);
    Preferences(const Preferences&);

    // Destructor
    ~Preferences() = default;

    // Class Members
    void Owner(Presentable& xx) { owner_ = &xx; }
    Presentable& Owner() const { return *owner_; }

    // Class functions
    static MvRequest CoastRequest();


private:
    Preferences& operator=(const Preferences&) { return *this; }

    Presentable* owner_;
};

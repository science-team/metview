/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "PlotMod.h"

class uPlotBase;

class PlotModInteractive : public PlotMod
{
public:
    PlotModInteractive();

    void setPlotApplication(uPlotBase* pa) { plotApplication_ = pa; }
    std::string currentWorkDir() const override;
    void addWeatherSymbol(const MvRequest&) override;

private:
    uPlotBase* plotApplication_{nullptr};
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QFileDialog>
#include <QMap>

class QComboBox;


class MvQVideoDialog : public QFileDialog
{
    Q_OBJECT

public:
    MvQVideoDialog(QWidget* parent = nullptr);
    ~MvQVideoDialog() override;

signals:
    void sendInfo(QString, QString);

public slots:
    void accept() override;
    void reject() override;

private:
    QComboBox* formatCombo_;
    QMap<QString, QString> formats_;
};

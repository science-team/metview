/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QBrush>
#include <QGraphicsItem>
#include <QPen>

#include "MvQPlotItem.h"

class MgQLayoutItem;

class MvQAreaSelection : public MvQPlotItem
{
    Q_OBJECT

public:
    MvQAreaSelection(MgQPlotScene*, MvQPlotView*, QGraphicsItem* parent = nullptr);
    ~MvQAreaSelection() override;

    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;
    QRectF boundingRect() const override;

    void setArea(double, double, double, double);
    void clearArea();
    void selectAllArea();

    void setActivated(bool) override;

    void mousePressEventFromView(QMouseEvent*) override;
    void mouseMoveEventFromView(QMouseEvent*) override;
    void mouseReleaseEventFromView(QMouseEvent*) override;
    void reset() override;

signals:
    void areaIsDefined(double, double, double, double);
    void areaIsUndefined();

private:
    enum CurrentAction
    {
        NoAction,
        DefineAction,
        ResizeAction,
        MoveAction
    };

    void updateHandles();
    QRectF updateControlRect(CurrentAction, QPointF&);
    void updateItemByControlRect(QRectF);
    QRectF controlRect();
    void getAreaGeoCorners(QPointF&, QPointF&);
    void checkHandleSize() const;

    QPen pen_;
    QBrush brush_;
    QRectF rect_;
    QPointF zoomRectOrigin_;
    QList<QPointF> scenePoint_;
    QList<QPointF> coordPoint_;
    CurrentAction currentAction_;
    MgQLayoutItem* zoomLayout_;
    QPointF dragPos_;

    bool showHandles_;
    QPen selectionPen_;
    mutable QList<QRectF> handle_;
    mutable float handleSize_;
    mutable float physicalHandleSize_;
    int resizeId_;
    int handleHover_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  Canvas
//
// .AUTHOR:
//  Gilberto Camara, Baudouin Raoult and Fernando Ii
//
// .SUMMARY:
//  A  base class for access to drawing areas
//
// .CLIENTS:
//  Presentable, SuperPage, Page, Subpage, DataObject
//
//
// .RESPONSABILITIES:
//  Provides an interface, which, in combination
//  with multiple inheritance of its derived classes, is able
//  to support both X-Window and Postscript drawing areas
//
//
// .COLLABORATORS:
//  Graphics Engine, Device
//
//
// .BASE CLASS:
//
//
// .DERIVED CLASSES:
//  XCanvas, PSCanvas
//
// .REFERENCES:
//
//
#pragma once

#include <Rectangle.h>

#include "PaperSize.h"
#include "Location.h"
#include "Device.h"

class SubPage;

enum
{
    RMALL,
    RMMATCH,
    RMNONE
};

class Canvas
{
public:
    // Contructors
    Canvas(Device& device, const Rectangle& rect, const PaperSize& size, int presentableId);

    // Destructor
    virtual ~Canvas();  // base class

    virtual bool HasSegment(int, const std::string&)
    {
        return false;
    }

    virtual bool RemoveForAll(int, const std::string&)
    {
        return false;
    }

    // Dealing with drawing requests
    //	virtual void InitTitleDraw ( int, Presentable* ) {}
    //	virtual void EndDraw ( Presentable* ) {}

    //	double AspectRatio()
    //		{ return aspectRatio_; }

    double GetWidth()
    {
        return size_.GetWidth();
    }

    double GetHeight()
    {
        return size_.GetHeight();
    }

    virtual void PresentableId(int presentableId)
    {
        presentableId_ = presentableId;
    }

    int PresentableId()
    {
        return presentableId_;
    }

    virtual void RemovePresentableId(int, int = RMMATCH) {}

    //        virtual void MovePresentable(SubPage*,int ) {}

    virtual void ShowPresentableId(int) {}
    virtual int ShowPresentableId()
    {
        return -1;
    }

    const Device& GetDevice() const
    {
        return myDevice_;
    }  // pointer to the device

    virtual MvRequest PrinterRequest();

protected:
    // Members
    Device& myDevice_;  // pointer to the device

    double aspectRatio_;  // aspectRatio must be preserved

    Rectangle rect_;  // size of the canvas

    PaperSize size_;

    Location drawingAreaCoord_;  // coordinates on paper of
                                 // effective drawing area

    int presentableId_;  // id of the presentable associated to the canvas

private:
    // No copy allowed
    Canvas(const Canvas&);
    Canvas& operator=(const Canvas&) { return *this; }
};

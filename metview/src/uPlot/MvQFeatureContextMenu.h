/***************************** LICENSE START ***********************************

 Copyright 2020 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <vector>

#include <QPoint>
#include <QShortcut>
#include <QString>
#include <QStringList>
#include <QWidget>

class MvRequest;
class MvQFeatureCommandTarget;

class MvQFeatureMenuItem
{
public:
    MvQFeatureMenuItem(bool node, QString name, QString label);
    MvQFeatureMenuItem(const MvQFeatureMenuItem&) = delete;
    MvQFeatureMenuItem& operator=(const MvQFeatureMenuItem&) = delete;
    ~MvQFeatureMenuItem() = default;

    static void add(const MvRequest& r);
    static void setupShortcut(QObject* receiver, QWidget* w, const std::string& view);
    static QString execute(const MvQFeatureCommandTarget& sel, QList<QAction*> extraActions, QPoint globalPos);
    static void init();

private:
    QShortcut* createShortcut(QWidget* parent, const std::string& view);

    bool node_{false};
    QString name_;
    QString label_;
    QString type_;
    QString parent_;
    QStringList visible_for_;
    QStringList not_visible_for_;
    QString shortcut_;

    static std::vector<MvQFeatureMenuItem*> items_;
    static bool shortcutInited_;
};

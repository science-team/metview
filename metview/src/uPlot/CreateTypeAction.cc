/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "CreateTypeAction.h"
#include "PlotMod.h"
#include "MvLog.h"

// This is the exemplar object for the Type Action class
static CreateTypeAction createTypeActionInstance("CreateType");

PlotModAction&
CreateTypeAction::Instance()
{
    return createTypeActionInstance;
}

// METHOD : Execute
//
// PURPOSE: Build a new request based on the TYPE parameter.
//          Skip the current request and add the new one.

void CreateTypeAction::Execute(PmContext& context)
{
    // Create a request based on the TYPE parameter
    MvRequest req = context.InRequest();

    const char* type = req("TYPE");
    if (!type) {
        MvLog().popup().err() << "uPlot CreateTypeAction-> Context Request does not have parameter TYPE";
        return;
    }

    MvRequest typeReq = req.justOneRequest();
    typeReq.setVerb(type);
    typeReq.unsetParam("TYPE");
    typeReq("_CLASS") = type;
    typeReq("_VERB") = type;
    req.advance();

    // It will skip all the previous requests and only save the
    // remaining ones.
    while (req) {
        typeReq = typeReq + req.justOneRequest();
        req.advance();
    }

    context.Advance();        // Skip this request
    context.Update(typeReq);  // Update context
    typeReq.print();
}

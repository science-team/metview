/***************************** LICENSE START ***********************************

 Copyright 2020 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  ThermoView
//
// .SUMMARY:
//  Describes the ThermoView class, which handles the
//  matching issues related to the Thermo View
//
// .CLIENTS:
//  DropAction
//
// .RESPONSIBILITY:
//
//  - When receiving a drop or a request in a page associated
//    to a service view, call the service to process the request
//
//  - When the application is finished, pass the request sent
//    by the application to the presentable, which then should
//    perform the data matching
//
// .COLLABORATORS:
//  MvRequest - extracts information from the request
//  MvTask    - communication with METVIEW modules
//
// .DESCENDENT:
//
// .RELATED:
//  Presentable, SuperPage, Page, DataObject
//
// .ASCENDENT:
// CommonXSectView
//

#pragma once

#include "CommonXSectView.h"

//---------------------------------------------------------------------
// ThermoView factory definition
class ThermoViewFactory : public PlotModViewFactory
{
    // --  Virtual Constructor - Builds a new ThermoView
    PlotModView* Build(Page&, const MvRequest&, const MvRequest&) override;

public:
    ThermoViewFactory() :
        PlotModViewFactory("ThermoView") {}
};

//---------------------------------------------------------------------
// ThermoView class definition
class ThermoView : public CommonXSectView
{
public:
    // -- Constructors
    ThermoView(Page&, const MvRequest&, const MvRequest&);
    ThermoView(const ThermoView&);
    PlotModView* Clone() const override { return new ThermoView(*this); }

    ~ThermoView() override = default;

    std::string Name() override;
    void DrawBackground() override {}
    void DrawForeground() override;
    void DescribeYourself(ObjectInfo&) override;
    void SetVariables(const MvRequest&, bool);

    bool UpdateView() override;
    bool UpdateViewWithReq(MvRequest&);
    void updateThermoGrid(const MvRequest&);
    void Drop(PmContext&) override;
    bool ConsistencyCheckWithStr(MvRequest&, MvRequest&, std::string&);

private:
    // No assignment
    ThermoView& operator=(const ThermoView&);

    void ApplicationInfo(const MvRequest&) override;
    bool SetThermoType(MvRequest&);
    bool isThermoGridForeground() const;

    std::string type_;  // Tephigram, SkewT, Emagram
    MvRequest thermoGridReq_;
};

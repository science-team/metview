/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  AverageView
//
// .AUTHOR:
//  Geir Austad
//      Modified by Fernando Ii 03-2012
//
// .SUMMARY:
//  Describes the AverageView class, which handles the
//  matching issues related to the Average View
//
//
// .CLIENTS:
//  DropAction
//
// .RESPONSIBILITY:
//
////  - When receiving a drop or a request in a page associated
//    to a service view, call the service to process the request
//
//  - When the application is finished, pass the request sent
//    by the application to the presentable, which then should
//    perform the data matching
//
//
// .COLLABORATORS:
//  MvRequest - extracts information from the request
//  MvTask - communication with METVIEW modules
//
// .DESCENDENT:
//
// .RELATED:
//  Presentable, SuperPage, Page, DataObject
//
// .ASCENDENT:
// CommonXSectView
//

#pragma once

#include "CommonXSectView.h"

//-------------------------------------------------------------------
// Average factory definition
class AverageViewFactory : public PlotModViewFactory
{
    // --  Virtual Constructor - Builds a new AverageView
    PlotModView* Build(Page&, const MvRequest&, const MvRequest&) override;

public:
    // Constructors
    AverageViewFactory() :
        PlotModViewFactory("AverageView") {}
};

//---------------------------------------------------------------------
// Average factory definition to handle translation from Metview 3 to 4.
// This should be delete in the future. It is defined here for backwards
// compatibility.
class AverageViewM3Factory : public PlotModViewFactory
{
    // --  Virtual Constructor - Builds a new AverageView
    PlotModView* Build(Page&, const MvRequest&, const MvRequest&) override;

    // Translate Metview 3 view request to Metview 4
    MvRequest Translate(const MvRequest&);

public:
    // Constructors
    AverageViewM3Factory() :
        PlotModViewFactory("AverageM3View") {}
};

//---------------------------------------------------------------------
// Average class definition
class AverageView : public CommonXSectView
{
public:
    // -- Constructors
    AverageView(Page&, const MvRequest&, const MvRequest&);
    AverageView(const AverageView&) = default;
    PlotModView* Clone() const override { return new AverageView(*this); }

    // -- Destructor
    ~AverageView() override = default;

    // -- Methods
    // -- Overriden from CommonXSectView class
    std::string Name() override;

    // Draw the background (axis )
    void DrawBackground() override {}

    // Describe the contents of the view
    void DescribeYourself(ObjectInfo&) override;

    // Initialize some variable members
    virtual void SetVariables(const MvRequest&, bool);

    // Update view
    bool UpdateView() override;

    // Check consistency between the View and Data Module
    bool ConsistencyCheck(MvRequest&, MvRequest&) override;

private:
    // Save some data specific to some DataApplication
    void ApplicationInfo(const MvRequest&) override;

    // No assignment
    AverageView& operator=(const AverageView&);

    // Variables members
    std::string axisType_;  // X axis type: "latitude"/"longitude"
    std::string yReverse_;  // Y axis direction: "on"/"off"
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// File PlotModBuilder
// Gilberto Camara - ECMWF Apr 97
//
// .NAME:
//  Builder
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Describes the builder class, an abstract class for
//  building the PlotMod page hierarchy from a request
//
// .DESCRIPTION:
//  This class is based on the "Builder" pattern (see
//  "Design Patterns" book, page 97).
//
//  The idea is to define a base class for all possible instances
//  of requests which create a plot window hierarchy from a request
//  Details of internal structure of the objects being created are
//  hidden from client applications which then can invoke the
//  appropriate builder.
//
//  There are three types of specialised builders in PlotMod:
//
//  - SuperPageBuilder, which builds a page hierarcy based on
//    a "SUPERPAGE" request
//
//  - PlotWindowBuilder, which builds a page hierarchy based on
//    a "PLOT" request
//
//  - GribBuilder, which builds a page hierarchy based on a
//    "GRIB" request
//
//
//  These builders will be concrete classes derived from the
//  Builder abstract (base) class
//
// .DESCENDENT:
//  SuperPageBuilder, PlotWindowBuilder, GribBuilder
//
// .RELATED:
//  Presentable, SuperPage, Page, DataObject
//
// .ASCENDENT:
//
//
#pragma once

#include "Prototype.hpp"

class Presentable;
class PlotModBuilder;
class PmContext;

struct BuilderTraits
{
    using Type = PlotModBuilder;
    static Type& DefaultObject();
};

class PlotModBuilder : public Prototype<BuilderTraits>
{
public:
    // Contructors
    PlotModBuilder(const Cached& builderName) :
        Prototype<BuilderTraits>(builderName, this) {}

    // Destructor
    ~PlotModBuilder() override = default;

    // Methods
    virtual Presentable* Execute(PmContext&) = 0;
    virtual Cached GetView(PmContext&);

    // Friends
    // friend std::ostream& operator<<(std::ostream& ostr, const PlotModBuilder& bld)
    //{ return ostr << bld.Name(); }
};

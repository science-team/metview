/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "PlotModTask.h"

PlotModTask::PlotModTask(MvClient* client,
                         PmContext& context,
                         const Cached& serviceName,
                         const MvRequest& outRequest) :
    MvServiceTask(client, serviceName, outRequest),
    context_(context)
{
    context_.Attach();  // increment the refence counting
                        // in order to GUARANTEE that the
                        //  context will still exist when the
                        //  service module responds
}

// -- Destructor
PlotModTask::~PlotModTask()
{
    context_.Detach();  // decrement the reference counting
}

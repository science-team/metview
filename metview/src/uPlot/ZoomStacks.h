/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// .NAME:
//  ZoomStacks
//
// .AUTHOR:
//  Laercio M, Namikawa and Lubia Vinhas
//  Modified by Fernando Ii to adapt to uPlot environment
//
// .SUMMARY:
//  A class for handling stacks with window coordinates
//  associated with X Window System. It stores world
//  and geodetic coordinates and performs Undo and Redo
//  operations
//
//
// .CLIENTS:
//  XCanvas, OpenGLCanvas
//
//
// .RESPONSABILITIES:
//  Handle coordinates stacks for drawing areas
//
//
// .COLLABORATORS:
//
//
// .DERIVED CLASSES:
//
//
// .REFERENCES:
//
//
#pragma once

#include <string>
#include <vector>

class ZoomStacks
{
public:
    // Contructors
    ZoomStacks();

    // Destructor
    ~ZoomStacks();

    // Methods
    void ClearAll();

    // Insert a new element
    void GeodeticPush(const std::string&);

    // Stack size
    int Size()
    {
        return zoomGeodeticCoordStack_.size();
    }

    // Set/Get current zoom position
    void Current(int index)
    {
        zoomIndex_ = index;
    }

    int Current()
    {
        return zoomIndex_;
    }

    // Get an element
    std::string Get(int ipos);

protected:
    // Methods ( local to the class )
    std::vector<std::string> zoomGeodeticCoordStack_;
    int zoomIndex_;  // 0 - original area, 1 - first zoom, ...

private:
    // No copy allowed
    ZoomStacks(const ZoomStacks&);
    ZoomStacks& operator=(const ZoomStacks&) { return *this; }
};

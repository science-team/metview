/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  ErrorAction
//
// .AUTHOR:
//  Fernando Ii
//
// .SUMMARY:
//  Describes the ErrorAction class, a derived class
//  for the various types of requests in PlotMod which indicate
//  that an error occurs from a called module.
//
// .CLIENTS:
//  PlotModAction class, called by PlotMod main module (PlotMod.cc)
//
// .RESPONSIBILITY:
//  Will handle the error message.
//
// .COLLABORATORS:
//
// .ASCENDENT:
//  PlotModAction
//
// .DESCENDENT:
//
// .REFERENCES:
//

#pragma once
#include "PlotModAction.h"

class ErrorAction : public PlotModAction
{
public:
    // Constructors
    ErrorAction(const std::string& name) :
        PlotModAction(name.c_str()) {}

    // Destructor
    ~ErrorAction() override = default;

    // Methods
    static PlotModAction& Instance();

    // Overriden from Base Class (PlotModAction)
    void Execute(PmContext& context) override;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "PmIndividualProjection.h"

// =============================================================
// class PmCartesianViewProjectionFactory - builds PmCartesianView projection
class PmCartesianViewProjectionFactory : public PmProjectionFactory
{
    PmProjection* Build(const MvRequest& reqst) override
    {
        return new PmCartesianView(reqst);
    }

public:
    PmCartesianViewProjectionFactory() :
        PmProjectionFactory("CARTESIANVIEW") {}
};

// Instance which is loaded in the "factory"
static PmCartesianViewProjectionFactory cartesianViewProjectionFactoryInstance;

// ====================================================================
//
// PmCartesianView projection

PmCartesianView::PmCartesianView(const MvRequest& viewRequest) :
    PmProjection(viewRequest)
{
    this->CalculateProjectionCoord();
}

// GEODETIC TO CARTESIANVIEW COORDINATES
Point PmCartesianView::LL2PC(Point&)
{
    // Add code later
    return {0., 0.};
}

// CARTESIANVIEW TO GEODETIC COORDINATES
Point PmCartesianView ::PC2LL(Point&)
{
    // Add code later
    return {0., 0.};
}

double
PmCartesianView::CheckOriginLongitude()
{
    // Add code later
    return 0.;
}

bool PmCartesianView::CheckGeodeticCoordinates(Location&)
{
    // Add code later
    return true;
}

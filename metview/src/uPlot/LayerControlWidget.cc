/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "LayerControlWidget.h"

#include <QStackedWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include "MvQLayerWidget.h"
#include "MvQDataWidget.h"

LayerControlWidget::LayerControlWidget(MvQLayerWidget* layerListW,
                                       MvQDataWidget* layerInfoW,
                                       QWidget* parent) :
    QWidget(parent),
    layerListW_(layerListW),
    layerInfoW_(layerInfoW)
{
    auto* vb = new QVBoxLayout(this);
    vb->setContentsMargins(0, 0, 0, 0);
    vb->setSpacing(0);

    stackedW_ = new QStackedWidget(this);
    vb->addWidget(stackedW_, 1);

    stackedW_->addWidget(layerListW_);
    Q_ASSERT(ListViewIndex == stackedW_->count() - 1);

    stackedW_->addWidget(layerInfoW_);
    Q_ASSERT(InfoViewIndex == stackedW_->count() - 1);

    connect(layerInfoW_, SIGNAL(layerListRequested()),
            this, SLOT(showLayerList()));

    connect(layerListW_, SIGNAL(layerInfoRequested(MgQLayerItem*)),
            this, SLOT(showLayerInfo(MgQLayerItem*)));
}

void LayerControlWidget::showLayerList()
{
    stackedW_->setCurrentIndex(ListViewIndex);
}


void LayerControlWidget::showLayerInfo(MgQLayerItem* layer)
{
    if (layer) {
        layerInfoW_->setLayer(layer);
        stackedW_->setCurrentIndex(InfoViewIndex);
    }
}

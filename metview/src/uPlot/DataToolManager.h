/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

/************************************
  Application DataToolManager

  Manager the tools to analyse data
*************************************/

#include "Metview.h"
#include <MvTask.h>

class DataToolManager : public MvService
{
public:
    // Constructor
    DataToolManager(const char* kw);

    // Destructor
    ~DataToolManager() override = default;

    void serve(MvRequest&, MvRequest&) override;

protected:
};


class DataToolService : public MvClient
{
public:
    // Access to singleton object
    static DataToolService& Instance();

    // Destructor
    ~DataToolService();

    // Methods

    // Perform actions after receiving response from the data tools
    void endOfTask(MvTask*) override;

    // Call data tiik
    void CallDataTool(MvRequest& in, MvRequest& out);

private:
    // Constructors
    DataToolService();  // no external access point
};

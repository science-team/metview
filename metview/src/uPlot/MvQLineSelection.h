/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QBrush>
#include <QGraphicsItem>
#include <QPen>

#include "MvQPlotItem.h"

class MgQLayoutItem;

class MvQLineSelection : public MvQPlotItem
{
    Q_OBJECT

public:
    MvQLineSelection(MgQPlotScene*, MvQPlotView*, QGraphicsItem* parent = nullptr);
    ~MvQLineSelection() override;

    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;
    QRectF boundingRect() const override;

    void setLine(double, double, double, double);
    void clearLine();

    void setActivated(bool) override;

    void mousePressEventFromView(QMouseEvent*) override;
    void mouseMoveEventFromView(QMouseEvent*) override;
    void mouseReleaseEventFromView(QMouseEvent*) override;
    void reset() override;
    void changeDirection();

signals:
    void lineIsDefined(double, double, double, double);
    void lineIsUndefined();

protected:
    enum CurrentAction
    {
        NoAction,
        DefineAction,
        ResizeAction,
        MoveAction
    };

    void updateHandles();
    void checkEndPoint(QPointF&);
    void checkMoveDelta(QPointF&);
    bool checkPointInLineArea(QPointF&);
    void checkHandleSize() const;

    QPen pen_;
    QBrush brush_;
    QLineF line_;
    QPointF lineOrigin_;
    QList<QPointF> scenePoint_;
    QList<QPointF> coordPoint_;
    CurrentAction currentAction_;
    MgQLayoutItem* zoomLayout_;
    QPointF dragPos_;

    bool showHandles_;
    QPen selectionPen_;
    mutable QList<QRectF> handle_;
    mutable float handleSize_;
    mutable float physicalHandleSize_;
    int resizeId_;
    int handleHover_;

    mutable float arrowSize_;
    QBrush arrowBrush_;
    QPen arrowPen_;
};

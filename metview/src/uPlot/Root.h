/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  Root
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Defines a concrete Root class for the page hierarchy in PlotMod.
//  The Root class is an example a Component class
//  of the Composite "pattern".
//
//  It is also implemented as a Singleton, since there is only one
//  copy of the Root object available to the clients
//
// .DESCRIPTION:
//
//  The objects of this class lie at the root of the page hierarchy
//  in PlotMod, and provide the defaults for all actions related
//  to visualisation and plotting
//
//  The Root object contains a copy of METVIEW's Object List and
//  a map which associates each DataUnit to its default VisDef
//
//  The ObjectList is an object which is read
//  from the METVIEW general description file
//  (located in $METVIEW_DIR/etc/ObjectList).
//
//  The DataUnitVisDefMap is an associative map
//  which contains, for each class of DataUnit,
//  the corresponding VisDef class.
//
//
// .DESCENDENT:
//
//
// .RELATED:
//  PlotMod, DataObject, SuperPage,
//  Builder, Visitor, Device, DataUnit, VisDef
//
// .ASCENDENT:
//  Presentable
//
#pragma once

#include "Presentable.h"

class Root : public Presentable
{
public:
    // Access Point to the Root's single instance
    static Root& Instance();

    // Create the instance
    static void Create()
    {
        if (rootInstance_ == nullptr)
            rootInstance_ = new Root();
    }

    // Destructor
    ~Root() override = default;

    // Cleanup function
    static void ServerDead(int, void*);

    // Methods
    // Overriden from Presentable class
    // Returns the default visdef list for the data unit or visdef name
    bool DefaultVisDefList(const char*, MvIconList&, int ndimflag = 0, int type = GETBYDATAUNIT) override;

    // Returns the default text list
    bool RetrieveTextTitle(MvIconList&) override;

    // Returns the default legend
    bool RetrieveLegend(MvIcon&) override;

    // There is no default import list.
    // If reaches here returns false.
    bool RetrieveImportList(MvIconList&) override { return false; }

    // Returns the superpage
    Presentable* FindSuperPage(int index = -1) override;

    // Clean the structure and release memory
    void Clean();

    // Notify the server to draw me later
    void AddDrawTask();

    // Check if there is a task to be performed
    bool CheckDrawTask() override;

    // Ask the server to draw me
    void DrawTask() override;

    // Draw the presentable
    void DrawTrailer() override;

protected:
    // Constructors
    Root();

private:
    static Root* rootInstance_;
    Root(const Root&);
    Root& operator=(const Root&);
};

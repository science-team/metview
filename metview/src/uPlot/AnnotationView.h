/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  AnnotationView
//
// .AUTHOR:
//  Fernando Ii
//
// .SUMMARY:
//  Describes the AnnotationView class, which plots text and images
//
// .CLIENTS:
//  Page
//
// .RESPONSIBILITY:
//
// .COLLABORATORS:
//
// .ASCENDENT:
//  PlotModView, PlotModTable
//
// .DESCENDENT:
//
//
//
#pragma once

#include "PlotModView.h"

class AnnotationView : public PlotModView
{
public:
    // -- Constructors
    AnnotationView(Page&, const MvRequest&, const MvRequest&,
                   const std::string& viewName = "annotationview");

    AnnotationView& operator=(const AnnotationView&) = delete;
    PlotModView* Clone() const override { return new AnnotationView(*this); }

    // -- Destructor
    ~AnnotationView() override = default;

    // --  Methods overriden from PlotModView class
    std::string Name() override;

    // Update the current view
    bool UpdateView() override;

    // Decode the data Unit
    void DecodeDataUnit(MvIcon&) override {}

    // Draw the background
    void DrawBackground() override {}
    // virtual bool EmptyShared() { return false; }

    // Draw the foreground
    void DrawForeground() override {}

    // Describe the contents of the view
    void DescribeYourself(ObjectInfo&) override;

    // Process the drop request
    void Drop(PmContext& context) override;

    MvIconList InsertDataRequest(MvRequest&) override { return {}; }

private:
    std::string viewName_;
};

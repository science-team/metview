/***************************** LICENSE START ***********************************

 Copyright 2023 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

#include <QLabel>
#include <QSettings>

class CursorCoordinate;
class QAction;
class MvQPlotView;

namespace metview {
namespace uplot {

class LocationLabelWidget : public QLabel
{
Q_OBJECT
public:
    LocationLabelWidget(QWidget*, MvQPlotView* view);
    QAction* viewAction() const {return viewAction_;}
    void writeSettings(QSettings& settings);
    void readSettings(QSettings& settings);

public slots:
    void setLocation(const CursorCoordinate&);

protected slots:
     void showLabel(bool st);

private:
    MvQPlotView* view_{nullptr};
    QAction* viewAction_{nullptr};
};

} // uplot
} // metview

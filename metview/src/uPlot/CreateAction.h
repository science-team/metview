/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// .NAME:
//  CreateAction
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Describes the CreateAction class, a derived class
//  for the various types of requests in PlotMod which require
//  creation of a new branch in the Tree.
//
//
// .CLIENTS:
//  PlotModAction class, called by PlotMod main module (PlotMod.cc)
//
// .RESPONSABILITY:
//  This class will provide all support for the visualisation
//  actions whithin PlotMod
//
//   A visualisation action consists of:
//  - Building a Tree
//  - Matching DataUnits and VisDefs within a tree node
//  - Drawing the tree node
//
//  For each step, the request list is processed sequentially.
//
// .COLLABORATORS:
//  Builder, Matcher, Presentable
//
// .ASCENDENT:
//  PlotModAction
//
// .DESCENDENT:
//

#pragma once

#include "PmContext.h"
#include "PlotModAction.h"

class CreateAction : public PlotModAction
{
public:
    // Constructors
    CreateAction(const Cached name) :
        PlotModAction(name) {}

    // Destructor
    ~CreateAction() override = default;

    // Methods
    static PlotModAction& Instance();

    // Overriden from PlotModAction
    void Execute(PmContext& context) override;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// OutputFormatAction class
//

#include "OutputFormatAction.h"
#include "PlotMod.h"

#include "MvVersionInfo.h"

void OutputFormatAction::Execute(PmContext& context)
{
    // Save the output definition request to be used in the plotting routine
    MvRequest req = context.InRequest().justOneRequest();

    // Check if request has a flag indicating that it comes from the Macro module
    if (PlotMod::Instance().CalledFromMacro(req))
        PlotMod::Instance().CalledFromMacro(true);

#if 1
    // FAMI062010 THIS CODE SHOULD BE REMOVED LATER
    // For TEMPORARY backwards compatibiliy we are translating Metview 3
    // output format definition, e.g. DEVICE_DRIVER, to Metview 4 format
    MvRequest newreqdev;
    MvRequest reqdev = req("OUTPUT_DEVICES");
    if (strcmp(reqdev.getVerb(), "DEVICE_DRIVER") == 0) {
        const char* format = reqdev("FORMAT");
        if (!format || strcmp(format, "POSTSCRIPT") == 0)
            newreqdev.setVerb("PSOUTPUT");
        else if (strcmp(format, "JPEG") == 0)
            newreqdev.setVerb("JPEGOUTPUT");
        else if (strcmp(format, "PNG") == 0)
            newreqdev.setVerb("PNGOUTPUT");
        // else //SCREEN

        newreqdev("OUTPUT_FULLNAME") = reqdev("FILE_NAME");
        reqdev = newreqdev;
        req("OUTPUT_DEVICES") = newreqdev;
    }
#endif

    // Update OUTPUT_DEVICES request:
    // a) Magics requires the output filename to have an absolute path.
    //    Hidden parameter _CWD contains the absolute path. If it is not
    //    given then nothing can be done
    // b) Add some meta-data about who created the plot
    while (reqdev) {
        if ((const char*)reqdev("OUTPUT_FULLNAME") && (const char*)req("_CWD")) {
            std::string name = (const char*)reqdev("OUTPUT_FULLNAME");
            if (!name.empty() && name[0] != '/')  // UNIX environment
            {
                name = (string)req("_CWD") + "/" + name;  // absolute path
                reqdev("OUTPUT_FULLNAME") = name.c_str();
            }
        }

        // The same test for the other output name parameter
        if ((const char*)reqdev("OUTPUT_NAME") && (const char*)req("_CWD")) {
            std::string name = (const char*)reqdev("OUTPUT_NAME");
            if (!name.empty() && name[0] != '/')  // UNIX environment
            {
                name = (string)req("_CWD") + "/" + name;  // absolute path
                reqdev("OUTPUT_NAME") = name.c_str();
            }
        }

        // Add meta-data OUTPUT_CREATOR
        MvVersionInfo mvInfo;  // the constructor populates the class with information
        reqdev("OUTPUT_CREATOR") = mvInfo.nameAndVersion().c_str();

        reqdev.advance();
    }

    // Update output definition
    reqdev.rewind();
    req("OUTPUT_DEVICES") = reqdev;

    // Save output definition
    PlotMod::Instance().OutputFormat(req);

    // Advance the input context
    context.Advance();

    // Save the remaining requests.
    // It will skip all the previous requests and only save the
    // remaining ones. Usually, the skipped request is the first
    // request, such as ODB_MANAGER and NETCDF_MANAGER. So, there
    // will be no problems to save only the remaining requests.
    // If this situation changes, this function.will need to be updated.
    MvRequest remainingReqs;
    remainingReqs.copyFromCurrent(context.InRequest());

    // Update context
    context.Update(remainingReqs);
}

//
// This is the exemplar object for the OutputFormat Action class
//
static OutputFormatAction outputFormatActionInstance("OutputFormat");

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// File Graphics Engine
// Gilberto Camara - ECMWF Apr 97
//
//
#include "MagicsGraphicsEngine.h"
#include "GraphicsEngine.h"
#include "ObjectList.h"
#include "Presentable.h"

Cached
GEFactoryTraits::FactoryName(Presentable& owner)
{
    return ObjectList::FindGraphicsEngine(owner.Request());
}

GraphicsEngine*
GEFactoryTraits::DefaultObject(Presentable& owner)
{
    return new MagicsGraphicsEngine(owner);
    // return 0; // no factory was found
    // change to return the default factory
    // new DefaultGraphicsEngine;
}

GraphicsEngine::GraphicsEngine(Presentable& owner) :
    owner_(owner)
{
}

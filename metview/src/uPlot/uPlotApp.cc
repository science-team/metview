/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <MvTemplates.h>

#include "uPlotApp.h"
#include "PmContext.h"
#include "PlotMod.h"
#include "MvException.h"

// TODO: set resources per geoTool and uPlot!
uPlotApp::uPlotApp(int& ac, char** av, const char* name) :
    MvQApplication(ac, av, name, {"geoSelect", "uPlot", "keyDialog", "window"})
{
    // Install Resources
    //	InstallResources();

    // Register this service
    registerTMvServe(*this, nullptr, "_UPLOT_MANAGER_ID", true);
}

// Serve Method
// called by MvTransaction to process the request
void uPlotApp::serve(MvProtocol& proto, MvRequest& inRequest)
{
    // Create a new context, based on the input Request
    auto* context = new PmContext(proto, inRequest);

    // Increment the reference counting
    context->Attach();

    // Execute the context
    context->Execute();

    // Decrement the reference counting
    //	context->Detach();
}

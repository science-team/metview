/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QImage>
#include <QMap>
#include <QMenu>
#include <QSize>
#include <QWidget>

class QAction;
class QListView;

class MgQLayoutItem;
class MgQPlotScene;
class MgQSceneItem;

class MvQZoomStackLevel
{
public:
    MvQZoomStackLevel() = default;
    void setImage(QImage&);
    const QPixmap& pixmap() const { return pixmap_; }
    static QSize pixSize() { return pixSize_; }

protected:
    QPixmap pixmap_;
    static const QSize pixSize_;
};


class MvQZoomStackData : public QObject
{
    Q_OBJECT
public:
    MvQZoomStackData(QString);
    ~MvQZoomStackData() override;
    int update(int, int, MgQLayoutItem*);
    void clear();

    int levelNum() { return levelNum_; }
    int actLevel() { return actLevel_; }
    int selectedLevel() { return selectedLevel_; }
    QString grLayoutId() { return grLayoutId_; }
    void stepUp();
    void stepDown();
    bool setActLevel(int);
    const QList<MvQZoomStackLevel*>& levels() const { return levels_; }

public slots:
    void slotStepTo(int);

signals:
    void actLevelChanged(QString, int);

protected:
    void generatePreview(MvQZoomStackLevel*, MgQLayoutItem*);

    QString grLayoutId_;
    int levelPreviewWidth_;
    int levelNum_;
    int actLevel_;
    int selectedLevel_;
    QList<MvQZoomStackLevel*> levels_;
};


class MvQZoomStackModel : public QAbstractItemModel
{
public:
    MvQZoomStackModel(QObject* parent);

    int columnCount(const QModelIndex&) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    void reload(MvQZoomStackData*);
    void clearData();

protected:
    MvQZoomStackData* data_{nullptr};
};

class MvQZoomStackWidget : public QWidget
{
    Q_OBJECT

public:
    MvQZoomStackWidget(MgQPlotScene*, QWidget* parent = nullptr);
    ~MvQZoomStackWidget() override;
    void reset(MgQSceneItem*, bool);
    MvQZoomStackData* pageData(MgQSceneItem*);

    int levelNum();
    int currentLevel();

protected slots:
    void slotStepUp();
    void slotStepDown();
    void slotStepTo(int);
    void itemSelected(const QModelIndex& current);

signals:
    void actLevelChanged(QString, int);

protected:
    void managePreviewLayout(MvQZoomStackData* page, int levelNum, int actLevel);
    void adjustViewSize();

    QListView* view_;
    MvQZoomStackModel* model_;
    QMap<QString, MvQZoomStackData*> page_;
    MvQZoomStackData* currentPage_{nullptr};
    bool skipUpdate_{false};
    MgQPlotScene* plotScene_{nullptr};
    MgQSceneItem* activeScene_{nullptr};
    int margin_{4};
};

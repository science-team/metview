/***************************** LICENSE START ***********************************

 Copyright 2021 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQFeatureCommandTarget.h"

#include "MvQPlotView.h"
#include "MvQFeatureCommand.h"
#include "MvQFeatureItem.h"
#include "MvQFeatureSelector.h"

// determines who should recieve the context menu or shortcut action
void MvQFeatureCommandTarget::eval(const QPointF& scenePos)
{
    scenePos_ = scenePos;
    MvQFeatureHandler::Instance()->identifyComandTarget(this);
}

void MvQFeatureCommandTarget::setTargetItem(MvQFeatureItemPtr item)
{
    targetItem_ = item;
    itemMode_ = "item";
    if (targetItem_) {
        itemType_ = targetItem_->typeName();
    }
    else if (inSelector_) {
        itemType_ = "selector";
    }
}

void MvQFeatureCommandTarget::setTargetNodeItem(MvQFeatureItemPtr item, int nodeIndex)
{
    targetItem_ = item;
    itemMode_ = "node";
    nodeIndex_ = nodeIndex;
    if (targetItem_) {
        itemType_ = targetItem_->typeName();
    }
}

void MvQFeatureCommandTarget::addDisabledCommands(QStringList lst)
{
    for (auto cmd : lst) {
        if (!disabledCommands_.contains(cmd)) {
            disabledCommands_ << cmd;
        }
    }
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QComboBox>
#include <QDebug>
#include <QLabel>
#include <QGridLayout>
#include <QSettings>
#include <QVBoxLayout>

#include "MvQVideoDialog.h"
#include "MvQMethods.h"

MvQVideoDialog::MvQVideoDialog(QWidget* parent) :
    QFileDialog(parent, "MvQVideo")
{
    // setAttribute(Qt::WA_DeleteOnClose);

    // Set widget mode behaviour to "Save As"
    this->setAcceptMode(QFileDialog::AcceptSave);

    // Restore previous gui settings
    /*QSettings settings(MVQ_QSETTINGS_DEFAULT_ARGS,"uPlotMvQVideoDialog");
    settings.beginGroup("main");
        selectFile(settings.value("fileName").toString());
    QStringList fout = settings.value("formats").toStringList();
    settings.endGroup();*/

    // Set filter options to select which files to be shown
    this->setFilter(tr("All Files (*.*);;Images (*.gif *.tar)"));

    // Get the main layout
    QGridLayout* mainLayout = static_cast<QGridLayout*>(layout());
    int row = mainLayout->rowCount();
    int col = mainLayout->columnCount();

    // Build label for the Output format
    QLabel* label = new QLabel(this);
    label->setText("Output formats:");
    mainLayout->addWidget(label, row, 0);

    formats_["Animated gif (*.gif)"] = "gif";
    formats_["Png frame set in a tar file"] = "tar";

    formatCombo_ = new QComboBox(this);
    foreach (QString key, formats_.keys()) {
        formatCombo_->addItem(key);
    }

    mainLayout->addWidget(formatCombo_, row, 1, 1, 1);
}

// Destructor
MvQVideoDialog::~MvQVideoDialog()
{
    // Save current gui settings
    /*QSettings settings(MVQ_QSETTINGS_DEFAULT_ARGS,"uPlotMvQVideoDialog");
    settings.beginGroup("main");
        settings.setValue("fileName",selectedFiles());
    QStringList formats = comboBox_->getSelectedValues();
    if ( formats.size() )
        settings.setValue("formats",formats);
    else
        settings.setValue("formats","PS");
    settings.endGroup();*/

    // delete comboBox_;
}

// Callback from the SAVE button
void MvQVideoDialog::accept()
{
    QString filename = selectedFiles().at(0);
    QString format = formats_[formatCombo_->currentText()];

    hide();

    emit sendInfo(filename, format);

    // done(0);
}

void MvQVideoDialog::reject()
{
    QDialog::reject();
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  MatchingCriteria
//
// .AUTHOR:
//  Gilberto Camara, Baudouin Raoult and Fernando Ii
//
// .SUMMARY:
//  Defines a class for that stores the criteria used for
//  matching. Is HAS_A request which is of the following form:
//       OVERLAY_CONTROL,
//               OVERLAY_MODE   = CHECK_SETTINGS/NEVER_OVERLAY/ALWAYS_OVERLAY
//               OVERLAY_DATES  = ON/OFF,
//               OVERLAY_TIMES  = ON/OFF,
//               OVERLAY_LEVELS = ON/OFF,
//               OVERLAY_AREAS  = ON/OFF,
//
//
// .CLIENTS:
//  PlotModView
//
//
// .RESPONSABILITIES:
//  - Create a matching criteria request from a view request
//  - Compare two matching information and indicate if they match
//
//
// .COLLABORATORS:
//  MatchingInfo
//
// .BASE CLASS:
//
//
// .DERIVED CLASSES:
//
//
// .REFERENCES:
//
//
#pragma once

#include <MvRequest.h>

class MatchingInfo;

class MatchingCriteria
{
public:
    MatchingCriteria(const MvRequest& matchingRequest);
    ~MatchingCriteria();

    bool Match(const MatchingInfo&, const MatchingInfo&) const;
    bool IsPair(const MatchingInfo&, const MatchingInfo&, bool&) const;

private:
    MvRequest matchingRequest_;
};

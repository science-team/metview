/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// .NAME:
//  Location
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Defines a class for handling the location of a presentable.
//  It contains two types of informatin
//
//  1. A bounding box which indicates the relative position of
//     the presentable in normalized coordinates (0...1),
//     ordered from the to the bottom of the page.
//
//  2. The width and height of the presentable, in world coordinates
//      ( centimeters ).
//
// .CLIENTS:
//  Presentable
//
//
// .RESPONSABILITIES:
//  1. Decode the presentable's request, and extract information about
//     the bounding box, and the requested width and height
//
//
// .COLLABORATORS:
//  MvRequest, Rectangle
//
//
// .BASE CLASS:
//  (none)
//
// .DERIVED CLASSES:
//  (none)
//
// .REFERENCES:
//
//
#pragma once

#include "Rectangle.h"

class MvRequest;

class Location
{
public:
    // Contructors
    // Default constructor
    Location(double top = 0., double left = 0., double bottom = 1., double right = 1.);

    // Constructor for the PLOTPAGE request
    Location(const MvRequest& pageRequest);

    // Constructor from a Box
    Location(const Rectangle& rect);

    // Construction from a string
    Location(char* rect);

    // Copy Constructor
    Location(const Location& location);

    // Operator =
    Location& operator=(const Location& rhs);

    // Destructor
    virtual ~Location();

    // Access to members
    operator Rectangle&()
    {
        return rect_;
    }

    double Left() const
    {
        return rect_.left;
    }

    double Top() const
    {
        return rect_.top;
    }

    double Bottom() const
    {
        return rect_.bottom;
    }

    double Right() const
    {
        return rect_.right;
    }

    // Methods
    // Translation transformation
    friend Location operator+(const Location& lhs, const Location& rhs);

    // Translation transformation
    friend Location operator-(const Location& lhs, const Location& rhs);

    // Scaling transformation
    friend Location operator*(const double scaleFactor, const Location& rhs);

    // Comparison operator
    friend bool operator==(const Location& lhs, const Location& rhs);

#if 0
	// Offset
	void Offset ( const double offsetX, const double offsetY );

	// Aspect Ratio Transformation
	void AdjustToAspectRatio ( double aspectRatio );

	// Normalization Trasnf
	void Normalize ( double, double, double, double );

	// Scaling and Translation Transformation
	Location ScaleAndTranslate ( const Location& origArea,
				     const Location& destArea );

	// Update rectangle
	void UpdateRectangle ( double top, double left, double bottom, double right );
#endif

    // Print rectangle
    void Print() const;

private:
    // Checking values
    //	void CheckNormalized ();  // ensure box is between [0..1]

    // Members

    // The rectangle is always expressed as a normalized box
    // [0,..,1] which is oriented from top to bottom and from
    // left to right
    Rectangle rect_;
};

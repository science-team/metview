/***************************** LICENSE START ***********************************

 Copyright 2023 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

class CursorCoordinate
{
public:
    enum Type {NoType, LonLatType, XyType};
    CursorCoordinate(double x, double y, Type type) : x_(x), y_(y), type_(type), valid_{true} {}
    CursorCoordinate() = default;
    double x() const {return x_;}
    double y() const {return y_;}
    double isValid() const {return valid_;}
    Type type() const {return type_;}

private:
    double x_{0.};
    double y_{0.};
    Type type_{NoType};
    bool valid_{false};
};

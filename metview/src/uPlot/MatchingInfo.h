/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  MatchingInfo
//
// .AUTHOR:
//  Gilberto Camara, Baudouin Raoult and Fernando Ii
//
// .SUMMARY:
//  Defines a class for handling the matching information.
//  Each object of this class HAS_A request, which should
//  be of the type:
//          MATCHING_INFO,
//                        DATE = 860125,
//                        TIME = 1330Z,
//                        .....
//
// .CLIENTS:
//  Presentable, Matching Criteria
//
//
//
// .RESPONSABILITIES:
//  Store the information
//  Retrieve the matching information
//
//
// .COLLABORATORS:
//  MvRequest
//
//
// .BASE CLASS:
//
//
// .DERIVED CLASSES:
//
//
// .REFERENCES:
//


#pragma once

#include <MvRequest.h>

class MatchingInfo
{
    friend class MatchingCriteria;

public:
    MatchingInfo(const MvRequest& matchingRequest);
    ~MatchingInfo();

    MvRequest Request() const
    {
        return matchingRequest_;
    }

private:
    MvRequest matchingRequest_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <vector>

#include "uPlotBase.h"

class MvRequest;

class QAction;
class QComboBox;
class QGraphicsRectItem;
class QGraphicsScene;
class QGraphicsView;
class QLabel;
class QScrollArea;
class QSlider;
class QSplitter;
class QTabWidget;
class QTreeView;
class QTimer;
class QVBoxLayout;
class QWidgetAction;

class CursorCoordinate;
class ExportDialog;
class MgQPlotScene;
class MgQSceneItem;
class MvQDataWidget;
class MvQStepModel;
class MvQLayerWidget;
class MvQFeatureWidget;
class MvQPlotView;
class MvQStepWidget;
class MvQTreeView;
class MvQZoomStackWidget;
class LayerControlWidget;
class LocationLabelWidget;

namespace metview {
namespace uplot {
    class LocationLabelWidget;
}
}

class MvQSceneComboBox : public QComboBox
{
    Q_OBJECT

public:
    MvQSceneComboBox(QWidget* parent = nullptr) :
        QComboBox(parent) {}
    void hidePopup() override;
signals:
    void cursorLeft();
};

class uPlot : public uPlotBase
{
    Q_OBJECT

public:
    uPlot(QWidget* parent = nullptr);
    ~uPlot() override;

    void newRequestForDriversBegin() override;
    void newRequestForDriversEnd() override;
    void progressMessage(const std::string&) override;

public slots:
    void slotShowControlPanel(bool);
    void slotPlay();
    void slotStop();
    void slotToFirst();
    void slotToLast();
    void slotToNext();
    void slotToPrev();
    void slotStepTo(int);
    void slotStepTo(const QModelIndex&);
    void slotSetSpeed(int);
    void slotLayerUpdate();
    void slotSelectActiveScene();
    void slotActiveSceneSelected(QPointF);
    void slotSetActiveScene(MgQSceneItem*) override;
    void slotSetActiveSceneByIndex(int);
    void slotHighlightActiveScene(bool);
    void slotHighlightScene(int);
    void slotNotHighlightScene();
    void slotAnimatedScenes(bool);
    void slotLayerTransparencyChanged(QString, int);
    void slotLayerVisibilityChanged(QString, bool);
    void slotLayerStackingOrderChanged(QList<QPair<QString, int> >);
    void slotControlTabChanged(int);
    void slotPlotScaleChanged();
    void slotShareTarget();

protected slots:
    void slotAddRibbonEditor(QWidget* w) override;

protected:
    void setupFileActions();
    void setupShareActions();
    void setupViewActions() override;
    void setupToolsActions();
    void setupAnimationActions();
    void setupEditActions();
    void setupProgressBarActions();

    bool setDropTarget(QPoint) override;
    bool setDropTargetInView(QPoint) override;

    void updateAnimationActionState();
    int currentStep() override;
    int stepNum() override;

    void loadStarted() override;
    void loadFinished() override;

    void readSettings() override;
    void writeSettings() override;

    QVBoxLayout* plotLayout_;

    QWidget* controlWidget_;
    QTabWidget* controlTab_;

    MvQStepWidget* stepWidget_;
    MvQLayerWidget* layerWidget_;
    MvQDataWidget* dataWidget_;
    LayerControlWidget* layerControlWidget_;
    MvQFeatureWidget* symbolWidget_;

    QAction* actionAntialias_;
    QAction* actionControlPanel_;

    // Animation control
    QAction* actionPlay_;
    QAction* actionStop_;
    QAction* actionNext_;
    QAction* actionPrevious_;
    QAction* actionFirst_;
    QAction* actionLast_;
    QSlider* speedSlider_;
    QTimer* timer_;
    std::vector<float> speed_;
    int actSpeedIndex_;
    QLabel* speedLabel_;

    QAction* actionHighlightScene_;
    QList<MgQSceneItem*> animatedScenes_;
    QAction* actionAnimatedScenes_;
    MvQSceneComboBox* sceneCb_;
    QWidget* sceneWidget_;
    bool ignoreResetEnd_{false};

    metview::uplot::LocationLabelWidget *locationLabel_{nullptr};

    //	MvQProgressItem* progressItem_;

    // Progress Bar
    //    MvQProgressBarPanel* progressBar_;
    //    QWidgetAction* acltb_;
    //    QWidgetAction* acptb_;

    QSplitter* mainSplitter_;

    bool weatherRoom_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QSettings>
#include <QWidget>

class QAbstractButton;
class QButtonGroup;
class QLabel;
class QListWidget;
class QListWidgetItem;
class QStackedLayout;
class QToolButton;

class MgQLayerItem;
class MgQPlotScene;
class MgQSceneItem;

class MvQLayerDataWidget;
class MvQLayerMetaDataWidget;
class MvQPlotView;

class MvQDataWidget : public QWidget
{
    Q_OBJECT

public:
    MvQDataWidget(MgQPlotScene*, MvQPlotView*, QWidget* parent = nullptr);
    ~MvQDataWidget() override;
    void layersAreAboutToChange();
    void reset(MgQSceneItem*);
    // void resetLayerList(bool restoreCurrentItem = true);
    // void resetLayerList(MgQLayerItem*);
    void writeSettings(QSettings&);
    void readSettings(QSettings&);

public slots:
    void slotPlotScaleChanged();
    void slotFrameChanged();
    void slotShowContentsByButton(QAbstractButton*);
    void setLayer(MgQLayerItem*);

signals:
    void layerListRequested();

protected:
    // MgQLayerItem* currentLayerFromList();
    // void setCurrentItemForLayerList(QString);
    void clear();
    void updateTitle();
    QString layerTitle(MgQLayerItem*) const;
    void showContents(int);

    enum ContentsIndex
    {
        MetadataContentsIndex = 0,
        DataContentsIndex = 1
    };

    MgQSceneItem* sceneItem_;
    MgQLayerItem* layer_;
    QStackedLayout* centralLayout_;
    MvQLayerMetaDataWidget* metaWidget_;
    MvQLayerDataWidget* dataWidget_;
    QLabel* previewLabel_;
    QLabel* titleLabel_;
    QToolButton* showLayerTb_;
    QToolButton* showMetaTb_;
    QToolButton* showDataTb_;
    QButtonGroup* showBg_;
    // QListWidget* layerList_;
    QString lastSelectedLayerText_;
    bool metaLoaded_;
    bool dataLoaded_;
};

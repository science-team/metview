/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// File Canvas
// Gilberto Camara - ECMWF Apr 97
//
//
//
//  Methods for the Canvas class
//
//

#include "Canvas.h"

#include "PlotModConst.h"

Canvas::Canvas(Device& device,
               const Rectangle& rect,
               const PaperSize& size,
               int presentableId) :
    myDevice_(device),
    rect_(rect),
    size_(size),
    drawingAreaCoord_(Location(0., 0., 1., 1.)),
    presentableId_(presentableId)
{
    aspectRatio_ = size.GetWidth() / size.GetHeight();
}

Canvas::~Canvas() = default;

#if 0
void
Canvas::SetDrawingArea ( const Location& drawingAreaCoord ) 
{
	// Initialize effective drawing area Coordinates

	drawingAreaCoord_ = drawingAreaCoord;
}

bool
Canvas::HasDrawingAreaBeenSet ()
{
	if (drawingAreaCoord_.Top()    == 0. &&
	    drawingAreaCoord_.Left()   == 0. &&
	    drawingAreaCoord_.Bottom() == 1. &&
	    drawingAreaCoord_.Right()  == 1.    )
		return false;
	else
		return true;
}

// Copy all data from Input Stream to Output Stream - ASCII data
bool Canvas::MoveToFile ( const char* inFileName, 
			  const char* outFileName, bool appendMode )
{
  std::ios::openmode openMode;

  // Write to a new file or append to a old one
  if ( appendMode )
	  openMode = std::ios::app;   // Append to the existing output file
  else
	  openMode = std::ios::out;   // Create a new file

  // Open input and output files
  ofstream outStream ( outFileName, openMode );
  if (  !outStream )
  {
	  COUT << "Could not open output file: " << outFileName << std::endl;
	  return false;
  }

  ifstream inStream  ( inFileName );
  if ( !inStream )
  {
	  COUT << "Could not open input file: " << inFileName << std::endl;
	  return false;
  }

  // Append data
  outStream << inStream.rdbuf();
  if ( !outStream )
  {
	  COUT << "Error updating output file" << outFileName << std::endl;
	  return false;
  }

  inStream.close();
  unlink ( inFileName );

  return true;
}
#endif

MvRequest Canvas::PrinterRequest()
{
    MvRequest deviceRequest = myDevice_.DeviceRequest();

    return deviceRequest;
}

#if 0  // Change this code to PlotMod:IsWindow
//U If derived Canvas classes (e.g. XCanvas, PSCanvas,...) are
// no longer needed, this code should be used instead.
Cached Canvas::CanvasType ()
{
	MvRequest deviceRequest = myDevice_.DeviceRequest();
deviceRequest.print();

	MvRequest outDev = deviceRequest("OUTPUT_DEVICES");
outDev.print();

	while ( outDev )
	{
		MvRequest req =  outDev.justOneRequest();
		req.print();
		if ( req.getVerb() == Cached("QtDriver" ) )
			return SCREEN;

		outDev.advance();
	}

	return Cached(" ");
}
#endif

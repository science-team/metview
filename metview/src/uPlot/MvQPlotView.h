/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QGraphicsView>

#include <memory>

class QLabel;
class QLineEdit;
class QPlainTextEdit;
class QTimer;
class QUndoStack;

class MvKeyProfile;

class CursorCoordinate;
class MvQAreaSelection;
class MvQCursorData;
class MvQDrop;
class MvQLineSelection;
class MvQMagnifier;
class MvQZoom;
class MvQPlaceMark;
class MvQPointSelection;
class MvQFeatureItem;
class MvQFeatureSelector;
class MvQFeatureCommandTarget;
class MvQFeatureRibbonEditor;

class MgQPlotScene;
class MgQLayoutItem;
class MgQSceneItem;

#include "Layer.h"

// TODO: move it into a separate header
//#define MVQ_FEATURE_DEV_MODE_

class MvQPlotView : public QGraphicsView
{
    Q_OBJECT

    friend class MvQUndoCommand;
    friend class MvQFeatureAddCommand;
    friend class MvQFeatureHandler;

public:
    explicit MvQPlotView(QGraphicsScene*, QWidget* parent = nullptr);
    ~MvQPlotView() override = default;
    void resetBegin();
    void resetEnd();
    void setMvPlotSize(double, double);
    void setDataProbe(MvQPlaceMark*);

    void itemSelected(MvQFeatureItem*);
    bool inFeatureAddMode() const { return inFeatureAddMode_; }
    bool hasFeatures() const;
    QAction* handleContextMenu(QList<QAction*> topLevelActions, const QPoint& globalPos, const QPointF& scenePos);
    void setUndoStack(QUndoStack*);
    void setFeatureInfoWidget(QPlainTextEdit*);
    void initFeatureInterface();
    void enterFeatureAddMode();
    void createCursorData();
    void setEnableTrackCursorCoordinates(bool b);

public slots:
    void slotSetEnableZoom(bool);
    void slotSetEnableMagnifier(bool);
    void slotMagnifierChanged();
    void slotSetEnableCursorData(bool);
    void slotResetCursorData();
    void slotSetEnableAreaSelection(bool);
    void slotSetEnableLineSelection(bool);
    void slotSetEnablePointSelection(bool);
    void slotChangeArea(double, double, double, double);
    void slotClearArea();
    void slotSelectAllArea();
    void slotChangeLine(double, double, double, double);
    void slotClearLine();
    void slotChangeLineDirection();
    void slotChangePoint(double, double);
    void slotClearPoint();
    void slotSelectScene();
    void slotCommandShortcut();

signals:
    void zoomRectangleIsDefined(const std::string&, const std::string&);
    void areaIsDefined(double, double, double, double);
    void areaIsUndefined();
    void lineIsDefined(double, double, double, double);
    void lineIsUndefined();
    void pointIsDefined(double, double);
    void pointIsUndefined();
    void magnifierIsEnabledProgramatically(bool);
    void zoomIsEnabledProgramatically(bool);
    void inputSIsEnabledProgramatically(bool);
    void sceneSelected(QPointF);
    void contextMenuEventHappened(const QPoint&, const QPointF&);
    void resizeEventHappened(const QSize&);
    void zoomActionStarted(MgQSceneItem*);
    void cursorDataCoordsChanged(const CursorCoordinate&);
    void iconDropped(const MvQDrop&, QPoint);
    void addRibbonEditor(QWidget*);

protected:
    void mouseDoubleClickEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void contextMenuEvent(QContextMenuEvent*) override;
    void resizeEvent(QResizeEvent*) override;
    void dragEnterEvent(QDragEnterEvent*) override;
    void dragMoveEvent(QDragMoveEvent*) override;
    void dropEvent(QDropEvent*) override;
    void keyPressEvent(QKeyEvent* event) override;
    void removeFeatureTextEditor();
    void leaveFeatureAddMode();

    MgQPlotScene* plotScene_;

    // Zoom
    MvQZoom* zoom_{nullptr};
    MvQMagnifier* magnifier_{nullptr};
    MvQCursorData* cursorData_{nullptr};
    MvQAreaSelection* area_{nullptr};
    MvQLineSelection* line_{nullptr};
    MvQPointSelection* point_{nullptr};
    bool sceneIsBeingSelected_{false};
    MvQPlaceMark* dataProbe_{nullptr};

    // features
    bool inFeatureAddMode_{false};
    MvQFeatureRibbonEditor* featureRibbonEditor_{nullptr};
};

set(common_srcs
    AnnotationView.cc AverageView.cc BufrDecoder.cc
    Canvas.cc CartesianView.cc CommonXSectView.cc
    CreateAction.cc CreateTypeAction.cc
    DataBuilder.cc DataObject.cc Device.cc
    DrawingPriority.cc DropAction.cc
    ErrorAction.cc
    GenAppService.cc
    GeopointsDecoder.cc GraphicsEngine.cc MvGribDecoder.cc
    HovmoellerView.cc Location.cc
    IconClass.cc
    MvLayer.cc MacroConverter.cc MacroVisitor.cc
    MagicsGraphicsEngine.cc MagicsTranslator.cc
    MagPlusService.cc MapView.cc MatchingCriteria.cc MatchingInfo.cc
    MvDecoder.cc MvIcon.cc MvIconDataBase.cc
    NewpageAction.cc
    ObjectInfo.cc ObjectList.cc OutputFormatAction.cc
    Page.cc PlotAction.cc PlotMod.cc PlotModAction.cc PlotModBuilder.cc
    PlotModService.cc PlotModTask.cc PlotModView.cc PlotPageBuilder.cc
    PmContext.cc PmIndividualProjection.cc PmGeneralProjection.cc PmProjection.cc
    Preferences.cc Presentable.cc
    Root.cc
    SatelliteProjection.cc SimpleDecoder.cc
    SkipAction.cc
    Spline.cc
    SubPage.cc SuperPage.cc
    Task.cc ThermoView.cc VertProfView.cc XDevice.cc XSectView.cc ZoomStacks.cc)

if(ENABLE_UI)


set(common_qt_srcs
    MvQCursorData.cc
    MvQDataWidget.cc
    MvQFeatureItem.cc
    MvQFeatureItemObserver.h
    MvQFeatureCommand.cc
    MvQFeatureCommandTarget.cc
    MvQFeatureContextMenu.cc
    MvQFeatureCurveItem.cc
    MvQFeatureRibbonEditor.cc
    MvQFeatureFactory.cc
    MvQFeatureFrontItem.cc
    MvQFeatureImageItem.cc
    MvQFeatureSelector.cc
    MvQFeatureShapeItem.cc
    FeatureStore.cc
    MvQFeatureTextItem.cc
    MvQFeatureWidget.cc
    MvQLayerContentsIcon.cc
    MvQLayerDataWidget.cc
    MvQLayerMetaDataWidget.cc
    MvQLayerModel.cc
    MvQLayerSuperPage.cc
    MvQLayerViewLevel.cc
    MvQLayerWidget.cc
    MvQMagnifier.cc
    MvQPlaceMark.cc
    MvQPlotView.cc
    MvQPointSelection.cc
    MvQProgressBarPanel.cc
    MvQProgressItem.cc
    MvQProgressManager.cc
    MvQStepModel.cc
    MvQStepWidget.cc
    MvQZoom.cc
    MvQAreaSelection.cc
    MvQLineSelection.cc
    MvQZoomStackWidget.cc
    LayerControlWidget.cc
    LocationLabelWidget.cc
    ShareTargets.cc)


set(common_moc_files
    FeatureStore.h
    MvQCursorData.h
    MvQDataWidget.h
    MvQEcChartsDialog.h
    MvQFeatureRibbonEditor.h
    MvQFeatureTextItem.h
    MvQFeatureWidget.h
    MvQLayerContentsIcon.h
    MvQLayerDataWidget.h
    MvQLayerDialog.h
    MvQLayerMetaDataWidget.h
    MvQLayerModel.h
    MvQLayerSuperPage.h
    MvQLayerViewLevel.h
    MvQLayerWidget.h
    MvQMagnifier.h
    MvQPlaceMark.h
    MvQPlotView.h
    MvQPointSelection.h
    MvQProgressManager.h
    MvQStepModel.h
    MvQStepWidget.h
    MvQZoom.h
    MvQAreaSelection.h
    MvQLineSelection.h
    MvQZoomStackWidget.h
    ExportDialog.h
    LayerControlWidget.h
    LocationLabelWidget.h
    ShareTargets.h
    uPlotBase.h
    uPlot.h
)


set(common_ui_srcs
    MagPlusInteractiveService.cc
    PlotModInteractive.cc
    ExportDialog.cc
    MvQEcChartsDialog.cc
    MvQLayerDialog.cc
    uPlotBase.cc
)

set(common_uiform_srcs
    FeatureStoreAddDialog.ui
    FeatureStoreEditDialog.ui
    FeatureStoreWidget.ui
)

# The Qt MOC file needs to be generated with the Weather Room slot functions,
# so we need to add the define here
if(ENABLE_WEATHER_ROOM)
    set(WEATHER_ROOM_MOC_FLAGS -DMETVIEW_WEATHER_ROOM)
endif()

if(METVIEW_QT5)
  QT5_WRAP_CPP(common_MOC ${common_moc_files} OPTIONS ${WEATHER_ROOM_MOC_FLAGS})
  QT5_WRAP_UI(common_FORMS_HEADERS ${common_uiform_srcs})
elseif(METVIEW_QT6)
  QT6_WRAP_CPP(common_MOC ${common_moc_files} OPTIONS ${WEATHER_ROOM_MOC_FLAGS})
  QT6_WRAP_UI(common_FORMS_HEADERS ${common_uiform_srcs})
endif()

#-------- uPLOT ------------------------------------------------------------------

set(uPlot_srcs
    MvMain.cc
    ${common_FORMS_HEADERS}
    ${common_MOC}
    ${common_srcs}
    ${common_ui_srcs}
    ${common_qt_srcs}
    uPlot.cc uPlotApp.cc
#   uPlot.h uPlotApp.h
)


if(ENABLE_WEATHER_ROOM)
    if(METVIEW_QT5)
      QT5_WRAP_CPP(wr_MOC MvQWeatherRoomDialog.h)
    elseif(METVIEW_QT6)
      QT6_WRAP_CPP(wr_MOC MvQWeatherRoomDialog.h)
    endif()
    list(APPEND uPlot_srcs  MvQWeatherRoomDialog.cc ${wr_MOC})
    set(WEATHER_ROOM_FLAGS METVIEW_WEATHER_ROOM)
endif()

if(ENABLE_SHARE_PLOT)
#    list(APPEND uPlot_srcs ShareTargets.cc)
    set(SHARE_PLOT_FLAGS METVIEW_SHARE_PLOT)
endif()

if(ENABLE_SHARE_TO_GLOBE)
     set(SHARE_GLOBE_FLAGS "MV_SHARE_GLOBE_BUILD=1")
endif()

# we need to make sure that
#set(CMAKE_INCLUDE_DIRECTORIES_BEFORE "ON")

#include_directories(BEFORE ${MAGICS_INCLUDE_DIRS})


# the ODB_API libararies are required in the case where Metview was built with ODB support,
# but Magics wasn't

ecbuild_add_executable( TARGET       uPlot
                        SOURCES      ${uPlot_srcs}
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS} ${MAGICS_DEFINITIONS} ${WEATHER_ROOM_FLAGS} ${SHARE_PLOT_FLAGS} ${SHARE_GLOBE_FLAGS}
#                        INCLUDES     /a /b /c
                        INCLUDES     ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR} ${MAGICS_INCLUDE_DIRS} ${METVIEW_QT_INCLUDE_DIRS} ${METVIEW_STANDARD_INCLUDES} cavc
                        LIBS         ${METVIEW_QT_LIBRARIES} ${STANDARD_METVIEW_LIBS_ODB} MagPlus MagWrapper ${METVIEW_ODB_API_LIBRARIES}
                    )

ecbuild_debug("UPLOT CMAKE_INCLUDE_PATH: " ${CMAKE_INCLUDE_PATH})
###nodist_bin_uPlot_SOURCES = ${uPlot_COMMON_QT_SOURCES_nodist} ${uPlot_COMMON_UI_SOURCES_nodist} \
###                           uPlot.moc.cpp


endif()  # UI



set(uPlotManager_srcs
    uPlotManager.cc
    uPlotService.cc
    )
#uPlotManager.hpp uPlotService.hpp

ecbuild_add_executable( TARGET       uPlotManager
                        SOURCES      ${uPlotManager_srcs}
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )


#-------- uPLOT Batch-----------------------------------------------------------------

set(uPlotBatch_srcs
    MvMainuPlotBatch.cc
)

set(uPlotBatchApp_srcs
    MagPlusBatchService.cc
    PlotModBatch.cc
    uPlotBatchApp.cc
    ${common_srcs}
)

 ###MagPlusBatchService.h PlotModBatch.h uPlotBatchApp.h

# we add the QT flags tothe compilation directives here because although this module should have
# no dependencies on Qt, it still uses Magics++, and if that has been built with Qt support, then
# we will need the flags in order to have a correct compilation. If Magics++ has been built without
# Qt, then these flags should be empty, so no harm done.

# the ODB_API libararies are required in the case where Metview was built with ODB support,
# but Magics wasn't

ecbuild_add_executable( TARGET       uPlotBatch
                        SOURCES      ${uPlotBatch_srcs}
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS} NOMETVIEW_QT
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )

ecbuild_add_executable( TARGET       uPlotBatchApp
                        SOURCES      ${uPlotBatchApp_srcs}
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS} ${MAGICS_DEFINITIONS}  NOMETVIEW_QT
                        INCLUDES     ${MAGICS_INCLUDE_DIRS}  ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS_ODB} MagPlus MagWrapper ${METVIEW_ODB_API_LIBRARIES}
                    )

#-------- GeoTool ---------------------------------------------------------------

if(ENABLE_UI)

if(METVIEW_QT5)
  QT5_WRAP_CPP(geotool_MOC GeoTool.h)
elseif(METVIEW_QT6)
  QT6_WRAP_CPP(geotool_MOC GeoTool.h)
endif()


set(uPlotGeoTool_srcs
    MvMainGeoTool.cc
    GeoTool.cc
    ${common_FORMS_HEADERS}
    ${common_MOC}
    ${geotool_MOC}
    ${common_srcs}
    ${common_ui_srcs}
    ${common_qt_srcs}
    uPlot.cc uPlotApp.cc
    uPlot.h uPlotApp.h
)


# the ODB_API libararies are required in the case where Metview was built with ODB support,
# but Magics wasn't

ecbuild_add_executable( TARGET       GeoTool
                        SOURCES      ${uPlotGeoTool_srcs}
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS} ${MAGICS_DEFINITIONS}
                        INCLUDES     ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR} ${MAGICS_INCLUDE_DIRS}  ${METVIEW_QT_INCLUDE_DIRS} ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${METVIEW_QT_LIBRARIES} ${STANDARD_METVIEW_LIBS_ODB} MagPlus MagWrapper ${METVIEW_ODB_API_LIBRARIES}
                    )

set(GeoToolManager_srcs
    GeoToolManager.cc
    )
#GeoToolManager.h

ecbuild_add_executable( TARGET       GeoToolManager
                        SOURCES      ${GeoToolManager_srcs}
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )
endif()  # UI


# -------------------------------------------------------

metview_module_files(ETC_FILES ObjectSpec.OutputFormats)


ecbuild_dont_pack(FILES "MvQWeatherRoomDialog.cc;MvQWeatherRoomDialog.h" )


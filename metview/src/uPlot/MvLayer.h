/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  MvLayer
//
// .AUTHOR:
//  Fernando Ii
//
// .SUMMARY:
//  Defines a class that stores the layer information, such as transparency
//  and visibility.
//
// .CLIENTS:
//
// .RESPONSABILITIES:
//  - Store and provide the layer information.
//
// .COLLABORATORS:
//
// .BASE CLASS:
//
// .DERIVED CLASSES:
//
// .REFERENCES:
//
#pragma once

#include <string>
#include <vector>

class MvRequest;

class MvLayer
{
public:
    // Constructors
    MvLayer();
    MvLayer(const char*);
    MvLayer(const MvRequest&);

    // Destructor
    ~MvLayer() = default;

    // Members get information
    int Id() { return id_; }
    int Transparency() { return transparency_; }
    int Visibility() { return visibility_; }
    int StackingOrder() { return order_; }
    std::string Name() { return name_; }
    std::vector<int> IconsId() { return icons_; }
    MvRequest Request();

    // Members set information
    void Id(int id) { id_ = id; }
    void Visibility(int n) { visibility_ = n; }
    void StackingOrder(int n) { order_ = n; }
    void Name(std::string name) { name_ = name; }
    void IconsId(std::vector<int> icons) { icons_ = icons; }

private:
    int id_;                  // layer Id
    int transparency_;        // 0 - 100
    int visibility_;          // 0/1
    int order_;               // stacking order: 0, 1, ...
    std::string name_;        // layer name
    std::vector<int> icons_;  // list of icons Id
};

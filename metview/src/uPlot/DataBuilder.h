/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  DataBuilder
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Describes the DataBuilder class, an concrete class for
//  building the PlotMod page hierarchy from a "GRIB" or "BUFR" request
//
// .DESCRIPTION:
//  This class is based on the "Builder" pattern (see
//  "Design Patterns" book, page 97).
//
//
//
// .DESCENDENT:
//
// .RELATED:
//  Presentable, SuperPage, Page, DataObject
//
// .ASCENDENT:
//  PlotModBuilder
//
#pragma once

#include "PlotModBuilder.h"

class DataBuilder : public PlotModBuilder
{
public:
    // Constructors
    DataBuilder(Cached name) :
        PlotModBuilder(name) {}

    // Destructor
    ~DataBuilder() override = default;

    // Methods
    // Overriden from PlotModBuilder class
    Presentable* Execute(PmContext&) override;

private:
    // No copy allowed
    DataBuilder(const DataBuilder&);
    DataBuilder& operator=(const DataBuilder&);
};

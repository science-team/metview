/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// .NAME:
//  XDevice
//
// .AUTHOR:
//  Gilberto Camara, Baudoin Raoult and Fernando Ii
//
// .SUMMARY:
//  Defines a class for creating devices associated
//  with X Window System (see the Device class for
//  more information)
//
//
// .CLIENTS:
//  Presentable
//
//
// .RESPONSABILITIES:
//  Provide the creating of X-based widgets associated
//  with each presentable
//
//
// .COLLABORATORS:
//  PresentableWidget
//
//
// .BASE CLASS:
//  Device
//
// .DERIVED CLASSES:
//  (none)
//
// .REFERENCES:
//
//
#pragma once

#include "Device.h"

class XDevice : public Device
{
public:
    // Contructors
    XDevice(const MvRequest& deviceRequest);

    // Destructor
    ~XDevice() override;  // Change to virtual if base class

    // Visitor methods
    void Visit(SuperPage&) override;
    void Visit(Page&) override;
    void Visit(SubPage&) override;

private:
    // No copy allowed
    XDevice(const XDevice&);
    XDevice& operator=(const XDevice&) { return *this; }
};

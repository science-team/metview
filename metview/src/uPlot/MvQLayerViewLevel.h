/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QListView>

#include "MvIconDataBase.h"
#include "MvQDragDrop.h"
#include "MvQLayerContentsIcon.h"

class MvQLayerViewLevelModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    // Constructors & destructors
    MvQLayerViewLevelModel(QObject* parent = nullptr);
    ~MvQLayerViewLevelModel() override;

    // View-model functions
    int columnCount(const QModelIndex&) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    // Member functions
    // Initialize icon list
    void setIcons(MvIconList&);

    //   bool add (const MvQLayerContentsIcon&);
    //   void remove (const MvQLayerContentsIcon&);
    MvQLayerContentsIcon* indexToIcon(const QModelIndex&);

    // Function trigguered by the parent widget because layers have changed
    // (e.g. uPlot has requested a new visualisation window).
    // The icons in the internal list may not have the correct ID because they
    // were initialised by -1 and added to the list before calling uPlot.
    // If an icon is sent to uPlot for the first time, uPlot registers it in the
    // database and assigns a new ID.
    // Check if the icons's ID in the internal list need to be updated.
    void reset(int);

    // Handle presentable
    int presentableId()
    {
        return presentableId_;
    }

    void presentableId(int id)
    {
        presentableId_ = id;
    }

    // Retrieve icons from the dataBase associated with the input Id
    int getIconsFromDB(int, MvIconList&);

protected:
    QList<MvQLayerContentsIcon> icons_;  // icons list
    int presentableId_;                  // current presentable/scene id
};

//----------------------------------------------------------------------

class MvQLayerViewLevelView : public QListView
{
    Q_OBJECT

public:
    MvQLayerViewLevelView(MvQLayerViewLevelModel*, QWidget* parent = nullptr);
    ~MvQLayerViewLevelView() override;

public slots:

    //   void slotContextMenu (const QPoint&);
    void slotDoubleClickItem(const QModelIndex&);

protected slots:

    void slotEdit();
    void slotDelete();
    void slotSave();
    void slotSaveAs();

signals:

    void iconDropped(const MvQDrop&);


protected:
    void currentChanged(const QModelIndex&, const QModelIndex&) override;

    void dragEnterEvent(QDragEnterEvent*) override;
    void dragLeaveEvent(QDragLeaveEvent*) override;
    void dragMoveEvent(QDragMoveEvent*) override;
    void dropEvent(QDropEvent*) override;

    void createContextMenu();

    MvQLayerViewLevelModel* model_;

    QList<QAction*> actions_;
    QAction* actionEdit_;
    QAction* actionSave_;
    QAction* actionSaveAs_;
    QAction* actionDelete_;
};

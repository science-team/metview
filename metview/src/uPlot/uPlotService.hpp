//
// .NAME:
//  uPlotService
//
// .AUTHOR:
//  Fernando Ii
//  (adapted from MagicsService class)
//     ECMWF, December 2008
//
// .SUMMARY:
//  Describes the uPlotService class, which handles
//  the communication with uPlot for the purposes of
//  execution of tasks. This is a "singleton" class
//
//  uPlotManager calls uPlot for three different reasons:
//
//       - when generating a plot request
//  	 - when producing a macro which is able to
//         reproduce the contents of the current window
//       - when zooming a geographical area to generate
//  	   a new window with the contents of the current one
//
// .CLIENTS:
//  SuperPage, as called by the main menu (SuperPageWidget)
//
// .RESPONSABILITY:
//  This class will provide support for the communication
//  with uPlot for the purposes of user interface definitions
//
//  The unique object of this class will, on the "Call uPlot"
//  method :
//  - stores a copy of a callback procedure to be called
//    when uPlot returns its actions
//  - calls uPlot (using the MvServiceTask facilities)
//
//  When the "endofTask" function is returned by "event",
//  the object will perform the callback action stored
//  in the "Call uPlot" method
//
// .COLLABORATORS:
//  MvServiceTask, uPlot
//
// .ASCENDENT:
//  MvClient
//
// .DESCENDENT:
//
//
// .REFERENCES:
//  This class is based on the general facilities for
//  process communication used in METVIEW, especially the
//  MvClient and MvServiceTask facilities. Please refer
//  to these classes (or have a chat with Baudoin) for
//  more information.
//
//  This class implements a "singleton" as suggested
//  by Myers, "More Effective C+", page 130.
//

#pragma once

#include <MvTask.h>

class MvRequest;

class uPlotService : public MvClient
{
public:
    // -- Access to singleton object

    static uPlotService& Instance();

    // -- Destructor

    ~uPlotService();

    // -- Methods

    // Perform actions after receiving response from uPlot
    virtual void endOfTask(MvTask*);

    // Call uPlot
    void CalluPlot(MvRequest& in, MvRequest& out);
    void CallMetZoom(MvRequest& in, MvRequest& out);

private:
    // -- Constructors

    uPlotService();  // no external access point
};

/***************************** LICENSE START ***********************************

 Copyright 2017 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QWidget>

class QFileSystemWatcher;
class MvQProgressBarPanel;

class MvQProgressManager : public QWidget
{
    Q_OBJECT

public:
    MvQProgressManager(int, MvQProgressBarPanel* pbp = nullptr, QWidget* parent = nullptr);

    ~MvQProgressManager() override;

    QFileSystemWatcher* getWatcher()
    {
        return watcher_;
    }

    void setWatcher(QFileSystemWatcher* watcher)
    {
        watcher_ = watcher;
    }

    void setVisible(bool) override;

    void waitFileCreation();

signals:
    void endTempFilesCreation();
    void endFileCreation();

public slots:
    void directoryChanged(const QString&);
    void fileChanged(const QString&);

private:
    QFileSystemWatcher* watcher_;
    MvQProgressBarPanel* progressBarPanel_;
    int ntotalframes_;
    int nframes_;
};

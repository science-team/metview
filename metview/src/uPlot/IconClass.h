/***************************** LICENSE START ***********************************

 Copyright 2021 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <map>

#include "Metview.h"
#include "MvIconClassCore.h"

class Path;
class MvIconLanguage;

class IconClass : public MvIconClassCore
{
public:
    IconClass(const std::string&, request*);
    ~IconClass() override = default;

    static const std::map<std::string, IconClass*>& items() { return items_; }
    static void load(request*);
    static IconClass* find(const std::string&);

private:
    static std::map<std::string, IconClass*> items_;
};

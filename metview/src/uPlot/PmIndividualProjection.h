/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
// PmIndividualProjection
//
// .AUTHOR:
// Fernando Ii
//
// .SUMMARY:
//  Provides methods that are required to handle specific map projections.
//
// .DESCRIPTION:
//  Defines individual projections that are not following conventional map projections.
//
// .DESCENDENT:
//
// .RELATED:
//
// .ASCENDENT:
//  PmProjection

// .COMMENTS:
//  None

#pragma once

#include "PmProjection.h"

class PmCartesianView : public PmProjection
{
protected:
public:
    PmCartesianView(const MvRequest& reqst);
    //		Normal constructor from a request

    ~PmCartesianView() override = default;
    //		Empty destructor

    friend bool operator==(PmCartesianView& a, PmCartesianView& b);
    //		Equality operator. Objects a and b are considered equal if
    //		their common projection parameters are the same.
    //		Input:
    //			a:	Object of type PmCartesianView
    //			b:	Object of type PmCartesianView
    //		Return:
    //			TRUE:	Objects are equal;
    //			FALSE:	Objects are different.

    Point LL2PC(Point& p) override;
    //		Pure virtual method that transforms geodetic into
    //		projection coordinates.
    //		Description:
    //			This method is implemented for each available
    //			projection class and represents the so-called
    //			direct formulas, which compute projection
    //			coordinates from geodetic coordinates.
    //		Input:
    //			p:	Geodetic coordinates (rad)
    //		Return:
    //			p:	Projection coordinates (m)
    //		Preconditions:
    //			Geodetic coordinates must be a valid latitude
    //			([0,pi/2] or [0,-pi/2]) and a valid longitude
    //			([0,pi] or [0,-pi])

    Point PC2LL(Point& p) override;
    //		Pure virtual method that transforms projection into
    //		geodetic coordinates.
    //		Description:
    //			This method is implemented for each available
    //			projection class and represents the so-called
    //			inverse formulas, which compute geodetic
    //			coordinates from projection coordinates.
    //		Input:
    //			p:	Projection coordinates (m)
    //		Return:
    //			p:	Geodetic coordinates (rad)
    //		Preconditions:
    //			X and Y projection coordinates must be both valid,
    //			within the typical range of each projection class.

    bool CheckGeodeticCoordinates(Location&) override;

    double CheckOriginLongitude() override;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// File ObjectList
// Gilberto Camara - ECMWF Sep 97
//
// .NAME:
//  ObjectList
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  A class for handling methods associated
//  with the ObjectList
//
// .CLIENTS:
//  Most modules in PlotMod
//
//
// .RESPONSABILITIES:
//  Provide configuration information for
//  other modules in PlotMod, by use of static methods
//
//
// .COLLABORATORS:
//
//
//
// .BASE CLASS:
//
//
// .DERIVED CLASSES:
//
//
// .REFERENCES:
//
//
#pragma once

#include <MvRequest.h>
#include "MvIconDataBase.h"

class ObjectList
{
public:
    static MvRequest& Instance();

    static MvRequest Find(const Cached&, const Cached&);
    static MvRequest FindPlusChildren(const char*, const char*);

    static int FindAll(const Cached& name, MvRequest& out);

    static int FindAll(const Cached& name, const Cached& classe, MvRequest& out);

    static Cached Find(const Cached&, const Cached&, const Cached&);
    static std::string FindPlusChildren(const std::string&, const std::string&, const std::string&);

    static Cached FindMainRequestValue(const Cached& requestVerb,
                                       const Cached& parameter);

    static Cached FindGraphicsEngine(const MvRequest& reqst);
    static Cached FindView(const MvRequest& reqst);
    static MvRequest FindReqService(const char* classe, const char* action);
    static std::string FindService(const char* classe, const char* action);

    static Cached DefaultVisDefClass(const char* dataUnitVerb);

    static bool CheckValidVisDef(const char*, const char*, const char*);
    static bool CheckValidVisDef(const char*, const char*, int = 0);
    static bool CheckValidVisDefList(const char*, MvIconList&, const char* = nullptr);

    static void VisDefNDimFlags(int, std::vector<std::string>&);

    static MvRequest UserPreferencesRequest(const char*);
    static MvRequest UserDefaultRequest(const char*, bool expand = false);

    static MvRequest ExpandRequest(const MvRequest&, long);

    // Create a request in case there isn't one.
    // expandFlag = 0 means use the expand flag value from the ObjectList.
    static MvRequest CreateDefaultRequest(const char*, int expandFlag = 0, const char* path = nullptr);

    // Check objects
    static bool IsDataUnit(const Cached&);
    static bool IsImage(const char*);
    static bool IsService(const char*, const char* action = nullptr, bool context = false);
    static bool IsDefaultValue(MvRequest&, const char*);
    static bool IsWindow(const Cached&);
    static bool IsView(const Cached&);
    static bool IsGeographicalView(const std::string&);
    static bool IsVisDef(const Cached&);
    static bool IsVisDefContour(const char*);
    static bool IsVisDefIsotachs(const MvRequest&);
    static bool IsVisDefCoastlines(const Cached&);
    static bool IsVisDefText(const char*);
    static bool IsVisDefTextTitle(const MvRequest&);
    static bool IsVisDefTextAnnotation(const MvRequest&);
    static bool IsVisDefLegend(const char*);
    static bool IsVisDefImport(const char*);
    static bool IsVisDefAxis(const Cached&);
    static bool IsVisDefGraph(const Cached&);
    static bool IsLayer(const char*);
    static bool AreCompanions(const std::string&, const std::string&);
    static bool CalledFromMacro(const MvRequest&);
    static bool IsThermoView(const char*);
    static bool IsThermoGrid(const char*);
    static bool IsTaylorGrid(const char*);
    static bool IsWeatherSymbol(const Cached& requestVerb);

    static Cached MacroName(const Cached& iconClass);
    static void GetCompanion(const std::string&, std::string&, std::string&, bool&);

private:
    // Contructor
    ObjectList();

    // Destructor
    ~ObjectList();
};

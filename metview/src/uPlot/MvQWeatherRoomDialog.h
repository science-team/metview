/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <vector>

#include <QDialog>
#include <QWidget>

#include "MvRequest.h"

class QComboBox;
class QGridLayout;
class QLayout;
class QNetworkReply;
class QRadioButton;
class QLineEdit;

// ----------------------------------------------------------
// MvQVideoWallCellInfo
// Contains information for a single cell within a video wall
// ----------------------------------------------------------

class MvQVideoWallCellInfo
{
public:
    MvQVideoWallCellInfo(double x, double y, double w, double h, int number);

    int number() { return number_; }
    double x() { return x_; }
    double y() { return y_; }
    double w() { return w_; }
    double h() { return h_; }

private:
    int number_;
    double x_;
    double y_;
    double w_;
    double h_;
};


// --------------------------------------------
// MvQVideoWallInfo
// Contains information for a single video wall
// --------------------------------------------

class MvQVideoWallInfo
{
public:
    MvQVideoWallInfo() = default;
    ;
    std::vector<MvQVideoWallCellInfo>& cellInfos() { return cellInfos_; }

    QString& name() { return name_; }
    void setName(QString& name) { name_ = name; }

    QString& nameId() { return nameId_; }
    void setNameId(QString& name) { nameId_ = name; }

    void addCellInfo(MvQVideoWallCellInfo& cell) { cellInfos_.push_back(cell); }

    int numCells() { return cellInfos_.size(); }

private:
    QString name_;    // title name
    QString nameId_;  // base name
    std::vector<MvQVideoWallCellInfo> cellInfos_;
    //   QString wallURL_;
    //   QString wallToken_;
};


// ----------------------------------------------
// MvQVideoWallServer
// Handles communication with a video wall server
// ----------------------------------------------

class MvQVideoWallServer : public QObject
{
    Q_OBJECT

public:
    MvQVideoWallServer(QObject* parent = nullptr);

    MvQVideoWallInfo* wallInfo() { return wallInfo_; }
    void createWallLayout(int);
    size_t numWalls() { return wallNames_.size(); }
    int wallInfoIndexFromName(const std::string&);

    std::string wallInfoName(int i) { return wallNames_[i]; }
    std::string wallInfoId(int i) { return wallIds_[i]; }
    std::string wallInfoId(const std::string&);

    QString& URL() { return URL_; }
    QString& token() { return token_; }

    int numCells() { return wallInfo_ ? wallInfo_->numCells() : 0; }

signals:
    void gotServerResponse();
    void gotServerLayoutResponse();

protected:
    bool parseWallServer(QString&);
    bool parseWallLayout(QString&);

protected slots:
    void slotWallServerReply(QNetworkReply*);
    void slotWallLayoutReply(QNetworkReply*);

private:
    MvQVideoWallInfo* wallInfo_;          // hold one wall information
    std::vector<std::string> wallNames_;  // list of wall names
    std::vector<std::string> wallIds_;    // list of wall ids
    QString URL_;
    QString token_;
};


class MvQVideoWallWidget : public QWidget
{
    Q_OBJECT

public:
    MvQVideoWallWidget(QWidget* parent = nullptr);
    void setWall(MvQVideoWallInfo* info);
    int current() const { return current_; }

    int numCells() { return items_.size(); }

public slots:
    void updateCellSelection(const QString&);

signals:
    void selectionChanged(const QString&);

protected:
    void paintEvent(QPaintEvent*) override;
    void mousePressEvent(QMouseEvent*) override;

private:
    bool select(QPoint);
    int offset_;
    int current_;
    QList<QRect> items_;
    QPixmap pix_;
    QPixmap pixSelect_;
};


class MvQWeatherRoomDialog : public QDialog
{
    Q_OBJECT

public:
    MvQWeatherRoomDialog(int, int, QWidget* parent = nullptr);
    ~MvQWeatherRoomDialog() override;

    // Get info from the user interface
    int getCell();
    std::string getWallName();
    std::string getWallId();

public slots:
    //   void slotRangeFrameSelection(bool);
    void updateWallServerSelector();
    void updateWallLayoutSelector();
    void updateWallSelection(int);
    void accept() override;

protected:
    void setVisible(bool) override;
    void readSettings();
    void writeSettings();

private:
    //   bool getFrameSelection ( MvRequest& );
    //   void buildFrameSelection( QLayout* );
    //   void errorMessage ( const char*, const char* = 0 );
    void setSelectedCell(int);

    QComboBox* wallCb_;
    MvQVideoWallWidget* cellSelector_;
    MvQVideoWallServer* wallServer_;
    QLineEdit* leCellSelection_;

    int currentFrame_;  // Current frame
    int totalFrames_;   // Total number of frames

    std::string lastWall_;     // last wall used
    int lastCell_;             // last cell used
    bool updatingFromServer_;  // to avoid the wall listwallCB_

    QLineEdit* leFs3_;     // Frame selection: line edit
    QRadioButton* cbFs1_;  // Frame selection: Current radio button
    QRadioButton* cbFs2_;  // Frame selection: All radio button
    QRadioButton* cbFs3_;  // Frame selection: Range radio button
};

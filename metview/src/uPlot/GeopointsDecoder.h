/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  GeopointsDecoder
//
// .AUTHOR:
//  Geir Austad
//
// .SUMMARY:
//  A concrete  class for decoding geopoints files
//
// .CLIENTS:
//  PlotModMatcher, Graphics Engine
//
// .RESPONSABILITIES:
//  Return true first time ReadNextData is called. No matching is done.
//
// .COLLABORATORS:
//
// .BASE CLASS:
//  Decoder
//
// .DERIVED CLASSES:
//
//
// .REFERENCES:
//
//  The design of this class is based on the "Factory" pattern
//  ("Design Patterns", page. 107).
//
//
#pragma once

#include "MvDecoder.h"
//#include <MvDate.h>
//#include <MvFilter.h>
//#include <MvFieldSet.h>

class GeopointsDecoder : public Decoder
{
public:
    // Contructors
    GeopointsDecoder(const MvRequest& inRequest);

    // Destructor
    ~GeopointsDecoder() override;

    // Overridden methods from Decoder class
    // Returns true first time it's called
    bool ReadNextData() override;

    MatchingInfo CreateMatchingInfo() override;

    bool UpdateDataRequest(MvRequest&) override;

private:
    // No copy allowed
    GeopointsDecoder(const GeopointsDecoder&);
    GeopointsDecoder& operator=(const GeopointsDecoder&) { return *this; }
};

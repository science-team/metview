/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QColor>
#include <QBrush>
#include <QDebug>
#include <QLinearGradient>
#include <QPainter>

#include "MvKeyProfile.h"

#include "MvQStepModel.h"
#include "MgQPlotScene.h"
#include "MgQSceneItem.h"
#include "MvQTheme.h"


MvQStepDelegate::MvQStepDelegate(QWidget* parent) :
    QStyledItemDelegate(parent)
{
}

void MvQStepDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option,
                            const QModelIndex& index) const
{
    QStyledItemDelegate::paint(painter, option, index);
    QRect rect = option.rect;
    QLine line(rect.x(), rect.y(), rect.x() + rect.width(), rect.y());

    painter->setPen(QPen("#BBBBBB"));
    painter->drawLine(line);
}


MvQStepModel::MvQStepModel(QObject* parent) :
    MvQKeyProfileModel(parent)
{
    stepData_ = nullptr;
    cachedBg_ = MvQTheme::colour("treeview", "cached_bg");
}

void MvQStepModel::stepDataIsAboutToChange()
{
    beginResetModel();
}

void MvQStepModel::setStepData(MgQSceneItem* data)
{
    stepData_ = data;
    // endResetModel();
}

QVariant MvQStepModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return {};
    }

    if (role == Qt::BackgroundRole) {
        if (stepData_ && stepData_->stepCached(index.row())) {
            return cachedBg_;
            //            if (index.row() % 2 == 0)
            //                return QColor("#FFE6BF");
            //            else
            //                return QColor("#FFF2DE");
        }
        else {
            return {};  // QBrush(Qt::white);
        }
    }

    return MvQKeyProfileModel::data(index, role);
}

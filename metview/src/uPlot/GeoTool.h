/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QLineEdit>

#include "uPlotBase.h"
#include "MvRequest.h"

class GeoTool : public uPlotBase
{
    Q_OBJECT

public:
    GeoTool(MvRequest&, QWidget* parent = nullptr);
    ~GeoTool() override;

    MvRequest SuperPageRequest() override { return spRequest_; }

    void newRequestForDriversBegin() override;
    void newRequestForDriversEnd() override;

public slots:
    void slotOk();
    void slotCoordChanged();
    void slotAreaSelection(double, double, double, double);
    void slotAreaIsUndefined();
    void slotLineSelection(double, double, double, double);
    void slotLineIsUndefined();
    void slotPointSelection(double, double);
    void slotPointIsUndefined();
    void slotPlotWindowUpdated();
    void slotSetActiveScene(MgQSceneItem*) override {}

signals:
    void areaChanged(double, double, double, double);
    void lineChanged(double, double, double, double);
    void pointChanged(double, double);

private:
    void setupInputSelectionActions();

    // Initialize parameters
    QString InitializeParams(MvRequest&);

    bool setDropTarget(QPoint) override;
    bool setDropTargetInView(QPoint) override;
    int currentStep() override { return 0; }
    int stepNum() override { return 0; }

    // Get input coordinates
    void UpdateCoordinates(QString&);

    // Translate coordinates to/from request
    QString requestToCoord(MvRequest&, const char*);
    void coordToRequest(QString&, MvRequest&, const char*);

    void readSettings() override;
    void writeSettings() override;

    QLineEdit* coordEdit_;   // geographical coordinates area
    QString coord_;          // coordinates
    MvRequest geoRequest_;   // request to be sent out to the caller
    MvRequest spRequest_;    // superpage request
    std::string inputType_;  // AREA, LINE, POINT
    std::string inputPar_;
    bool isFirstMap_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QBrush>
#include <QGraphicsItem>
#include <QPen>

#include "MvQPlotItem.h"

class MgQLayoutItem;

class MvQPointSelection : public MvQPlotItem
{
    Q_OBJECT

public:
    MvQPointSelection(MgQPlotScene*, MvQPlotView*, QGraphicsItem* parent = nullptr);
    ~MvQPointSelection() override;

    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;
    QRectF boundingRect() const override;

    void setPoint(double, double);
    void clearPoint();

    void setActivated(bool) override;

    void mousePressEventFromView(QMouseEvent*) override;
    void mouseMoveEventFromView(QMouseEvent*) override;
    void mouseReleaseEventFromView(QMouseEvent*) override;
    void reset() override;

signals:
    void pointIsDefined(double, double);
    void pointIsUndefined();

protected:
    enum CurrentAction
    {
        NoAction,
        DefineAction,
        MoveAction
    };

    bool checkPointInSelector(QPointF&);
    void checkPoint(QPointF&);
    void checkSelectorSize() const;

    QPen pen_;
    QBrush brush_;
    QBrush hoverBrush_;
    QPointF point_;
    bool pointDefined_;
    QPointF coordPoint_;
    CurrentAction currentAction_;
    MgQLayoutItem* zoomLayout_;
    QPointF dragPos_;

    bool hover_;
    mutable float selectorSize_;
    mutable float physicalSelectorSize_;
};

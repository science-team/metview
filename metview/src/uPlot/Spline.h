/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <vector>


class Spline
{
public:
    Spline() = default;
    Spline(const std::vector<double>& x,
           const std::vector<double>& y);
    double eval(double x) const;
    bool status() const { return status_; }

private:
    bool tdma(const std::vector<double>& a, const std::vector<double>& b,
              const std::vector<double>& c, const std::vector<double>& d,
              std::vector<double>& X);

    std::vector<double> xp_;
    std::vector<double> yp_;
    // cubic polynomial coefficients
    std::vector<double> coeffA_, coeffB_, coeffC_;
    bool status_{false};
};


class SplineParam
{
public:
    SplineParam() = default;
    SplineParam(const std::vector<double>& x,
                const std::vector<double>& y);

    void eval(double t, double& x, double& y) const;
    bool status() const { return status_; }

protected:
    Spline splineX_;
    Spline splineY_;
    bool status_{false};
};

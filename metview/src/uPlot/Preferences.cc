/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Preferences.h"
#include "MvPath.hpp"
#include "ObjectList.h"

Preferences::Preferences(Presentable& owner) :
    owner_(&owner)
{
}

Preferences::Preferences(const Preferences& old) = default;

MvRequest Preferences::CoastRequest()
{
    // Create a coastline request and read user's preferences
    MvRequest coastRequest;
    std::string path = MakeUserDefPath("CoastlinesGeographyHelper");

    // Do we have a preference ?
    if ((FileCanBeOpened(path.c_str(), "rw") == false) ||
        (FileHasValidSize(path.c_str()) == false)) {
        // Create a new one
        coastRequest = ObjectList::CreateDefaultRequest("MCOAST");
        coastRequest.save(path.c_str());
    }
    else
        coastRequest.read(path.c_str());

    return coastRequest;
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  MvIcon
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Describes the MvIcon class, a base class
//  for the various types of icons in PlotMod
//
// .CLIENTS:
//  Presentable
//
// .RESPONSABILITY:
//  This class will provide support for dealing with
//  the different types of Icons in PlotMod.
//
// .COLLABORATORS:
//  MvIconRep
//
// .ASCENDENT:
//
// .DESCENDENT:
//
//
// .REFERENCES:
//  This "reference counted" class is based on the
//  "handle/body" C++ idiom, described in Coplien(1992), pg. 58.
//

#include <Assertions.hpp>
#include "MvIcon.h"

// --- This is the body class - MvIconRep

MvIconRep::MvIconRep() :
    myRequest_("EMPTY")
{
    // Empty
}

MvIconRep::MvIconRep(const std::string& iconName, const std::string& iconClass) :
    iconName_(iconName),
    iconClass_(iconClass)
{
}

MvIconRep::MvIconRep(const MvRequest& inRequest, bool idSave) :
    iconId_(++iconNextId_),
    myRequest_(inRequest)
{
    iconName_ = ObjectInfo::ObjectName(inRequest, "", iconId_);
    iconClass_ = inRequest.getVerb();
    iconFullPath_ = ObjectInfo::ObjectPath(inRequest);

    // Save iconId to the request
    if (idSave)
        myRequest_("_ID") = iconId_;
}

MvIconRep::MvIconRep(bool idSave) :
    myRequest_("EMPTY")
{
    // Save iconId to the request
    if (idSave) {
        ++iconNextId_;
        iconId_ = iconNextId_;
    }
    else
        iconId_ = 0;
}

// Copy Constructor and Operator =
MvIconRep&
MvIconRep::operator=(const MvIconRep& mvIconRep)
{
    if (this != &mvIconRep) {
        iconName_ = mvIconRep.iconName_;
        iconClass_ = mvIconRep.iconClass_;
        iconFullPath_ = mvIconRep.iconFullPath_;
        iconId_ = mvIconRep.iconId_;
        parentId_ = mvIconRep.parentId_;

        myRequest_ = mvIconRep.myRequest_;

        visibility_ = mvIconRep.visibility_,
        transparency_ = mvIconRep.transparency_;
        order_ = mvIconRep.order_;
    }
    return *this;
}

MvIconRep::MvIconRep(const MvIconRep& mvIconRep)
{
    iconName_ = mvIconRep.iconName_;
    iconClass_ = mvIconRep.iconClass_;
    iconFullPath_ = mvIconRep.iconFullPath_;
    iconId_ = mvIconRep.iconId_;
    parentId_ = mvIconRep.parentId_;

    myRequest_ = mvIconRep.myRequest_;

    visibility_ = mvIconRep.visibility_,
    transparency_ = mvIconRep.transparency_;
    order_ = mvIconRep.order_;
}

// Initialization of the static counter
// (this gives each icon its unique Id)
int MvIconRep::iconNextId_ = 0;

//---------------------------------------------
// --- This is the Handle class - MvIcon

MvIcon::MvIcon()
{
    ptrIconRep_ = new MvIconRep;
    ptrIconRep_->refCount_ = 1;
}

MvIcon::MvIcon(const std::string& iconName, const std::string& iconClass)
{
    ptrIconRep_ = new MvIconRep(iconName, iconClass);
    ptrIconRep_->refCount_ = 1;
}

MvIcon::MvIcon(bool idSave)
{
    ptrIconRep_ = new MvIconRep(idSave);
    ptrIconRep_->refCount_ = 1;
}

// Default constructor
// Builds an icon from Request
// Initialises the reference counter
MvIcon::MvIcon(const MvRequest& inRequest, bool idSave)
{
    ptrIconRep_ = new MvIconRep(inRequest, idSave);
    ptrIconRep_->refCount_ = 1;
}

// Copy constructor
// copies the representation pointer
// increments the reference counter
MvIcon::MvIcon(const MvIcon& inIcon)
{
    ptrIconRep_ = inIcon.ptrIconRep_;
    ptrIconRep_->refCount_++;
}

// Operator =
// Copies the representation pointer
// Decrements the reference counter of the current object
// Increments the reference counter for the new object
MvIcon&
MvIcon::operator=(const MvIcon& inIcon)
{
    if (this != &inIcon) {
        inIcon.ptrIconRep_->refCount_++;

        if (--(ptrIconRep_->refCount_) <= 0)
            delete ptrIconRep_;

        ptrIconRep_ = inIcon.ptrIconRep_;
    }
    return *this;
}

// Destructor
MvIcon::~MvIcon()
{
    if (--(ptrIconRep_->refCount_) <= 0)
        delete ptrIconRep_;
}

void MvIcon::SaveRequest(const MvRequest& inRequest)
{
    ptrIconRep_->myRequest_ = inRequest;
}

MvRequest
MvIcon::Request() const
{
    require(ptrIconRep_ != nullptr);

    MvRequest req = ptrIconRep_->myRequest_;

    return req;
}

MvRequest
MvIcon::SvcRequest() const
{
    require(ptrIconRep_ != nullptr);

    return ptrIconRep_->svcRequest_;
}

bool operator==(MvIcon& a, MvIcon& b)
{
    if (a.sIconClass() != b.sIconClass())
        return false;
    if (a.sFullName() != b.sFullName())
        return false;
    if (a.sPathName() != b.sPathName())
        return false;

    return true;
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Location.h"

#include <iostream>

#include <MvRequest.h>

Location::Location(const MvRequest& pageRequest)
{
    // Find out the size of the page
    double top = pageRequest("TOP");
    double bottom = pageRequest("BOTTOM");
    double left = pageRequest("LEFT");
    double right = pageRequest("RIGHT");

    rect_.top = top / 100.;
    rect_.left = left / 100.;

    rect_.bottom = bottom / 100.;
    rect_.right = right / 100.;

    //	CheckNormalised();
    //	ensure ( ( right - left ) <= 100. );
}

Location::Location(double top, double left, double bottom, double right)
{
    rect_.top = top;
    rect_.left = left;

    rect_.bottom = bottom;
    rect_.right = right;
}

Location::Location(const Location& rhs)
{
    rect_.top = rhs.rect_.top;
    rect_.left = rhs.rect_.left;

    rect_.bottom = rhs.rect_.bottom;
    rect_.right = rhs.rect_.right;
}

Location::Location(char* rect)
{
    // The expected format is: n1/n2...
    double coord[4];
    char str1[30];
    int i = 0, j = 0;
    char* p1 = &rect[0];

    coord[0] = coord[1] = coord[2] = coord[3] = 0;
    j = 0;
    while (*p1 && j < 4) {
        i = 0;
        while (*p1 && *p1 != '/' && i < 29) {
            str1[i++] = *p1;
            p1++;
        }

        str1[i] = 0;
        coord[j++] = atof(str1);
        if (*p1)
            p1++;
    }

    rect_.top = coord[2];
    rect_.left = coord[1];

    rect_.bottom = coord[0];
    rect_.right = coord[3];
}

Location::~Location() = default;

Location&
Location::operator=(const Location& rhs)
{
    if (this == &rhs)
        return *this;

    rect_.top = rhs.rect_.top;
    rect_.left = rhs.rect_.left;

    rect_.bottom = rhs.rect_.bottom;
    rect_.right = rhs.rect_.right;

    return *this;
}

Location
operator+(const Location& lhs, const Location& rhs)
{
    return {lhs.rect_.top + rhs.rect_.top,
            lhs.rect_.left + rhs.rect_.left,
            lhs.rect_.bottom + rhs.rect_.bottom,
            lhs.rect_.right + rhs.rect_.right};
}

Location
operator-(const Location& lhs, const Location& rhs)
{
    return {lhs.rect_.top - rhs.rect_.top,
            lhs.rect_.left - rhs.rect_.left,
            lhs.rect_.bottom - rhs.rect_.bottom,
            lhs.rect_.right - rhs.rect_.right};
}

Location
operator*(const double scaleFactor, const Location& rhs)
{
    return {scaleFactor * rhs.rect_.top,
            scaleFactor * rhs.rect_.left,
            scaleFactor * rhs.rect_.bottom,
            scaleFactor * rhs.rect_.right};
}

bool operator==(const Location& lhs, const Location& rhs)
{
    if (lhs.rect_.top != rhs.rect_.top)
        return false;

    if (lhs.rect_.left != rhs.rect_.left)
        return false;

    if (lhs.rect_.bottom != rhs.rect_.bottom)
        return false;

    if (lhs.rect_.right != rhs.rect_.right)
        return false;

    return true;
}

#if 0
void
Location::Offset ( const double offsetX, const double offsetY )
{
	rect_.top    -= offsetY;
	rect_.bottom -= offsetY;
	rect_.left   -= offsetX;
	rect_.right  -= offsetX;
}

void
Location::AdjustToAspectRatio ( double desiredAspectRatio )
{
	double origHeight      = fabs ( rect_.top - rect_.bottom );
	double origWidth       = fabs ( rect_.right - rect_.left );
	double origAspectRatio = origWidth / origHeight;

	// Set coordinates according to the aspect ratio desired

	if ( desiredAspectRatio >= origAspectRatio )
	{
		double newHeight = ( origHeight * origAspectRatio ) / desiredAspectRatio;

		rect_.bottom += ( origHeight - newHeight ) / 2. ;
		rect_.top     = rect_.bottom + newHeight;
	}
	else
	{	

		double newWidth = ( origWidth * desiredAspectRatio ) / origAspectRatio;

		rect_.left    += ( origWidth - newWidth ) / 2. ;
		rect_.right    = rect_.left + newWidth;
	}
}

void
Location::Normalize ( double referenceTop, 
		      double referenceLeft,
		      double referenceBottom,
		      double referenceRight )
{
	double gainV = fabs ( referenceTop - referenceBottom );
	double gainH = fabs ( referenceRight - referenceLeft );

	rect_.bottom = ( rect_.bottom * gainV ) + referenceBottom;

	rect_.top    = ( rect_.top    * gainV ) + referenceBottom;

	rect_.left   = ( rect_.left   * gainH ) + referenceLeft;

	rect_.right  = ( rect_.right  * gainH ) + referenceLeft;
}

static // should be : inline but compiler is broken
void
normalise  (double& x)
{
	if ( x < 0 ) x = 0.;
	if ( x > 1 ) x = 1. ;
}

static // should be : inline but compiler is broken
void
compare ( double& x , double& y )
{
	if ( x > y ) 
		std::swap (x, y );
}

void
Location::CheckNormalized()
{
	normalise ( rect_.top );
	normalise ( rect_.left );
	normalise ( rect_.bottom );
	normalise ( rect_.right );

	compare ( rect_.top,  rect_.bottom );
	compare ( rect_.left, rect_.right  );
}

//
//
//   Scale and Translate
//
//   PURPOSE: Perform a scaling and translation transformation
//
//   GIVEN:
//            this (the object) = a window in the original coodinate system 
//            origArea          - the external origin area (box)
//            destArea          - the destination coordinate system (box)
//
//   CALCULATES:
//            destCoord         - the window in the destination coordinate system


Location
Location::ScaleAndTranslate( const Location& origArea,
			     const Location& destArea )
{
	double scaleH = ( destArea.Right() - destArea.Left() ) /
		        ( origArea.Right() - origArea.Left() ) ;
	
	double scaleV = ( destArea.Top()   - destArea.Bottom() ) /
		        ( origArea.Top()   - origArea.Bottom() ) ;

	
	double right  = scaleH * ( this->Right()  - origArea.Right() ) +
		        destArea.Right();

	double left   = scaleH * ( this->Left()   - origArea.Right() ) +
		        destArea.Right();

	double bottom = scaleV * ( this->Bottom() - origArea.Bottom()) +
		        destArea.Bottom();

	double top    = scaleV * ( this->Top()    - origArea.Bottom()) +
		        destArea.Bottom();

	return Location ( top, left, bottom, right );
}

void
Location::UpdateRectangle ( double top, double left, double bottom, double right )
{
	rect_.top    = top / 100.;
	rect_.left   = left / 100.;
		      
	rect_.bottom = bottom / 100.;
	rect_.right  = right / 100.;
}
#endif

void Location::Print() const
{
    std::cout << "Location(t,l,b,r) = " << std::endl;
    std::cout << rect_.top << " " << rect_.left << " " << rect_.bottom << " " << rect_.right << std::endl;
}

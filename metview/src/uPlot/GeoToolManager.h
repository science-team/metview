/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  GeoToolService and GeoToolManager
//
// .AUTHOR:
//  Fernando Ii
//  (adapted from MagicsService class)
//     ECMWF, April 2010
//
// .SUMMARY:
//  Describes the GeoToolService  and GeoToolManger classes,
//  which handles the communication with GeoTool (including uPlot base
//  classes) for the purposes of execution of geography tool tasks.
//  This is a "singleton" class
//
// .CLIENTS:
//  SuperPage, as called by the main menu (SuperPageWidget)
//
// .RESPONSABILITY:
//  This class will provide support for the communication
//  with uPlot for the purposes of user interface definitions
//
//  The unique object of the GeoToolService class will, on the "Call GeoTool"
//  method :
//  - stores a copy of a callback procedure to be called
//    when GeoTool returns its actions
//  - calls GeoTool (using the MvServiceTask facilities)
//
//  When the "endofTask" function is returned by "event",
//  the object will perform the callback action stored
//  in the "Call GeoTool" method
//
// .COLLABORATORS:
//  MvServiceTask, GeoTool
//
// .ASCENDENT:
//  MvClient
//
// .DESCENDENT:
//
//
// .REFERENCES:
//  This class is based on the general facilities for
//  process communication used in METVIEW, especially the
//  MvClient and MvServiceTask facilities. Please refer
//  to these classes (or have a chat with Baudoin) for
//  more information.
//
//  This class implements a "singleton" as suggested
//  by Myers, "More Effective C+", page 130.
//

#pragma once

//#include "Metview.h"

#include <mars.h>
#include <MvRequest.h>
#include <MvProtocol.h>
#include <MvService.h>
#include <MvTask.h>

class GeoToolManager : public MvService
{
public:
    // Constructor
    GeoToolManager(const char* kw);

    // Destructor
    ~GeoToolManager() override = default;

    void serve(MvRequest&, MvRequest&) override;
};


class GeoToolService : public MvClient
{
public:
    // Access to singleton object
    static GeoToolService& Instance();

    // Destructor
    ~GeoToolService() override;

    // Methods

    // Perform actions after receiving response from the GeographyTool tools
    void endOfTask(MvTask*) override;

    // Call data tiik
    void CallGeoTool(MvRequest& in, MvRequest& out);

private:
    // Constructors
    GeoToolService();  // no external access point
};

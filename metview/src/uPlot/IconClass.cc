/***************************** LICENSE START ***********************************

 Copyright 2021 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "IconClass.h"

#include "ConfigLoader.h"
#include "MvIconLanguage.h"

#include <algorithm>

std::map<std::string, IconClass*> IconClass::items_;

IconClass::IconClass(const std::string& name, request* r) :
    MvIconClassCore(name, r)
{
    items_[name] = this;
}

void IconClass::load(request* r)
{
    new IconClass(get_value(r, "class", 0), r);
}

IconClass* IconClass::find(const std::string& name)
{
    for (auto& item : items_) {
        IconClass* kind = item.second;

        std::string strName = name;
        std::transform(strName.begin(), strName.end(), strName.begin(), ::tolower);

        std::string s = kind->name();
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);
        if (s == strName) {
            return kind;
        }
    }

    return nullptr;
}


static SimpleLoader<IconClass> loadClassesUplot("object", 0);

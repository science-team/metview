/***************************** LICENSE START ***********************************

 Copyright 2021 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QBrush>
#include <QFont>
#include <QGraphicsItem>
#include <QGraphicsProxyWidget>
#include <QPen>
#include <QPixmap>

#include "MvQFeatureItem.h"

class MvQPanel;

class MvQFeatureHandler;
class MvQFeatureTextItem;
class QTextEdit;

class MvQFeatureTextEditor : public QGraphicsProxyWidget
{
    Q_OBJECT
public:
    MvQFeatureTextEditor(MvQFeatureTextItemPtr item, QGraphicsItem* parent = nullptr);

    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;
    QTextEdit* editor() const;
    MvQFeatureTextItemPtr item() const;
    void itemChanged();
    void itemTextChanged();
    void finish();
    void initPos();
    bool canBeDraggedAt(const QPointF& scPos) const;
    bool containsScenePos(const QPointF&) const;
    void adjustCursorToMouseLeave(QMouseEvent* viewEvent);

protected Q_SLOTS:
    void textChanged();

Q_SIGNALS:
    void finishedEditing();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
    void updateFromItem();
    void adjustAlignment();

    MvQFeatureTextItemPtr item_;
    MvQFeatureHandler* featureHandler_{nullptr};
    QWidget* holderW_{nullptr};
    QTextEdit* editor_{nullptr};
    bool startup_{true};
    bool doNotNotifyItem_{false};

    int frame_{5};
    QPen outerPen_{QColor(90, 90, 90)};
    QPen innerPen_{QColor(100, 100, 100)};
    QPen shadowPen_{QColor(65, 65, 65), 2};
    QPen cornerPen_{QColor(120, 120, 120)};
    QBrush frameBrush_{QColor(220, 220, 220)};
    bool dragCursor_{false};
};


class MvQFeatureTextItem : public MvQFeatureItem
{
    friend class MvQFeatureMaker<MvQFeatureTextItem>;

public:
    void init(const QJsonObject& d) override;
    void toJson(QJsonObject& d) override;
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;
    void resize(QRectF rect) override;
    void resizeTo(const MvQFeatureGeometry& geom) override;
    void adjustBRect() override;
    void setText(QString t) override;
    void setTextFromEditor(QString t);
    QString text() const override { return text_; }
    QFont font() const { return font_; }
    QPen fontPen() const { return fontPen_; }
    QPen boxPen() const { return pen_; }
    QBrush boxBrush() const { return brush_; }
    QPointF textPos() const;
    Qt::AlignmentFlag alignment() const { return alignment_; }
    bool hasEditableText() const override { return true; }
    void textEditStarted(MvQFeatureTextEditor*);
    void textEditFinished();
    void initContents() override;
    int margin() const { return margin_; }
    void resizeFinished() override;
    QSizeF minSize() const override;
    void setStyle(const MvRequest& req = {}, bool init = false) override;

protected:
    using MvQFeatureItem::MvQFeatureItem;
    bool getRequestParameters() override;
    void editText() override;
    void adjustText();
    QRectF estimateTextRect() const;
    double halo() const override;

    QString text_;
    QRectF textRect_;
    Qt::AlignmentFlag alignment_{Qt::AlignLeft};
    //    int textSize_;
    int margin_{3};
    QSize minTextSize_;
    QFont font_;
    QPen fontPen_;
    QPen pen_;
    QBrush brush_;
    bool inTextEdit_{false};
    MvQFeatureTextEditor* editor_{nullptr};
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  MapView
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Describes the MapView class, which handles the
//  matching issues related to the Map View
//
//
// .CLIENTS:
//  DropAction
//
// .RESPONSIBILITY:
//
//  - Process the drop, asking the
//
//
// .COLLABORATORS:
//  MvRequest - extracts information from the request
//
// .DESCENDENT:
//
// .RELATED:
//  Presentable, SuperPage, Page, DataObject
//
// .ASCENDENT:
// PlotModView, PlotModService
//

#pragma once

#include "PlotModView.h"
#include "PlotModService.h"

class MapView : public PlotModView, public PlotModService
{
public:
    // Constructors
    MapView(Page&, const MvRequest&, const MvRequest&);
    MapView(const MapView&) = default;
    MapView& operator=(const MapView&) = delete;
    PlotModView* Clone() const override { return new MapView(*this); }

    // Destructor
    ~MapView() override = default;

    // Get the object name
    std::string Name() override;

    //  static void SaveRequest ( const Cached& path, MvRequest& viewRequest );
    // save a request for later use

    // Methods Overriden from PlotModView class
    void Drop(PmContext&) override;

    // Insert a new data request into the page hierarchy;
    MvIconList InsertDataRequest(MvRequest&) override;

    // Updates the current view
    void UpdateViewWithReq(MvRequest& viewRequest);

    // Replaces the current geographical area
    //  virtual void ReplaceArea ( const Location& coordinates, int izoom=-1 );

    // Draw the background (coastlines)
    void DrawBackground() override;

    // Draw the foreground (coastlines with land-sea shade)
    void DrawForeground() override;

    // Describe the contents of the view
    void DescribeYourself(ObjectInfo&) override;

    // Decode the data Unit
    void DecodeDataUnit(MvIcon&) override;

    // Call an application
    bool CallService(const MvRequest&, PmContext&) override;

    // Handle Coastlines icon
    // Get background/foreground icons
    int RetrieveBackground(MvRequest&);
    int RetrieveForeground(MvRequest&);

protected:
    void ConvertToSatellite(MvRequest& decoderRequest);

private:
    // Get background/foreground icons
    // const char* = "BACKGROUND" or "FOREGROUND"
    int RetrieveBackForeground(const char*, MvRequest&);
};

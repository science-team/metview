/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QWidget>

class QStackedWidget;
class MvQLayerWidget;
class MvQDataWidget;
class MgQLayerItem;

class LayerControlWidget : public QWidget
{
    Q_OBJECT

public:
    LayerControlWidget(MvQLayerWidget*, MvQDataWidget*, QWidget* parent = nullptr);
    ~LayerControlWidget() override = default;

public slots:
    void showLayerList();
    void showLayerInfo(MgQLayerItem*);

protected:
    enum ViewIndex
    {
        ListViewIndex = 0,
        InfoViewIndex = 1
    };
    MvQLayerWidget* layerListW_;
    MvQDataWidget* layerInfoW_;
    QStackedWidget* stackedW_;
};

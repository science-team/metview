/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MagPlusBatchService.h"

void MagPlusBatchService::CallMagPlus(MvRequest& in)
{
    MvMagRequest request(in);
    magplus_->execute(request);

    // The Magics log messages are not broadcast until the next log event - therefore, the
    // last log message will not be broadcast. We fix that by flushing the message streams
    // - we only need to do one of them, and all will be flushed behind the scenes.
    MagLog::info().flush();

    MvApplication::setKeepAlive(false);
}

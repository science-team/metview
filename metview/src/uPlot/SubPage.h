/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  SubPage
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Provides supprot for subpages in the PlotMod plot hierarchy
//
//
// .CLIENTS:
//  Page
//
// .RESPONSBILITIES:
//  - Include new data units
//  - Create data objects (leaves of the PlotMod tree )
//  - Keep track of the matching information
//
//
// .COLLABORATORS:
//  DatObject, MatchingInfo
//
// .ASCENDENT:
//  Presentable
//
//
// .DESCENDENT:
//  (none)
//
#pragma once

#include "Presentable.h"

class DataObject;

class SubPage : public Presentable
{
public:
    // Constructors
    // Constructor from a request
    SubPage(MvRequest& subpageRequest);

    SubPage(MvRequest&, int, long, long, const MatchingInfo&);

    SubPage(const SubPage&);

    Presentable* Clone() const override { return new SubPage(*this); }

    // Destructor
    ~SubPage() override;

    // Methods
    // Overriden from Presentable class

    // Draw me and all my children
    void Draw() override;

    // Draw Title Text only, positional is drawn by Page
    void DrawText() override;

    // Draw Legend
    virtual void DrawLegend();

    // Draw Import icons
    virtual void DrawImport();

    // Draws output file Prolog
    void DrawProlog() override;

    // Draws output file page header
    void DrawPageHeader() override;

    // Finalize drawing
    void DrawTrailer() override;  // CHECK IF THIS IS STILL NEEDED
    void DrawTrailerWithReq(MvRequest&) override;

    // Accept a  visitor
    void Visit(Visitor& v) override;

    long PictureId(const char* typeName, bool generate = false) override;

    // Local to subpage class
    // Set/return/check the canvas ( one per subpage )
    void SetCanvas(Canvas* canvas) override;
    Canvas& GetCanvas() const override;
    bool CheckCanvas();

    // Insert a new data unit (response to a drop action)
    void InsertDataUnit(int, long, long, const MatchingInfo&);

    // Remove icon from tree structure
    void RemoveIcon(MvIcon&) override;

    // Remove all data icon from tree structure
    void RemoveAllData() override { RemoveMyData(); }

    bool GetDrawPrior(DrawPriorMap*);

    bool GetDataObjects(std::list<DataObject*>&);

    // Indicate/determine whether a page is visited in background
    bool VisitInBackground() const override { return visitInBackground_; }
    void SetVisitInBackground(bool xx) override { visitInBackground_ = xx; }

    void PendingDrawingsAdd() override;
    void PendingDrawingsRemove() override;

    void SetVisibility(bool) override;
    bool IsVisible() override;

    // virtual void DescribeData ( MvRequest& );
    //  Add to request SubPage dataunit icons with dataobject requests

    // virtual Cached DescribeMatchingInfo ();
    //  Describes Matching info in the "DATE TIME LEVEL PARAM" format

    // Check if there is any Visdef associated with the input Dataunit.
    // This also includes Visdefs that are associated with the children of the Dataunit.
    // This function returns a flag (True: found a Dataunit) and a list of
    // associated Dataunits.
    bool CheckValidVisDefWithIcl(MvIcon&, MvRequest&, MvIconList&, int = 0);

private:
    // No copy allowed
    SubPage& operator=(const SubPage&);

    // Members
    Canvas* subpageCanvas_;
    bool visitInBackground_;
    bool hasTitleDraw_;

    using PictureIdMap = std::map<std::string, long>;
    PictureIdMap pictureIdMap_;  // for synchronization problems
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME: MagPlusBatchService
//
// .AUTHOR:
//  Fernando Augusto Mitsuo Ii
//     ECMWF, June 2010
//
// .SUMMARY:
//  Describes the batch mode of the MagPlusService class, which handles
//  the communication with Magics++.
//
// .CLIENTS:
//
// .RESPONSABILITY:
//
// .COLLABORATORS:
//  MvServiceTask
//
// .ASCENDENT:
//  MvClient, MagPlusService
//
// .DESCENDENT:
//
//
// .REFERENCES:
//  This class is based on the general facilities for
//  process communication used in METVIEW, especially the
//  MvClient and MvServiceTask facilities. Please refer
//  to these classes (or have a chat with Baudoin) for
//  more information.
//
//  This class implements a "singleton" as suggested
//  by Myers, "More Effective C+", page 130.
//

#pragma once

#include "MagPlusService.h"

class MagPlusBatchService : public MagPlusService
{
public:
    MagPlusBatchService() = default;
    ~MagPlusBatchService() override = default;

    void CallMagPlus(MvRequest&) override;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  VertProfView
//
// .AUTHOR:
//  Geir Austad and Gilberto Camara
//    Modified by Fernando Ii 03-2012
//
// .SUMMARY:
//  Describes the VProfileView class, which deals with
//  vertical profile application
//
// .CLIENTS:
//  Page
//
// .RESPONSIBILITY:
//
//  - When receiving a drop or a request in a page associated
//    to a service view, call the service to process the request
//
//  - When the application is finished, pass the request sent
//    by the application to the presentable, which then should
//    perform the data matching
//
//
// .COLLABORATORS:
//  MvRequest - extracts information from the request
//  MvTask - communication with METVIEW modules
//
// .ASCENDENT:
//  CommonXSectView
//
// .DESCENDENT:
//
// .RELATED:
//  Presentable, SuperPage, Page, DataObject
//
#pragma once

#include "CommonXSectView.h"

//---------------------------------------------------------------------
// VerticalProfile factory definition
class VertProfViewFactory : public PlotModViewFactory
{
    // --  Virtual Constructor - Builds a new VerticalProfile view
    virtual PlotModView* Build(Page&, const MvRequest&, const MvRequest&);

public:
    // Constructors
    VertProfViewFactory() :
        PlotModViewFactory("VProfileView") {}
};

//---------------------------------------------------------------------
// VerticalProfile factory definition to handle translation from Metview 3 to 4.
// This should be delete in the future. It is defined here for backwards
// compatibility.
class VertProfViewM3Factory : public PlotModViewFactory
{
    // --  Virtual Constructor - Builds a new VerticalProfile view
    virtual PlotModView* Build(Page&, const MvRequest&, const MvRequest&);

    // Translate Metview 3 view request to Metview 4
    MvRequest Translate(const MvRequest&);

public:
    // Constructors
    VertProfViewM3Factory() :
        PlotModViewFactory("VertProfM3View") {}
};

//---------------------------------------------------------------------
// VerticalProfile class definition
class VertProfView : public CommonXSectView
{
public:
    // -- Constructors
    VertProfView(Page&, const MvRequest&, const MvRequest&);
    VertProfView(const VertProfView&);
    PlotModView* Clone() const override { return new VertProfView(*this); }

    // -- Destructor
    ~VertProfView() {}

    // --   Methods overriden from CurveView class
    std::string Name() override;

    // Draw the background
    void DrawBackground() override {}

    // Describe the contents of the view
    void DescribeYourself(ObjectInfo&) override;

    // Initialize some variable members
    virtual void SetVariables(const MvRequest&, bool);

    // Update the current view
    bool UpdateView() override;

    // Check consistency between the View and Data Module
    bool ConsistencyCheck(MvRequest&, MvRequest&) override;

private:
    // Save some data specific to some DataApplication
    void ApplicationInfo(const MvRequest&) override;

    // No assignment
    VertProfView& operator=(const VertProfView&);

    // Variables members
    std::string yReverse_;  // Y axis direction: "on"/"off"
    bool xValuesAuto_;      // true: x values provided by user
    double xMin_, xMax_;    // X min/max values
};

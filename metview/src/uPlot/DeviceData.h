/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  DeviceData
//
// .AUTHOR:
//  Gilberto Camara, Baudoin Raoult and Fernando Ii
//
// .SUMMARY:
//  A completely "opaque" class which acts as a handler
//  for storing device information.
//
//  This class is  needed because PlotMod works VERY
//  differently when in interactive mode (using an XDevice)
//  and when in batch mode (using a PSDevice). Therefore,
//  this class allows each presentable to store the information
//  about its "device" (the media in which it is shown) in
//  a completely abstract way.
//
//  Each presentable HAS_A "Device Data" object, and this object
//  is the only way it can have access to its associated device
//
// .CLIENTS:
//  Presentable
//
//
// .RESPONSABILITIES:
//  Provide an encapsulation of the device information,
//  when used in connection with another class which may be either
//  a user interface class (such as PresentableWidget) or a plotting
//  class (such as PresentablePaper)
//
//
// .COLLABORATORS:
//  PresentableWidget, PresentablePaper (?)
//
//
// .BASE CLASS:
//
//
// .DERIVED CLASSES:
//
//
// .REFERENCES:
//   (If in doubt, try to answer the question:
//    "Of what use is a class that doesn't do anything ?")
//
//
#pragma once

class DeviceData
{
public:
    DeviceData() = default;
    virtual ~DeviceData() = default;
    DeviceData(const DeviceData&) = delete;
    DeviceData& operator=(const DeviceData&) = default;
};

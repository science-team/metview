/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  Visitor
//
// .AUTHOR:
//  Gilberto Camara, Baudouin Raoult and Fernando Ii
//
// .SUMMARY:
//  An abstract base class for definition of a "Visitor" pattern
//
//
// .CLIENTS:
//  Presentable
//
//
// .RESPONSABILITIES:
//  Provide an interface for the subclasses which visit the
//  page hierarchy
//
//
// .COLLABORATORS:
//
//
// .BASE CLASS:
//
//
// .DERIVED CLASSES:
//  Device, MacroVisitor
//
// .REFERENCES:
//  The relation between
//
#pragma once

class SuperPage;
class Page;
class SubPage;

class Visitor
{
public:
    Visitor() = default;
    virtual ~Visitor() = default;
    Visitor(const Visitor&) = delete;

    //  Visitor methods
    virtual void Visit(SuperPage&) = 0;

    virtual void Visit(Page&) = 0;

    virtual void Visit(SubPage&) = 0;

    //	virtual void Visit ( MagnifyPage& ) = 0;

    //	virtual void Visit ( EditMapPage& ) = 0;

private:
    // No copy allowed
    Visitor& operator=(const Visitor&) = default;
};

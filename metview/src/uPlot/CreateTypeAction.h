/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  CreateTypeAction
//
// .AUTHOR:
//  Fernando Ii
//
// .SUMMARY:
//  Describes the CreateTypeAction class, a derived class
//  for the various types of requests in PlotMod which skip
//  the processing of a request.
//
// .CLIENTS:
//  PlotModAction class, called by PlotMod main module (PlotMod.cc)
//
// .RESPONSIBILITY:
//  Will update the request according to the TYPE parameter
//
// .COLLABORATORS:
//
// .ASCENDENT:
//  PlotModAction
//
// .DESCENDENT:
//
// .REFERENCES:
//

#pragma once
#include "PlotModAction.h"

class CreateTypeAction : public PlotModAction
{
public:
    // Constructors
    CreateTypeAction(const Cached& name) :
        PlotModAction(name) {}

    // Destructor
    ~CreateTypeAction() override = default;

    // Methods
    static PlotModAction& Instance();

    // Overriden from Base Class (PlotModAction)
    void Execute(PmContext& context) override;
};

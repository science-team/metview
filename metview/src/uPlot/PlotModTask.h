/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// File PlotModTask
// Gilberto Camara - ECMWF Sep 97
//
// .NAME:
//  PlotModTask
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//
// .CLIENTS:
//
// .RESPONSABILITIES:
//
// .COLLABORATORS:
//
// .BASE CLASS:
//
// .DERIVED CLASSES:
//
// .REFERENCES:
//
//
#pragma once

#include <MvServiceTask.h>
#include "PmContext.h"

class PlotModTask : public MvServiceTask
{
public:
    // -- Contructors
    PlotModTask(MvClient* client,
                PmContext& context,
                const Cached& serviceName,
                const MvRequest& outRequest);

    // -- Destructor
    ~PlotModTask() override;  // Change to virtual if base class

    // -- Convertors
    // None

    // -- Operators
    // None

    // -- Methods
    // None

    // -- Overridden methods
    // None

protected:
    // -- Members
    // None

    // -- Methods
    // void print(ostream&) const; // Change to virtual if base class

    // -- Overridden methods
    // None

    // -- Class members
    // None

    // -- Class methods
    // None

private:
    // No copy allowed
    PlotModTask(const PlotModTask&);
    PlotModTask& operator=(const PlotModTask&) { return *this; }

    // -- Members
    PmContext& context_;
};

/***************************** LICENSE START ***********************************

 Copyright 2015 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// PlotAction class
//

#include "PlotAction.h"
#include "PlotMod.h"
#include "MagPlusService.h"
#include "PlotModConst.h"

void PlotAction::Execute(PmContext& context)
{
    // The request constains all the information to be sent to Magics
    // Just needs to add the output devices
    MvRequest fullReq = PlotMod::Instance().OutputFormat()("OUTPUT_DEVICES");
    fullReq = fullReq + context.InRequest();

    // Call MAGICS to process the request
    MagPlusService::Instance().SetRequest(fullReq);

    // Check where to send the output file(s)
    // If destination is file, Magics has already created it
    MvRequest info = PlotMod::Instance().OutputFormatInfo();
    if (strcmp((const char*)info("DESTINATION"), MVPRINTER) == 0) {
        // Call lpr command
        std::ostringstream tmpp;
        tmpp << "lpr -P" << (const char*)info("PRINTER_NAME");
        tmpp << " -#" << (int)info("NUMBER_COPIES");
        tmpp << " " << (const char*)info("FILE_NAME");
        system(tmpp.str().c_str());

        // Remove temporary file
        std::ostringstream rm;
        rm << "rm -f " << (const char*)info("FILE_NAME");
        system(rm.str().c_str());
    }
    else if (strcmp((const char*)info("DESTINATION"), MVPREVIEW) == 0) {
        // add code later
    }

    // Advance to the end the input context
    context.AdvanceToEnd();
}

// This is the exemplar object for the PlotAction class
static PlotAction plotActionInstance("Plot");

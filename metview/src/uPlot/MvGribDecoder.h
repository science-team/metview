/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  GribDecoder
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  A concrete  class for decoding GRIB files
//  (which uses EMOSlib and Mars' interface to Emoslib)
//
// .CLIENTS:
//  PlotModMatcher, Graphics Engine
//
// .RESPONSABILITIES:
//  For each data time, decode the GRIB header
//  and provide information on parameter,
//  date, time and level
//
// .COLLABORATORS:
//  Date, Time, Emos, PathName
//
// .BASE CLASS:
//  Decoder
//
// .DERIVED CLASSES:
//
//
// .REFERENCES:
//
//  The design of this class is based on the "Factory" pattern
//  ("Design Patterns", page. 107).
//
//
#pragma once

#include <MvFieldSet.h>
#include "MvDecoder.h"

class GribDecoder : public Decoder
{
public:
    // Contructors
    GribDecoder(const MvRequest& inRequest);

    // Destructor
    ~GribDecoder() override;

    // Overridden methods from Decoder class
    // Goes to the next data and returns false if
    // there are no more fields
    bool ReadNextData() override;

    MatchingInfo CreateMatchingInfo() override;

private:
    // WARNING, fieldSetIterator_ should be declared AFTER
    // fieldSet_ as the former is intialised with the later
    MvFieldSet fieldSet_;
    MvFieldSetIterator fieldSetIterator_;
    MvField currentField_;

    // No copy allowed
    GribDecoder(const GribDecoder&);
    GribDecoder& operator=(const GribDecoder&) { return *this; }
};

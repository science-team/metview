/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QApplication>
#include <QBrush>
#include <QColor>
#include <QDebug>
#include <QPainter>

#include "MvQOdbStatModel.h"

//======================================
// Item delegate
//======================================

MvQOdbStatDelegate::MvQOdbStatDelegate(QWidget* parent) :
    QStyledItemDelegate(parent)
{
}

void MvQOdbStatDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option,
                               const QModelIndex& index) const
{
    QStyledItemDelegate::paint(painter, option, index);
    QRect rect = option.rect;
    QLine line(rect.x(), rect.y(), rect.x() + rect.width(), rect.y());

    painter->setPen(QPen("#BBBBBB"));
    painter->drawLine(line);
}


//======================================
// Item model
//======================================

/*void MvQOdbStatModel::layersAreAboutToChange()
{
    beginResetModel();
}

void MvQOdbStatModel::resetLayers()
{
    layers_=scene_->layerNodes();
    endResetModel();
}*/


void MvQOdbStatModel::setData(QList<MvQOdbStat*> data)
{
    data_ = data;
    reset();
}

int MvQOdbStatModel::columnCount(const QModelIndex& /* parent */) const
{
    return 1;
}

int MvQOdbStatModel::rowCount(const QModelIndex& index) const
{
    if (!index.isValid()) {
        return data_.count();
    }
    else {
        return 0;
    }
}


QVariant MvQOdbStatModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid() || index.column() != 0) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        return data_.at(index.row())->name();
    }
    else if (role == Qt::BackgroundRole) {
        if (data_.at(index.row())->isLoaded()) {
            if (index.row() % 2 == 0)
                // return QBrush(QColor(70,180,40));
                // return QBrush(QColor(92,206,113));
                return QColor("#FFE6BF");
            else
                // return QBrush(QColor(187,238,202));
                return QColor("#FFF2DE");
        }
        else {
            return QBrush(Qt::white);
        }
    }
    return QVariant();
}


QVariant MvQOdbStatModel::headerData(const int section, const Qt::Orientation orient, const int role) const
{
    return QVariant();
}


QModelIndex MvQOdbStatModel::index(int row, int column, const QModelIndex& parent) const
{
    return createIndex(row, column, 0);
}


QModelIndex MvQOdbStatModel::parent(const QModelIndex& index) const
{
    return QModelIndex();
}

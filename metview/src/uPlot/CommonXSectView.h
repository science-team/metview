/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  CommonXSectView
//
// .AUTHOR:
//  Geir Austad. Modified by Fernando Ii 03-2012
//
// .SUMMARY:
//  Convenience class, for implementing methods common
//  to XSectView and AverageView.
//
// .CLIENTS:
//  DropAction
//
// .RESPONSIBILITY:
//
// .ASCENDENT:
//  PlotModView, PlotModService
//
// .DESCENDENT:
//  XSectView, AverageView
//
//
#pragma once

#include <string>

#include "Page.h"
#include "PlotModService.h"
#include "PlotModView.h"


class CommonXSectView : public PlotModView, public PlotModService
{
public:
    // -- Constructor
    CommonXSectView(Page&, const MvRequest&, const MvRequest&);
    CommonXSectView(const CommonXSectView&) = default;
    CommonXSectView& operator=(const CommonXSectView&) = delete;

    // -- Destructor
    ~CommonXSectView() override = default;

    // --  Methods overriden from PlotModView class
    std::string Name() override;

    // --  Methods that the subclasses must provide

    // Decode the data Unit
    void DecodeDataUnit(MvIcon& dataUnit) override;

    // Process a drop
    void Drop(PmContext& context) override;

    // Draw the foreground.
    void DrawForeground() override {}

    // Set data unit id
    void ParentDataUnitId(int parentDataUnitId)
    {
        viewRequest_("_PARENT_DATAUNIT_ID") = parentDataUnitId;
        appViewReq_("_PARENT_DATAUNIT_ID") = parentDataUnitId;
        parentDataUnitId_ = parentDataUnitId;
    }

    int ParentDataUnitId() const { return parentDataUnitId_; }

    bool ConsistencyCheck(MvRequest&, MvRequest&) override { return true; }

protected:
    // Call a service and wait its return
    bool callServiceInternal(const MvRequest&, PmContext&, MvRequest&);

    void ApplicationName(const std::string& appname)
    {
        applicationName_ = appname;
    }

    std::string& ApplicationName() { return applicationName_; }

    void Draw(SubPage*) override;

    // Save some data specific to some DataApplication
    // It must be pure virtual because ThermoView and
    // HovmoellerView have their own implementation.
    virtual void ApplicationInfo(const MvRequest&) = 0;

    // Insert a new data request into the page hierarchy
    MvIconList InsertDataRequest(MvRequest& dropRequest) override;

    // Get output requests from executing a Module (they all have the
    // same tag _MODULEID
    bool DropFromModule(MvRequest&, MvRequest&, bool&);

    double latMin_{0.};
    double latMax_{0.};
    double lonMin_{0.};
    double lonMax_{0.};
    double yMin_{0.};
    double yMax_{0.};
    std::string applicationName_;
    MvRequest appViewReq_;  // original application view request
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// File PlotModAction
// Gilberto Camara - ECMWF Apr 97
//
// .NAME:
//  PlotModAction
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Describes the PlotModAction class, a base class
//  for the various types of actions in PlotMod
//
// .CLIENTS:
//  PlotMod main module (PlotMod.cc)
//
// .RESPONSABILITY:
//  This class will provide a run-time binding
//  for the PlotMod actions. The idea is to define, at run-time,
//  the action which is called based on the contents of a request.
//
//
// .COLLABORATORS:
//  Builder, Matcher, GraphicsEngine
//
// .ASCENDENT:
//
// .DESCENDENT:
//  VisualiseAction, EditMapAction, DropAction, HardcopyAction
//
// .REFERENCES:
//
//  The design of this class is based on the "Factory" pattern
//  ("Design Patterns", page. 107).
//
//  For additional references please see Coplien's chapther 8
//  ("Programming with Exemplars")
//
#pragma once

#include "Prototype.hpp"
#include "PmContext.h"

class PlotModAction;

struct ActionTraits
{
    using Type = PlotModAction;
    static Type& DefaultObject();
};

class PlotModAction : public Prototype<ActionTraits>
{
public:
    // Contructors
    PlotModAction(const Cached& actionName) :
        Prototype<ActionTraits>(actionName, this) {}

    // Destructor
    ~PlotModAction() override = default;

    // Methods
    virtual void Execute(PmContext&) = 0;

private:
    // No copy allowed
    PlotModAction(const PlotModAction&) = delete;
    PlotModAction& operator=(const PlotModAction&) { return *this; }

    // Friends
    // friend std::ostream& operator<<(std::ostream& ostr, const PlotModAction& act)
    //	{ act.print(ostr); return ostr; }
};

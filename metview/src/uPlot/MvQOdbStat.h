/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QStringList>

class MvQOdbStat
{
public:
    MvQOdbStat(QString name, QString query) :
        name_(name),
        query_(query),
        loaded_(false){};

    bool isLoaded() { return loaded_; }
    QString name() { return name_; }
    QString query() { return query_; }
    const QList<QStringList>& resultData() const { return resultData_; }
    const QStringList& resultName() const { return resultName_; }
    void addResult(QString name, QStringList& data)
    {
        resultName_ << name;
        resultData_ << data;
    }

protected:
    QString name_;
    QString query_;
    QStringList resultName_;
    QList<QStringList> resultData_;
    bool loaded_;
};

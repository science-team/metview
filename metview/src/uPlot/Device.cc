/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Device.h"
#include "Presentable.h"
#include "MvRequestUtil.hpp"
#include "XDevice.h"
#include "PlotMod.h"

Cached
DeviceFactoryTraits::FactoryName(const MvRequest& request)
{
    Cached name;
    MvRequest req1 = request("OUTPUT_DEVICES");
    if (req1)
        name = req1.getVerb();
    else
        name = "ERROR";

    return name;
}

Device*
DeviceFactoryTraits::DefaultObject(const MvRequest& request)
{
    return new XDevice(request);
}

Device::Device(const MvRequest& deviceRequest) :
    deviceRequest_(deviceRequest),
    outFileName_("PlotFile1"),
    outTmpFileName_("PlotTmpFile1")
{
}

Device::~Device() = default;

// Implement the Visitor pattern
void Device::Initialise(Presentable& p)
{
    p.Visit(*this);
}

// -- METHOD :  CreateRequest
//
// -- PURPOSE:  Create a device driver request, based on a superpage request
// -- INPUT  :  Superpage request
//
// -- OUTPUT :  Device driverRequest
MvRequest
Device::CreateRequest(const MvRequest&)
{
    return PlotMod::Instance().OutputFormat();
#if 0  // U032010
	// If the device driver request is missing, plot on the screen
	if ( !IsParameterSet ( (MvRequest&)inRequest, "_DRIVER" ) )
		return PlotMod::Instance().MakeDefaultOutputFormat();

	// Plot not on the screen
	MvRequest deviceRequest;
	deviceRequest = inRequest.getSubrequest ("_DRIVER");
	double  width = inRequest ( "CUSTOM_WIDTH" );
	double  height= inRequest ( "CUSTOM_HEIGHT" );
       	deviceRequest ( "CUSTOM_WIDTH" ) = width;
	deviceRequest ( "CUSTOM_HEIGHT") = height;

	// Does the deviceRequest already contain a path ?
	const char*  path = deviceRequest ( "PATH" );
	if ( path == 0 )
	{
		Cached mvpath = ObjectInfo::ObjectPath ( inRequest );
		deviceRequest ("PATH") = (const char* ) mvpath;
	}

	return deviceRequest;
#endif
}

#if 0
Cached
Device::DeviceType ()
{
  if ( ( const char *) deviceRequest_ ( "DESTINATION" ) )
	  return deviceRequest_ ( "DESTINATION" );
  else
	  return deviceRequest_ ( "FORMAT" );
}
#endif

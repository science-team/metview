/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <Assertions.hpp>

#include "XDevice.h"
#include "GraphicsEngine.h"
#include "SubPage.h"

// class XDeviceFactory - builds X Devices
class XDeviceFactory : public DeviceFactory
{
    Device* Build(const MvRequest& deviceRequest) override
    {
        return new XDevice(deviceRequest);
    }

public:
    XDeviceFactory(const Cached& name) :
        DeviceFactory(name) {}
};

// Instance of XDevice which is loaded in the "factory"
static XDeviceFactory x11DeviceFactoryInstance("SCREEN");

///////////////////////////////////////////////////////////////

XDevice::XDevice(const MvRequest& deviceRequest) :
    Device(deviceRequest)
{
    // Empty
}

XDevice::~XDevice() = default;

// -- Implementation of the "Visitor" pattern
// -- Each type of node associated to widgets (superpage, page and subpage)
//    is visited by the "XDevice" object, which performs the appropriate action
//    of creating the widgets associated with each presentable
void XDevice::Visit(SuperPage&)
{
}

void XDevice::Visit(Page&)
{
}

void XDevice::Visit(SubPage& subpage)
{
    // Creation a new canvas - there is one canvas per subpage.
    if (subpage.CheckCanvas())
        return;  // Canvas already created

    GraphicsEngine& ge = subpage.GetGraphicsEngine();
    // U	XCanvas* canvas = ge.MakeXCanvas ( *this, spw,
    Canvas* canvas = ge.MakeCanvas(*this, subpage.GetLocation(), subpage.GetMySize(), subpage.Id());
    ensure(canvas != nullptr);

    // U	subpage.SetDeviceData ( spw );
    subpage.SetCanvas(canvas);
    // U	spw->SetCanvas ( canvas );
    // FAMI	SetTranslations( (*canvas), spw );

    // FAMI	subpage.SetVisibility ( true );
    // U	spw->Manage();
}

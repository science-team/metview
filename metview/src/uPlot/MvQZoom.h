/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include <QBrush>
#include <QGraphicsItem>
#include <QPen>

#include "MvQPlotItem.h"

class MgQLayoutItem;
class MgQSceneItem;

class MvQZoom : public MvQPlotItem
{
    Q_OBJECT

public:
    enum CurrentAction
    {
        NoAction,
        ZoomAction
    };

    MvQZoom(MgQPlotScene*, MvQPlotView*, QGraphicsItem* parent = nullptr);
    ~MvQZoom() override;

    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;
    QRectF boundingRect() const override;

    void mousePressEventFromView(QMouseEvent*) override;
    void mouseMoveEventFromView(QMouseEvent*) override;
    void mouseReleaseEventFromView(QMouseEvent*) override;

    CurrentAction currentAction() { return currentAction_; }
    bool zoomWasPerformedAfterMouseRelease() { return zoomWasPerformedAfterMouseRelease_; }

signals:
    void zoomRectangleIsDefined(const std::string&, const std::string&);
    void zoomRectangleIsDefined(QString, double, double, double, double);
    void mousePressInSceneItem(MgQSceneItem*);

private:
    QPen pen_;
    QBrush brush_;
    QRectF zoomRect_;
    QPointF zoomRectOrigin_;
    CurrentAction currentAction_;
    MgQLayoutItem* zoomLayout_;
    bool zoomWasPerformedAfterMouseRelease_;
};

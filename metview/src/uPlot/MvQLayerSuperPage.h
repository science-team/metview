/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QListView>

#include "MvIconDataBase.h"
#include "MvQDragDrop.h"
#include "MvQLayerContentsIcon.h"

class MvQLayerSuperPageModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    // Constructors & destructors
    MvQLayerSuperPageModel(QObject* parent = 0);
    ~MvQLayerSuperPageModel();

    // View-model functions
    int columnCount(const QModelIndex&) const;
    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const;
    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex&) const;

    // Member functions
    // Initialize icon list
    void setIcons(MvIconList&);

    //   bool add (const MvQLayerContentsIcon&);
    //   void remove (const MvQLayerContentsIcon&);
    MvQLayerContentsIcon* indexToIcon(const QModelIndex&);

    // Function trigguered by the parent widget because layers have changed
    // (e.g. uPlot has requested a new visualisation window).
    // The icons in the internal list may not have the correct ID because they
    // were initialised by -1 and added to the list before calling uPlot.
    // If an icon is sent to uPlot for the first time, uPlot registers it in the
    // database and assigns a new ID.
    // Check if the icons's ID in the internal list need to be updated.
    void reset();

    // The default id for a SuperPage is 1
    int presentableId()
    {
        return 1;
    }

protected:
    QList<MvQLayerContentsIcon> icons_;
};

//----------------------------------------------------------------------

class MvQLayerSuperPageView : public QListView
{
    Q_OBJECT

public:
    MvQLayerSuperPageView(MvQLayerSuperPageModel*, QWidget* parent = 0);
    ~MvQLayerSuperPageView();

public slots:

    //   void slotContextMenu (const QPoint&);
    void slotDoubleClickItem(const QModelIndex&);

protected slots:

    void slotEdit();
    void slotDelete();
    void slotSave();
    void slotSaveAs();

signals:

    void iconDropped(const MvQDrop&);

protected:
    void currentChanged(const QModelIndex&, const QModelIndex&);

    void dragEnterEvent(QDragEnterEvent*);
    void dragLeaveEvent(QDragLeaveEvent*);
    void dragMoveEvent(QDragMoveEvent*);
    void dropEvent(QDropEvent*);

    void createContextMenu();

    MvQLayerSuperPageModel* model_;

    QList<QAction*> actions_;
    QAction* actionEdit_;
    QAction* actionSave_;
    QAction* actionSaveAs_;
    QAction* actionDelete_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QColor>
#include <QSortFilterProxyModel>
#include <QStyledItemDelegate>

#include "MvQKeyProfileModel.h"

class MgQPlotScene;
class MgQSceneItem;


class MvQStepDelegate : public QStyledItemDelegate
{
public:
    MvQStepDelegate(QWidget* parent = nullptr);
    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const override;
};


class MvQStepModel : public MvQKeyProfileModel
{
    Q_OBJECT
public:
    MvQStepModel(QObject*);

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;


    MgQSceneItem* stepData() { return stepData_; }
    void stepDataIsAboutToChange();
    void setStepData(MgQSceneItem*);

signals:
    void stepCacheStateChanged();

protected:
    // QString label( const int,const int) const;

private:
    MgQSceneItem* stepData_;
    QColor cachedBg_;
};

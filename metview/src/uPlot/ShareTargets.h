/***************************** LICENSE START ***********************************

 Copyright 2017 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <vector>
#include "MvRequest.h"

class MvQProgressManager;

/* The classes here define each 'share' target of uPlot */

//**********************************************************
// The base class from which the 'real' share targets inherit

class ShareTarget
{
public:
    ShareTarget(const std::string&, const std::string&);

    virtual std::string& name() { return name_; }
    virtual std::string& icon() { return icon_; }

    virtual bool dialog(MvRequest&, MvQProgressManager* pm = nullptr) = 0;
    virtual void customisePlotRequest() = 0;

protected:
    MvRequest reqPlot_;       // plotting requests
    MvQProgressManager* pm_;  // progress manager

private:
    std::string name_;  // share target name
    std::string icon_;  // share target icon
};


//**********************************************************
// The registry of share targets

class ShareTargetManager : public std::vector<ShareTarget*>
{
public:
    ShareTargetManager() = default;
    ~ShareTargetManager();

    static ShareTargetManager* instance();

private:
    static ShareTargetManager* instance_;
};


//**********************************************************
//#ifdef MV_SHARE_GLOBE_BUILD

#include <QObject>

class GlobeShareTarget : public QObject, public ShareTarget
{
    Q_OBJECT

public:
    GlobeShareTarget(QObject* parent = nullptr);
    ~GlobeShareTarget() override;

    bool dialog(MvRequest&, MvQProgressManager* pm = nullptr) override;
    void edited(MvRequest&);
    void customisePlotRequest() override;

public slots:
    bool upload();
    void fileChanged(const QString&);

private:
    std::string class_{"GLOBE"};
    std::string tmp_path_;        // temporary path where the png files will be generated
    std::string out_dir_;         // directory where the output file mp4 will be created
    std::string out_fname_;       // output file name
    std::string out_frame_rate_;  // output frame rate
    int out_image_width_{256};    // output image width
};

//#endif

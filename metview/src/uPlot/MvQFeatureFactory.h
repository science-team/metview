/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include "MvQFeatureFwd.h"

class WsType;
class MvQPlotView;
class QGraphicsItem;
class QJsonObject;

class MvQFeatureFactory
{
public:
    MvQFeatureFactory(const std::string&);
    MvQFeatureFactory(const MvQFeatureFactory&) = delete;
    MvQFeatureFactory& operator=(const MvQFeatureFactory&) = delete;
    virtual ~MvQFeatureFactory() = default;

    static MvQFeatureItemPtr create(WsType*);
    static MvQFeatureItemPtr create(const QJsonObject& d);
    static MvQFeatureItemPtr clone(MvQFeatureItemPtr);

private:
    virtual MvQFeatureItemPtr make(WsType*) = 0;
    static MvQFeatureItemPtr createInner(WsType*);
};

template <class T>
class MvQFeatureMaker : public MvQFeatureFactory
{
public:
    using MvQFeatureFactory::MvQFeatureFactory;

private:
    MvQFeatureItemPtr make(WsType* f) override
    {
        return MvQFeatureItemPtr(new T(f));
    }
};

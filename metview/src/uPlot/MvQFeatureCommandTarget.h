/***************************** LICENSE START ***********************************

 Copyright 2021ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QPointF>
#include <QString>
#include <QStringList>

#include "MvQFeatureFwd.h"

// indentifies the recepient of a context menu event or a shortcut
class MvQFeatureCommandTarget
{
public:
    enum SelectionMode
    {
        ContextMenuMode,
        ShortcutMode
    };
    MvQFeatureCommandTarget(SelectionMode mode) :
        mode_(mode) {}

    SelectionMode mode() const { return mode_; }
    MvQFeatureItemPtr targetItem() const { return targetItem_; }
    QPointF scenePos() const { return scenePos_; }
    QString itemMode() const { return itemMode_; }
    QString itemType() const { return itemType_; }
    int nodeIndex() const { return nodeIndex_; }
    bool inSelector() const { return inSelector_; }
    bool isCommmandEnabled(QString cmd) const { return !disabledCommands_.contains(cmd); }

    void eval(const QPointF& scenePos);
    void setTargetItem(MvQFeatureItemPtr);
    void setTargetNodeItem(MvQFeatureItemPtr item, int nodeIndex);
    void setInSelector(bool b) { inSelector_ = b; }
    void addDisabledCommands(QStringList lst);

private:
    SelectionMode mode_{ContextMenuMode};
    QString itemMode_{"-"};
    QString itemType_{"-"};
    int nodeIndex_{-1};
    MvQFeatureItemPtr targetItem_;
    bool inSelector_{false};
    QPointF scenePos_;
    QStringList disabledCommands_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  PlotPageBuilder
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Describes the PlotPageBuilder class, an concrete class for
//  building the PlotMod page hierarchy from a "SUPERPAGE" request
//
// .DESCRIPTION:
//  This class is based on the "Builder" pattern (see
//  "Design Patterns" book, page 97).
//
//
// .DESCENDENT:
//
// .RELATED:
//  Presentable, PlotPage, Page, DataObject
//
// .ASCENDENT:
// PlotModBuilder
//
#pragma once

#include "PlotModBuilder.h"

class PlotPageBuilder : public PlotModBuilder
{
public:
    // Constructors
    PlotPageBuilder(Cached name) :
        PlotModBuilder(name) {}

    //  Destructor
    ~PlotPageBuilder() override = default;

    // Methods
    static PlotPageBuilder& Instance();

    // Overriden from PlotModBuilder class
    //  Builds the superpage and its children
    //  Return a pointer to the node which has been created
    Presentable* Execute(PmContext&) override;

private:
    // No copy allowed
    PlotPageBuilder(const PlotPageBuilder&);
    PlotPageBuilder& operator=(const PlotPageBuilder&);
};

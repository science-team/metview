/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "AnnotationView.h"
#include "DataObject.h"
#include "ObjectList.h"
#include "Page.h"
#include "PmContext.h"
#include "SubPage.h"

// Class AnnotationViewFactory - builds service views
class AnnotationViewFactory : public PlotModViewFactory
{
    // --  Virtual Constructor - Builds a new AnnotationView
    PlotModView* Build(Page& page,
                       const MvRequest& contextRequest,
                       const MvRequest& setupRequest) override
    {
        return new AnnotationView(page, contextRequest, setupRequest);
    }

public:
    AnnotationViewFactory() :
        PlotModViewFactory("AnnotationView") {}
};

static AnnotationViewFactory annotationViewFactoryInstance;

//------------------------------------------------------------------

AnnotationView::AnnotationView(Page& owner,
                               const MvRequest& viewRequest,
                               const MvRequest& setupRequest,
                               const std::string& viewName) :
    PlotModView(owner, viewRequest, setupRequest),
    viewName_(viewName)
{
}

std::string
AnnotationView::Name()
{
    int id = Owner().Id();
    return (const char*)ObjectInfo::ObjectName(viewRequest_, "AnnotationView", id);
}

void AnnotationView::Drop(PmContext& context)
{
    // Process the drop
    MvRequest dropRequest = context.InRequest();
    while (dropRequest) {
        MvRequest request = dropRequest.justOneRequest();
        const char* verb = request.getVerb();

        if ((const char*)verb == PLOTSUPERPAGE) {
            context.AdvanceTo(PLOTSUPERPAGE);
            return;
        }
        else if ((const char*)verb == NEWPAGE) {
            context.AdvanceTo(NEWPAGE);
            return;
        }
        else
            Owner().InsertCommonIcons(request);

        dropRequest.advance();
    }

    // Consume all of the input request (the service will use it)
    context.AdvanceToEnd();
}

void AnnotationView::DescribeYourself(ObjectInfo& description)
{
    // Convert my request to macro
    std::set<Cached> skipSet;
    description.ConvertRequestToMacro(viewRequest_, PUT_END, MacroName().c_str(), viewName_.c_str(), skipSet);
}

bool AnnotationView::UpdateView()
{
    // Parameter SUBPAGE_FRAME has a default ON in Magics. However, this parameter has
    // a default value OFF in AnnotationView. Magics needs to be informed if the value
    // OFF is to be used.
    if ((const char*)viewRequest_("SUBPAGE_FRAME"))
        return true;

    // It should use the AnnotationView default value for parameter SUBPAGE_FRAME
    MvRequest exp = ObjectList::ExpandRequest(viewRequest_, EXPAND_DEFAULTS);
    viewRequest_("SUBPAGE_FRAME") = exp("SUBPAGE_FRAME");

    return true;
}

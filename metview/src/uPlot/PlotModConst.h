/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  PlotModConst
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  General Definitions, Enumerations,
//  Macros and Constants used in PlotMod
//

#pragma once

// ImageParameter Number taken from WMO Code Table 2
const int GRIB_IMAGE_PARAM_NUMBER = 127;

// Maximum number of Legends package allowed in Magics
const short MAX_LEGENDS_PACKAGE = 5;

// Data Requests
static std::string GRIB = "GRIB";
static std::string NETCDF = "NETCDF";
static std::string BUFR = "BUFR";

// Page visualization Definitions
static Cached MCOAST = "MCOAST";
static Cached PCOAST = "PCOAST";
static std::string MTEXT = "MTEXT";
static std::string PTEXT = "PTEXT";
static std::string MLEGEND = "MLEGEND";
static Cached MAXIS = "MAXIS";
static Cached PAXIS = "PAXIS";
static Cached ANNOTATION = "ANNOTATION";
static std::string MIMPORT = "MIMPORT";
static std::string GEOLAYERS = "GEOLAYERS";
static std::string MTHERMOGRID = "MTHERMOGRID";
static std::string MTAYLORGRID = "MTAYLOR";

// Data Visualization Definitions
static Cached MCONT = "MCONT";
static Cached PCONT = "PCONT";
static Cached MGRAPH = "MGRAPH";
static Cached PGRAPH = "PGRAPH";
static Cached MWIND = "MWIND";
static Cached PWIND = "PWIND";
static Cached MSYMB = "MSYMB";
static Cached PSYMB = "PSYMB";
static Cached PTACH = "PTACH";
static Cached ISOTACHS = "ISOTACHS";
static std::string MTHERMO = "MTHERMO";

// Command Definitions
static Cached DEVICEDRIVER = "DEVICE_DRIVER";
static std::string PLOTSUPERPAGE = "PLOT_SUPERPAGE";
static Cached PLOTPAGE = "PLOT_PAGE";
static Cached PAGES = "PAGES";
static std::string NEWPAGE = "NEWPAGE";

// Others
static Cached ON = "ON";

// Views
static const std::string MAPVIEW = "MAPVIEW";
static const std::string GEOVIEW = "GEOVIEW";
static const std::string CARTESIANVIEW = "CARTESIANVIEW";
static const std::string DEFAULTVIEW = "GEOVIEW";
static const std::string THERMOVIEW = "THERMOVIEW";

// Output devices
static const char* const MVPRINTER = "PRINTER";
static const char* const MVSCREEN = "SCREEN";
static const char* const MVPREVIEW = "PREVIEW";
static const char* const MVFILE = "FILE";
static const char* const MVSCREEN_DRIVER = "QTOUTPUT";

// Weather features/symbols
static Cached WEATHERSYMBOL = "WEATHERSYMBOL";

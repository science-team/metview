/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  NewpageAction
//
// .AUTHOR:
//  Fernando Ii
//
// .SUMMARY:
//  Describes the NewpageAction class, a derived class
//  for the various types of requests in PlotMod which indicate
//  that a new superpage branch will be created in the Tree.
//
// .CLIENTS:
//  PlotModAction class, called by PlotMod main module (PlotMod.cc)
//
// .RESPONSIBILITY:
//  Will set a flag to indicate that a new superpage branch is going
//  to be created.
//
// .COLLABORATORS:
//
// .ASCENDENT:
//  PlotModAction
//
// .DESCENDENT:
//
// .REFERENCES:
//

#pragma once
#include "PlotModAction.h"

class NewpageAction : public PlotModAction
{
public:
    // Constructors
    NewpageAction(const Cached& name) :
        PlotModAction(name) {}

    // Destructor
    ~NewpageAction() override = default;

    // Methods
    static PlotModAction& Instance();

    // Overriden from Base Class (PlotModAction)
    void Execute(PmContext& context) override;
};

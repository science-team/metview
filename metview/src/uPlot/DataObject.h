/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// .NAME:
//  DataObject
//
// .AUTHOR:
//  Gilberto Camara, Baudouin Raoult and Fernando Ii
//
// .SUMMARY:
//  Provides support for the data object class, the lowest
//  level of the PlotMod tree hierarchy
//
//
// .CLIENTS:
//
//
//
// .RESPONSABILITIES:
//
//
//
// .COLLABORATORS:
//
//
//
// .BASE CLASS:
//
//
// .DERIVED CLASSES:
//
//
// .REFERENCES:


#pragma once

#include <map>
#include <string>
#include <utility>
#include <vector>

#include "Presentable.h"

class DataObject : public Presentable
{
public:
    // Contructors
    DataObject(int duId, long off1, long off2, const MvRequest& dataInfo);
    DataObject(MvRequest&);
    DataObject(const DataObject&);

    Presentable* Clone() const override { return new DataObject(*this); }

    // Destructor
    ~DataObject() override = default;  // Change to virtual if base class

    // Overridden from Presentable
    //        virtual void DuplicateChildren(const Presentable&);

    // Class members
    int DataUnitId() { return myDataUnitId_; }

    // Return building request, IconDataBase request if GRIB,
    // IMAGE or VECTOR_FIELD, updated with grib index.
    //        MvRequest DataRequest ();

    DrawPriorMap DrawPriority();
    DrawPriorMap DrawPriority(MvIconList&);

    bool AppendDataUnit(int dataunitId, long offset, long offset2, const MvRequest& req);
    bool Match(int dataunitId, const MvRequest& req);

    void DrawDataVisDef() override;

    void DataUnit(MvIcon& dataUnit) override;

#if 0
	virtual void RetrieveTitleFromMagics( MvIcon& );

	// Methods
	// Overrriden from Presentable Class
	virtual bool IsVisible()
		{ return true;}

	virtual void Insert(Presentable*) {}

	virtual void Remove(Presentable*) {}

	void EraseDraw ( int visDefId = 0 );

	virtual void EraseDefaultDraw ();

	void SetTitle  ( const MvRequest& reqst );
#endif

    void GetTitle(MvRequest& reqst, int* order);
    //   	void SetLegend ( const MvRequest& reqst );
    bool GetLegend(MvRequest& reqst, int* order);
    //	bool RemoveLegend ( std::string );

    bool RetrieveMyVisDefList(MvIcon& dataUnit, MvIconList& visdefList);

    // Update some visdef parameters according to the dataunit type
    void UpdateVisDef(MvRequest, MvIconList&);

    void SetTextOrigin(MvRequest&);

    // True if Magics can produces a title for this data object
    bool MagicsTitle();

private:
    // No copy allowed
    DataObject& operator=(const DataObject&);

    // Members
    int myDataUnitId_{0};
    int dimFlag_{0};  // 0 - no information
                      // 1 = one dimensional fields
                      // 2 = two dimensional fields
                      // 3 = one and two dimensional fields
                      //	vector <long> myDataIndex_;
                      //	vector <long> myDataIndex2_;
    // this second index holds the index of a possible second component
    // in case of vector data

    using LegendPackage = std::pair<std::string, std::vector<int>>;
    using LegendsMap = std::map<std::string, LegendPackage>;
    LegendsMap legends_;

    //        typedef std::map<int,std::string> SegmentNameMap;
    //        SegmentNameMap segmentNameMap_;

    std::vector<int> titleText_;

    MvRequest titleRequest_;  // Text information related to 'GRIB_TEXT_...'
                              // parameters, which are defined in PCONT
};

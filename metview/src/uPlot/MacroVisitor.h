/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  MacroVisitor
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Defines a class for generating macro programs
//  from a display window.
//
// .CLIENTS:
//  SuperPage, Page
//
//
// .RESPONSABILITIES:
//  - For the caller (superpage or page), save its name and
//    requests which was used to create it.
//  - For the caller and all its children, save the drop
//    actions associated with data units and visual definitions
//
// .COLLABORATORS:
//  MvIconDataBase (stores the relations between presentables,
//  data units and visdefs)
//
// .BASE CLASS:
//  Device
//
// .DERIVED CLASSES:
//  (none)
//
// .REFERENCES:
//   The implementation of this class is based on the "visitor"
//   pattern (See "Design Patterns" book, page 331).
//
//
#pragma once

#include <cstdio>

#include "MvRequest.h"
#include "ObjectInfo.h"
#include "Types.hpp"
#include "Visitor.h"

using MacroVisDefMap = std::map<int, std::string>;

class MacroVisitor : public Visitor
{
public:
    enum LanguageMode {MacroMode, PythonMode};
    // -- Constructor
    MacroVisitor(std::string&, const MvRequest&, LanguageMode);

    // -- Destructor
    ~MacroVisitor() override;

    // -- Creates a new fileName
    static std::string CreateFileName(const MvRequest&, const std::string& suffix);

    // --  Visitor methods
    void Visit(SuperPage&) override;
    void Visit(Page&) override;
    void Visit(SubPage&) override;

private:
    void WriteProlog();
    void WriteTrailer();

    // Get icons requests related to a specific Page
    bool getIconsByPageId(const int, MvRequest&);

    // No copy allowed
    MacroVisitor(const MacroVisitor&);
    MacroVisitor& operator=(const MacroVisitor&) { return *this; }

    std::string macroName_;

    FILE* filePtr_;  // pointer to the temporary file

    std::string tmpFileName_;  // name of the temporary file
    std::string outFileName_;  // name of the output file

    ObjectInfo deviceInfo_;
    ObjectInfo superpageInfo_;
    ObjectInfo superpageDrops_;
    ObjectInfo pageInfo_;
    ObjectInfo pageDrops_;

    MvRequest plotReq_;  // plotting requests

    MacroVisDefMap vdMap_;

    LanguageMode langMode_{MacroMode};
};

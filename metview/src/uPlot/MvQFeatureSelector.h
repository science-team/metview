/***************************** LICENSE START ***********************************

 Copyright 2021 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvRequest.h"

#include <QElapsedTimer>
#include <QGraphicsItem>
#include <QMimeData>
#include <QPixmap>
#include <QPen>
#include <QUndoCommand>

#include "MvQFeatureFwd.h"

class QMouseEvent;
class MvQFeatureItem;
class MvQFeatureHandler;
class MvQFeatureSelectorPoint;
class MvQFeatureCommandTarget;
class MvQPlotView;
class SelectorState;

class MvQFeatureSelector : public QGraphicsItem
{
    friend class SelectorIdleState;
    friend class SelectorNormalState;

public:
    enum CurrentAction
    {
        NoAction,
        NormalAction,
        TextEditAction,
        PointEditAction,
        LineEditAction
    };
    MvQFeatureSelector(MvQPlotView*, MvQFeatureHandler*, QGraphicsItem*);
    QRectF boundingRect() const override;
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;
    void controlPointPressed(MvQFeatureSelectorPoint*);
    void controlPointMoved(MvQFeatureSelectorPoint*, QRectF);
    void controlPointReleased(MvQFeatureSelectorPoint*);

    void suspend(bool);
    void clear();
    void selectionChanged();
    bool keepAspectRatio() const;
    const QList<MvQFeatureItemPtr>& managedItems() const;
    QList<MvQFeatureItemPtr> allItems() const;
    MvQFeatureItemPtr firstItem() const;
    void managedItemsChanged();
    void adjustToItems();

    bool identifyCommandTarget(MvQFeatureCommandTarget*);
    void itemStyleChanged(MvQFeatureItemPtr item);

    MvQPlotView* view() const { return view_; }
    QList<MvQFeatureItemPtr> currentSelection() const;
    void registerMousePress(QGraphicsSceneMouseEvent*);

    void transitionTo(SelectorState* state);

protected:
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent*) override;
    void mousePressEvent(QGraphicsSceneMouseEvent*) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent*) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent*) override;

private:
    void createControlPoints();
    void adjust(QRectF);
    QRectF computeBRect() const;
    QSizeF minSize() const;
    MvQFeatureItemList nonGeoLockedManagedItems() const;

    MvQPlotView* view_{nullptr};
    MvQFeatureHandler* handler_{nullptr};
    QList<MvQFeatureSelectorPoint*> controlPoints_;
    QRectF bRect_;
    QRectF oriRect_;
    QPointF dragPos_;
    QPen pen_;
    QBrush brush_;
    bool controlPointUsed_{false};
    bool controlPointMove_{false};

    enum MouseAction
    {
        NoMouseAction,
        MousePressedAction,
        MouseMoveAction
    };
    MouseAction currentMouseAction_{NoMouseAction};

    SelectorState* state_{nullptr};
};

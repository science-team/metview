/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "ZoomStacks.h"

ZoomStacks::ZoomStacks() :
    zoomIndex_(-1)
{
    // empty constructor
}

ZoomStacks::~ZoomStacks() = default;

void ZoomStacks::ClearAll()
{
    while (zoomGeodeticCoordStack_.empty() != true)
        zoomGeodeticCoordStack_.pop_back();

    zoomIndex_ = -1;
}

std::string
ZoomStacks::Get(int index)
{
    // Check input index
    if (zoomGeodeticCoordStack_.empty() || index >= (int)zoomGeodeticCoordStack_.size()) {
        zoomIndex_ = -1;
        return "";
    }
    else {
        zoomIndex_ = index;
        return zoomGeodeticCoordStack_[index];
    }
}

void ZoomStacks::GeodeticPush(const std::string& zinfo)
{
    // If this zoom is not related to the stack top position then
    // remove elements from the stack
    if (zoomIndex_ + 1 < Size()) {
        for (int i = Size(); i > zoomIndex_ + 1; i--)
            zoomGeodeticCoordStack_.pop_back();
    }

    // Add new zoom to the stack
    zoomGeodeticCoordStack_.push_back(zinfo);
    zoomIndex_++;
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  SimpleDecoder
//
// .AUTHOR:
//  Geir Austad
//
// .SUMMARY:
//  A class for decoding data units with no metadata.
//
// .CLIENTS:
//  PlotModMatcher, Graphics Engine
//
// .RESPONSABILITIES:
//  Return true first time ReadNextData is called. No matching is done.
//
// .COLLABORATORS:
//
// .BASE CLASS:
//  Decoder
//
// .DERIVED CLASSES:
//
//
// .REFERENCES:
//
//  The design of this class is based on the "Factory" pattern
//  ("Design Patterns", page. 107).
//
//
#pragma once

#include "MvDecoder.h"

class SimpleDecoder : public Decoder
{
public:
    // Contructors
    SimpleDecoder(const MvRequest& inRequest);

    // Destructor
    ~SimpleDecoder() override;

    // Overridden methods from Decoder class
    // Returns true first time it's called
    bool ReadNextData() override;

    MatchingInfo CreateMatchingInfo() override;

private:
    // No copy allowed
    SimpleDecoder(const SimpleDecoder&);
    SimpleDecoder& operator=(const SimpleDecoder&) { return *this; }
};

/***************************** LICENSE START ***********************************

 Copyright 2017 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QWidget>

class QProgressBar;
class QToolBar;
class QWidgetAction;

class MvQProgressBarPanel : public QWidget
{
    //   Q_OBJECT   //put it back if it needs to use signal/slot

public:
    MvQProgressBarPanel(QToolBar*, QWidget* parent = nullptr);

    ~MvQProgressBarPanel() override;

    void setRange(int, int);
    void setVisible(bool) override;
    void reset();
    void setValue(int);

private:
    QWidgetAction* acLabel_;
    QWidgetAction* acToolBar_;
    QProgressBar* progressBar_;
};

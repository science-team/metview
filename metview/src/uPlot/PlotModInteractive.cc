/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "PlotModInteractive.h"

#include "MvApplication.h"
#include "PlotModConst.h"
#include "uPlotBase.h"
#include "MvQFeatureCommand.h"

PlotModInteractive::PlotModInteractive()
{
    isInteractive_ = true;
    isWindow_ = true;
    plotDestination_ = MVSCREEN;
    printerName_ = "";
}

void PlotModInteractive::addWeatherSymbol(const MvRequest& r)
{
    MvQFeatureHandler::Instance()->addFromRequest(r);
}

std::string PlotModInteractive::currentWorkDir() const
{
    if (plotApplication_) {
        return plotApplication_->currentWorkDir();
    }
    return {};
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QSettings>
#include <QWidget>

class QLabel;
class QStackedLayout;
class QToolButton;
class QVBoxLayout;

class MvQPlaceMark;
class MvQPlotView;

class MgQSceneItem;
class MgQPlotScene;


#include "MgQLayerItem.h"

class MvQLayerDataWidget : public QWidget
{
    Q_OBJECT

public:
    MvQLayerDataWidget(MgQPlotScene*, MvQPlotView*, QWidget* parent = nullptr);
    ~MvQLayerDataWidget() override;

    void updateProbe();
    void reset(MgQSceneItem*, MgQLayerItem*);
    void setLayer(MgQLayerItem*);
    void writeSettings(QSettings&);
    void readSettings(QSettings&);

public slots:
    void slotFrameChanged();
    void slotProbe(bool);
    void slotFilterProbe(bool);
    void slotResetProbe(bool);
    void slotUpdateProbeLabel(QPointF);
    void slotPointSelected(QPointF);
    void slotSetIndex(int);

private:
    enum PanelType
    {
        EmptyPanel,
        OdbPanel
    };

    void updateData();
    void layerMetaData(MetaDataCollector&);
    void loadOdb(QString, const DataIndexCollector&);
    void setIndexForPanel(const std::vector<int>& indexVec, PanelType, QWidget*);
    PanelType panelType(QWidget*);
    QWidget* findIdlePanel(PanelType);
    QWidget* findActivePanel(PanelType);
    void storePanelProperties();
    void clear();
    void createDataProbe();
    void updateProbeLabel();
    void checkButtonStatus();
    void applyFilterProbe();
    bool isProbeFiltered() const;
    void removeTmpFilterFromPanel();
    void adjustDataProbe();

    MgQPlotScene* plotScene_;
    MvQPlotView* plotView_;
    MgQSceneItem* sceneItem_;
    MgQLayoutItem* layoutItem_;
    MgQLayerItem* layer_;
    QVBoxLayout* layout_;
    QStackedLayout* panelLayout_;
    QMap<MgQLayerItem*, QWidget*> activePanels_;
    QList<QPair<PanelType, QWidget*> > panels_;

    QToolButton* probeTb_;
    QToolButton* resetProbeTb_;
    QToolButton* filterProbeTb_;
    QLabel* probeLabel_;
    MvQPlaceMark* dataProbe_;

    QString coordXName_;
    QString coordYName_;

    QStringList visibleOdbColumns_;
};

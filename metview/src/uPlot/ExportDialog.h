/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <vector>

#include <QComboBox>
#include <QFileDialog>
#include <QGridLayout>
#include <QLineEdit>
#include <QToolButton>
#include <QRadioButton>

#include "MvRequest.h"

class ExportDialog : public QFileDialog
{
    Q_OBJECT

public:
    ExportDialog(int, int, const std::string&, bool, QWidget* parent = nullptr);
    ~ExportDialog() override;

    void edited(MvRequest&);
    const MvRequest& reqOut() const { return reqOut_; }

public slots:
    void accept() override;
    void slotEditFormat();
    void slotRangeFrameSelection(bool);

private:
    void writeSettings();
    const char* getFormatVerb(std::string);
    bool getFrameSelection(MvRequest&);
    void buildFrameSelection(int, int, QGridLayout*,
                             const std::vector<bool>&, const QString&, bool);
    void errorMessage(const char*, const char* = nullptr);

    int current_{0};                // Current frame
    int total_{0};                  // Total number of frames
    bool export_qt_{false};         // qt driver
    MvRequest reqFormats_;          // list of output formats requests
    QComboBox* comboBox_{nullptr};  // List of output formats
    QToolButton* bEdit_{nullptr};   // Edit button
    QLineEdit* leFs3_{nullptr};     // Frame selection: line edit
    QRadioButton* cbFs1_{nullptr};  // Frame selection: Current radio button
    QRadioButton* cbFs2_{nullptr};  // Frame selection: All radio button
    QRadioButton* cbFs3_{nullptr};  // Frame selection: Range radio button
    MvRequest reqOut_;
};

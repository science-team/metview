/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  MagicsGraphics Engine
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Describes the MagicsGraphics Engine class, a class for
//  handling the different drawing packages supported by PlotMod
//
// .DESCRIPTION:
//  This class is based on the "Strategy" pattern (see
//  "Design Patterns" book, page 97).
//
//  The idea is to define an abstract base class for all possible instances
//  of graphics engines.
//
//  Details of internal structure of the objects being created are
//  hidden from client applications.
//
// .CLIENTS:
//  Presentable::Draw (called by CreateAction::Execute and DropAction::Execute)
//
// .RESPONSABILITIES:
//  Clla
//
//
// .COLLABORATORS:
//  Device, MvRequest
//
//
// .BASE CLASS:
//  GraphicsEngine
//
// .DERIVED CLASSES:
//  (none)
//

#pragma once

#include <string>

#include <MvRequest.h>

#include "GraphicsEngine.h"

class MagicsGraphicsEngine : public GraphicsEngine
{
public:
    // Constructors
    MagicsGraphicsEngine(Presentable& owner);

    // Destructor
    ~MagicsGraphicsEngine() override;

    // Methods
    Canvas* MakeCanvas(Device&, const Rectangle&, const PaperSize&, int) const override;

    // Draws a dataUnit+visDef onto a device
    // REQUIRE: 1. dataUnit must provide a valid request, which contains
    //             information about data location
    //          2. dataInfoRequest indicates the location of the field/obs
    //             whithin the dataUnit (where applicable)
    //          3. List of Visdefs (1D and 2D)
    //          4. data unit n-dim flag: 0-not defined, 1-1D, 2-2D, 3-1D&2D
    void DrawDataVisDef(MvIcon&, MvRequest&, MvIconList&, int = 0) override;

    // Draws a dataUnit onto a device
    // REQUIRE: 1. dataUnit must provide a valid request, which contains
    //             information about data location
    //          2. dataUnitIndex indicates the location of the field/obs
    //             whithin the dataUnit (where applicable)
    //          3. Device must have been pre-configured
    void DrawDataUnit(MvIcon&, MvRequest&, MvIcon&) override;

    void DrawNewPage(MvRequest&) override;
    void Draw(const MvRequest&, bool = false) override;

    void DrawProlog() override;
    void DrawPageHeader(Canvas&) override;
    // virtual void DrawTrailer    ( Canvas& );  //CHECK IF THIS IS STILL NEEDED
    void DrawTrailer(MvRequest&) override;

    void StartPicture(const char*) override;
    void EndPicture() override;

    // Translate METVIEW calls into MAGICS calls
    virtual void TranslateVisDefRequest(MvIconList&, MvRequest&, MvRequest&);

private:
    MvRequest serviceRequest_;  // temporary request
    MvRequest fullRequest_;     // full request
    std::string drawType_;

    double ComputeAspectRatio(Canvas& canvas, MvRequest&);
    // std::string GetSuperpageIndex ( int index );
};

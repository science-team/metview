/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  CartesianView
//
// .AUTHOR:
//  Fernando Ii, Sylvie Thepaut
//
// .SUMMARY:
//  Describes the CartesianView class
//
// .CLIENTS:
//  Page
//
// .RESPONSIBILITY:
//
// .COLLABORATORS:
//
// .ASCENDENT:
//  PlotModView, PlotModTable
//
// .DESCENDENT:
//
//
#pragma once

#include "MvIconDataBase.h"
#include "PlotModView.h"

class CartesianView;

class CartesianView : public PlotModView
{
public:
    // Constructor
    CartesianView(Page&, const MvRequest&, const MvRequest&,
                  const std::string& haxisName = "HORIZONTAL_AXIS",
                  const std::string& vaxisName = "VERTICAL_AXIS",
                  const std::string& viewName = "cartesianview");

    PlotModView* Clone() const override { return new CartesianView(*this); }

    // Destructor
    ~CartesianView() override;

    // Methods overriden from PlotModView class
    std::string Name() override;

    // Replace the current axis ( axis are the background of this view )
    void ReplaceAxis(MvRequest&);
    void ProcessAxis(MvRequest&);

    // Decode the data Unit
    void DecodeDataUnit(MvIcon&) override;

    // Draw the background (axis )
    void DrawBackground() override;

    // Draw the background (axis )
    void DrawForeground() override {}

    void Draw(SubPage*) override;

    // Describe the contents of the view
    void DescribeYourself(ObjectInfo&) override;

    void Drop(PmContext&) override;

    void DescribeAxis(ObjectInfo&, MvRequest&, const Cached&);

    MvIconList InsertDataRequest(MvRequest&) override;

    bool BackgroundFromData() const override { return true; }

    // Remove all visdefs when Page::EraseDraw is called. Segments
    // names does not correspond to visdefs in hierarchy
    virtual bool EraseAll() { return true; }

    int RetrieveBackground(MvRequest&);

    // Update view
    bool UpdateViewWithReq(MvRequest&);

protected:
    // Update X/Y axes type in the view
    void UpdateAxisTypeView(MvRequest&);

    std::string hAxisName_, vAxisName_, viewName_;

    const char* CheckDataRequest(MvIcon&, MvIconList&);

private:
    // Get list of background parameters according to the projection
    void GetBackgroundParameters(std::vector<std::string>&);

    // No assignment
    CartesianView& operator=(const CartesianView&);
};

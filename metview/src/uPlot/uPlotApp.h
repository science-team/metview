/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <MvRequest.h>
#include <MvProtocol.h>
#include <MvQApplication.h>

class uPlotApp : public MvQApplication
{
public:
    // Constructor
    uPlotApp(int& ac, char** av, const char* name = nullptr);
    uPlotApp(const uPlotApp&) = delete;
    uPlotApp& operator=(const uPlotApp&) = delete;

    // Processing the request
    virtual void serve(MvProtocol&, MvRequest&);

    //    bool notify(QObject* receiver, QEvent*  event) override;
};

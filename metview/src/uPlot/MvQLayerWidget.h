/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QModelIndex>
#include <QWidget>

#include "MvQDragDrop.h"
#include "MvRequest.h"

class QSlider;
class QSpinBox;
class QToolButton;

class MgQLayerItem;
class MgQPlotScene;
class MgQSceneItem;

class MvQLayerModel;
class MvQLayerFilterModel;
class MvQLayerView;

class MvQLayerSuperPageModel;
class MvQLayerSuperPageView;

class MvQLayerViewLevelModel;
class MvQLayerViewLevelView;

class MvQLayerContentsItemEditor
{
public:
    MvQLayerContentsItemEditor(const Cached& name);

    ~MvQLayerContentsItemEditor() = default;

    void EditedDefinition(MvRequest&);
};

class MvQLayerWidget : public QWidget
{
    Q_OBJECT

public:
    MvQLayerWidget(MgQPlotScene*, QWidget* parent = nullptr);
    ~MvQLayerWidget() override = default;
    void layersAreAboutToChange();
    void reset(MgQSceneItem*, MgQLayerItem*);
    void reset(MgQSceneItem*);
    MgQLayerItem* currentLayer();
    QString currentLayerText();
    int layerAtGlobalPos(const QPoint&);

    MvQLayerModel* model()
    {
        return model_;
    }

public slots:
    void slotSelectLayer(MgQLayerItem* layer);
    void slotSelectLayer(const QModelIndex&);
    void slotTrSpinValueChanged(int);
    void slotTrSliderValueChanged(int);
    void slotTrSliderReleased();
    void slotMoveBottom();
    void slotMoveDown();
    void slotMoveUp();
    void slotMoveTop();
    void slotShowInfo();
    void slotCheckItemStatus();
    void slotContextMenu(const QPoint&);
    void slotFrameChanged();
    void slotIconDropped(const MvQDrop&, const QModelIndex&);
    void slotIconDroppedTopLevel(const MvQDrop&);
    void slotIconDroppedViewLevel(const MvQDrop&);
    void slotDoubleClickItem(const QModelIndex&);

signals:
    void layerVisibilityChanged(QString, bool);
    void layerTransparencyChanged(QString, int);
    void layerStackingOrderChanged(QList<QPair<QString, int> >);
    void sendDropRequest(MvRequest*);
    void layerInfoRequested(MgQLayerItem*);

protected:
    void changeTransparency(int);

    // Verify if dropped icons are valid
    bool checkDroppedIcon(const MvQDrop&, MgQLayerItem*, int&);

    MvQLayerView* view_;
    MvQLayerModel* model_;
    MvQLayerFilterModel* filterModel_;
    MvQLayerSuperPageModel* spModel_;
    MvQLayerSuperPageView* spView_;
    MvQLayerViewLevelModel* vlModel_;
    MvQLayerViewLevelView* vlView_;

    QSlider* trSlider_;
    QSpinBox* trSpin_;
    bool initTrWidgets_;

    QToolButton* topTb_;
    QToolButton* upTb_;
    QToolButton* downTb_;
    QToolButton* bottomTb_;
    QToolButton* infoTb_;

    QModelIndexList expandList_;
    QString lastSelectedLayerText_;
};

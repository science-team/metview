/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvLayer.h"
#include "MvRequest.h"

MvLayer::MvLayer() :
    id_(0),
    transparency_(0),
    visibility_(1),
    order_(-1),
    name_("")
{
}

MvLayer::MvLayer(const char* name) :
    id_(0),
    transparency_(0),
    visibility_(1),
    order_(-1)
{
    name_ = name ? name : " ";
}

MvLayer::MvLayer(const MvRequest& layerReq)
{
    id_ = (int)layerReq("_ID") ? (int)layerReq("_ID") : 0;
    transparency_ = (int)layerReq("TRANSPARENCY") ? (int)layerReq("TRANSPARENCY") : 0;
    visibility_ = (int)layerReq("VISIBILITY") ? (int)layerReq("VISIBILITY") : 0;
    order_ = (int)layerReq("STACKING_ORDER") ? (int)layerReq("STACKING_ORDER") : -1;
    name_ = (const char*)layerReq("_NAME") ? (const char*)layerReq("_NAME") : "";

    if ((const char*)layerReq("ICONS_ID")) {
        size_t count = layerReq.countValues("ICONS_ID");
        icons_.reserve(count);
        for (unsigned int i = 0; i < count; i++)
            icons_[i] = layerReq("ICONS_ID", i);
    }
}

MvRequest
MvLayer::Request()
{
    MvRequest req("LAYER");
    req("TRANSPARENCY") = transparency_;
    req("VISIBILITY") = visibility_;
    req("STACKING_ORDER") = order_;
    req("_NAME") = name_.c_str();
    req("_ID") = id_;

    return req;
}

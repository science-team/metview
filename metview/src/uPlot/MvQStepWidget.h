/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QWidget>

class QComboBox;

class MvKeyProfile;

class MvQKeyProfileSortFilterModel;
class MvQKeyProfileTree;
class MvQKeyManager;
class MvQStepModel;
class MvQTreeView;
class MgQPlotScene;
class MgQSceneItem;

class MvQStepWidget : public QWidget
{
    Q_OBJECT

public:
    MvQStepWidget(QWidget* parent = nullptr);
    ~MvQStepWidget() override;

    MvQKeyProfileTree* view() { return view_; }
    void init(MgQPlotScene*, QString);
    void init(QString);
    void reset(MgQSceneItem*);
    void updateStepForScenes(const QList<MgQSceneItem*>&);
    int setCurrentStep(int, const QList<MgQSceneItem*>&);
    int currentStep();
    int stepNum();
    void setViewToCurrentStep();
    void stepDataIsAboutToChange();
    QString currentKeyProfile();
    void setCurrentKeyProfile(QString);

public slots:
    void slotLoadKeyProfile(int);
    void adjustProfile(bool, int);
    void slotManageKeyProfiles();

signals:
    void keyProfileChanged(MvKeyProfile*);
    void stepCacheStateChanged();
    void selectionChanged(const QModelIndex&);

protected:
    void collectKeyProfileData(MvKeyProfile*);
    void loadKeyProfile(MvKeyProfile*);
    void saveKeyProfiles(bool);
    MvKeyProfile* loadedKeyProfile() const;
    void reloadKeyProfile();

    MvQKeyProfileTree* view_;
    MvQStepModel* model_;
    MvQKeyProfileSortFilterModel* sortModel_;
    QComboBox* keyCombo_;

    MgQPlotScene* scene_;
    MvQKeyManager* keyManager_;

    QAction* actionSave_;

    QList<MvKeyProfile*> allKeys_;

    MgQSceneItem* activeScene_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QComboBox>
#include <QSet>
#include <QToolButton>
#include <QWidget>

class QComboBox;
class QListWidget;
class QListWidgetItem;
class QTextEdit;

class MvKeyProfile;
class MvQKeyManager;

class MgQSceneItem;
class MgQPlotScene;

#include "MgQLayerItem.h"

class MvQLayerMetaDataWidget : public QWidget
{
    Q_OBJECT

public:
    MvQLayerMetaDataWidget(QWidget* parent = nullptr);
    ~MvQLayerMetaDataWidget() override;

    void reset(MgQSceneItem*, MgQLayerItem*);
    void setHistoEnabled(bool b) { histoTb_->setChecked(b); }
    bool isHistoEnabled() { return histoEnabled_; }
    void setStatsEnabled(bool b) { statsTb_->setChecked(b); }
    bool isStatsEnabled() { return statsEnabled_; }
    int histoVisdefCurrentIndex() { return histoVisdefCb_->currentIndex(); }
    void setHistoVisdefInitialIndex(int);
    void setLayer(MgQLayerItem*);

public slots:
    void slotStatsEnabled(bool);
    void slotHistoEnabled(bool);
    void slotFrameChanged();
    void slotSelectHistoVisdef(int);

protected:
    void updateData();
    void addTitleInfoRow(QString, QString&, int);
    void addTextInfoRow(QString, QString, const std::map<std::string, std::string>&, QString&, int);
    void addTextInfoRow(QString, QString, QString&, int);
    void addImageInfoRow(QString, QString, const std::map<std::string, std::string>&, QString&, int);
    void addListInfoRow(QString, QStringList, QString, const std::map<std::string, std::string>&, QString&);
    void addTableInfoRow(QString, QStringList, QStringList, QString, const std::map<std::string, std::string>&, QString&, int);
    int getColSpan(MvKeyProfile*, const std::map<std::string, std::string>&);
    void layerMetaData(MetaDataCollector&);
    QString layerMetaData(QString);
    QPixmap layerInfoImage(QString);

    MvQKeyManager* keyManager_;
    QTextEdit* layerTe_;
    QToolButton* statsTb_;
    QToolButton* histoTb_;
    QComboBox* histoVisdefCb_;
    QSet<QString> visdefs_;
    QHash<QString, QPixmap> visdefPixmaps_;

    MgQSceneItem* sceneItem_;
    MgQLayerItem* layer_;
    bool statsEnabled_;
    bool histoEnabled_;
    int histoVisdefInitialIndex_;
};

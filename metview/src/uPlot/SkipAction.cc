/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "SkipAction.h"

// This is the exemplar object for the Skip Action class
static SkipAction skipActionInstance("Skip");

PlotModAction&
SkipAction::Instance()
{
    return skipActionInstance;
}

// METHOD : Execute
//
// PURPOSE: // Skip the request and update the request without the skipped one
void SkipAction::Execute(PmContext& context)
{
    context.Advance();  // Skip this request

    // Save the remaining requests.
    // It will skip all the previous requests and only save the
    // remaining ones. Usually, the skipped request is the first
    // request, such as ODB_MANAGER and NETCDF_MANAGER. So, there
    // will be no problems to save only the remaining requests.
    // If this situation changes, this function.will need to be updated.
    MvRequest remainingReqs;
    MvRequest aux = context.InRequest();
    while (aux) {
        remainingReqs = remainingReqs + aux.justOneRequest();
        aux.advance();
    }
    context.Update(remainingReqs);
}

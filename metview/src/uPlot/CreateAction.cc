/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
//   Methods for the Create Action class
//
#include "CreateAction.h"

#include <Assertions.hpp>
#include "ObjectList.h"
#include "PlotMod.h"
#include "PlotModBuilder.h"
#include "Presentable.h"

//
// This is the exemplar object for the Create Action class
// which is loaded in the prototype map
//
static CreateAction createActionInstance("Create");

PlotModAction&
CreateAction::Instance()
{
    return createActionInstance;
}

void CreateAction::Execute(PmContext& context)
{
    // Check if request has a flag indicating that it comes from the Macro module
    MvRequest req = context.InRequest().justOneRequest();
    if (PlotMod::Instance().CalledFromMacro(req))
        PlotMod::Instance().CalledFromMacro(true);

    // Build the Tree branch associated with the Request
    // First, find which builder will process the request
    const char* requestName = req.getVerb();
    const char* builderName = ObjectList::Find("request", requestName, "builder");

    PlotModBuilder& builder = PlotModBuilder::Find(builderName);

    // Execute the builder (return the tree node which has been created)
    Presentable* superpage = builder.Execute(context);
    ensure(superpage != nullptr);

    // This was needed to send the driver info to Magics for each newpage.
    // Now, Magics only receive the driver info once (FAMI20150514)
    // Add output device to Superpage
    //   MvRequest deviceReq = PlotMod::Instance().OutputFormat()("OUTPUT_DEVICES");
    //   MvRequest spReq = superpage->Request();
    //   spReq("_OUTPUT_DEVICES") = deviceReq;
    //   superpage->SetRequest(spReq);
}

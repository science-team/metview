/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// .NAME:
//  DropAction
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//  Describes the DropAction class, a derived class
//  for the various types of requests in PlotMod which require
//  creation of a new branch in the Tree.
//
//
// .CLIENTS:
//  PlotModAction class, called by PlotMod main module (PlotMod.cc)
//
// .RESPONSABILITY:
//  This class will provide all support for the visualisation
//  actions whithin PlotMod
//
//   A visualisation action consists of:
//  - Building a Tree
//  - Matching DataUnits and VisDefs within a tree node
//  - Drawing the tree node
//
//  For each step, the request list is processed sequentially.
//
// .COLLABORATORS:
//  Builder, Matcher, Drawer
//
// .ASCENDENT:
//  PlotModAction
//
// .DESCENDENT:
//
//
#pragma once

#include "PlotModAction.h"

class DropAction : public PlotModAction
{
public:
    // Constructors
    DropAction(const Cached name) :
        PlotModAction(name) {}

    // Destructor
    ~DropAction() override = default;

    // Methods
    // Overriden from PlotModActio
    void Execute(PmContext& context) override;

private:
    // Local to class
    // Process the page
    bool ProcessPage(PmContext& context);

    // Massage context if old application was dropped.
    void PrepareForMacro(PmContext& context);
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QPixmap>

#include "MvRequest.h"

class MvQDrop;
class MgQIcon;

class MvQLayerContentsIcon
{
public:
    // Constructors
    MvQLayerContentsIcon(const QString&, const QString&, const QString&, bool);

    // Destructor
    ~MvQLayerContentsIcon();

    // Operators
    MvQLayerContentsIcon& operator=(const MvQLayerContentsIcon&);
    bool operator==(const MvQLayerContentsIcon&);

    QString name() { return name_; }
    QPixmap pixmap() { return pix_; }
    QString path() { return path_; }
    QString type() { return type_; }
    QString id() { return id_; }

    void id(const QString& id) { id_ = id; }

    void startEditor();
    void edited(MvRequest&);
    void deleteIcon();
    void saveIcon();
    void saveAsIcon();

    void visibility(bool vis) { visibility_ = vis; }
    bool visibility() { return visibility_; }

    bool canBeDeleted() { return canBeDeleted_; }
    bool canBeEdited() { return canBeEdited_; }
    bool canBeSaved() { return canBeSaved_; }
    bool canBeSavedAs() { return canBeSavedAs_; }
    bool isData() { return isData_; }

private:
    QString name_;
    QString path_;
    QString type_;
    QString id_;
    QPixmap pix_;

    bool visibility_;
    bool canBeDeleted_;
    bool canBeEdited_;
    bool canBeSaved_;
    bool canBeSavedAs_;
    bool isData_;

    int calledFromMacro_;  // icon originated from a Macro

    std::string getPixmapPath(const std::string&);

    // Members related to the icon hidden parameters.
    // They are used between functions startEditor() and edited()
    // because the "editor" does not send forward the hidden parameters
    std::string nameOrg_;  // original icon name
};

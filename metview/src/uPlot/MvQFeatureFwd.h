/***************************** LICENSE START ***********************************

 Copyright 2021 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

#include <QList>
#include <memory>

class MvQFeatureItem;
using MvQFeatureItemPtr = std::shared_ptr<MvQFeatureItem>;
using MvQFeatureItemList = QList<MvQFeatureItemPtr>;

class MvQFeatureTextItem;
using MvQFeatureTextItemPtr = std::shared_ptr<MvQFeatureTextItem>;

enum class StyleEditInfo
{
    StandardStyleEdit,
    TransparencyStyleEdit,
    SizeStyleEdit
};

template <typename T>
class MvQFeatureMaker;

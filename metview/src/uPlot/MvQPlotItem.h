/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QGraphicsObject>

class QMouseEvent;

class MgQPlotScene;
class MvQPlotView;

class MvQPlotItem : public QGraphicsObject
{
public:
    MvQPlotItem(MgQPlotScene* scene, MvQPlotView* view, QGraphicsItem* parent = nullptr) :
        QGraphicsObject(parent),
        plotScene_(scene),
        plotView_(view),
        activated_(false),
        acceptMouseEvents_(true) {}

    bool activated() { return activated_; }
    virtual void setActivated(bool b) { activated_ = b; }
    void setAcceptMouseEvents(bool b) { acceptMouseEvents_ = b; }

    virtual void mousePressEventFromView(QMouseEvent*) {}
    virtual void mouseMoveEventFromView(QMouseEvent*) {}
    virtual void mouseReleaseEventFromView(QMouseEvent*) {}

    virtual void prepareForReset() {}
    virtual void reset() {}

    MgQPlotScene* plotScene() const { return plotScene_; }
    MvQPlotView* plotView() const { return plotView_; }

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent*) override {}
    void mouseMoveEvent(QGraphicsSceneMouseEvent*) override {}
    void mouseReleaseEvent(QGraphicsSceneMouseEvent*) override {}

    MgQPlotScene* plotScene_;
    MvQPlotView* plotView_;
    bool activated_;
    bool acceptMouseEvents_;
};

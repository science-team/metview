/***************************** LICENSE START ***********************************

 Copyright 2017 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQProgressBarPanel.h"

//#include <QDebug>
#include <QLabel>
#include <QProgressBar>
#include <QToolBar>
#include <QWidgetAction>

MvQProgressBarPanel::MvQProgressBarPanel(QToolBar* toolBar, QWidget* parent) :
    QWidget(parent)
{
    // Create a progress bar label
    auto* label = new QLabel(this);
    label->setText("Progress Bar: ");
    acLabel_ = new QWidgetAction(this);
    acLabel_->setDefaultWidget(label);
    acLabel_->setVisible(false);
    toolBar->addAction(acLabel_);

    // Create progress bar
    progressBar_ = new QProgressBar(this);
    acToolBar_ = new QWidgetAction(this);
    acToolBar_->setDefaultWidget(progressBar_);
    acToolBar_->setVisible(false);
    toolBar->addAction(acToolBar_);
}

MvQProgressBarPanel::~MvQProgressBarPanel()
{
    // parent should destroy all Qt objects
}

void MvQProgressBarPanel::setRange(int min, int max)
{
    progressBar_->setRange(min, max);
}

void MvQProgressBarPanel::setVisible(bool flag)
{
    acLabel_->setVisible(flag);
    acToolBar_->setVisible(flag);
}

void MvQProgressBarPanel::reset()
{
    progressBar_->reset();
}

void MvQProgressBarPanel::setValue(int value)
{
    progressBar_->setValue(value);
}

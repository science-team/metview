/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "SimpleDecoder.h"
#include "MatchingInfo.h"

// =====================================================
// class SimpleDecoderFactory - builds  decoders
//

class SimpleDecoderFactory : public DecoderFactory
{
    // Virtual Constructor - Builds a new SimpleDecoder
    Decoder* Build(const MvRequest& inRequest) override
    {
        return new SimpleDecoder(inRequest);
    }

public:
    SimpleDecoderFactory() :
        DecoderFactory("SimpleDecoder") {}
};

static SimpleDecoderFactory Instance;

//=======================================================
//
// Methods for the SimpleDecoder class
//
SimpleDecoder::SimpleDecoder(const MvRequest& dataUnitRequest) :
    Decoder(dataUnitRequest)
{
    metadataRequest_ = dataUnitRequest;
    nextData_ = true;
}

SimpleDecoder::~SimpleDecoder() = default;

// Read Next Data
//
// -- Returns true first time it's called, then false.
bool SimpleDecoder::ReadNextData()
{
    if (nextData_) {
        nextData_ = false;
        return true;
    }
    else
        return false;
}

// Create MatchingInfo
//
// -- Nothing meaningful to match, as we have no metadata,
//    so just return an empty request.
//
MatchingInfo
SimpleDecoder::CreateMatchingInfo()
{
    MvRequest matchingRequest("MATCHING_INFO");
    matchingRequest("DATA_TYPE") = metadataRequest_.getVerb();
    return MatchingInfo(matchingRequest);
}

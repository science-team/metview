/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QDebug>
#include <QFile>
#include <QPainter>

#include "MvQProgressItem.h"

MvQProgressItem::MvQProgressItem(QGraphicsItem* parent) :
    QGraphicsItem(parent)
{
    font_ = new QFont();
    font_->setPointSize(26);
    fontMetrics_ = new QFontMetrics(*font_);

    // fontPen_=QPen(Qt::white);
    // fontBrush_=QBrush(Qt::white);

    fontPen_ = QPen();
    fontBrush_ = QBrush();

    bgPen_ = QPen(QColor(255, 255, 255, 190));
    bgBrush_ = QBrush(QColor(120, 120, 120, 190));
}

MvQProgressItem::~MvQProgressItem() = default;

void MvQProgressItem::setRect(QRectF r)
{
    if (rect_ != r) {
        rect_ = r;
        update();
    }
}

QRectF MvQProgressItem::boundingRect() const
{
    return rect_;
}

void MvQProgressItem::paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*)
{
    painter->setPen(bgPen_);
    painter->setBrush(bgBrush_);
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->drawRect(rect_);


    // Render text
    painter->translate(rect_.width() / 2, rect_.height() / 2);
    painter->scale(1., -1.);
    painter->drawText(0, 0, QObject::tr("Loading ..."));
}

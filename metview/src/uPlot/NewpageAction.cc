/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "NewpageAction.h"
#include "PlotMod.h"
#include "Root.h"

// This is the exemplar object for the Newpage Action class
static NewpageAction newpageActionInstance("Newpage");

PlotModAction&
NewpageAction::Instance()
{
    return newpageActionInstance;
}

// METHOD : Execute
//
// PURPOSE: // Skip the request and update the request without the skipped one
void NewpageAction::Execute(PmContext& context)
{
    context.Advance();  // Skip the newpage request

    // Indicate a new superpage
    if (!PlotMod::Instance().IsInteractive())
        Root::Instance().HasReceivedNewpage(true);

    // Save the remaining requests.
    // It will skip all the previous requests and only save the
    // remaining ones.
    MvRequest remainingReqs;
    remainingReqs.copyFromCurrent(context.InRequest());

    // Update context
    context.Update(remainingReqs);
}

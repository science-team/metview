/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QListView>
#include <QSettings>
#include <QSortFilterProxyModel>
#include <QWidget>

class QAbstractButton;
class QButtonGroup;
class QComboBox;
class QStackedWidget;
class QTreeView;
class QVBoxLayout;
class WsType;
class MgQPlotScene;
class FeatureStoreWidget;

class MvQFeatureListProxyModel : public QSortFilterProxyModel
{
public:
    MvQFeatureListProxyModel(QString group, QObject* parent);
    void setSubGroup(QString subGroup);

protected:
    bool filterAcceptsRow(int source_column, const QModelIndex& source_parent) const override;

    QString group_;
    QString subGroup_;
};

//----------------------------------------------------------------------

class MvQFeatureListModel : public QAbstractItemModel
{
public:
    MvQFeatureListModel(QString group, QObject* parent);

    enum
    {
        SubGroupRole = Qt::UserRole + 1
    };

    int columnCount(const QModelIndex&) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    WsType* indexToItem(const QModelIndex&) const;

protected:
    QString group_;
};

//----------------------------------------------------------------------

class MvQFeatureListView : public QListView
{
    Q_OBJECT
public:
    MvQFeatureListView(MvQFeatureListModel*, MvQFeatureListProxyModel*, bool, QWidget* parent = nullptr);

protected slots:
    void slotClicked(const QModelIndex& current);
    void delayedAdjustHeight();

protected:
    void resizeEvent(QResizeEvent* e) override;

    MvQFeatureListModel* model_;
    MvQFeatureListProxyModel* proxyModel_;
    QPoint startPos_;
    bool autoAdjustHeight_{false};
};

class MvQWmoFeatureWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MvQWmoFeatureWidget(QString groupId, QWidget* parent = nullptr);
    ~MvQWmoFeatureWidget() override = default;

    void writeSettings(QSettings& settings);
    void readSettings(QSettings& settings);

protected slots:
    void subGroupSelected(int);

protected:
    MvQFeatureListProxyModel* proxyModel_;
    MvQFeatureListView* view_;
    QComboBox* selectorW_;
};


class MvQFeatureWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MvQFeatureWidget(QWidget* parent = nullptr);
    ~MvQFeatureWidget() override = default;

    void writeSettings(QSettings& settings);
    void readSettings(QSettings& settings);

protected slots:
    void slotControlTbClicked(QAbstractButton*);

protected:
    void buildGroup(const std::string& grId, const std::string& grLabel);

    QVBoxLayout* typeLayout_{nullptr};
    QButtonGroup* controlTbGroup_{nullptr};
    FeatureStoreWidget* storeW_{nullptr};
    QStackedWidget* stackedW_{nullptr};
    MvQWmoFeatureWidget* wmoW_{nullptr};
};

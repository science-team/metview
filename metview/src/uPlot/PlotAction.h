/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// .NAME:
//  PlotAction
//
// .AUTHOR:
//  Fernando Ii  Jul-2010
//
// .SUMMARY:
//  Describes the PlottAction class, a derived class
//  for the various types of requests in uPlot which requires
//  plotting all the requests related to this plot.
//
//
// .CLIENTS:
//  PlotModAction class, called by uPlot main module
//
// .RESPONSABILITY:
//  This class will provide all support for the visualisation
//  actions whithin uPlot
//
// .COLLABORATORS:
//  Builder, Matcher, Drawer
//
// .ASCENDENT:
//  PlotModAction
//
// .DESCENDENT:
//
//
#pragma once

#include "PlotModAction.h"

class PlotAction : public PlotModAction
{
public:
    // Constructors
    PlotAction(const char* name) :
        PlotModAction(name) {}

    // Destructor
    ~PlotAction() override = default;

    // Methods
    // Overriden from PlotModAction
    void Execute(PmContext& context) override;
};

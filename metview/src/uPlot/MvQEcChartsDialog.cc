/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/
#include "MvQEcChartsDialog.h"

#include <QDialogButtonBox>
#include <QLabel>
#include <QSettings>
#include <QVBoxLayout>

#include "MvQMethods.h"

//===========================================
//
// MvQEcChartsDialog
//
//============================================

MvQEcChartsDialog::MvQEcChartsDialog(int /*currentFrame*/, int /*totalFrames*/, QWidget* parent) :
    QDialog(parent),
    layerName_("")
{
    // Wall combo
    auto* vb = new QVBoxLayout();
    setLayout(vb);

    // Layer name text box
    auto* hb1 = new QHBoxLayout();
    auto* screenSelectionLabel = new QLabel(tr("Layer Name:"), this);
    hb1->addWidget(screenSelectionLabel);
    leLayerNameSelection_ = new QLineEdit(this);
    hb1->addWidget(leLayerNameSelection_);
    vb->addLayout(hb1);

    // Output file text box
    auto* hb2 = new QHBoxLayout();
    auto* outputFileNameLabel = new QLabel(tr("Output File Name:"), this);
    hb2->addWidget(outputFileNameLabel);
    leOutputFileNameSelection_ = new QLineEdit(this);
    hb2->addWidget(leOutputFileNameSelection_);
    vb->addLayout(hb2);

    // Two-way connection between the screen selection widget and text box
    //   connect(leCellSelection_, SIGNAL(textEdited(const QString &)),
    //           cellSelector_, SLOT(updateCellSelection(const QString &)));

    //   connect(cellSelector_, SIGNAL(selectionChanged(const QString &)),
    //           leCellSelection_, SLOT(setText(const QString &)));

    // Buttonbox
    auto* buttonBox = new QDialogButtonBox(this);
    buttonBox->addButton(QDialogButtonBox::Ok);
    buttonBox->addButton(QDialogButtonBox::Cancel);

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    vb->addWidget(buttonBox);

    // Read the previous gui settings
    readSettings();

    // Initialise parameeters in the user interface
    leLayerNameSelection_->setText(QString(layerName_.c_str()));
    leOutputFileNameSelection_->setText(QString(outputFileName_.c_str()));
}

// Destructor
MvQEcChartsDialog::~MvQEcChartsDialog()
{
    writeSettings();
}

// Called when the user clicks Ok or <CR>
void MvQEcChartsDialog::accept()
{
    // Store the selections
    layerName_ = getLayerName();
    outputFileName_ = getOutputFileName();

    QDialog::accept();
}

void MvQEcChartsDialog::readSettings()
{
    // Restore previous gui settings
    QSettings settings(MVQ_QSETTINGS_DEFAULT_ARGS "uPlot_MvQEcChartsDialog");
    settings.beginGroup("main");
    if (settings.contains("size"))
        resize(settings.value("size").toSize());
    else
        resize(QSize(420, 400));

    settings.endGroup();

    layerName_ = settings.value("layerName").toString().toStdString();
    outputFileName_ = settings.value("outputFileName").toString().toStdString();
}

void MvQEcChartsDialog::writeSettings()
{
    // Save current gui settings
    QSettings settings(MVQ_QSETTINGS_DEFAULT_ARGS, "uPlot_MvQEcChartsDialog");
    settings.beginGroup("main");
    settings.setValue("size", size());
    settings.endGroup();

    settings.setValue("layerName", QString(layerName_.c_str()));
    settings.setValue("outputFileName", QString(outputFileName_.c_str()));
}

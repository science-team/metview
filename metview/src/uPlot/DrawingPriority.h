/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .SUMMARY:
//  Define DrawingPriority class
//
// .DESCRIPTION
//
// .SEE ALSO
//
//

#pragma once

#include <set>
#include <string>
#include <map>

class Cached;
class MvRequest;
class ObjectInfo;

class DrawingPriority
{
public:
    // Constructors
    DrawingPriority();

    // Destructor
    ~DrawingPriority();

    // Methods
    int Priority(MvRequest&);
    void SetPriority(MvRequest&);
    int GetPriority(std::string);

    std::string DescribeYourself(ObjectInfo&, std::string);

    Cached DrawSegmentName(int, MvRequest&, int);

private:
    // No copy allowed
    // DrawingPriority ( const DrawingPriority& );
    DrawingPriority& operator=(const DrawingPriority&)
    {
        return *this;
    }

    int IsValidKey(std::string);

    // Members
    std::set<std::string> mySet_;
    std::map<std::string, int> myMap_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// File Counted
// Gilberto Camara - ECMWF Sep 97
//
// .NAME:
//  Counted
//
// .AUTHOR:
//  Gilberto Camara and Fernando Ii
//
// .SUMMARY:
//
//
//
// .CLIENTS:
//
//
//
// .RESPONSABILITIES:
//
//
//
//
// .COLLABORATORS:
//
//
//
// .BASE CLASS:
//
//
// .DERIVED CLASSES:
//
//
// .REFERENCES:
//
//
#pragma once


class Counted
{
public:
    // Contructors
    Counted() = default;

    // Methods
    void Attach()
    {
        refCount_++;
    }

    void Detach()
    {
        if (--refCount_ == 0)
            delete this;
    }

protected:
    // Destructor
    virtual ~Counted() = default;

private:
    // No copy allowed
    Counted(const Counted&) = delete;
    Counted& operator=(const Counted&) { return *this; }

    // Members
    int refCount_{0};
};

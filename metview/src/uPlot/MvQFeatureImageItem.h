/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QGraphicsItem>
#include <QPixmap>

#include "MvQFeatureItem.h"

class MvQFeatureImageItem : public MvQFeatureItem
{
public:
    void init(const QJsonObject& d) override;
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;
    void resize(QRectF rect) override;
    void resizeTo(const MvQFeatureGeometry& geom) override;

protected:
    MvQFeatureImageItem(WsType*);
    void makePixmap() override;
    void adjustSizeInReq();
    void updateRect(double, double);
    void adjustBRect() override;
    double halo() const override;

    QPixmap pix_;
    QPen boxPen_{Qt::NoPen};
    QBrush boxBrush_{Qt::NoBrush};
    QColor shapeCol_;
    bool square_{true};
    float hwRatio_{1.};
};

class MvQFeatureStandardImageItem : public MvQFeatureImageItem
{
    friend class MvQFeatureMaker<MvQFeatureStandardImageItem>;

protected:
    using MvQFeatureImageItem::MvQFeatureImageItem;
    bool getRequestParameters() override;
};

class MvQFeatureCharImageItem : public MvQFeatureImageItem
{
    friend class MvQFeatureMaker<MvQFeatureCharImageItem>;

protected:
    using MvQFeatureImageItem::MvQFeatureImageItem;
    void makePixmap() override;
    bool getRequestParameters() override;

    QChar letter_;
};

class MvQFeatureWmoSymbolItem : public MvQFeatureImageItem
{
    friend class MvQFeatureMaker<MvQFeatureWmoSymbolItem>;

public:
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*) override;

protected:
    using MvQFeatureImageItem::MvQFeatureImageItem;
    bool getRequestParameters() override;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//
// .NAME:
//  PaperSize
//
// .AUTHOR:
//  Gilberto Camara, Baudouin Raoult and Fernando Ii
//
// .SUMMARY:
//  A class for encapsulation of the size of the drawing area
//  (expressed in paper coordinates, in cm. )
//
// .CLIENTS:
//  Presentable
//
//
// .RESPONSABILITIES:
//   - Keep track of width and height (in cm) of the drawing area
//
//
// .COLLABORATORS:
//   (none)
//
//
// .BASE CLASS:
//   (none)
//
// .DERIVED CLASSES:
//   (none)
//
// .REFERENCES:
//
//
#pragma once

class PaperSize
{
public:
    // Contructors
    PaperSize(double width = 1., double height = 1.) :
        width_(width),
        height_(height) {}

    // Destructor
    ~PaperSize() = default;

    double GetWidth() const
    {
        return width_;
    }

    double GetHeight() const
    {
        return height_;
    }

private:
    double width_;   // size in cm
    double height_;  // size in cm
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QString>

#include "MvQRequestPanelLine.h"

class RequestPanel;

class MvQNoneLine : public MvQRequestPanelLine
{
public:
    MvQNoneLine(RequestPanel& owner, const MvIconParameter& param);
    ~MvQNoneLine() override;

    void gray(bool) override {}
    void mark(bool) override {}

    QString currentValue() { return {}; }
    void addValue(QString) {}

    void refresh(const std::vector<std::string>&) override {}
};

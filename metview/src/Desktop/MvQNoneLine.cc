/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQNoneLine.h"

#include "LineFactory.h"
#include "RequestPanel.h"

MvQNoneLine::MvQNoneLine(RequestPanel& owner, const MvIconParameter& param) :
    MvQRequestPanelLine(owner, param, false)
{
}

MvQNoneLine::~MvQNoneLine() = default;

static LineMaker<MvQNoneLine> maker1("none");

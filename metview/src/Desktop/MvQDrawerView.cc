/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQDrawerView.h"

#include "IconObject.h"
#include "Folder.h"

#include "MvQContextMenu.h"
#include "MvQFolderModel.h"

#include <QScrollBar>

MvQDrawerView::MvQDrawerView(MvQFolderModel* folderModel, QWidget* parent) :
    MvQIconStripView(folderModel, parent)
{
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
}

MvQDrawerView::~MvQDrawerView() = default;

MvQContextItemSet* MvQDrawerView::cmSet()
{
    static MvQContextItemSet cmItems("DrawerView");
    return &cmItems;
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QObject>

#include <cstdio>
#include <string>


class QFileSystemWatcher;

class MvQFileWatcher : public QObject
{
    Q_OBJECT

public:
    MvQFileWatcher();
    ~MvQFileWatcher() override;

    void start(FILE*, const std::string&);
    void stop();

signals:
    void ready(const char*);
    void done(FILE*);

protected slots:
    void slotInput(const QString&);

private:
    // No copy allowed
    MvQFileWatcher(const MvQFileWatcher&);
    MvQFileWatcher& operator=(const MvQFileWatcher&);

    FILE* file_;
    QFileSystemWatcher* watcher_;
};

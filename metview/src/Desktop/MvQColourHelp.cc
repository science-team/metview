/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQColourHelp.h"

#include <QAction>
#include <QDebug>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPainter>
#include <QPushButton>
#include <QToolButton>
#include <QVBoxLayout>

#include <cmath>

#include "HelpFactory.h"
#include "RequestPanel.h"

#include "MvQColourWidget.h"
#include "MvQMethods.h"
#include "MvQPalette.h"

//==================================================
//
//  MvQColourHelp
//
//==================================================

MvQColourHelp::MvQColourHelp(RequestPanel& owner, const MvIconParameter& param) :
    MvQRequestPanelHelp(owner, param)
{
    selector_ = new MvQColourSelectionWidget(parentWidget_, true);

    connect(selector_, SIGNAL(colourSelected(QColor)),
            this, SLOT(slotSelected(QColor)));

    connect(selector_, SIGNAL(dragFinished()),
            this, SIGNAL(editConfirmed()));
}

QWidget* MvQColourHelp::widget()
{
    return selector_;
}

void MvQColourHelp::slotSelected(QColor col)
{
    std::vector<std::string> vs;

    if (MvQPalette::magics(oriName_.toStdString()) == col)
        vs.push_back(oriName_.toStdString());
    else
        vs.push_back(MvQPalette::toString(col));

    emit edited(vs);
}

void MvQColourHelp::refresh(const std::vector<std::string>& values)
{
    if (values.size() == 0)
        return;

    oriName_ = QString::fromStdString(values[0]);
    QColor col = MvQPalette::magics(values[0]);

    if ((selector_->currentColour() == col ||
         MvQPalette::toString(selector_->currentColour()) == values[0]))
        return;

    if (col.isValid()) {
        selector_->setColour(col);
    }
}

//==================================================
//
//  MvQColourListHelp
//
//==================================================

MvQColourListHelp::MvQColourListHelp(RequestPanel& owner, const MvIconParameter& param) :
    MvQRequestPanelHelp(owner, param)
{
    w_ = new QWidget(parentWidget_);

    auto* vb = new QVBoxLayout();
    w_->setLayout(vb);
    vb->setContentsMargins(1, 1, 1, 1);
    vb->setSpacing(1);

    // Buttons

    auto* hb = new QHBoxLayout(w_);
    vb->addLayout(hb);
    hb->setContentsMargins(1, 1, 1, 1);
    hb->setSpacing(1);

    resetLabel_ = new QLabel(tr("Revert to:"), w_);
    hb->addWidget(resetLabel_);
    hb->addSpacing(2);

    oriColorBox_ = new QLabel(w_);
    oriColorBox_->setFixedHeight(18);
    oriColorBox_->setFixedWidth(24);
    hb->addWidget(oriColorBox_);

    resetTb_ = new QToolButton(w_);
    resetTb_->setToolTip(tr("Revert to original colour"));
    resetTb_->setIcon(QPixmap(":/desktop/undo_grey.svg"));
    resetTb_->setAutoRaise(true);
    hb->addWidget(resetTb_);

    hb->addSpacing(16);

    resetLabel_->setEnabled(false);
    oriColorBox_->setEnabled(false);
    resetTb_->setEnabled(false);

    connect(resetTb_, SIGNAL(clicked(bool)),
            this, SLOT(slotReset(bool)));


    // Colour selector

    selector_ = new MvQColourSelectionWidget(w_, true);
    vb->addWidget(selector_);

    connect(selector_, SIGNAL(colourSelected(QColor)),
            this, SLOT(slotSelected(QColor)));
}

QWidget* MvQColourListHelp::widget()
{
    return w_;
}

void MvQColourListHelp::setExternalActions(QList<QAction*> lst)
{
    if (auto* hb = dynamic_cast<QHBoxLayout*>(w_->layout()->itemAt(0))) {
        foreach (QAction* ac, lst) {
            if (!ac->isSeparator() && ac->data().toString() != "ignore") {
                auto* tb = new QToolButton(w_);
                hb->addWidget(tb);
                tb->setDefaultAction(ac);
            }
            else {
                hb->addSpacing(6);
            }
        }
        hb->addStretch(1);
    }
}

void MvQColourListHelp::slotSelected(QColor col)
{
    std::vector<std::string> vs;
    vs.push_back(MvQPalette::toString(col));

    emit edited(vs);

    if (col == oriColour_) {
        resetLabel_->setEnabled(false);
        resetTb_->setEnabled(false);
        oriColorBox_->setEnabled(false);
    }
    else {
        resetLabel_->setEnabled(true);
        resetTb_->setEnabled(true);
        oriColorBox_->setEnabled(true);
    }
}

void MvQColourListHelp::refresh(const std::vector<std::string>& values)
{
    if (values.size() == 0)
        return;

    QColor col = MvQPalette::magics(values[0]);

    setOriColourBox(col);

    if (MvQPalette::toString(selector_->currentColour()) != values[0] && col.isValid()) {
        selector_->setColour(col);
    }
}

void MvQColourListHelp::setOriColourBox(QColor colour)
{
    oriColour_ = colour;

    QPixmap pixmap(24, 18);
    QPainter painter(&pixmap);
    pixmap.fill(Qt::gray);

    painter.fillRect(2, 2, 20, 14, colour);

    oriColorBox_->setPixmap(pixmap);
}

void MvQColourListHelp::slotReset(bool)
{
    if (oriColour_.isValid()) {
        selector_->setColour(oriColour_);
    }
}

//==================================================
//
//  MvQColourGradHelp
//
//==================================================

MvQColourGradHelp::MvQColourGradHelp(RequestPanel& owner, const MvIconParameter& param) :
    MvQRequestPanelHelp(owner, param)
{
    w_ = new QWidget(parentWidget_);

    auto* vb = new QVBoxLayout();
    w_->setLayout(vb);
    vb->setContentsMargins(1, 1, 1, 1);
    vb->setSpacing(1);

    // Buttons

    auto* hb = new QHBoxLayout(w_);
    vb->addLayout(hb);
    hb->setContentsMargins(1, 1, 1, 1);
    hb->setSpacing(1);

    resetLabel_ = new QLabel(tr("Revert to:"), w_);
    hb->addWidget(resetLabel_);
    hb->addSpacing(2);

    oriColorBox_ = new QLabel(w_);
    oriColorBox_->setFixedHeight(18);
    oriColorBox_->setFixedWidth(24);
    hb->addWidget(oriColorBox_);

    resetTb_ = new QToolButton(w_);
    resetTb_->setToolTip(tr("Revert to original colour"));
    resetTb_->setIcon(QPixmap(":/desktop/undo_grey.svg"));
    resetTb_->setAutoRaise(true);
    hb->addWidget(resetTb_);

    hb->addSpacing(16);

    resetLabel_->setEnabled(false);
    oriColorBox_->setEnabled(false);
    resetTb_->setEnabled(false);

    connect(resetTb_, SIGNAL(clicked(bool)),
            this, SLOT(slotReset(bool)));

    // Colour selector

    selector_ = new MvQColourSelectionWidget(w_, true);
    vb->addWidget(selector_);

    connect(selector_, SIGNAL(colourSelected(QColor)),
            this, SLOT(slotSelected(QColor)));
}

QWidget* MvQColourGradHelp::widget()
{
    return w_;
}

void MvQColourGradHelp::setExternalActions(QList<QAction*> lst)
{
    if (auto* hb = dynamic_cast<QHBoxLayout*>(w_->layout()->itemAt(0))) {
        foreach (QAction* ac, lst) {
            if (!ac->isSeparator() && ac->data().toString() != "ignore") {
                auto* tb = new QToolButton(w_);
                hb->addWidget(tb);
                tb->setDefaultAction(ac);
            }
            else {
                hb->addSpacing(6);
            }
        }
        hb->addStretch(1);
    }
}

void MvQColourGradHelp::slotSelected(QColor col)
{
    std::vector<std::string> vs;
    vs.push_back(MvQPalette::toString(col));

    emit edited(vs);

    if (col == oriColour_) {
        resetLabel_->setEnabled(false);
        resetTb_->setEnabled(false);
        oriColorBox_->setEnabled(false);
    }
    else {
        resetLabel_->setEnabled(true);
        resetTb_->setEnabled(true);
        oriColorBox_->setEnabled(true);
    }
}

void MvQColourGradHelp::refresh(const std::vector<std::string>& values)
{
    if (values.size() == 0)
        return;

    QColor col = MvQPalette::magics(values[0]);

    setOriColourBox(col);

    if (MvQPalette::toString(selector_->currentColour()) != values[0] && col.isValid()) {
        selector_->setColour(col);
    }
}

void MvQColourGradHelp::setOriColourBox(QColor colour)
{
    oriColour_ = colour;

    QPixmap pixmap(24, 18);
    QPainter painter(&pixmap);
    pixmap.fill(Qt::gray);

    painter.fillRect(2, 2, 20, 14, colour);

    oriColorBox_->setPixmap(pixmap);
}

void MvQColourGradHelp::slotReset(bool)
{
    if (oriColour_.isValid()) {
        selector_->setColour(oriColour_);
    }
}


static HelpMaker<MvQColourHelp> maker1("colour");
static HelpMaker<MvQColourHelp> maker2("help_colour");
static HelpMaker<MvQColourListHelp> maker3("help_colourlist");
static HelpMaker<MvQColourGradHelp> maker4("help_colourgrad");

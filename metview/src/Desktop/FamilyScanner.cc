/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "FamilyScanner.h"

#include "IconClass.h"

static std::map<std::string, FamilyScanner*> cache;


FamilyScanner& FamilyScanner::find(const IconClass& kind)
{
    auto j = cache.find(kind.name());
    if (j != cache.end())
        return *(*j).second;

    return *(new FamilyScanner(kind));
}


FamilyScanner::FamilyScanner(const IconClass& kind) :
    class_(kind)
{
    // std::cout << "FamilyScanner " << kind.name() << std::endl;

    kind.language().scan(*this);
    keyword_ = kind.language().getInfo();
    if (keyword_ == "")
        keyword_ = "_SECOND_GROUP";

    cache[kind.name()] = this;
}

FamilyScanner::~FamilyScanner() = default;

void FamilyScanner::next(const MvIconParameter& param)
{
    params_.emplace_back(param.name());
    param.scan(*this);
}

const std::vector<std::string>& FamilyScanner::params() const
{
    // std::cout << "FamilyScanner parans " << params_.size() << std::endl;
    return params_;
}

const std::vector<std::string>& FamilyScanner::values(const std::string& s)  // const
{
    return values_[s];
}

const std::vector<std::string>& FamilyScanner::beau(const std::string& s)  // const
{
    return beau_[s];
}

void FamilyScanner::next(const MvIconParameter& p, const char* first, const char* second)
{
    const char* type = (second) ? second : first;
    values_[p.name()].push_back(type);
    beau_[p.name()].push_back(p.beautifiedName(first));
    names_[type] = type;
}


const std::string& FamilyScanner::keyword() const
{
    return keyword_;
}

bool FamilyScanner::validate(Request& r) const
{
    Request s = r;
    s.advance();
    if (!s) {
        if (!r)
            r = Request(class_.name());

        Request f = class_.language().expand(r.justOneRequest(),
                                             EXPAND_2ND_NAME);

        const char* p = f(keyword_.c_str());
        r = f + Request(p);
        return true;
    }
#if 0
	std::cout << "FamilyScanner::validate -> " << std::endl;
	r.print();

	Request f = class_.language().expand(s.justOneRequest(), EXPAND_2ND_NAME);
	f.print();

	if(!f || !f.getVerb() || class_.name() != f.getVerb())
	{
		r = Request(class_.name().c_str());
		validate(r);
		return true;
	}

	const char* v = s.getVerb();
	const char* p = f(keyword_.c_str());

	if(!v || /*names_.find(v) == names_.end() ||*/ v != p)
	{
		std::cout << "FamilyScanner::validate -> " << std::endl;
		r.print();
		r = f + Request( p );
		r.print();
		std::cout << "<- FamilyScanner::validate " << std::endl;
		return true;
	}

#endif
    return false;
}

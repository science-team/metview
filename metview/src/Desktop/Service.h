/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <ostream>


class IconObject;
class Action;
class Task;

class Service
{
public:
    Service(const std::string&);
    virtual ~Service();

    std::string name();
    virtual Task* task(const Action&, IconObject*) = 0;
    static Service* find(const std::string&);

protected:
    std::string name_;
    virtual void print(std::ostream&) const;

private:
    // No copy allowed
    Service(const Service&);
    Service& operator=(const Service&);

    friend std::ostream& operator<<(std::ostream& s, const Service& p)
    {
        p.print(s);
        return s;
    }
};

inline void destroy(Service**) {}

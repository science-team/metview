/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QListView>
#include <QString>

#include "MvQRequestPanelLine.h"

class QHBoxLayout;

class IconObject;
class MvIconParameter;
class Request;
class RequestPanel;

class MvQIconHolderModel;
class MvQIconHolderView;

class MvQIconLine : public MvQRequestPanelLine
{
    Q_OBJECT

public:
    MvQIconLine(RequestPanel& owner, const MvIconParameter& param);

    void refresh(const std::vector<std::string>&);
    void reset();
    void update();
    void apply();
    void cleanup();
    void set(Request&);

public slots:
    void slotHolderEdited();

protected:
    QHBoxLayout* buildAcceptLayout(const std::vector<std::string>&);

    MvQIconHolderView* view_;
    MvQIconHolderModel* model_;
    QHBoxLayout* acceptLayout_;
};

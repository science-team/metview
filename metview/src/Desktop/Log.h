/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <memory>
#include <ostream>
#include <string>

#include "Path.h"

class IconObject;

class Log
{
public:
    Log(IconObject*);
    ~Log();

    operator std::ostream&();
    const Path& path();

    static std::ostream& info(IconObject*);
    static std::ostream& warning(IconObject*);
    static std::ostream& error(IconObject*);
    static std::ostream& syserr(std::ostream&);

    static std::ostream& info(const std::string&);
    static std::ostream& warning(const std::string&);
    static std::ostream& error(const std::string&);

private:
    // No copy allowed
    Log(const Log&);
    Log& operator=(const Log&);

    Path path_;
    std::unique_ptr<std::ostream> out_;
    std::unique_ptr<std::ostream> tee_;
    IconObject* object_;

    static std::ostream& global();
    static std::ostream& find(IconObject*);
};

inline void destroy(Log**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(Log);
//#endif

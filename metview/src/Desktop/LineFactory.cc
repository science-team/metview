/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "LineFactory.h"

#include "MvQRequestPanelLine.h"
#include "MvIconParameter.h"
#include "Request.h"

#include "MvQTextLine.h"

static std::map<std::string, LineFactory*>* makers = nullptr;

LineFactory::LineFactory(const std::string& name)
{
    if (makers == nullptr)
        makers = new std::map<std::string, LineFactory*>;

    // Put in reverse order...
    (*makers)[name] = this;
}

LineFactory::~LineFactory() = default;

MvQRequestPanelLine* LineFactory::create(RequestPanel& e, const MvIconParameter& p)
{
    const char* o = p.interface();
    if (o != nullptr) {
        auto j = makers->find(o);
        if (j != makers->end())
            return (*j).second->make(e, p);
    }

    // Default
    return new MvQTextLine(e, p);
    // return new MvQLineEditItem(e,p) ;
}

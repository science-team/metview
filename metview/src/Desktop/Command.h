/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

class IconObject;

class Command
{
public:
    Command(const std::string& name);
    virtual ~Command();
    Command(const Command&) = delete;
    Command& operator=(const Command&) = delete;

    const std::string& name() const { return name_; }
    virtual void execute(IconObject*) = 0;
    static void execute(const std::string&, IconObject*);
    static bool isValid(const std::string&);

private:
    std::string name_;
};

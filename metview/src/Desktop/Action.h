/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <ostream>

class Action
{
public:
    Action(const std::string& = "*", const std::string& = "*");
    ~Action();  // Change to virtual if base class

    bool operator<(const Action&) const;
    const std::string& name() const;
    const std::string& mode() const;

protected:
    void print(std::ostream&) const;  // Change to virtual if base class

private:
    // No copy allowed
    std::string name_;
    std::string mode_;

    friend std::ostream& operator<<(std::ostream& s, const Action& p)
    {
        p.print(s);
        return s;
    }
};

inline void destroy(Action**) {}

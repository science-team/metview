/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QList>
#include <QObject>

class QAction;

class MvQHelpBrowser;
class MvQProductBrowser;

class MvQTools : public QObject
{
    Q_OBJECT

public:
    static MvQTools* instance();
    void make(QList<QAction*>& lst, QObject* parent);

public slots:
    void slotHelp();

protected:
    MvQTools();

    static MvQTools* instance_;

    MvQHelpBrowser* helpBrowser_;
};

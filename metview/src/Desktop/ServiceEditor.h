/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Editor.h"
#include "ReplyObserver.h"

class ServiceEditor;

class ServiceEditorObserver : public ReplyObserver
{
    friend class ServiceEditor;

public:
    ServiceEditorObserver(ServiceEditor* editor, std::string& path);
    ~ServiceEditorObserver() override = default;

    void start(const std::string& pidFile);

    // From ReplyObserver
    void reply(const Request&, int) override;
    void progress(const Request&) override {}
    void message(const std::string&) override {}

private:
    // No copy allowed
    ServiceEditorObserver(const ServiceEditorObserver&);
    ServiceEditorObserver& operator=(const ServiceEditorObserver&);

    ServiceEditor* editor_{nullptr};
    std::string path_;
};


class ServiceEditor : public Editor
{
public:
    ServiceEditor(const IconClass&, const std::string&);
    ~ServiceEditor() override = default;
    void changed() override {}
    void raiseIt() override;
    void closeIt() override;
    void merge(IconObject*) {}
    void replace(IconObject*) {}
    void editDone() override;

private:
    // No copy allowed
    ServiceEditor(const ServiceEditor&);
    ServiceEditor& operator=(const ServiceEditor&);

    void edit() override;
    void showIt() override {}
    void getRemotePid();
    bool isRemoteRunning() const;

    ServiceEditorObserver* replyObserver_;
    bool replyCalled_{false};
    std::string pidFile_;
    int remotePid_{-1};
};

inline void destroy(ServiceEditor**) {}

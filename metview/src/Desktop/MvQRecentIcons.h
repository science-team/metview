/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QStringList>

class MvQRecentIcons;

class RecentIconsObserver
{
public:
    RecentIconsObserver() = default;
    virtual void notifyRecentChanged() = 0;
};

class MvQRecentIcons
{
public:
    MvQRecentIcons();
    static const QStringList items() { return items_; }
    static void add(QString);
    static void init();
    static void save();
    static void addObserver(RecentIconsObserver* o) { observers_.push_back(o); }
    static void removeObserver(RecentIconsObserver*);

protected:
    static int maxNum_;
    static QStringList items_;
    static QList<RecentIconsObserver*> observers_;
};

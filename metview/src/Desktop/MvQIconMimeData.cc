/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QDebug>
#include <QDataStream>
#include <QIODevice>

#include "MvQIconMimeData.h"
#include "MvQFolderModel.h"
#include "IconClass.h"
#include "IconObject.h"

//=================================
//
//  MvQNewIconMimeData
//
//=================================

MvQNewIconMimeData::MvQNewIconMimeData(QString className, IconDefType iconDefType) :
    className_(className),
    iconDefType_(iconDefType)
{
    formats_ << "metview/new_icon";
    formats_ << "text/plain"
             << "text/html";
}

QStringList MvQNewIconMimeData::formats() const
{
    return formats_;
}

#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
QVariant MvQNewIconMimeData::retrieveData(const QString& /*format*/, QMetaType /*type*/) const
#else
QVariant MvQNewIconMimeData::retrieveData(const QString& /*format*/, QVariant::Type /*type*/) const
#endif
{
    return {};
}

//=================================
//
//  MvQIconMimeData
//
//=================================

MvQIconMimeData::MvQIconMimeData(MvQFolderModel* model, IconObject* dragObj,
                                 QPointF hotSpot, QList<IconObject*> objects,
                                 QList<QPoint> pixDistances, ClipboardAction clipboardAction) :
    model_(model),
    dragObject_(dragObj),
    hotSpotInVisRect_(hotSpot),
    objects_(objects),
    pixDistances_(pixDistances),
    fromHelper_(false),
    fromEditor_(false),
    clipboardAction_(clipboardAction)
{
    formats_ << "metview/icon";
    formats_ << "text/plain"
             << "text/html";

    if (objects_.count() > 0) {
        objPath_ = QString::fromStdString(objects_.at(0)->path().str());
    }

    if (dragObject_) {
        dragObject_->addObserver(this);
    }

    for (int i = 0; i < objects_.count(); i++)
        objects_[i]->addObserver(this);
}

MvQIconMimeData::~MvQIconMimeData()
{
    if (dragObject_) {
        dragObject_->removeObserver(this);
    }

    for (int i = 0; i < objects_.count(); i++)
        objects_[i]->removeObserver(this);
}

QStringList MvQIconMimeData::formats() const
{
    return formats_;
}

#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
QVariant MvQIconMimeData::retrieveData(const QString& format, QMetaType type) const
#else
QVariant MvQIconMimeData::retrieveData(const QString& format, QVariant::Type type) const
#endif
{
    if (format == "metview/icon") {
        QByteArray encodedData;
        QDataStream stream(&encodedData, QIODevice::WriteOnly);
        stream << QString::number(objects_.count());

        foreach (IconObject* obj, objects_) {
            stream << QString::fromStdString(obj->fullName());
            // stream << QString::fromStdString(obj->className());
            stream << QString::fromStdString(obj->editorClass().name());
        }

        return encodedData;
    }
    else if (format == "text/plain") {
        QString s = "file://" + objPath_;
        return s;
    }
    else if (format == "text/html") {
        // return "file://" + path_;
    }
    else if (format == "image/png") {
        QString s = "file://" + objPath_;
        return s;
    }

    return QMimeData::retrieveData(format, type);
}

void MvQIconMimeData::iconDestroyed(IconObject* obj)
{
    if (objects_.contains(obj)) {
        obj->removeObserver(this);
        objects_.removeAll(obj);
    }

    if (dragObject_ && obj == dragObject_) {
        obj->removeObserver(this);
        dragObject_ = nullptr;
    }

    if (!dragObject_ && objects_.isEmpty()) {
        objPath_.clear();
        formats_.clear();
    }
}

bool MvQIconMimeData::isEmpty() const
{
    return (!dragObject_ && objects_.isEmpty());
}

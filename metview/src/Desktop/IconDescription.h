/***************************** LICENSE START ***********************************

 Copyright 2020 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QString>
#include <QPixmap>

#include <string>
#include <deque>

#include "MvRequest.h"
#include "ReplyObserver.h"

class IconDescriptionPresenter;

class IconObject;
class IconDescription;

class IconDescriptionMakerObserver : public ReplyObserver
{
public:
    IconDescriptionMakerObserver(IconObject*, IconDescriptionPresenter*);
    ~IconDescriptionMakerObserver() override = default;

    virtual void start();
    void reply(const Request&, int) override;
    void progress(const Request&) override {}
    void message(const std::string&) override {}

private:
    // No copy allowed
    IconDescriptionMakerObserver(const IconDescriptionMakerObserver&);
    IconDescriptionMakerObserver& operator=(const IconDescriptionMakerObserver&);

    std::string path_;
    std::string cls_;
    IconDescriptionPresenter* caller_{nullptr};
};

class IconDescriptionCacheItem
{
public:
    friend class IconDescriptionCache;
    QPixmap pix() const { return pix_; }
    QString text() const { return text_; }

protected:
    std::string path_;
    QPixmap pix_;
    QString text_;
    time_t creationTime_;
};


class IconDescriptionCache
{
public:
    void add(const std::string& path, QPixmap, QString);
    IconDescriptionCacheItem* find(IconObject*);
    void print();

protected:
    void check();

    const std::size_t maxNum_{25};
    std::deque<IconDescriptionCacheItem*> data_;
};


class IconDescription
{
public:
    static IconDescription* Instance();
    QString info(IconObject*);
    QString table(IconObject*, bool addFullPath = false);
    void getContents(IconObject* item, IconDescriptionPresenter* caller);
    void notifyReply(const Request&, int, const std::string&, const std::string&, IconDescriptionPresenter*);

protected:
    IconDescription();
    QString formatFileDate(time_t t) const;

    static IconDescription* instance_;

    QStringList localImageClasses_;
    QStringList serviceClasses_;

    IconDescriptionMakerObserver* replyObserver_{nullptr};
    IconDescriptionCache contentsCache_;
};

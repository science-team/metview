/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include "mars.h"

class MvQRequestPanelHelp;
class RequestPanel;
class MvIconParameter;

class HelpFactory
{
public:
    HelpFactory(const std::string& name);
    virtual ~HelpFactory();

    virtual MvQRequestPanelHelp* make(RequestPanel& owner, const MvIconParameter& def) = 0;
    static MvQRequestPanelHelp* create(RequestPanel& owner, const MvIconParameter& info);

private:
    // No copy allowed
    HelpFactory(const HelpFactory&);
    HelpFactory& operator=(const HelpFactory&);
};


template <class T>
class HelpMaker : public HelpFactory
{
    MvQRequestPanelHelp* make(RequestPanel& owner, const MvIconParameter& def) override { return new T(owner, def); }

public:
    HelpMaker(const std::string& name) :
        HelpFactory(name) {}
};

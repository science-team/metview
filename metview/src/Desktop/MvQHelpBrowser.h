/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQMainWindow.h"


#include <QWebView>

class QAction;
class QCloseEvent;
class QHelpEngine;
class QSplitter;
class QWebView;
class MvQWebViewSearchLine;
class MvQSearchLinePanel;
class QLabel;
class QLineEdit;


class MvQHelpBrowser : public MvQMainWindow
{
    Q_OBJECT

public:
    MvQHelpBrowser(QWidget* parent = 0);
    ~MvQHelpBrowser();

public slots:
    void setWebSource(const QUrl&);
    void docHome();

protected:
    void closeEvent(QCloseEvent*);
    void setupNavigateActions();
    void setupHelpActions();
    void setupSearchActions();
    void writeSettings();
    void readSettings();

    QSplitter* splitter_;
    QSplitter* r_splitter_;
    QHelpEngine* engine_;
    QWebView* webView_;
    MvQWebViewSearchLine* search_;
    MvQSearchLinePanel* searchPanel_;

    MvQMainWindow::MenuItemMap menuItems_;

    QAction* actionBack_;
    QAction* actionForward_;
    QAction* actionHome_;

    QLabel* lookFor_label_;
    QLineEdit* lookFor_edit_;
    QSplitter* i_splitter_;
};

//////////////////////////////////////////////////////////////////////////

#include <QBuffer>
#include <QHelpEngine>
#include <QHelpIndexWidget>
#include <QHelpSearchEngine>
#include <QHelpSearchQueryWidget>
#include <QHelpSearchResultWidget>
#include <QNetworkAccessManager>
#include <QNetworkProxy>
#include <QNetworkReply>
#include <QPointer>

class QHelpEngineCore;
class QUrl;

class MvHelpNetworkReply : public QNetworkReply
{
    Q_OBJECT
public:
    MvHelpNetworkReply(const QUrl& url, QHelpEngineCore* helpEngine);

    virtual void abort()
    {
    }
    virtual qint64 bytesAvailable() const
    {
        return d_buffer.bytesAvailable();
    }
    virtual bool isSequential() const
    {
        return d_buffer.isSequential();
    }

private slots:
    void process();

protected:
    virtual qint64 readData(char* data, qint64 maxSize)
    {
        return d_buffer.read(data, maxSize);
    }
    QPointer<QHelpEngineCore> d_help_engine;
    QBuffer d_buffer;

private:
    Q_DISABLE_COPY(MvHelpNetworkReply)
};

class MvHelpNetworkAccessManager : public QNetworkAccessManager
{
public:
    MvHelpNetworkAccessManager(QHelpEngineCore* helpEngine,
                               QNetworkAccessManager* manager, QObject* parentObject) :
        QNetworkAccessManager(parentObject),
        d_help_engine(helpEngine)
    {
        Q_ASSERT(manager);
        Q_ASSERT(helpEngine);

        setCache(manager->cache());
        setProxy(manager->proxy());
        setProxyFactory(manager->proxyFactory());
        setCookieJar(manager->cookieJar());
    }

protected:
    virtual QNetworkReply* createRequest(Operation operation,
                                         const QNetworkRequest& request, QIODevice* device)
    {
        if (request.url().scheme() == "qthelp" && operation == GetOperation) {
            QNetworkReply* obj = new MvHelpNetworkReply(request.url(),
                                                        d_help_engine);
            return obj;
        }
        else
            return QNetworkAccessManager::createRequest(operation, request,
                                                        device);
    }

    QPointer<QHelpEngineCore> d_help_engine;

private:
    Q_DISABLE_COPY(MvHelpNetworkAccessManager)
};

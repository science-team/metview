/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include "mars.h"

class Editor;
class IconClass;

class EditorFactory
{
public:
    EditorFactory(const std::string& name);
    virtual ~EditorFactory();

    virtual Editor* make(const IconClass&) = 0;

    static Editor* find(const IconClass& name);
    static Editor* find(const IconClass& name, const std::string& type);

protected:
    std::string name_;

private:
    EditorFactory(const EditorFactory&);
    EditorFactory& operator=(const EditorFactory&);
};

template <class T>
class EditorMaker : public EditorFactory
{
    Editor* make(const IconClass& n) override { return new T(n, name_); }

public:
    EditorMaker(const std::string& name) :
        EditorFactory(name) {}
};

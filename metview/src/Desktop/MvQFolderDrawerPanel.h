/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQDrawerPanel.h"
#include "FolderObserver.h"

#include <string>

class MvQContextItemSet;
class MvQFolderModel;

class Folder;
class IconObject;

class MvQFolderDrawerPanel : public MvQDrawerPanel, public FolderObserver
{
    Q_OBJECT

public:
    MvQFolderDrawerPanel(QWidget* parent = nullptr);
    ~MvQFolderDrawerPanel() override;

    void setIconPositions();

public slots:
    void slotCreateDrawer(bool);
    void slotCommandFromView(QString, QPoint);

signals:
    void itemEntered(IconObject*);
    void commandRequested(Folder*, QString, QPoint);

protected:
    void command(QString, int) override;
    void createDrawer(Folder*);
    void renameDrawer(int);
    void deleteDrawer(int);

    void arrived(IconObject*) override;
    void renamed(IconObject*, const std::string&) override;
    void gone(IconObject*) override;
    void position(IconObject*, int, int) override {}

    std::string getUniqueName(const std::string&);
    int folderToIndex(Folder*);
    Folder* indexToFolder(int);
    MvQFolderModel* indexToModel(int);
    MvQContextItemSet* cmSet() override;

    Folder* parentFolder_;
};

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <map>
#include <QString>

#include "Desktop.h"
#include "FolderPresenter.h"
#include "IconObject.h"

class Folder;
class MvQFolderModel;

class IconClass;
class IconObject;

class QStackedLayout;
class QWidget;

class MvQFolderViewBase;
class MvQFolderItemProperty;


class MvQFolderViewHandler
{
public:
    MvQFolderViewHandler(QStackedLayout*, MvQFolderItemProperty*);

    void add(Desktop::FolderViewMode, MvQFolderViewBase*);
    Desktop::FolderViewMode currentMode() const { return currentMode_; }
    void setCurrentMode(int);
    void setCurrentMode(Desktop::FolderViewMode);
    MvQFolderViewBase* currentBase() const { return base(currentMode_); }
    QList<QWidget*> uniqueWidgets();
    bool changeFolder(QString path);
    bool changeFolder(Folder*);

private:
    MvQFolderViewBase* base(Desktop::FolderViewMode) const;
    QWidget* widget(Desktop::FolderViewMode);
    bool setCurrentMode(Desktop::FolderViewMode mode, Folder* folder, int iconSize = -1);

    Desktop::FolderViewMode currentMode_;
    std::map<Desktop::FolderViewMode, MvQFolderViewBase*> bases_;
    std::map<Desktop::FolderViewMode, QWidget*> widgets_;
    std::map<Desktop::FolderViewMode, int> indexes_;
    QStackedLayout* stacked_;
    MvQFolderItemProperty* itemProp_;
};

/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QBrush>
#include <QPen>
#include <QStyledItemDelegate>


class QStyleOptionViewItem;
class QTimer;

class MvQFolderViewDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    MvQFolderViewDelegate(QWidget* parent = nullptr);
    void blink(const QModelIndex&);

    QWidget* createEditor(QWidget*, const QStyleOptionViewItem&, const QModelIndex&) const override;
    void setEditorData(QWidget*, const QModelIndex&) const override;
    void setModelData(QWidget*, QAbstractItemModel*, const QModelIndex&) const override;
    void setEnablePaint(bool b) { enablePaint_ = b; }

public slots:
    void slotBlink();
    void slotInitEditor(int oldPos, int newPos);
    void changeSize(const QModelIndex&);

signals:
    void repaintIt(const QModelIndex&);

protected:
    void setColours();

    QTimer* timer_;
    int blinkCnt_;
    QModelIndex blinkIndex_;
    int blinkNum_;
    bool enablePaint_;
    QPen editPen_;
    QBrush editBrush_;
    QPen hoverPen_;
    QBrush hoverBrush_;
    QPen selectPen_;
    QBrush selectBrush_;
};

class MvQIconDelegate : public MvQFolderViewDelegate
{
public:
    MvQIconDelegate(QWidget* parent = nullptr);
    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const override;
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const override;
};

class MvQDetailedViewDelegate : public MvQFolderViewDelegate
{
public:
    MvQDetailedViewDelegate(QWidget* parent = nullptr);
    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const override;
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const override;
};

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Command.h"

class MethodCommand : public Command
{
public:
    using Proc = void (IconObject::*)();

    MethodCommand(const std::string&, Proc);
    ~MethodCommand() override;

    void execute(IconObject*) override;
    static void execute(const std::string&, IconObject*);

protected:
    std::string name_;

private:
    // No copy allowed
    MethodCommand(const MethodCommand&);
    MethodCommand& operator=(const MethodCommand&);

    Proc proc_;
};

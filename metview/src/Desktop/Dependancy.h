/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Counted.h"

class IconObject;
class Task;
class Action;
class Request;

class Dependancy : public virtual Counted
{
public:
    Dependancy();
    ~Dependancy() override;

    virtual Task* action(const Action&) = 0;
    virtual void success(const Request&) = 0;
    virtual void failure(const Request&) = 0;
};

class DependancyH : public Handle<Dependancy>
{
public:
    DependancyH(Dependancy* o = nullptr) :
        Handle<Dependancy>(o) {}
};

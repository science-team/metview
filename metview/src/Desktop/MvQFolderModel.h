/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QAbstractListModel>
#include <QHash>
#include <QFont>
#include <QFontMetrics>
#include <QSortFilterProxyModel>

#include "IconObject.h"
#include "FolderObserver.h"

class QMimeData;

class Folder;
class IconClass;
class IconObject;
class MvQFolderItemProperty;
class FolderSearchData;


class PositionHint
{
public:
    PositionHint() :
        className(""),
        x(0),
        y(0) {}
    void clear()
    {
        className = "";
        x = 0;
        y = 0;
    }
    std::string className;
    int x;
    int y;
};

class MvQFolderModel : public QAbstractItemModel, public FolderObserver, public IconObserver
{
    Q_OBJECT

public:
    MvQFolderModel(QObject* parent = nullptr);
    MvQFolderModel(int, QObject* parent = nullptr);
    ~MvQFolderModel() override;

    enum CustomItemRole
    {
        ClassFilterRole = Qt::UserRole + 1,
        EditorRole = Qt::UserRole + 2,
        PositionRole = Qt::UserRole + 3,
        FontMetricsRole = Qt::UserRole + 4
    };

    int columnCount(const QModelIndex&) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex&, const QVariant&, int) override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;
    Qt::ItemFlags flags(const QModelIndex&) const override;

    void folderIsAboutToChange();
    Folder* folder() { return folder_; }
    bool setFolder(Folder*, int iconSize = -1);
    bool setFolder(QString, int iconSize = -1);
    bool setFolder(const QModelIndex&, int iconSize = -1);
    bool setFolderToParent(int iconSize = -1);

    IconObject* objectFromIndex(const QModelIndex&) const;
    QModelIndex indexFromObject(IconObject*);
    QString fullName();
    QString fullName(const QModelIndex&);
    bool isFolder(const QModelIndex&);
    QModelIndex lastArrived();

    void createNewFolder();
    void saveFolderInfo();

    // Observer methods

    void arrived(IconObject*) override;
    void gone(IconObject*) override;
    void position(IconObject*, int, int) override;
    void renamed(IconObject*, const std::string&) override;

    void waiting(IconObject*) override;
    void error(IconObject*) override;
    void modified(IconObject*) override;
    void ready(IconObject*) override;
    void opened(IconObject*) override;
    void closed(IconObject*) override;
    void highlight(IconObject*) override;
    void iconChanged(IconObject*) override;

    void setIconSize(int);
    int iconSize() const;
    MvQFolderItemProperty* itemProp() const { return itemProp_; }

    void setAcceptedClasses(const std::vector<std::string>&);
    bool isAccepted(const IconClass&) const;
    void setPositionHint(const std::string&, int, int);

    void setSearchData(FolderSearchData*);
    void removeSearchData();

signals:
    void iconSizeChanged();
    void folderChanged(Folder*);
    void objectArrived(const QModelIndex&);
    void objectRenamed(const QModelIndex&, QString);

protected:
    void loadItems();
    bool isDataSet() const;
    QString formatFileSize(qint64) const;
    QString formatFileDate(time_t) const;
    bool isAccepted(IconObject*) const;
    void objectChanged(IconObject*);
    void updateSearchData(bool doReset);
    bool hasSearchData() const;

    Folder* folder_{nullptr};
    QList<IconObject*> items_;
    std::vector<std::string> classes_;
    IconObject* lastArrived_{nullptr};
    MvQFolderItemProperty* itemProp_;
    PositionHint posHint_;

    static QHash<IconObject::IconStatus, QColor> statusColour_;
    static QColor fadedColour_;

    mutable FolderSearchData* searchData_{nullptr};

private:
    void init();
};


class MvQFolderFilterModel : public QSortFilterProxyModel
{
public:
    MvQFolderFilterModel(QObject* parent = nullptr);
    void setSourceModel(QAbstractItemModel*) override;
    bool filterAcceptsRow(int, const QModelIndex&) const override;
    bool lessThan(const QModelIndex& left,
                  const QModelIndex& right) const override;

    // QVariant data(const QModelIndex& index, int role ) const;
    // bool setFilter(QString,QString);

protected:
    MvQFolderModel* folderModel_{nullptr};
    std::string name_;
    std::string type_;
    QList<IconObject*> matchLst_;
};

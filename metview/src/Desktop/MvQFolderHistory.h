/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QList>
#include <QObject>
#include <QPoint>
#include <QString>
#include <QStringList>

class QAction;
class QMenu;

class MvQFolderHistory : public QObject
{
    Q_OBJECT

public:
    MvQFolderHistory(QMenu*);
    ~MvQFolderHistory() override;

    void add(QString);
    static QStringList items() { return items_; }
    static void init(QStringList);

protected slots:
    void slotSelected(QAction*);
    void slotContextMenu(const QPoint&);

signals:
    void itemSelected(QString);
    void commandRequested(QString, QString);

protected:
    void init();
    void updateMenu();
    QAction* createAction(QString);
    QString path(QAction*);

    QList<QAction*> actions_;
    const int maxNum_;
    QMenu* menu_;
    QString prefix_;

    static QList<MvQFolderHistory*> fhLst_;
    static QStringList items_;
};

/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQPalette.h"
#include "MvQRequestPanelHelp.h"
#include "MvIconParameter.h"

#include <QPen>
#include <QWidget>
#include <QAbstractItemModel>
#include <QStyledItemDelegate>

class QComboBox;
class QGridLayout;
class QLabel;
class QLineEdit;
class QPushButton;
class QSlider;
class QSpinBox;
class QTabWidget;
class QToolButton;
class QTreeView;
class QSortFilterProxyModel;

class MvQPaletteDbItem;
class RequestPanel;

class MvQPaletteDelegate : public QStyledItemDelegate
{
public:
    MvQPaletteDelegate(QWidget* parent = nullptr);
    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const override;
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const override;

protected:
    QColor borderCol_;
};

// Model to dislay/select the suites
class MvQPaletteModel : public QAbstractItemModel
{
public:
    enum CustomItemRole
    {
        SortRole = Qt::UserRole + 1
    };

    explicit MvQPaletteModel(QObject* parent = nullptr);
    ~MvQPaletteModel() override;

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    void setOriginFilter(QString s);
    void setColourFilter(QString s);
    void setNumFilter(QString s);
    void setParamFilter(QString s);
    void setNameFilter(QString s);
    void clearFilter();

protected:
    bool isFiltered(MvQPaletteDbItem* item) const;

    QString nameFilter_;
    QString originFilter_;
    QString colourFilter_;
    QString numFilter_;
    QString paramFilter_;
};

class MvQPaletteSelectionWidget : public QWidget
{
    Q_OBJECT
public:
    MvQPaletteSelectionWidget(QWidget* parent = nullptr);
    void setCurrent(const std::string&);

protected slots:
    void slotOriginFilter(int);
    void slotColourFilter(int);
    void slotNumFilter(int);
    void slotParamFilter(int);
    void slotNameFilter(QString s);
    void slotResetOriginFilter();
    void slotResetColourFilter();
    void slotResetNumFilter();
    void slotResetParamFilter();
    void slotItemSelected(const QModelIndex&, const QModelIndex&);
    void slotClearFilter();
    void slotShowFilters(bool visible);

signals:
    void itemSelected(int);

protected:
    void checkButtonState();
    void showFilters(bool visible);

    bool ignoreFilterChanged_;
    QGridLayout* filterGrid_;
    QToolButton* showFilterTb_;
    QToolButton* resetTb_;

    QLineEdit* nameLe_;
    QComboBox* originCb_;
    QComboBox* colourCb_;
    QComboBox* numCb_;
    QComboBox* paramCb_;

    // QToolButton* nameResetTb_;
    QToolButton* originResetTb_;
    QToolButton* colourResetTb_;
    QToolButton* numResetTb_;
    QToolButton* paramResetTb_;

    QTreeView* tree_;
    MvQPaletteModel* model_;
    QSortFilterProxyModel* sortModel_;
};

class MvQPaletteHelp : public MvQRequestPanelHelp
{
    Q_OBJECT

public:
    MvQPaletteHelp(RequestPanel& owner, const MvIconParameter& param);
    ~MvQPaletteHelp() override = default;

    void start() override {}
    bool dialog() override { return false; }
    QWidget* widget() override { return selector_; }

public slots:
    void slotSelected(int);

protected:
    void refresh(const std::vector<std::string>&) override;

private:
    MvQPaletteSelectionWidget* selector_;
    QString oriName_;
};

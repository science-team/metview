/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QGridLayout>

#include <set>

#include "MvIconLanguage.h"
#include "IconObject.h"

class Editor;
class RequestPanelItem;
class IconClass;
class MvQRequestPanelWidget;

class RequestPanel : public QGridLayout, public MvIconLanguageScanner
{
public:
    RequestPanel(const IconClass&, MvQRequestPanelWidget*, Editor*, bool, const char* = nullptr);
    ~RequestPanel() override;

    void changed();
    void changed(RequestPanelItem&, bool = true);
    void set(const char*, const std::string&);
    void set(const char*, const std::vector<std::string>&);
    void clear(const char*);

    void registerObserver(RequestPanelItem*);
    const IconClass& iconClass();

    // Widget where();
    // Widget main();

    void scan();

    void apply();
    void reset(IconObject*);
    void edit(IconObject*);
    void close();
    void setToDefault(RequestPanelItem*);
    void itemWasChangedToDefault();
    void setFilter(QString);
    void runFilter();
    void applyTemporaryRules(bool temporary);
    int filterNonMatchCount();
    void setShowDisabledItems(bool);

    void replace(IconObject*);
    void merge(IconObject*);

    const Request& request(long = EXPAND_2ND_NAME);
    void request(const Request&, long = EXPAND_2ND_NAME);
    IconObjectH currentObject();

    int defaultCnt();
    void updateParentWidget();
    bool showDisabledItems() const { return showDisabledItems_; }

protected:
    std::vector<RequestPanelItem*> items_;
    const IconClass& class_;
    bool apply_;

    using Method = void (RequestPanelItem::*)();
    void call(Method);

private:
    RequestPanel(const RequestPanel&);
    RequestPanel& operator=(const RequestPanel&);

    void next(const MvIconParameter&) override;

    std::map<long, Request> cache_;
    MvQRequestPanelWidget* panelWidget_;
    Editor* owner_;
    std::vector<RequestPanelItem*> dependentItems_;
    bool showDisabledItems_;
    QString filterText_;
};

inline void destroy(RequestPanel**) {}

#if 0
template<class T>
class XLEditor : public RequestPanel, public T {
public:
	XLEditor(IconClass& c): 
		RequestPanel(c)
	{
		T::create(main());
		XtManageChild(T::xd_rootwidget());
	}
};
#endif

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(RequestPanel);
//#endif

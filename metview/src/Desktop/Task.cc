/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Task.h"
#include "Queue.h"

Task::Task() :
    stopped_(false)
{
}

Task::~Task() = default;

void Task::add(TaskObserver* o)
{
    observers_.insert(o);
}

void Task::success(const Request& r)
{
    TaskH save = this;  // Make sure we don't get deleted

    for (auto observer : observers_)
        observer->success(this, r);
}

void Task::failure(const Request& r)
{
    TaskH save = this;  // Make sure we don't get deleted

    for (auto observer : observers_)
        observer->failure(this, r);
}

void Task::stop()
{
    stopped_ = true;
}

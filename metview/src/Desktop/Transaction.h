/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Metview.h"

class Transaction : public MvTransaction
{
public:
    Transaction(const char*);
    Transaction(MvTransaction*);
    ~Transaction() override;

    MvTransaction* cloneSelf() override = 0;
    void callback(MvRequest&) override = 0;

private:
    // No copy allowed
    Transaction(const Transaction&);
    Transaction& operator=(const Transaction&);
};

inline void destroy(Transaction**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(Transaction);
//#endif

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQListHelp.h"

#include <QListWidget>
#include <QScrollBar>
#include <QVector>

#include "HelpFactory.h"
#include "RequestPanel.h"
#include "MvQFilterTreeWidget.h"


class ParamHelperList : public ParameterScanner
{
public:
    ParamHelperList(const MvIconParameter& param);

    QStringList header_;
    QVector<QStringList> value_;

protected:
    void next(const MvIconParameter&, const char* first, const char* second) override;
};

ParamHelperList::ParamHelperList(const MvIconParameter& param)
{
    header_ << "Value"
            << "Description";
    value_ << QStringList() << QStringList();
    param.scan(*this);
}

void ParamHelperList::next(const MvIconParameter& p, const char* first, const char* second)
{
    if (isalnum(*first)) {
        std::string s(first);
        auto sb = p.beautifiedName(first);
        value_[1] << QString::fromStdString(sb);
        if (second) {
            value_[0] << QString(second);
        }
        else {
            value_[0] << QString(first);
        }
    }
}

//----------------------------------
// MvQListHelp
//----------------------------------

MvQListHelp::MvQListHelp(RequestPanel& owner, const MvIconParameter& param) :
    MvQRequestPanelHelp(owner, param)
{
    multiple_ = param.multiple();

    // Create list
    auto paramList = ParamHelperList(param);

    selector_ = new MvQFilterTreeWidget(paramList.header_, paramList.value_, 0,
                                        multiple_, parentWidget_);
    selector_->adjustHeight();

    connect(selector_, SIGNAL(valueCheckedChanged(QString, bool)),
            this, SLOT(slotSelectionChanged(QString, bool)));
}

QWidget* MvQListHelp::widget()
{
    return selector_;
}

void MvQListHelp::refresh(const std::vector<std::string>& values)
{
    if (values.size() > 0) {
        if (!multiple_) {
            QString txt(values[0].c_str());
            selector_->setCurrentValue(txt);
        }
        else {
            QStringList lst;
            for (auto s : values) {
                lst << QString::fromStdString(s);
            }
            selector_->updateValueChecked(lst);
        }
    }
}

void MvQListHelp::slotSelectionChanged(QString s, bool)
{
    if (!multiple_) {
        std::vector<std::string> val;
        val.push_back(s.toStdString());
        emit edited(val);
    }
    else {
        QStringList val;
        QVector<bool> st;
        selector_->queryValueChecked(val, st);
        emit edited(val, st);
    }
}

static HelpMaker<MvQListHelp> maker("help_multiple_selection");

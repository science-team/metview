/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QFileInfo>
#include <QList>
#include <QMap>
#include <QMainWindow>
#include <QList>
#include <QSessionManager>
#include <QSettings>
#include <QString>

#include "IconObject.h"

#include "Desktop.h"
#include "MvQMainWindow.h"

class Folder;

class IconClass;
class IconObject;

class QAction;
class QActionGroup;
class QCloseEvent;
class QLabel;
class QListView;
class QMenu;
class QStackedWidget;
class QSplitter;
class QVBoxLayout;

class MvQBookmarks;
class MvQBookmarksPanel;
class MvQDesktop;
class MvQDrawerPanel;
class MvQFileBrowserWidget;
class MvQFolderHistory;
class MvQFolderNavigation;
class MvQFolderPanel;
class MvQNewIconPanel;
class MvQNewIconWidget;
class MvQPathWidget;
class MvQFolderSearchPanel;
class MvQSlider;
class PreviewPanel;


class MvQFileBrowser : public MvQMainWindow
{
    Q_OBJECT

public:
    MvQFileBrowser(QStringList, QWidget* parent = nullptr);
    ~MvQFileBrowser() override;

    static void init();
    static void showBrowsers();
    static void openBrowser(QString path, QWidget* fromW = nullptr);
    static void openBrowser(QStringList, QWidget* fromW = nullptr);
    static void locate(QWidget*, QString, QString);

public slots:
    void slotQuit();
    void slotCommitDataRequest(QSessionManager& manager);
    void slotOpenInBrowser(QString);
    void slotLookupOrOpenInTab(QString fullName);
    void slotOpenInNewTab(QString);
    void slotOpenInWin(QString);
    void slotOpenInTabs(QStringList);
    void slotOpenGroupInWin(QStringList);
    void slotBookmarkFolder();
    void slotBookmarkTabs();
    void slotItemInfo(IconObject*);
    void slotChangeViewMode(QAction*);
    void slotIconSizeDown();
    void slotIconSizeUp();
    void slotGlobalIconSize();
    void slotSaveViewAsDefault();
    void slotRevertViewToDefault();
    void slotSetIconSizeByIndex(int);
    void slotPreferences();
    void slotLog();
    void slotAdvancedSearch();
    void slotCurrentFolderChanged();
    void slotPathChanged();
    void slotUpdateNavigationActions(MvQFolderNavigation*);
    void slotSidebar(QAction*);
    void slotCloseSidebar();
    void slotClosePreview();
    void slotLoadMetviewUIFolders();
    void slotShowAboutBox();
    void slotProductBrowser();

protected:
    void showIt();
    void closeEvent(QCloseEvent*) override;

    void setupFileActions();
    void setupEditActions();
    void setupNavigateActions();
    void setupBookmarksActions();
    void setupHistoryActions();
    void setupViewActions();
    void setupEditorActions();
    void setupToolsActions();
    void setupHelpActions();

    void updateSidebarStatus();
    bool sidebarEnabled();
    void connectBookmarksObject(MvQBookmarks*);
    void updateViewModeStatus(Desktop::FolderViewMode);
    void setupIconSizeWidget();
    void updateIconSizeActionState();
    void updateSearchPanel(bool forced = false);
    void forceIconSize(int);
    void updateWindowTitle();

    void saveFolderInfo();
    void writeSettings(QSettings&);
    void readSettings(QSettings&);

    static MvQFileBrowser* makeBrowser(QSettings&);
    static MvQFileBrowser* makeBrowser();
    static MvQFileBrowser* makeBrowser(QString path);
    static MvQFileBrowser* makeBrowser(QStringList);
    static void aboutToQuit(MvQFileBrowser*);
    static void loadMetviewUIFolders();
    static void save(MvQFileBrowser*);
    static void broadcastIconSize(int);
    static MvQFileBrowser* findBrowser(QWidget* childW);

    void init(MvQFileBrowser*);
    bool showIcon(IconObject* obj, bool addFolder);


    static bool quitStarted_;
    static bool sessionLogoutStarted_;
    static QList<MvQFileBrowser*> browsers_;

    MvQMainWindow::MenuItemMap menuItems_;

    QActionGroup* viewModeAg_;
    QMap<Desktop::FolderViewMode, QAction*> actionViewMode_;
    QAction* actionBack_;
    QAction* actionForward_;
    QAction* actionParent_;
    QAction* actionMvHome_;
    QAction* actionWastebasket_;
    QAction* actionDefaults_;
    QAction* actionDrawers_;
    QAction* actionStatusbar_;
    QAction* actionPreview_;
    QMenu* gridMenu_;

    QSplitter* folderSplitter_;
    QSplitter* mainSplitter_;
    QSplitter* drawerSplitter_;

    QActionGroup* sidebarAg_;
    QStackedWidget* sidebar_;
    MvQBookmarksPanel* bookmarksPanel_;
    MvQFolderPanel* folderPanel_;
    MvQFolderSearchPanel* searchPanel_;
    PreviewPanel* previewPanel_;
    bool ignoreSidebarAction_;

    MvQDrawerPanel* drawerPanel_;

    QAction* actionIconSizeDown_;
    QAction* actionIconSizeUp_;
    QAction* actionGlobalIconSize_;
    QAction* actionSaveViewAsDefault_;
    QAction* actionRevertViewToDefault_;
    QList<int> iconSizes_;
    MvQSlider* iconSizeSlider_;
    bool ignoreIconSizeSlider_;

    QLabel* itemInfoLabel_;

    MvQPathWidget* breadcrumbs_;
};

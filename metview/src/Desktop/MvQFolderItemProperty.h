/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QFont>
#include <QMap>
#include <QFontMetrics>

class IconObject;

#include "Desktop.h"

class MvQAbstractIconItemProperty
{
public:
    MvQAbstractIconItemProperty();
    virtual ~MvQAbstractIconItemProperty() = default;

    int iconSize() const { return iconSize_; }
    QFont font() const { return font_; }
    float scaling() const { return scaling_; }
    int textHeight();
    int textOffset() const { return textOffset_; }
    bool setIconSize(int);
    int paddingX() const { return paddingX_; }
    int paddingY() const { return paddingY_; }

    virtual QPoint position(QString, QPoint) = 0;
    virtual QPoint positionFromHotSpot(QString name, QPoint pos, QPointF hotSpotAtPos) = 0;
    virtual QPoint referencePosition(QString, QPoint) = 0;
    virtual QPoint adjsutReferencePosition(QString, QString, QPoint) = 0;
    virtual QSize size(QString) = 0;
    virtual QRect textRect(QString) = 0;
    virtual QRect iconRect(QString) = 0;
    virtual QPoint storedPosition(QPoint viewRefPos) = 0;
    virtual QPoint referencePosition(IconObject*) = 0;
    virtual QPoint referencePositionFromStored(QPoint storedPos) = 0;

protected:
    virtual void update() = 0;
    int textWidth(QString);
    int refTextWidth(QString);
    int refTextHeight();

    int iconSize_{32};
    const int refIconSize_{32};
    float scaling_{1.};

    QFont font_;
    QFont refFont_;
    QFontMetrics fm_;
    QFontMetrics refFm_;
    int height_{0};
    int refHeight_{0};
    int textOffset_{0};
    int paddingX_{3};
    int paddingY_{3};
};

class MvQClassicIconItemProperty : public MvQAbstractIconItemProperty
{
public:
    MvQClassicIconItemProperty();

    QPoint position(QString, QPoint) override;
    QPoint positionFromHotSpot(QString name, QPoint pos, QPointF hotSpotAtPos) override;
    QPoint referencePosition(QString, QPoint) override;
    QPoint adjsutReferencePosition(QString, QString, QPoint) override;
    QSize size(QString) override;
    QRect textRect(QString) override;
    QRect iconRect(QString) override;
    QPoint storedPosition(QPoint viewRefPos) override;
    QPoint referencePosition(IconObject*) override;
    QPoint referencePositionFromStored(QPoint storedPos) override;

protected:
    void update() override;
};

class MvQSimpleIconItemProperty : public MvQAbstractIconItemProperty
{
public:
    MvQSimpleIconItemProperty();

    QPoint position(QString, QPoint) override;
    QPoint positionFromHotSpot(QString name, QPoint pos, QPointF hotSpotAtPos) override;
    QPoint referencePosition(QString, QPoint) override;
    QPoint adjsutReferencePosition(QString, QString, QPoint) override;
    QSize size(QString) override;
    QRect textRect(QString) override;
    QRect iconRect(QString) override;
    QPoint storedPosition(QPoint viewRefPos) override;
    QPoint referencePosition(IconObject*) override;
    QPoint referencePositionFromStored(QPoint storedPos) override;

protected:
    void update() override;
};

class MvQDetailedIconItemProperty : public MvQAbstractIconItemProperty
{
public:
    MvQDetailedIconItemProperty();

    QPoint position(QString, QPoint) override;
    QPoint positionFromHotSpot(QString name, QPoint pos, QPointF hotSpotAtPos) override;
    QPoint referencePosition(QString, QPoint) override;
    QPoint adjsutReferencePosition(QString, QString, QPoint) override;
    QSize size(QString) override;
    QRect textRect(QString) override;
    QRect iconRect(QString) override;
    QPoint storedPosition(QPoint viewRefPos) override;
    QPoint referencePosition(IconObject*) override;
    QPoint referencePositionFromStored(QPoint storedPos) override;

protected:
    void update() override;
};


class MvQFolderItemProperty
{
public:
    // enum TextPosMode {TextBelow,TextBeside};

    MvQFolderItemProperty();
    ~MvQFolderItemProperty();
    int iconSize();
    float scaling();
    QFont font();
    QSize size(QString);
    int textHeight();
    int textOffset();
    int paddingX() const;
    int paddingY() const;
    QRect textRect(QString);
    QRect iconRect(QString);

    bool setIconSize(int);
    QPoint position(QString, QPoint);
    QPoint positionFromHotSpot(QString name, QPoint pos, QPointF hotSpotAtPos);
    QPoint referencePosition(QString, QPoint);
    QPoint adjsutReferencePosition(QString, QString, QPoint);
    void setViewMode(Desktop::FolderViewMode);
    Desktop::FolderViewMode viewMode() const { return viewMode_; }
    QPoint storedPosition(QPoint viewRefPos);
    QPoint referencePosition(IconObject*);
    QPoint referencePositionFromStored(QPoint storedPos);

protected:
    int iconSize_;
    Desktop::FolderViewMode viewMode_;
    QMap<Desktop::FolderViewMode, MvQAbstractIconItemProperty*> prop_;
    MvQAbstractIconItemProperty* current_;
};

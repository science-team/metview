/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQPaletteLine.h"

#include <QDebug>
#include <QHBoxLayout>
#include <QLabel>
#include <QToolButton>

#include "MvQPalette.h"
#include "MvQPaletteDb.h"

#include "MvQRequestPanelHelp.h"
#include "MvMiscellaneous.h"

#include "LineFactory.h"
#include "RequestPanel.h"

class MvQPaletteLineWidget : public QWidget
{
public:
    MvQPaletteLineWidget(MvQPaletteLine* owner, QWidget* parent) :
        QWidget(parent),
        owner_(owner) {}

protected:
    void mousePressEvent(QMouseEvent*) override
    {
        owner_->widgetClicked();
    }

    MvQPaletteLine* owner_;
};

MvQPaletteLine::MvQPaletteLine(RequestPanel& owner, const MvIconParameter& param) :
    MvQRequestPanelLine(owner, param)
{
    MvQPaletteDb::instance();

    auto* w = new MvQPaletteLineWidget(this, parentWidget_);
    auto* hb = new QHBoxLayout(w);
    hb->setContentsMargins(0, 0, 0, 0);

    palPixLabel_ = new QLabel(w);
    palNameLabel_ = new QLabel(w);
    hb->addWidget(palPixLabel_);
    hb->addWidget(palNameLabel_, 1);

    QFont font;
    QFontMetrics fm(font);
    pixSize_ = QSize(180, fm.height() + 4);

    owner_.addWidget(w, row_, 3);

#if 0
    connect(colCb_,SIGNAL(currentIndexChanged(int)),
        this,SLOT(slotCurrentChanged(int)));
#endif
}

void MvQPaletteLine::widgetClicked()
{
    if (helpTb_) {
        if (!helpTb_->isChecked())
            helpTb_->toggle();
    }
}


void MvQPaletteLine::next(const MvIconParameter&, const char* /*first*/, const char* /*second*/)
{
}

void MvQPaletteLine::refresh(const std::vector<std::string>& values)
{
    if (values.size() > 0) {
        if (MvQPaletteDbItem* item = MvQPaletteDb::instance()->find(values[0])) {
            palPixLabel_->setPixmap(item->makePixmap(pixSize_));
            palNameLabel_->setText(item->name());
            updateHelper();
            return;
        }
    }

    palPixLabel_->setPixmap(QPixmap());
    palNameLabel_->clear();
    // changed_ = false;
}

void MvQPaletteLine::slotHelperEdited(const std::vector<std::string>& values)
{
    if (values.size() > 0 && !values[0].empty()) {
        int idx = metview::fromString<int>(values[0]);
        if (idx >= 0 && idx < MvQPaletteDb::instance()->items().count()) {
            MvQPaletteDbItem* item = MvQPaletteDb::instance()->items()[idx];
            Q_ASSERT(item);
            palPixLabel_->setPixmap(item->makePixmap(pixSize_));
            palNameLabel_->setText(item->name());
            owner_.set(param_.name(), item->name().toStdString());
        }


#if 0
        for(int i=0; i < colCb_->count(); i++)
        {
            if(colCb_->itemData(i).toString().toStdString() == values[0])
            {
                colCb_->setCurrentIndex(i);
                return;
            }
        }

        QString name=QString::fromStdString(values[0]);
        //This sets the current index as well
        setCustomColour(MvQPalette::magics(values[0]),name,name,true);
#endif

        // If the current index did not change the slotCurrentChanged() was
        // not called, so here we need to notify the owner about the change!!
        // if(colCb_->currentIndex() ==  currentPrev)
        //{
        //	owner_.set(param_.name(),name.toStdString());
        // }
    }
}


void MvQPaletteLine::updateHelper()
{
    if (!helper_)
        return;

    std::vector<std::string> vals;
    vals.push_back(palNameLabel_->text().toStdString());
    helper_->refresh(vals);
}

#if 0
void MvQPaletteLine::slotHelperEditConfirmed()
{
    int index=colCb_->currentIndex();
    if(index != -1)
    {
        QString name=colCb_->itemData(index).toString();
        owner_.set(param_.name(),name.toStdString());
    }
}
#endif

static LineMaker<MvQPaletteLine> maker1("palette");

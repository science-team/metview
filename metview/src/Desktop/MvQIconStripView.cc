/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QApplication>
#include <QDataStream>
#include <QDropEvent>
#include <QDebug>
#include <QMouseEvent>
#include <QMenu>
#include <QPainter>
#include <QScrollBar>
#include <QShortcut>
#include <QTimer>

#include "MvQIconStripView.h"

#include "IconClass.h"
#include "IconInfo.h"
#include "IconObject.h"
#include "Folder.h"

#include "MvQFolderModel.h"
#include "MvQFolderViewDelegate.h"
#include "MvQContextMenu.h"
#include "MvQIconMimeData.h"

MvQIconStripView::MvQIconStripView(MvQFolderModel* folderModel, QWidget* parent) :
    MvQListFolderViewBase(folderModel, parent)
{
    setContextMenuPolicy(Qt::CustomContextMenu);
    setViewMode(QListView::IconMode);
    setFlow(QListView::LeftToRight);
    setMovement(QListView::Static);
    setResizeMode(QListView::Fixed);

    // Need to set all these again because setting movement
    // to static disables the whole drag and drop in the view
    // and even in the viewport!!!
    setDragEnabled(true);
    setAcceptDrops(true);
    setDropIndicatorShown(true);
    setDragDropMode(QAbstractItemView::DragDrop);
    viewport()->setAcceptDrops(true);

    // Icons can be moved to/from these folders
    setAllowMoveAction(false);
    setEnterFolders(false);

    setSpacing(6);

    setWrapping(false);

    folderModel_->setIconSize(20);
}


MvQIconStripView::~MvQIconStripView() = default;

void MvQIconStripView::performDrop(Qt::DropAction, const MvQIconMimeData* data,
                                   QPoint, bool fromSameView)
{
    // Drop from another view/model  --> always copy!!!
    if (!fromSameView) {
        foreach (IconObject* obj, data->objects()) {
#if 0
            //Cannot move/copy edited icons into another folder!!!
            //if (!obj->editor() && isAccepted(obj)) {
#endif
            if (isAccepted(obj)) {
                obj->clone(folderModel_->folder(), false);
            }
        }
    }
}

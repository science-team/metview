/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QSettings>
#include <QTreeView>

#include "MvQFolderViewBase.h"

class MvQActionList;
class MvQContextItemSet;
class MvQDetailedViewDelegate;
class MvQIconMimeData;
class Folder;

#include "IconObject.h"

class MvQDetailedFolderView : public QTreeView, public MvQFolderViewBase
{
    Q_OBJECT

public:
    MvQDetailedFolderView(MvQFolderModel*, MvQActionList*, MvQActionList*, QWidget* parent = nullptr);
    ~MvQDetailedFolderView() override;
    static void writeGlobalSettings(QSettings&);
    static void readGlobalSettings(QSettings&);
    QWidget* concreteWidget() override;
    void doReset() override;

public slots:
    void slotIconShortCut();
    void slotDefaultShortCut();
    void slotDesktopShortCut();
    void slotContextMenu(const QPoint&);
    void slotSelectItem(const QModelIndex&);
    void slotEntered(const QModelIndex&);
    void slotDoubleClickItem(const QModelIndex&);
    void slotObjectRenamed(const QModelIndex&, QString);
    void slotIconSizeChanged();
    void slotHeaderContextMenu(const QPoint&);

signals:
    void currentFolderChanged(Folder*);
    void itemEntered(IconObject*);
    void iconCommandRequested(QString, IconObjectH);
    void desktopCommandRequested(QString, QPoint);

protected:
    void attachModel() override;
    void detachModel() override;

    MvQContextItemSet* cmSet() override;
    void setupShortCut() override;
    void setPositions() {}
    void blink(const QModelIndex&) override;
    void showIcon(const QModelIndex&) override;
    void rename(IconObject*) override;

    void changeEvent(QEvent* event) override;
    void leaveEvent(QEvent* event) override;
    void mousePressEvent(QMouseEvent*) override;
    void mouseMoveEvent(QMouseEvent*) override;
    void performDrag(Qt::DropAction, QPoint);
    void checkDropTarget(QDropEvent* event);
    void removeDropTarget();
    void dragEnterEvent(QDragEnterEvent*) override;
    void dragMoveEvent(QDragMoveEvent*) override;
    void dragLeaveEvent(QDragLeaveEvent*) override;
    void dropEvent(QDropEvent*) override;
    void performDrop(Qt::DropAction dropAction, const MvQIconMimeData* data,
                     QPoint cursorScenePos, bool fromSameView) override;

    void keyPressEvent(QKeyEvent* event) override;

    QModelIndexList selectedList();
    QRect itemRect(IconObject*) override;
    QRect itemRect(QList<IconObject*>) override;
    QRect pixmapRect(QList<IconObject*>) override;
    QRect pixmapRect(IconObject*) override;

    bool ignoreItemPositionForCm() override { return true; }

    void folderChanged() override;
    void iconCommandFromMain(QString name) override;
    void iconCommand(QString, IconObjectH) override;
    void desktopCommand(QString, QPoint) override;

    static void broadcastHeaderChange();
    void updateHeader();

    QPoint startPos_;
    bool moveActionEnabled_;
    MvQDetailedViewDelegate* delegate_;
    QShortcut* defaultShortCut_;
    MvQActionList* appIconActions_;
    MvQActionList* appDesktopActions_;
    IconObject* itemInfo_{nullptr};
};

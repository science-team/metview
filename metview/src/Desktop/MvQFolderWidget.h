/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QModelIndex>
#include <QSettings>
#include <QWidget>

#include "Desktop.h"
#include "FolderPresenter.h"
#include "IconObject.h"

class Folder;
class FolderSearchData;
class IconClass;
class IconObject;

class QStackedLayout;

class MvQActionList;
class MvQFolderHistory;
class MvQFolderModel;
class MvQFolderNavigation;
class MvQFolderViewBase;
class MvQFolderViewHandler;


class MvQFolderWidget : public QWidget, public FolderPresenter
{
    Q_OBJECT

public:
    MvQFolderWidget(QString, MvQFolderHistory*, MvQActionList*, MvQActionList*, QWidget* parent = nullptr);
    ~MvQFolderWidget() override;

    // From FolderPresenter
    Folder* currentFolder() override;
    bool isDisplayed() override { return isVisible(); }
    QObject* realObject() override;

    QString currentFolderName();
    QString currentFolderPath();
    QString currentFolderFsPath();
    void saveFolderInfo();
    MvQFolderNavigation* folderNavigation() const { return folderNavigation_; }

    int iconSize() const;
    void setIconSize(int);

    void chFolderBack();
    void chFolderForward();
    void chFolderParent();
    void chFolder(QString);
    void chFolderFromHistory(QString);
    void chFolderFromBookmarks(QString);
    void chFolderFromBreadcrumbs(QString);
    void toGrid(Desktop::GridSortMode);
    void reload();
    void showLastCreated();
    void showIcon(IconObject*);
    void findIcons(FolderSearchData*);
    void iconCommandFromMain(QString);

    Desktop::FolderViewMode viewMode();
    void setViewMode(Desktop::FolderViewMode);
    bool viewModeGrid();

    void writeSettings(QSettings&);
    void readSettings(QSettings&);

public slots:
    void slotFolderReplacedInView(Folder*);
    void slotFolderChanged(Folder*);

signals:
    void iconCommandRequested(QString, IconObjectH);
    void desktopCommandRequested(QString, QPoint);
    void itemInfoChanged(IconObject*);
    void pathChanged();

protected:
    void showEvent(QShowEvent* event) override;
    void hideEvent(QHideEvent* event) override;

private:
    QModelIndex changeFolder(const QModelIndex&);

    QStackedLayout* centralLayout_;
    MvQFolderViewHandler* views_;
    MvQFolderHistory* folderHistory_;
    MvQFolderNavigation* folderNavigation_;
    MvQFolderModel* folderModel_;

    // stores the initial path, even if the folder itself
    // does not exists
    QString expectedFolderPath_;
};

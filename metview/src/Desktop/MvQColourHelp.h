/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

// include "MvQPalette.h"
#include "MvQRequestPanelHelp.h"
#include "MvIconParameter.h"

#include <QPen>
#include <QWidget>

class QLabel;
class QLineEdit;
class QPushButton;
class QSlider;
class QSpinBox;
class QTabWidget;
class QToolButton;

class RequestPanel;
class MvQColourSelectionWidget;

class MvQColourHelp : public MvQRequestPanelHelp
{
    Q_OBJECT

public:
    MvQColourHelp(RequestPanel& owner, const MvIconParameter& param);
    ~MvQColourHelp() override = default;

    void start() override {}
    bool dialog() override { return false; }
    QWidget* widget() override;

public slots:
    void slotSelected(QColor);

protected:
    void refresh(const std::vector<std::string>&) override;

private:
    MvQColourSelectionWidget* selector_;
    QString oriName_;
};

class MvQColourListHelp : public MvQRequestPanelHelp
{
    Q_OBJECT

public:
    MvQColourListHelp(RequestPanel& owner, const MvIconParameter& param);
    ~MvQColourListHelp() override = default;

    void start() override {}
    bool dialog() override { return false; }
    QWidget* widget() override;
    void setExternalActions(QList<QAction*>) override;

public slots:
    void slotSelected(QColor);
    void slotReset(bool);

protected:
    void refresh(const std::vector<std::string>&) override;
    void setOriColourBox(QColor colour);

    QToolButton* insertBeforeTb_;
    QToolButton* insertAfterTb_;
    QToolButton* deleteCellTb_;
    QWidget* w_;
    QLabel* oriColorBox_;
    QToolButton* resetTb_;
    QLabel* resetLabel_;

private:
    MvQColourSelectionWidget* selector_;
    QColor oriColour_;
};

class MvQColourGradHelp : public MvQRequestPanelHelp
{
    Q_OBJECT

public:
    MvQColourGradHelp(RequestPanel& owner, const MvIconParameter& param);
    ~MvQColourGradHelp() override = default;

    void start() override {}
    bool dialog() override { return false; }
    QWidget* widget() override;
    void setExternalActions(QList<QAction*>) override;

public slots:
    void slotSelected(QColor);
    void slotReset(bool);

protected:
    void refresh(const std::vector<std::string>&) override;

private:
    void setOriColourBox(QColor colour);

    QToolButton* insertBeforeTb_;
    QToolButton* insertAfterTb_;
    QToolButton* deleteCellTb_;
    QWidget* w_;
    QLabel* oriColorBox_;
    QToolButton* resetTb_;
    QLabel* resetLabel_;

private:
    MvQColourSelectionWidget* selector_;
    QColor oriColour_;
};

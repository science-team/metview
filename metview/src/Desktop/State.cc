/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "State.h"

#include <iostream>

#include "ConfigLoader.h"
#include "IconClass.h"
#include "Service.h"


void State::load(request* r)
{
    // class
    // action
    // output_class

    int nc = count_values(r, "class");
    if (nc == 0)
        nc = 1;
    int na = count_values(r, "action");
    if (na == 0)
        na = 1;
    int no = count_values(r, "output_class");
    if (no == 0)
        no = 1;
    int nm = count_values(r, "mode");
    if (nm == 0)
        nm = 1;

    const char* s = get_value(r, "service", 0);
    if (s == nullptr)
        return;


    Service* p = Service::find(s);
    if (p == nullptr) {
        std::cout << "Cannot find service " << s << std::endl;
        return;
    }

    for (int ic = 0; ic < nc; ic++) {
        const char* c = get_value(r, "class", ic);
        const IconClass& cc = IconClass::find(c ? c : "*");

        for (int ia = 0; ia < na; ia++) {
            const char* a = get_value(r, "action", ia);

            for (int im = 0; im < nm; im++) {
                const char* m = get_value(r, "mode", im);
                for (int io = 0; io < no; io++) {
                    const char* o = get_value(r, "output_class", io);
                    const IconClass* oc = o ? &IconClass::find(o) : nullptr;
                    cc.service(Action(a ? a : "*", m ? m : "*"), oc, p);
                }
            }
        }
    }
}

static SimpleLoader<State> loadClasses("state", 2);

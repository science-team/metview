/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QMenu>
#include <QList>

class Editor;

class MvQEditorListMenu : public QObject
{
    Q_OBJECT

public:
    MvQEditorListMenu(QMenu*);
    ~MvQEditorListMenu() override;

    static void add(Editor*);
    static void remove(Editor*);
    static void initDockMenu();

public slots:
    // void slotUpdate();
    void slotMenu(QAction*);

protected:
    static void sortList();
    void updateMenu();

    QMenu* menu_;
    static QList<Editor*> editors_;
    static QList<MvQEditorListMenu*> items_;

#ifdef __APPLE__
    static MvQEditorListMenu* dockMenu_;
#endif

    // QAction* actionFolder_;
    // QAction* actionTabs_;
    // QAction* actionAdd_;
};

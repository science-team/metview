/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQRequestPanelHelp.h"

#include "HelpFactory.h"
#include "RequestPanel.h"

MvQRequestPanelHelp::MvQRequestPanelHelp(RequestPanel& owner, const MvIconParameter& param) :
    owner_(owner),
    param_(param),
    parentWidget_(owner.parentWidget())
{
}

MvQRequestPanelHelp* MvQRequestPanelHelp::build(RequestPanel& owner, const MvIconParameter& def)
{
    return HelpFactory::create(owner, def);
}
/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include "IconObject.h"
#include "Request.h"

class TemporaryObject : public IconObject
{
public:
    TemporaryObject(IconObject*, const Request&, const IconClass* = nullptr);
    ~TemporaryObject() override;

protected:
    TemporaryObject(const TemporaryObject&);
    TemporaryObject& operator=(const TemporaryObject&);

    void createFiles() override;
    // virtual Log& log();
    Request request() const override;
    void setRequest(const Request&) override;
    std::string fullName() const override;
    Path path() const override;
    // Different implementation of the path() if used by a
    // ShellTask.
    // For TemporaryObject Objects, it will return the PATH of the attached request, (if it exists)
    // This is done to enable the visualisation of the MAGML Objects
    // without possible side effects!
    Path pathForShellTask() const override;
    Path dotPath() const override;
    Folder* parent() const override;

    bool rename(const std::string&) override;
    bool renamable() const override;
    bool temporary() const override;

    Folder* embeddedFolder(const std::string& n, bool) const override;
    Path embeddedPath() const override;
    Folder* embeddedFolder(bool) const override;

    std::string relativeName(IconObject*) const override;
    std::string makeFullName(const std::string&) const override;

    bool storedAsRequestFile() const override { return true; }

private:
    IconObjectH ref_;
    Request request_;
};

inline void destroy(TemporaryObject**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(TemporaryObject);
//#endif

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQSearchPanel.h"

#include <QHBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QPainter>
#include <QPushButton>
#include <QStyle>
#include <QStyleOption>
#include <QToolButton>
#include <QVBoxLayout>

#include "FolderSearchData.h"
#include "MvQLineEdit.h"

MvQFolderSearchPanel::MvQFolderSearchPanel(QWidget* parent) :
    QWidget(parent)

{
    searchData_ = new FolderSearchData;
    searchData_->setObserver(this);

    auto* hb = new QHBoxLayout;
    hb->setSpacing(4);
    hb->setContentsMargins(2, 1, 0, 2);
    setLayout(hb);

    // Name
    auto* label = new QLabel(this);
    label->setText(tr("&Name:"));
    nameLe_ = new MvQLineEdit(this);
    nameLe_->setPlaceholderText("Filter name ...");
    // nameLe_->setDecoration(QPixmap(":/desktop/filter_decor.svg"));

    label->setBuddy(nameLe_);

    hb->addWidget(label);
    hb->addWidget(nameLe_, 1);

    // Type
    label = new QLabel(this);
    label->setText(tr("&Type:"));
    typeLe_ = new MvQLineEdit(this);
    typeLe_->setPlaceholderText("Filter type ...");
    // typeLe_->setDecoration(QPixmap(":/desktop/filter_decor.svg"));
    label->setBuddy(typeLe_);

    hb->addWidget(label);
    hb->addWidget(typeLe_, 1);

    // Buttons
    nextPb_ = new QPushButton(tr("&Next"), this);
    nextPb_->setIcon(QIcon(QPixmap(QString::fromUtf8(":/desktop/arrow_down.svg"))));

    prevPb_ = new QPushButton(tr("&Previous"), this);
    prevPb_->setIcon(QIcon(QPixmap(QString::fromUtf8(":/desktop/arrow_up.svg"))));

    hb->addWidget(nextPb_);
    hb->addWidget(prevPb_);

    hb->addStretch(1);

    connect(nameLe_, SIGNAL(textChanged(QString)),
            this, SLOT(slotFind(QString)));

    connect(nameLe_, SIGNAL(returnPressed()),
            this, SLOT(slotFindNext()));

    connect(typeLe_, SIGNAL(textChanged(QString)),
            this, SLOT(slotFind(QString)));

    connect(typeLe_, SIGNAL(returnPressed()),
            this, SLOT(slotFindNext()));

    connect(nextPb_, SIGNAL(clicked()),
            this, SLOT(slotFindNext()));

    connect(prevPb_, SIGNAL(clicked()),
            this, SLOT(slotFindPrev()));

    auto* closeTb = new QToolButton(this);
    closeTb->setAutoRaise(true);
    closeTb->setIcon(QPixmap(":/desktop/close_grey.svg"));
    closeTb->setToolTip(tr("Close find panel"));
    hb->addWidget(closeTb);

    connect(closeTb, SIGNAL(clicked(bool)),
            this, SLOT(closePanel(bool)));
}

MvQFolderSearchPanel::~MvQFolderSearchPanel()
{
    delete searchData_;
}

void MvQFolderSearchPanel::slotFind(QString)
{
    if (isVisible()) {
        searchData_->setSearchTerm(nameLe_->text().toLower().toStdString(), typeLe_->text().toLower().toStdString());
        emit find(searchData_);
        updateStatus();
        if (IconObject* obj = searchData_->first())
            emit iconSelected(obj);
    }
}

void MvQFolderSearchPanel::slotFindNext()
{
    if (isVisible()) {
        if (IconObject* obj = searchData_->next())
            emit iconSelected(obj);
    }
}

void MvQFolderSearchPanel::slotFindPrev()
{
    if (isVisible()) {
        if (IconObject* obj = searchData_->prev())
            emit iconSelected(obj);
    }
}

void MvQFolderSearchPanel::targetChanged()
{
    if (isVisible()) {
        emit find(searchData_);
        updateStatus();
    }
}

void MvQFolderSearchPanel::openPanel(bool st)
{
    if (st == true) {
        nameLe_->setFocus(Qt::OtherFocusReason);
        show();
        slotFind("");
    }
    else {
        if (isVisible()) {
            hide();
            searchData_->clear();
            updateStatus();
            emit find(searchData_);
        }
    }
}

void MvQFolderSearchPanel::closePanel(bool)
{
    hide();
    searchData_->clear();
    updateStatus();
    emit find(searchData_);
    emit panelClosed();
}

void MvQFolderSearchPanel::matchChanged()
{
    if (isVisible()) {
    }
}

void MvQFolderSearchPanel::updateStatus()
{
}

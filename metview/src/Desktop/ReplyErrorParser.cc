/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "ReplyErrorParser.h"
#include "MvRequest.h"
#include "IconObject.h"

void ReplyErrorParser::parse(const MvRequest& r)
{
    if (r) {
        std::string v, verb;

        if (const char* ch = r.getVerb()) {
            verb = std::string(ch);
        }
        r.getValue("STATUS", v, true);
        if (verb == "ERROR" || v == "ERROR") {
            r.getValue("TEXT", err_, true);
            std::vector<std::string> items;
            r.getValue("FAILED_ITEMS", items, true);
            r.getValue("ITEM_ERRORS", itemsErr_, true);

            for (std::size_t i = 0; i < items.size(); i++) {
                items_.emplace_back(IconObject::search(items[i]));
            }

            r.getValue("DETAILED", detailed_, true);
            if (detailed_.empty() && itemsErr_.size() == 1) {
                detailed_ = itemsErr_[0];
            }
        }
    }
}

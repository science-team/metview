/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <map>
#include <string>

#include "Path.h"


class Folder;
class IconInfo;
class Request;

class FolderInfo
{
public:
    FolderInfo(Folder*);

    void write();
    IconInfo* add(const std::string&, IconInfo*);
    IconInfo* add(const std::string&, const std::string&);
    IconInfo* add(const std::string&, const Request&);
    IconInfo* find(const std::string&);
    void remove(const std::string&);
    void rename(const std::string&, const std::string&);

    static bool read(const Path& folderPath, std::map<std::string, std::string>& icons);

private:
    void read();
    void check();
    static void adjustIcName(std::string& icName);

    Folder* folder_;
    std::map<std::string, IconInfo*> info_;
    bool checkWasDone_;

    static std::string fileName_;
};

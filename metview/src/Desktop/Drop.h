/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <map>
#include <set>
#include <string>

#include "IconObject.h"
#include "Request.h"
#include "TaskObserver.h"

#include <QObject>

class Drop : public QObject, public TaskObserver
{
public:
    Drop(const std::set<IconObject*>&, const Request&, const std::string&, const Request&, QObject* obj = nullptr);
    ~Drop() override;

    void start();

    MvRequest request() { return request_; }

private:
    // No copy allowed
    Drop(const Drop&);
    Drop& operator=(const Drop&);

    void check();
    void success(Task*, const Request&) override;
    void failure(Task*, const Request&) override;

    Request request_;
    std::string service_;
    Request mode_;
    bool error_;
    int waiting_;

    std::map<IconObjectH, Request> requests_;
    std::map<Task*, IconObject*> tasks_;
};

inline void destroy(Drop**) {}

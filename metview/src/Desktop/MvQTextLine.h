/***************************** LICENSE START ***********************************

 Copyright 2015 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQRequestPanelLine.h"

#include <QString>
#include <QStringList>
#include <QVector>

class MvQLineEdit;

class RequestPanel;
class MvIconParameter;

class MvQTextLine : public MvQRequestPanelLine
{
    Q_OBJECT
public:
    MvQTextLine(RequestPanel& owner, const MvIconParameter& param);
    ~MvQTextLine() override = default;

    QString currentValue() { return {}; }
    void addValue(QString) {}

    void refresh(const std::vector<std::string>&) override;

public slots:
    void slotCleared();
    void slotTextEdited(const QString&);
    void slotHelperEdited(const std::vector<std::string>&) override;
    void slotHelperEdited(QStringList, QVector<bool>) override;

protected:
    void dispatchChange();
    void updateHelper();
    void checkValidity();

    MvQLineEdit* lineEdit_;
    bool doNotUpdateHelper_{false};
};

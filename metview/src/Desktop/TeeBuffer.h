/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <ostream>
#include <streambuf>


class TeeBuffer : public std::streambuf
{
public:
    TeeBuffer(std::ostream&, std::ostream&);
    ~TeeBuffer() override;

protected:
    int overflow(int) override;
    int sync() override;

private:
    // No copy allowed
    TeeBuffer(const TeeBuffer&);
    TeeBuffer& operator=(const TeeBuffer&);

    char buffer_[1024];
    std::ostream& one_;
    std::ostream& two_;

    void dumpBuffer();
};

inline void destroy(TeeBuffer**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(TeeBuffer);
//#endif

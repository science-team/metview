/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQEditor.h"

#include "MvIconParameter.h"
#include "TaskObserver.h"
#include "EditorObserver.h"
#include "IconObject.h"
#include "MacroParamState.h"

class QScrollArea;

class RequestPanel;
class MacroParamEditor;
class MacroParamObject;

class MvQIconHolderModel;
class MvQIconHolderView;


class MvQMacroParamDropWidget : public QWidget
{
    Q_OBJECT

public:
    MvQMacroParamDropWidget(MacroParamEditor*, const std::vector<std::string>&, QWidget* parent = nullptr);
    void reset(IconObjectH);

public slots:
    void slotHolderEdited();

protected:
    MacroParamEditor* owner_;
    MvQIconHolderModel* model_;
    MvQIconHolderView* view_;
};

class MacroParamEditor : public MvQEditor
{
    Q_OBJECT

public:
    MacroParamEditor(const IconClass&, const std::string&);
    ~MacroParamEditor() override;

    void editMacro(IconObjectH);
    IconObject* macro() const;

public slots:
    void slotIconDropped(IconObject*);

protected slots:
    void slotFilterItems(QString) override {}
    void slotShowDisabled(bool) override {}

protected:
    void readSettings(QSettings&) override {}
    void writeSettings(QSettings&) override {}

private:
    // No copy allowed
    MacroParamEditor(const MacroParamEditor&);
    MacroParamEditor& operator=(const MacroParamEditor&);

    MacroParamObject* macroParamObject() const;
    void createParamPanel();
    void deleteParamPanel();

    void apply() override;
    void reset() override;
    void close() override;
    void replace(IconObjectH) override;
    void merge(IconObjectH) override;

    virtual IconObject* copy(const std::string&);

    // Iconobserver methods
    void iconEdited(IconObject*) override;
    void iconClosed(IconObject*) override;
    void iconChanged(IconObject*) override;
    void iconDestroyed(IconObject*) override;

    MvQMacroParamDropWidget* macroDropWidget_;
    QScrollArea* paramPanelArea_;
    RequestPanel* paramPanel_;
    MacroParamStateH state_;
    IconObjectH macro_;
    std::vector<std::string> classes_;
};

inline void destroy(MacroParamEditor**) {}

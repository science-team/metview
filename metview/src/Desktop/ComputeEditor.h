/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQEditor.h"

class MvQLineEdit;

class ComputeEditor : public MvQEditor
{
    Q_OBJECT

public:
    ComputeEditor(const IconClass&, const std::string&);
    virtual ~ComputeEditor();

public slots:
    void slotTextChanged();

protected slots:
    void slotFilterItems(QString) {}
    void slotShowDisabled(bool) {}

protected:
    void readSettings(QSettings&) {}
    void writeSettings(QSettings&) {}

private:
    ComputeEditor(const ComputeEditor&);
    ComputeEditor& operator=(const ComputeEditor&);

    virtual void apply();
    virtual void reset();
    virtual void close();
    virtual void merge(IconObjectH);
    virtual void replace(IconObjectH);

    MvQLineEdit* te_;
};

inline void destroy(ComputeEditor**) {}

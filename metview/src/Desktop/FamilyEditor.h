/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <map>
#include <string>

#include "MvQEditor.h"

#include "RequestPanel.h"
#include "MvIconLanguage.h"

class QComboBox;
class QStackedWidget;
class QVBoxLayout;

class IconObject;
class FamilyScanner;
class RequestPanel;
class FamilyEditorObserver;

class FamilyEditor : public MvQEditor
{
    Q_OBJECT
public:
    FamilyEditor(const IconClass&, const std::string&);
    ~FamilyEditor() override = default;
    FamilyEditor(const FamilyEditor&) = delete;
    FamilyEditor& operator=(const FamilyEditor&) = delete;

    void replace(IconObjectH) override;
    void merge(IconObjectH) override;

public slots:
    void slotChangePage(int);
    void slotIconDropped(IconObject*);

protected slots:
    void slotFilterItems(QString) override {}
    void slotShowDisabled(bool) override {}

protected:
    void readSettings(QSettings&) override {}
    void writeSettings(QSettings&) override {}

private:
    void setStartPage();
    void makePages(int, Request& r);
    void keyValues(std::map<std::string, std::string>&);

    void apply() override;
    void reset() override;
    void close() override;
    void edit() override;

    virtual IconObject* copy(const std::string&);

    FamilyScanner& scanner_;
    QList<QComboBox*> combos_;
    QGridLayout* comboLayout_{nullptr};

    QStackedWidget* page_{nullptr};
    std::vector<RequestPanel*> panels_;
    std::vector<IconObjectH> objects_;
};

inline void destroy(FamilyEditor**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(FamilyEditor);
//#endif

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "HelpFactory.h"
#include "Request.h"
#include "MvIconParameter.h"

static std::map<std::string, HelpFactory*>* makers = nullptr;

HelpFactory::HelpFactory(const std::string& name)
{
    if (makers == nullptr)
        makers = new std::map<std::string, HelpFactory*>;

    (*makers)[name] = this;
}

HelpFactory::~HelpFactory() = default;

MvQRequestPanelHelp* HelpFactory::create(RequestPanel& owner, const MvIconParameter& param)
{
    const char* help = param.help();

    if (help) {
        auto j = makers->find(help);
        if (j != makers->end())
            return j->second->make(owner, param);
    }

    return nullptr;
}

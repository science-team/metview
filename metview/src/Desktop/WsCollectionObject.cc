/***************************** LICENSE START ***********************************

 Copyright 2022 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "WsCollectionObject.h"
#include "IconClass.h"
#include "IconFactory.h"
#include "Request.h"

Request WsCollectionObject::request() const
{
    const char* n = className().c_str();
    Request r(n);
    r("PATH") = path().str().c_str();
    return r;
}

void WsCollectionObject::setRequest(const Request&)
{
}

static IconMaker<WsCollectionObject> maker1("WeatherSymbolCollection");

/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQTemporaryEditor.h"

#include <QCheckBox>
#include <QDebug>
#include <QDialogButtonBox>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QInputDialog>
#include <QLabel>
#include <QPushButton>
#include <QSplitter>
#include <QToolButton>
#include <QVBoxLayout>

#include "MvQIconProvider.h"
#include "MvQMethods.h"
#include "MvQTheme.h"

#include "Folder.h"
#include "IconClass.h"
#include "MvRequest.h"
#include "Path.h"
#include "Request.h"

MvQTemporaryEditor::MvQTemporaryEditor(const IconClass& name, const std::string& kind, QWidget* parent) :
    QDialog(parent),
    Editor(name, kind)
{
    auto* vb = new QVBoxLayout;
    vb->setSpacing(0);
    vb->setContentsMargins(1, 1, 1, 1);
    setLayout(vb);

    // Header
    auto* hb = new QHBoxLayout;
    vb->addLayout(hb);

    // TODO: do not use if
    auto* pixLabel = new QLabel(this);
    if (name.name().find("python_") == 0) {
        pixLabel->setPixmap(MvQIconProvider::pixmap(IconClass::find("PYTHON"), 32));
    }
    else {
        pixLabel->setPixmap(MvQIconProvider::pixmap(IconClass::find("MACRO"), 32));
    }

    pixLabel->setFrameShape(QFrame::Box);
    pixLabel->setFrameShape(QFrame::StyledPanel);

    hb->addWidget(pixLabel);

    headerLabel_ = new QLabel("", this);
    headerLabel_->setObjectName(QString::fromUtf8("editorHeaderLabel"));
    headerLabel_->setMargin(8);
    headerLabel_->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    headerLabel_->setAutoFillBackground(true);
    headerLabel_->setTextInteractionFlags(Qt::LinksAccessibleByMouse | Qt::TextSelectableByKeyboard | Qt::TextSelectableByMouse);
    headerLabel_->setFrameShape(QFrame::Box);
    headerLabel_->setFrameShape(QFrame::StyledPanel);

    hb->addWidget(headerLabel_, 1);

    // Panel layout
    auto* w = new QWidget(this);
    centralLayout_ = new QHBoxLayout;
    centralLayout_->setSpacing(0);
    centralLayout_->setContentsMargins(1, 1, 1, 1);
    w->setLayout(centralLayout_);
    vb->addWidget(w);

    //--------------------
    // Buttonbox
    //--------------------

    buttonBox_ = new QDialogButtonBox(this);

    okPb_ = buttonBox_->addButton(QDialogButtonBox::Ok);
    cancelPb_ = buttonBox_->addButton(QDialogButtonBox::Cancel);
    resetPb_ = buttonBox_->addButton(QDialogButtonBox::Reset);

    resetPb_->setEnabled(false);

    connect(buttonBox_, SIGNAL(clicked(QAbstractButton*)),
            this, SLOT(slotButtonClicked(QAbstractButton*)));

    vb->addWidget(buttonBox_);
}

MvQTemporaryEditor::~MvQTemporaryEditor() = default;

void MvQTemporaryEditor::changed()
{
    resetPb_->setEnabled(true);
}

void MvQTemporaryEditor::edit()
{
    if (!current_)
        return;

    updateHeader();

    reset();

    resetPb_->setEnabled(false);
}

void MvQTemporaryEditor::updateHeader()
{
    if (!current_)
        return;

    auto subCol = MvQTheme::subText();
    QString name = "Macro";
    if (class_.name().find("python_") == 0) {
        name = "Python";
    }
    QString t = "Editor started from a " + name + " script";
    headerLabel_->setText(MvQ::formatText(t, subCol));
}

void MvQTemporaryEditor::raiseIt()
{
    if (isMinimized())
        showNormal();

    raise();
}

void MvQTemporaryEditor::closeIt()
{
    // Is this the right thing to do?
    close();
    reject();
    editDone();
}

void MvQTemporaryEditor::slotButtonClicked(QAbstractButton* button)
{
    if (!button)
        return;

    if (buttonBox_->standardButton(button) == QDialogButtonBox::Cancel) {
        close();
        reject();
    }
    else if (buttonBox_->standardButton(button) == QDialogButtonBox::Ok) {
        apply();
        close();
        accept();
    }

    else if (buttonBox_->standardButton(button) == QDialogButtonBox::Reset) {
        reset();
        resetPb_->setEnabled(false);
    }
}

void MvQTemporaryEditor::accept()
{
    notifyObserverApply();
    editDone();
    QDialog::accept();
}

void MvQTemporaryEditor::reject()
{
    notifyObserverClose();
    editDone();
    QDialog::reject();
}

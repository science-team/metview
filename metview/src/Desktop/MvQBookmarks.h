/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QDialog>
#include <QListView>
#include <QMap>
#include <QStyledItemDelegate>

#include "MvQKeyModel.h"

#include <string>


class QAction;
class QLineEdit;
class QMenu;
class QModelIndex;
class QPlainTextEdit;

class MvQContextItemSet;
class MvQKeyManager;
class MvQKeyModel;

class Folder;
class IconObject;
class MvKey;
class MvKeyProfile;
class MvQBookmarksPanel;

class MvQBookmarksModel : public MvQKeyModel
{
public:
    MvQBookmarksModel(MvKeyProfile* prof) :
        MvQKeyModel(prof, ShortNameMode, nullptr) {}
    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
};


class MvQBookmarks : public QObject
{
    Q_OBJECT

public:
    MvQBookmarks(QWidget*);
    ~MvQBookmarks() override = default;

    static void init();
    static void save();
    static bool addItem(IconObject*);
    static bool addItem(QString);
    static bool addItem(QList<Folder*>);
    static bool editItem(QString);
    static bool editItem(MvKey*);
    static bool isBookmark(QString);

signals:
    void openInBrowser(QString);
    void lookupOrOpenInTab(QString);
    void openInNewTab(QString);
    void openInWin(QString);
    void openGroupInTabs(QStringList);
    void openGroupInWin(QStringList);
    void bookmarkFolder();
    void bookmarkTabs();

protected:
    static MvQContextItemSet* cmSet();
    void defaultCommand(const QModelIndex&);
    void command(QString);
    void command(QString, const QModelIndex&);
    static bool addItem(MvKey*);
    bool editItem(const QModelIndex&);
    bool duplicateItem(const QModelIndex&);
    bool deleteItem(const QModelIndex&);

    MvKey* getKey(const QModelIndex&);
    QString getPath(const QModelIndex&);
    QStringList getPathsFromKey(MvKey*);
    static bool isFolderGroup(MvKey*);

    static MvQKeyManager* manager_;
    static MvQBookmarksModel* model_;
    static MvKeyProfile* prof_;
};

class MvQBookmarksMenu : public MvQBookmarks
{
    Q_OBJECT

public:
    MvQBookmarksMenu(QWidget*, QMenu*);
    ~MvQBookmarksMenu() override = default;

public slots:
    void slotUpdate();
    void slotMenu(QAction*);
    void slotContextMenu(const QPoint&);

protected:
    QString getPath(QAction*);
    MvKey* getKey(QAction*);
    QModelIndex getIndex(QAction*);

    QMenu* menu_;
    QAction* actionFolder_;
    QAction* actionTabs_;
    QAction* actionAdd_;
};


class MvQBookmarksView : public QListView
{
public:
    MvQBookmarksView(QWidget* parent = nullptr);

protected:
    void checkDropTarget(QDropEvent* event);
    void removeDropTarget();

    void dragEnterEvent(QDragEnterEvent* event) override;
    void dragMoveEvent(QDragMoveEvent* event) override;
    void dragLeaveEvent(QDragLeaveEvent* event) override;
    void dropEvent(QDropEvent* event) override;
};

class MvQBookmarksPanel : public MvQBookmarks
{
    Q_OBJECT

public:
    MvQBookmarksPanel(QWidget*);
    ~MvQBookmarksPanel() override = default;

    QWidget* widget() { return widget_; }

public slots:
    void slotDoubleClickItem(const QModelIndex&);
    void slotContextMenu(const QPoint&);
    void slotAddItem(bool);

signals:
    void closePanel(bool b = true);

protected:
    QWidget* widget_;
    MvQBookmarksView* view_;
};


class MvQBookmarksDialog : public QDialog
{
    Q_OBJECT

public:
    MvQBookmarksDialog(MvKey*, QString, QWidget* parent = nullptr);
    ~MvQBookmarksDialog() override = default;

public slots:
    void accept() override;

protected:
    MvKey* key_;
    QLineEdit* labelEdit_;
    QLineEdit* pathEdit_;
};

class MvQBookmarksGroupDialog : public QDialog
{
    Q_OBJECT

public:
    MvQBookmarksGroupDialog(MvKey*, QString, QWidget* parent = nullptr);
    ~MvQBookmarksGroupDialog() override = default;

public slots:
    void accept() override;

protected:
    QString getPathsFromKey();

    MvKey* key_;
    QLineEdit* labelEdit_;
    QPlainTextEdit* pathEdit_;
};

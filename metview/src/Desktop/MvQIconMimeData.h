/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QtGlobal>
#include <QMimeData>
#include <QRect>
#include <QStringList>
#include "IconObserver.h"

class MvQFolderModel;
class IconObject;

class MvQNewIconMimeData : public QMimeData
{
    Q_OBJECT

public:
    enum IconDefType
    {
        UserDef,
        SystemDef
    };

    MvQNewIconMimeData(QString, IconDefType);

    QStringList formats() const override;
    QString className() const { return className_; }
    IconDefType iconDefType() const { return iconDefType_; }

protected:
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    QVariant retrieveData(const QString& format, QMetaType type) const override;
#else
    QVariant retrieveData(const QString& mimeType, QVariant::Type type) const;
#endif

    QStringList formats_;
    QString className_;
    IconDefType iconDefType_;
};


class MvQIconMimeData : public QMimeData, IconObserver
{
    Q_OBJECT

public:
    enum ClipboardAction
    {
        NoAction,
        CopyAction,
        CutAction
    };

    MvQIconMimeData(MvQFolderModel*, IconObject*, QPointF, QList<IconObject*>, QList<QPoint>, ClipboardAction clipboardAction = NoAction);
    ~MvQIconMimeData() override;

    QStringList formats() const override;
    MvQFolderModel* model() const { return model_; }
    IconObject* dragObject() const { return dragObject_; }
    QPointF hotSpotInVisRect() const { return hotSpotInVisRect_; }
    QList<IconObject*> objects() const { return objects_; }
    QList<QPoint> pixDistances() const { return pixDistances_; }
    void setFromHelper(bool b) { fromHelper_ = b; }
    bool fromHelper() const { return fromHelper_; }
    void setFromEditor(bool b) { fromEditor_ = b; }
    bool fromEditor() const { return fromEditor_; }
    ClipboardAction clipboardAction() const { return clipboardAction_; }
    bool isEmpty() const;

    void iconDestroyed(IconObject*) override;

protected:
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    QVariant retrieveData(const QString& format, QMetaType type) const override;
#else
    QVariant retrieveData(const QString& mimeType, QVariant::Type type) const;
#endif
    QStringList formats_;
    MvQFolderModel* model_;
    IconObject* dragObject_;
    QPointF hotSpotInVisRect_;
    QList<IconObject*> objects_;
    QList<QPoint> pixDistances_;
    bool fromHelper_;
    bool fromEditor_;
    ClipboardAction clipboardAction_;
    QString objPath_;
};

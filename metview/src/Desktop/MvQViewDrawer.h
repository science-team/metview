/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QObject>

#include "EditorDrawer.h"
#include "IconClass.h"
#include "MvQIconStripView.h"

class MvQContextItemSet;
class MvQViewDrawerView;

class MvQViewDrawer : public QObject, public EditorDrawer, public ClassScanner
{
    Q_OBJECT

public:
    MvQViewDrawer(MvQEditor*);

    QString name() override;
    QWidget* widget() override;
    void next(const IconClass&) override;

public slots:
    void slotCommand(QString, IconObjectH);

protected:
    MvQFolderModel* model_;
    MvQViewDrawerView* view_;
    Folder* folder_;
    std::vector<std::string> classes_;
};


class MvQViewDrawerView : public MvQIconStripView
{
public:
    MvQViewDrawerView(MvQFolderModel*, QWidget* parent = nullptr);
    ~MvQViewDrawerView() override;

protected:
    MvQContextItemSet* cmSet() override;
};

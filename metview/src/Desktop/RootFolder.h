/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "SystemFolder.h"

class RootFolder : public SystemFolder
{
public:
    RootFolder();
    ~RootFolder() override;

private:
    // No copy allowed
    RootFolder(const RootFolder&);
    RootFolder& operator=(const RootFolder&);

    Path path() const override;
    Path dotPath() const override;
    std::string fullName() const override;
    Folder* parent() const override;
    Path logPath() override;
    bool isEmbedded() override { return false; }
};

inline void destroy(RootFolder**) {}

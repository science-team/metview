/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "RootFolder.h"
#include "IconClass.h"
#include "FolderInfo.h"
#include "FolderSettings.h"

RootFolder::RootFolder() :
    SystemFolder(nullptr, IconClass::find("HOME"), "", nullptr)
{
    // Make sure this one is never deleted
    //	_attach();
}

RootFolder::~RootFolder() = default;

Path RootFolder::dotPath() const
{
    std::string p = "/";
    return Path(p);
    // return path().add(".MainWindowResources");
}

std::string RootFolder::fullName() const
{
    return "/";
}

Path RootFolder::path() const
{
    static Path user = std::string(getenv("METVIEW_USER_DIRECTORY"));
    return user;
}

Folder* RootFolder::parent() const
{
    return nullptr;
}

Path RootFolder::logPath()
{
    std::string p = Folder::folder("temporary")->path().str() + "/messages";
    return Path(p);
}

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "SystemFolder.h"

class TemporaryFolder : public SystemFolder
{
public:
    TemporaryFolder(Folder* parent, const IconClass& kind,
                    const std::string& name, IconInfo* info);

    ~TemporaryFolder() override;  // Change to virtual if base class

private:
    // No copy allowed
    TemporaryFolder(const TemporaryFolder&);
    TemporaryFolder& operator=(const TemporaryFolder&);

    void createFiles() override;
};

inline void destroy(TemporaryFolder**) {}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "IconObject.h"

class FolderInfo;
class FolderSettings;
class FolderObserver;
class Request;

class FolderVisitor
{
public:
    virtual void visit(IconObject*) = 0;
};

class Folder : public IconObject
{
public:
    Folder(Folder*, const IconClass& kind, const std::string& name,
           IconInfo* info);

    ~Folder() override;

    IconObject* findMulti(const std::vector<std::string>&) override;
    IconObject* find(const std::string&) override;
    virtual IconObject* create(const std::string&);

    virtual bool adopt(IconObject*);
    virtual bool release(IconObject*);

    // virtual void visit(FolderVisitor&);

    void addObserver(FolderObserver*);
    void removeObserver(FolderObserver*);

    std::string uniqueName(const std::string&);
    std::string duplicateName(const std::string&);
    void position(IconObject*, int x, int y);
    void renamed(IconObject*, const std::string&, const std::string&);

    // virtual void empty();
    void destroy() override;
    void scan();
    void scanForNewFile(const std::string&, int x = 0, int y = 0);
    void recheckKidsIconClass();
    void recheckKidIconClass(IconObject*);

    // void copyContent(Folder*);
    // virtual void drop(IconObject*);

    using TellObserverProc = void (FolderObserver::*)(IconObject*);
    void tellObservers(TellObserverProc, IconObject*);

    // void open();
    // void close();

    virtual bool ancestor(IconObject*);
    virtual bool descendant(Folder*);
    // virtual bool sameAs(IconObject*);
    // virtual void notifyChanged();

    int numOfIconKids();
    const std::map<std::string, IconObjectH>& kids() const { return kids_; }
    void iconClasses(std::map<std::string, IconObjectH>&);
    bool isFolder() const override { return true; }

    Request request() const override;
    void setRequest(const Request&) override {}

    static Folder* top();
    static Folder* folder(const std::string&, bool create = true);
    static Folder* folder(const std::string&, const std::string&, bool create = true);
    static Folder* folder(const std::string&, const std::string&, const std::string&, bool create = true);
    static IconObject* icon(const std::string&);

    void saveFolderInfo();
    FolderInfo* folderInfo();
    FolderSettings* settings();

protected:
    std::set<std::string> can() override;

    using KidMap = std::map<std::string, IconObjectH>;
    KidMap kids_;
    std::set<std::string> noKids_;

    static const unsigned int maxNokidsNum_;
    bool ready_;
    bool scanIsOn_;
    FolderInfo* folderInfo_;
    FolderSettings* settings_;

    std::set<FolderObserver*> observers_;

    static std::vector<Folder*> folders_;

    void createFiles() override;

private:
    // No copy allowed
    Folder(const Folder&);
    Folder& operator=(const Folder&);

    // -- Overridden methods

    // From IconObject
    // virtual void edit();
};

class FolderH : public Handle<Folder>
{
public:
    FolderH(Folder* f) :
        Handle<Folder>(f) {}
};

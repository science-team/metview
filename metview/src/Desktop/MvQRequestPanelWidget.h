/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QWidget>

#include <string>
#include <vector>

class IconClass;
class IconObject;

class MvQRequestPanelWidget : public QWidget
{
    Q_OBJECT

public:
    MvQRequestPanelWidget(const std::vector<std::string>&, QWidget* parent = nullptr);
    void delayedForceRepaint();

signals:
    void iconDropped(IconObject*);

protected slots:
    void forceRepaint();

protected:
    bool isAccepted(const IconClass& kind) const;
    void paintEvent(QPaintEvent*) override;
    void dragEnterEvent(QDragEnterEvent*) override;
    void dragMoveEvent(QDragMoveEvent*) override;
    void dropEvent(QDropEvent*) override;

    static QPen linePen_;
    static QPen framePen_;
    static QBrush bgBrush_;
    static QBrush helperBrush_;
    std::vector<std::string> classes_;
    bool delayedRepaintScheduled_;
};

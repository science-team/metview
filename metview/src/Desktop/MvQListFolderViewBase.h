/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QListView>

#include "MvQ.h"
#include "MvQFolderViewBase.h"
#include "MvQFolderModel.h"

class MvQIconDelegate;
class MvQCurrentFolderFilterModel;

class Folder;
class IconObject;

class MvQListFolderViewBase : public QListView, public MvQFolderViewBase
{
    Q_OBJECT

public:
    MvQListFolderViewBase(MvQFolderModel*, QWidget* parent = nullptr);
    ~MvQListFolderViewBase() override;
    QWidget* concreteWidget() override;
    void doReset() override;

public slots:
    void slotIconShortCut();
    void slotDefaultShortCut();
    void slotContextMenu(const QPoint&);
    void slotSelectItem(const QModelIndex&);
    void slotDoubleClickItem(const QModelIndex&);
    void slotEntered(const QModelIndex&);
    void reset() override;

signals:
    void currentFolderChanged(Folder*);
    void folderDoubleCliked(QString);
    void itemEntered(IconObject*);
    void iconCommandRequested(QString, IconObjectH);
    void desktopCommandRequested(QString, QPoint);

protected:
    void attachModel() override {}
    void detachModel() override {}

    void setAllowMoveAction(bool b) { allowMoveAction_ = b; }
    void setEnterFolders(bool b) { enterFolders_ = b; }

    void setupShortCut() override;
    void setPositions() {}
    void blink(const QModelIndex&) override;
    void showIcon(const QModelIndex&) override {}
    void rename(IconObject*) override;

    void folderChanged() override;
    void iconCommandFromMain(QString) override {}
    void iconCommand(QString, IconObjectH) override;
    void desktopCommand(QString, QPoint) override;

    bool event(QEvent* event) override;
    void leaveEvent(QEvent* event) override;
    void keyPressEvent(QKeyEvent*) override;
    void mousePressEvent(QMouseEvent*) override;
    void mouseMoveEvent(QMouseEvent*) override;
    void performDrag(Qt::DropAction, QPoint);
    void dragEnterEvent(QDragEnterEvent*) override;
    void dragMoveEvent(QDragMoveEvent*) override;
    void dropEvent(QDropEvent*) override;

    QRect itemRect(QList<IconObject*>) override;
    QRect itemRect(IconObject*) override;
    QRect pixmapRect(QList<IconObject*>) override;
    QRect pixmapRect(IconObject*) override;

    bool ignoreItemPositionForCm() override { return false; }

    MvQIconDelegate* delegate_;

private:
    bool allowMoveAction_;
    bool enterFolders_;
    QPoint startPos_;
    bool canDrag_;
    QShortcut* defaultShortCut_;
    IconObject* itemInfo_{nullptr};
};

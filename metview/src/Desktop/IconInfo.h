/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Request.h"

class IconInfo
{
public:
    IconInfo(const Request&);
    IconInfo(const std::string&);
    IconInfo(const std::string&, int, int);
    ~IconInfo();

    int x() const;
    int y() const;
    void position(int, int);
    void save(const Path&);
    bool changeIconClass(const std::string&);
    std::string type() { return type_; }

    static int undefX() { return undefX_; }
    static int undefY() { return undefY_; }
    static bool validPos(int x, int y);

private:
    // No copy allowed
    IconInfo(const IconInfo&);
    IconInfo& operator=(const IconInfo&);

    bool modified_{false};
    Request info_;
    std::string type_;
    int x_{0};
    int y_{0};
    static int undefX_;
    static int undefY_;
};

inline void destroy(IconInfo**) {}

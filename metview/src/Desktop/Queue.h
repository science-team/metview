/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "TaskObserver.h"
#include "Task.h"
#include "IconObject.h"
#include "Runnable.h"


class Queue : public TaskObserver, public Runnable
{
public:
    Queue(IconObject* owner);

    void push(Task*);
    void ownerGone();

private:
    ~Queue() override;

    // No copy allowed
    Queue(const Queue&);
    Queue& operator=(const Queue&);

    void done();
    void success(Task*, const Request&) override;
    void failure(Task*, const Request&) override;
    void run() override;

    IconObjectH owner_;
    std::list<TaskH> tasks_;
    TaskH active_;
};

inline void destroy(Queue**) {}

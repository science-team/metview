/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQRequestPanelHelp.h"
#include "MvIconParameter.h"

#include <QPen>
#include <QWidget>
#include <QAbstractItemModel>
#include <QStyledItemDelegate>

class QComboBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QSlider;
class QSpinBox;
class QTabWidget;
class QToolButton;
class QTreeView;
class QSortFilterProxyModel;

class MvQEcLayerDbItem;
class RequestPanel;

class MvQEcLayerTreeWidget;

#if 0
class MvQEcLayerDelegate : public QStyledItemDelegate
{
public:
    MvQEcLayerDelegate(QWidget *parent=0);
    void paint(QPainter *painter,const QStyleOptionViewItem &option,
                   const QModelIndex& index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

protected:
    QColor borderCol_;
};
#endif


// Model to dislay/select the suites
class MvQEcLayerModel : public QAbstractItemModel
{
public:
    enum CustomItemRole
    {
        SortRole = Qt::UserRole + 1,
        FilterRole = Qt::UserRole + 2
    };

    explicit MvQEcLayerModel(QObject* parent = nullptr);
    ~MvQEcLayerModel() override;

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    void setFilter(QString s);
    void clearFilter();

protected:
    bool isFiltered(MvQEcLayerDbItem* item) const;

    QString filter_;
};

class MvQEcLayerSelectionWidget : public QWidget
{
    Q_OBJECT
public:
    MvQEcLayerSelectionWidget(QWidget* parent = nullptr);
    void setCurrent(const std::string&);

protected slots:
    void slotNameFilter(QString s);
    void slotItemSelected(const QModelIndex&, const QModelIndex&);
    void slotClearFilter();

signals:
    void itemSelected(int);

protected:
    bool ignoreFilterChanged_;
    QLineEdit* nameLe_;

    MvQEcLayerTreeWidget* browser_;
    MvQEcLayerModel* model_;
    QSortFilterProxyModel* sortModel_;
};

class MvQEcLayerHelp : public MvQRequestPanelHelp
{
    Q_OBJECT

public:
    MvQEcLayerHelp(RequestPanel& owner, const MvIconParameter& param);
    ~MvQEcLayerHelp() override = default;

    void start() override {}
    bool dialog() override { return false; }
    QWidget* widget() override { return selector_; }

public slots:
    void slotSelected(int);

protected:
    void refresh(const std::vector<std::string>&) override;

private:
    MvQEcLayerSelectionWidget* selector_;
    QString oriName_;
};

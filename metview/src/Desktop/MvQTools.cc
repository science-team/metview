/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQTools.h"

#include <QAction>

#include "MvQHelpBrowser.h"
#include "MvQProductBrowser.h"

MvQTools* MvQTools::instance_ = 0;

MvQTools::MvQTools() :
    QObject(0),
    helpBrowser_(0),
    productBrowser_(0)
{
}

MvQTools* MvQTools::instance()
{
    if (!instance_)
        instance_ = new MvQTools;

    return instance_;
}

void MvQTools::slotHelp()
{
    if (!helpBrowser_) {
        helpBrowser_ = new MvQHelpBrowser();
    }
    helpBrowser_->show();
}

void MvQTools::make(QList<QAction*>& lst, QObject* parent)
{
    QAction* ac;

    ac = new QAction(parent);
    ac->setText("Icon objects");
    lst << ac;

    connect(ac, SIGNAL(triggered()),
            this, SLOT(slotObject()));

    ac = new QAction(parent);
    ac->setText("Icon classes");
    lst << ac;

    connect(ac, SIGNAL(triggered()),
            this, SLOT(slotClass()));

    ac = new QAction(parent);
    ac->setText("Help");
    lst << ac;

    connect(ac, SIGNAL(triggered()),
            this, SLOT(slotHelp()));
}

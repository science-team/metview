/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

class Request;
class RequestPanel;
class MvIconParameter;

#include "IconObject.h"

class RequestPanelItem
{
public:
    RequestPanelItem(RequestPanel&, const MvIconParameter&);
    virtual ~RequestPanelItem();

    // virtual void getSizes(Dimension&,Dimension&);
    // virtual void setSizes(Dimension,Dimension);

    virtual void reset();
    virtual void update();
    virtual void apply();
    virtual void cleanup();
    virtual void set(Request&);

    virtual void grey();

    virtual long flags();
    virtual void refresh(const std::vector<std::string>&) = 0;
    virtual void changed(const char* /*param*/, const std::vector<std::string>&) {}
    virtual void changed(const char* /*param*/, const std::string&) {}

    void checkDefault();

    virtual void setVisibleByFilter(bool) = 0;
    virtual void setVisibleByTemporary(bool) = 0;
    virtual void adjustDisabled() = 0;
    virtual void gray(bool) = 0;
    virtual void mark(bool) = 0;
    virtual bool hasDefaultTb() = 0;
    virtual bool isDependent() const { return false; }

    virtual bool isGray() const = 0;
    virtual bool isVisibleByFilter() const = 0;

    virtual void edit(IconObject*);
    IconObjectH currentObject() { return currentObject_; }
    const MvIconParameter& parameter() { return param_; }

protected:
    RequestPanel& owner_;
    const MvIconParameter& param_;
    IconObjectH currentObject_;
    bool visibleByFilter_{true};

    void decache();

    bool visibleByTemporary_{true};

private:
    RequestPanelItem(const RequestPanelItem&);
    RequestPanelItem& operator=(const RequestPanelItem&);

    std::vector<std::string> cache_;
};

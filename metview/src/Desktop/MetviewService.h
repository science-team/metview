/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Service.h"

class Task;
class IconObject;

class MetviewService : public Service
{
public:
    MetviewService(const std::string&);
    ~MetviewService() override;

private:
    // No copy allowed
    MetviewService(const MetviewService&);
    MetviewService& operator=(const MetviewService&);

    Task* task(const Action&, IconObject*) override;
};

inline void destroy(MetviewService**) {}

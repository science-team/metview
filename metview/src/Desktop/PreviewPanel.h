/***************************** LICENSE START ***********************************

 Copyright 2020 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QWidget>

class IconObject;

namespace Ui
{
class PreviewPanel;
}

class IconDescriptionPresenter
{
public:
    virtual void setContents(const std::string& /*path*/, QPixmap) {}
    virtual void setContents(const std::string& /*path*/, QString) {}
    virtual void setContents(const std::string& /*path*/, QPixmap, QString) {}
};


class PreviewPanel : public QWidget, public IconDescriptionPresenter
{
    Q_OBJECT
public:
    PreviewPanel(QWidget* parent = nullptr);
    ~PreviewPanel() override = default;

    void setItem(IconObject*);
    void setContents(const std::string& path, QPixmap) override;
    void setContents(const std::string& path, QString) override;
    void setContents(const std::string& path, QPixmap pix, QString txt) override;
    void clear();

signals:
    void closePanel();

protected:
    void paintEvent(QPaintEvent*) override;

    Ui::PreviewPanel* ui_;
    bool cleared_{true};
    std::string currentPath_;
    QString cssStyle_;
};

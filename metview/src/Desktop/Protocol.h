/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

class Protocol
{
public:
    Protocol();
    virtual ~Protocol();

    static void init();

private:
    // No copy allowed
    Protocol(const Protocol&);
    Protocol& operator=(const Protocol&);

    virtual void make();
};

inline void destroy(Protocol**) {}

template <class T>
class ProtocolFactory : public Protocol
{
    void make() override { new T(); }

public:
    ProtocolFactory() = default;
};

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(Protocol);
//#endif

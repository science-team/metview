/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Editor.h"

class NoEditor : public Editor
{
public:
    NoEditor(const IconClass&, const std::string&);
    ~NoEditor() override;

    void changed() override {}
    void raiseIt() override {}
    void closeIt() override {}

private:
    NoEditor(const NoEditor&);
    NoEditor& operator=(const NoEditor&);

    void edit() override {}
    void showIt() override {}
};

inline void destroy(NoEditor**) {}

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQExternalHelp.h"

#include "HelpFactory.h"
#include "MvIconParameter.h"
#include "RequestPanel.h"

MvQExternalHelp::MvQExternalHelp(RequestPanel& owner, const MvIconParameter& param) :
    MvQRequestPanelHelp(owner, param)
{
}

void MvQExternalHelp::start()
{
    attach();  // Will be decremented by ReplyHandler, so make sure it stays alive.

    Request info = param_.interfaceRequest();

    Request request((const char*)info("help_verb"));
    request.merge(owner_.request());
    callService((const char*)info("help_module"), request);
}

void MvQExternalHelp::refresh(const std::vector<std::string>& /*values*/)
{
}

void MvQExternalHelp::set(Request& r)
{
    r.merge(request_);
    request_ = Request();
}

void MvQExternalHelp::reply(const Request& reply, int error)
{
    // cout << "ExternalHelp::reply" << std::endl;
    if (error == 0)
        reply.print();
}

void MvQExternalHelp::progress(const Request& progress)
{
    // cout << "ExternalHelp::progress" << std::endl;
    progress.print();
    request_ = progress;
    // owner_.changed(*this);
}

void MvQExternalHelp::message(const std::string& /*msg*/)
{
    // cout << "MvQExternalHelp::message" << msg << std::endl;
}

/*void MvQExternalHelp::message(const Request& msg)
{
      std::string s = msg.getVerb();
      if (s != "INPUT") return;

          request_ = owner_.request();
      request_.merge(msg);

     int cnt = msg.countValues(param_.name());
     std::vector<std::string> res;
     for(int i=0; i<cnt; ++i )
     {
        const char *ch;
        msg.getValue(ch,param_.name(),i);
        res.push_back(string(ch));
     }
     owner_.set(param_.name(),res);

          //owner_.changed(*this);
}*/

// static HelpMaker<MvQExternalHelp> maker("help_external");

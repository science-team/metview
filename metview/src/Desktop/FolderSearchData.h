/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <vector>
#include <string>

class IconObject;

class FolderSearchDataObserver
{
public:
    FolderSearchDataObserver() = default;
    virtual ~FolderSearchDataObserver() = default;
    virtual void matchChanged() = 0;
};


class FolderSearchData
{
public:
    FolderSearchData() = default;

    void setObserver(FolderSearchDataObserver*);
    void setSearchTerm(const std::string&, const std::string&);
    void clear();
    bool isSet();
    bool hasMatch();
    void clearMatch();
    bool match(IconObject*);
    void load(IconObject*);
    void matchChanged();
    void setOwner(void* p) { owner_ = p; }
    void* owner() const { return owner_; }
    void resetCurrent();
    IconObject* first();
    IconObject* prev();
    IconObject* next();

private:
    FolderSearchDataObserver* observer_{nullptr};
    void* owner_{nullptr};
    std::string name_;
    std::string type_;
    std::vector<IconObject*> matchVec_;
    int current_{0};
};

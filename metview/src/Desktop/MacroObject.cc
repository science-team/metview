/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MacroObject.h"

#include <stdlib.h>

#include "IconFactory.h"
#include "Request.h"
#include "MvLog.h"
#include "Path.h"

MacroObject::MacroObject(Folder* parent, const IconClass& kind,
                         const std::string& name, IconInfo* info) :
    FileObject(parent, kind, name, info)
{
}

MacroObject::~MacroObject() = default;

void MacroObject::createFiles()
{
    IconObject::createFiles();
    Path p = path();
    if (!p.exists())
        p.saveText("#Metview Macro");
}

std::set<std::string> MacroObject::can()
{
    auto res = IconObject::can();
    res.insert("convert_python");
    return res;
}

static IconMaker<MacroObject> maker("Macro");

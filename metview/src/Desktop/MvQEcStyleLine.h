/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQRequestPanelLine.h"
#include "MvIconParameter.h"

#include <QPixmap>
#include <QSet>
#include <QString>

#include <string>

class QColor;
class QComboBox;

class RequestPanel;

class MvQEcStyleLine : public MvQRequestPanelLine, public ParameterScanner
{
    Q_OBJECT

public:
    MvQEcStyleLine(RequestPanel& owner, const MvIconParameter& param);
    ~MvQEcStyleLine() override = default;

    void refresh(const std::vector<std::string>&) override;
    void widgetClicked();
    void changed(const char* /*param*/, const std::vector<std::string>&) override {}
    void changed(const char* param, const std::string&) override;
    bool isDependent() const override { return true; }

public slots:
    void slotHelperEdited(const std::vector<std::string>&) override;

protected:
    void next(const MvIconParameter&, const char* first, const char* second) override;
    void updateHelper();
    std::string layerName() const;

    QPixmap pix_;
    QSize pixSize_;
    QLabel* stylePixLabel_;
    QLabel* styleNameLabel_;
    std::string layerParam_;
};

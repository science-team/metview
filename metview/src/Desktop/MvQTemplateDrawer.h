/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QObject>
#include "EditorDrawer.h"
#include "MvQIconStripView.h"

class MvQContextItemSet;
class MvQTemplateDrawerView;

class MvQTemplateDrawer : public QObject, public EditorDrawer
{
    Q_OBJECT

public:
    MvQTemplateDrawer(MvQEditor*);

    QString name() override;
    QWidget* widget() override;

public slots:
    void slotCommand(QString, IconObjectH);

protected:
    MvQFolderModel* model_;
    MvQTemplateDrawerView* view_;
};


class MvQTemplateDrawerView : public MvQIconStripView
{
public:
    MvQTemplateDrawerView(MvQFolderModel*, QWidget* parent = nullptr);
    ~MvQTemplateDrawerView() override;

protected:
    MvQContextItemSet* cmSet() override;
};

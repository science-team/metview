/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Counted.h"

class Request;

class MessageObserver : public virtual Counted
{
public:
    MessageObserver();
    ~MessageObserver() override;

    virtual void message(const Request&) = 0;
    void observe();
    void stop();

    static void broadcast(const Request&);

private:
    // No copy allowed
    MessageObserver(const MessageObserver&);
    MessageObserver& operator=(const MessageObserver&);
};


class MessageObserverH : public Handle<MessageObserver>
{
public:
    MessageObserverH(MessageObserver* o) :
        Handle<MessageObserver>(o) {}
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQEditor.h"

#include "RequestPanel.h"
#include "MvIconLanguage.h"

class QScrollArea;

class RequestPanel;
class SimpleEditorObserver;

class SimpleEditor : public MvQEditor
{
    Q_OBJECT
public:
    SimpleEditor(const IconClass&, const std::string&);
    ~SimpleEditor() override;

    // Request currentRequest(long custom_expand=0);

    void replace(IconObjectH) override;
    void merge(IconObjectH) override;

public slots:
    void slotIconDropped(IconObject*);

protected slots:
    void slotFilterItems(QString) override;
    void slotShowDisabled(bool) override;

protected:
    int filterNonMatchCount() override;
    void applyTemporayRules() override;
    void readSettings(QSettings&) override {}
    void writeSettings(QSettings&) override {}

private:
    SimpleEditor(const SimpleEditor&);
    SimpleEditor& operator=(const SimpleEditor&);

    void apply() override;
    void reset() override;
    void close() override;
    void edit() override;

    virtual IconObject* copy(const std::string&);

    RequestPanel* panel_;
    QScrollArea* panelArea_;
};

inline void destroy(SimpleEditor**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(SimpleEditor);
//#endif

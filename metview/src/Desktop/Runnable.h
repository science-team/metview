/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QObject>

class Runnable : public QObject
{
    Q_OBJECT

public:
    Runnable();
    ~Runnable() override;  // Change to virtual if base class

    void enable();
    void disable();
    bool actived() { return actived_; }

    virtual void run() = 0;

public slots:
    void slotWork();

private:
    Runnable(const Runnable&);
    Runnable& operator=(const Runnable&);

    bool actived_;

    // QT slot???
    // static Boolean workCB(XtPointer);
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "IconClass.h"
#include "MvIconLanguage.h"
#include "MvIconParameter.h"

class FamilyScanner : public ParameterScanner, public MvIconLanguageScanner
{
public:
    const std::string& keyword() const;
    bool validate(Request&) const;
    const std::vector<std::string>& params() const;
    const std::vector<std::string>& values(const std::string&);  // const;
    const std::vector<std::string>& beau(const std::string&);    // const;

    static FamilyScanner& find(const IconClass&);

protected:
    FamilyScanner(const IconClass&);
    ~FamilyScanner() override;

private:
    FamilyScanner(const FamilyScanner&);
    FamilyScanner& operator=(const FamilyScanner&);

    std::string keyword_;
    std::map<std::string, std::string> names_;
    const IconClass& class_;
    std::vector<std::string> params_;
    std::map<std::string, std::vector<std::string> > values_;
    std::map<std::string, std::vector<std::string> > beau_;

    // From LanguageScanner
    void next(const MvIconParameter&) override;

    // From ParameterScanner
    void next(const MvIconParameter&, const char*, const char*) override;
};

inline void destroy(FamilyScanner**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(FamilyScanner);
//#endif

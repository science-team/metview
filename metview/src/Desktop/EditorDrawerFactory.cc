/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "EditorDrawerFactory.h"
#include "EditorDrawer.h"

using Map = std::multimap<int, EditorDrawerFactory*>;

static Map* makers = nullptr;

EditorDrawerFactory::EditorDrawerFactory(int n)
{
    if (makers == nullptr)
        makers = new Map();

    makers->insert(Map::value_type(n, this));
}

EditorDrawerFactory::~EditorDrawerFactory() = default;

std::map<std::string, EditorDrawer*> EditorDrawerFactory::create(MvQEditor* e)
{
    std::map<std::string, EditorDrawer*> m;
    for (auto& maker : *makers) {
        EditorDrawer* d = maker.second->make(e);
        if (d != nullptr)
            m[d->name().toStdString()] = d;
    }
    return m;
}

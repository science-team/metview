/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQScriptHelp.h"

#include "Request.h"
#include "MvIconParameter.h"
#include "HelpFactory.h"
#include "RequestPanel.h"

#include <QDebug>
#include <QDialogButtonBox>
#include <QLabel>
#include <QTextEdit>

//=================================================
//
// ScriptHelpTask
//
//=================================================

ScriptHelpTask::ScriptHelpTask(const std::string& n, IconObject* o) :
    ShellTask(n, o) {}

void ScriptHelpTask::ready(const char* line)
{
    emit textArrived(QString(line));
}

void ScriptHelpTask::done(FILE* fp)
{
    emit finished();
    ShellTask::done(fp);
}

//=================================================
//
// MvQMessageDialog
//
//=================================================

MvQMessageDialog::MvQMessageDialog(QWidget* parent) :
    QDialog(parent)
{
    auto* vb = new QVBoxLayout(this);

    header_ = new QLabel(this);
    vb->addWidget(header_);

    te_ = new QTextEdit(this);
    te_->setReadOnly(true);
    vb->addWidget(te_);

    auto* bb = new QDialogButtonBox(this);
    bb->addButton(QDialogButtonBox::Close);
    vb->addWidget(bb);

    connect(bb, SIGNAL(rejected()),
            this, SLOT(reject()));
}

void MvQMessageDialog::setHeader(QString txt)
{
    header_->setText(txt);
}

void MvQMessageDialog::append(QString txt)
{
    te_->insertHtml(txt);
}

void MvQMessageDialog::clear()
{
    te_->clear();
}

//=================================================
//
// MvQScriptHelp
//
//=================================================

MvQScriptHelp::MvQScriptHelp(RequestPanel& owner, const MvIconParameter& def) :
    MvQRequestPanelHelp(owner, def),
    task_(nullptr),
    mb_(nullptr)
{
}

void MvQScriptHelp::start()
{
    if (!task_)
        task_ = new ScriptHelpTask(static_cast<const char*>(param_.interfaceRequest()("help_script_command")), nullptr);

    if (!mb_) {
        mb_ = new MvQMessageDialog(owner_.parentWidget());
        mb_->setModal(false);

        connect(task_, SIGNAL(textArrived(QString)),
                mb_, SLOT(append(QString)));

        connect(task_, SIGNAL(finished()),
                this, SLOT(slotFinished()));

        connect(mb_, SIGNAL(rejected()),
                this, SLOT(slotClosed()));
    }

    mb_->clear();
    mb_->setHeader(tr("Running external command ..."));

    task_->attach();
    task_->Object(owner_.currentObject());
    task_->start();

    mb_->show();
}

void MvQScriptHelp::slotFinished()
{
    if (mb_) {
        if (1)
            mb_->setHeader(tr("External command finished."));
        else
            mb_->setHeader(tr("External command produced no output. Check message window for errors"));
    }
}

void MvQScriptHelp::slotClosed()
{
    // the dialog will be automatically deleted
    mb_ = nullptr;

    delete task_;
    task_ = nullptr;
}

QIcon MvQScriptHelp::dialogIcon()
{
    return QIcon(QPixmap(":/desktop/help_script.svg"));
}

static HelpMaker<MvQScriptHelp> maker("help_script");

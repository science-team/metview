/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QApplication>
#include <QPixmap>
#include <QSplashScreen>

#include "MvQDesktop.h"

int main(int argc, char** argv)
{
    auto* app = new MvQDesktop(argc, argv, "Desktop");

    // Initialise resorces from a static library (libMvQtGui)
    // Q_INIT_RESOURCE(edit);

    QPixmap pixmap(":/desktop/splash_screen.png");
    QSplashScreen splash(pixmap);
    splash.showMessage("Loading resources ...", Qt::AlignBottom | Qt::AlignLeft);
    splash.show();

    app->processEvents();
    app->init();

    splash.close();

    app->showBrowsers();

    return app->exec();
}

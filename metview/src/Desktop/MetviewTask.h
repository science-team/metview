/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Task.h"
#include "Action.h"
#include "ReplyObserver.h"

#include "TaskObserver.h"
#include "IconObject.h"
#include "Dependancy.h"
#include "Request.h"

class MetviewTask : public Task, public ReplyObserver, public TaskObserver
{
public:
    MetviewTask(const std::string&, const Action&, IconObject*);
    ~MetviewTask() override;

    void addContext(const Request& req) override { context_ = req; }

protected:
    void print(std::ostream&) const override;

private:
    // No copy allowed
    MetviewTask(const MetviewTask&);
    MetviewTask& operator=(const MetviewTask&);

    void check();

    std::string service_;
    Action action_;
    IconObjectH object_;
    bool error_;
    int waiting_;
    Request context_;

    using Map = std::map<TaskH, DependancyH>;
    Map tasks_;

    // -- Overridden methods

    // From Task
    void start() override;

    // From ReplyObserver
    void reply(const Request&, int) override;
    void progress(const Request&) override;
    void message(const std::string&) override;

    // From TaskObserver
    void success(Task*, const Request&) override;
    void failure(Task*, const Request&) override;
};

inline void destroy(MetviewTask**) {}

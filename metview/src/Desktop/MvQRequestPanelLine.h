/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "RequestPanelItem.h"

#include <QObject>
#include <QString>
#include <QVector>

class QLabel;
class QToolButton;

class MvQRequestPanelHelp;

class MvIconParameter;
class RequestPanel;

class MvQRequestPanelLine : public QObject, public RequestPanelItem
{
    Q_OBJECT

public:
    enum ColumnPos
    {
        DefaultColumn = 0,
        NameColumn = 1,
        ExpandColumn = 2,
        WarningColumn = 3,
        WidgetColumn = 4
    };

    MvQRequestPanelLine(RequestPanel&, const MvIconParameter&, bool buildLine = true);
    ~MvQRequestPanelLine() override;

    static MvQRequestPanelLine* build(RequestPanel&, const MvIconParameter&);

    void adjustDisabled() override;
    void setVisibleByFilter(bool) override;
    void setVisibleByTemporary(bool) override;
    void gray(bool) override;
    void mark(bool) override;
    bool hasDefaultTb() override;
    bool isGray() const override { return gray_; }
    bool isVisibleByFilter() const override { return visibleByFilter_; }
    void setWarningLabelStatus(bool st, QString msg={});
    bool warningLabelStatus() const {return warningLabelStatus_;}

public slots:
    void slotChangeToDefault(bool);
    virtual void slotHelperEdited(const std::vector<std::string>&) {}
    virtual void slotHelperEdited(const std::vector<std::string>&, const std::vector<std::string>&) {}
    virtual void slotHelperEdited(QStringList, QVector<bool>) {}
    void slotStarHelpDialog(bool);
    virtual void slotHelperOpened(bool) {}
    virtual void slotHelperEditConfirmed() {}

protected slots:
    void slotHelpTbToggled(bool b);

protected:
    virtual void buildHelper();
    // void setEnabled(bool);
    void setLineVisible(bool, bool forced);
    void setLineEnabled(bool);
    void adjustState(bool forced = false);

    QToolButton* defaultTb_;
    QLabel* nameLabel_;
    QToolButton* helpTb_;
    QLabel* warningLabel_{nullptr};
    bool warningLabelStatus_{false};
    QString name_;
    QString source_;
    int row_;

    bool lineVisible_{true};
    bool gray_{false};
    bool mark_{false};  // indicates that a non-default value is set
    bool visibleByFilter_{true};

    MvQRequestPanelHelp* helper_;
    QWidget* parentWidget_;
};

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "IconInfo.h"
#include "Path.h"

int IconInfo::undefX_ = -100;
int IconInfo::undefY_ = -100;

IconInfo::IconInfo(const Request& r) :
    info_(r)
{
    if (!info_)
        info_ = Request("USER_INTERFACE");
    const char* p = info_("ICON_CLASS");
    if (p) {
        type_ = std::string(info_("ICON_CLASS"));
    }

    const char* x = info_("X");
    if (!x)
        info_("X") = undefX_;

    const char* y = info_("Y");
    if (!y)
        info_("Y") = undefY_;
}

IconInfo::IconInfo(const std::string& type) :
    type_(type)
{
    info_ = Request("USER_INTERFACE");
    info_("ICON_CLASS") = type.c_str();

    info_("X") = undefX_;
    info_("Y") = undefY_;
}


IconInfo::IconInfo(const std::string& type, int x, int y) :
    type_(type),
    x_(x),
    y_(y)
{
    info_ = Request("USER_INTERFACE");
    info_("ICON_CLASS") = type.c_str();
    info_("X") = x;
    info_("Y") = y;
}

IconInfo::~IconInfo() = default;

int IconInfo::x() const
{
    int x = undefX_;
    info_.getValue(x, "X");
    return x;
}


int IconInfo::y() const
{
    int y = undefY_;
    info_.getValue(y, "Y");
    return y;
}

void IconInfo::position(int x, int y)
{
    info_("X") = x;
    info_("Y") = y;
}

bool IconInfo::validPos(int x, int y)
{
    if (x <= undefX_ + 2 && y <= undefY_ + 2)
        return false;
    return true;
}

void IconInfo::save(const Path& s)
{
    info_.save(s);
    // cout << "Save into " << s.str() << std::endl;
    // info_.print();
}

bool IconInfo::changeIconClass(const std::string& kind)
{
    if (type_ != kind) {
        type_ = kind;
        info_("ICON_CLASS") = kind.c_str();
        return true;
    }
    return false;
}

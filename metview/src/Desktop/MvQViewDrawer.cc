/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQViewDrawer.h"

#include "EditorDrawerFactory.h"
#include "IconObject.h"
#include "IconClass.h"
#include "Folder.h"
#include "ViewEditor.h"

#include "MvQEditor.h"
#include "MvQContextMenu.h"
#include "MvQFolderModel.h"

MvQViewDrawer::MvQViewDrawer(MvQEditor* editor) :
    EditorDrawer(editor)
{
    folder_ = Folder::folder("templates",
                             editor_->iconClass().defaultName() + "/Views", true);

    IconClass::scan(*this);

    model_ = new MvQFolderModel(this);

    view_ = new MvQViewDrawerView(model_);
    // view_->setHelper(true);
    view_->setMinimumHeight(1);
    view_->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);

    model_->setAcceptedClasses(classes_);

    model_->setFolder(folder_);

    connect(view_, SIGNAL(iconCommandRequested(QString, IconObjectH)),
            this, SLOT(slotCommand(QString, IconObjectH)));
}

void MvQViewDrawer::next(const IconClass& c)
{
    if (c.type() == "View") {
        classes_.push_back(c.name());
    }
}

QString MvQViewDrawer::name()
{
    return tr("Views");
}

QWidget* MvQViewDrawer::widget()
{
    return view_;
}

void MvQViewDrawer::slotCommand(QString /*name*/, IconObjectH /*obj*/)
{
}


MvQViewDrawerView::MvQViewDrawerView(MvQFolderModel* folderModel, QWidget* parent) :
    MvQIconStripView(folderModel, parent)
{
}

MvQViewDrawerView::~MvQViewDrawerView()
{
    // delete filterModel_;
}

MvQContextItemSet* MvQViewDrawerView::cmSet()
{
    static MvQContextItemSet cmItems("ViewDrawerView");
    return &cmItems;
}

static EditorDrawerMaker<MvQViewDrawer, ViewEditor> maker(99);
/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <cassert>

#include "Metview.h"
#include "EditTransaction.h"
#include "IconObject.h"
#include "IconClass.h"
#include "Request.h"
#include "Task.h"
#include "Folder.h"
#include "IconFactory.h"
#include "EditorObserver.h"
#include "Editor.h"

#include "Protocol.h"
#include "MacroGuiObject.h"
#include "PythonGuiObject.h"
#include "Action.h"
#include "MvLog.h"

EditTransaction::EditTransaction() :
    Transaction("EDIT")
{
}

EditTransaction::EditTransaction(MvTransaction* t) :
    Transaction(t)
{
}

MvTransaction* EditTransaction::cloneSelf()
{
    return new EditTransaction(this);
}

void EditTransaction::apply(IconObject*)
{
    Task* task = current_->action(Action("prepare"));
    task->add(this);
}

void EditTransaction::close(IconObject*)
{
    setError(1, "Edit canceled");
    sendReply(MvRequest());
}

void EditTransaction::success(Task*, const Request& r)
{
    sendReply(r);
}

void EditTransaction::failure(Task*, const Request& r)
{
    setError(1, "Edit canceled");
    sendReply(MvRequest(r));
}

void EditTransaction::callback(MvRequest& r)
{
    current_ = nullptr;

    std::cout << "CALLBACK" << std::endl;
    r.print();
    std::cout << "----------" << std::endl;

    MvRequest def = r("DEFINITION");

    if (def) {
        const char* name = def("_NAME");
        if (name) {
            if ((const char*)def("_DEFAULT")) {
                const IconClass& kind = IconClass::find(def.getVerb());
                current_ = kind.defaultObject();
            }
            else {
                current_ = IconObject::search(name);
            }
        }

        if (!current_) {
            if (const char* uplotDir = def("_UPLOT_TEMP_DIR")) {
                Folder* f = Folder::folder("temporary");
                assert(f);
                f->scan();
                if (IconObject* o = f->find(uplotDir)) {
                    assert(o->isFolder());
                    auto* uf = dynamic_cast<Folder*>(o);
                    assert(uf);
                    uf->scan();
                    current_ = IconObject::search(name);
                }
            }

            if (!current_) {
                MvLog().dbg() << MV_FN_INFO << "CREATE";
                current_ = IconFactory::create(Folder::folder("temporary"), def, 0, 0);
            }
        }
    }

    else {
        r.advance();

        //        std::cout << "ADVANCE" << std::endl;
        //        r.print();
        //        std::cout << "----------" << std::endl;

        if (r) {
            if (const char* ch = r("_APPL")) {
                if (strcmp(ch, "Python") == 0) {
                    subClass_ = new PythonGuiObject(r);
                }
            }
            if (!subClass_) {
                subClass_ = new MacroGuiObject(r);
            }
            Request t(subClass_->name().c_str());
            current_ = IconFactory::create(Folder::folder("temporary"), t, 0, 0);
        }
        else {
            close(current_);
            return;
        }
    }

    MvLog().dbg() << "OBJECT-REC --->";
    current_->request().print();
    MvLog().dbg() << "<-------";

    current_->edit();

    Editor* e = current_->editor();
    e->observer(this);
    e->temporary();
}

static ProtocolFactory<EditTransaction> edit;

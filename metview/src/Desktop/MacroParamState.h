/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Request.h"
#include "IconObject.h"

class MacroParamState : public Counted
{
public:
    MacroParamState(IconObject*, IconObject*, const Request&);
    ~MacroParamState() override;

    const IconClass& iconClass();
    IconObject* iconObject();

    IconObject* macro() const;
    void macro(IconObject*);

    Request request() const;
    void request(const Request&);

private:
    // No copy allowed
    MacroParamState(const MacroParamState&);
    MacroParamState& operator=(const MacroParamState&);

    Request requests() const;
    Request interface() const;

    IconObjectH owner_;
    IconObjectH temp_;
    IconObjectH macro_;
    Request request_;
    IconClass* class_;
};

class MacroParamStateH : public Handle<MacroParamState>
{
public:
    MacroParamStateH(MacroParamState* o = nullptr) :
        Handle<MacroParamState>(o) {}
};

/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QWidget>
#include <QToolButton>

#include <string>


class QAction;
class QHBoxLayout;
class QLabel;
class QMenu;
class QSignalMapper;

class MvQContextItem;
class MvQPathWidgetItem;

class Folder;

class MvQPathButton : public QToolButton
{
    Q_OBJECT

public:
    MvQPathButton(MvQPathWidgetItem* owner, QWidget* parent = nullptr);
    MvQPathWidgetItem* owner_;

signals:
    void iconDropped(QDropEvent*);

protected:
    void checkDropTarget(QDropEvent* event);
    void removeDropTarget();
    void dragEnterEvent(QDragEnterEvent* event) override;
    void dragMoveEvent(QDragMoveEvent* event) override;
    void dragLeaveEvent(QDragLeaveEvent* event) override;
    void dropEvent(QDropEvent*) override;
};


class MvQPathWidgetItem
{
public:
    MvQPathWidgetItem(QString n, QString fn) :
        name_(n),
        fullName_(fn),
        menuTb_(nullptr),
        nameTb_(nullptr) {}
    ~MvQPathWidgetItem()
    {
        if (menuTb_)
            menuTb_->deleteLater();
        if (nameTb_)
            nameTb_->deleteLater();
    }

    QString name_;
    QString fullName_;
    QToolButton* menuTb_;
    QToolButton* nameTb_;
};

class MvQPathWidget : public QWidget
{
    Q_OBJECT

public:
    MvQPathWidget(QWidget* parent = nullptr);
    ~MvQPathWidget() override = default;

    void setPath(QString);
    void setReloadAction(QAction*);

public slots:
    void slotShowDirMenu();
    void slotChangeDir(int);
    void slotContextMenu(const QPoint&);
    void slotBookmark();
    void slotIconDropped(QDropEvent* event);

signals:
    void pathClicked(int);
    void dirChanged(QString);
    void commandRequested(QString, QString);

protected:
    void setPath(Folder*, QString path);
    void clearLayout();
    QString getPath(QToolButton*);
    void paintEvent(QPaintEvent*) override;
    void updateBookmarkStatus();

    QHBoxLayout* layout_;
    QSignalMapper* smp_;
    QList<MvQPathWidgetItem*> items_;
    QAction* actionReload_;
    QToolButton* reloadTb_;
    QToolButton* bookmarkTb_;
    QString path_;
    QLabel* emptyLabelIcon_;
    QLabel* emptyLabel_;

    static QList<MvQContextItem*> cmTbItems_;
    static QList<MvQContextItem*> cmMenuItems_;
};

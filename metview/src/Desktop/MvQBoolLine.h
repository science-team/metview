/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QString>

#include "MvQRequestPanelLine.h"

class QHBoxLayout;
class QRadioButton;

class Request;
class RequestPanel;

class MvQBoolLine : public MvQRequestPanelLine
{
    Q_OBJECT

public:
    MvQBoolLine(RequestPanel& owner, const MvIconParameter& param);
    ~MvQBoolLine() override;

    QString currentValue() { return {}; }
    void addValue(QString) {}
    void refresh(const std::vector<std::string>&) override;

public slots:
    void slotStatusChanged(bool);

protected:
    QWidget* radioParent_;
    QHBoxLayout* layout_;
    QRadioButton* radioOn_;
    QRadioButton* radioOff_;
};

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "TemporaryEditor.h"

#include <QHBoxLayout>
#include <QPainter>
#include <QScrollArea>

#include "EditorFactory.h"
#include "IconObject.h"
//#include "Log.h"

#include "IconClass.h"
#include "IconFactory.h"
#include "RequestPanel.h"

#include "MvQRequestPanelWidget.h"

TemporaryEditor::TemporaryEditor(const IconClass& kind, const std::string& name) :
    MvQTemporaryEditor(kind, name)
{
    // Panel
    panelArea_ = new QScrollArea(this);
    panelArea_->setWidgetResizable(true);

    centralLayout_->addWidget(panelArea_);

    std::vector<std::string> classes;
    classes.push_back(kind.name());

    auto* w = new MvQRequestPanelWidget(classes, this);
    panel_ = new RequestPanel(kind, w, this, false);

    panelArea_->setWidget(w);
}

TemporaryEditor::~TemporaryEditor()
{
    delete panel_;
}


IconObject* TemporaryEditor::copy(const std::string& /*name*/)
{
    // IconObject* o = IconFactory::create(name, class_);
    // panel_.apply();
    // o->request(panel_.request());
    // return o;
    return nullptr;
}

void TemporaryEditor::edit()
{
    if (!current_)
        return;

    MvQTemporaryEditor::edit();
}


void TemporaryEditor::apply()
{
    panel_->apply();
    current_->setRequest(panel_->request());
}

void TemporaryEditor::reset()
{
    panel_->reset(current_);
}

void TemporaryEditor::close()
{
    panel_->close();
}

// Request TemporaryEditor::currentRequest(long custom_expand)
//{
// panel_.apply();
// return custom_expand ? panel_.request(custom_expand) : panel_.request();
// }

static EditorMaker<TemporaryEditor> editorMaker1("TemporaryEditor");

/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QStringList>
#include <QVector>
#include <QPixmap>

class QColor;
class QJsonArray;
class MvQEcLayerDb;

class MvQEcLayerDbItem
{
    friend class MvQEcLayerDb;

public:
    MvQEcLayerDbItem(QString name);

    QString name() const { return name_; }
    QString description() const { return description_; }
    QString title() const { return title_; }
    QString group() const { return group_; }
    QStringList keywords() const { return keywords_; }
    QString defaultStyle() const { return defaultStyle_; }
    QList<int> styleIds() const { return styleIds_; }

    QStringList steps() const { return steps_; }
    QStringList quantiles() const { return quantiles_; }

    bool keywordMatch(QString pattern, Qt::CaseSensitivity cs) const
    {
        foreach (QString t, keywords_)
            if (t.contains(pattern, cs))
                return true;

        return false;
    }

    QPixmap makePixmap(QSize pixSize);
    void paint(QPainter* painter, QRect rect);

private:
    void resolveStyles(QStringList lst);
    QString previewFile() const;

    QString name_;
    QString description_;
    QString title_;
    QString group_;
    QStringList keywords_;
    QString defaultStyle_;
    // QStringList styles_;
    QList<int> styleIds_;
    QStringList steps_;
    QStringList quantiles_;
};

class MvQEcLayerDb
{
public:
    static MvQEcLayerDb* instance();
    QVector<MvQEcLayerDbItem*> items() const { return items_; }
    QStringList names() const;
    QStringList titleWords() const;
    QStringList groups() const { return groups_; }
    QStringList keywords() const { return keywords_; }
    int maxNameLen() const { return maxNameLen_; }

    MvQEcLayerDbItem* find(const std::string& name) const;
    int indexOf(const std::string& name) const;

private:
    MvQEcLayerDb();
    void load();
    void collect(QStringList lst, QStringList& allLst) const;
    void toStringList(const QJsonArray& chAr, QStringList& lst) const;
    void toStringList(const QJsonArray& chAr, QStringList& lst, QStringList& allLst) const;

    static MvQEcLayerDb* instance_;
    QVector<MvQEcLayerDbItem*> items_;
    int maxNameLen_{0};

    QStringList groups_;
    QStringList keywords_;
};

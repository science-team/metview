/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <unistd.h>

#include "TemporaryFolder.h"

#include "IconFactory.h"

TemporaryFolder::TemporaryFolder(Folder* parent, const IconClass& kind,
                                 const std::string& name, IconInfo* info) :
    SystemFolder(parent, kind, name, info)
{
    // On creation, move remove old
    removeFiles();
    createFiles();
}

TemporaryFolder::~TemporaryFolder() = default;

void TemporaryFolder::createFiles()
{
    IconObject::createFiles();
    Path tmp(getenv("METVIEW_TMPDIR"));
    path().symlink(tmp);
}

static IconMaker<TemporaryFolder> maker("TEMPORARY");

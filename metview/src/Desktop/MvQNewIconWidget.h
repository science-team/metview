/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QDialog>
#include <QListWidget>
#include <QMap>
#include <QSettings>
#include <QStyledItemDelegate>

class QAbstractButton;

#include "IconClass.h"

#include "MvQRecentIcons.h"

class QButtonGroup;
class QComboBox;
class QLineEdit;
class QListWidget;
class QMouseEvent;
class QStackedWidget;

class MvQNewIconListWidget : public QListWidget
{
    Q_OBJECT

public:
    MvQNewIconListWidget(QWidget* parent = nullptr);
signals:
    void iconCreated(QString);

protected:
    void mousePressEvent(QMouseEvent*) override;
    void mouseMoveEvent(QMouseEvent*) override;

    QPoint startPos_;
};

class MvQNewIconPanel : public QWidget, public ClassScanner, public RecentIconsObserver
{
    Q_OBJECT

public:
    enum SelectionMode
    {
        PanelMode,
        DialogMode
    };

    MvQNewIconPanel(SelectionMode, QWidget* parent = nullptr);
    ~MvQNewIconPanel() override;

    void next(const IconClass&) override;
    void setCurrentMode(int);
    int currentMode() const;
    QString currentSelection() const;

    // Observer method
    void notifyRecentChanged() override;

    void writeSettings(QSettings&);
    void readSettings(QSettings&);

public slots:
    void slotTypeChanged(int);
    void slotIconSelected(QListWidgetItem*);
    void slotSearch(QString);
    void slotChangeMode(QAbstractButton*);
    void slotAddToRecentIcons(QString);

signals:
    void iconSelected(QString);
    void closePanel(bool b = true);

protected:
    void keyPressEvent(QKeyEvent* event) override;
    void updateRecentList();
    void paintEvent(QPaintEvent*) override;

    QStackedWidget* mainPanel_;
    MvQNewIconListWidget* recentList_;
    QComboBox* typeCb_;
    QStackedWidget* iconPanel_;
    QMap<QString, MvQNewIconListWidget*> iconLists_;
    QLineEdit* searchLine_;
    MvQNewIconListWidget* searchList_;
    std::vector<const IconClass*> iconClasses_;
    QButtonGroup* modeBg_;
    SelectionMode selectionMode_;
};

class MvQNewIconDialog : public QDialog
{
    Q_OBJECT

public:
    MvQNewIconDialog(QWidget* parent = nullptr);
    ~MvQNewIconDialog() override;
    const IconClass& selected();

public slots:
    void accept() override;
    void slotIconSelected(QString);

private:
    void writeSettings();
    void readSettings();

    MvQNewIconPanel* iconWidget_;
    QString selected_;
};

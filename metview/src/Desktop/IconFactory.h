/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

class IconObject;
class IconClass;
class IconInfo;
class Folder;
class Request;

class IconFactory
{
public:
    IconFactory(const std::string& name);
    virtual ~IconFactory();

    virtual IconObject* make(Folder* parent, const IconClass& kind,
                             const std::string& name, IconInfo* info) = 0;

    static IconObject* create(Folder* parent, const std::string& name, const IconClass&, IconInfo*);
    static IconObject* createTemporary(IconObject*, const Request&, const IconClass* = nullptr);
    static IconObject* create(Folder* parent, const std::string& name, const IconClass&);
    static IconObject* create(Folder* parent, const std::string& name, const IconClass& kind, int x, int y);
    static IconObject* create(Folder* parent, const std::string& name, int x, int y);
    static IconObject* create(Folder* parent, const std::string& name);
    static IconObject* create(const std::string& name, const IconClass&);

    // We should never call this
    static IconObject* create(IconObject*, const Request&, const IconClass* = nullptr);

    static IconObject* create(Folder*, const Request&, int x, int y);

private:
    // No copy allowed
    IconFactory(const IconFactory&);
    IconFactory& operator=(const IconFactory&);
};

template <class T>
class IconMaker : public IconFactory
{
    IconObject* make(Folder* parent, const IconClass& kind,
                     const std::string& name, IconInfo* info) override
    {
        T* t = new T(parent, kind, name, info);
        return t;
        // return new T(parent, kind, name, info);
    }

public:
    IconMaker(const std::string& name) :
        IconFactory(name) {}
};

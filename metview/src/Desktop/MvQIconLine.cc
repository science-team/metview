/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQIconLine.h"

#include <QHBoxLayout>
#include <QDebug>
#include <QLabel>
#include <QPainter>
#include <QVBoxLayout>

#include "MvQContextMenu.h"
#include "MvQFolderModel.h"
#include "MvQIconHolder.h"
#include "MvQIconMimeData.h"
#include "MvQIconProvider.h"

#include "IconClass.h"
#include "IconFactory.h"
#include "IconObject.h"
#include "LineFactory.h"
#include "MvIconParameter.h"
#include "RequestPanel.h"

MvQIconLine::MvQIconLine(RequestPanel& owner, const MvIconParameter& param) :
    MvQRequestPanelLine(owner, param)
{
    Request interface = param_.interfaceRequest();

    model_ = new MvQIconHolderModel(interface.get("class"), parentWidget_);
    view_ = new MvQIconHolderView(model_, this, parentWidget_);

    const std::vector<std::string>& classes = interface.get("class");

    /*QString title(tr("Icons: "));
    const std::vector<std::string>& classes=interface.get("class");
    for(vector<std::string>::const_iterator it = classes.begin(); it != classes.end(); ++it)
    {
        if(it != classes.begin())
        {
            title+=", ";
        }
        title+=QString::fromStdString(IconClass::find(*it).defaultName());
    }*/

    // QLabel *titleLabel=new QLabel(title,parentWidget_);

    acceptLayout_ = buildAcceptLayout(classes);

    // QLabel *titleLabel=new QLabel(parentWidget_);
    // titleLabel->setPixmap(iconClassPixmap(classes));

    auto* layout = new QVBoxLayout(parentWidget_);
    layout->setSpacing(3);
    layout->setContentsMargins(1, 1, 1, 1);

    auto* w = new QWidget(parentWidget_);
    w->setLayout(layout);

    layout->addLayout(acceptLayout_);
    layout->addWidget(view_);

    owner_.addWidget(w, row_, WidgetColumn);

    connect(view_, SIGNAL(edited()),
            this, SLOT(slotHolderEdited()));
}

void MvQIconLine::refresh(const std::vector<std::string>& values)
{
    if (values.size() == 0) {
        cleanup();
    }
}

void MvQIconLine::reset()
{
    const Request& r = owner_.request();

    std::string parName(param_.name());
    std::vector<IconObjectH> sub = currentObject_->subObjects(parName, r);

    grey();
    // This will clear the unwanted iconobjects!!
    model_->setIcons(sub);

    mark(model_->rowCount() > 0);

    // changed_ = false;
}

void MvQIconLine::update()
{
    grey();
    mark(model_->rowCount() > 0);
}

void MvQIconLine::apply()
{
    owner_.changed(*this);
}

void MvQIconLine::set(Request& r)
{
    std::vector<IconObjectH> vals;
    model_->iconObjects(vals);
    currentObject_->setSubObjects(param_.name(), vals, r, true);
}

// Should be called when we close the editor
void MvQIconLine::cleanup()
{
    if (!currentObject_)
        return;

    // We read the saved request
    const Request& r = owner_.request();

    std::string parName(param_.name());
    std::vector<IconObjectH> sub = currentObject_->subObjects(parName, r);

    // This will clear the unwanted iconObjects!!
    model_->clearIcons(sub);
}

void MvQIconLine::slotHolderEdited()
{
    owner_.changed();
    mark(model_->rowCount() > 0);
}

QHBoxLayout* MvQIconLine::buildAcceptLayout(const std::vector<std::string>& classes)
{
    auto* hb = new QHBoxLayout();

    auto* lw = new QLabel(tr("<i>Accepted icons:</i>"), parentWidget_);
    hb->addWidget(lw);

    int pixSize = 16;
    for (const auto& classe : classes) {
        QPixmap pix = MvQIconProvider::pixmap(IconClass::find(classe), pixSize);
        lw = new QLabel(parentWidget_);
        lw->setPixmap(pix);
        lw->setToolTip(QString::fromStdString(IconClass::find(classe).defaultName()));
        hb->addSpacing(4);
        hb->addWidget(lw);

        // painter.drawPixmap(i*pixSize+i*pixGap,0,pix);
    }

    hb->addStretch(1);
    return hb;

    // return cpix;
}


static LineMaker<MvQIconLine> maker1("icon");

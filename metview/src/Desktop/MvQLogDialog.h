/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "IconObject.h"

#include <QDialog>

class QLabel;
class QPlainTextEdit;
class QToolButton;

class MvQFileWatcher;

class MvQLogDialog : public QDialog
{
    Q_OBJECT

public:
    ~MvQLogDialog() override;
    void close();
    static void open(IconObject*);

protected:
    MvQLogDialog();
    void closeEvent(QCloseEvent*) override;

public slots:
    void reject() override;
    void slotFreeze(bool);
    void slotClear(bool);
    void slotReady(const char*);
    void slotDone(FILE*);

private:
    // No copy allowed
    MvQLogDialog(const MvQLogDialog&);
    MvQLogDialog& operator=(const MvQLogDialog&);
    void readSettings();
    void writeSettings();

    void show(IconObject*);
    void load();
    QString formatText(QString);

    IconObjectH current_;
    FILE* file_;

    QLabel* headerLabel_;
    QPlainTextEdit* textEdit_;
    MvQFileWatcher* watcher_;
    QToolButton* freezeTb_;
};


inline void destroy(MvQLogDialog**) {}

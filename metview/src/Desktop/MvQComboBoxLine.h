/***************************** LICENSE START ***********************************

 Copyright 2015 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QComboBox>
#include <QString>

#include "MvQRequestPanelLine.h"
#include "MvIconParameter.h"

class Request;
class RequestPanel;

class MvQEditorComboBox : public QComboBox
{
public:
    MvQEditorComboBox(QWidget* parent = nullptr);

protected:
    void wheelEvent(QWheelEvent* event) override;
    void focusInEvent(QFocusEvent* event) override;
    void focusOutEvent(QFocusEvent* event) override;
};


class MvQComboBoxLine : public MvQRequestPanelLine, public ParameterScanner
{
    Q_OBJECT

public:
    MvQComboBoxLine(RequestPanel& owner, const MvIconParameter& param);

    QString currentValue() { return {}; }
    void addValue(QString) {}

    void refresh(const std::vector<std::string>&) override;

public slots:
    void slotCurrentChanged(int);

protected:
    void next(const MvIconParameter&, const char* first, const char* second) override;
    void fixedFontAndHeight(QFont& font, int& h) const;

    MvQEditorComboBox* cbEdit_;
    bool reportChange_;
};

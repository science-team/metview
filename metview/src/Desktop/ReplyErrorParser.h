/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <vector>

class MvRequest;
class IconObject;

class ReplyErrorParser
{
public:
    enum Status {OkStatus, ErroStatus, WarningStatus, NoStatus};
    ReplyErrorParser(const MvRequest& r) {parse(r);}
    const std::string& error() const {return err_;}
    const std::vector<IconObject*>& items() const {return items_;}
    const std::vector<std::string>& itemsError() const {return itemsErr_;}
    const std::string& detailed() const {return detailed_;}
    Status status() const {return status_;}

private:
    void parse(const MvRequest& r);

    Status status_{NoStatus};
    std::string err_;
    std::vector<IconObject*> items_;
    std::vector<std::string> itemsErr_;
    std::string detailed_;
};

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <vector>

#include "mars.h"

#include <QDebug>
#include <QFile>
#include <QModelIndexList>
#include <QPixmap>
#include <QPoint>
#include <QStringList>

#include "Desktop.h"

class QDrag;
class QModelIndex;
class QShortcut;
class QWidget;

class MvQContextItemSet;
class MvQFolderFilterModel;
class MvQFolderItemProperty;
class MvQFolderModel;
class MvQIconMimeData;

class IconClass;
class IconObject;
class IconObjectH;
class Folder;

class MvQFolderViewBase
{
public:
    enum IndexType
    {
        FsModelIndex,
        FilterModelIndex
    };

    MvQFolderViewBase(MvQFolderModel*, QWidget*);
    virtual ~MvQFolderViewBase();
    MvQFolderViewBase(const MvQFolderViewBase&) = delete;
    MvQFolderViewBase& operator=(const MvQFolderViewBase&) = delete;

    Folder* currentFolder();
    bool changeFolderToParent();
    bool changeFolder(QString, int iconSize = -1);
    bool changeFolder(Folder*, int iconSize = -1);
    bool isFolder(const QModelIndex&, IndexType indexType = FsModelIndex);
    QString fullName(const QModelIndex&, IndexType indexType = FsModelIndex);
    MvQFolderModel* folderModel() const { return folderModel_; }
    void showLastCreated();
    virtual void doReset() = 0;

    virtual void iconCommandFromMain(QString) = 0;
    int getIconSize();
    virtual void toGrid(Desktop::GridSortMode) {}
    void showIcon(IconObject*);

    virtual QWidget* concreteWidget() = 0;

    static void dropToFolder(Folder*, QDropEvent*);

protected:
    virtual void attachModel() = 0;
    virtual void detachModel() = 0;
    bool changeFolder(const QModelIndex&, IndexType indexType = FsModelIndex);
    virtual void folderChanged() = 0;
    virtual void blink(const QModelIndex&) = 0;
    virtual void showIcon(const QModelIndex&) = 0;
    virtual void iconSizeChanged() {}
    virtual void rename(IconObject*) = 0;

    void handleDoubleClick(const QModelIndex&);
    void handleContextMenu(const QModelIndex&, QModelIndexList, QPoint, QPoint, QWidget*);
    void handleIconShortCut(QShortcut*, QModelIndexList);
    void handleDesktopShortCut(QShortcut* sc, QPoint pos, QWidget* widget);
    virtual void iconCommand(QString, IconObjectH) = 0;
    virtual void desktopCommand(QString, QPoint) = 0;

    IconObject* itemInfo(const QModelIndex&, IndexType);
    QString formatFileSize(qint64);
    QString formatFilePermissions(const QFile::Permissions&);

    bool isAccepted(const IconClass&);
    bool isAccepted(IconObject*);

    void handleIconCommand(QModelIndexList indexLst, QString name);

    virtual MvQContextItemSet* cmSet() = 0;
    virtual void setupShortCut() = 0;

    virtual bool ignoreItemPositionForCm() = 0;

    // Drag and drop
    virtual QRect itemRect(IconObject*) = 0;
    virtual QRect itemRect(QList<IconObject*>) = 0;
    virtual QRect pixmapRect(IconObject*) = 0;
    virtual QRect pixmapRect(QList<IconObject*>) = 0;
    virtual void performDrop(Qt::DropAction, const MvQIconMimeData*, QPoint, bool) = 0;
    QPixmap dragPixmapWithCount(IconObject*, int cnt, QRect&);
    QDrag* buildDrag(IconObject* dragObj, QList<IconObject*> objLst, bool, QWidget*, QPoint dragPos = QPoint());

    // Clipboard
    void copyToClipboard(IconObject*, QList<IconObject*>);
    void cutToClipboard(IconObject*, QList<IconObject*>);
    void copyPathToClipboard(IconObject* obj, QList<IconObject*> lst);
    void fromClipboard(QPoint);
    MvQIconMimeData* buildClipboardData(IconObject* dragObj, QList<IconObject*> objLst, bool fromHelper, bool copy);

    MvQFolderFilterModel* filterModel_{nullptr};
    MvQFolderModel* folderModel_{nullptr};
    QWidget* view_{nullptr};
    MvQFolderItemProperty* itemProp_{nullptr};
    bool shortCutInit_{false};

private:
    void handleIconCommand(const std::vector<IconObject*>&, QString);
    void handleIconCommand(IconObject*, QString);
    void handleDesktopCommand(QString, QPoint, QWidget*);
    bool confirmDelete(int);
};

/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

#include "MvQRequestPanelHelp.h"
#include "MvIconParameter.h"
#include "ReplyObserver.h"
#include "MessageObserver.h"

#include <QIcon>
#include <QMap>
#include <QString>

class MvQGeoHelp : public MvQRequestPanelHelp,
                   public ReplyObserver,
                   public MessageObserver
{
public:
    MvQGeoHelp(RequestPanel& owner, const MvIconParameter& param);
    ~MvQGeoHelp() override = default;

    void start() override;
    bool dialog() override { return true; }
    QIcon dialogIcon() override;
    bool useLargeDialogIcon() override { return true; }
    QWidget* widget() override { return nullptr; }

protected:
    void refresh(const std::vector<std::string>&) override;
    virtual long flags() { return 9; }
    virtual void set(Request&);

    void reply(const Request&, int) override;
    void progress(const Request&) override;
    void message(const std::string&) override;
    void message(const Request&) override;

    Request request_;
};

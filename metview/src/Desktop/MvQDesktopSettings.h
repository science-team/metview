/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END ************/

#pragma once

#include <QSettings>

#include "Desktop.h"

class MvQDesktopSettings
{
public:
    MvQDesktopSettings();

    static Desktop::DragPolicy dragPolicy();
    static QList<bool> headerVisible_;
    static QList<int> headerIndex_;

    static void writeSettings(QSettings&);
    static void readSettings(QSettings&);
};

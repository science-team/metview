/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//#include "Log.h"
#include "Wastebasket.h"
#include "IconFactory.h"

Wastebasket::Wastebasket(Folder* parent, const IconClass& kind,
                         const std::string& name, IconInfo* info) :
    SystemFolder(parent, kind, name, info)
{
}

Wastebasket::~Wastebasket() = default;

bool Wastebasket::adopt(IconObject* obj)
{
    IconObject* x = find(obj->name());
    if (x == obj) {
        return false;
    }

    if (x != nullptr) {
        x->destroy();
        // Log::warning(this) << "Object " << o->name()
        //	<< " already in wastebasket" << std::endl;
    }

    return SystemFolder::adopt(obj);
}

void Wastebasket::empty()
{
    scan();
    KidMap kids = kids_;
    for (auto& kid : kids) {
        IconObject* o = kid.second;
        // Log::info(this) << "Remove " << *o << std::endl;
        o->destroy();
    }
}

std::set<std::string> Wastebasket::can()
{
    // scan();
    std::set<std::string> c = SystemFolder::can();
    c.insert("empty");
    // if(kids_.size()) c.insert("empty");
    return c;
}

static IconMaker<Wastebasket> wastebasketFactory("WASTEBASKET");

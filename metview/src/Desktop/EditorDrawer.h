/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QString>

class QWidget;
class MvQEditor;

class EditorDrawer
{
public:
    EditorDrawer(MvQEditor* editor) :
        editor_(editor) {}

    virtual QString name() = 0;
    virtual QWidget* widget() = 0;

protected:
    MvQEditor* editor_;
};

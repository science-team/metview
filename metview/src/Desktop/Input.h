/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QObject>

#include <cstdio>

class QSocketNotifier;

class Input : public QObject
{
    Q_OBJECT

public:
    Input();
    ~Input() override;

    void start(FILE*);
    void stop();

    virtual void ready(const char*) = 0;
    virtual void done(FILE*) = 0;

protected slots:
    void slotInput(int);

private:
    // No copy allowed
    Input(const Input&);
    Input& operator=(const Input&);

    FILE* file_;
    QSocketNotifier* notifier_;
};

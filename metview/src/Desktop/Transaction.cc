/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Metview.h"
#include "Transaction.h"
#include "TaskObserver.h"
#include "Action.h"
#include "IconObject.h"
#include "Request.h"
#include "Task.h"
#include "Folder.h"
#include "IconFactory.h"
#include "EditorObserver.h"
#include "Editor.h"

Transaction::Transaction(const char* t) :
    MvTransaction(t)
{
}
Transaction::Transaction(MvTransaction* t) :
    MvTransaction(t)
{
}

Transaction::~Transaction() = default;

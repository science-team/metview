/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQEcLayerLine.h"

#include <QComboBox>
#include <QDebug>
#include <QHBoxLayout>
#include <QLabel>
#include <QPainter>
#include <QLinearGradient>
#include <QToolButton>

#include "MvQEcLayerDb.h"
#include "MvQRequestPanelHelp.h"
#include "MvMiscellaneous.h"

#include "LineFactory.h"
#include "RequestPanel.h"

//==========================================
//
// MvQEcLayerLine
//
//==========================================

class MvQEcLayerLineWidget : public QWidget
{
public:
    MvQEcLayerLineWidget(MvQEcLayerLine* owner, QWidget* parent) :
        QWidget(parent),
        owner_(owner) {}

protected:
    void mousePressEvent(QMouseEvent*) override
    {
        owner_->widgetClicked();
    }

    MvQEcLayerLine* owner_;
};

MvQEcLayerLine::MvQEcLayerLine(RequestPanel& owner, const MvIconParameter& param) :
    MvQRequestPanelLine(owner, param)
{
    // MvQEcLayerDb::instance();

    auto* w = new MvQEcLayerLineWidget(this, parentWidget_);
    auto* hb = new QHBoxLayout(w);
    hb->setContentsMargins(0, 0, 0, 0);

    pixLabel_ = new QLabel(w);
    nameLabel_ = new QLabel(w);
    hb->addWidget(pixLabel_);
    hb->addWidget(nameLabel_, 1);

    QFont font;
    QFontMetrics fm(font);
    pixSize_ = QSize(180, fm.height() + 4);

    owner_.addWidget(w, row_, WidgetColumn);

#if 0
    connect(colCb_,SIGNAL(currentIndexChanged(int)),
        this,SLOT(slotCurrentChanged(int)));
#endif
}

void MvQEcLayerLine::widgetClicked()
{
    if (helpTb_) {
        if (!helpTb_->isChecked())
            helpTb_->toggle();
    }
}


void MvQEcLayerLine::next(const MvIconParameter&, const char* /*first*/, const char* /*second*/)
{
}

void MvQEcLayerLine::refresh(const std::vector<std::string>& values)
{
    if (values.size() > 0) {
        if (MvQEcLayerDbItem* item = MvQEcLayerDb::instance()->find(values[0])) {
            pixLabel_->setPixmap(item->makePixmap(pixSize_));
            nameLabel_->setText(item->name());
            updateHelper();
            return;
        }
    }

    pixLabel_->setPixmap(QPixmap());
    nameLabel_->clear();
    // changed_ = false;
}

void MvQEcLayerLine::slotHelperEdited(const std::vector<std::string>& values)
{
    if (values.size() > 0 && !values[0].empty()) {
        int idx = metview::fromString<int>(values[0]);
        if (idx >= 0 && idx < MvQEcLayerDb::instance()->items().count()) {
            MvQEcLayerDbItem* item = MvQEcLayerDb::instance()->items()[idx];
            Q_ASSERT(item);
            pixLabel_->setPixmap(item->makePixmap(pixSize_));
            nameLabel_->setText(item->name());
            owner_.set(param_.name(), item->name().toStdString());
        }

#if 0
        for(int i=0; i < colCb_->count(); i++)
        {
            if(colCb_->itemData(i).toString().toStdString() == values[0])
            {
                colCb_->setCurrentIndex(i);
                return;
            }
        }

        QString name=QString::fromStdString(values[0]);
        //This sets the current index as well
        setCustomColour(MvQPalette::magics(values[0]),name,name,true);
#endif

        // If the current index did not change the slotCurrentChanged() was
        // not called, so here we need to notify the owner about the change!!
        // if(colCb_->currentIndex() ==  currentPrev)
        //{
        //	owner_.set(param_.name(),name.toStdString());
        // }
    }
}


void MvQEcLayerLine::updateHelper()
{
    if (!helper_)
        return;

    std::vector<std::string> vals;
    vals.push_back(nameLabel_->text().toStdString());
    helper_->refresh(vals);
}

#if 0
void MvQEcLayerLine::slotHelperEditConfirmed()
{
    int index=colCb_->currentIndex();
    if(index != -1)
    {
        QString name=colCb_->itemData(index).toString();
        owner_.set(param_.name(),name.toStdString());
    }
}
#endif

static LineMaker<MvQEcLayerLine> maker1("eclayer");

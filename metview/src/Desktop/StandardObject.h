/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <set>
#include <string>

#include "IconObject.h"

class IconClass;

class StandardObject : public IconObject
{
public:
    StandardObject(Folder* parent,
                   const IconClass& kind, const std::string& name,
                   IconInfo* info);

    ~StandardObject() override;
    void duplicate() override;
    IconObject* clone(Folder*, bool update = false) override;
    IconObject* clone(Folder*, int x, int y, bool update = false) override;
    bool storedAsRequestFile() const override { return true; }

protected:
    Request request() const override;
    const std::set<DependancyH>& dependancies() override;
    void setDependancies(std::set<DependancyH>&, const Request&);
    Request fullRequest() const override;

private:
    StandardObject(const StandardObject&);
    StandardObject& operator=(const StandardObject&);

    void doubleClick() override;
    std::set<std::string> can() override;

    void createFiles() override;
    void setRequest(const Request&) override;
    bool rename(const std::string&) override;
};

inline void destroy(StandardObject**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(StandardObject);
//#endif

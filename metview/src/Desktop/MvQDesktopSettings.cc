/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END ************/

#include "MvQDesktopSettings.h"

#include <QStringList>

#include "MvApplication.h"

QList<bool> MvQDesktopSettings::headerVisible_;
QList<int> MvQDesktopSettings::headerIndex_;

MvQDesktopSettings::MvQDesktopSettings() = default;

Desktop::DragPolicy MvQDesktopSettings::dragPolicy()
{
    MvRequest r = MvApplication::getPreferences();

    if (const char* c = r("ICON_DRAG_POLICY")) {
        if (strcmp(c, "SHOW_ALL") == 0)
            return Desktop::DragShowAllIcons;
        else if (strcmp(c, "SHOW_DRAGGED") == 0)
            return Desktop::DragShowDraggedIcon;
    }

    return Desktop::DragShowDraggedIcon;
}

// Writing/reading QLists for QSettings did not work whatever we
// tried with Q_DECLARE_METATYPE and qRegisterMetaType
//(got error: QVariant::load: unable to load type 257).
// So instead we need to use QStringlist here.

void MvQDesktopSettings::writeSettings(QSettings& settings)
{
    settings.beginGroup("detailedView");

    QStringList lst;
    foreach (bool b, headerVisible_)
        lst << QString::number((b) ? 1 : 0);
    settings.setValue("headerVisible", lst);

    lst.clear();
    foreach (int i, headerIndex_)
        lst << QString::number(i);
    settings.setValue("headerIndex", lst);

    settings.endGroup();
}

void MvQDesktopSettings::readSettings(QSettings& settings)
{
    settings.beginGroup("detailedView");

    headerVisible_.clear();

    QStringList lst = settings.value("headerVisible").toStringList();
    foreach (QString v, lst) {
        bool b = (v == "1") ? true : false;
        headerVisible_ << b;
    }

    headerIndex_.clear();
    lst = settings.value("headerIndex").toStringList();
    foreach (QString v, lst)
        headerIndex_ << v.toInt();

    settings.endGroup();
}

static MvQDesktopSettings globalInfo;

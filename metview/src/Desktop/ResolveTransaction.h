/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once


#include "Transaction.h"
#include "TaskObserver.h"


class ResolveTransaction : public Transaction, public TaskObserver
{
public:
    ResolveTransaction();
    ~ResolveTransaction() override;

    static void init();

    // Uncomment for persistent, remove otherwise
    // static os_typespec* get_os_typespec();

private:
    // No copy allowed
    ResolveTransaction(const ResolveTransaction&);
    ResolveTransaction(MvTransaction*);
    ResolveTransaction& operator=(const ResolveTransaction&);

    MvTransaction* cloneSelf() override;
    void callback(MvRequest&) override;

    void success(Task*, const Request&) override;
    void failure(Task*, const Request&) override;
};

inline void destroy(ResolveTransaction**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(ResolveTransaction);
//#endif

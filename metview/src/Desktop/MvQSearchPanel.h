/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QWidget>

#include <string>

#include "FolderSearchData.h"

class Folder;
class QComboBox;
class QLineEdit;
class QListView;
class QPushButton;
class MvQLineEdit;
class IconObject;

class MvQFolderSearchPanel : public QWidget, public FolderSearchDataObserver
{
    Q_OBJECT

public:
    MvQFolderSearchPanel(QWidget* parent = nullptr);
    ~MvQFolderSearchPanel() override;
    void targetChanged();

    // Observer method
    void matchChanged() override;

public slots:
    void slotFind(QString);
    void slotFindNext();
    void slotFindPrev();
    void openPanel(bool);
    void closePanel(bool);

signals:
    void find(FolderSearchData*);
    void iconSelected(IconObject*);
    void panelClosed();

protected:
    void updateStatus();

    MvQLineEdit* nameLe_;
    MvQLineEdit* typeLe_;
    QPushButton* nextPb_;
    QPushButton* prevPb_;
    FolderSearchData* searchData_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QDialog>

#include "Editor.h"

class QAbstractButton;
class QDialogButtonBox;
class QHBoxLayout;
class QLabel;
class QPushButton;

class MvQTemporaryEditor : public QDialog, public Editor
{
    Q_OBJECT

public:
    MvQTemporaryEditor(const IconClass& name, const std::string& kind, QWidget* parent = nullptr);
    ~MvQTemporaryEditor() override;

    void changed() override;

public slots:
    void slotButtonClicked(QAbstractButton*);
    void accept() override;
    void reject() override;

protected:
    void raiseIt() override;
    void showIt() override { show(); }
    void closeIt() override;
    void updateHeader();
    virtual void apply() = 0;
    virtual void reset() = 0;
    virtual void close() = 0;
    void edit() override;

    QLabel* headerLabel_;
    QHBoxLayout* centralLayout_;
    QDialogButtonBox* buttonBox_;
    QPushButton* okPb_;
    QPushButton* cancelPb_;
    QPushButton* resetPb_;
};

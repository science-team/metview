/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Task.h"
#include "Action.h"
#include "ReplyObserver.h"
#include "TaskObserver.h"
#include "IconObject.h"
#include "Dependancy.h"

class InternalTask : public TaskObserver, public Task
{
public:
    InternalTask(const Action&, IconObject*);
    ~InternalTask() override;

    void addContext(const Request&) override {}

protected:
    void print(std::ostream&) const override;

private:
    // No copy allowed
    InternalTask(const InternalTask&);
    InternalTask& operator=(const InternalTask&);

    void check();
    void start() override;
    void success(Task*, const Request&) override;
    void failure(Task*, const Request&) override;

    Action action_;
    IconObjectH object_;
    bool error_;
    int waiting_;

    using Map = std::map<TaskH, DependancyH>;
    Map tasks_;
};

inline void destroy(InternalTask**) {}

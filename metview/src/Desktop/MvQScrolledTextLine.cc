/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQScrolledTextLine.h"

#include "LineFactory.h"
#include "MvIconParameter.h"
#include "RequestPanel.h"

#include <QPlainTextEdit>

MvQScrolledTextLine::MvQScrolledTextLine(RequestPanel& owner, const MvIconParameter& param) :
    MvQRequestPanelLine(owner, param)
{
    textEdit_ = new QPlainTextEdit;

    // Try to set the optimal widget height
    QFont font;
    QFontMetrics fm(font);
    textEdit_->setMaximumHeight(fm.size(Qt::TextExpandTabs, "A\nA\nA\nA\nA\nA\nA\nA\nA\nA\n").height());

    owner_.addWidget(textEdit_, row_, WidgetColumn);

    connect(textEdit_, SIGNAL(textChanged()),
            this, SLOT(slotTextEdited()));
}


void MvQScrolledTextLine::refresh(const std::vector<std::string>& values)
{
    std::string s;
    for (const auto& value : values) {
        if (s.length())
            s += "/";
        s += value;
    }

    if (s != textEdit_->toPlainText().toStdString()) {
        textEdit_->setPlainText(s.c_str());
    }

    // changed_ = false;
}

void MvQScrolledTextLine::slotTextEdited()
{
    owner_.set(param_.name(), textEdit_->toPlainText().toStdString());
}


static LineMaker<MvQScrolledTextLine> maker1("scrolled_text");

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

#include "MvQRequestPanelLine.h"
#include "MvIconParameter.h"

#include <QPen>
#include <QString>
#include <QWidget>

class QColor;
class QLabel;

class RequestPanel;

class MvQColourListWidget : public QWidget
{
    Q_OBJECT

public:
    MvQColourListWidget(QWidget* parent = nullptr);
    void setColours(QList<QColor>);
    QColor currentColour() const;
    void setCurrentColour(QColor);
    QList<QColor> colours() { return colours_; }
    void setEdited(bool);

public slots:
    void slotInsertBeforeCell();
    void slotInsertAfterCell();
    void slotDeleteCell();

protected slots:
    void slotCopyColourList();
    void slotCopyColour();
    void slotCopyRgb();
    void slotCopyHsl();
    void slotPasteColour();
    void slotPasteColourList();

signals:
    void changed();
    void currentChanged(QColor);
    void singleCellClicked();

protected:
    void resizeEvent(QResizeEvent*) override;
    void changeEvent(QEvent* event) override;
    void paintEvent(QPaintEvent*) override;
    void mousePressEvent(QMouseEvent*) override;
    void createPixmap();
    void renderCurrentCell(QPainter*);
    void setCurrentCell(QPoint);
    void setCurrentCell(int);
    void deleteCell(int);
    void insertBeforeCell(int);
    void insertAfterCell(int);
    void checkActionState();

    bool edited_;
    QPixmap pix_;
    QList<QColor> colours_;
    int cellWidth_;
    int currentCell_;
    QPen blackPen_;
    QPen whitePen_;
    QAction* insertBeforeAc_;
    QAction* insertAfterAc_;
    QAction* deleteCellAc_;
    QAction* copyColAc_;
    QAction* pasteColAc_;
    int editorGapX_;
    int editorGapY_;
};

class MvQColourListLine : public MvQRequestPanelLine
{
    Q_OBJECT

public:
    MvQColourListLine(RequestPanel& owner, const MvIconParameter& param);
    ~MvQColourListLine() override = default;

    QString currentValue() { return {}; }
    void refresh(const std::vector<std::string>&) override;

public slots:
    void slotListChanged();
    void slotCurrentChanged(QColor);
    void slotHelperEdited(const std::vector<std::string>&) override;
    void slotSingleCellClicked();
    void slotHelperOpened(bool) override;

protected:
    void buildHelper() override;
    void updateHelper();

    MvQColourListWidget* widget_;
};

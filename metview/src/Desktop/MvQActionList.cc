/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQActionList.h"

#include <QAction>

bool MvQActionList::trigger(QString name)
{
    foreach (QAction* ac, lst_) {
        if (ac->data().toString() == name) {
            ac->trigger();
            return true;
        }
    }
    return false;
}

QAction* MvQActionList::action(QString name)
{
    foreach (QAction* ac, lst_) {
        if (ac->data().toString() == name)
            return ac;
    }
    return nullptr;
}

bool MvQActionList::shortCutDefined(QString scName)
{
    foreach (QAction* ac, lst_) {
        if (ac->shortcut().toString() == scName)
            return true;
    }
    return false;
}
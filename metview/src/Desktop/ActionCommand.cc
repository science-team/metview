/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "ActionCommand.h"
#include "IconObject.h"
#include "Action.h"


ActionCommand::ActionCommand(const std::string& name) :
    Command(name)
{
}

ActionCommand::~ActionCommand() = default;

void ActionCommand::execute(IconObject* o)
{
    o->action(name());
}


static ActionCommand saveCmd("save");
static ActionCommand visualiseCmd("visualise");
static ActionCommand examineCmd("examine");
static ActionCommand executeCmd("execute");
static ActionCommand analyseCmd("analyse");
static ActionCommand extractCmd("extract");
static ActionCommand extractSubfolderCmd("extract_subfolder");
static ActionCommand exportMacroCmd("export_macro");
static ActionCommand exportPythonCmd("export_python");
static ActionCommand metzoomCmd("metzoom");

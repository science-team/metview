/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QList>
#include <QMap>
#include <QObject>
#include <QTime>

#include <ctime>

class Folder;
class FolderPresenter;
class QTimer;
class QFileSystemWatcher;

struct MvQTimeoutInfo
{
    MvQTimeoutInfo() :
        lenght(0) {}
    QTime last;
    int lenght;
};


class MvQFolderWatcher : public QObject
{
    Q_OBJECT

public:
    static MvQFolderWatcher* add(FolderPresenter*);
    static void remove(FolderPresenter*);
    static void print();
    static void reload(FolderPresenter*);

    FolderPresenter* folderPresenter() { return presenter_; }
    Folder* folder();

public slots:
    void slotTimeout();

protected slots:
    void slotFolderChanged(QString);
    void slotFolderPathChanged();

protected:
    MvQFolderWatcher(FolderPresenter*);
    ~MvQFolderWatcher() override = default;

    void reload();
    static void cleanLog();
    static MvQFolderWatcher* find(FolderPresenter* fp);

    FolderPresenter* presenter_;
    QFileSystemWatcher* fsWatcher_;
    QTimer* timer_;
    static int timeout_;
    static QList<MvQFolderWatcher*> items_;
    static QMap<Folder*, MvQTimeoutInfo> timeoutLog_;
};

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Folder.h"

class SystemFolder : public Folder
{
public:
    SystemFolder(Folder* parent, const IconClass& kind,
                 const std::string& name, IconInfo* info);

    ~SystemFolder() override;

protected:
    std::set<std::string> can() override;

private:
    SystemFolder(const SystemFolder&);
    SystemFolder& operator=(const SystemFolder&);

    bool renamable() const override;
};

inline void destroy(SystemFolder**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(SystemFolder);
//#endif

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Action.h"

Action::Action(const std::string& name, const std::string& mode) :
    name_(name),
    mode_(mode)
{
}

Action::~Action() = default;

const std::string& Action::name() const
{
    return name_;
}

const std::string& Action::mode() const
{
    return mode_;
}

void Action::print(std::ostream& s) const
{
    s << "Action(" << name_ << "," << mode_ << ")";
}

bool Action::operator<(const Action& other) const
{
    if (name_ == other.name_)
        return mode_ < other.mode_;
    else
        return name_ < other.name_;
}

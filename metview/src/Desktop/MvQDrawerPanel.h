/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QIcon>
#include <QTabBar>
#include <string>


class QAbstractButton;
class QHBoxLayout;
class QStackedWidget;
class QTabBar;

class MvQContextItemSet;

class MvQDrawerTabBar : public QTabBar
{
    Q_OBJECT

public:
    MvQDrawerTabBar(QWidget* parent = nullptr);
signals:
    void leftClicked(int);

protected:
    void mousePressEvent(QMouseEvent*) override;
};

class MvQDrawerPanel : public QWidget
{
    Q_OBJECT

public:
    MvQDrawerPanel(QWidget* parent = nullptr);
    ~MvQDrawerPanel() override;

    void setSorted(bool b) { sorted_ = b; }
    int drawerCount();
    int drawerIndex(QWidget*);
    QWidget* drawer(int);
    QString drawerText(int);
    void addDrawer(QWidget*, QString);
    void shrink();

public slots:
    void slotTabClicked(int);
    void slotContextMenu(const QPoint&);
    void slotTabButton();

protected:
    virtual void command(QString, int) = 0;
    virtual MvQContextItemSet* cmSet() = 0;

    void addCornerButton(QAbstractButton*);
    void renameDrawer(int, QString);
    void deleteDrawer(int);
    void sortDrawers();
    void paintEvent(QPaintEvent*) override;

private:
    void setCurrent(QString);
    void updateTabButtonStatus();
    void setTabButtonStatus(int index, const QIcon& icon, QString);

    QHBoxLayout* barLayout_;
    MvQDrawerTabBar* bar_;
    QStackedWidget* stacked_;
    bool sorted_;
    QIcon openIcon_;
    QIcon closeIcon_;
    QIcon noIcon_;
};

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include "Desktop.h"
#include "Path.h"


class Folder;

class FolderSettings
{
public:
    FolderSettings(Folder*);

    Desktop::FolderViewMode viewMode();
    int iconSize();
    void setViewModeAndSize(Desktop::FolderViewMode, int);
    void setViewMode(Desktop::FolderViewMode);
    void setIconSize(int);
    bool hasFile();
    void readDefaults();
    bool halfSizeImported() const { return halfSizeImported_; }
    void halfSizeImportUsed() { halfSizeImported_ = false; }
    static Desktop::FolderViewMode toViewMode(int);
    static void setDefaults(Desktop::FolderViewMode mode, int iconSize);
    static Desktop::FolderViewMode defaultViewMode();
    static int defaultIconSize();

private:
    void read();
    void write();

    Folder* folder_{nullptr};
    std::string fileName_;
    Desktop::FolderViewMode viewMode_{Desktop::IconViewMode};
    int iconSize_{32};
    bool halfSizeImported_{false};
};

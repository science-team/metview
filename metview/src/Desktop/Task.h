/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <set>

#include "Counted.h"

class Queue;
class TaskObserver;
class Request;

class Task : public virtual Counted
{
public:
    Task();
    ~Task() override;

    virtual void start() = 0;
    virtual void stop();
    void add(TaskObserver*);
    void remove(TaskObserver*);
    virtual void addContext(const Request&) = 0;

protected:
    void success(const Request&);
    void failure(const Request&);

    bool stopped_;

private:
    // No copy allowed
    Task(const Task&);
    Task& operator=(const Task&);

    std::set<TaskObserver*> observers_;

    friend std::ostream& operator<<(std::ostream& s, const Task& p)
    {
        p.print(s);
        return s;
    }
    friend class Queue;
};

using TaskH = Handle<Task>;

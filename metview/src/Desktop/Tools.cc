/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END ************/

#include "Tools.h"

#include <iostream>

#include "ConfigLoader.h"
#include "Request.h"

#include <QAction>

static std::vector<Tools*> tools;

Tools::Tools(request* r) :
    request_(r)
{
}

Tools::~Tools() = default;

void Tools::make(QList<QAction*>& lst, QObject* parent)
{
    for (auto& tool : tools) {
        auto* ac = new QAction(parent);
        const char* text = get_value(tool->request_, "label", 0);
        ac->setText(text);

        if (const char* icon = get_value(tool->request_, "icon", 0))
            ac->setIcon(QPixmap(":/desktop/" + QString(icon) + ".svg"));

        lst << ac;

        connect(ac, SIGNAL(triggered()),
                tool, SLOT(start()));
    }
}


void Tools::start()
{
    std::cout << "Tools::start " << get_value(request_, "service", 0) << std::endl;
    Request r(get_value(request_, "action", 0));

    MvApplication::callService(get_value(request_, "service", 0), r, nullptr);
}


void Tools::load(request* r)
{
    tools.push_back(new Tools(r));
}


static SimpleLoader<Tools> loadClasses("desktop_tool", 1);

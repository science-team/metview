/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "SimpleEditor.h"

#include <QHBoxLayout>
#include <QPainter>
#include <QScrollArea>

#include "EditorFactory.h"
#include "IconObject.h"
//#include "Log.h"

#include "IconClass.h"
#include "IconFactory.h"
#include "RequestPanel.h"

#include "MvQRequestPanelWidget.h"


SimpleEditor::SimpleEditor(const IconClass& kind, const std::string& name) :
    MvQEditor(kind, name)
{
    setupLineFilter();
    setupShowDisabledActions();

    // Panel
    panelArea_ = new QScrollArea(this);
    panelArea_->setWidgetResizable(true);

    centralLayout_->addWidget(panelArea_);

    std::vector<std::string> classes;
    classes.push_back(kind.name());

    auto* w = new MvQRequestPanelWidget(classes, this);
    Q_ASSERT(showDisabledAc_);
    panel_ = new RequestPanel(kind, w, this, showDisabled());

    panelArea_->setWidget(w);

    connect(w, SIGNAL(iconDropped(IconObject*)),
            this, SLOT(slotIconDropped(IconObject*)));
}

SimpleEditor::~SimpleEditor()
{
    delete panel_;
}

IconObject* SimpleEditor::copy(const std::string& /*name*/)
{
    // IconObject* o = IconFactory::create(name, class_);
    // panel_.apply();
    // o->request(panel_.request());
    // return o;
    return nullptr;
}

void SimpleEditor::edit()
{
    if (!current_)
        return;

    MvQEditor::edit();
}


void SimpleEditor::apply()
{
    panel_->apply();
    current_->setRequest(panel_->request());
}

void SimpleEditor::reset()
{
    panel_->reset(current_);

    if (current_ && current_->locked()) {
        if (QWidget* w = panelArea_->widget())
            w->setEnabled(false);
    }

    panel_->runFilter();
}

void SimpleEditor::close()
{
    panel_->close();
    // Enable again the widget
    if (QWidget* w = panelArea_->widget())
        w->setEnabled(true);
}

// Request SimpleEditor::currentRequest(long custom_expand)
//{
// panel_.apply();
// return custom_expand ? panel_.request(custom_expand) : panel_.request();
// }

void SimpleEditor::replace(IconObjectH obj)
{
    panel_->replace(obj);
    panel_->runFilter();
}

void SimpleEditor::merge(IconObjectH obj)
{
    panel_->merge(obj);
    panel_->runFilter();
}

void SimpleEditor::slotIconDropped(IconObject* obj)
{
    merge(obj);
}

void SimpleEditor::slotFilterItems(QString txt)
{
    Q_ASSERT(panel_);
    panel_->setFilter(txt);
    updateFilterCountLabel();
}

int SimpleEditor::filterNonMatchCount()
{
    return panel_->filterNonMatchCount();
}

void SimpleEditor::slotShowDisabled(bool st)
{
    Q_ASSERT(panel_);
    panel_->setShowDisabledItems(st);
    updateFilterCountLabel();
}

void SimpleEditor::applyTemporayRules()
{
    Q_ASSERT(panel_);
    panel_->applyTemporaryRules(temporary_);
}

static EditorMaker<SimpleEditor> editorMaker1("SimpleEditor");
static EditorMaker<SimpleEditor> editorMaker2("RetrieveEditor");

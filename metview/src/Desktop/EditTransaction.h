/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Transaction.h"
#include "EditorObserver.h"
#include "TaskObserver.h"
#include "IconObject.h"

class EditTransaction : public Transaction,
                        public EditorObserver,
                        public TaskObserver
{
public:
    EditTransaction();
    EditTransaction(MvTransaction* t);
    ~EditTransaction() override = default;
    EditTransaction(const EditTransaction&) = delete;
    EditTransaction& operator=(const EditTransaction&) = delete;

private:
    MvTransaction* cloneSelf() override;
    void callback(MvRequest&) override;
    void apply(IconObject*) override;
    void close(IconObject*) override;
    void success(Task*, const Request&) override;
    void failure(Task*, const Request&) override;

    IconObjectH current_;
    IconClass* subClass_{nullptr};
};

inline void destroy(EditTransaction**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(EditTransaction);
//#endif

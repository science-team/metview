/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQMessageBox.h"

#include "mars.h"
#include "MvRequest.h"
#include "ReplyErrorParser.h"

MvQMessageBox::MvQMessageBox(request* req, QWidget* parent) :
    QMessageBox(parent)
{
    if (!req)
        return;
    MvRequest r(req);
    const char* verb = r.getVerb();
    if (verb && strcmp(verb, "MESSAGE") == 0) {
        if (const char* st = r("STATUS")) {
            if (strcmp(st, "ERROR") == 0) {
                setIcon(QMessageBox::Critical);
                setWindowTitle("Error message");
            }
            else if (strcmp(st, "WARNING") == 0) {
                setIcon(QMessageBox::Warning);
                setWindowTitle("Warning message");
            }
        }
        else {
            setIcon(QMessageBox::Information);
            setWindowTitle("Message");
        }

        if (const char* txt = r("TEXT"))
            setText(txt);

        if (const char* detailed = r("DETAILED"))
            setDetailedText(detailed);

        exec();
    }
}

MvQMessageBox::MvQMessageBox(const ReplyErrorParser& p, QWidget* parent) : QMessageBox(parent)
{
    if (p.status() == ReplyErrorParser::ErroStatus) {
        setIcon(QMessageBox::Critical);
        setWindowTitle("Error message");
    } else if (p.status() == ReplyErrorParser::WarningStatus) {
        setIcon(QMessageBox::Warning);
        setWindowTitle("Warning message");
    } else {
        setWindowTitle("Message");
    }

    setText(QString::fromStdString(p.error()));

    if (!p.detailed().empty()) {
        setDetailedText(QString::fromStdString(p.detailed()));
    }

    exec();
}

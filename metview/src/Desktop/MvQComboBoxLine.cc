/***************************** LICENSE START ***********************************

 Copyright 2015 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QDebug>
#include <QFontMetrics>
#include <QWheelEvent>

#include "MvQComboBoxLine.h"

#include "LineFactory.h"
#include "RequestPanel.h"

//=================================================================================
// MvQEditorComboBox
// A custom combox implementation: we do not want the combobox to steal the mouse
// wheel event when it is in focus.
//=================================================================================

MvQEditorComboBox::MvQEditorComboBox(QWidget* parent) :
    QComboBox(parent)
{
    setFocusPolicy(Qt::StrongFocus);
}

void MvQEditorComboBox::wheelEvent(QWheelEvent* event)
{
    if (!hasFocus()) {
        event->ignore();
    }
    else {
        QComboBox::wheelEvent(event);
    }
}

void MvQEditorComboBox::focusInEvent(QFocusEvent* event)
{
    setFocusPolicy(Qt::WheelFocus);
    QComboBox::focusInEvent(event);
}

void MvQEditorComboBox::focusOutEvent(QFocusEvent* event)
{
    setFocusPolicy(Qt::StrongFocus);
    QComboBox::focusOutEvent(event);
}

//=================================================================================
//
// MvQComboBoxLine
//
//=================================================================================

MvQComboBoxLine::MvQComboBoxLine(RequestPanel& owner, const MvIconParameter& param) :
    MvQRequestPanelLine(owner, param),
    reportChange_(true)
{
    cbEdit_ = new MvQEditorComboBox(parentWidget_);

    owner_.addWidget(cbEdit_, row_, WidgetColumn);

    param_.scan(*this);

    connect(cbEdit_, SIGNAL(currentIndexChanged(int)),
            this, SLOT(slotCurrentChanged(int)));

    // combo_->setCurrentIndex(dindex);
}


void MvQComboBoxLine::next(const MvIconParameter&, const char* first, const char* second)
{
    if (!first)
        return;
    QString dataStr = (second) ? QString(second) : QString(first);
    cbEdit_->addItem(QString::fromStdString(param_.beautifiedName(first)), dataStr);
}

void MvQComboBoxLine::refresh(const std::vector<std::string>& values)
{
    if (values.size() > 0) {
        for (int i = 0; i < cbEdit_->count(); i++) {
            if (cbEdit_->itemData(i).toString().toStdString() == values[0]) {
                reportChange_ = false;
                cbEdit_->setCurrentIndex(i);
                reportChange_ = true;
            }
        }
    }

    // changed_ = false;
}

void MvQComboBoxLine::slotCurrentChanged(int index)
{
    if (reportChange_ && index >= 0 && index < cbEdit_->count())
        owner_.set(param_.name(), cbEdit_->itemData(index).toString().toStdString());
}

void MvQComboBoxLine::fixedFontAndHeight(QFont& font, int& h) const
{
    font = QFont();
    font.setPointSize(font.pointSize() - 1);
    auto fm = QFontMetrics(font);
    h = fm.height() + 4;
}

static LineMaker<MvQComboBoxLine> maker1("menu");
static LineMaker<MvQComboBoxLine> maker2("option_menu");

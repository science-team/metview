/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QListView>
#include <QString>

#include "IconObject.h"

#include <vector>


class MvIconParameter;
class Request;
class RequestPanelItem;
class MvQContextItemSet;
class QShortcut;

class MvQIconHolderModel : public QAbstractItemModel
{
public:
    MvQIconHolderModel(const std::vector<std::string>&, QObject* parent = nullptr);

    int columnCount(const QModelIndex&) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    void setIcons(const std::vector<IconObjectH>&);
    void clearIcons(const std::vector<IconObjectH>&);
    void add(IconObjectH);
    void remove(IconObjectH);
    bool accept(IconObjectH);
    IconObject* iconObject(const QModelIndex&);
    void iconObjects(std::vector<IconObjectH>&);

protected:
    QList<IconObjectH> icons_;
    std::vector<std::string> classes_;
};


class MvQIconHolderView : public QListView
{
    Q_OBJECT

public:
    MvQIconHolderView(MvQIconHolderModel*, RequestPanelItem*, QWidget* parent = nullptr);
    ~MvQIconHolderView() override;

protected slots:
    void slotDoubleClickItem(const QModelIndex& index);
    void slotContextMenu(const QPoint&);
    void slotIconShortCut();

signals:
    void edited();
    void iconDropped(QString);

protected:
    MvQContextItemSet* cms();
    void setupShortCut();

    void remove(IconObjectH);
    void command(QString, IconObjectH);

    void dragEnterEvent(QDragEnterEvent*) override;
    void dragMoveEvent(QDragMoveEvent*) override;
    void dropEvent(QDropEvent*) override;

    MvQIconHolderModel* model_;
    std::vector<std::string> classes_;
    RequestPanelItem* owner_;
};

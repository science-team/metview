/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include "mars.h"

class MvQRequestPanelLine;

class RequestPanel;
class MvIconParameter;

class LineFactory
{
public:
    LineFactory(const std::string&);
    virtual ~LineFactory();

    virtual MvQRequestPanelLine* make(RequestPanel&, const MvIconParameter&) = 0;
    static MvQRequestPanelLine* create(RequestPanel&, const MvIconParameter&);

private:
    LineFactory(const LineFactory&);
    LineFactory& operator=(const LineFactory&);
};

template <class T>
class LineMaker : public LineFactory
{
    MvQRequestPanelLine* make(RequestPanel& e, const MvIconParameter& p) override { return new T(e, p); }

public:
    LineMaker(const std::string& name) :
        LineFactory(name) {}
};

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Counted.h"

class Request;

class ReplyObserver : public virtual Counted
{
public:
    ReplyObserver();
    ~ReplyObserver() override;
    ReplyObserver(const ReplyObserver&) = delete;
    ReplyObserver& operator=(const ReplyObserver&) = delete;

    virtual void reply(const Request&, int) = 0;
    virtual void progress(const Request&) = 0;
    virtual void message(const std::string&) = 0;

protected:
    void callService(const std::string&, const Request&);
};


class ReplyObserverH : public Handle<ReplyObserver>
{
public:
    ReplyObserverH(ReplyObserver* o) :
        Handle<ReplyObserver>(o) {}
};

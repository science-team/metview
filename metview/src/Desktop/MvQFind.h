/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QMutex>
#include <QThread>

#include "IconClass.h"
#include "Path.h"

class Folder;

struct FindOptions
{
    FindOptions() :
        includeSub(false),
        followLinks(false),
        includeSys(false),
        caseSensitive(false),
        useTime(false),
        strictType(false),
        contentCaseSensitive(false),
        size(0) {}

    std::string name;
    std::string type;
    std::string contentText;
    bool includeSub;
    bool followLinks;
    bool includeSys;
    bool caseSensitive;
    bool useTime;
    bool strictType;
    bool contentCaseSensitive;
    time_t fromTime;
    time_t toTime;
    int sizeOper;
    off_t size;
    std::string owner;
    std::string group;
};


class MvQFind : public QThread
{
    Q_OBJECT

public:
    MvQFind(std::string, QObject* parent = nullptr);
    ~MvQFind() override;
    // void findOptions(FindOptions options) {options_=options;}
    void find(const std::string&, FindOptions);
    void abortFind();

signals:
    void found(QString, QString);

protected:
    void run() override;
    void scan();
    void scan(const Path& dir, const FindOptions& option, const std::vector<std::string>& classes);
    // void scanFolder(const Path& dir,const FindOptions& option,const std::vector<std::string>& classes,Folder*);
    bool match(const FindOptions& options, const std::string& path, const std::string fileName,
               const std::vector<std::string>& classes, std::string& icName, std::string& icType);

    bool isTextType(const std::string&, const std::string&);
    void toRelPath(const std::string& path, std::string& relPath);

    std::string path_;
    FindOptions options_;
    Path mvRoot_;
    bool abort_;
    QMutex mutex_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "RequestPanelItem.h"

#include <QIcon>
#include <QObject>
#include <QStringList>
#include <QVector>

class QAction;
class QWidget;

class MvIconParameter;
class RequestPanel;

class MvQRequestPanelHelp : public QObject
{
    Q_OBJECT

public:
    MvQRequestPanelHelp(RequestPanel&, const MvIconParameter&);
    ~MvQRequestPanelHelp() override = default;

    virtual void start() = 0;
    virtual bool dialog() = 0;
    virtual QIcon dialogIcon() { return {}; }
    virtual bool useLargeDialogIcon() { return false; }
    virtual QWidget* widget() = 0;
    virtual void refresh(const std::vector<std::string>&) = 0;
    virtual void setExternalActions(QList<QAction*>) {}

    static MvQRequestPanelHelp* build(RequestPanel&, const MvIconParameter&);

signals:
    void edited(const std::vector<std::string>&);
    void edited(QStringList, QVector<bool>);
    void editConfirmed();

protected:
    RequestPanel& owner_;
    const MvIconParameter& param_;
    QWidget* parentWidget_;
};

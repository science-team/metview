/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <map>
#include <string>
#include <vector>


class FolderDatabase
{
public:
    virtual ~FolderDatabase() = default;
    static std::string path(const std::string&);

protected:
    FolderDatabase();

    void read();
    void write();
    std::string find(const std::string&);
    std::string add(const std::string&);
    std::string uniquePath();
    virtual bool isOwner(const std::string&) = 0;
    const std::string& homePath() const { return homePath_; }
    const std::string& rootPath() const { return rootPath_; }
    const std::string& dbPath() const { return dbPath_; }
    void setDbPath(const std::string& p) { dbPath_ = p; }
    void setDbFile(const std::string& f) { dbFile_ = f; }

private:
    std::string homePath_;
    std::string rootPath_;
    std::string dbPath_;
    std::string dbFile_;
    std::map<std::string, std::string> folders_;

    static std::vector<FolderDatabase*> db_;
};

class InternalFolderDatabase : public FolderDatabase
{
public:
    InternalFolderDatabase();
    bool isOwner(const std::string&) override;
};

class ExternalFolderDatabase : public FolderDatabase
{
public:
    ExternalFolderDatabase();
    bool isOwner(const std::string&) override;
};

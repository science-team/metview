/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQRequestPanelHelp.h"
#include "MvIconParameter.h"

#include <QPen>
#include <QWidget>
#include <QAbstractItemModel>

class QComboBox;
class QHBoxLayout;
class QLabel;
class QLineEdit;
class QPushButton;
class QSlider;
class QSplitter;
class QSpinBox;
class QTabWidget;
class QToolButton;
class QTreeView;
class QSortFilterProxyModel;

class MvQStyleDbItem;
class RequestPanel;
class MvQStyleTreeWidget;

// Model to dislay/select the suites
class MvQStyleModel : public QAbstractItemModel
{
public:
    enum CustomItemRole
    {
        SortRole = Qt::UserRole + 1
    };

    explicit MvQStyleModel(QObject* parent = nullptr);
    ~MvQStyleModel() override;

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    void setFilter(QString s);
    void clearFilter();

protected:
    bool isFiltered(MvQStyleDbItem* item) const;

    QString filter_;
};

class MvQStyleSelectionWidget : public QWidget
{
    Q_OBJECT
public:
    MvQStyleSelectionWidget(QWidget* parent = nullptr);
    void setCurrent(const std::string&);

protected slots:
    void slotNameFilter(QString s);
    void slotItemSelected(const QModelIndex&, const QModelIndex&);

signals:
    void itemSelected(int);

protected:
    void clearFilter();

    bool ignoreFilterChanged_;
    QLineEdit* nameLe_;
    MvQStyleTreeWidget* browser_;
    MvQStyleModel* model_;
    QSortFilterProxyModel* sortModel_;
};

class MvQStyleHelp : public MvQRequestPanelHelp
{
    Q_OBJECT

public:
    MvQStyleHelp(RequestPanel& owner, const MvIconParameter& param);
    ~MvQStyleHelp() override = default;

    void start() override {}
    bool dialog() override { return false; }
    QWidget* widget() override { return selector_; }

public slots:
    void slotSelected(int);

protected:
    void refresh(const std::vector<std::string>&) override;

private:
    MvQStyleSelectionWidget* selector_;
    QString oriName_;
};

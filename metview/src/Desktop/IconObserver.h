/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

class IconObject;

class IconObserver
{
public:
    virtual ~IconObserver() = default;
    virtual void iconChanged(IconObject*) {}
    virtual void iconDestroyed(IconObject*) {}
    virtual void iconEdited(IconObject*) {}
    virtual void iconClosed(IconObject*) {}
};

/***************************** LICENSE START ***********************************

 Copyright 2022 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QDialog>

class IconObject;

namespace Ui
{
class FilePropertyDialog;
}

class FilePropertyDialog : public QDialog
{
    Q_OBJECT
public:
    FilePropertyDialog(QWidget* parent = nullptr);
    ~FilePropertyDialog() override;
    void setItem(IconObject* item);

public slots:
    void accept() override;

private:
    void writeSettings();
    void readSettings();

    Ui::FilePropertyDialog* ui_{nullptr};
    std::string path_;
    std::string linkPath_;
};

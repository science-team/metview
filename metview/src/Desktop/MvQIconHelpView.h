/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQIconStripView.h"

class MvQContextItemSet;

class MvQIconHelpView : public MvQIconStripView
{
public:
    MvQIconHelpView(MvQFolderModel*, QWidget* parent = nullptr);
    ~MvQIconHelpView() override = default;

protected:
    MvQContextItemSet* cmSet() override;
};

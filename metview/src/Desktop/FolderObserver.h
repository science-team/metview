/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

class IconObject;
class Folder;

class FolderObserver
{
public:
    virtual void arrived(IconObject*) = 0;
    virtual void gone(IconObject*) = 0;
    virtual void position(IconObject*, int, int) = 0;
    virtual void renamed(IconObject*, const std::string&) = 0;


    virtual void waiting(IconObject*) {}
    virtual void error(IconObject*) {}
    virtual void modified(IconObject*) {}
    virtual void ready(IconObject*) {}
    virtual void opened(IconObject*) {}
    virtual void closed(IconObject*) {}

    virtual void highlight(IconObject*) {}
};

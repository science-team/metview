/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QList>
#include <QSettings>
#include <QString>
#include <QWidget>

#include "Desktop.h"
#include "MvQActionList.h"

class QMenu;
class QStackedWidget;
class QTabBar;
class QToolButton;
class QVBoxLayout;

class FolderSearchData;

class MvQContextItemSet;
class MvQFolderHistory;
class MvQFolderNavigation;
class MvQFolderWidget;
class PreviewPanel;

class Folder;

#include "IconObject.h"


class MvQTabWidget : public QWidget
{
    Q_OBJECT

public:
    MvQTabWidget(QWidget* parent = nullptr);

    int currentIndex() const;
    int indexOfWidget(QWidget*) const;
    QWidget* widget(int) const;
    QWidget* currentWidget() const;
    void checkTabStatus();
    void checkTabNavigationStatus(bool prev, bool next);
    void addTab(QWidget*, QPixmap, QString);
    void setTabText(int, QString);
    void setTabIcon(int, QPixmap);
    void setTabToolTip(int index, QString txt);
    void setTabData(int index, QString txt);
    QIcon tabIcon(int index) const;
    QVariant tabData(int index) const;
    int count() const;
    void clear();

public slots:
    void removeTab(int);
    void removeOtherTabs(int);
    void setCurrentIndex(int);
    virtual void slotTabList() = 0;
    virtual void slotRecentTabs() = 0;

private slots:
    void slotContextMenu(const QPoint&);
    void currentTabChanged(int index);
    void tabMoved(int from, int to);

signals:
    void currentIndexChanged(int);
    void newTabRequested(bool);
    void prevTabRequested(bool);
    void nextTabRequested(bool);
    void iconDropped(int, QDropEvent*);

protected:
    virtual MvQContextItemSet* cmSet() = 0;
    virtual void tabBarCommand(QString, int) = 0;
    virtual QString folderPath(int) = 0;
    void removeDuplicatedTabs();

    void checkDropTarget(QDropEvent* event);
    void removeDropTarget();
    void dragEnterEvent(QDragEnterEvent* event) override;
    void dragMoveEvent(QDragMoveEvent* event) override;
    void dragLeaveEvent(QDragLeaveEvent* event) override;
    void dropEvent(QDropEvent* event) override;
    void paintEvent(QPaintEvent*) override;

    QToolButton* tabListTb_;
    QToolButton* recentTb_;
    QAction* prevAc_;
    QAction* nextAc_;
    QToolButton* prevTabTb_;
    QToolButton* nextTabTb_;

private:
    QTabBar* bar_;
    QStackedWidget* stacked_;
    QToolButton* addTb_;
};

class MvQFolderPanel : public MvQTabWidget
{
    Q_OBJECT

public:
    MvQFolderPanel(QWidget* parent = nullptr);
    ~MvQFolderPanel() override;

    Folder* currentFolder();
    QString currentFolderName();
    QString currentFolderPath();
    QList<Folder*> currentFolders();
    void setHistoryMenu(QMenu*);
    void setViewMode(Desktop::FolderViewMode);
    Desktop::FolderViewMode viewMode();
    bool viewModeGrid();
    void setIconSize(int);
    int iconSize();
    void forceIconSize(int);
    MvQFolderNavigation* folderNavigation();
    void saveFolderInfo();
    MvQFolderWidget* addWidget(QString);
    void resetWidgets(QStringList);
    void addIconAction(QAction*);
    void addDesktopAction(QAction*);
    bool showIcon(IconObject*, bool addFolder);
    void setPreviewPanel(PreviewPanel* p) { previewPanel_ = p; }

    void writeSettings(QSettings&);
    void readSettings(QSettings&);

public slots:
    void slotCurrentWidgetChanged(int);
    void slotChFolderBack();
    void slotChFolderForward();
    void slotChFolderParent();
    void slotChFolderMvHome();
    void slotChFolderWastebasket();
    void slotChFolderDefaults();
    void slotChFolderFromHistory(QString);
    void slotChFolderFromBookmarks(QString);
    void slotChFolderFromBreadcrumbs(QString);
    void slotIconCommand(QString, IconObjectH);
    void slotPathCommand(QString, QString);
    void slotDesktopCommand(QString, QPoint);
    void slotGridByName(bool);
    void slotGridBySize(bool);
    void slotGridByType(bool);
    void slotReload(bool);
    void slotPathChanged();
    void slotShowLastCreated(bool);
    void slotLookupOrOpenInTab(QString path);
    void slotNewTab(bool);
    void slotNewWindow(bool);
    void slotIconDroppedToTab(int, QDropEvent*);
    void slotShowIconInCurrent(IconObject*);
    void slotFindIcons(FolderSearchData*);
    void slotIconCommandFromMain(QAction* ac);

protected slots:
    void slotPrevTab(bool);
    void slotNextTab(bool);
    void slotTabList() override;
    void slotRecentTabs() override;

signals:
    void itemInfoChanged(IconObject*);
    void pathChanged();
    void currentWidgetChanged();

protected:
    MvQContextItemSet* cmSet() override;
    MvQFolderWidget* currentFolderWidget();
    MvQFolderWidget* folderWidget(int);
    QString folderPath(int) override;
    void tabBarCommand(QString, int) override;

    QString mvHomePath_;

    MvQFolderHistory* folderHistory_;
    MvQFolderNavigation* tabNavigation_;
    bool inTabNavigation_;
    bool inInitialLoad_;
    MvQActionList iconActions_;
    MvQActionList desktopActions_;
    PreviewPanel* previewPanel_{nullptr};
};

/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQRequestPanelHelp.h"
#include "ShellTask.h"

#include <QDialog>

class QTextEdit;

class QAbstractButton;
class QLabel;

class MvQScriptHelp;

class ScriptHelpTask : public ShellTask
{
    friend class MvQScriptHelp;

    Q_OBJECT
public:
    ScriptHelpTask(const std::string&, IconObject*);

signals:
    void textArrived(QString);
    void finished();

protected:
    void ready(const char*) override;
    void done(FILE*) override;
};

class MvQMessageDialog : public QDialog
{
    Q_OBJECT

public:
    MvQMessageDialog(QWidget* parent = nullptr);
    void setHeader(QString);
    void clear();

public slots:
    void append(QString);

protected:
    QTextEdit* te_;
    QLabel* header_;
};

class MvQScriptHelp : public MvQRequestPanelHelp
{
    Q_OBJECT

public:
    MvQScriptHelp(RequestPanel&, const MvIconParameter&);
    ~MvQScriptHelp() override = default;

    void start() override;
    bool dialog() override { return true; }
    QIcon dialogIcon() override;
    QWidget* widget() override { return nullptr; }

public slots:
    void slotFinished();
    void slotClosed();

protected:
    void refresh(const std::vector<std::string>&) override {}

    ScriptHelpTask* task_;
    MvQMessageDialog* mb_;
};

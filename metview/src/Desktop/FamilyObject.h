/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "StandardObject.h"

#include <string>

class FamilyScanner;

class FamilyObject : public StandardObject
{
public:
    FamilyObject(Folder* parent, const IconClass& kind, const std::string& name,
                 IconInfo* info);
    ~FamilyObject() override;

private:
    // No copy allowed
    FamilyObject(const FamilyObject&);
    FamilyObject& operator=(const FamilyObject&);

    FamilyScanner& scanner_;

    Request first() const;
    Request second() const;
    std::string keyword() const;

    Request request() const override;
    void setRequest(const Request&) override;
    Request requestForVerb(const std::string&) const override;

    const IconClass& editorClass() const override;
    const IconClass& iconClass() const override;
    void createFiles() override;
};

inline void destroy(FamilyObject**) {}

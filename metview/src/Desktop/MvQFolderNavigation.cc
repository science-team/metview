/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQFolderNavigation.h"

#include <cassert>

void MvQFolderNavigation::clear()
{
    folders_.clear();
    pos_ = -1;
}

bool MvQFolderNavigation::isPosCorrect()
{
    return (pos_ >= 0 && pos_ < folders_.count());
}

bool MvQFolderNavigation::hasNext()
{
    return (pos_ >= 0 && pos_ < folders_.count() - 1);
}

bool MvQFolderNavigation::hasPrev()
{
    return (pos_ > 0);
}

QString MvQFolderNavigation::next()
{
    if (hasNext())
        pos_++;
    return current();
}

QString MvQFolderNavigation::prev()
{
    if (hasPrev())
        pos_--;
    return current();
}

QString MvQFolderNavigation::current()
{
    return (isPosCorrect()) ? folders_[pos_] : QString();
}

void MvQFolderNavigation::add(QString path)
{
    if (path.isEmpty())
        return;

    if (!folders_.isEmpty() && path == folders_.back()) {
        return;
    }

    folders_ << path;
    pos_ = folders_.count() - 1;

    if (folders_.count() > maxSize_) {
        assert(maxSize_ > 50);
        while (folders_.count() > maxSize_ - 50)
            folders_.takeFirst();
    }
    pos_ = folders_.count() - 1;
}

void MvQFolderNavigation::removeAfterCurrent()
{
    if (isPosCorrect()) {
        while (folders_.count() > pos_ + 1)
            folders_.takeLast();
    }
}

void MvQFolderNavigation::removeBeforeCurrent()
{
    if (isPosCorrect()) {
        for (int i = 0; i < pos_; i++)
            folders_.takeFirst();
        pos_ = 0;
    }
}

void MvQFolderNavigation::print()
{
    for (int i = 0; i < folders_.count(); i++) {
        qDebug() << i << folders_[i];
    }
}

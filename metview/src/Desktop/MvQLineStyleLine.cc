/***************************** LICENSE START ***********************************

 Copyright 2015 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQLineStyleLine.h"

#include <map>

#include <QPainter>
#include <QPixmap>

#include "LineFactory.h"
#include "MvQTheme.h"


//=================================================================================
//
// MvQComboBoxLine
//
//=================================================================================

MvQLineStyleLine::MvQLineStyleLine(RequestPanel& owner, const MvIconParameter& param) :
    MvQComboBoxLine(owner, param)

{
    QFont f;
    int h = 0;
    fixedFontAndHeight(f, h);
    cbEdit_->setFont(f);
    cbEdit_->setIconSize(QSize(100, h - 4));
    loadPixmaps();
}

void MvQLineStyleLine::loadPixmaps()
{
    std::map<QString, Qt::PenStyle> lineStyleDef = {
        {"solid", Qt::SolidLine},
        {"dash", Qt::DashLine},
        {"dot", Qt::DotLine},
        {"chain_dot", Qt::DashDotLine},
        {"chain_dash", Qt::DashDotDotLine},
        {"dash_dot", Qt::DashDotLine},
        {"dash_dot_dot", Qt::DashDotDotLine}};

    for (int i = 0; i < cbEdit_->count(); i++) {
        auto style = cbEdit_->itemData(i).toString().toLower();
        auto pix = QPixmap(cbEdit_->iconSize());
        pix.fill(Qt::transparent);
        auto it = lineStyleDef.find(style);
        if (it != lineStyleDef.end()) {
            renderLine(&pix, it->second);
            cbEdit_->setItemIcon(i, QIcon(pix));
        }
    }
}


void MvQLineStyleLine::renderLine(QPixmap* pix, Qt::PenStyle penStyle)
{
    int margin = 4;
    auto h = pix->height();
    auto w = pix->width();
    auto painter = QPainter(pix);
    painter.setPen(QPen(MvQTheme::colour("uplot", "ribbon_pen"), 2, penStyle));
    painter.drawLine(margin, h / 2, w - margin, h / 2);
}

static LineMaker<MvQLineStyleLine> maker1("line_style");

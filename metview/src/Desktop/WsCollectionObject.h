/***************************** LICENSE START ***********************************

 Copyright 2022 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "IconObject.h"

class WsCollectionObject : public IconObject
{
public:
    using IconObject::IconObject;
    WsCollectionObject(const WsCollectionObject&) = delete;
    WsCollectionObject& operator=(const WsCollectionObject&) = delete;

    ~WsCollectionObject() override = default;
    Request request() const override;
    void setRequest(const Request&) override;
};

inline void destroy(WsCollectionObject**) {}

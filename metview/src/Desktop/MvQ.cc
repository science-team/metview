/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQ.h"

#include <QtGlobal>
#include <QAction>
#include <QMessageBox>
#include "Folder.h"

namespace MvQ
{
void emptyWasteBasket()
{
    if (QMessageBox::warning(nullptr, QObject::tr("Empty Wastebin"),
                             QObject::tr("Do you really want to empty the Wastebasket? All items will be <b>permanently</b> deleted!"),
                             QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel) == QMessageBox::Yes) {
        Folder* wbo = Folder::folder("wastebasket");
        if (wbo)
            wbo->empty();
    }
}

}  // namespace MvQ

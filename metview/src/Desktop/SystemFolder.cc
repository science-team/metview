/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "SystemFolder.h"
#include "IconFactory.h"

SystemFolder::SystemFolder(Folder* parent, const IconClass& kind,
                           const std::string& name, IconInfo* info) :
    Folder(parent, kind, name, info)
{
}

SystemFolder::~SystemFolder() = default;

std::set<std::string> SystemFolder::can()
{
    std::set<std::string> c = Folder::can();
    c.erase("delete");
    c.erase("duplicate");
    c.erase("cut");
    c.erase("destroy");
    c.erase("rename");
    return c;
}

bool SystemFolder::renamable() const
{
    return false;
}

static IconMaker<SystemFolder> systemFactory("SYSTEM");

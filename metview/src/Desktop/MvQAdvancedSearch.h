/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QDialog>

#include "IconClass.h"

class QCheckBox;
class QComboBox;
class QDateEdit;
class QLabel;
class QProgressBar;
class QRadioButton;
class QSpinBox;
class QStackedLayout;
class QTreeWidget;
class MvQFind;
class MvQLineEdit;

class Folder;
class IconObject;

class MvQAdvancedSearchDialog : public QDialog, public ClassScanner
{
    Q_OBJECT

public:
    // From ClassScanner
    void next(const IconClass&) override;
    static void open(QWidget* parent, Folder* folder = nullptr);

public slots:
    void reject() override;
    void slotFind();
    void slotCancel();
    void itemArrived(QString, QString);
    void findStarted();
    void findFinished();
    void slotContextMenu(const QPoint&);
    void slotUpdateFolderBox(int);
    void slotUpdateDateBox(int);
    void slotDateRadio(bool);
    void slotUpdateSizeBox(int);
    void slotTypeRadio(bool);

protected:
    MvQAdvancedSearchDialog(QWidget* parent, Folder* folder = nullptr);
    ~MvQAdvancedSearchDialog() override;

    void closeEvent(QCloseEvent* event) override;
    QString toRelPath(QString path);
    QString fromRelPath(QString path);
    QString formatFileSize(qint64 size) const;
    QString formatFileDate(time_t t) const;
    void writeSettings();
    void readSettings();

private:
    QWidget* caller_;

    MvQLineEdit* folderLe_;
    MvQLineEdit* nameLe_;
    MvQLineEdit* typeLe_;
    MvQLineEdit* contLe_;

    QCheckBox* contentsCaseCheck_;

    QStackedLayout* typeStacked_;
    QComboBox* typeCb_;
    QRadioButton* typeTextRb_;
    QRadioButton* typeListRb_;

    QCheckBox* subFolderCheck_;
    QCheckBox* linkCheck_;
    QCheckBox* sysCheck_;
    QCheckBox* caseCheck_;
    QTreeWidget* resTree_;
    MvQFind* finder_;
    QPushButton* findPb_;
    QPushButton* cancelPb_;
    QProgressBar* findProgress_;
    QLabel* statusMessageLabel_;

    QCheckBox* dateCheck_;
    QRadioButton* periodRb_;
    QDateEdit* fromDateE_;
    QDateEdit* toDateE_;

    QRadioButton* durationRb_;
    QSpinBox* durationSp_;
    QComboBox* durationCb_;

    QComboBox* sizeOperCb_;
    QSpinBox* sizeValSp_;
    QComboBox* sizeUnitsCb_;

    MvQLineEdit* ownerLe_;
    MvQLineEdit* groupLe_;

    QList<QWidget*> dateBoxTop_;
    QList<QWidget*> dateBoxBottom_;

    static MvQAdvancedSearchDialog* dialog_;
    static QList<const IconClass*> iconClasses_;


private:
    QWidget* buildMainTab();
    QWidget* buildContentsTab();
    QWidget* buildPropTab();
    void loadTypeCb();
};

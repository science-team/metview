/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQRequestPanelHelp.h"
#include "MvIconParameter.h"

#include <QListWidget>
#include <QMap>
#include <QString>

class RequestPanel;
class MvQFilterTreeWidget;

class MvQListHelp : public MvQRequestPanelHelp
{
    Q_OBJECT

public:
    MvQListHelp(RequestPanel& owner, const MvIconParameter& param);
    ~MvQListHelp() override = default;

    void start() override {}
    bool dialog() override { return false; }
    QWidget* widget() override;

public slots:
    void slotSelectionChanged(QString s, bool);

protected:
    void refresh(const std::vector<std::string>&) override;
    virtual long flags() { return 9; }
    virtual void set(Request&) {}

    bool multiple_;
    MvQFilterTreeWidget* selector_{nullptr};
};

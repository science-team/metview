/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Task.h"
#include "IconObject.h"
#include "Input.h"


class ShellTask : public Task, public Input
{
public:
    ShellTask(const std::string&, IconObject*);
    ~ShellTask() override;

    void addContext(const Request&) override {}

protected:
    void print(std::ostream&) const override;
    void Object(IconObjectH xx) { object_ = xx; }

    // From Task
    void start() override;
    // From Input
    void done(FILE*) override;

private:
    // No copy allowed
    ShellTask(const ShellTask&);
    ShellTask& operator=(const ShellTask&);

    IconObjectH object_;
    FILE* file_;
    std::string cmd_;

    // From Input
    void ready(const char*) override;
};

inline void destroy(ShellTask**) {}

/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QString>

#include "MvQApplication.h"
#include "MvRequest.h"
#include "MvProtocol.h"


class MvQDesktop : public MvQApplication
{
public:
    MvQDesktop(int& argc, char** argv, std::string);
    ~MvQDesktop() override;

    void init();
    void showBrowsers();

    // Processing the request
    virtual void serve(MvProtocol&, MvRequest&);
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Service.h"
#include "MetviewService.h"
#include "ShellService.h"

#include <map>

using Map = std::map<std::string, Service*>;
static Map* services = nullptr;

Service::Service(const std::string& name) :
    name_(name)
{
    if (services == nullptr)
        services = new Map();
    (*services)[name] = this;
}

Service::~Service() = default;

std::string Service::name()
{
    return name_;
}

Service* Service::find(const std::string& name)
{
    if (services) {
        auto j = services->find(name);
        if (j != services->end())
            return (*j).second;
    }

    if (name[0] == '(')
        return new ShellService(name);
    else
        return new MetviewService(name);

    return nullptr;
}

void Service::print(std::ostream& s) const
{
    s << name_;
}

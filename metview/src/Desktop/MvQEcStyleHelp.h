/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQRequestPanelHelp.h"
#include "MvIconParameter.h"

#include <QPen>
#include <QWidget>
#include <QAbstractItemModel>

class QComboBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QSlider;
class QSpinBox;
class QTabWidget;
class QToolButton;
class QTreeView;
class QSortFilterProxyModel;

class MvQStyleDbItem;
class RequestPanel;
class MvQStyleTreeWidget;

// Model to dislay/select the suites
class MvQEcStyleModel : public QAbstractItemModel
{
public:
    enum CustomItemRole
    {
        SortRole = Qt::UserRole + 1
    };

    explicit MvQEcStyleModel(QObject* parent = nullptr);
    ~MvQEcStyleModel() override;

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    void setIndexFilter(QList<int>);
    void clearFilter();

protected:
    bool isFiltered(int) const;

    QList<int> indexFilter_;
};

class MvQEcStyleSelectionWidget : public QWidget
{
    Q_OBJECT
public:
    MvQEcStyleSelectionWidget(QWidget* parent = nullptr);
    void setCurrent(const std::string&, const std::string& layerName);

protected slots:

    void slotItemSelected(const QModelIndex&, const QModelIndex&);
    void slotClearFilter();

signals:
    void itemSelected(int);

protected:
    void checkButtonState();

    MvQStyleTreeWidget* browser_;
    MvQEcStyleModel* model_;
    QSortFilterProxyModel* sortModel_;
    std::string layerName_;
};

class MvQEcStyleHelp : public MvQRequestPanelHelp
{
    Q_OBJECT

public:
    MvQEcStyleHelp(RequestPanel& owner, const MvIconParameter& param);
    ~MvQEcStyleHelp() override = default;

    void start() override {}
    bool dialog() override { return false; }
    QWidget* widget() override { return selector_; }

public slots:
    void slotSelected(int);

protected:
    void refresh(const std::vector<std::string>&) override;

private:
    MvQEcStyleSelectionWidget* selector_;
    QString oriName_;
};

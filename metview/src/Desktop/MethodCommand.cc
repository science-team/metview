/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MethodCommand.h"
#include "IconObject.h"

MethodCommand::MethodCommand(const std::string& name, Proc proc) :
    Command(name),
    proc_(proc)
{
}

MethodCommand::~MethodCommand() = default;

void MethodCommand::execute(IconObject* o)
{
    (o->*proc_)();
}

static MethodCommand editCmd("edit", &IconObject::edit);
// static MethodCommand copyCmd("copy",&IconObject::duplicate);
// static MethodCommand cutCmd("cut",&IconObject::duplicate);
static MethodCommand duplicateCmd("duplicate", &IconObject::duplicate);
static MethodCommand deleteCmd("delete", &IconObject::toWastebasket);
static MethodCommand infoCmd("log", &IconObject::showLog);
static MethodCommand emptyCmd("empty", &IconObject::empty);
static MethodCommand destroyCmd("destroy", &IconObject::destroy);
static MethodCommand clearCmd("clear", &IconObject::modified);

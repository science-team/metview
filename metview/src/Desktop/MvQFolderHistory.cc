/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQFolderHistory.h"

#include <QAction>
#include <QMenu>

#include "MvQContextMenu.h"
#include "MvQIconProvider.h"

#include "Folder.h"

QList<MvQFolderHistory*> MvQFolderHistory::fhLst_;
QStringList MvQFolderHistory::items_;

MvQFolderHistory::MvQFolderHistory(QMenu* menu) :
    maxNum_(30),
    menu_(menu),
    prefix_("mv:")
{
    if (!menu_)
        return;

    connect(menu_, SIGNAL(triggered(QAction*)),
            this, SLOT(slotSelected(QAction*)));

    menu_->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(menu_, SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(slotContextMenu(const QPoint&)));

    init();

    fhLst_ << this;
}

MvQFolderHistory::~MvQFolderHistory()
{
    fhLst_.removeOne(this);
}

void MvQFolderHistory::init()
{
    updateMenu();
}

void MvQFolderHistory::init(QStringList lst)
{
    items_.clear();
    Q_FOREACH (QString s, lst) {
        if (!s.simplified().isEmpty() &&
            s.simplified() != ".") {
            if (Folder::folder(s.toStdString(), false) != nullptr)
                items_ << s;
        }
    }

    foreach (MvQFolderHistory* f, fhLst_) {
        f->init();
    }
}

void MvQFolderHistory::add(QString fullName)
{
    if (fullName.simplified().isEmpty())
        return;

    if (!items_.isEmpty() && items_[0] == fullName)
        return;

    if (items_.contains(fullName)) {
        items_.removeAll(fullName);
    }

    if (items_.count() < maxNum_) {
        if (items_.count() == 0) {
            items_ << fullName;
        }
        else {
            items_.prepend(fullName);
        }
    }
    else {
        while (items_.count() >= maxNum_) {
            items_.removeLast();
        }

        items_.prepend(fullName);
    }

    foreach (MvQFolderHistory* f, fhLst_) {
        f->updateMenu();
    }
}

void MvQFolderHistory::updateMenu()
{
    menu_->clear();
    foreach (QString s, items_) {
        menu_->addAction(createAction(s));
    }
}


QString MvQFolderHistory::path(QAction* action)
{
    if (!action)
        return {};

    return action->data().toString();
}

QAction* MvQFolderHistory::createAction(QString fullName)
{
    // Home
    auto* action = new QAction(menu_);
    // action->setText("<font color=\"#b4bbb2\">" + prefix_ +"</font>" + fullName);
    // action->setText(prefix_ + fullName);
    action->setText(fullName);
    action->setData(fullName);

    auto* folder = Folder::folder(fullName.toStdString(), false);
    if (folder) {
        action->setIcon(MvQIconProvider::pixmap(folder, 24));
    }
    return action;
}


void MvQFolderHistory::slotSelected(QAction* ac)
{
    if (ac)
        emit itemSelected(path(ac));
}

void MvQFolderHistory::slotContextMenu(const QPoint& pos)
{
    if (menu_) {
        static MvQContextItemSet cmItems("FolderHistory");

        QAction* ac = menu_->actionAt(pos);
        QString pt = path(ac);
        if (pt.isEmpty())
            return;

        QString selection = MvQContextMenu::instance()->exec(cmItems.icon(), menu_->mapToGlobal(pos), menu_);
        if (!selection.isEmpty())
            emit commandRequested(selection, pt);
    }
}

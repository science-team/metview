/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQTemplateDrawer.h"

#include "EditorDrawerFactory.h"
#include "IconObject.h"
#include "IconClass.h"
#include "Folder.h"

#include "MvQEditor.h"
#include "MvQContextMenu.h"
#include "MvQFolderModel.h"

MvQTemplateDrawer::MvQTemplateDrawer(MvQEditor* editor) :
    EditorDrawer(editor)
{
    std::vector<std::string> templateClasses;
    templateClasses.push_back(editor->iconClass().name());

    model_ = new MvQFolderModel(this);

    view_ = new MvQTemplateDrawerView(model_);
    // view_->setHelper(true);
    view_->setMinimumHeight(1);
    view_->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);

    model_->setAcceptedClasses(templateClasses);

    Folder* f = Folder::folder("templates",
                               editor_->iconClass().defaultName());

    model_->setFolder(f);

    connect(view_, SIGNAL(iconCommandRequested(QString, IconObjectH)),
            this, SLOT(slotCommand(QString, IconObjectH)));
}

QString MvQTemplateDrawer::name()
{
    return tr("Templates");
}

QWidget* MvQTemplateDrawer::widget()
{
    return view_;
}

void MvQTemplateDrawer::slotCommand(QString name, IconObjectH obj)
{
    if (name == "mergeIcon") {
        editor_->merge(obj);
    }
    else if (name == "replaceIcon") {
        editor_->replace(obj);
    }
}


MvQTemplateDrawerView::MvQTemplateDrawerView(MvQFolderModel* folderModel, QWidget* parent) :
    MvQIconStripView(folderModel, parent)
{
}

MvQTemplateDrawerView::~MvQTemplateDrawerView()
{
    // delete filterModel_;
}

MvQContextItemSet* MvQTemplateDrawerView::cmSet()
{
    static MvQContextItemSet cmItems("TemplateDrawerView");
    return &cmItems;
}

static EditorDrawerMaker<MvQTemplateDrawer, MvQEditor> maker(1);
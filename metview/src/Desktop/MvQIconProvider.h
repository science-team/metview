/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <map>
#include <vector>
#include <string>

#include <QImage>
#include <QMap>
#include <QPixmap>

class Folder;
class IconClass;
class IconObject;

class MvQIcon
{
public:
    MvQIcon(QString);
    virtual QPixmap pixmap(int);
    QPixmap pixmap(IconObject*, int, bool checkEmbedded = false);
    QPixmap greyedOutPixmap(IconObject*, int);

protected:
    static void greyOut(QImage&);

    QString path_;
    std::map<int, QPixmap> pixmaps_;
    std::map<int, QPixmap> fadedPixmaps_;
};

class MvQUnknownIcon : public MvQIcon
{
public:
    MvQUnknownIcon(QString);
    QPixmap pixmap(int) override;
};

class MvQIconProvider
{
public:
    MvQIconProvider();

    static QPixmap pixmap(IconObject*, int, bool checkEmbedded = false);
    static QPixmap pixmap(const IconClass&, int);
    static QPixmap pixmap(const std::string& type, int size);
    static QPixmap greyedOutPixmap(IconObject*, int);

    static QPixmap bookmarkPixmap(std::string, int);
    static QPixmap lockPixmap(int);
    static QPixmap warningPixmap(int);
    static QPixmap homePixmap(int);

    static int defaultSize() { return defaultSize_; }
    static const std::vector<int>& sizes() { return sizes_; }

private:
    static MvQIcon* icon(const IconClass&);
    static MvQIcon* icon(const std::string& kind);

    static std::vector<int> sizes_;
    static int defaultSize_;
    static std::map<std::string, MvQIcon*> icons_;
    static std::map<std::string, Folder*> folders_;
};

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQRequestPanelLine.h"
#include "MvIconParameter.h"

#include <QPixmap>
#include <QString>

class QColor;
class QComboBox;

class RequestPanel;

class MvQColourLine : public MvQRequestPanelLine, public ParameterScanner
{
    Q_OBJECT

public:
    MvQColourLine(RequestPanel& owner, const MvIconParameter& param);
    ~MvQColourLine() override = default;

    QString currentValue() { return {}; }
    void addValue(QString) {}

    void refresh(const std::vector<std::string>&) override;

public slots:
    void slotHelperEdited(const std::vector<std::string>&) override;
    void slotHelperEditConfirmed() override;

protected slots:
    void slotCurrentChanged(int);
    void slotCopyColour();
    void slotCopyRgb();
    void slotCopyHsl();
    void slotPasteColour();

protected:
    void next(const MvIconParameter&, const char* first, const char* second) override;
    void addColour(QColor, QString, QString, bool setCurrent = false);
    void setCustomColour(QColor, QString, QString, bool setCurrent = false);
    void updateHelper();
    void setColour(int, QColor);

    QComboBox* colCb_;
    QPixmap pix_;
    int customColIndex_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MacroGuiObject.h"

static std::string next_name()
{
    static int i = 0;
    char buf[80];
    sprintf(buf, "macro_lang%04d", i++);
    return buf;
}


MacroGuiObject::MacroGuiObject(const Request& lang, const IconClass* super) :
    IconClass(next_name(), empty_request(""), super),
    lang_(this, lang),
    maker_(name())
{
}

MacroGuiObject::~MacroGuiObject() = default;

MvIconLanguage& MacroGuiObject::language() const
{
    auto* c = const_cast<MacroGuiObject*>(this);
    return c->lang_;
}

std::string MacroGuiObject::editor() const
{
    return "TemporaryEditor";
}

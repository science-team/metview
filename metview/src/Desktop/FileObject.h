/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "IconObject.h"

#include <set>
#include <string>


class FileObject : public IconObject
{
public:
    FileObject(Folder* parent,
               const IconClass& kind, const std::string& name,
               IconInfo* info);
    FileObject(const FileObject&) = delete;
    FileObject& operator=(const FileObject&) = delete;

    ~FileObject() override = default;

protected:
    bool recheckIconClass() override;

private:
    void doubleClick() override;
    std::set<std::string> can() override;

    void createFiles() override;
    Request request() const override;
    void setRequest(const Request&) override;
};

inline void destroy(FileObject**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(FileObject);
//#endif

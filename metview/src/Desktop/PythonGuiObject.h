/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Metview.h"
#include "IconClass.h"
#include "MvIconLanguage.h"
#include "StandardObject.h"
#include "IconFactory.h"

class PythonGuiObject : public IconClass
{
public:
    PythonGuiObject(const Request& lang, const IconClass* = nullptr);
    ~PythonGuiObject() override;

private:
    // No copy allowed
    PythonGuiObject(const PythonGuiObject&);
    PythonGuiObject& operator=(const PythonGuiObject&);

    MvIconLanguage& language() const override;
    std::string editor() const override;

    MvIconLanguage lang_;
    IconMaker<StandardObject> maker_;
};

inline void destroy(PythonGuiObject**) {}

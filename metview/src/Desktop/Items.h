/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <vector>

#include "Metview.h"

class IconObject;

class Items
{
public:
    Items(request*);
    ~Items();

    static void load(request*);
    static void createAll();
    static IconObject* find(const std::string&);
    static void objects(std::vector<IconObject*>&);

private:
    Items(const Items&);
    Items& operator=(const Items&);

    IconObject* object();

    request* r_;
    IconObject* object_;
};

inline void destroy(Items**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(Items);
//#endif

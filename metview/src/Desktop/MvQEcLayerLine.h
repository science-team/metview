/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQRequestPanelLine.h"
#include "MvIconParameter.h"

#include <QPixmap>
#include <QSet>
#include <QString>

class QColor;
class QComboBox;

class RequestPanel;
class MvQStyleDb;

class MvQEcLayerLine : public MvQRequestPanelLine, public ParameterScanner
{
    Q_OBJECT

public:
    MvQEcLayerLine(RequestPanel& owner, const MvIconParameter& param);
    ~MvQEcLayerLine() override = default;

    void refresh(const std::vector<std::string>&) override;
    void widgetClicked();

public slots:
    void slotHelperEdited(const std::vector<std::string>&) override;

protected:
    void next(const MvIconParameter&, const char* first, const char* second) override;
    void updateHelper();

    QPixmap pix_;
    QSize pixSize_;
    QLabel* pixLabel_;
    QLabel* nameLabel_;
};

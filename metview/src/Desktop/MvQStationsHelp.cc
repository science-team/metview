/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQStationsHelp.h"

#include <QDebug>

#include "HelpFactory.h"
#include "RequestPanel.h"

#include "MvQStationsWidget.h"


//==================================================
//
//  MvQStationsHelp
//
//==================================================

MvQStationsHelp::MvQStationsHelp(RequestPanel& owner, const MvIconParameter& param) :
    MvQRequestPanelHelp(owner, param),
    selector_(nullptr)
{
    Request req = param.interfaceRequest();
    const char* type = req("help_param");

    if (!type)
        return;

    MvQStationsWidget::SelectionMode mode = MvQStationsWidget::NameMode;
    if (strcmp(type, "NAME") == 0)
        mode = MvQStationsWidget::NameMode;
    else if (strcmp(type, "IDENT") == 0)
        mode = MvQStationsWidget::IdMode;
    else if (strcmp(type, "WMO_BLOCK") == 0)
        mode = MvQStationsWidget::WmoMode;


    selector_ = new MvQStationsWidget(mode, parentWidget_);

    connect(selector_, SIGNAL(selected(QString)),
            this, SLOT(slotSelected(QString)));
}

QWidget* MvQStationsHelp::widget()
{
    return selector_;
}

void MvQStationsHelp::slotSelected(QString txt)
{
    std::vector<std::string> vs;

    vs.push_back(txt.toStdString());

    emit edited(vs);
}

void MvQStationsHelp::refresh(const std::vector<std::string>& values)
{
    if (values.size() == 0)
        return;

    /*if(MvQPalette::toString(selector_->currentColour()) == values[0])
        return;

    QColor col=MvQPalette::magics(values[0]);
    if(col.isValid())
    {
        selector_->setColour(col);
    }*/
}

static HelpMaker<MvQStationsHelp> maker("help_external");

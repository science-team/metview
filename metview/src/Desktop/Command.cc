/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Command.h"
//#include "Log.h"

#include <string>
#include <map>
#include <iostream>

static std::map<std::string, Command*>* actions = nullptr;

Command::Command(const std::string& name) :
    name_(name)
{
    if (actions == nullptr)
        actions = new std::map<std::string, Command*>;

    (*actions)[name] = this;
}

Command::~Command() = default;


void Command::execute(const std::string& name, IconObject* p)
{
    auto j = actions->find(name);
    if (j == actions->end())
        // Log::error(p) << name << ": action not found" << std::endl;
        std::cout << name << ": action not found" << std::endl;
    else
        (*j).second->execute(p);
}

bool Command::isValid(const std::string& name)
{
    auto j = actions->find(name);
    return (j != actions->end());
}

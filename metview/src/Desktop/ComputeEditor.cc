/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "ComputeEditor.h"

#include <QLabel>
#include <QVBoxLayout>

#include "MvQLineEdit.h"

#include "EditorFactory.h"
#include "IconObject.h"
#include "Request.h"

ComputeEditor::ComputeEditor(const IconClass& kind, const std::string& name) :
    MvQEditor(kind, name)
{
    auto* label = new QLabel(tr("<b>Formula:<b>"), this);
    centralLayout_->addWidget(label, 0);

    te_ = new MvQLineEdit(this);
    centralLayout_->addWidget(te_, 0);
    centralLayout_->addStretch(1);

    // std::vector<std::string> classes;
    // classes.push_back(kind.name());

    // connect(w,SIGNAL(iconDropped(IconObject*)),
    //	 this,SLOT(slotIconDropped(IconObject*)));

    connect(te_, SIGNAL(textChanged()),
            this, SLOT(slotTextChanged()));
}

ComputeEditor::~ComputeEditor() = default;

void ComputeEditor::apply()
{
    Request r("COMPUTE");
    r("FORMULA") = te_->text().toStdString().c_str();
    r.save(current_->path());
}

void ComputeEditor::reset()
{
    Request r = current_->request();
    const char* f = r("FORMULA");

    QString str;
    if (f)
        str = QString(f);

    te_->setText(str);

    // modified_ = false;
}

void ComputeEditor::close()
{
    te_->clear();
}

void ComputeEditor::merge(IconObjectH)
{
    /*XMText text(text_);

    string s = current_->relativeName(o);
    bool quote = false;


    //for(string::iterator i = s.begin(); i != s.end(); ++i)
    for(int it=0; it<s.length(); ++it)
    {
        //if(i == s.begin() &&  *i != '_' && !isalpha(*i)) quote = true;
        if(it == 0 &&  s[it] != '_' && !isalpha(s[it]))
            quote = true;

        //if(*i != '_' && !isalnum(*i)) quote = true;
        if(s[it] != '_' && !isalnum(s[it]))
            quote = true;
    }


    if(quote)
        s = std::string("\'") + s + std::string("\'");
    text.insert(s);*/
}

void ComputeEditor::replace(IconObjectH)
{
}

void ComputeEditor::slotTextChanged()
{
    changed();
}

static EditorMaker<ComputeEditor> editorMaker("ComputeEditor");

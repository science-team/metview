/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <map>
#include <set>
#include <string>
#include <vector>

#include "Metview.h"
#include "Action.h"
#include "MvIconClassCore.h"

class Service;
class IconObject;
class Folder;
class Path;
class MvIconLanguage;


class IconClass;
struct ClassScanner
{
    virtual void next(const IconClass&) = 0;
};

class IconClass : public MvIconClassCore
{
public:
    IconClass(const std::string&, request*, const IconClass* = nullptr);
    ~IconClass() override;

    virtual Service* service(const Action&) const;
    virtual void service(const Action&, const IconClass*, Service*) const;
    virtual std::set<std::string> can() const;

    virtual IconObject* defaultObject() const;
    virtual IconObject* createOne(Folder*) const;
    virtual IconObject* createOneFromRequest(Folder*) const;

    virtual IconObject* createOne(Folder*, int x, int y) const;
    virtual IconObject* createOneFromRequest(Folder*, int x, int y) const;

    virtual bool isSubClassOf(const IconClass&) const;
    virtual bool canBecome(const IconClass&) const;

    // virtual MvIconLanguage& language() const;

    virtual int priority() const;

    static bool isValid(const std::string& name);
    static const IconClass& find(const std::string&);
    static const IconClass& find(const std::string& name, const std::string& defaultType, bool);
    static void find(const std::string&, std::vector<const IconClass*>&, bool onlyCreatable = true);
    static void load(request*);
    static void globalService(const std::string&, Service*);

    static void scan(ClassScanner&);

    static const IconClass& guess(const Path&);
    static const IconClass& guess(const std::string&);
    static void guess(const std::string& fullName, std::string& icName, std::string& icType);

    // Uncomment for persistent, remove otherwise
    // static os_typespec* get_os_typespec();
private:
    // No copy allowed
    IconClass(const IconClass&);
    IconClass& operator=(const IconClass&);
    std::set<std::string> generateCan() const;

    const IconClass* super_;
    // std::string name_;
    // request* request_;

    std::map<Action, Service*> services_;
    std::set<const IconClass*> outputs_;
    mutable std::set<std::string> can_;

    // friend std::ostream& operator<<(std::ostream& s,const IconClass& p)
    //     { p.print(s); return s; }
};

inline void destroy(IconClass**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(IconClass);
//#endif

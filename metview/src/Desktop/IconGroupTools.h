/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Metview.h"
#include "IconObject.h"
#include "ReplyObserver.h"
#include "Task.h"
#include "TaskObserver.h"

class IconGroupTools;

class IconGroupTask : public Task, public ReplyObserver //, public TaskObserver
{
public:
    IconGroupTask(const std::string& service, const MvRequest& r);
    ~IconGroupTask() override = default;

    void addContext(const Request&) override {}

//protected:
//    void print(std::ostream&) const override;

private:
    std::string service_;
    MvRequest request_;

    // From Task
    void start() override;

    // From ReplyObserver
    void reply(const Request&, int) override;
    void progress(const Request&) override {}
    void message(const std::string&) override {}
};

class IconGroupTools : public TaskObserver
{
public:
    IconGroupTools(request*);
    ~IconGroupTools();
    IconGroupTools(const IconGroupTools&) = delete;
    IconGroupTools& operator=(const IconGroupTools&) = delete;

    std::string name();
    static void load(request*);
    static void run(const std::string&, const std::vector<IconObject*>& items);
    static bool can(const std::string&);

    // from TaskObserver
    void success(Task*, const Request&) override;
    void failure(Task*, const Request&) override;

private:
    bool popupOnError() const;
    void run(const std::vector<IconObject*>& items);
    void parseReply(const Request& r, std::string& errStr, std::vector<std::string>& failedItems,
                                    std::vector<std::string>& itemErrors) const;

    request* request_;
};

inline void destroy(IconGroupTools**) {}

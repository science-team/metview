/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Service.h"

class InternalService : public Service
{
public:
    InternalService();
    ~InternalService() override;

private:
    // No copy allowed
    InternalService(const InternalService&);
    InternalService& operator=(const InternalService&);

    Task* task(const Action&, IconObject*) override;
};

inline void destroy(InternalService**) {}

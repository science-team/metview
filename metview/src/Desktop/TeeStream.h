/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <ostream>


class TeeStream : public std::ostream
{
public:
    TeeStream(std::ostream&, std::ostream&);
    ~TeeStream() override;

private:
    // No copy allowed
    TeeStream(const TeeStream&);
    TeeStream& operator=(const TeeStream&);
};

inline void destroy(TeeStream**) {}

// If persistent, uncomment, otherwise remove
//#ifdef _ODI_OSSG_
// OS_MARK_SCHEMA_TYPE(TeeStream);
//#endif

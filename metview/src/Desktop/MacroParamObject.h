/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "StandardObject.h"

#include <string>

#include "MacroParamState.h"

class MacroParamObject : public StandardObject
{
public:
    MacroParamObject(Folder* parent,
                     const IconClass& kind, const std::string& name,
                     IconInfo* info);
    ~MacroParamObject() override;

    MacroParamState* state() const;
    void state(MacroParamState*);

private:
    // No copy allowed
    MacroParamObject(const MacroParamObject&);
    MacroParamObject& operator=(const MacroParamObject&);

    MvIconLanguage& language() const override;
    Request fullRequest() const override;
    void setRequest(const Request&) override;
    Request request() const override;

    MacroParamStateH state_;
};

inline void destroy(MacroParamObject**) {}

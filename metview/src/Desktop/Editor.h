/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once


#include <string>

class IconObject;
class IconClass;
class EditorDrawer;
class EditorTool;
class EditorObserver;

#include "IconObject.h"
#include "IconObserver.h"

class Editor : public IconObserver
{
public:
    ~Editor() override;

    const IconClass& iconClass();
    const std::string& kind();
    IconObject* current();
    void observer(EditorObserver*);

    void edit(IconObject*);
    virtual void editDone();

    void notifyObserverApply();
    void notifyObserverClose();

    virtual void empty();
    virtual void temporary();
    virtual std::string alternateEditor();
    virtual void changed() = 0;
    virtual void raiseIt() = 0;
    virtual void closeIt() = 0;

    static void open(IconObject*);

protected:
    Editor(const IconClass&, const std::string& kind);
    virtual void applyTemporayRules() {}

    const IconClass& class_;
    std::string kind_;
    EditorObserver* observer_;
    IconObjectH current_;
    bool temporary_;

    virtual void edit() = 0;
    virtual void showIt() = 0;

private:
    explicit Editor(const Editor&);
    Editor& operator=(const Editor&);
};

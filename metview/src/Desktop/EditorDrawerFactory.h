/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <map>
#include <string>

// UKMO/HP/aCC:
#ifdef hp
#include <MvQEditor.h>
#endif

class EditorDrawer;
class MvQEditor;

class EditorDrawerFactory
{
public:
    EditorDrawerFactory(int);
    virtual ~EditorDrawerFactory();

    virtual EditorDrawer* make(MvQEditor*) = 0;
    static std::map<std::string, EditorDrawer*> create(MvQEditor*);

private:
    // No copy allowed
    EditorDrawerFactory(const EditorDrawerFactory&);
    EditorDrawerFactory& operator=(const EditorDrawerFactory&);
};

template <class T, class E>
class EditorDrawerMaker : public EditorDrawerFactory
{
    EditorDrawer* make(MvQEditor* e) override
    {
        E* a = dynamic_cast<E*>(e);
        return a ? new T(a) : 0;
    }

public:
    EditorDrawerMaker(int n) :
        EditorDrawerFactory(n) {}
};

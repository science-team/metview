/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQTemporaryEditor.h"

#include "RequestPanel.h"
#include "MvIconLanguage.h"

class QScrollArea;
class RequestPanel;

class TemporaryEditor : public MvQTemporaryEditor
{
public:
    TemporaryEditor(const IconClass&, const std::string&);
    ~TemporaryEditor() override;

private:
    TemporaryEditor(const TemporaryEditor&);
    TemporaryEditor& operator=(const TemporaryEditor&);

    void apply() override;
    void reset() override;
    void close() override;
    void edit() override;

    virtual IconObject* copy(const std::string&);

    RequestPanel* panel_;
    QScrollArea* panelArea_;
};

inline void destroy(TemporaryEditor**) {}

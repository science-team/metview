/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQIconObjectModel.h"

#include <QDebug>

#include "IconClass.h"
#include "IconObject.h"


//====================================
//
//	MvQIconObjectModel
//
//====================================

MvQIconObjectModel::MvQIconObjectModel(QObject* parent) :
    QAbstractItemModel(parent)
{
}

void MvQIconObjectModel::update()
{
    beginResetModel();
    endResetModel();
}


int MvQIconObjectModel::columnCount(const QModelIndex& parent) const
{
    return parent.isValid() ? 0 : 3;
}


int MvQIconObjectModel::rowCount(const QModelIndex& index) const
{
    if (!index.isValid()) {
        return static_cast<int>(IconObject::objects().size());
    }
    else {
        return 0;
    }
}

QVariant MvQIconObjectModel::data(const QModelIndex& index, int role) const
{
    if (role != Qt::DisplayRole && role != Qt::DecorationRole) {
        return {};
    }

    if (!index.isValid())
        return {};

    IconObjectH obj = nullptr;
    if (index.row() >= 0 && index.row() < static_cast<int>(IconObject::objects().size()))
        obj = IconObject::objects().at(index.row());

    if (!obj)
        return {};

    if (index.column() == 0) {
        if (role == Qt::DisplayRole)
            return QString::fromStdString(obj->name());
    }
    else if (index.column() == 1) {
        if (role == Qt::DisplayRole)
            return QString::fromStdString(obj->className());
    }
    //====================================
    //
    //	MvQIconObjectModel
    //
    //====================================
    else if (index.column() == 2) {
        if (role == Qt::DisplayRole)
            return QString::number(obj->count());
    }

    return {};
}

QModelIndex MvQIconObjectModel::index(int row, int column, const QModelIndex& /*parent */) const
{
    return createIndex(row, column, (void*)nullptr);
}


QModelIndex MvQIconObjectModel::parent(const QModelIndex& /*index*/) const
{
    return {};
}

QVariant MvQIconObjectModel::headerData(const int section, const Qt::Orientation orient, const int role) const
{
    if (orient != Qt::Horizontal || role != Qt::DisplayRole)
        return {};

    if (section == 0)
        return "Name";
    else if (section == 1)
        return "Class";
    else if (section == 2)
        return "Count";

    return {};
}

/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QColor>
#include <QStyledItemDelegate>

class MvQEcLayerDbItem;
class QTreeView;
class QTextEdit;
class QSplitter;

class MvQEcLayerDelegate : public QStyledItemDelegate
{
public:
    MvQEcLayerDelegate(QWidget* parent = nullptr);
    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const override;
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const override;

protected:
    QColor borderCol_;
    int textHeight_;
    int pixHeight_;
};


class MvQEcLayerTreeWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MvQEcLayerTreeWidget(QWidget* parent = nullptr);

    void setLayer(MvQEcLayerDbItem* item);
    QTreeView* tree() const { return tree_; }

protected slots:
    void slotCopyLayerName();

protected:
    QSplitter* splitter_;
    QTreeView* tree_;
    QTextEdit* te_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Protocol.h"

#include <vector>

static std::vector<Protocol*>* all = nullptr;

Protocol::Protocol()
{
    if (all == nullptr)
        all = new std::vector<Protocol*>();
    all->push_back(this);
}

Protocol::~Protocol() = default;

void Protocol::make()
{
}

void Protocol::init()
{
    for (auto& j : *all)
        j->make();
}

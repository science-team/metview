object,
    class           = MXSECTION,
    icon_box        = Data processing,
    can_be_created  = True,
    definition_file = '$METVIEW_DIR_SHARE/etc/MXSectionDef',
    rules_file      = '$METVIEW_DIR_SHARE/etc/MXSectionRules',
    default_name    = Cross Section Data,
    help_page       = Cross+Section+Data,
    type            = Data,
    expand          = 512, #expand default values
    macro           = mcross_sect,
    editor_type     = SimpleEditor,
    pixmap          = '$METVIEW_DIR_SHARE/icons/MXSECTION.icon'

object,
    class           = MXAVERAGE,
    icon_box        = Data processing,
    can_be_created  = True,
    definition_file = '$METVIEW_DIR_SHARE/etc/MXAverageDef',
    rules_file      = '$METVIEW_DIR_SHARE/etc/MXAverageRules',
    default_name    = Average Data,
    help_page       = Average+Data,
    type            = Data,
    expand          = 512, #expand default values
    macro           = mxs_average,
    editor_type     = SimpleEditor,
    pixmap          = '$METVIEW_DIR_SHARE/icons/MXAVERAGE.icon'

object,
    class           = MVPROFILE,
    icon_box        = Data processing,
    can_be_created  = True,
    definition_file = '$METVIEW_DIR_SHARE/etc/MVProfileDef',
    rules_file      = '$METVIEW_DIR_SHARE/etc/MVProfileRules',
    default_name    = Vertical Profile Data,
    help_page       = Vertical+Profile+Data,
    type            = Data,
    expand          = 520, #EXPAND_DEFAULTS|EXPAND_2ND_NAME
    macro           = mvert_prof,
    editor_type     = SimpleEditor,
    pixmap          = '$METVIEW_DIR_SHARE/icons/MVPROFILE.icon'

state,
    class        = MXSECTION/MXAVERAGE,
    action       = visualise/execute/examine/save/hardcopy/prepare/drop,
    output_class = NETCDF,
    service      = XSection

state,
    class        = MVPROFILE,
    action       = visualise/execute/examine/save/hardcopy/prepare/drop,
    output_class = NETCDF/NETCDF_RTTOV_INPUT,
    service      = XSection

service,
    timeout  = $timeout,
    name     = 'XSection',
    fullname = Cross Section,
    cmd      = "$metview_command $METVIEW_BIN/XSection"

object,
    class               = MXSECTIONVIEW,
    can_be_created      = True,
    icon_box            = Views,
    definition_file     = '$METVIEW_DIR_SHARE/etc/MXSectionViewDef',
    rules_file          = '$METVIEW_DIR_SHARE/etc/MXSectionViewRules',
    default_name        = Cross Section View,
    help_page           = Cross+Section+View,
    type                = View,
    macro               = mxsectview,
    editor_type         = SimpleEditor,
    pixmap              = '$METVIEW_DIR_SHARE/icons/MXSECTIONVIEW.icon',
    expand              = 512, #expand default values
    doubleclick_method  = Edit

object,
    class               = MXAVERAGEVIEW,
    can_be_created      = True,
    icon_box            = Views,
    definition_file     = '$METVIEW_DIR_SHARE/etc/MXAverageViewDef',
    rules_file          = '$METVIEW_DIR_SHARE/etc/MXAverageViewRules',
    default_name        = Average View,
    help_page           = Average+View,
    type                = View,
    macro               = maverageview,
    editor_type         = SimpleEditor,
    pixmap              = '$METVIEW_DIR_SHARE/icons/MXAVERAGEVIEW.icon',
    expand              = 512, #expand default values
    doubleclick_method  = Edit

object,
    class               = MVPROFILEVIEW,
    can_be_created      = True,
    icon_box            = Views,
    definition_file     = '$METVIEW_DIR_SHARE/etc/MVProfileViewDef',
    rules_file          = '$METVIEW_DIR_SHARE/etc/MVProfileViewRules',
    default_name        = Vertical Profile View,
    help_page           = Vertical+Profile+View,
    type                = View,
    macro               = mvertprofview,
    editor_type         = SimpleEditor,
    pixmap              = '$METVIEW_DIR_SHARE/icons/MVPROFILEVIEW.icon',
    expand              = 512, #expand default values
    doubleclick_method  = Edit

state,
   class      = MXSECTIONVIEW/MXAVERAGEVIEW/MVPROFILEVIEW,
   action     = execute/visualise,
   service    = uPlotManager

# These are the Metview 3 icons (temporarily available for backwards compatibility)

object,
   class           = PM_XSECT,
   icon_box        = To be organised,
   can_be_created  = False,
   definition_file = '$METVIEW_DIR_SHARE/etc/XsectDef',
   rules_file      = '$METVIEW_DIR_SHARE/etc/XsectRules',
   default_name    = Cross Section Data,
   help_page       = Cross_Section_Data,
   type            = Data,
   expand          = 512, #expand default values
   macro           = cross_sect,
   editor_type     = SimpleEditor,
   pixmap          = '$METVIEW_DIR_SHARE/icons/XSECT.icon',
   obsolete        = True

object,
   class           = PM_VPROF,
   icon_box        = To be organised,
   can_be_created  = False,
   definition_file = '$METVIEW_DIR_SHARE/etc/VertProfDef',
   rules_file      = '$METVIEW_DIR_SHARE/etc/VertProfRules',
   default_name    = Vertical Profile Data,
   help_page       = Vertical_Profile_Data,
   type            = Data,
   expand          = 512, #expand default values
   macro           = vert_prof,
   editor_type     = SimpleEditor,
   pixmap          = '$METVIEW_DIR_SHARE/icons/VPROF.icon',
   obsolete        = True

object,
   class           = PM_AVERAGE,
   icon_box        = To be organised,
   can_be_created  = False,
   definition_file = '$METVIEW_DIR_SHARE/etc/AverageDef',
   rules_file      = '$METVIEW_DIR_SHARE/etc/AverageRules',
   default_name    = Average Data,
   help_page       = Average_Data,
   type            = Data,
   expand          = 512, #expand default values
   macro           = xs_average,
   editor_type     = SimpleEditor,
   pixmap          = '$METVIEW_DIR_SHARE/icons/AVERAGE.icon',
   obsolete        = True

state,
   class        = PM_XSECT/PM_AVERAGE/PM_VPROF,
   action       = visualise/execute/examine/save/hardcopy/prepare/drop,
   output_class = NETCDF,
   service      = XSection

object,
    class               = XSECTVIEW,
    can_be_created      = False,
    icon_box            = Views,
    definition_file     = '$METVIEW_DIR_SHARE/etc/XsectViewDef',
    rules_file          = '$METVIEW_DIR_SHARE/etc/XsectViewRules',
    default_name        = Cross Section View,
    help_page           = Cross_Section_View,
    type                = View,
    macro               = xsectview,
    editor_type         = SimpleEditor,
    pixmap              = '$METVIEW_DIR_SHARE/icons/XSECTVIEW.icon',
    expand              = 512, #expand default values
    doubleclick_method  = Edit,
    obsolete            = True

object,
    class               = AVERAGEVIEW,
    can_be_created      = False,
    icon_box            = Views,
    definition_file     = '$METVIEW_DIR_SHARE/etc/AverageViewDef',
    rules_file          = '$METVIEW_DIR_SHARE/etc/AverageViewRules',
    default_name        = Average View,
    help_page           = Average_View,
    type                = View,
    macro               = averageview,
    editor_type         = SimpleEditor,
    pixmap              = '$METVIEW_DIR_SHARE/icons/AVERAGEVIEW.icon',
    expand              = 512, #expand default values
    doubleclick_method  = Edit,
    obsolete            = True

object,
    class               = VERTPROFVIEW,
    can_be_created      = False,
    icon_box            = Views,
    definition_file     = '$METVIEW_DIR_SHARE/etc/VertProfViewDef',
    rules_file          = '$METVIEW_DIR_SHARE/etc/VertProfViewRules',
    default_name        = Vertical Profile View,
    help_page           = Vertical_ProfileView,
    type                = View,
    macro               = vertprofview,
    editor_type         = SimpleEditor,
    pixmap              = '$METVIEW_DIR_SHARE/icons/VPROFVIEW.icon',
    expand              = 512, #expand default values
    doubleclick_method  = Edit,
    obsolete            = True

state,
    class   = VERTPROFVIEW/XSECTVIEW/AVERAGEVIEW,
    action  = execute/visualise,
    service = uPlotManager

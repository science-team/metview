/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

/********************************************************************************
  Application for Average.
  It takes a GRIB file as input (in addition to the values given by the user),
  and produces an NetCDF file as output. There is no metadata on output request,
  only the path to NetCDF file.

  First the GRIB while is read, and info about all parameters and level are stored.
  Then several flags is set, based on the data and the input request, to determine
  how the application should run.

  The file is rewound, and sorted by param, date, time, step and expver. All fields
  with the same values for the sorting parameters are one plot. If any of these
  values change, all data are written out as netcdf variables with names put
  together from the sorting values.
********************************************************************************/

#include "Xsect.h"

class Average : public Xsect
{
public:
    // Constructors
    Average();
    Average(const char* kw);

    // Get geographical coordinates and application parameters
    bool getCoordinates(MvRequest&, ApplicationInfo&);
    bool getAppParameters(MvRequest&, ApplicationInfo&) override;

    bool generateData(ApplicationInfo&, ParamMap&, MvNetCDF&, MvField&, const std::string&) override;

    // Create output request
    MvRequest createOutputRequest(ApplicationInfo&, ParamInfo*) override;

    int computeGeographicalPoints(ApplicationInfo&, MvField* = nullptr) override;

    bool fillValues(ApplicationInfo&, MvField&, double*) override;

    bool fieldConsistencyCheck(MvField&, double, double, double, double) override;

    // Check if parameters between two requests are consistent
    bool consistencyCheck(MvRequest&, MvRequest&) override;

private:
    std::string titleVariable(ApplicationInfo&, ParamInfo*) override;

    int inputMode_{0};  // AVERAGE_NS/AVERAGE_EW
};

//---------------------------------------------------------------------

class AverageM3 : public Average
{
public:
    AverageM3() :
        Average("PM_AVERAGE") {}

    void serve(MvRequest&, MvRequest&) override;
};

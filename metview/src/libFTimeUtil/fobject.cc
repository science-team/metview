/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// fobject.cpp
// rev vk 940824

#include <cstddef>

#include "fobject.h"


#ifdef MAGICS
namespace magics
{
#else
namespace metview
{
#endif

//_________________________________________________________ TFObject

TFObject::TFObject() = default;
//_________________________________________________________ ~TFObject

TFObject::~TFObject() = default;
//_________________________________________________________ operator==

bool TFObject::operator==(const TFObject& aObjTest) const
{
    return (IsEqual(aObjTest));
}
//_________________________________________________________ operator!=

bool TFObject::operator!=(const TFObject& aObjTest) const
{
    return (bool)(!(IsEqual(aObjTest)));
}
}

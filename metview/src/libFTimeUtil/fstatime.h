/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// rev vk 970214 -------------------------- TStaticTime

#pragma once

#ifdef METVIEW
#include "libFTimeUtil/fsortabl.h"
#else
#include "fsortabl.h"
#endif

#include <ostream>

#include <cstring>
#ifdef METVIEW
#include "MvDate.h"
#endif


#ifdef MAGICS
namespace magics
{
#else
namespace metview
{
#endif


class TStaticTime : public TSortable
{
    friend std::ostream& operator<<(std::ostream& oStream, const TStaticTime& myTime);

public:
    TStaticTime();
    TStaticTime(const TStaticTime&);
    TStaticTime(const short year, const short month, const short day);
    TStaticTime(const short year, const short month, const short day, const short hour, const short minute = 0, const short sec = 0);

    bool IsEqual(const TFObject& anotherTime) const override;
    bool IsLessThan(const TFObject& anotherTime) const override;

    TStaticTime& operator=(const TStaticTime&);
#ifdef METVIEW
    operator MvDate(void) const
    {
        return MvDate(CharValue());
    }
#endif
    void SetDate(const short year, const short month, const short day);
    void GetDate(short& year, short& month, short& day) const;
    void SetTime(const short hour, const short min = 0, const short sec = 0);
    void GetTime(short& hour, short& min, short& sec) const;

    short GetYear() const
    {
        return fYear;
    }
    short GetMonth() const
    {
        return fMonth;
    }
    short GetDay() const
    {
        return fDay;
    }
    short GetHour() const
    {
        return fHour;
    }
    short GetMin() const
    {
        return fMin;
    }
    short GetSec() const
    {
        return fSec;
    }
    int dateAsInt() const;
    std::string timeAsString() const;

    // short GetLocalHour () const;
    const char* CharDate() const;
    const char* CharHhMm() const;
    const char* CharValue() const;
    const char* ShorterCharValue() const;

    void ReadDateTime();                 // for testing
    void XPrint() const;                 // for testing
    void XPrint(const char* str) const;  // for testing
    virtual void Print() const;

protected:
    void SetYear(const short year);
    void SetMonth(const short month);
    void SetDay(const short day);
    void SetHour(const short hour);
    void SetMin(const short minute);
    void SetSec(const short sec);
    void _setCurrent();

private:
    short fYear;
    short fMonth;
    short fDay;
    short fHour;
    short fMin;
    short fSec;
};
}

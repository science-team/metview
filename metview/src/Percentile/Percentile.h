/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

/**************
  Application Percentile.
  A percentile is a value on a scale of one hundred that indicates the
  percent of a distribution that is equal to or below it.
***************/

#include "Metview.h"
#include "MvFieldSet.h"

// Constants
enum
{
    PERC_NN,
    PERC_LI
};  // interpolation: nearest neighbour/linear

class Percentile : public MvService
{
public:
    // Constructor
    Percentile(const char* kw);

    // Destructor
    ~Percentile() override;

    void serve(MvRequest&, MvRequest&) override;

    // Initialize variables from user interface
    bool GetInputInfo(MvRequest& in);

    // Compute rank of percentiles
    double ComputeRank(double percentile, int nfsin);

    // Compute rank of percentiles
    void ComputeRankForAllPercentiles(std::vector<double>& vrank, int nfsin);

    // Compute percentiles
    bool ComputePercentile(MvRequest&);

    // Get values
    double GetValue(double rank, std::vector<double>& gvals, size_t numVals);

protected:
    // variables
    MvRequest dataRequest_;          // input data request
    std::vector<double> percList_;   // percentiles input values
    int interp_{0};                  // interporlation method
    bool computeWithMissing_{true};  // if missing values present, do we still compute?
};

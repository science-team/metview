/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QSortFilterProxyModel>
#include <QStyledItemDelegate>

#include "MvQOdbStat.h"

class MvQOdbStatDelegate : public QStyledItemDelegate
{
public:
    MvQOdbStatDelegate(QWidget* parent = 0);
    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const;
};

class MvQOdbStatModel : public QAbstractItemModel
{
public:
    MvQOdbStatModel() = default;

    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    int rowCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex&) const;

    void setBaseData(QList<MvQOdbStat*>);

private:
    QList<MvQOdbStat*> data_;
};

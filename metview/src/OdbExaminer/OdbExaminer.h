/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QMainWindow>
#include <QModelIndex>

#include "MvQMainWindow.h"
#include "MvQMenuItem.h"

class QGroupBox;
class QLabel;
class QListWidget;
class QSpinBox;
class QSplitter;
class QListView;
class QTreeView;
class QSortFilterProxyModel;
class QStandardItemModel;
class QTabWidget;
class QTextBrowser;
class QToolButton;

class MvQFileInfoLabel;
class MvQLogPanel;
class MvQOdbColumnModel;
class MvQOdbDataModel;
class MvQOdbDataWidget;
class MvQOdbMetaData;
class MvQOdbTableView;
class MvQOdbVarModel;
class MvQSearchLinePanel;
class MvQTreeView;
class MvQTreeViewSearchLine;

class MvAbstractOdb;

class OdbExaminer : public MvQMainWindow
{
    Q_OBJECT

public:
    OdbExaminer(QWidget* parent = 0);
    ~OdbExaminer();
    void init(MvQOdbMetaData*);
    void updateFileInfoLabel();

public slots:
    void slotTableSelected(QString);
    void slotTabIndexChanged(int);
    void slotShowAboutBox();
    void slotConfigure();

protected:
    void setupEditActions();
    void setupViewActions();
    void setupSettingsActions();
    void setupHelpActions();
    void setupTableBox();
    void setupColumnBox();
    void setupVarBox();
    void setupFindBox();

    void loadStarted();
    void loadFinished();

    void readSettings();
    void writeSettings();

    MvQOdbMetaData* data_;
    MvQMainWindow::MenuItemMap menuItems_;
    MvQFileInfoLabel* fileInfoLabel_;

    QTabWidget* metaTab_;

    QWidget* tablePanel_;
    QSplitter* tableSplitter_;
    MvQOdbTableView* tableView_;
    QTreeView* tableColumnTree_;
    MvQOdbColumnModel* tableColumnModel_;
    QSortFilterProxyModel* tableColumnSortModel_;
#ifdef UI_TODO
    MvQTreeViewSearchLine* tableColumnFind_;
#endif
    QGroupBox* columnBox_;
    QWidget* columnPanel_;
    QTreeView* columnTree_;
    MvQOdbColumnModel* columnModel_;
    QSortFilterProxyModel* columnSortModel_;
#ifdef UI_TODO
    MvQTreeViewSearchLine* columnFind_;
#endif
    QWidget* varPanel_;
    QTreeView* varTree_;
    MvQOdbVarModel* varModel_;
    QSortFilterProxyModel* varSortModel_;
#ifdef UI_TODO
    MvQTreeViewSearchLine* varFind_;
#endif

    MvQOdbDataWidget* dataPanel_;
#ifdef UI_TODO
    MvQTreeViewSearchLine* dataFind_;
#endif
    MvQSearchLinePanel* findPanel_;

    QSplitter* mainSplitter_;
    MvQLogPanel* logPanel_;
};

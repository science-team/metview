

ecbuild_add_executable( TARGET       pool
                        SOURCES      pool.cc
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS} ${LIBEMOS_LIBRARIES}
                    )


if ( NOT FDB_FOUND )
add_definitions(-DNOFDB)
endif()

ecbuild_add_executable( TARGET       Mars
                        SOURCES      MarsG2.cc
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES} ${ODB_API_INCLUDE_DIRS} ${FDB_INCLUDE_DIRS}
                        LIBS         ${STANDARD_METVIEW_LIBS_ODB} ${LIBEMOS_LIBRARIES} ${FDB_LIBRARIES} ${METVIEW_ODB_API_LIBRARIES}
                    )

ecbuild_add_executable( TARGET       togrib
                        SOURCES      togrib.cc
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )

ecbuild_add_executable( TARGET       tomatrix
                        SOURCES      tomatrix.cc
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )

ecbuild_add_executable( TARGET       geo_to_grib
                        SOURCES      geo_to_grib.cc
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )

ecbuild_add_executable( TARGET       grib_to_geo
                        SOURCES      grib_to_geo.cc
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                    )


if (METVIEW_MARS_ODB)
    metview_module_files(ETC_FILES ObjectSpec.MarsOdb
                               
                         XPM_FILES ODB_MARS.xpm
                        )
endif()

if(HAVE_PPROC_MIR)
    metview_module_files(ETC_FILES ObjectSpec.MarsMir
                         SVG_FILES READ_MIR.svg RETRIEVE_MIR.svg
                         )
endif()

# only expose the retrieve_emos, etc functions if we have both emoslib and mir
if(HAVE_PPROC_EMOS AND HAVE_PPROC_MIR)
    metview_module_files(ETC_FILES ObjectSpec.MarsEmos
                         SVG_FILES READ_EMOS.svg RETRIEVE_EMOS.svg
                         )
endif()

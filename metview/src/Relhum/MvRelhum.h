/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

const int Temp = 130;
const int Dew = 133;
const int LnPress = 152;
const float RD = 287.05967367;
const float RV = 461.52499331;
const float TMELT = 273.16;
const float TICE = 250.16;
const float C1ES = 611.21;
const float C3LES = 17.502;
const float C3IES = 22.587;
const float C4LES = 32.19;
const float C4IES = -0.7;


class MvRelhum
{
protected:
    int refDataRepres{-1};
    int refDataLength{-1};

    void init();


public:
    MvRelhum();
    virtual ~MvRelhum();
    virtual MvRequest compute(int, MvField&, MvFieldSetIterator&, MvFieldSetIterator&, int);
    void setRefDataRepres(int v) { refDataRepres = v; }
    void setRefDataLength(int v) { refDataLength = v; }
};

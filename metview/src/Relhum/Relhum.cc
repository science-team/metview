/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#include <cstdio>
#include <cmath>

#include <iostream>

#include "Metview.h"

#include "MvRelhum.h"
#include "MvException.h"


class Relhum : public MvService
{
protected:
    Relhum(char* kw) :
        MvService(kw) {}

public:
    Relhum() :
        MvService("RELHUM2") {}
    void serve(MvRequest&, MvRequest&) override;
};


void Relhum::serve(MvRequest& in, MvRequest& out)
{
    std::cout << "Relhum in" << std::endl;
    in.print();

    mars.computeflg = 0;  //-- for a nicer plot title (no "**" in front of param name)

    MvField field;
    MvRequest grib;

    auto* mvrh = new MvRelhum();

    in.getValue(grib, "DATA");

    MvFieldSet fsInput(grib);
    MvFieldSetIterator iterLNSP(fsInput), iter_t(fsInput), iter_q(fsInput);

    MvFilter param("PARAM");

    iterLNSP.setFilter(param == LnPress);
    iter_t.setFilter(param == Temp);
    iter_q.setFilter(param == Dew);

    int count_LnPres = 0, count_Temp = 0, count_Dew = 0;

    while (iterLNSP())
        count_LnPres++;
    iterLNSP.rewind();

    while (iter_t())
        count_Temp++;
    iter_t.rewind();

    while (iter_q())
        count_Dew++;
    iter_q.rewind();


    if (count_Temp != count_Dew) {
        delete mvrh;
        setError(1, "Relhum-> Different number of T and Q fields");
        return;
    }

    field = iter_q();
    MvRequest rq = field.getRequest();
    const char* ML = rq("LEVTYPE");

    int lmodlv = 0;
    if (strcmp("ML", ML)) {
        lmodlv = 0;
        count_LnPres = rq.countValues("LEVELIST");
    }
    else {
        if (count_LnPres == 0) {
            delete mvrh;
            setError(1, "Relhum-> LNSP must be available in Model Level...");
            return;
        }
        lmodlv = 1;
    }

    //-- for checking field format consistency --//
    auto* fe = new MvFieldExpander(field);
    mvrh->setRefDataLength(field.countValues());
    int repres = field.dataRepres();
    if (repres == 50) {
        delete mvrh;
        delete fe;
        setError(1, "Relhum-> Cannot compute with spectral data!");
        return;
    }
    mvrh->setRefDataRepres(repres);
    // delete fe;

    iter_q.rewind();

    try {
        for (int i = 0; i < count_LnPres; i++) {
            MvField f_LNSP;
            f_LNSP = iterLNSP();
            MvFieldSet out_q, out_LNSP;

            if (lmodlv) {
                MvFieldExpander xLNSP(f_LNSP);
                out = out + mvrh->compute(count_Temp, f_LNSP, iter_t, iter_q, lmodlv);
            }
            else {
                MvField f_LNSP;
                f_LNSP = iterLNSP();
                out = out + mvrh->compute(count_Temp, f_LNSP, iter_t, iter_q, lmodlv);
            }
        }
    }
    catch (MvException& e) {
        delete mvrh;
        delete fe;
        setError(1, "Relhum-> %s", e.what());
        return;
    }

    std::cout << "Relhum out" << std::endl;
    out.print();

    delete mvrh;
    delete fe;

    return;
}

int main(int argc, char** argv)
{
    MvApplication theApp(argc, argv);
    Relhum Relhum;

    theApp.run();
}

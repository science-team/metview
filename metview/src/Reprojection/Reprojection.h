/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

/**************
  Application Reprojection.

  Reproject an input data to a selected output projection.
***************/

#include "Metview.h"
#include "MvFieldSet.h"

// Constants
enum
{
    REPROJ_NN,
    REPROJ_LI
};  // interpolation: nearest neighbour/linear

class Reprojection : public MvService
{
public:
    // Constructor
    Reprojection(const char* kw);

    // Destructor
    ~Reprojection();

    void serve(MvRequest&, MvRequest&);

    // Initialize variables from user interface
    bool GetInputInfo(MvRequest& in);

    // Compute percentiles
    bool ComputeReprojection(MvRequest&);

    // Get values
    //	fortfloat GetValue(float rank, std::vector<fortfloat>& gvals);

protected:
    // variables
    MvRequest dataRequest_;        // input data request
    fortint interp_;               // interporlation method
    fortfloat x1_, x2_, y1_, y2_;  // geographical coordinates
    fortfloat xres_, yres_;        // x/y resolution
    std::string projectionOut_;    // output projection
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include <UserPoint.h>
#include <RasterData.h>
#include <GribDecoder.h>
#include <Matrix.h>


class ReprojectService
{
public:
    ReprojectService() {}
    ReprojectService(std::string in, std::string out, double rx, double ry, double x1, double x2, double y1, double y2);

    bool execute();
    int gribit(magics::Matrix& matrix, magics::GribDecoder& sourceGrib);

private:
    std::string gribIn_;
    std::string gribOut_;
    double resX_, resY_;  // resolution
};

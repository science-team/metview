/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Reprojection.h"
#include "ReprojectService.h"
#include "mars.h"

Reprojection::Reprojection(const char* kw) :
    MvService(kw)
{
    // empty
}

Reprojection::~Reprojection() = default;

void Reprojection::serve(MvRequest& in, MvRequest& out)
{
    std::cout << "Reprojection::serve in" << std::endl;
    in.print();

    // Get information from the user interface
    if (!GetInputInfo(in))
        return;

    // Check input data
    // Needs to be done

    // Compute percentiles
    if (!ComputeReprojection(out))
        return;

    std::cout << "Reprojection::serve out" << std::endl;
    out.print();

    return;
}

bool Reprojection::GetInputInfo(MvRequest& in)
{
    // Get data information from UI
    std::string str;
    if ((const char*)in("SOURCE") && (strcmp((const char*)in("SOURCE"), "OFF") && strcmp((const char*)in("SOURCE"), "off"))) {
        str = (const char*)in("SOURCE");
        dataRequest_.setVerb("GRIB");
        dataRequest_("PATH") = str.c_str();
    }
    else {
        // Get information from the icon
        in.getValue(dataRequest_, "DATA");
        if (!in.countValues("DATA") || !dataRequest_.countValues("PATH")) {
            setError(1, "Reprojection-> No Data files specified...");
            return false;
        }
    }

    // Get area
    y1_ = in("AREA", 0);
    x1_ = in("AREA", 1);
    y2_ = in("AREA", 2);
    x2_ = in("AREA", 3);

#if 0
	// Check if coordinates follow Mars rules (n/w/s/e)
	if ( x1_ > x2_ )
	{
		double W = x1_;
		x1_ = x2_;
		x2_ = W;
	}
	if( y2_ > y1_ )
	{
		double W = y1_;
		y1_ = y2_;
		y2_ = W;
	}
#endif
    // Get resolution
    xres_ = in("RESOLUTION", 0);
    yres_ = (in.countValues("RESOLUTION") == 1) ? xres_ : in("RESOLUTION", 1);

    // Get output projection
    projectionOut_ = (const char*)in("PROJECTION");

    // Get interpolation method
    interp_ = strcmp((const char*)in("INTERPOLATION"), "NEAREST_NEIGHBOUR") ? REPROJ_LI : REPROJ_NN;

    return true;
}

bool Reprojection::ComputeReprojection(MvRequest& out)
{
    std::string gin = (const char*)dataRequest_("PATH");
    std::string gout = marstmp();

    ReprojectService reps(gin, gout, xres_, yres_, x1_, x2_, y1_, y2_);
    if (!reps.execute())
        return false;

    // Create fieldset request
    MvRequest req("GRIB");
    req("PATH") = gout.c_str();
    req("TEMPORARY") = 1;

    // Update output request
    out = out + req;

    return true;
}
// GRIB,
//     PATH       = '/var...',
//     TEMPORARY  = 1,
//     OFFSET     = 0,
//     LENGTH     = 87228


//--------------------------------------------------------

int main(int argc, char** argv)
{
    MvApplication theApp(argc, argv);
    Reprojection reproj("REPROJECTION");

    // The applications don't try to read or write from pool, this
    // should not be done with the new PlotMod.
    // a.addModeService("GRIB", "DATA");
    // c.saveToPool(false);
    // perc.saveToPool(false);

    theApp.run();
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

// -*-C++-*-
// Header file for Hovmoeller application


#pragma once

#include "Metview.h"
#include "MvDate.h"

const double HMISSING_VALUE = 1.0E22;

#define HOV_STEPSIZE 6  // number of characters to represent parameter STEP

// Holds info about the values for one entry in
// the matrix (e.g. values for one time or level)
class IndexesInfo
{
public:
    double* Values() const { return values_; }
    double Index() const { return index_; }

private:
    friend class ParamInfo;

    IndexesInfo(double index) :
        index_(index) {}

    double* values_{nullptr};
    double index_{0.};
};

using IndexesMap = std::map<std::string, IndexesInfo*>;
using IndexIterator = IndexesMap::iterator;
using ReverseIndexIterator = IndexesMap::reverse_iterator;

// Info about a parameter at one level and expver
// Contains  a IndexesMap with info about the times (date+time+step)
class ParamInfo
{
public:
    ParamInfo();
    ParamInfo(int, double, double, std::string, std::string, std::string, std::string);

    ~ParamInfo();

    MvDate& Date() { return date_; }
    int Parameter() const { return param_; }
    std::string ParamName() const { return paramName_; }
    std::string ParamLongName() const { return paramLongName_; }
    std::string Units() const { return units_; }

    double Level() const { return level_; }
    std::string LevelTitle();

    std::string ExpVer() const { return expver_; }
    std::string ExpVerTitle();
    void ExpVer(const char* xx) { expver_ = xx; }

    void FillIndex(double*, const std::string&, int);
    IndexesMap& Indexes() { return indexes_; }

    void AddIndex(std::string& index);
    int NrIndexes() const { return static_cast<int>(indexes_.size()); }

    friend int operator<(const ParamInfo& p1, const ParamInfo& p2)
    {
        return (p1.param_ < p2.param_ || p1.date_ < p2.date_ || p1.level_ < p2.level_);
    }

private:
    int param_;
    double level_;
    MvDate date_;
    std::string expver_;
    std::string paramName_;
    std::string paramLongName_;
    std::string units_;
    IndexesMap indexes_;
};

using ParamMap = std::map<std::string, ParamInfo*>;
using ParamIterator = ParamMap::iterator;
using ParamInsertPair = std::pair<ParamIterator, bool>;
using ParamPair = std::pair<const std::string, ParamInfo*>;

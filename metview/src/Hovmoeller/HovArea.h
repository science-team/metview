/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Hovmoeller.h"
#include "HovToolkit.h"

class HovAreaToolkit : public HovToolkit
{
public:
    using HovToolkit::HovToolkit;
    ~HovAreaToolkit() override = default;

    // Common functions
    std::string GetTitle(ParamInfo*) override;
    const char* GetSecondCoordName() override;
    bool SecondAuxiliaryCoord() override { return false; }
    const char* ApplicationType() override { return "AREA_HOVM"; }
    bool NcWriteSecondCoord() override;
    void NcWriteGlobalAttributesApp() override;

    // Calculate diagram values
    bool ComputeValues(MvField&, int = 0) override;

    // Compute coordinates
    bool ComputeSecondCoord() override;

    // Initialize variables
    bool GetInputInfo(MvRequest&) override;  // from user interface
    bool GetInputInfo(MvNetCDF*) override;   // from the netCDF file

    // Check if parameters between two requests are consistent
    bool consistencyCheck(MvRequest& vieReq, MvRequest& dataReq) override;

private:
    // Create View request
    MvRequest CreateViewRequest() override;

    int geoDir_{HOV_EW};
};

//---------------------------------------------------------------------

class AreaHovmoeller : public HovAreaToolkit, public Hovmoeller
{
public:
    AreaHovmoeller() :
        Hovmoeller("AREA_HOVM") {}
    AreaHovmoeller(const char* kw) :
        Hovmoeller(kw) {}

    ~AreaHovmoeller() override = default;

    // Entry point
    void serve(MvRequest& in, MvRequest& out) override;
};

static SimpleObjectMaker<HovAreaToolkit, HovToolkit> area("AREA_HOVM");

//---------------------------------------------------------------------

class AreaHovmoellerM3 : public AreaHovmoeller
{
public:
    AreaHovmoellerM3() :
        AreaHovmoeller("AREA_HOV") {}
    ~AreaHovmoellerM3() override = default;

    // Entry point
    void serve(MvRequest&, MvRequest&) override;
};

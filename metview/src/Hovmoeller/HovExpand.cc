/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "HovExpand.h"
#include "HovToolkit.h"

void ExpandHovmoeller::serve(MvRequest& in, MvRequest& out)
{
    std::cout << "Hovmoeller::serve in" << std::endl;
    in.print();

    // Initialize object from the user interface
    std::string netfnameIn, hovTypeIn;
    MvRequest dataRequestIn;
    if (!GetInputInfo(in, netfnameIn, dataRequestIn, hovTypeIn))
        return;

    // Instantiate the related HovToolkit
    HovToolkit* hov = SimpleObjectMaker<HovToolkit>::create(hovTypeIn.c_str());

    // Compute the Hovmoeller diagram
    std::string action = (const char*)in("_ACTION") ? (const char*)in("_ACTION") : "prepare";
    if (!hov->Compute(netfnameIn, dataRequestIn, action, out))
        return;

    // Update output request
    if ((const char*)in("_NAME"))
        out("_NAME") = in("_NAME");

    std::cout << "Hovmoeller::serve out" << std::endl;
    out.print();

    return;
}

bool ExpandHovmoeller::GetInputInfo(MvRequest& in, std::string& netfnameIn, MvRequest& dataRequestIn, std::string& hovTypeIn)
{
    // Get netcdf information from UI
    if ((const char*)in("NETCDF_PATH") != nullptr && strcmp((const char*)in("NETCDF_PATH"), "OFF") != 0 && strcmp((const char*)in("NETCDF_PATH"), "off") != 0)
        netfnameIn = (const char*)in("NETCDF_PATH");
    else {
        // Get information from the icon
        MvRequest req;
        in.getValue(req, "NETCDF_DATA");
        if (!in.countValues("NETCDF_DATA") || !req.countValues("PATH")) {
            setError(1, "ExpandHovmoeller-> No Netcdf Data files specified");
            return false;
        }
        netfnameIn = (const char*)req("PATH");
    }

    // Get fieldset information from UI
    std::string str;
    if ((const char*)in("DATA_PATH") != nullptr && strcmp((const char*)in("DATA_PATH"), "OFF") != 0 && strcmp((const char*)in("DATA_PATH"), "off") != 0) {
        str = (const char*)in("DATA_PATH");
        dataRequestIn.setVerb("GRIB");
        dataRequestIn("PATH") = str.c_str();
    }
    else {
        // Get information from the icon
        in.getValue(dataRequestIn, "DATA");
        if (!in.countValues("DATA") || !dataRequestIn.countValues("PATH")) {
            setError(1, "ExpandHovmoeller-> No Data files specified");
            return false;
        }
    }

    // Extract information from the input netcdf file
    // Open netcdf file
    auto* netcdfIn = new MvNetCDF();
    netcdfIn->init(netfnameIn, 'r');

    // Check netcdf file attributes
    MvNcAtt* tmpatt = netcdfIn->getAttribute("_View");
    if (!tmpatt) {
        setError(1, "ExpandHovmoeller-> Netcdf file: missing attribute _View");
        delete netcdfIn;
        return false;
    }

    tmpatt = netcdfIn->getAttribute("type");
    if (tmpatt)
        hovTypeIn = tmpatt->as_string(0);
    else {
        setError(0, "ExpandHovmoeller-> Netcdf file: missing attribute type");
        delete netcdfIn;
        return false;
    }

    delete netcdfIn;
    return true;
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Hovmoeller.h"
#include "HovArea.h"
#include "HovExpand.h"
#include "HovHeight.h"
#include "HovLine.h"

//--------------------------------------------------------

int main(int argc, char** argv)
{
    MvApplication theApp(argc, argv);

    LineHovmoeller lhov;
    AreaHovmoeller ahov;
    ExpandHovmoeller ehov;
    HeightHovmoeller hhov;

    // The applications don't try to read or write from pool, this
    // should not be done with the new PlotMod.
    // a.addModeService("GRIB", "DATA");
    lhov.saveToPool(false);
    ahov.saveToPool(false);
    ehov.saveToPool(false);
    hhov.saveToPool(false);

    // These are Metview 3 definitions used for backwards compatibility.
    // Remove them later.
    LineHovmoellerM3 lhovM3;
    AreaHovmoellerM3 ahovM3;
    HeightHovmoellerM3 hhovM3;
    lhovM3.saveToPool(false);
    ahovM3.saveToPool(false);
    hhovM3.saveToPool(false);

    theApp.run();
}

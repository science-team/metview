/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

/*! \file Factory.cc
    \brief Definition of Parameter base class.

    Magics Team - ECMWF 2004

    Started: Jan 2004

    Changes:

*/

template <class B>
std::map<std::string, SimpleFactory<B>*>* SimpleFactory<B>::map_ = 0;

template <class B>
SimpleFactory<B>::SimpleFactory(const std::string& name) :
    name_(name)
{
    if (!map_)
        map_ = new std::map<std::string, SimpleFactory<B>*>();
    (*map_)[name_] = this;
}

template <class B>
SimpleFactory<B>::~SimpleFactory()
{
}

template <class B>
B* SimpleFactory<B>::create(const std::string& name)
{
    SimpleFactory<B>* maker = get(name);
    if (maker) {
        B* object = (*maker).make();
        return object;
    }

#ifdef MAGICS_EXCEPTION
    throw NoFactoryException(name);
#else
    return 0;
#endif
}

template <class B>
SimpleFactory<B>* SimpleFactory<B>::get(const std::string& name)
{
    auto maker = (*map_).find(name);
    if (maker != (*map_).end())
        return (*maker).second;
#ifdef MAGICS_EXCEPTION
    throw NoFactoryException(name);
#else
    return 0;
#endif
}

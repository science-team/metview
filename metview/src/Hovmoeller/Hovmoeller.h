/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

/*************************************************************
  Application for Hovmoeller (similar to XSection, Average).
  It takes a GRIB file as input (in addition to the values
  given by the user),  and produces an NetCDF
  file as output. There is no metadata on output request,
  only the path to NetCDF file.

  First the GRIB while is read, and info about all
  parameters and level are stored. Then several
  flags is set, based on the data and the input
  request, to determine how the application should
  run.

  The file is rewound, and sorted by param, date,
  time, step and expver. All fields with the
  same values for the sorting parameters are
  one plot. If any of these values change, all
  data are written out as netcdf variables with
  names put together from the sorting values, and
  an attribute telling PlotMod how to show these
  data.
***********************************************************/

#include "MvRequest.h"
#include <MvProtocol.h>
#include "MvService.h"

class Hovmoeller : public MvService
{
public:
    // Constructor
    Hovmoeller(const char* kw) :
        MvService(kw) {}

    // Destructor
    ~Hovmoeller() override = default;

    void serve(MvRequest&, MvRequest&) override {}
};

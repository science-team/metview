/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Hovmoeller.h"
#include "HovToolkit.h"

class LevelInterpolator;

class HovHeightToolkit : public HovToolkit
{
public:
    HovHeightToolkit();
    ~HovHeightToolkit() override;

    // Common functions
    bool GenerateData(MvRequest&) override;
    std::string GetTitle(ParamInfo*) override;
    const char* GetSecondCoordName() override { return "vertical"; }
    bool SecondAuxiliaryCoord() override { return false; }
    const char* ApplicationType() override { return "VERTICAL_HOVM"; }
    bool NcWriteSecondCoord() override;
    void NcWriteGlobalAttributesApp() override;

    // Functions to handle list of levels
    void InitializeLevelList() override { coord1_.clear(); }
    void collectVerticalData(MvField&, const std::string&, double) override;
    bool collectLnsp(MvRequest& data) override;

    // Calculate diagram values
    double computeValue(MvField& field) const;
    bool ComputeValues(MvField&, int) override;

    // Compute coordinates
    bool ComputeSecondCoord() override;

    // Create access keys
    void GenerateKey(std::string&, MvRequest&) override;

    // Initialize variables
    bool GetInputInfo(MvRequest&) override;  // from user interface
    bool GetInputInfo(MvNetCDF*) override;   // from the netCDF file

    // Check if parameters between two requests are consistent
    bool consistencyCheck(MvRequest& viewReq, MvRequest& dataReq) override;

private:
    enum VerticalLevelType
    {
        AsInDataLevelType,
        PressureLevelType,
        ParamLevelType,
        NoLevelType
    };

    // Create View request
    MvRequest CreateViewRequest() override;

    VerticalLevelType vertAxisType_{AsInDataLevelType};
    static std::map<VerticalLevelType, std::string> vertAxisTypeNames_;
    LevelInterpolator* levHandler_{nullptr};
    std::string inputLevelType_;
    std::string outputLevelType_;
    enum InputMode
    {
        NoInputMode,
        PointInputMode,
        NearestGrpInputMode,
        AreaInputMode
    };
    static std::map<InputMode, std::string> inputModeNames_;
    InputMode inputMode_{NoInputMode};
};

//---------------------------------------------------------------------

class HeightHovmoeller : public HovHeightToolkit, public Hovmoeller
{
public:
    HeightHovmoeller() :
        Hovmoeller("VERTICAL_HOVM") {}
    HeightHovmoeller(const char* kw) :
        Hovmoeller(kw) {}

    ~HeightHovmoeller() override = default;

    // Entry point
    void serve(MvRequest&, MvRequest&) override;
};

static SimpleObjectMaker<HovHeightToolkit, HovToolkit> height("VERTICAL_HOVM");

//---------------------------------------------------------------------

class HeightHovmoellerM3 : public HeightHovmoeller
{
public:
    HeightHovmoellerM3() :
        HeightHovmoeller("HEIGHT_HOV") {}
    ~HeightHovmoellerM3() override = default;

    // Entry point
    void serve(MvRequest&, MvRequest&) override;
};

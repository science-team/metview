/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Hovmoeller.h"

class ExpandHovmoeller : public Hovmoeller
{
public:
    // Constructor/destructor
    ExpandHovmoeller() :
        Hovmoeller("EXPAND_HOVM") {}
    ~ExpandHovmoeller() override = default;

    // Main function
    void serve(MvRequest&, MvRequest&) override;

    // Initialize variables from user interface
    bool GetInputInfo(MvRequest&, std::string&, MvRequest&, std::string&);
};

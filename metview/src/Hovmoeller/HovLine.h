/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Hovmoeller.h"
#include "HovToolkit.h"

class HovLineToolkit : public HovToolkit
{
public:
    // Initialize variables
    bool GetInputInfo(MvRequest&) override;  // from user interface
    bool GetInputInfo(MvNetCDF*) override;   // from the netCDF file

    // Common functions
    std::string GetTitle(ParamInfo*) override;
    const char* GetSecondCoordName() override;
    const char* GetSecondAuxiliaryCoordName() override;
    bool SecondAuxiliaryCoord() override { return true; }
    const char* ApplicationType() override { return "LINE_HOVM"; }
    bool NcWriteSecondCoord() override;
    void NcWriteGlobalAttributesApp() override;

    // Calculate diagram values
    bool ComputeValues(MvField&, int = 0) override;

    // Compute coordinates
    bool ComputeSecondCoord() override;

    // Check if parameters between two requests are consistent
    bool consistencyCheck(MvRequest& viewReq, MvRequest& dataReq) override;

private:
    // Compute number of points
    bool EvaluateNPoints();

    // Create View request
    MvRequest CreateViewRequest() override;
};

//---------------------------------------------------------------------

class LineHovmoeller : public HovLineToolkit, public Hovmoeller
{
public:
    LineHovmoeller() :
        Hovmoeller("LINE_HOVM") {}
    LineHovmoeller(const char* kw) :
        Hovmoeller(kw) {}

    ~LineHovmoeller() override = default;

    // Entry point
    void serve(MvRequest&, MvRequest&) override;
};

static SimpleObjectMaker<HovLineToolkit, HovToolkit> line("LINE_HOVM");

//---------------------------------------------------------------------

class LineHovmoellerM3 : public LineHovmoeller
{
public:
    LineHovmoellerM3() :
        LineHovmoeller("LINE_HOV") {}

    ~LineHovmoellerM3() override = default;

    // Entry point
    void serve(MvRequest&, MvRequest&) override;
};

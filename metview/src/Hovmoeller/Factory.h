/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

/*! \file Factory.h
    \brief Definition of Factory  class.

    Magics Team - ECMWF 2004

    Started: Jan 2004

    Changes:

*/
#pragma once

#include <map>
#include <string>

#if 0
class NoFactoryException : public MagicsException
{
public:
	NoFactoryException( const std::string& factory ):
		MagicsException("Factory (" +  factory + ") not found") {}
	NoFactoryException():
		MagicsException("Factory not found") {}
};
#endif


template <class B>
class SimpleFactory
{
public:
    // -- Contructors
    SimpleFactory(const std::string& name);
    virtual ~SimpleFactory();

    // -- Methods
    static B* create(const std::string& name);
    virtual B* make() const = 0;

    // -- Members
    static std::map<std::string, SimpleFactory<B>*>* map_;
    static SimpleFactory<B>* get(const std::string& name);
    std::string name_;
};

template <class A, class B = A>
class SimpleObjectMaker : public SimpleFactory<B>
{
public:
    SimpleObjectMaker(const std::string& name) :
        SimpleFactory<B>(name)
    {
    }

    B* make() const { return new A(); }
};

#include "Factory.cc"

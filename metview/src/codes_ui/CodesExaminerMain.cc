
/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QApplication>
#include <QDebug>
#include <QFile>
#include <QStyleFactory>

#include <iostream>

#include "MvQFileList.h"
#include "MvQKeyManager.h"
#include "MvQTheme.h"

#include "BufrExaminer.h"
#include "BufrMetaData.h"
#include "CodesDirHandler.h"
#include "GribExaminer.h"
#include "GribMetaData.h"

// Globals
int ac;
char** av;

void printUsage()
{
    std::cerr << "usage: " << av[0] << " grib|bufr|no input_file1 input_file2 ..." << std::endl;
}

int main(int argc, char** argv)
{
    ac = argc;
    av = argv;

    // first arg is the mode
    std::string fMode(argv[1]);

    if (fMode != "no" && fMode != "bufr" && fMode != "grib") {
        if (fMode.empty()) {
            std::cerr << "ERROR: mode is not specified!" << std::endl;
        }
        else {
            std::cerr << "ERROR: invalid mode=" << fMode << " specified!" << std::endl;
        }

        printUsage();
        exit(1);
    }

    if (fMode == "no") {
        if (ac < 3) {
            std::cerr << "ERROR: input file(s) missing!" << std::endl;
            printUsage();
            exit(1);
        }
    }

    QStringList inFiles;
    for (int i = 2; i < argc; i++) {
        inFiles << argv[i];
    }

    MvQFileList* fileLst = 0;

    if (fMode == "grib") {
        fileLst = new MvQFileList(inFiles, MvQFileList::GribFile);
    }
    else if (fMode == "bufr") {
        fileLst = new MvQFileList(inFiles, MvQFileList::BufrFile);
    }
    else {
        fileLst = new MvQFileList(inFiles);
        if (fileLst->type() != MvQFileList::BufrFile &&
            fileLst->type() != MvQFileList::GribFile) {
            std::cerr << "ERROR: No GRIB or BUFR files were found in the specified list of files!" << std::endl;
            exit(1);
        }
        Q_ASSERT(fileLst->count() > 0);
    }

    Q_ASSERT(fileLst);

    // Create the qt application
    QApplication app(ac, av);

    // Init the style
    QStringList styleLst = QStyleFactory::keys();

    // qDebug() << styleLst;

    // Set the style
    QString style = "Plastique";
    if (styleLst.contains(style)) {
        app.setStyle(style);
    }
    else {
        style = "Fusion";
        if (styleLst.contains(style)) {
            app.setStyle(style);
        }
    }


    // Initialise resorces from a static library (libMvQtGui)
    Q_INIT_RESOURCE(examiner);
    Q_INIT_RESOURCE(keyDialog);
    Q_INIT_RESOURCE(window);
    Q_INIT_RESOURCE(find);

#ifdef ECCODES_UI
    Q_INIT_RESOURCE(codes_ui);
#endif

    MvQTheme::init(&app);

    if (fMode == "bufr" || fileLst->type() == MvQFileList::BufrFile) {
        // Create the bufr key manager and initialize it
        MvQKeyManager* manager = new MvQKeyManager(MvQKeyManager::BufrType);
        manager->loadProfiles();

        // Create the grib metadata object and initialize it with the first file
        BufrMetaData* bufr = new BufrMetaData;
        if (fileLst->count() > 0)
            bufr->setFileName(fileLst->path(0).toStdString());

        // Create the bufr browser and initialize it
        BufrExaminer* browser = new BufrExaminer();
        browser->init(bufr, manager, fileLst);
        browser->show();
    }
    else if (fMode == "grib" || fileLst->type() == MvQFileList::GribFile) {
        // Create the grib key manager and initialize it
        MvQKeyManager* manager = new MvQKeyManager(MvQKeyManager::GribType);
        manager->loadProfiles();

        // Create the grib metadata object and initialize it
        GribMetaData* grib = new GribMetaData;
        if (fileLst->count() > 0)
            grib->setFileName(fileLst->path(0).toStdString());

        // Create the grib browser and initialize it
        // Set tmpfile name
        std::string ftmp = "/tmp/grib1234.tmp";
        GribExaminer* browser = new GribExaminer(ftmp);
        browser->init(grib, manager, fileLst);
        browser->show();
    }

    // Enter the app loop
    app.exec();
}

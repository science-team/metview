::============================================================================
:: Copyright 2019 ECMWF.
:: This software is licensed under the terms of the Apache Licence version 2.0
:: which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
:: In applying this licence, ECMWF does not waive the privileges and immunities
:: granted to it by virtue of its status as an intergovernmental organisation
:: nor does it submit to any jurisdiction.
::============================================================================

:: This script is a light wrapper around the codes_ui launch script to be used
:: on Windows.
@echo off

:: switch \ for / so bash can find input files
set args=%*
set args=%args:\=/%

bash -c "codes_ui %args%"

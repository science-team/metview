
#pragma once

#include <string>

class CodesDirHandler
{
public:
    static CodesDirHandler* instance();

    const std::string& shareDir() const { return shareDir_; }
    const std::string& configDir() const { return configDir_; }

    std::string shareDirFile(const std::string& fName) const;
    std::string confDirFile(const std::string& fName) const;
    std::string qtResourceDirFile(const std::string& fName) const;

protected:
    CodesDirHandler();

    static CodesDirHandler* instance_;

    std::string shareDir_;
    std::string configDir_;
    std::string qtResourceDir_;
};

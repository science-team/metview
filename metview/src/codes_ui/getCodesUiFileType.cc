/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQFileList.h"
#include <iostream>

int main(int argc, char** argv)
{
    if (argc < 2) {
        std::cerr << "Input file(s) missing!" << std::endl;
        exit(1);
    }

    QStringList inFiles;
    for (int i = 1; i < argc; i++) {
        inFiles << argv[i];
    }

    MvQFileList::FileType fileType = MvQFileList::firstValidType(inFiles);

    if (fileType == MvQFileList::BufrFile)
        std::cout << "bufr" << std::endl;
    else if (fileType == MvQFileList::GribFile)
        std::cout << "grib" << std::endl;
    else {
        std::cerr << "No GRIB or BUFR files were found in the specified list of files!" << std::endl;
        exit(1);
    }

    return 0;
}

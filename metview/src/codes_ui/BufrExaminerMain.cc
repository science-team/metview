/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QApplication>
#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QStyleFactory>

#include <iostream>

#include "BufrExaminer.h"
#include "BufrMetaData.h"
#include "CodesDirHandler.h"
#include "MvQFileList.h"
#include "MvQKeyManager.h"
#include "MvQTheme.h"
#include "MvQSimpleApplication.h"

// Globals
int ac;
char** av;

int main(int argc, char** argv)
{
    ac = argc;
    av = argv;

    // Get input files
    QStringList inFiles;
    for (int i = 1; i < argc; i++) {
        inFiles << argv[i];
    }

    // Create the filelist
    auto* fileLst = new MvQFileList(inFiles, MvQFileList::BufrFile);
    Q_ASSERT(fileLst);

    // Create the qt application
    MvQSimpleApplication app(ac, av, "BufrExaminer",
                             {"examiner", "keyDialog", "window", "find", "codes_ui"});

    //    //Init the style
    //    QStringList styleLst = QStyleFactory::keys();

    //    //Set the style
    //    QString style = "Plastique";
    //    if (styleLst.contains(style)) {
    //        app.setStyle(style);
    //    }
    //    else {
    //        style = "Fusion";
    //        if (styleLst.contains(style)) {
    //            app.setStyle(style);
    //        }
    //    }

    //    //Set fontsize if defined in env var
    //    if (const char* fontSizeCh = getenv("CODES_UI_FONT_SIZE")) {
    //        int fontSize = atoi(fontSizeCh);
    //        if (fontSize < 8)
    //            fontSize = 8;
    //        else if (fontSize > 28)
    //            fontSize = 28;
    //        QFont f = app.font();
    //        f.setPointSize(fontSize);
    //        app.setFont(f);
    //    }

    //    MvQTheme::init(&app, {"examiner","keyDialog","window","find","codes_ui"});

    // Create the bufr key manager and initialize it
    auto* manager = new MvQKeyManager(MvQKeyManager::BufrType);
    manager->loadProfiles();

    // Create the grib metadata object and initialize it with the first file
    auto* bufr = new BufrMetaData;
    if (fileLst->count() > 0)
        bufr->setFileName(fileLst->path(0).toStdString());

    // Create the bufr browser and initialize it
    auto* browser = new BufrExaminer();
    browser->init(bufr, manager, fileLst);
    browser->show();

    if (!inFiles.isEmpty() && fileLst->count() == 0)
        QMessageBox::warning(browser, "codes_ui",
                             "No BUFR files were found in the specified input. The BUFR files used last time were loaded instead.",
                             QMessageBox::Ok);

    // Enter the app loop
    app.exec();
}

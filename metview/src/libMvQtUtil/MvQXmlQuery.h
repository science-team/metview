/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QMap>
#include <QStringList>
#include <QtXmlPatterns/QXmlQuery>
#include <QtXmlPatterns/QXmlItem>
#include <QtXmlPatterns/QXmlResultItems>


class MvQXmlQuery : public QXmlQuery
{
public:
    MvQXmlQuery(){};

    void getAtomicValue(QXmlItem&, QString&);
    void getAtomicValues(QXmlResultItems&, QStringList&);
    void getAttributes(QXmlItem&, QMap<QString, QString>&);
};

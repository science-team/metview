/***************************** LICENSE START ***********************************

 Copyright 2022 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QFontMetrics>

class FontMetrics : public QFontMetrics
{
public:
    FontMetrics(const QFont& font);
    int realHeight() const { return realHeight_; }
    int realWidth(QString txt, int& leftPadding, int& rightPadding) const;
    int topPaddingForCentre() const { return topPadding_; }
    int bottomPaddingForCentre() const { return bottomPadding_; }

protected:
    QFont font_;
    int realHeight_;
    int topPadding_{0};
    int bottomPadding_{0};

private:
    void computeRealHeight(QFont f);
};

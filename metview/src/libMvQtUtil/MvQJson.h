/***************************** LICENSE START ***********************************

 Copyright 2021 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <type_traits>

#include <QPointF>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QList>
#include <QRectF>
#include "MvRequest.h"

class MvQJson
{
public:
    static QJsonObject fromRequest(const MvRequest&);
    static void toRequest(const QJsonObject& o, MvRequest& r);
    static QPointF toPointF(const QJsonArray& a);
    static QJsonArray fromPointF(const QPointF&);
    static QJsonArray fromPointFList(QList<QPointF> lst, QString geoJsonType = {});
    static QList<QPointF> toPointFList(const QJsonArray& a, QString geoJsonType = {});
    static QRectF toRectF(const QJsonArray& a);
    static QJsonArray fromRectF(const QRectF&);

    static QString toString(const QJsonValue& v);
    static double toDouble(const QJsonValue& v);
    static bool toBool(const QJsonValue& v);

private:
    static void setRequestValue(MvRequest& r, const char* par, const QJsonValue& v);
    static void addRequestValue(MvRequest& r, const char* par, const QJsonValue& v, QJsonValue::Type);
};

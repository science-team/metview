/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QObject>
#include <QFileInfo>
#include <QList>
#include <QStringList>

class MvQFileList : public QObject
{
    Q_OBJECT

public:
    enum FileType
    {
        BufrFile,
        GribFile,
        OtherFile,
        BadFile
    };

    MvQFileList(FileType type);
    MvQFileList(QStringList expr);
    MvQFileList(QStringList exprLst, FileType type);

    void clear();
    void clearOthers(QString);
    QStringList paths() const;
    QStringList names();
    QList<QFileInfo> files() const { return files_; }
    QString name(int i) const;
    QString path(int i) const;
    FileType type() const { return type_; }
    QString typeName() const;
    int count() const { return files_.count(); }
    bool add(QString s, QString& err, bool broadcast = true);
    bool isPresent(QString s) const;
    static FileType firstValidType(QStringList exprLst);

signals:
    void fileAddRemoveBegin();
    void fileAddRemoveEnd();

protected:
    void init();
    static FileType fileType(QString);

    QList<QFileInfo> files_;
    QStringList paths_;
    QString expr_;
    FileType type_;
    static QMap<FileType, QString> typeNames_;
};

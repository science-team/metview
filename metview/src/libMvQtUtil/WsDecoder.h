/***************************** LICENSE START ***********************************

 Copyright 2022 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include <QJsonArray>
#include <QJsonDocument>
#include <QList>
#include <QPointF>

#include "MvRequest.h"

class WsType;
class QJsonArray;

class WsDecoder
{
public:
    static void toJson(const MvRequest&, QJsonDocument&);
    static void toJson(WsType* wst, QPointF scenePos, const QList<QPointF>& coord,
                       const MvRequest& styleReq, QJsonDocument& doc);
    static void toJson(WsType* wst, const QList<QPointF>& coord,
                       const MvRequest& styleReq, QJsonDocument& doc);
    static void toJsonCollection(const QJsonArray& a, QJsonDocument& doc);
    static void readCollection(QString fPath, QJsonArray& a);
    static WsType* getWsType(const QJsonDocument&);

    static void toRequest(const QJsonDocument&, MvRequest&, long flags = EXPAND_DEFAULTS);
    static void toRequest(const std::string& fPath, MvRequest& r, long flags = EXPAND_DEFAULTS);
    static void toStyleRequest(const QJsonDocument& doc, MvRequest& style);
    static void styleValue(const QJsonObject& obj, QString param, double& v);
    static void styleValue(const QJsonObject& obj, QString param, std::vector<bool>& vec);
    static void styleValue(const QJsonObject& obj, QString param, std::vector<double>& vec);
    static void setStyleValue(QJsonObject& obj, QString param, double v);
    static void setStyleValue(QJsonObject& obj, QString param, const std::vector<bool>& vec);
    static void setStyleValue(QJsonObject& obj, QString param, const std::vector<double>& vec);
    static QPointF scenePos(const QJsonObject& obj);
    static void identify(const QJsonObject& obj, QString& wsCls, QString& wsType);
    static QString toolTip(const QJsonDocument& doc);
    static void setToolTip(QJsonDocument& doc, QString toolTip);
    static bool geoLocked(const QJsonDocument& doc);
    static void setGeoLocked(QJsonDocument& doc, bool geoLocked);
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QDebug>

#include "MvQXmlQuery.h"

void MvQXmlQuery::getAtomicValues(QXmlResultItems& result, QStringList& lst)
{
    QXmlItem item(result.next());
    while (!item.isNull()) {
        // qDebug() << item.isNode() <<  item.isAtomicValue() << item.toAtomicValue();

        if (item.isAtomicValue()) {
            QVariant v = item.toAtomicValue();
            lst << v.toString().simplified();
        }
        item = result.next();
    }
}

void MvQXmlQuery::getAtomicValue(QXmlItem& node, QString& result)
{
    setFocus(node);

    setQuery("string()");
    evaluateTo(&result);
    result = result.simplified();
}

void MvQXmlQuery::getAttributes(QXmlItem& node, QMap<QString, QString>& att)
{
    QXmlResultItems result;

    setFocus(node);

    setQuery("./@*");
    evaluateTo(&result);

    QXmlItem item(result.next());
    while (!item.isNull()) {
        if (item.isNode()) {
            QString name, value;

            setFocus(item);

            setQuery("name()");
            evaluateTo(&name);
            name = name.simplified();

            // qDebug() << "name:" << name;

            setQuery("string()");
            evaluateTo(&value);
            value = value.simplified();

            // qDebug() << "value:" << value;

            if (name.isEmpty() == false) {
                att[name] = value;
            }
        }
        item = result.next();
    }
}

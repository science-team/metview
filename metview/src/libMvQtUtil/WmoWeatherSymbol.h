/***************************** LICENSE START ***********************************

 Copyright 2020 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <map>
#include <vector>

#include <QBrush>
#include <QColor>
#include <QPen>
#include <QPixmap>

class WmoWeatherSymbolGroup
{
public:
    WmoWeatherSymbolGroup(const std::string& name, const std::string& desc) :
        name_(name), desc_(desc) {}
    std::string name_;
    std::string desc_;
};

class WmoWeatherSymbol
{
public:
    WmoWeatherSymbol(const std::string& name, const std::string& title,
                     WmoWeatherSymbolGroup* group,
                     const std::string& body) :
        name_(name), title_(title), group_(group), body_(body) {}

    const std::string& name() const { return name_; }
    const std::string& title() const { return title_; }
    WmoWeatherSymbolGroup* group() const { return group_; }
    QPixmap pixmap(int w, int h, QColor fgCol = QColor(Qt::black), QPen boxPen = QPen(Qt::NoPen), QBrush boxBrush = QBrush(Qt::NoBrush));
    static void init();
    static WmoWeatherSymbol* find(const std::string& name);
    static const std::map<std::string, WmoWeatherSymbol*>& elems() { return elems_; }
    static const std::vector<WmoWeatherSymbolGroup*>& groups() { return groups_; }

protected:
    std::string name_;
    std::string title_;
    WmoWeatherSymbolGroup* group_{nullptr};
    static std::string svgRef_;
    std::string body_;
    static std::map<std::string, WmoWeatherSymbol*> elems_;
    static std::vector<WmoWeatherSymbolGroup*> groups_;
};

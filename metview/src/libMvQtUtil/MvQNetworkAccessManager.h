/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QtNetwork/QNetworkAccessManager>
#include <QObject>
#include <QUrl>

class MvQNetworkAccessManager : public QNetworkAccessManager
{
    Q_OBJECT

public:
    MvQNetworkAccessManager(QObject* parent = 0);

public slots:
    void slotReplyFinished(QNetworkReply*);

signals:
    void replyReadyToProcess(QNetworkReply*);

protected:
    QUrl redirectUrl(const QUrl&, const QUrl&) const;

    QUrl urlRedirectedTo_;
};

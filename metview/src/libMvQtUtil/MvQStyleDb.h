/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QStringList>
#include <QVector>
#include <QPixmap>

class QColor;
class QJsonArray;
class MvQStyleDb;

class MvQStyleDbItem
{
    friend class MvQStyleDb;

public:
    MvQStyleDbItem(QString name);

    QString name() const { return name_; }
    QString description() const { return description_; }
    QStringList colours() const { return colours_; }
    QStringList keywords() const { return keywords_; }
    QStringList parameters() const { return layers_; }
    QStringList layers() const { return layers_; }
    bool keywordMatch(QString pattern, Qt::CaseSensitivity cs) const;
    bool layerMatch(QString pattern, Qt::CaseSensitivity cs) const;
    bool colourMatch(QString pattern, Qt::CaseSensitivity cs) const;
    QPixmap makePixmapToHeight(QSize pixSize);
    QPixmap makePixmapToWidth(QSize pixSize);
    void paint(QPainter* painter, QRect rect);

protected:
    bool listMatch(QStringList lst, QString pattern, Qt::CaseSensitivity cs) const;
    QString previewFile() const;

    QString name_;
    QString description_;
    QStringList colours_;
    QStringList keywords_;
    QStringList layers_;
};

class MvQStyleDb
{
public:
    static MvQStyleDb* instance();
    QVector<MvQStyleDbItem*> items() const { return items_; }
    QStringList names() const;
    QStringList colours() const { return colours_; }
    QStringList keywords() const { return keywords_; }
    QStringList parameters() const { return layers_; }
    QStringList layers() const { return layers_; }

    MvQStyleDbItem* find(const std::string& name) const;
    int indexOf(const std::string& name) const;

protected:
    MvQStyleDb();
    void load();
    void loadMagicsDef();
    void collect(QStringList lst, QStringList& allLst) const;
    void toStringList(const QJsonArray& chAr, QStringList& lst) const;
    void toStringList(const QJsonArray& chAr, QStringList& lst, QStringList& allLst) const;

    static MvQStyleDb* instance_;
    QVector<MvQStyleDbItem*> items_;

    QStringList colours_;
    QStringList keywords_;
    QStringList parameters_;
    QStringList layers_;
};

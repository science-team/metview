/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <vector>
#include <string>

#include <QtGlobal>
#include <QMap>
#include <QString>

//#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
//#include <QtXmlPatterns/QXmlQuery>
//#else
//#include <QXmlQuery>
//#endif

class MvKeyProfile;

class MvQKeyManager
{
public:
    enum KeyType
    {
        FrameType,
        GribType,
        BufrType,
        PlaceMarkType,
        LayerType,
        BookmarkType
    };

    MvQKeyManager(KeyType t, std::string fProfUser = std::string());
    MvQKeyManager(const MvQKeyManager&);
    ~MvQKeyManager();

    MvQKeyManager* clone();
    MvKeyProfile* addProfile(const std::string&);
    void addProfile(MvKeyProfile*);
    MvKeyProfile* addProfile(const std::string&, MvKeyProfile*);
    MvKeyProfile* addProfile(const std::string&, MvKeyProfile*, KeyType);
    void changeProfile(const std::string&, MvKeyProfile*);
    KeyType keyType() { return keyType_; }
    QString keyTypeName(KeyType t) const;
    const std::string& profilePath() const { return fProfUser_; }

    const std::vector<MvKeyProfile*>& data() const { return data_; }
    MvKeyProfile* findProfile(std::string);
    bool isEmpty() { return (data_.size() == 0) ? true : false; }
    void update(MvQKeyManager*);
    void deleteProfile(int);
    void loadProfiles();
    bool loadProfiles(std::string, std::vector<MvKeyProfile*>&);
    void loadAllKeys(std::vector<MvKeyProfile*>&);
    void saveProfiles();
    void clear();
    QString findUniqueProfileName(QString);
    // void getAttributes(QXmlQuery &,QXmlItem &,QMap<QString,QString>&);
    bool predefinedKeysOnly() { return predefinedKeysOnly_; }

private:
    void createDefaultProfile(MvKeyProfile*);

    std::string fProfDefault_;
    std::string fProfAll_;
    std::string fProfUser_;

    std::vector<MvKeyProfile*> data_;
    std::vector<MvKeyProfile*> dataAll_;

    KeyType keyType_;
    static QMap<KeyType, QString> keyTypeName_;
    bool systemProfOnly_;
    bool predefinedKeysOnly_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQAbstractMessageScanner.h"

class GribMessageScanner : public MvQAbstractMessageScanner
{
    Q_OBJECT

public:
    GribMessageScanner(MvMessageMetaData*, QObject* parent = nullptr);
    ~GribMessageScanner() override;

protected slots:
    void slotProgress(int percent, MvKeyProfile* chunkProf, int chunkStart, int chunkSize);
    void slotTotalMessageNumComputed() override;

signals:
    void progress(int percent, MvKeyProfile* chunkProf, int chunkStart, int chunkSize);

protected:
    bool useThreadForScanning(qint64 fSize) const override;
    bool precomputeMessageNum() const override;
    void readMessages(MvKeyProfile* prof) override;
    void readMessage(MvKeyProfile* prof, codes_handle* ch, int msgCnt, int profIndex);

    bool hasMultiMessage_;
};

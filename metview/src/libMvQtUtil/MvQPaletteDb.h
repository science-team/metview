/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QPixmap>
#include <QString>

class QColor;
class QComboBox;
class QJsonArray;
class MvQPaletteDb;

class MvQPaletteDbItem
{
    friend class MvQPaletteDb;

public:
    MvQPaletteDbItem(QString name) :
        name_(name) {}

    QString name() const { return name_; }
    QList<QColor> colLst() const { return colLst_; }
    QStringList colNameLst() const { return colNameLst_; }

    QStringList colours() const { return cols_; }
    QStringList ecStyle() const { return ecStyle_; }
    QStringList keywords() const { return keywords_; }
    int nLevels() const { return nLevels_; }
    QString origin() const { return origin_; }
    QStringList parameters() const { return params_; }

    QPixmap makePixmap(QSize pixSize);
    void paint(QPainter* painter, QRect rect);

protected:
    void generateColLst();

    QString name_;
    QList<QColor> colLst_;
    QStringList colNameLst_;

    QStringList cols_;
    QStringList ecStyle_;
    QStringList keywords_;
    int nLevels_;
    QString origin_;
    QStringList params_;
};

class MvQPaletteDb
{
public:
    static MvQPaletteDb* instance();
    QVector<MvQPaletteDbItem*> items() const { return items_; }
    QStringList origins() const { return origins_; }
    QStringList colours() const { return colours_; }
    QList<int> nLevels() const { return nLevels_; }
    QStringList ecStyles() const { return ecStyles_; }
    QStringList keywords() const { return keywords_; }
    QStringList parameters() const { return parameters_; }

    MvQPaletteDbItem* find(const std::string& name) const;
    int indexOf(const std::string& name) const;

protected:
    MvQPaletteDb();
    void load();
    void toStringList(const QJsonArray& chAr, QStringList& lst) const;
    void toStringList(const QJsonArray& chAr, QStringList& lst, QStringList& allLst) const;

    static MvQPaletteDb* instance_;
    QVector<MvQPaletteDbItem*> items_;

    QStringList origins_;
    QStringList colours_;
    QStringList ecStyles_;
    QList<int> nLevels_;
    QStringList keywords_;
    QStringList parameters_;
};

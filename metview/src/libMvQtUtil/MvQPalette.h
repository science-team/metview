/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QColor>
#include <QRectF>

#include <string>
#include <vector>
#include <map>

class QPainter;
struct request;

class PaletteScanner
{
public:
    virtual void next(const std::string&, QColor, bool) = 0;
};

// class PaletteLoader
//{
// public:
//     static void load(request*);
// };

struct MvQPaletteItem
{
    MvQPaletteItem() {}
    MvQPaletteItem(const std::string& name, QColor col, bool pseudo) :
        name_(name),
        col_(col),
        pseudo_(pseudo) {}
    std::string name_;
    QColor col_;
    bool pseudo_;
};

class MvQPalette
{
public:
    MvQPalette(int);
    ~MvQPalette();

    static std::string toString(QColor);
    static QString toRgbString(QColor);
    static QString toRgbIntString(QColor col);
    static QString toHslString(QColor);
    static QString toHslIntString(QColor);
    static QString toStringAsList(QList<QColor> colLst);
    static QList<QColor> toColourList(QString txt);
    static QColor magics(const std::string&);

    static void scan(PaletteScanner&);
    static void load(request*);
    static bool isPseudo(const std::string& name);

    static void toHclLabF(QColor c, float& hue, float& chroma, float& luminance);
    static void toHclLab(QColor c, int& hue, int& chroma, int& luminance);
    static QColor fromHclLabF(float hue, float chroma, float luminance);
    static QColor fromHclLab(int hue, int chroma, int luminance);

    static void toHclLuvF(QColor c, float& hue, float& chroma, float& luminance);
    static void toHclLuv(QColor c, int& hue, int& chroma, int& luminance);
    static QColor fromHclLuvF(float hue, float chroma, float luminance);
    static QColor fromHclLuv(int hue, int chroma, int luminance);

    static void paintAlphaBg(QPainter* painter, QRect area);

protected:
    static QColor hexaCharToColour(const char*);

private:
    MvQPalette(const MvQPalette&);
    MvQPalette& operator=(const MvQPalette&);

    static std::vector<std::string> names_;
    static std::vector<std::string> pseudoColourNames_;
    static std::map<std::string, MvQPaletteItem> items_;
};

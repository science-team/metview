/***************************** LICENSE START ***********************************

 Copyright 2022 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <sstream>
#include <iostream>

#include <QStringList>

class QDateTime;
class QGraphicsItem;
class QModelIndex;
class QPoint;
class QRect;
class QRegion;
class QString;
class QVariant;
class QColor;

// Overload ostringstream for qt objects
std::ostream& operator<<(std::ostream&, const QString&);
std::ostream& operator<<(std::ostream&, const QModelIndex&);
std::ostream& operator<<(std::ostream&, const QVariant&);
std::ostream& operator<<(std::ostream&, const QStringList&);
std::ostream& operator<<(std::ostream&, const QRegion&);
std::ostream& operator<<(std::ostream&, const QRect&);
std::ostream& operator<<(std::ostream&, const QPoint&);
std::ostream& operator<<(std::ostream&, const QDateTime&);
std::ostream& operator<<(std::ostream&, QGraphicsItem*);
std::ostream& operator<<(std::ostream&, const QColor&);

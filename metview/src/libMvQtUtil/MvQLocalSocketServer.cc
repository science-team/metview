//============================================================================
// Copyright 2009-2017 ECMWF.
// This software is licensed under the terms of the Apache Licence version 2.0
// which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
// In applying this licence, ECMWF does not waive the privileges and immunities
// granted to it by virtue of its status as an intergovernmental organisation
// nor does it submit to any jurisdiction.
//============================================================================

#include "MvQLocalSocketServer.h"
#include "MvWindef.h"

#include <QApplication>
#include <QLocalServer>
#include <QLocalSocket>
#include <QDebug>

#ifndef METVIEW_ON_WINDOWS
#include <unistd.h>
#endif

MvQLocalSocketServer::MvQLocalSocketServer(QString serverId, QObject* parent) :
    QObject(parent),
    serverId_(serverId)
{
    QString name = generateServerName();

    // remove existings sockets with the same name
    QLocalServer::removeServer(name);

    // Create the server
    server_ = new QLocalServer(parent);

    // Restrict access to the socket
    server_->setSocketOptions(QLocalServer::UserAccessOption);

    // Start listening
    server_->listen(name);

    // qDebug() << "b" << b << server_->serverError() << server_->errorString();
    // qDebug() << "full" << server_->fullServerName();

    connect(server_, SIGNAL(newConnection()),
            this, SLOT(slotMessageReceived()));
}

MvQLocalSocketServer::~MvQLocalSocketServer() = default;

void MvQLocalSocketServer::slotMessageReceived()
{
    QLocalSocket* localSocket = server_->nextPendingConnection();
    if (!localSocket->waitForReadyRead(5000)) {
        // qDebug(localSocket->errorString().toLatin1());
        return;
    }
    QByteArray byteArray = localSocket->readAll();
    QString message = QString::fromUtf8(byteArray.constData());
    Q_EMIT messageReceived(message);
}

QString MvQLocalSocketServer::generateServerName()
{
    if (char* tmpPath = getenv("CODES_UI_SOCKETDIR")) {
        return QString(tmpPath) + "/" + serverId_ + "_" + QString::number(qApp->applicationPid());
    }

    return {};
}

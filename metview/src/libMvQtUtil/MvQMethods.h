/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QFont>
#include <QPixmap>
#include <QPoint>
#include <QPointF>
#include <QSettings>
#include <QString>
#include <QTextLayout>
#include <QVector>

#include "MvFwd.h"

class QAction;
class QButtonGroup;
class QComboBox;
class QDropEvent;
class QLineEdit;
class QPainter;
class QPointF;
class QStackedWidget;
class QTabWidget;
class QTreeView;
class QVBoxLayout;

#ifdef __APPLE__
#define MVQ_QSETTINGS_DEFAULT_ARGS QSettings::IniFormat, QSettings::UserScope, "ECMWF"
#else
#define MVQ_QSETTINGS_DEFAULT_ARGS QSettings::NativeFormat, QSettings::UserScope, "ECMWF"
#endif

namespace MvQ
{
void initComboBox(QSettings&, QString key, QComboBox* cb);
void initComboBoxByData(QSettings& settings, QString key, QComboBox* cb);
void initTabId(QSettings& settings, QString key, QTabWidget* tab);
void saveTabId(QSettings& settings, QString key, QTabWidget* tab);
void initTreeColumnWidth(QSettings& settings, QString key, QTreeView* tree);
void saveTreeColumnWidth(QSettings& settings, QString key, QTreeView* tree);
void initStacked(QSettings& settings, QString key, QStackedWidget* stacked);
void initButtonGroup(QSettings& settings, QString key, QButtonGroup* bg);
void initCheckableAction(QSettings& settings, QString key, QAction* ac);
void showTabLabel(QTabWidget* tab, int index, QPixmap pix);
void hideTabLabel(QTabWidget* tab, int index);
QString formatBoldText(QString txt, QColor col = {});
QString formatText(QString txt, QColor col = {}, bool bold = false);
QString formatTableThText(QString txt, QColor col = {});
QString formatTableTdText(QString txt, QColor col = {});
QString formatTableTrText(QString txt);
QString formatTableRow(QString col1Text, QString col2Text, bool boldCol1 = false);
void toClipboard(QString txt);
QString fromClipboard();
void initSettings(QString path);
QString iconPixmapPath(QString name);
void setLineEditTextFormat(QLineEdit* lineEdit, const QList<QTextLayout::FormatRange>& formats);
void clearLineEditTextFormat(QLineEdit* lineEdit);
bool hasOpenGLSupport();
void changeFontSize(QWidget*, int delta);
void setFontSize(QWidget*, int fs);
void showShortcutInContextMenu(QWidget*);
void showShortcutInContextMenu(QAction*);
void showShortcutInContextMenu(QList<QAction*>);
QString formatShortCut(QString);
QString formatShortCut(QAction*);
void addShortCutToToolTip(QWidget*);
void addShortCutToToolTip(QList<QAction*>);
int textWidth(const QFontMetrics& fm, QString txt, int len = -1);
int textWidth(const QFontMetrics& fm, QChar ch);
QPixmap makePixmap(QString path, int width, int height, QColor bg = QColor());
QPixmap makePixmapFromSvgTemplate(QString path, int width, int height, QColor col, QString templateCol, QString txt, QString templateTxt);
QPixmap fitPixmapToSize(QSize targetSize, QPixmap p, QColor bg);
#ifdef METVIEW
QPen makePen(const char* style, int width, const char* colour,
             Qt::PenCapStyle cap = Qt::SquareCap,
             Qt::PenJoinStyle join = Qt::BevelJoin);
QBrush makeBrush(const char* style, const char* colour);
#endif
QFont makeFont(const std::string& family, const std::string& style, int fontSize);
QFont makeFont(const std::string& family, bool bold, bool italic, bool underline, int fontSize);
#ifdef METVIEW
QWidget* makeLabelPanel(QString name, QWidget* parent, int fontSizeDelta = 0);
#endif
QFont findMonospaceFont(bool keepDefaultSize=false);
void safeDrawLine(const QPointF& p1, const QPointF& p2, QPainter* painter);
void safeDrawLine(const QPoint& p1, const QPoint& p2, QPainter* painter);
void drawPolyline(const QVector<QPointF>& pp, QPainter* painter);
void drawPolyline(const std::vector<double>& x, const std::vector<double>& y, QPainter* painter);
void showMessageBox(const std::string& msg, MvLogLevel level, const std::string& details ={});

template <typename T>
QPoint eventPos(T* event)
{
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    return event->position().toPoint();
#else
    return event->pos();
#endif
}

}  // namespace MvQ

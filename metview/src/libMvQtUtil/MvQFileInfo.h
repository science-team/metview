/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <QFileInfo>

class MvQFileInfo : public QFileInfo
{
public:
    MvQFileInfo(const std::string& path) :
        QFileInfo(QString::fromStdString(path)) {}
    MvQFileInfo(QString path) :
        QFileInfo(path) {}

    QString formatSize() const;
    QString formatModDate() const;
    QString formatPermissions() const;

    static QString formatSize(qint64);

private:
    static QString handleSizePrecision(double sizeInUnits, QString unitsString);
};

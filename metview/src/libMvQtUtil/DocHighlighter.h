/***************************** LICENSE START ***********************************

 Copyright 2020 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

#include <QtGlobal>
#include <string>
#include <QSyntaxHighlighter>

#if QT_VERSION < QT_VERSION_CHECK(5, 5, 0)
#include <QRegExp>
#endif
#include <QRegularExpression>


class DocHighlighter : public QSyntaxHighlighter
{
public:
    DocHighlighter(QTextDocument* parent, QString id);
    static void init();
    void toHtml(QString& html);

protected:
    void highlightBlock(const QString& text) override;

private:
    void load(QString);
    QColor rgb(QString name) const;

    struct HighlightingRule
    {
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
        QRegularExpression pattern;
#else
        QRegExp pattern;
#endif
        QTextCharFormat format;
    };
    HighlightingRule makeRule(QString, QTextCharFormat, bool);


    QList<HighlightingRule> rules_;
    HighlightingRule commentStartRule_;
    HighlightingRule commentEndRule_;
    static QString parFile_;
};


set(header_files_for_moc
    MvQAbstractMessageScanner.h
    MvQBufrMessageScanner.h
    MvQGribMessageScanner.h
    MvQFileList.h
    MvQNetworkAccessManager.h
    MvQNetworkAccessManager.h )

if(METVIEW_QT5)
  QT5_WRAP_CPP(MvQtUtil_MOC ${header_files_for_moc})
elseif(METVIEW_QT6)
  QT6_WRAP_CPP(MvQtUtil_MOC ${header_files_for_moc})
endif()


set(libMvQtUtil_srcs
    MvQAbstractMessageScanner.cc
    MvQBufrMessageScanner.cc
    MvQGribMessageScanner.cc
    MvQFileInfo.cc
    MvQFileList.cc
    MvQFileUtil.cc
    MvQJson.cc
    MvQKeyManager.cc
    MvQMethods.cc
    MvQNetworkAccessManager.cc
    MvQNetworkProxyFactory.cc
    MvQPalette.cc
    MvQPaletteDb.cc
    MvQProperty.cc
    MvQStreamOper.cc
    MvQStyleDb.cc
    MvQTheme.cc
    FontMetrics.cc
    DocHighlighter.cc
    WmoWeatherSymbol.cc
    WsDecoder.cc
    WsType.cc
)

set(libmvqtutil_includes ${METVIEW_QT_INCLUDE_DIRS} ${METVIEW_STANDARD_INCLUDES})

if (METVIEW_ODB)
    list(APPEND libMvQtUtil_srcs MvQOdbMetaData.cc MvQOdbMetaData.h)
    list(APPEND libmvqtutil_includes ${METVIEW_ODB_API_INCLUDE_DIRS})
endif()


ecbuild_add_library( TARGET              MvQtUtil
                     TYPE                STATIC
                     NOINSTALL
                     SOURCES             ${libMvQtUtil_srcs} ${generated_srcs} ${MvQtUtil_MOC}
                     TEMPLATES           ${common_templates}
                     PRIVATE_LIBS        ${STANDARD_METVIEW_LIBS} ${METVIEW_EXTRA_LIBRARIES}
                     PRIVATE_INCLUDES    ${libmvqtutil_includes}
                     PRIVATE_DEFINITIONS ${METVIEW_EXTRA_DEFINITIONS}
                     DEPENDS     
)

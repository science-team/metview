/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQAbstractMessageScanner.h"

#include <map>
#include <string>

#include "MvEccBufr.h"

class BufrMetaData;

class BufrMessageScanner : public MvQAbstractMessageScanner
{
    Q_OBJECT

public:
    BufrMessageScanner(MvMessageMetaData* data, QObject* parent = nullptr);
    ~BufrMessageScanner() override;

protected slots:
    void slotProgress(int, MvKeyProfile*, int, int, std::vector<MvEccBufrMessage*>);
    void slotTotalMessageNumComputed() override;

signals:
    void progress(int, MvKeyProfile*, int, int, std::vector<MvEccBufrMessage*>);

protected:
    bool useThreadForScanning(qint64 fSize) const override;
    bool precomputeMessageNum() const override;
    void run() override;
    void loadKeyProfile(MvKeyProfile* prof) override;
    void readMessages(MvKeyProfile* prof) override;
    // void readMessage(MvKeyProfile* prof, codes_handle* ch, int msgCnt, int profIndex);
    void readMessage(MvKeyProfile* prof, codes_bufr_header* bh, int profIndex);

    BufrMetaData* bufrData_;
};

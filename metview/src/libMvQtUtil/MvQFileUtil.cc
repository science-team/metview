/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQFileUtil.h"

#include <QFile>

bool MvQFileUtil::copy(QString srcPath, QString targetPath, QString& err)
{
    QFile src(srcPath);
    if (!src.exists()) {
        err = "Failed to copy source=" + srcPath + "  to destination=" +
              targetPath + " ! Source does not exist!";
        return false;
    }

    QFile target(targetPath);
    if (target.exists()) {
        target.remove();
    }

    if (!QFile::copy(srcPath, targetPath)) {
        err = "Copy failed!";
        return false;
    }

    return true;
}

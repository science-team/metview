/***************************** LICENSE START ***********************************

 Copyright 2022 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQStreamOper.h"

#include <QDebug>
#include <QDateTime>
#include <QGraphicsItem>
#include <QModelIndex>
#include <QPoint>
#include <QRect>
#include <QRegion>
#include <QString>
#include <QVariant>

//------------------------------------------
// Overload ostringstream for qt objects
//------------------------------------------

std::ostream& operator<<(std::ostream& stream, const QString& str)
{
    stream << str.toStdString();
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const QModelIndex& idx)
{
    QString s;
    QDebug ts(&s);
    ts << idx;
    stream << s.toStdString();
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const QVariant& v)
{
    QString s;
    QDebug ts(&s);
    ts << v;
    stream << s.toStdString();
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const QStringList& lst)
{
    QString s;
    QDebug ts(&s);
    ts << lst;
    stream << s.toStdString();
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const QRegion& r)
{
    QString s;
    QDebug ts(&s);
    ts << r;
    stream << s.toStdString();
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const QRect& r)
{
    QString s;
    QDebug ts(&s);
    ts << r;
    stream << s.toStdString();
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const QPoint& p)
{
    QString s;
    QDebug ts(&s);
    ts << p;
    stream << s.toStdString();
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const QDateTime& d)
{
    QString s;
    QDebug ts(&s);
    ts << d;
    stream << s.toStdString();
    return stream;
}

std::ostream& operator<<(std::ostream& stream, QGraphicsItem* d)
{
    QString s;
    QDebug ts(&s);
    ts << d;
    stream << s.toStdString();
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const QColor& c)
{
    QString s;
    QDebug ts(&s);
    ts << c;
    stream << s.toStdString();
    return stream;
}

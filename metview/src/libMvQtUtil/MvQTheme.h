/***************************** LICENSE START ***********************************

 Copyright 2021 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QHash>
#include <QMap>
#include <QPalette>
#include <QPen>
#include <QString>
#include <QStringList>

class QApplication;
class MvRequest;

class MvQThemeGroup
{
    friend class MvQTheme;

public:
    MvQThemeGroup(QString name) :
        name_(name) {}
    QBrush brush(QString name);
    QPen pen(QString name);
    QColor colour(QString name);
    QString styleSheet(QString name);

protected:
    QString name_;
    QHash<QString, QBrush> brush_;
    QHash<QString, QString> sh_;
};

class MvQTheme
{
public:
    static void init(QApplication* app, QStringList resources = {});
    static MvQTheme* add(QApplication*, QString);
    static QString id();
    static QBrush brush(QString group, QString item);
    static QPen pen(QString group, QString item, float width = 1., Qt::PenStyle style = Qt::SolidLine);
    static QColor colour(QString group, QString item);
    static QColor colour(QString item);
    static QString styleSheet(QString group, QString item);
    static QColor text();
    static QColor subText();
    static QColor accentText();
    static QColor imageBorder();
    static QColor treeItemBorder();
    static QColor htmlTableText();
    static QColor htmlTableBg();
    static QString htmlTableCss();
    static QString htmlTableCss(QString group);
    static QString highlighterFile();
    static QString icIconFile(const std::string& icPixPath);
    static bool boldFileInfoTitle();
    static QPixmap logo();
#ifdef ECCODES_UI
    static void writeCodesUiStyleName(QString);
#endif

private:
    MvQTheme(QApplication*, QString);
    static QString determineStyleName();
    void load(QApplication*, QString);
    void parse(QString, QMap<QString, QString>);
    void readPalette(QPalette::ColorGroup group, QMap<QString, QString>);
    void readGroup(QString, QMap<QString, QString>);
    void readOptions(QMap<QString, QString> vals);
    QColor makeColour(QString name);
    QBrush makeBrush(QString name);

#ifdef ECCODES_UI
    static QString readCodesUiStyleName();
#endif

    QPalette palette_;
    QString id_;
    static QStringList resources_;
    QMap<QString, MvQThemeGroup*> groups_;
    QMap<QString, QColor> cols_;
    bool boldHtmlTableTitle_{true};
    bool boldFileInfoTitle_{true};

    static MvQTheme* current_;
    static QMap<QString, MvQTheme*> themes_;
};

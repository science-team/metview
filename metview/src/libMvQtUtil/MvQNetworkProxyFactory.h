/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QtNetwork/QNetworkProxyFactory>
#include <QStringList>

class MvQNetworkProxyFactory : public QNetworkProxyFactory
{
public:
    MvQNetworkProxyFactory();
    QList<QNetworkProxy> queryProxy(const QNetworkProxyQuery& query = QNetworkProxyQuery());
    void updateSettings();

protected:
    QNetworkProxy proxy_;
    bool useProxy_;
    QStringList noProxyLst_;
};

/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QObject>

class QLocalServer;

class MvQLocalSocketServer : public QObject
{
    Q_OBJECT

public:
    MvQLocalSocketServer(QString serverId, QObject* parent);
    ~MvQLocalSocketServer() override;

protected Q_SLOTS:
    void slotMessageReceived();

Q_SIGNALS:
    void messageReceived(QString);

protected:
    QString generateServerName();

    QLocalServer* server_;
    QString serverId_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QtGui>

#include "TabsDialog.h"


TabsDialog::TabsDialog(QWidget* parent) :
    QDialog(parent)
{
    setupUi(this);  // this sets up GUI// setupFileMenu();

    /*
    connect (buttonBox, SIGNAL(accepted()),                   this, SLOT(done()));
    connect (buttonBox, SIGNAL(rejected()),                   this, SLOT(reject()));
    connect (lineEdit,  SIGNAL(textChanged(const QString &)), this, SLOT(setButtonStatus()));*/


    connect(buttonBox, SIGNAL(accepted()), this, SLOT(done()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(useSpacesCheckBox, SIGNAL(stateChanged(int)), this, SLOT(onUseSpacesChanged()));
}


TabsDialog::~TabsDialog() = default;

/*
// ---------------------------------------------------------------------------
// GotoLineDialog::setButtonStatus
// if there is text in the input box, then we can activate the 'OK' button,
// otherwise we should disable it. This function is called each time the text
// in the box is changed.
// ---------------------------------------------------------------------------

void GotoLineDialog::setButtonStatus()
{
    QPushButton *okButton = buttonBox->button(QDialogButtonBox::Ok);

    if (lineEdit->text().isEmpty())
    {
        okButton->setEnabled(false);
    }
    else
    {
        okButton->setEnabled(true);
    }
}

*/
// ---------------------------------------------------------------------------
// GotoLineDialog::setupUIBeforeShow
// sets up UI elements before the dialog is displayed.
// ---------------------------------------------------------------------------

void TabsDialog::setupUIBeforeShow(TabsSettings& settings)
{
    QString spaces = QString::number(settings.numSpacesInTab);

    useSpacesCheckBox->setChecked(settings.useSpacesForTabs);
    numSpacesLineEdit->setEnabled(settings.useSpacesForTabs);
    numSpacesLineEdit->setText(spaces);
    droppedIconSpacesCheckBox->setChecked(settings.useSpacesForDrops);
    droppedIconSpacesCheckBox->setEnabled(settings.useSpacesForTabs);
    displayTabsAndSpacesCheckBox->setChecked(settings.displayTabsAndSpaces);
    autoIndentCheckBox->setChecked(settings.autoIndent);
}


// ---------------------------------------------------------------------------
// TabsDialog::onUseSpacesChanged
// called when the user clicks the button to say whether to use spaces
// instead of tabs or not - we just update the GUI when this happens.
// ---------------------------------------------------------------------------

void TabsDialog::onUseSpacesChanged()
{
    numSpacesLineEdit->setEnabled(useSpacesCheckBox->isChecked());
    droppedIconSpacesCheckBox->setEnabled(useSpacesCheckBox->isChecked());
}


// ---------------------------------------------------------------------------
// TabsDialog::done
// called when the user clicks the 'OK' button - emits a signal to tell the
// text editor to change its settings
// ---------------------------------------------------------------------------

void TabsDialog::done()
{
    TabsSettings settings;
    int numSpaces = numSpacesLineEdit->text().toInt();

    settings.useSpacesForTabs = useSpacesCheckBox->isChecked();
    settings.numSpacesInTab = numSpaces;
    settings.useSpacesForDrops = droppedIconSpacesCheckBox->isChecked();
    settings.displayTabsAndSpaces = displayTabsAndSpacesCheckBox->isChecked();
    settings.autoIndent = autoIndentCheckBox->isChecked();

    emit tabsSettingsChanged(settings);
    close();
}

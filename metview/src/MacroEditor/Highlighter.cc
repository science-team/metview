/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Highlighter.h"

#include <QtGlobal>
#include <QtGui>


Highlighter::Highlighter(QTextDocument* parent) :
    QSyntaxHighlighter(parent)
{
}


void Highlighter::highlightBlock(const QString& text)
{
    foreach (HighlightingRule rule, highlightingRules) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
        QRegularExpression expression(rule.pattern);
        QRegularExpressionMatch rmatch;
        int index = text.indexOf(expression, 0, &rmatch);
        while (index >= 0) {
            int length = rmatch.capturedLength();
            setFormat(index, length, rule.format);
            index = text.indexOf(expression, index + length, &rmatch);
        }
#else
        QRegExp expression(rule.pattern);
        int index = text.indexOf(expression);
        while (index >= 0) {
            int length = expression.matchedLength();
            setFormat(index, length, rule.format);
            index = text.indexOf(expression, index + length);
        }
#endif
    }
}

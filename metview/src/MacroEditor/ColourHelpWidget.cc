/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "ColourHelpWidget.h"
#include "MvQMethods.h"

#include "ui_ColourHelpWidget.h"

ColourHelpWidget::ColourHelpWidget(QWidget* parent) :
    QWidget(parent),
    ui_(new Ui::ColourHelpWidget)
{
    ui_->setupUi(this);

    connect(ui_->insertPb, SIGNAL(clicked()), this,
            SLOT(slotInsert()));

    connect(ui_->copyPb, SIGNAL(clicked()), this,
            SLOT(slotCopy()));
}

void ColourHelpWidget::setColour(QString col)
{
    ui_->editor->initColour(col);
}

void ColourHelpWidget::slotInsert()
{
    QString v = ui_->editor->currentValue();
    if (!v.isEmpty()) {
        emit insertRequested(v);
    }
}

void ColourHelpWidget::slotCopy()
{
    QString v = ui_->editor->currentValue();
    if (!v.isEmpty()) {
        MvQ::toClipboard(v);
    }
}

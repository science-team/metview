/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <vector>

#include "LanguageHelper.h"


// -----------------------------------------------------------------------------------
// class LanguageHelperFactory
// A simple object factory which handles a set of programming language helper objects.
// -----------------------------------------------------------------------------------

class LanguageHelperFactory
{
public:
    LanguageHelperFactory();
    ~LanguageHelperFactory();


    std::vector<LanguageHelper*>& languageHelpers();              // returns a list of language helper objects
    LanguageHelper* languageHelper(const QString& languageName);  // returns a specified language helper object


private:
    std::vector<LanguageHelper*> languageHelpers_;
};

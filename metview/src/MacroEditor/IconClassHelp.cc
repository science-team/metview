/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "IconClassHelp.h"

#include "ConfigLoader.h"
#include "MvIconLanguage.h"

#include <algorithm>

std::map<std::string, IconClassHelp*> IconClassHelp::items_;

IconClassHelp::IconClassHelp(const std::string& name, request* r) :
    MvIconClassCore(name, r)
{
    items_[name] = this;
}

void IconClassHelp::load(request* r)
{
    new IconClassHelp(get_value(r, "class", 0), r);
}

IconClassHelp* IconClassHelp::find(const std::string& name)
{
    for (auto& item : items_) {
        IconClassHelp* kind = item.second;

        std::string strName = name;
        std::transform(strName.begin(), strName.end(), strName.begin(), ::tolower);

        std::string s = kind->name();
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);
        if (s == strName) {
            return kind;
        }
    }

    return nullptr;
}

IconClassHelp* IconClassHelp::findByMacroFunction(const std::string& funcName)
{
    for (auto& item : items_) {
        IconClassHelp* kind = item.second;

        std::string strName = funcName;
        std::transform(strName.begin(), strName.end(), strName.begin(), ::tolower);

        std::string s = kind->macroFunction();
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);
        if (s == strName) {
            return kind;
        }
    }

    return nullptr;
}

static SimpleLoader<IconClassHelp> loadClassesHelp("object", 0);

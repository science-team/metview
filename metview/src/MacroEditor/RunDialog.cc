/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QtGui>

#include "RunDialog.h"


RunDialog::RunDialog(QWidget* parent) :
    QDialog(parent)
{
    setupUi(this);

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(done()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    //    connect (useSpacesCheckBox, SIGNAL(stateChanged(int)), this, SLOT(onUseSpacesChanged()));
}


RunDialog::~RunDialog() = default;


// ---------------------------------------------------------------------------
// RunDialog::setupUIBeforeShow
// sets up UI elements before the dialog is displayed.
// ---------------------------------------------------------------------------

void RunDialog::setupUIBeforeShow(MacroRunOptions& options)
{
    QString pause = QString::number(options.pause_);

    highlightLineCheckBox->setChecked(options.sendLines_);
    waitCheckBox->setChecked(options.waitMode_);
    traceCheckBox->setChecked(options.trace_);
    RunModeComboBox->setEditText(QString(options.runMode_.c_str()));
    PauseComboBox->setEditText(pause);
}


// ---------------------------------------------------------------------------
// RunDialog::done
// called when the user clicks the 'OK' button - emits a signal to tell the
// editor to change its settings
// ---------------------------------------------------------------------------

void RunDialog::done()
{
    MacroRunOptions options;

    options.sendLines_ = highlightLineCheckBox->isChecked();
    options.waitMode_ = waitCheckBox->isChecked();
    options.trace_ = traceCheckBox->isChecked();


    if (PauseComboBox->currentIndex() == 0)
        options.pause_ = 0;
    else {
        QStringList strlist = PauseComboBox->currentText().split(" ");
        options.pause_ = strlist[0].toInt();
    }


    // run mode. At the time of writing, the first entry is
    // 'Prepare (default)' - we want to catch this case and only
    // take the first part of the string.

    options.runMode_ = RunModeComboBox->currentText().toStdString();

    QStringList strlist = RunModeComboBox->currentText().split(" ");

    if (strlist.count() > 1)
        options.runMode_ = strlist[0].toStdString();


    emit runOptionsChanged(options);

    close();
}

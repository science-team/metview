/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "ui_CodeTemplateDialog.h"
#include "CodeTemplateDialog.h"
#include "TabsSettings.h"


// VerbFunctionHelpDialog
// the dialog class itself

class CodeTemplateDialog : public QDialog, private Ui::CodeTemplateDialog
{
    Q_OBJECT

public:
    CodeTemplateDialog(QString& templateListFilePath, QWidget* parent = 0);
    ~CodeTemplateDialog() = default;

    void reloadCodeTemplates(TabsSettings& tabSettings);


signals:
    void insertCodeTemplate(QString code);


public slots:
    void onItemChanged(QListWidgetItem* item, QListWidgetItem* previous);
    void onInsertButtonClicked();


private:
    QString templateListFilePath_;
    QStringList templateCodeBodies_;
};

/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "FunctionHelpWidget.h"
#include "LanguageHelper.h"
#include "IconClassHelp.h"
#include "FunctionListWidget.h"
#include "FunctionInfoWidget.h"
#include "VerbFunctionHelpWidget.h"
#include "MvMiscellaneous.h"

#include <QStackedWidget>
#include <QVBoxLayout>
#include <QMessageBox>

FunctionHelpWidget::FunctionHelpWidget(QWidget* parent) :
    QWidget(parent),
    language_(nullptr)
{
    auto* vb = new QVBoxLayout(this);
    vb->setContentsMargins(0, 0, 0, 0);
    vb->setSpacing(0);

    stackedW_ = new QStackedWidget(this);
    vb->addWidget(stackedW_, 1);

    functionListW_ = new FunctionListWidget(this);
    stackedW_->addWidget(functionListW_);
    Q_ASSERT(FunctionViewIndex == stackedW_->count() - 1);

    verbHelper_ = new VerbFunctionHelpWidget(this);
    stackedW_->addWidget(verbHelper_);
    Q_ASSERT(VerbHelpViewIndex == stackedW_->count() - 1);

    functionInfoW_ = new FunctionInfoWidget(this);
    stackedW_->addWidget(functionInfoW_);
    Q_ASSERT(FunctionInfoViewIndex == stackedW_->count() - 1);

    connect(functionListW_, SIGNAL(helperRequested(VerbFunctionHelpFunction*, int)),
            this, SLOT(showHelper(VerbFunctionHelpFunction*, int)));

    connect(functionListW_, SIGNAL(webDocRequested(VerbFunctionHelpFunction*, int)),
            this, SLOT(showWebDoc(VerbFunctionHelpFunction*, int)));

    connect(functionListW_, SIGNAL(insertRequested(QString)),
            this, SIGNAL(insertRequested(QString)));

    connect(verbHelper_, SIGNAL(functionListRequested()),
            this, SLOT(showFunctionList()));

    connect(verbHelper_, SIGNAL(insertRequested(QString)),
            this, SIGNAL(insertRequested(QString)));

    connect(functionInfoW_, SIGNAL(functionListRequested()),
            this, SLOT(showFunctionList()));

    connect(functionInfoW_, SIGNAL(insertRequested(QString)),
            this, SIGNAL(insertRequested(QString)));

    connect(functionInfoW_, SIGNAL(webDocRequested(VerbFunctionHelpFunction*, int)),
            this, SLOT(showWebDoc(VerbFunctionHelpFunction*, int)));
}

void FunctionHelpWidget::setLanguage(LanguageHelper* language)
{
    if (!language)
        return;

    if (language->hasVerbFunctionHelp()) {
        language->functionHelp()->init();
    }

    language_ = language;
    functionListW_->reload(language->functionHelp());
    verbHelper_->setLanguage(language);
    functionInfoW_->setLanguage(language);
}

void FunctionHelpWidget::setCodeEditorTheme(EditorTheme* theme, QFont font)
{
    verbHelper_->setCodeEditorTheme(theme, font);
}

bool FunctionHelpWidget::adjustContents(QString functionName, QString paramName)
{
    functionName = functionName.toLower();
    paramName = paramName.toLower();
    VerbFunctionHelpFunction* verb = nullptr;
    VerbFunctionHelpParam* param = nullptr;


    if (functionName.isEmpty()) {
        return false;
    }
    else {
        verb = verbHelper_->function(functionName);
    }

    if (verb) {
        param = verb->paramFromName(paramName);
        if (param && verb->iconClass()) {
            verbHelper_->showParam(verb, param);
            stackedW_->setCurrentIndex(VerbHelpViewIndex);
            return true;
        }
        showHelper(verb, 0);
        return true;
    }

    return false;
}

void FunctionHelpWidget::showHelper(VerbFunctionHelpFunction* fn, int descIndex)
{
    if (!fn)
        return;

    if (fn->iconClass()) {
        verbHelper_->showFunction(fn->name());
        stackedW_->setCurrentIndex(VerbHelpViewIndex);
    }
    else {
        functionListW_->selectFunction(fn, descIndex);
        functionInfoW_->showFunction(fn, descIndex);
        stackedW_->setCurrentIndex(FunctionInfoViewIndex);
    }
}

void FunctionHelpWidget::showWebDoc(VerbFunctionHelpFunction* fn, int descIndex)
{
    if (!fn)
        return;

    QString web = language_->functionHelp()->url(fn, descIndex);
    if (!web.isEmpty()) {
        std::string err;
        if (!metview::openInBrowser(web.toStdString(), err)) {
            QMessageBox::warning(this, "Code editor - Metview",
                                 "Failed to show documentation in web browser!<br><br>" + QString::fromStdString(err));
        }
    }
}

void FunctionHelpWidget::showFunctionList()
{
    stackedW_->setCurrentIndex(FunctionViewIndex);
}

void FunctionHelpWidget::slotInsert()
{
}

void FunctionHelpWidget::readSettings(QSettings& settings)
{
    settings.beginGroup("FunctionHelpWiget");
    functionListW_->readSettins(settings);
    verbHelper_->readSettings(settings);
    functionInfoW_->readSettings(settings);
    settings.endGroup();
}

void FunctionHelpWidget::writeSettings(QSettings& settings)
{
    settings.beginGroup("FunctionHelpWiget");
    functionListW_->writeSettings(settings);
    verbHelper_->writeSettings(settings);
    functionInfoW_->writeSettings(settings);
    settings.endGroup();
}

/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "LanguageHelper.h"

// --------------------------------------------------
// class MacroHelper
// A language helper specific to the Macro language
// --------------------------------------------------

class MacroHelper : public LanguageHelper
{
public:
    MacroHelper();
    virtual ~MacroHelper();

    Highlighter* createHighlighter(QTextDocument* parent = 0);
    bool canCheckSyntax() { return true; }
    bool canDebug() { return true; }
    bool canRun() { return true; }
    bool canHaveRunOptions() const { return true; }
    bool runAsService() { return true; }
    // bool isHelpAvailableForFunction(QString);
    QChar functionParameterAssignChar() const;
    bool isHeaderLine(const QString& text);                                  // is the text a header line (e.g. "# Metview Macro")?
    QString requiredHeaderLine() { return QString("# Metview Macro\n\n"); }  // required header line to be added automatically
    QString pathToFunctionList();
    QString pathToTemplateList();
    bool hasVerbFunctionHelp() const { return true; }
    void whatFunction(QStringList t, int cursorPosInLine, QString& functionName, QString& paramName) const;
    void whatColour(QString line, int cursorPosInLine, QString& colName) const;
    bool isConvertableToPython() const override {return true;}

private:
    QString whatParam(QString line, int cursorPosInLine) const;

    QString pathToMacroFunctionList_;
    QString pathToMacroDeprecatedFunctionList_;
    QString pathToMacroTemplateList_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "LanguageHelper.h"
#include "ShellHelper.h"
#include "Highlighter.h"

// -----------------------------------------------------------------------
// ------------------- Syntax highlighter code ---------------------------
//        this class is used by the PlainTextHelper object, defined below
// -----------------------------------------------------------------------


class ShellHighlighter : public Highlighter
{
public:
    ShellHighlighter(QTextDocument* parent = nullptr) :
        Highlighter(parent) {}
};

// -----------------------------------------------------------------------
// -------------------------- ShellHelper code ---------------------------
// -----------------------------------------------------------------------


// ---------------------------------------------------------------------------
// ShellHelper::ShellHelper
// constructor
// ---------------------------------------------------------------------------

ShellHelper::ShellHelper()
{
    languageName_ = "Shell";
    className_ = "SHELL";
    serviceName_ = R"((x=`dirname "%s"`;cd "$x" ; sh "%s"))";
    iconName_ = "SHELL";
}


// ---------------------------------------------------------------------------
// ShellHelper::~ShellHelper
// destructor
// ---------------------------------------------------------------------------

ShellHelper::~ShellHelper() = default;


// ---------------------------------------------------------------------------
// ShellHelper::~createHighlighter
// creates a new syntax highlighter object
// ---------------------------------------------------------------------------

Highlighter* ShellHelper::createHighlighter(QTextDocument* parent)
{
    return new ShellHighlighter(parent);
}


// ---------------------------------------------------------------------------
// ShellHelper::isHeaderLine
// decides whether the given line is a shell script header line
// ---------------------------------------------------------------------------

bool ShellHelper::isHeaderLine(const QString& text)
{
    return (text.contains("#") &&
            text.contains("bin") &&
            (text.contains("sh") || text.contains("ksh") || text.contains("csh") || text.contains("bash")));
}

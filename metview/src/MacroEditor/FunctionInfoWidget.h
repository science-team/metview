/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QSettings>
#include <QWidget>

class LanguageHelper;
class VerbFunctionHelpFunction;
class QTreeWidgetItem;


namespace Ui
{
class FunctionInfoWidget;
}

class FunctionInfoWidget : public QWidget
{
    Q_OBJECT

public:
    FunctionInfoWidget(QWidget* parent = 0);
    ~FunctionInfoWidget() {}

    void clear();
    void setLanguage(LanguageHelper*);
    void showFunction(VerbFunctionHelpFunction* function, int);
    bool hasData() const;

    void readSettings(QSettings&);
    void writeSettings(QSettings&);

protected slots:
    void slotInsert();
    void slotCopy();
    void slotWebDoc();

signals:
    void functionListRequested();
    void insertRequested(QString);
    void webDocRequested(VerbFunctionHelpFunction*, int);

protected:
    void loadWebDoc();
    void hideDescription();
    void setDescriptions(VerbFunctionHelpFunction* fn);

    Ui::FunctionInfoWidget* ui_;
    LanguageHelper* language_;
    VerbFunctionHelpFunction* fn_;
    int descIndex_;
    QString cssStyle_;
};

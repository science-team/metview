/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QTextDocument>

#include "ui_FindTextDialog.h"


class FindTextDialog : public QDialog, private Ui::Dialog
{
    Q_OBJECT

public:
    FindTextDialog(QWidget* parent = 0);
    ~FindTextDialog();

    void setupUIBeforeShow(const QString& findText, bool showReplaceOptions, bool isSelection);
    bool isBackwards();
    bool hasPerformedFind() { return hasPerformedFind_; }
    void repeatLastFind();

signals:
    void find(const QString&, QTextDocument::FindFlags);                               // emitted when the user clicks 'find'
    void replace(const QString&, const QString&, QTextDocument::FindFlags);            // emitted when the user clicks 'replace'
    void replaceInSelected(const QString&, const QString&, QTextDocument::FindFlags);  // emitted when the user clicks 'replace all in selected'
    void replaceAll(const QString&, const QString&, QTextDocument::FindFlags);         // emitted when the user clicks 'replace all in file'
    void closed();                                                                     // emitted when the dialog is closed

public slots:
    void accept();
    void setFindButtonStatus();
    void onReplaceButtonClicked();
    void onReplaceSelectedButtonClicked();
    void onReplaceAllButtonClicked();
    void onSelectionChanged(bool);


private:
    bool hasPerformedFind_;
    QTextDocument::FindFlags findFlags();
};

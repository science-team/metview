/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "FunctionInfoWidget.h"

#include <QDebug>
#include <QLabel>
#include <QTreeWidgetItem>
#include <QUrl>
#include <QVBoxLayout>

#include "LanguageHelper.h"
#include "IconClassHelp.h"
#include "VerbFunctionHelp.h"
#include "MessageLabel.h"
#include "MvQMethods.h"
#include "MvQTheme.h"

#include "ui_FunctionInfoWidget.h"

FunctionInfoWidget::FunctionInfoWidget(QWidget* parent) :
    QWidget(parent),
    ui_(new Ui::FunctionInfoWidget),
    language_(nullptr),
    fn_(nullptr),
    descIndex_(0)
{
    ui_->setupUi(this);

    connect(ui_->showFunctionListTb, SIGNAL(clicked()),
            this, SIGNAL(functionListRequested()));

    // Set css for the text formatting
    QString cssDoc =
        "td {background: " + MvQTheme::htmlTableBg().name() + "; color: " + MvQTheme::htmlTableText().name() +
        "; padding-left: 4px; padding-right: 3px;}" +
        "th {background: " + MvQTheme::htmlTableBg().name() + "; color: " + MvQTheme::subText().name() +
        "; padding-left: 4px; padding-right: 3px;}";

    ui_->descBrowser->setReadOnly(true);
    ui_->descBrowser->setWordWrapMode(QTextOption::WordWrap);
    ui_->descBrowser->document()->setDefaultStyleSheet(cssDoc);

    connect(ui_->insertTb, SIGNAL(clicked()),
            this, SLOT(slotInsert()));

    connect(ui_->copyTb, SIGNAL(clicked()),
            this, SLOT(slotCopy()));

    connect(ui_->webDocTb, SIGNAL(clicked()),
            this, SLOT(slotWebDoc()));

    hideDescription();
}

void FunctionInfoWidget::clear()
{
    ui_->functionNameLabel->setText("");
    ui_->descBrowser->clear();
    hideDescription();
    fn_ = nullptr;
}

void FunctionInfoWidget::hideDescription()
{
    ui_->descBrowser->hide();
}

void FunctionInfoWidget::setLanguage(LanguageHelper* language)
{
    clear();
    language_ = language;
}

void FunctionInfoWidget::showFunction(VerbFunctionHelpFunction* fn, int descIndex)
{
    hideDescription();

    if (!fn) {
        clear();
        return;
    }

    fn_ = fn;
    descIndex_ = descIndex;
    Q_ASSERT(descIndex_ >= 0);
    ui_->functionNameLabel->setText("<b> " + fn_->name() + "()</b>");

    ui_->descBrowser->clear();
    QString txt = "<table width=\'100%\'>";
    txt += "<tr><th><b>Category</b></th><th><b>Description<b></th></tr>";
    for (int i = 0; i < fn->descriptions().size(); i++) {
        if (i != descIndex_) {
            txt += "<tr><td>" + fn->descriptions()[i].first + "</td><td>" + fn->descriptions()[i].second + "</td></tr>";
        }
        else {
            txt += "<tr><td>" + fn->descriptions()[i].first + "</td><td>" + fn->descriptions()[i].second + "</td></tr>";
        }
    }
    // txt += "<tr><td class=\'first\'><b>Category</b></td><td>" + fn_->descriptions()[descIndex_].first + "</td></tr>";
    // txt += "<tr><td class=\'first\'><b>Description&nbsp;</b></td><td class=\'main\'>" + fn->descriptionAsStr(descIndex_) + "</td></tr>";
    txt += "</table>";
    ui_->descBrowser->show();
    ui_->descBrowser->insertHtml(txt);
}


bool FunctionInfoWidget::hasData() const
{
    return (language_ && language_->hasVerbFunctionHelp() &&
            !language_->functionHelp()->isEmpty());
}

void FunctionInfoWidget::slotInsert()
{
    if (fn_ && !fn_->name().isEmpty())
        emit insertRequested(fn_->name() + "()");
}

void FunctionInfoWidget::slotCopy()
{
    if (fn_ && !fn_->name().isEmpty())
        MvQ::toClipboard(fn_->name() + "()");
}

void FunctionInfoWidget::slotWebDoc()
{
    if (fn_)
        emit webDocRequested(fn_, descIndex_);
}


void FunctionInfoWidget::readSettings(QSettings&)
{
}

void FunctionInfoWidget::writeSettings(QSettings&)
{
}

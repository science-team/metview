/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "LanguageHelper.h"
#include "PlainTextHelper.h"
#include "Highlighter.h"

// -----------------------------------------------------------------------
// ------------------- Syntax highlighter code ---------------------------
//        this class is used by the PlainTextHelper object, defined below
// -----------------------------------------------------------------------


class PlainTextHighlighter : public Highlighter
{
public:
    PlainTextHighlighter(QTextDocument* parent = nullptr) :
        Highlighter(parent) {}
};


// -----------------------------------------------------------------------
// -------------------------- PlainTextHelper code ---------------------------
// -----------------------------------------------------------------------


// ---------------------------------------------------------------------------
// PlainTextHelper::PlainTextHelper
// constructor
// ---------------------------------------------------------------------------

PlainTextHelper::PlainTextHelper()
{
    languageName_ = "Plain Text";
    className_ = "NOTE";
    serviceName_ = "none";
    iconName_ = "NOTE";
}


// ---------------------------------------------------------------------------
// PlainTextHelper::~PlainTextHelper
// destructor
// ---------------------------------------------------------------------------

PlainTextHelper::~PlainTextHelper() = default;


// ---------------------------------------------------------------------------
// PlainTextHelper::createHighlighter
// creates a new syntax highlighter object
// ---------------------------------------------------------------------------

Highlighter* PlainTextHelper::createHighlighter(QTextDocument* parent)
{
    return new PlainTextHighlighter(parent);
}

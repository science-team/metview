/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "LanguageHelper.h"

// --------------------------------------------------
// class PythonHelper
// A language helper specific to the Python language
// --------------------------------------------------

class PythonHelper : public LanguageHelper
{
public:
    PythonHelper();
    virtual ~PythonHelper();

    Highlighter* createHighlighter(QTextDocument* parent = 0);
    bool canCheckSyntax() { return false; }
    bool canDebug() { return false; }
    bool canRun() { return true; }
    bool runAsService() { return false; }
    QChar functionParameterAssignChar() const;
    bool isHeaderLine(const QString& text);  // is the text a header line (e.g. "# Metview Macro")?
    QString autoHeaderText();
    QString pathToFunctionList();
    QString scopePrefix() const { return QString("mv."); }
    bool hasVerbFunctionHelp() const { return true; }
    void whatFunction(QStringList t, int cursorPosInLine, QString& functionName, QString& paramName) const;
    void whatColour(QString line, int cursorPosInLine, QString& colName) const;

protected:
    QString pathToMetviewFunctionList_;

private:
    QString whatParam(QString line, int cursorPosInLine) const;
};

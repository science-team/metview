/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Highlighter.h"
#include "VerbFunctionHelp.h"

/* -----------------------------------------------------------------------
    class LanguageHelper

    Base class. There will be one derived class for each programming
    language that the editor will be able to handle.
    The helper classes will provide the editor with any language-specific
    functionality (e.g. syntax highlighting, how to execute, etc.)
   ----------------------------------------------------------------------- */


class QTextDocument;


class LanguageHelper
{
public:
    LanguageHelper();
    virtual ~LanguageHelper() = default;
    LanguageHelper(const LanguageHelper&) = delete;
    LanguageHelper& operator=(const LanguageHelper&) = delete;

    virtual Highlighter* createHighlighter(QTextDocument* parent = 0) = 0;
    virtual bool canCheckSyntax() = 0;
    virtual bool canRun() = 0;
    virtual bool canDebug() = 0;
    virtual bool canHaveRunOptions() const { return false; }
    virtual bool runAsService() = 0;
    virtual QString pathToFunctionList() { return QString(""); }
    virtual QString pathToTemplateList() { return QString(""); }
    virtual QChar functionParameterAssignChar() const { return {}; }

    virtual QString commentBlock(const QString& text);            // makes the text lines into a comment block
    virtual bool isHeaderLine(const QString&) { return false; }   // is the text a header line (e.g. "# Metview Macro")?
    virtual QString requiredHeaderLine() { return QString(""); }  // required header line to be added automatically
    virtual bool isAutoLicenceTextAllowed() { return true; }      // is 'auto licence text' allowed for this language?
    virtual QString autoHeaderText() { return QString(""); }      // standard header line to add to empty file

    QString name() const { return languageName_; }
    QString className() const { return className_; }
    QString serviceName() const { return serviceName_; }
    QString iconName() const { return iconName_; }
    virtual void whatFunction(QStringList, int, QString&, QString&) const {}
    virtual void whatColour(QString /*line*/, int /*cursorPosInLine*/, QString& /*colName*/) const {}
    virtual bool hasVerbFunctionHelp() const { return false; }
    virtual QString scopePrefix() const { return {}; }

    // convenience function to test whether the given string matches any of the language's names
    bool isLanguageName(const QString& testName) { return name() == testName || className() == testName; }
    VerbFunctionHelp* functionHelp() const { return functionHelp_; }
    virtual bool isConvertableToPython() const {return false;}

protected:
    QString languageName_;
    QString className_;
    QString serviceName_;
    QString iconName_;
    VerbFunctionHelp* functionHelp_{nullptr};
};

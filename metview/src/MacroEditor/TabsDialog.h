/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "ui_TabsDialog.h"
#include "TabsSettings.h"


class TabsDialog : public QDialog, private Ui::TabsDialog
{
    Q_OBJECT

public:
    TabsDialog(QWidget* parent = 0);
    ~TabsDialog();
    void setupUIBeforeShow(TabsSettings& settings);

signals:
    // emitted when the user says 'ok'
    // probably better to send the whole structure, not just a reference
    void tabsSettingsChanged(TabsSettings settings);


public slots:
    void done();
    // void setButtonStatus();
    void onUseSpacesChanged();
};

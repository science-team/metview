/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

// convenience class to hold the macro run options. We won't bother encapsulating the
// members...
class MacroRunOptions
{
public:
    bool trace_;
    bool sendLines_;
    int waitMode_;
    int pause_;
    std::string runMode_;

    MacroRunOptions() :
        trace_(false),
        sendLines_(false),
        waitMode_(0),
        pause_(0),
        runMode_("prepare") {}
};

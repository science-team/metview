/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QWidget>
#include <QMap>
#include <QAbstractItemModel>
#include <QStyledItemDelegate>
#include <QPen>
#include <QSettings>

class LanguageHelper;
class QTreeWidgetItem;
class IconClassHelp;
class VerbFunctionHelp;
class VerbFunctionHelpFunction;
class QSortFilterProxyModel;

namespace Ui
{
class FunctionListWidget;
}

class FunctionListDelegate : public QStyledItemDelegate
{
public:
    explicit FunctionListDelegate(QWidget* parent = nullptr);
    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const;

    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const;

private:
    QPen borderPen_;
};

class FunctionListModel : public QAbstractItemModel
{
public:
    FunctionListModel(QObject* parent);

    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex& index) const;
    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex&) const;

    void reload(VerbFunctionHelp*);
    void clear();
    VerbFunctionHelpFunction* functionObj(const QModelIndex& idx);
    int functionDescriptionIndex(const QModelIndex& idx);
    QModelIndex functionToIndex(VerbFunctionHelpFunction* fn, int descIndex);
    QString scopePrefix() const;

private:
    bool hasData() const;

    VerbFunctionHelp* data_;
};

class FunctionListWidget : public QWidget
{
    Q_OBJECT

public:
    FunctionListWidget(QWidget* parent = 0);
    ~FunctionListWidget() {}

    void clear();
    void reload(VerbFunctionHelp*);
    void selectFunction(VerbFunctionHelpFunction* fn, int descIndex);
    void readSettins(QSettings&);
    void writeSettings(QSettings&);

signals:
    void insertRequested(QString);
    void helperRequested(VerbFunctionHelpFunction*, int);
    void webDocRequested(VerbFunctionHelpFunction*, int);

protected slots:
    void slotDoubleClicked(const QModelIndex& idx);
    void slotWebDoc();
    void slotIconParam();
    void slotInsert();
    void slotCopy();
    void slotFilter(QString);
    void slotCurrentChanged(const QModelIndex&, const QModelIndex&);
    void slotFontSizeDown();
    void slotFontSizeUp();

protected:
    void showWebDoc(const QModelIndex& idx);
    void showIconParams(const QModelIndex& idx);
    void checkButtonState();

    Ui::FunctionListWidget* ui_;
    FunctionListModel* model_;
    QSortFilterProxyModel* sortModel_;
};

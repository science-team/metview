/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "LanguageHelperFactory.h"
#include "MacroHelper.h"
#include "MagMLHelper.h"
#include "ShellHelper.h"
#include "PythonHelper.h"
#include "PlainTextHelper.h"


// ---------------------------------------------------------------------------
// LanguageHelperFactory::LanguageHelperFactory
// constructor. Here, we populate the list of available language helpers
// ---------------------------------------------------------------------------

LanguageHelperFactory::LanguageHelperFactory()
{
    languageHelpers_.push_back(new MacroHelper);
    languageHelpers_.push_back(new MagMLHelper);
    languageHelpers_.push_back(new ShellHelper);
    languageHelpers_.push_back(new PythonHelper);
    languageHelpers_.push_back(new PlainTextHelper);
}


// ---------------------------------------------------------------------------
// LanguageHelperFactory::~LanguageHelperFactory
// destructor
// ---------------------------------------------------------------------------

LanguageHelperFactory::~LanguageHelperFactory() = default;


// ---------------------------------------------------------------------------
// LanguageHelperFactory::languageHelpers
// returns a list of the registered language helpers
// ---------------------------------------------------------------------------

std::vector<LanguageHelper*>& LanguageHelperFactory::languageHelpers()
{
    return languageHelpers_;
}


// ---------------------------------------------------------------------------
// LanguageHelperFactory::languageHelper
// returns a language helper corresponding to the given name
// ---------------------------------------------------------------------------

LanguageHelper* LanguageHelperFactory::languageHelper(const QString& languageName)
{
    // go through the list of available language helpers until we find the right one

    for (auto& languageHelper : languageHelpers_) {
        if (languageHelper->isLanguageName(languageName)) {
            return languageHelper;
        }
    }

    return new PlainTextHelper;  // default
}

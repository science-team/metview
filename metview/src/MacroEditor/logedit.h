/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QPlainTextEdit>

class EditorTheme;

// ----------------------------------------------------------------------------
// LogEdit
// This class extends the QPlainTextEdit with the following abilities:
//  o allow user-configurable colour theme
// ----------------------------------------------------------------------------

class LogEdit : public QPlainTextEdit
{
    Q_OBJECT

public:
    LogEdit(QWidget* parent = 0);
    ~LogEdit();

    void setTheme(EditorTheme*);
};

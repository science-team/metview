/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#include "logedit.h"

#include <QDebug>

#include "EditorTheme.h"

LogEdit::LogEdit(QWidget* parent) :
    QPlainTextEdit(parent)
{
}


LogEdit::~LogEdit() = default;


// Set the colour scheme
void LogEdit::setTheme(EditorTheme* theme)
{
    Q_ASSERT(theme);

    QColor col = theme->colour("logBackground");
    QString styleStr;
    if (col.isValid()) {
        styleStr = "background: " + col.name() + ";";
    }
    col = theme->colour("text");
    if (col.isValid()) {
        styleStr += "color: " + col.name() + ";";
    }

    if (!styleStr.isEmpty())
        setStyleSheet("QPlainTextEdit:focus{" + styleStr +
                      "; border: none;} \
         QPlainTextEdit:!focus{" +
                      styleStr + "selection-color: black; selection-background-color: rgb(180,204,234);}");

    QFont f = font();
    int fSize = f.pointSize();

    QString family = theme->value("font");
    f.setFamily(family);
    f.setFixedPitch(true);
    f.setPointSize(fSize);
    setFont(f);
}

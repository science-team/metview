/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

// --------------------------------------------------
// class MagMLHelper
// A language helper specific to the MagML language
// --------------------------------------------------

class ShellHelper : public LanguageHelper
{
public:
    ShellHelper();
    virtual ~ShellHelper();

    Highlighter* createHighlighter(QTextDocument* parent = 0);
    bool canCheckSyntax() { return false; }
    bool canDebug() { return false; }
    bool canRun() { return true; }
    bool runAsService() { return false; }
    bool isHelpAvailableForFunction(QString /*function*/) { return false; }
    bool isHeaderLine(const QString& text);  // is the text a header line (e.g. "# Metview Macro")?
};

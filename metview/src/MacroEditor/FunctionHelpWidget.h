/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QSettings>
#include <QWidget>

class EditorTheme;
class LanguageHelper;
class QStackedWidget;
class FunctionListWidget;
class VerbFunctionHelpWidget;
class VerbFunctionHelpFunction;
class FunctionInfoWidget;

class FunctionHelpWidget : public QWidget
{
    Q_OBJECT

public:
    FunctionHelpWidget(QWidget* parent = 0);
    ~FunctionHelpWidget() {}

    void setLanguage(LanguageHelper*);
    void setCodeEditorTheme(EditorTheme*, QFont);
    bool adjustContents(QString functionName, QString paramName);
    void readSettings(QSettings& settings);
    void writeSettings(QSettings& settings);

signals:
    void insertRequested(QString);

public slots:
    void showHelper(VerbFunctionHelpFunction*, int);
    void showWebDoc(VerbFunctionHelpFunction*, int);
    void showFunctionList();
    void slotInsert();

protected:
    enum ViewIndex
    {
        FunctionViewIndex = 0,
        VerbHelpViewIndex = 1,
        FunctionInfoViewIndex = 2
    };
    LanguageHelper* language_;
    QStackedWidget* stackedW_;
    FunctionListWidget* functionListW_;
    VerbFunctionHelpWidget* verbHelper_;
    FunctionInfoWidget* functionInfoW_;
};

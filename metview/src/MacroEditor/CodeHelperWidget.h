/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QSettings>
#include <QWidget>

class QComboBox;
class QStackedWidget;
class EditorTheme;
class MvQColourWidget;
class LanguageHelper;
class VerbFunctionHelpWidget;
class FunctionHelpWidget;
class ColourHelpWidget;

class CodeHelperWidget : public QWidget
{
    Q_OBJECT

public:
    CodeHelperWidget(QWidget* parent = nullptr);

    bool canSupportLanguage() const;
    void setLanguage(LanguageHelper* language);
    void setCodeEditorTheme(EditorTheme*, QFont);
    void showFunctionList();
    void adjustContents(QStringList lines, int cursorPositionInLine);
    void adjustColourHelper(QString line, int cursorPositionInLine);

    void readSettings();
    void writeSettings();

public slots:
    void setVisible(bool);

protected slots:
    void slotCloseSideBar();
    void slotInsert(QString);

signals:
    void showRequested(bool);
    void insertRequested(QString);

private:
    void build();

    enum HelpModeIndex
    {
        FunctionHelpIndex = 0,
        ColourHelpIndex = 1
    };

    bool built_;
    QStackedWidget* stackedW_;
    QComboBox* modeCombo_;
    LanguageHelper* language_;
    FunctionHelpWidget* functionHelper_;
    ColourHelpWidget* colourHelper_;
    EditorTheme* editorTheme_;
    QFont editorFont_;
};

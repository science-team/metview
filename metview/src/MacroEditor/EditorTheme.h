/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QColor>
#include <QMap>
#include <QString>
#include <QTextCharFormat>

#include "MvRequest.h"

class EditorTheme
{
public:
    EditorTheme(const MvRequest&);

    QString name() const { return name_; }
    QString label() const { return label_; }
    void setFormat(QString id, QTextCharFormat&);
    QColor colour(QString) const;
    QString value(QString) const;

    static EditorTheme* current() { return current_; }
    static EditorTheme* setCurrent(QString);
    static EditorTheme* find(QString);
    static QMap<QString, EditorTheme*> items();

protected:
    bool bold(QString) const;
    bool italic(QString) const;
    bool underline(QString) const;
    QColor rgb(QString) const;
    QColor hexa(QString) const;
    bool getString(QString, QString, QString&) const;
    static void load();

    mutable MvRequest req_;
    QString name_;
    QString label_;
    static EditorTheme* current_;
    static QMap<QString, EditorTheme*> items_;
};

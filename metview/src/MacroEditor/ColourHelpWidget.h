/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QWidget>

class MvQColourWidget;

namespace Ui
{
class ColourHelpWidget;
}

class ColourHelpWidget : public QWidget
{
    Q_OBJECT

public:
    ColourHelpWidget(QWidget* parent = 0);
    ~ColourHelpWidget() {}

    void setColour(QString);

signals:
    void insertRequested(QString);

public slots:
    void slotInsert();
    void slotCopy();

protected:
    Ui::ColourHelpWidget* ui_;
};

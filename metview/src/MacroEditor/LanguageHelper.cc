/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#include "LanguageHelper.h"


LanguageHelper::LanguageHelper()
{
    functionHelp_ = new VerbFunctionHelp(this);
}

// ---------------------------------------------------------------------------
// LanguageHelper::commentBlock
// performs the most common form of comment blocking, which will be suitable
// for Macro, Shell Script and Plain Text. Placed in the base class so that
// it can be used by all these classes.
// ---------------------------------------------------------------------------

QString LanguageHelper::commentBlock(const QString& text)
{
    QString result;
    QStringList lines = text.split("\n");  // put separate lines into a list of strings
    int numLines = lines.size();

    for (int i = 0; i < numLines; i++)  // each line will be pre-pended with a '#'
    {
        result += "# " + lines.at(i);

        if (i != numLines - 1)  // append a new line, but not after the last one
            result += "\n";
    }

    return result;
}

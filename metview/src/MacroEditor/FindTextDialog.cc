/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QtGui>

#include "FindTextDialog.h"


FindTextDialog::FindTextDialog(QWidget* parent) :
    QDialog(parent)
{
    // setAttribute(Qt::WA_DeleteOnClose);

    setupUi(this);  // this sets up GUI// setupFileMenu();
    findComboBox->setInsertPolicy(QComboBox::InsertAtTop);
    hasPerformedFind_ = false;

    connect(findButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(replaceButton, SIGNAL(clicked()), this, SLOT(onReplaceButtonClicked()));
    connect(replaceAllInSelectionButton, SIGNAL(clicked()), this, SLOT(onReplaceSelectedButtonClicked()));
    connect(replaceAllInFileButton, SIGNAL(clicked()), this, SLOT(onReplaceAllButtonClicked()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(findComboBox, SIGNAL(editTextChanged(const QString&)), this, SLOT(setFindButtonStatus()));
}

FindTextDialog::~FindTextDialog() = default;


// ---------------------------------------------------------------------------
// FindTextDialog::isBackwards
// tells whether the user has the 'Find backwards' box checked
// ---------------------------------------------------------------------------

bool FindTextDialog::isBackwards()
{
    return checkBox_backward->isChecked();
}


// ---------------------------------------------------------------------------
// FindTextDialog::findFlags
// returns the 'find flags' as selected by the user
// ---------------------------------------------------------------------------

QTextDocument::FindFlags FindTextDialog::findFlags()
{
    QTextDocument::FindFlags flags;

    if (checkBox_backward->checkState() == Qt::Checked) {
        flags = QTextDocument::FindBackward;
    }
    if (checkBox_case->checkState() == Qt::Checked) {
        flags = flags | QTextDocument::FindCaseSensitively;
    }
    if (checkBox_word->checkState() == Qt::Checked) {
        flags = flags | QTextDocument::FindWholeWords;
    }

    return flags;
}


// ---------------------------------------------------------------------------
// FindTextDialog::accept
// called when the user clicks the 'Find' button - sets some flags and
// emits a signal to tell the text editor to search for the text.
// ---------------------------------------------------------------------------

void FindTextDialog::accept()
{
    emit find(findComboBox->currentText(), findFlags());

    hasPerformedFind_ = true;
}


// ---------------------------------------------------------------------------
// FindTextDialog::setFindButtonStatus
// if there is text in the 'find' box, then we can activate the 'Find' button,
// otherwise we should disable it. This function is called each time the text
// in the box is changed.
// ---------------------------------------------------------------------------

void FindTextDialog::setFindButtonStatus()
{
    if (findComboBox->currentText().isEmpty()) {
        findButton->setEnabled(false);
    }
    else {
        findButton->setEnabled(true);
    }
}


// ---------------------------------------------------------------------------
// FindTextDialog::onReplaceButtonClicked
// triggered when the 'Replace Once' button is clicked
// ---------------------------------------------------------------------------

void FindTextDialog::onReplaceButtonClicked()
{
    emit replace(findComboBox->currentText(), replaceComboBox->currentText(), findFlags());
}


// ---------------------------------------------------------------------------
// FindTextDialog::onReplaceSelectedButtonClicked
// triggered when the 'Replace all in selection' button is clicked
// ---------------------------------------------------------------------------

void FindTextDialog::onReplaceSelectedButtonClicked()
{
    emit replaceInSelected(findComboBox->currentText(), replaceComboBox->currentText(), findFlags());
}


// ---------------------------------------------------------------------------
// FindTextDialog::onReplaceAllButtonClicked
// triggered when the 'Replace all in file' button is clicked
// ---------------------------------------------------------------------------

void FindTextDialog::onReplaceAllButtonClicked()
{
    emit replaceAll(findComboBox->currentText(), replaceComboBox->currentText(), findFlags());
}

// ---------------------------------------------------------------------------
// FindTextDialog::onSelectionChanged
// triggered when the selection in the main editor is changed
// ---------------------------------------------------------------------------

void FindTextDialog::onSelectionChanged(bool isSelection)
{
    replaceButton->setEnabled(isSelection);  // only enable this button if there is a selection
}


// ---------------------------------------------------------------------------
// FindTextDialog::setupUIBeforeShow
// sets up UI elements before the dialog is displayed.
//   findText: string - if not empty, then it will be set as the current
//                      search string
//  showReplaceOptions: bool - says whether to include the 'replace' controls
//                             in the dialog
//  isSelection: bool - says whether there is selected text in the document
// ---------------------------------------------------------------------------

void FindTextDialog::setupUIBeforeShow(const QString& findText, bool showReplaceOptions, bool isSelection)
{
    findComboBox->setFocus(Qt::OtherFocusReason);
    findButton->setDefault(true);

    setFindButtonStatus();

    if (!findText.isEmpty()) {
        findComboBox->addItem(findText);
        findComboBox->setCurrentIndex(findComboBox->count() - 1);
    }

    replaceButton->setVisible(showReplaceOptions);
    replaceLabel->setVisible(showReplaceOptions);
    replaceComboBox->setVisible(showReplaceOptions);
    replaceAllInGroupBox->setVisible(showReplaceOptions);

    replaceAllInSelectionButton->setEnabled(showReplaceOptions && isSelection);


    /*    if (showReplaceOptions)
    {
        replaceAllInGroupBox->setSize(replaceAllInGroupBox->sizeHint());
//        replaceAllInGroupBox->setMinimumSize(replaceAllInGroupBox->sizeHint());
    }
    else
    {
        replaceAllInGroupBox->setSize(0, 0);
//        replaceAllInGroupBox->setMinimumSize(0, 0);
    }

    resize(sizeHint()); */
}


// ---------------------------------------------------------------------------
// FindTextDialog::repeatLastFind
// repeats the last-executed 'find' operation
// ---------------------------------------------------------------------------

void FindTextDialog::repeatLastFind()
{
    accept();  // just do the same as when the user clicks the 'Find' button
}

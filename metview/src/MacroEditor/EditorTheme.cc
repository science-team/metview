/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "EditorTheme.h"

#include "MvRequest.h"
#include <QDebug>
#include <QMap>
#include <QTextCharFormat>

#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
#include <QtCore5Compat/QRegExp>
#else
#include <QRegExp>
#endif

EditorTheme* EditorTheme::current_ = nullptr;
QMap<QString, EditorTheme*> EditorTheme::items_;

EditorTheme::EditorTheme(const MvRequest& r) :
    req_(r)
{
    const char* verb = req_.getVerb();
    Q_ASSERT(verb);
    name_ = QString(verb);
    label_ = value("label");
    if (label_.isEmpty()) {
        label_ = name_;
    }
}

bool EditorTheme::getString(QString key, QString str, QString& val) const
{
    if (const char* c = req_(key.toStdString().c_str())) {
        QString s(c);
        if (s.contains(str)) {
            foreach (QString v, s.split(" ")) {
                if (v.contains(str)) {
                    val = v;
                    return true;
                }
            }
        }
    }

    return false;
}

QString EditorTheme::value(QString key) const
{
    if (const char* c = req_(key.toStdString().c_str())) {
        QString s(c);
        return s;
    }
    return {};
}

QColor EditorTheme::colour(QString key) const
{
    QString s;
    if (getString(key, "rgb(", s)) {
        return rgb(s);
    }
    else if (getString(key, "#", s)) {
        return hexa(s);
    }

    return {};
}

bool EditorTheme::bold(QString key) const
{
    QString s;
    return getString(key, "bold", s);
}

bool EditorTheme::italic(QString key) const
{
    QString s;
    return getString(key, "italic", s);
}

bool EditorTheme::underline(QString key) const
{
    QString s;
    return getString(key, "underline", s);
}

QColor EditorTheme::rgb(QString name) const
{
    QColor col;
    QRegExp rx(R"(rgb\((\d+),(\d+),(\d+),?(\d+)?)");

    if (rx.indexIn(name) > -1) {
        if (rx.captureCount() >= 3) {
            col = QColor(rx.cap(1).toInt(),
                         rx.cap(2).toInt(),
                         rx.cap(3).toInt(),
                         rx.cap(4).isEmpty() ? 255 : rx.cap(4).toInt());
        }
    }
    return col;
}

QColor EditorTheme::hexa(QString name) const
{
    if (name.size() != 7)
        return {};

    return QColor(name);
}

void EditorTheme::setFormat(QString id, QTextCharFormat& f)
{
    QColor col = colour(id);
    if (col.isValid())
        f.setForeground(col);
    if (bold(id))
        f.setFontWeight(QFont::Bold);
    if (italic(id))
        f.setFontItalic(true);
    if (underline(id))
        f.setUnderlineStyle(QTextCharFormat::SpellCheckUnderline);
}

EditorTheme* EditorTheme::setCurrent(QString id)
{
    current_ = find(id);
    return current_;
}

EditorTheme* EditorTheme::find(QString id)
{
    if (items_.isEmpty())
        load();

    return (items_.contains(id)) ? items_.value(id) : 0;
}

QMap<QString, EditorTheme*> EditorTheme::items()
{
    if (items_.isEmpty())
        load();

    return items_;
}

void EditorTheme::load()
{
    const char* dshare = getenv("METVIEW_DIR_SHARE");
    if (!dshare)
        return;

    MvRequest r;

    std::string p(dshare);
    p += "/app-defaults/UiTheme.MacroEditor";
    r.read(p.c_str());
    if (r) {
        do {
            auto* t = new EditorTheme(r);
            items_[t->name()] = t;
        } while (r.advance());
    }
}

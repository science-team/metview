/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "LanguageHelper.h"
#include "MagMLHelper.h"
#include "Highlighter.h"

#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
#include <QtCore5Compat/QRegExp>
#else
#include <QRegExp>
#endif

#include <QRegularExpression>
#include <QRegularExpressionMatch>

// -----------------------------------------------------------------------
// ------------------- Syntax highlighter code ---------------------------
//        this class is used by the MagMLHelper object, defined below
// -----------------------------------------------------------------------


class MagMLHighlighter : public Highlighter
{
public:
    MagMLHighlighter(QTextDocument* parent = nullptr);
    ~MagMLHighlighter() override = default;

    void highlightBlock(const QString& text) override;

protected:
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
    QRegularExpression commentStartExpression;
    QRegularExpression commentEndExpression;
#else
    QRegExp commentStartExpression;
    QRegExp commentEndExpression;
#endif
    QTextCharFormat tagNameFormat;
    QTextCharFormat attributeNameFormat;
    QTextCharFormat tagSymbolsFormat;
    QTextCharFormat quotationFormat;
    QTextCharFormat multiLineCommentFormat;
};


MagMLHighlighter::MagMLHighlighter(QTextDocument* parent) :
    Highlighter(parent)
{
    HighlightingRule rule;


    // Note that the order in which these highlighting rules are given
    // DOES matter! The rules seem to be applied in the order in which
    // they are given.

    // tag names
    tagNameFormat.setForeground(Qt::darkBlue);
    QString pattern = "(<\\w*)|(</\\w*>)|(>)";
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
    rule.pattern = QRegularExpression(pattern);
#else
    rule.pattern = QRegExp(pattern);
#endif
    rule.format = tagNameFormat;
    highlightingRules.append(rule);

    // tag symbols
    pattern = "(<)|(</)|(>)";
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
    rule.pattern = QRegularExpression(pattern);
#else
    rule.pattern = QRegExp(pattern);
#endif
    rule.format = tagSymbolsFormat;
    highlightingRules.append(rule);

    // attribute names
    pattern = "(\\w+\\s*)(?==)";  // (?==) means 'true if '=', but don't match it'
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
    rule.pattern = QRegularExpression(pattern);
#else
    rule.pattern = QRegExp(pattern);
#endif
    attributeNameFormat.setForeground(Qt::red);
    rule.format = attributeNameFormat;
    highlightingRules.append(rule);


    // strings
    pattern = R"(("[^"]*")|('[^']*'))";
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
    rule.pattern = QRegularExpression(pattern);
#else
    rule.pattern = QRegExp(pattern);
#endif
    quotationFormat.setForeground(Qt::darkRed);
    rule.format = quotationFormat;
    highlightingRules.append(rule);


    // multi-line comments
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
    commentStartExpression = QRegularExpression("<!--");
    commentEndExpression = QRegularExpression("-->");
#else
    commentStartExpression = QRegExp("<!--");
    commentEndExpression = QRegExp("-->");
#endif
    multiLineCommentFormat.setFontItalic(true);
    multiLineCommentFormat.setForeground(Qt::darkGreen);
}


void MagMLHighlighter::highlightBlock(const QString& text)
{
    Highlighter::highlightBlock(text);

    // multi-line comments
    setCurrentBlockState(0);
#if 0
    int startIndex = 0;
    if (previousBlockState() != 1)
        startIndex = text.indexOf(commentStartExpression);

    while (startIndex >= 0) {
        int endIndex = text.indexOf(commentEndExpression, startIndex);
        int commentLength;
        if (endIndex == -1) {
            setCurrentBlockState(1);
            commentLength = text.length() - startIndex;
        }
        else {
            commentLength = endIndex - startIndex + commentEndExpression.matchedLength();
        }
        setFormat(startIndex, commentLength, multiLineCommentFormat);
        startIndex = text.indexOf(commentStartExpression,
                                  startIndex + commentLength);
    }
#endif
}


// -----------------------------------------------------------------------
// -------------------------- MagMLHelper code ---------------------------
// -----------------------------------------------------------------------


// ---------------------------------------------------------------------------
// MagMLHelper::MagMLHelper
// constructor
// ---------------------------------------------------------------------------

MagMLHelper::MagMLHelper()
{
    languageName_ = "MagML";
    className_ = "MAGML";
    serviceName_ = "MagML";
    iconName_ = "MAGML";
}

// ---------------------------------------------------------------------------
// MagMLHelper::~MagMLHelper
// destructor
// ---------------------------------------------------------------------------

MagMLHelper::~MagMLHelper() = default;


// ---------------------------------------------------------------------------
// MagMLHelper::~createHighlighter
// creates a new syntax highlighter object
// ---------------------------------------------------------------------------

Highlighter* MagMLHelper::createHighlighter(QTextDocument* parent)
{
    return new MagMLHighlighter(parent);
}


// ---------------------------------------------------------------------------
// MagMLHelper::commentBlock
// puts the text into an XML comment block
// ---------------------------------------------------------------------------

QString MagMLHelper::commentBlock(const QString& text)
{
    QString result("<!--\n");

    result += text + "-->\n";

    return result;
}

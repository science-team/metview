set(macroeditor_moc_srcs
     MvMain.h
     ColourHelpWidget.h
     FindTextDialog.h
     FunctionHelpWidget.h
     FunctionInfoWidget.h
     FunctionListWidget.h
     Highlighter.h
     MacroEdit.h
     logedit.h
     mvplaintextedit.h
     CodeHelperWidget.h
     CodeTemplateDialog.h
     GotoLineDialog.h
     RunDialog.h
     TabsDialog.h
     VerbFunctionHelpWidget.h
     MacroEdit.h)

set(macroeditor_ui_srcs
    CodeTemplateDialog.ui
    FindTextDialog.ui
    MacroEdit.ui
    GotoLineDialog.ui
    RunDialog.ui
    TabsDialog.ui
    VerbFunctionHelpWidget.ui
    FunctionHelpWidget.ui
    FunctionInfoWidget.ui
    ColourHelpWidget.ui
    FunctionListWidget.ui)

if(METVIEW_QT5)
#    QT5_ADD_RESOURCES(MacroEd_RES MacroEdit.qrc)
    QT5_WRAP_CPP(MacroEd_MOC ${macroeditor_moc_srcs})
    QT5_WRAP_UI(MacroEd_FORMS_HEADERS ${macroeditor_ui_srcs})
elseif(METVIEW_QT6)
#    QT6_ADD_RESOURCES(MacroEd_RES MacroEdit.qrc)
    QT6_WRAP_CPP(MacroEd_MOC ${macroeditor_moc_srcs})
    QT6_WRAP_UI(MacroEd_FORMS_HEADERS ${macroeditor_ui_srcs})
endif()

set(macroeditor_srcs MvMain.cc
                     Highlighter.cc
                     MacroEdit.cc                     
                     CodeHelperWidget.cc
                     CodeTemplateDialog.cc
                     ColourHelpWidget.cc
                     EditorTheme.cc
                     FindTextDialog.cc
                     FunctionHelpWidget.cc
                     FunctionInfoWidget.cc
                     GotoLineDialog.cc
                     IconClassHelp.cc
                     RunDialog.cc
                     TabsDialog.cc                     
                     FunctionListWidget.cc
                     VerbFunctionHelpWidget.cc                    
                     mvplaintextedit.cc
                     MacroHelper.cc
                     MagMLHelper.cc
                     ShellHelper.cc
                     PlainTextHelper.cc
                     PythonHelper.cc
                     LanguageHelper.cc
                     LanguageHelperFactory.cc
                     logedit.cc
                     VerbFunctionHelp.cc
                     )

#include_directories( CMAKE_CURRENT_BINARY_DIR . )

ecbuild_add_executable( TARGET       MacroEditor
                        SOURCES      ${MacroEd_FORMS_HEADERS}
                                     ${MacroEd_MOC}
#                                     ${MacroEd_RES}
                                     ${macroeditor_srcs}
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR}
                                     ${METVIEW_STANDARD_INCLUDES} ${X11_INCLUDE_DIR} ${METVIEW_QT_INCLUDE_DIRS} ${QTWEB_INCLUDE_DIR}
                        LIBS         ${METVIEW_QT_LIBRARIES} ${STANDARD_METVIEW_LIBS} ${X11_LIBRARIES} ${QTWEB_LIBS}
                    )


/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "ui_GotoLineDialog.h"

class GotoLineDialog : public QDialog, private Ui::GotoLineDialogQ
{
    Q_OBJECT

public:
    GotoLineDialog(QWidget* parent = 0);
    ~GotoLineDialog();
    void setupUIBeforeShow();

signals:
    void gotoLine(int line);  // emitted when the user says 'ok'


public slots:
    void slotDone();
    void setButtonStatus();
};

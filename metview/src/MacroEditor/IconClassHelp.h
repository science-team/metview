/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <map>

#include "Metview.h"
#include "MvIconClassCore.h"

class Path;
class MvIconLanguage;

class IconClassHelp : public MvIconClassCore
{
public:
    IconClassHelp(const std::string&, request*);
    ~IconClassHelp() {}

    static const std::map<std::string, IconClassHelp*>& items() { return items_; }
    static void load(request*);
    static IconClassHelp* find(const std::string&);
    static IconClassHelp* findByMacroFunction(const std::string&);

private:
    static std::map<std::string, IconClassHelp*> items_;
};

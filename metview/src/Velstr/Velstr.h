/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//--

#pragma once

#include "Metview.h"


class Velstr : public MvService
{
protected:
    char Vsmoothing[4];    // smoothing (YES/NO)
    char Vscaling[4];      // scaling   (YES/NO)
    int Vtrunc;            // truncation
    double Vfltc;          // constant
    double Vmfltexp;       // exponent
    int Vnfield;           // number of fields
    MvFieldSet Vfieldset;  // Vorticity or divergence field set

public:
    Velstr(const char* a) :
        MvService(a) {}
    ~Velstr() override = default;
    void serve(MvRequest&, MvRequest&) override;

    virtual bool GetData(MvRequest&) = 0;
    bool GetDataGen(MvRequest&);
    bool Apply(MvRequest&);
    int GetField(MvRequest&, int, MvFieldSet&);

    bool isValidData_DVVALID(grib_handle* gh);
    void convert_DVTOVS(grib_handle* gh, int trunc_out);
    bool smooth_BPPSMTH(grib_handle* gh);
    bool checkOutputTruncation(grib_handle* gh);
    void scaleData(grib_handle* gh);
    bool badRetVal(int ret, const char* gribApiFunc, const char* MvFunc);
};


class Velpot : public Velstr
{
public:
    Velpot(const char* a) :
        Velstr(a) {}
    bool GetData(MvRequest&) override;
};


class Streamfn : public Velstr
{
public:
    Streamfn(const char* a) :
        Velstr(a) {}
    bool GetData(MvRequest&) override;
};

/* VELSTR_H */

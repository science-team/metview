
set(Divrot_srcs
    Divrot.cc divrot-exFortran.cc Divrot.h
)

if(HAVE_PPROC_MIR)
    set(DIVROT_PPROC_MIR_DEF "HAVE_PPROC_MIR")
    set(EXTRA_LIBS eckit)
endif()

if(HAVE_PPROC_EMOS)
    set(DIVROT_PPROC_EMOS_DEF "HAVE_PPROC_EMOS")
endif()

ecbuild_add_executable( TARGET       Divrot
                        SOURCES      ${Divrot_srcs}
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS} ${DIVROT_PPROC_MIR_DEF} ${DIVROT_PPROC_EMOS_DEF}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES} ${MIR_INCLUDE_DIRS}
                        LIBS         ${STANDARD_METVIEW_LIBS} ${LIBEMOS_LIBRARIES} ${METVIEW_EXTRA_LIBRARIES} ${EXTRA_LIBS}
                    )

if(HAVE_PPROC_MIR)
    metview_module_files(ETC_FILES ObjectSpec.DivrotMir
                                   DivrotMirDef
                         SVG_FILES DIVROT_MIR.svg
                         )
endif()

# only expose the retrieve_emos, etc functions if we have both emoslib and mir
if(HAVE_PPROC_EMOS AND HAVE_PPROC_MIR)
    metview_module_files(ETC_FILES ObjectSpec.DivrotEmos
                                   DivrotEmosDef
                         SVG_FILES DIVROT_EMOS.svg
                         )
endif()

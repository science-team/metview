/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Event.h"

Queue OutputQueue::gQueue;
QIter OutputQueue::gIter;

//==============================================================================

OElem::OElem(request* r, Service* s) :
    QElem(r),
    svc_(s)
{
}

//==============================================================================

OElem::~OElem() = default;

//==============================================================================

void OutputQueue::Append(request* r, Service* s)
{
    Append(new OElem(r, s));
}

//==============================================================================

void OutputQueue::Insert(request* r, Service* s)
{
    Insert(new OElem(r, s));
}

//==============================================================================

void OutputQueue::Process()
{
    OElem* q;

    Rewind();
    while ((q = Next()) != nullptr) {
        Service* s = q->Module();
        request* r = q->Request();
#ifdef USE_THREADS
        s->EncodeRequest(q);
        Dequeue(q);
#else
        if (s->EncodeRequest(r) == 0)
            Remove(q);
        else
            break;
#endif
    }
}

//==============================================================================

void OutputQueue::RemoveAll(Service* s)
{
    OElem* q;

    Rewind();
    while ((q = Next()) != nullptr)
        if (q->Module() == s) {
            // Requeue in Waiting queue
            WaitingQueue::Append(q->Request(), q->Module()->Name());
            Remove(q);
        }
}

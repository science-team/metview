/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "mars.h"
#include "Queue.h"
#include "Service.h"
#include "ProcessingQueue.h"
#include "OutputQueue.h"
#include "WaitingQueue.h"


struct Options
{
    int port;
    int python_pid;
    char* startup;
    char* exit;
    char* env;
};

class Event
{
    static long gUniqID;
    static fd_set gFiles;
    static Options gOpt;

public:
    static long UniqueID() { return ++gUniqID; }

    static void Loop();
    static void Init(int, char**);
    static void Startup();
    static void NewService(int);
    static void CleanUp(int, void*);
    static void CheckQueues();
    static void CheckTimeout();
    static void EndOfModule(const char*);
    static void AbortIfBatch(const char*);

    static void AddInput(int soc) { FD_SET(soc, &gFiles); }
    static void RemoveInput(int soc) { FD_CLR(soc, &gFiles); }
    static int IsInput(int soc) { return FD_ISSET(soc, &gFiles); }

private:
    static void WriteEnvVarIfNotNull(FILE*, const char*);
};

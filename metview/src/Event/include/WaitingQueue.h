/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

// Queue of requests waiting for a module to start

class WElem : public QElem
{
    int fail_;
    int late_;
    time_t start_;
    time_t when_;  // Last time event tried to started the module
    char* name_;   // Name of the module to start

    ~WElem();

public:
    WElem(request*, const char*);
    void Update();
    time_t When() { return when_; }
    time_t Start() { return start_; }
    const char* Name() { return name_; }
    int Fail() { return fail_; }
    void Fail(int f) { fail_ = f; }
    void Late(int l) { late_ = l; }
    int Late() { return late_; }
};

class WaitingQueue
{
    static Queue gQueue;
    static QIter gIter;

    static void Rewind() { gIter.Rewind(gQueue); }
    static WElem* Next() { return (WElem*)gIter(); }
    static void Remove(WElem* e) { gQueue.Remove(e); }
    static void Append(WElem* e) { gQueue.Append(e); }

public:
    static void Process();
    static void Append(request* r, const char* n);
    static void Later(request* r, const char* n);
    static int StartModule(const char*);
    static int Empty() { return gQueue.Empty(); }
    static void NotifyMonitor();
};

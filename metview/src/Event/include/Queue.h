/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

class QElem
{
    friend class Queue;
    friend class QIter;

    request* req_;
    QElem* next_;

    // No copy
    QElem(const QElem&);
    QElem operator=(const QElem&);


protected:
    QElem(request*);
    virtual ~QElem();

public:
    request* Request() { return req_; }
};

class Queue
{
    friend class QIter;

    QElem* head_;
    QElem* tail_;

    // No copy
    Queue(const Queue&);
    Queue operator=(const Queue&);

    // No destructor
    //~Queue() {}

public:
    Queue();
    int Empty() { return head_ == 0; }
    void Append(QElem*);
    void Insert(QElem*);
    void Remove(QElem*);
    void Dequeue(QElem*);
    void RemoveAll();

    QElem* Pop();
};

class QIter
{
    QElem* q_;

    // No copy
    QIter(const QIter&);
    QIter operator=(const QIter&);

public:
    QIter() { q_ = 0; }
    QIter(Queue&);
    void Rewind(Queue&);
    QElem* operator()();
};

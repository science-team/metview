/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

// Queue of processed requests

class PElem : public QElem
{
    Service* svc_;  // Module handling the request
    ~PElem();

public:
    PElem(request*, Service*);
    Service* Module() { return svc_; }
    void Reparent(Service* s) { svc_ = s; }
};

class ProcessingQueue
{
    static Queue gQueue;
    static QIter gIter;

    static void Rewind() { gIter.Rewind(gQueue); }
    static PElem* Next() { return (PElem*)gIter(); }
    static void Remove(PElem* e) { gQueue.Remove(e); }
    static void Append(PElem* e) { gQueue.Append(e); }

public:
    static void Append(request*, Service*);
    static void RemoveAll(Service*);
    static void Remove(request*);
    static Service* Followup(request*, Service*);
    static int Empty() { return gQueue.Empty(); }
    static void NotifyMonitor();
};

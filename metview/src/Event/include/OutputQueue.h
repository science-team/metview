/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

// Queue of requests to be sent
class OElem : public QElem
{
    Service* svc_;  // Service to send the request to

    ~OElem();

public:
    OElem(request*, Service*);
    Service* Module() { return svc_; }
};

class OutputQueue
{
    static Queue gQueue;
    static QIter gIter;

    static void Rewind() { gIter.Rewind(gQueue); }
    static OElem* Next() { return (OElem*)gIter(); }
    static void Remove(OElem* e) { gQueue.Remove(e); }
    static void Dequeue(OElem* e) { gQueue.Dequeue(e); }
    static void Append(OElem* e) { gQueue.Append(e); }
    static void Insert(OElem* e) { gQueue.Insert(e); }

public:
    static void Process();
    static void Append(request*, Service*);
    static void Insert(request*, Service*);
    static void RemoveAll(Service*);
    static int Empty() { return gQueue.Empty(); }
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "Event.h"

//==============================================================================

QElem::QElem(request* req) :
    req_(clone_all_requests(req)),
    next_(nullptr)
{
}

//==============================================================================

QElem::~QElem()
{
    free_all_requests(req_);
}

//==============================================================================

QElem* QIter::operator()()
{
    if (!q_)
        return nullptr;

    QElem* q = q_;
    q_ = q_->next_;
    return q;
}

//==============================================================================

QIter::QIter(Queue& q)
{
    q_ = q.head_;
}

//==============================================================================

void QIter::Rewind(Queue& q)
{
    q_ = q.head_;
}

//==============================================================================

Queue::Queue() :
    head_(nullptr),
    tail_(nullptr)
{
}

//==============================================================================

void Queue::Append(QElem* q)
{
    q->next_ = nullptr;
    if (head_)
        tail_->next_ = q;
    else
        head_ = q;
    tail_ = q;
}

//==============================================================================

void Queue::Insert(QElem* q)
{
    q->next_ = head_;
    head_ = q;
    if (!tail_)
        tail_ = q;
}

//==============================================================================

QElem* Queue::Pop()
{
    QElem* p = head_;
    if (p)
        Dequeue(p);
    return p;
}

void Queue::Dequeue(QElem* q)
{
    QElem* p = head_;
    QElem* s = nullptr;
    while (p) {
        if (p == q) {
            if (s)
                s->next_ = q->next_;
            if (q == tail_)
                tail_ = s;
            if (q == head_)
                head_ = q->next_;

            q->next_ = nullptr;

            return;
        }
        s = p;
        p = p->next_;
    }
}

void Queue::Remove(QElem* q)
{
    Dequeue(q);
    delete q;
}

void Queue::RemoveAll()
{
    while (head_)
        Remove(head_);
}

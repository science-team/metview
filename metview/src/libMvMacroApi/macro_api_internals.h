/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "mars.h"

#pragma once

/*
 *  This header file hides 'mars.h' from user programs.
 *
 *  This header is included only in 'macro_api.c' while compiling
 *  the interface functions, it is not included in 'macro_api.h'.
 *
 *  Thus 'macro_api.h' can be included in users' Macro C/C++ programs
 *  without the overhead of 'mars.h'.
 */

typedef struct gribarg
{
    FILE* f;
    fieldset* v;
    int cnt;
    char* file;
} gribarg;


typedef struct bufrarg
{
    FILE* f;
    char* file;
} bufrarg;


typedef struct arg
{
    char kind;
    union
    {
        double dval;
        char* sval;
        request* rval;
    } u;
} arg;

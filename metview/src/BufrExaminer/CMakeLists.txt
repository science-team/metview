
set(BufrExaminer_moc
    BufrExaminer.h
    BufrCompDataWidget.h
    BufrFilterDialog.h
    BufrFilterEditor.h
    BufrKeyWidget.h
   )

set(BufrExaminer_form
    BufrCompDataWidget.ui
    BufrFilterDialog.ui
    BufrKeyWidget.ui
   )

if(METVIEW_QT5)
 QT5_WRAP_CPP(BufrExaminer_MOC ${BufrExaminer_moc})
 QT5_WRAP_UI(BufrExaminer_FORMS_HEADERS ${BufrExaminer_form})
elseif(METVIEW_QT6)
 QT6_WRAP_CPP(BufrExaminer_MOC ${BufrExaminer_moc})
 QT6_WRAP_UI(BufrExaminer_FORMS_HEADERS ${BufrExaminer_form})
endif()


ecbuild_add_executable( TARGET       BufrExaminer
                        SOURCES      MvMain.cc
                                     BufrCompDataWidget.cc                                     
                                     BufrExaminer.cc
                                     BufrExpandDataModel.cc
                                     BufrFilterDialog.cc
                                     BufrFilterEditor.cc
                                     BufrKeyWidget.cc                                                                          
                                     MvQBufrDataItem.cc
                                     MvQBufrDataModel.cc
                                     BufrCompDataWidget.h
                                     BufrExaminer.h                                   
                                     BufrFilterDialog.h
                                     BufrFilterEditor.h
                                     BufrKeyWidget.h                                                                          
                                     MvQBufrDataItem.h
                                     MvQBufrDataModel.h
                                     ${BufrExaminer_MOC}
                                     ${BufrExaminer_FORMS_HEADERS}
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR}
                                     ${METVIEW_STANDARD_INCLUDES} ${METVIEW_QT_INCLUDE_DIRS}
                        LIBS         ${METVIEW_QT_LIBRARIES} ${STANDARD_METVIEW_LIBS}
                    )



/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "BufrFilterDialog.h"

#include <QCloseEvent>
#include <QLayoutItem>
#include <QSettings>

#include "BufrFilterDef.h"
#include "BufrMetaData.h"
#include "MvQMethods.h"
#include "MvQTheme.h"

BufrFilterDialog::BufrFilterDialog(QWidget* parent) :
    QDialog(parent)
{
    setupUi(this);

    QString winTitle;

#ifdef ECCODES_UI
    winTitle = "BUFR filter (codes_ui)";
    settingsName_ = "codesui-bufr-msgFilterDialog";
#else
    winTitle = "BUFR filter (Metview)";
    settingsName_ = "mv-BufrExaminer-msgFilterDialog";
#endif

    setWindowTitle(winTitle);

    connect(buttonBox_, SIGNAL(accepted()),
            this, SLOT(accept()));
    connect(buttonBox_, SIGNAL(rejected()),
            this, SLOT(reject()));

    QFont groupf;
    groupf.setBold(true);
    groupf.setPointSize(groupf.pointSize() - 1);
    groupLabel_->setFont(groupf);
    groupLabel_->setProperty("type", "sub");

    // Find button
    QPalette pal = filterPb_->palette();
    pal.setColor(QPalette::Button, MvQTheme::colour("dialog", "default_button"));
    filterPb_->setPalette(pal);

    readSettings();
}

BufrFilterDialog::~BufrFilterDialog()
{
    writeSettings();
}

void BufrFilterDialog::closeEvent(QCloseEvent* event)
{
    event->accept();
    // writeSettings();
}

void BufrFilterDialog::accept()
{
    Q_ASSERT(editor_);
    Q_EMIT edited(editor_->filterDef());
}


void BufrFilterDialog::newFileLoaded(const std::string& fname)
{
    lastFileLoaded_ = fname;
}

void BufrFilterDialog::initHelpers(BufrMetaData* decoder, int msgIdx)
{
    Q_ASSERT(editor_);
    editor_->initHelpers(decoder, msgIdx);
}

void BufrFilterDialog::updateHelpers(BufrMetaData* decoder, int msgIdx)
{
    Q_ASSERT(editor_);
    editor_->updateHelpers(decoder, msgIdx);
}

void BufrFilterDialog::on_filterPb__clicked(bool)
{
    accept();
}

void BufrFilterDialog::on_clearPb__clicked(bool)
{
    editor_->clear();
}

void BufrFilterDialog::on_expandAllPb__clicked(bool)
{
    editor_->expandAllGroups();
}

void BufrFilterDialog::on_expandEditedPb__clicked(bool)
{
    editor_->expandEditedGroups();
}

void BufrFilterDialog::on_collapseAllPb__clicked(bool)
{
    editor_->collapseAllGroups();
}

//------------------------------------------
// Settings read/write
//------------------------------------------


void BufrFilterDialog::writeSettings()
{
    QSettings settings(MVQ_QSETTINGS_DEFAULT_ARGS, settingsName_);

    // We have to clear it so that should not remember all the previous values
    settings.clear();

    settings.beginGroup("main");
    settings.setValue("size", size());
    settings.endGroup();
}

void BufrFilterDialog::readSettings()
{
    QSettings settings(MVQ_QSETTINGS_DEFAULT_ARGS, settingsName_);

    settings.beginGroup("main");
    if (settings.contains("size")) {
        resize(settings.value("size").toSize());
    }
    else {
        resize(QSize(550, 540));
    }

    settings.endGroup();
}

#include "BufrDescWidget.hpp"
#include "ui_BufrDescWidget.h"

BufrDescWidget::BufrDescWidget(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::BufrDescWidget)
{
    ui->setupUi(this);
}

BufrDescWidget::~BufrDescWidget()
{
    delete ui;
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QSortFilterProxyModel>
#include <QStringList>

#include "BufrMetaData.h"

class GribWmoDump;
class GribSection;
class GribItem;

class MvQBufrDumpSortFilterModel : public QSortFilterProxyModel
{
public:
    MvQBufrDumpSortFilterModel(QObject* parent = 0) :
        QSortFilterProxyModel(parent){};

protected:
    bool lessThan(const QModelIndex&, const QModelIndex&) const;
};


class MvQBufrSectionDumpModel : public QAbstractItemModel
{
public:
    MvQBufrSectionDumpModel();

    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    int rowCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex&) const;

    void dataIsAboutToChange();
    void setDumpData(BufrSectionDump*);
    BufrSectionDump* dumpData() { return data_; }

    void setAllKeys(QList<MvKeyProfile*>&);

    Qt::ItemFlags flags(const QModelIndex& index) const;
    Qt::DropActions supportedDropActions() const;
    QStringList mimeTypes() const;
    QMimeData* mimeData(const QModelIndexList&) const;
    bool dropMimeData(const QMimeData* data,
                      Qt::DropAction action, int row, int column,
                      const QModelIndex& parent);

protected:
    QString label(BufrSection*, const int) const;
    QString label(BufrSectionItem*, const int) const;

    int idToLevel(int) const;
    int idToParentRow(int) const;

private:
    BufrSectionDump* data_;
    QList<MvKeyProfile*> allKeys_;
};


class MvQBufrDataDumpModel : public QAbstractItemModel
{
public:
    MvQBufrDataDumpModel();

    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    int rowCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex&) const;

    void dataIsAboutToChange();
    void setDumpData(BufrDataDump*);
    BufrDataDump* dumpData() { return data_; }

protected:
    QString label(BufrDataItem*, const int, const int) const;

private:
    BufrDataDump* data_;
};

class MvQBufrBitmapDumpModel : public QAbstractItemModel
{
public:
    MvQBufrBitmapDumpModel();

    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    int rowCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex&) const;

    void dataIsAboutToChange();
    void setDumpData(BufrBitmapDump*);
    BufrBitmapDump* dumpData() { return data_; }

private:
    BufrBitmapDump* data_;
};

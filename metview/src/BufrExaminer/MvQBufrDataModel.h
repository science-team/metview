/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>

class MvQBufrDataItem;

class MvQBufrDataModel : public QAbstractItemModel
{
public:
    explicit MvQBufrDataModel(QObject* parent = nullptr);
    ~MvQBufrDataModel() override;

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& index) const override;

    bool loadJson(const QByteArray& json, bool compressed, QString& err);
    bool hasData() const;
    void clear();
    void setSubsetNumber(int);
    MvQBufrDataItem* indexToItem(const QModelIndex& idx) const;

    // QModelIndexList match(const QModelIndex& start,int role,const QVariant& value,int hits = 1,
    //                        Qt::MatchFlags flags = Qt::MatchFlags( Qt::MatchStartsWith | Qt::MatchWrap )) const;

private:
    MvQBufrDataItem* root_;
    bool compressed_;
    int subsetNumber_;
};

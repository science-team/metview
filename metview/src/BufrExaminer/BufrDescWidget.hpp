#pragma once

#include <QWidget>

namespace Ui
{
class BufrDescWidget;
}

class BufrDescWidget : public QWidget
{
    Q_OBJECT

public:
    explicit BufrDescWidget(QWidget* parent = 0);
    ~BufrDescWidget();

private:
    Ui::BufrDescWidget* ui;
};

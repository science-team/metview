/***************************** LICENSE START ***********************************

 Copyright 2017 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "ui_BufrCompDataWidget.h"

#include <QAbstractItemModel>
#include <QSortFilterProxyModel>
#include <QWidget>

class BufrMetaData;
class MvKeyProfile;
class MvQKeyProfileModel;
class MvQKeyProfileSortFilterModel;


class BufrDataKeyFilterModel : public QSortFilterProxyModel
{
public:
    BufrDataKeyFilterModel(QObject* parent = nullptr);
    void setFilter(QStringList);

private:
    bool filterAcceptsRow(int source_column, const QModelIndex& source_parent) const override;

    QStringList filterStr_;
};

class BufrDataKeyModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit BufrDataKeyModel(QObject* parent = nullptr);
    ~BufrDataKeyModel() override;

    void setKeyProfile(MvKeyProfile*);
    MvKeyProfile* profile() const { return prof_; }

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    bool setData(const QModelIndex& idx, const QVariant& value, int role) override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& index) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;

    void clear();
    void selectAll(QString);
    void unselectAll();
    QList<int> filtered() const;

signals:
    void filterChanged();

private:
    MvKeyProfile* prof_{nullptr};
};


class BufrCompDataWidget : public QWidget, protected Ui::BufrCompDataWidget
{
    Q_OBJECT

public:
    explicit BufrCompDataWidget(QWidget* parent = nullptr);
    ~BufrCompDataWidget() override = default;

    void init(BufrMetaData* data);
    void clearData();
    void loadData(int msgNum, int subsetNum);

protected Q_SLOTS:
#if 0
    void on_columnTe__textChanged(QString);
#endif
    void on_columnLe__textChanged(QString s);
    void on_selectAllTb__clicked(bool);
    void on_unselectAllTb__clicked(bool);
    void on_filterPanelTb__toggled(bool);
    void slotFilterChanged();

private:
    BufrMetaData* data_{nullptr};
    MvQKeyProfileModel* model_{nullptr};
    MvQKeyProfileSortFilterModel* sortModel_{nullptr};
    BufrDataKeyModel* colModel_{nullptr};
    BufrDataKeyFilterModel* colFilterModel_{nullptr};
};

/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "ui_BufrFilterDialog.h"

#include <QDialog>
#include <string>

class BufrFilterDef;
class BufrMetaData;

class BufrFilterDialog : public QDialog, protected Ui::BufrFilterDialog
{
    Q_OBJECT

public:
    explicit BufrFilterDialog(QWidget* parent = nullptr);
    ~BufrFilterDialog() override;
    void newFileLoaded(const std::string& fname);
    void initHelpers(BufrMetaData*, int msgIdx = 0);
    void updateHelpers(BufrMetaData*, int msgIdx = 0);

public Q_SLOTS:
    void accept() override;

protected Q_SLOTS:
    void on_filterPb__clicked(bool);
    void on_clearPb__clicked(bool);
    void on_expandAllPb__clicked(bool);
    void on_expandEditedPb__clicked(bool);
    void on_collapseAllPb__clicked(bool);

Q_SIGNALS:
    void changeVisibleRequested(bool);
    void edited(const BufrFilterDef&);

protected:
    void closeEvent(QCloseEvent* event) override;

private:
    void writeSettings();
    void readSettings();

    QString settingsName_;
    std::string lastFileLoaded_;
};

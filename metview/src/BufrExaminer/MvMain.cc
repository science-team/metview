/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "BufrExaminer.h"

#include <string>

#include "Metview.h"
#include "MvQApplication.h"
#include "MvQKeyManager.h"
#include "BufrMetaData.h"


int main(int argc, char** argv)
{
    if (argc < 2) {
        marslog(LOG_EROR, "No arguments are specified");
        exit(1);
    }

    MvRequest in;
    in.read(argv[1], false, true);

    marslog(LOG_INFO, "Request:");
    in.print();

    if (!in) {
        marslog(LOG_EROR, "No request could be read from request file=%s", argv[1]);
        exit(1);
    }

    // Get grib file name
    std::string fbufr;
    if (const char* tmpc = in("PATH")) {
        fbufr = std::string(tmpc);
    }
    else {
        marslog(LOG_EROR, "No PATH is specified!");
        exit(1);
    }

    // Create the qt application. The appname must be unique!
    std::string appName = MvApplication::buildAppName("BufrExaminer");
    MvQApplication app(argc, argv, appName.c_str(), QStringList{"examiner", "keyDialog", "window", "find"});

    // Create the bufr key manager and initialize it
    auto* manager = new MvQKeyManager(MvQKeyManager::BufrType);
    manager->loadProfiles();

    // Create the bufr metadata object and initialize it
    auto* bufr = new BufrMetaData;
    bufr->setFileName(fbufr);

    // Create the bufr browser and initialize it
    auto* browser = new BufrExaminer;
    browser->setAppIcon("BUFR");
    browser->init(bufr, manager, nullptr);

    browser->show();

    // Enter the app loop
    app.exec();
}

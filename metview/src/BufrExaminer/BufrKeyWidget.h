/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "ui_BufrKeyWidget.h"

#include <QWidget>

class MvQBufrDataItem;
class MvEccBufrMessage;
class MvQKeyProfileModel;
class MvQKeyProfileSortFilterModel;

class BufrKeyWidget : public QWidget, protected Ui::BufrKeyWidget
{
    Q_OBJECT

public:
    explicit BufrKeyWidget(QWidget* parent = nullptr);
    void setData(MvQBufrDataItem*, int, MvEccBufrMessage*);
    void clear();

protected slots:
    void slotValueItemSelected(const QModelIndex& idx);

signals:
    void subsetSelected(int);

private:
    QString formatKey(QString);
    QString formatRow(QString key, QString val);
    QString formatElementBlock(QString val);

    MvQKeyProfileModel* model_{nullptr};
    MvQKeyProfileSortFilterModel* sortModel_{nullptr};
    bool broadcast_{true};
};

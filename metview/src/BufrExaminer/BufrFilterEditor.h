/***************************** LICENSE START ***********************************

 Copyright 2017 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "BufrFilterDef.h"

#include <set>

#include <QDate>
#include <QGridLayout>
#include <QIcon>
#include <QJsonValue>
#include <QMap>
#include <QSet>
#include <QPair>
#include <QPen>
#include <QWidget>

class BufrMetaData;
class EditLine;
class EditLineHeader;
class MvQProperty;

class BufrFilterEditor : public QWidget
{
    Q_OBJECT

public:
    explicit BufrFilterEditor(QWidget* parent = nullptr);
    ~BufrFilterEditor() override = default;

    const BufrFilterDef& filterDef();
    void expandAllGroups();
    void expandEditedGroups();
    void collapseAllGroups();
    void clear();
    void applyChange();
    void initHelpers(BufrMetaData* decoder, int msgIdx);
    void updateHelpers(BufrMetaData* decoder, int msgIdx);

public Q_SLOTS:
    void delayedForceRepaint();
    void forceRepaint();

protected Q_SLOTS:
    void slotOutputLeChanged(EditLine*, QString mode);
    void slotParamCountLeChanged(EditLine*, QString mode);
    void slotCoordModeLeChanged(EditLine*, QString mode);
    void slotCoordKeyCountLeChanged(EditLine*, QString mode);
    void slotDateModeChanged(EditLine*, QString mode);
    void slotCustomModeLeChanged(EditLine*, QString mode);
    void slotCustomKeyCountLeChanged(EditLine*, QString mode);

protected:
    void paintEvent(QPaintEvent* event) override;
    void addToDef(QString id);
    MvQProperty* findProperty(QString id) const;
    QString lineValue(QString id);
    void loadInit();
    void loadProperty(QJsonValue val, MvQProperty* prop);
    void adjustExtractOptions();
    void adjustCoordOptions();
    void adjustCustomOptions();
    void setParGroupVisibility(QString parPattern, int firstHiddenKey);
    void updateHelpers(int);

private:
    QGridLayout* grid_{nullptr};
    QMap<QString, EditLineHeader*> headers_;
    QMap<QString, EditLine*> lines_;
    BufrFilterDef def_;
    QList<MvQProperty*> groups_;
    QList<MvQProperty*> props_;
    QSet<QPair<int, int> > helperTypes_;
    QPen linePen_;
    QPen framePen_;
};

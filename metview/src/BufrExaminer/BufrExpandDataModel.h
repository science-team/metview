/***************************** LICENSE START ***********************************

 Copyright 2019 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>

class MvKeyProfile;

class BufrExpandDataModel : public QAbstractItemModel
{
public:
    BufrExpandDataModel(QObject* parent);
    ~BufrExpandDataModel() override;
    bool loadDumpData(const std::string&, QString& err);
    void clear();

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& index) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    QVariant headerData(const int section, const Qt::Orientation orient, const int role) const override;
    QModelIndex index(int row, int column, const QModelIndex& parent) const override;
    QModelIndex parent(const QModelIndex& index) const override;

private:
    MvKeyProfile* templateProf_{nullptr};
    MvKeyProfile* prof_{nullptr};
};

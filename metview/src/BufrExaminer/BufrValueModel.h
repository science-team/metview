/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QPixmap>

class MvQBufrDataItem;

class BufrValueModel : public QAbstractItemModel
{
public:
    explicit BufrValueModel(QObject* parent = 0);
    ~BufrValueModel();

    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    QVariant data(const QModelIndex& index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex& index) const;

private:
    // MvQBufrDataItem* indexToItem(const QModelIndex& idx) const;

    MvQBufrDataItem* root_;
    QStringList keys_;
    // bool compressed_;
    // int subsetNumber_;
    // QPixmap arrayPix_;
};

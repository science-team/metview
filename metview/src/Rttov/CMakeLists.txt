
ecbuild_add_executable( TARGET       RttovRun
                        SOURCES      RttovRun.cc
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                      )


ecbuild_add_executable( TARGET       RttovVisualiser
                        SOURCES      RttovVisualiser.cc
                        DEFINITIONS  ${METVIEW_EXTRA_DEFINITIONS}
                        INCLUDES     ${METVIEW_STANDARD_INCLUDES}
                        LIBS         ${STANDARD_METVIEW_LIBS}
                      )


metview_module_files(ETC_FILES ObjectSpec.Rttov 
                               RttovRunDef
                               RttovRunRules
                               RttovVisualiserDef
                               RttovVisualiserRules
                    )



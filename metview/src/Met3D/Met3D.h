/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

/***********************************************************************
  Application Met3D.
  Get the input data and call external application Met3D
***********************************************************************/

#include "Metview.h"

class Met3D : public MvService
{
public:
    // Constructor
    Met3D(const char* kw);

    // Destructor
    ~Met3D();

    void serve(MvRequest&, MvRequest&);

    // Initialize variables from user interface
    bool GetInputInfo(MvRequest& in);

    // Execute Met3D
    void execute();

protected:
    // Variables
    std::string path_;           // input data path
    std::string frontend_file_;  // input frontend file path
    std::string pipeline_file_;  // input pipeline file path
};

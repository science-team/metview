/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "mars.h"
#undef D  /// MARS
#include <QtWidgets>


class XServBar : public QWidget
{
    Q_OBJECT

public:
    XServBar(svc* s);
    ~XServBar() override = default;


public slots:
    void buttonClicked();


private:
    svc* service_;
    QHBoxLayout layoutMain_;
    QScrollArea scrollArea_;
    QGridLayout buttonLayout_;
    QWidget* scrollAreaInternalWidget_;
};

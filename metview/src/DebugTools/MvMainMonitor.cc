/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <Metview.h>
#include "mars.h"
#include <iostream>

#include "MvQApplication.h"

#include "ProcMonitor.h"
#include "MvQTheme.h"

svc* s = nullptr;
int ac;
char** av;

ProcMonitor* mon;

void browse_cb(svcid* id, request* r, void*)
{
    mon->input(id, r);
}

void dummy_cb(svcid* id, request* /*r*/, void*)
{
    send_reply(id, nullptr);
}

void monitor()
{
    request* t = empty_request("MONITOR");
    call_switchboard(s, t);
    free_all_requests(t);
}

int main(int argc, char** argv)
{
    MvQApplication app(argc, argv, nullptr, QStringList{"debug", "window"});

    QApplication::setWindowIcon(QIcon(MvQTheme::logo()));

    s = app.getService();
    add_service_callback(s, nullptr, dummy_cb, nullptr);
    add_progress_callback(s, nullptr, browse_cb, nullptr);

    mon = new ProcMonitor(s);

    // start
    monitor();

    mon->show();

    return app.exec();
}

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QColor>
#include <QSortFilterProxyModel>

#include "MvProcNode.h"

class MvQPixmapCache;


class MvQProcFilterModel : public QSortFilterProxyModel
{
public:
    MvQProcFilterModel(QObject* parent = nullptr);
    bool lessThan(const QModelIndex& left,
                  const QModelIndex& right) const override;
};

class MvQProcModel : public QAbstractItemModel, public MvProcNodeObserver
{
public:
    MvQProcModel(QObject* parent = nullptr);

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    void dataIsAboutToChange();
    void changeData(MvProcNode*);
    void updateData();
    MvProcNode* itemForIndex(const QModelIndex&) const;
    MvQPixmapCache* pixmapCache() { return pm_; }

    // Observer method
    void infoChanged(MvProcNode*) override;

protected:
    QModelIndex indexForItem(MvProcNode*, int);

    MvProcNode* rootItem_;
    QColor cpuYellow_;
    QColor cpuOrange_;
    QColor cpuRed_;
    QColor textGrey_;

    MvQPixmapCache* pm_;
};

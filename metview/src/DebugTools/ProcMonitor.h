/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

#include "mars.h"

#include <QList>
#include <QStyledItemDelegate>

#include "MvQMainWindow.h"

#include <string>

class QAbstractButton;
class QDialogButtonBox;
class QModelIndex;
class QTextEdit;
class QSplitter;
class QTabWidget;

class MvQPixmapCache;
class MvQProcModel;
class MvQProcFilterModel;
class MvProcNode;
class MvQTreeView;

class ProcViewDelegate : public QStyledItemDelegate
{
public:
    ProcViewDelegate(QWidget* parent = 0) :
        QStyledItemDelegate(parent) {}
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const;
};

class ProcMonitor : public MvQMainWindow
{
    Q_OBJECT
public:
    ProcMonitor(svc*, QWidget* parent = 0);
    void input(svcid*, request*);

public slots:
    void slotProcessService();
    void slotButtonClicked(QAbstractButton*);
    void slotContextMenu(const QPoint&);
    void slotProcInfo(const QModelIndex&);

protected:
    void closeEvent(QCloseEvent*);
    void adjustView();
    void showInfo(MvProcNode*);
    void showRequest(MvProcNode*);
    void addInfoRow(QString, const std::string&, QString&);
    void addTitleInfoRow(QString, QString&, int);
    void loadIcons(MvQPixmapCache*);
    void startService();
    void readSettings();
    void writeSettings();

    svc* service_;
    QSplitter* mainSplitter_;

    MvQProcModel* procModel_;
    MvQProcFilterModel* filterModel_;
    MvQTreeView* procTree_;
    QTextEdit* procInfo_;
    QTextEdit* procReq_;
    QDialogButtonBox* buttonBox_;
    QTabWidget* infoTab_;
    bool firstUpdate_;
};

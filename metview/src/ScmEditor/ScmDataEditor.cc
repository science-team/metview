/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QAbstractItemView>
#include <QAction>
#include <QApplication>
#include <QComboBox>
#include <QDebug>
#include <QDialogButtonBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QListWidget>
#include <QMessageBox>
#include <QPainter>
#include <QPen>
#include <QPushButton>
#include <QScrollBar>
#include <QSettings>
#include <QSplitter>
#include <QStyledItemDelegate>
#include <QToolButton>
#include <QVBoxLayout>

#include "ScmDataEditor.h"

#include "MvQAbout.h"
#include "MvQFileInfoLabel.h"
#include "MvQProfileWidget.h"
#include "MvQScmDataWidget.h"
#include "MvQMethods.h"
#include "MvQTheme.h"

class MvQScmListDelegate : public QStyledItemDelegate
{
public:
    MvQScmListDelegate(QObject* parent = 0) :
        QStyledItemDelegate(parent) { pen_ = QPen(MvQTheme::colour("scmview", "params_border")); }

    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const
    {
        // Paint everything
        QStyledItemDelegate::paint(painter, option, index);

        // Paint the border
        painter->save();
        painter->setPen(pen_);
        painter->drawRect(option.rect);
        painter->restore();
    }

protected:
    QPen pen_;
};


ScmDataEditor::ScmDataEditor(QWidget* parent) :
    MvQMainWindow(parent),
    data_(0),
    profileData_(0),
    ignoreParListW_(false)
{
    setAttribute(Qt::WA_DeleteOnClose);

    setWindowTitle(tr("Metview - Profile Data Editor"));

    // Initial size
    setInitialSize(1100, 800);

    //----------------------
    // Main layout
    //----------------------

    auto* w = new QWidget(this);
    mainLayout_ = new QVBoxLayout(w);
    mainLayout_->setContentsMargins(0, 0, 0, 0);
    mainLayout_->setSpacing(1);
    setCentralWidget(w);

    //------------------
    // File info label
    //------------------

    fileInfoLabel_ = new MvQFileInfoLabel;
    mainLayout_->addWidget(fileInfoLabel_);

    //-------------------
    // Central splitter
    //-------------------

    centralSplitter_ = new QSplitter;
    centralSplitter_->setOpaqueResize(false);
    mainLayout_->addWidget(centralSplitter_, 1);

    //-------------------
    // Central tab
    //-------------------

    dataPanel_ = new MvQScmDataWidget(this);
    centralSplitter_->addWidget(dataPanel_);

    //-------------------
    // Preview area
    //-------------------

    auto* viewVb = new QVBoxLayout;
    viewVb->setContentsMargins(0, 0, 0, 0);
    viewVb->setSpacing(1);
    w = new QWidget(this);
    w->setLayout(viewVb);
    centralSplitter_->addWidget(w);

    // Parameter list
    parListW_ = new QListWidget(this);
    parListW_->setFlow(QListView::LeftToRight);
    parListW_->setWrapping(false);

    parListW_->setSpacing(2);
    parListW_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    parListW_->setItemDelegate(new MvQScmListDelegate(this));

    QScrollBar* hbar = parListW_->horizontalScrollBar();
    if (hbar) {
        QFont font;
        QFontMetrics fm(font);
        parListW_->setMaximumHeight(fm.height() + hbar->size().height() + 4);
    }

    viewVb->addWidget(parListW_);

    profilePanel_ = new MvQProfileWidget(this);
    viewVb->addWidget(profilePanel_, 1);

    //--------------------------
    // Buttonbox
    //--------------------------

    auto* buttonBoxHb = new QHBoxLayout;

    buttonBox_ = new QDialogButtonBox(this);

    savePb_ = buttonBox_->addButton(QDialogButtonBox::Save);
    okPb_ = buttonBox_->addButton(QDialogButtonBox::Ok);
    cancelPb_ = buttonBox_->addButton(QDialogButtonBox::Cancel);
    resetPb_ = buttonBox_->addButton(QDialogButtonBox::Reset);

    savePb_->setDefault(true);
    savePb_->setEnabled(false);
    resetPb_->setEnabled(false);

    // buttonBox_->addButton(QDialogButtonBox::Close);

    connect(buttonBox_, SIGNAL(clicked(QAbstractButton*)),
            this, SLOT(slotButtonClicked(QAbstractButton*)));

    buttonBoxHb->addSpacing(5);
    buttonBoxHb->addWidget(buttonBox_);
    buttonBoxHb->addSpacing(5);

    mainLayout_->addSpacing(5);
    mainLayout_->addLayout(buttonBoxHb);
    mainLayout_->addSpacing(5);

    //----------------------------
    // Setup actions
    //----------------------------

    setupEditActions();
    setupViewActions();
    setupAnimationActions();
    setupHelpActions();

    //----------------------------
    // Setup menus and toolbars
    //----------------------------

    setupMenus(menuItems_);

    //-----------------
    // Signals slots
    //-----------------

    connect(dataPanel_, SIGNAL(dataEdited(const MvScmProfileChange&)),
            this, SLOT(slotDataEdited(const MvScmProfileChange&)));

    connect(dataPanel_, SIGNAL(paramSelected(MvScmVar*, int)),
            this, SLOT(slotParamSelectedFromData(MvScmVar*, int)));

    connect(dataPanel_, SIGNAL(stepChanged(int)),
            this, SLOT(slotStepChanged(int)));

    //----------------------------
    // Read settings
    //----------------------------

    readSettings();
}

ScmDataEditor::~ScmDataEditor()
{
    writeSettings();
}

void ScmDataEditor::setupEditActions()
{
    actionRedo_ = new QAction(this);
    actionRedo_->setObjectName(QString::fromUtf8("actionRedo"));
    actionRedo_->setText(tr("Redo"));
    actionRedo_->setToolTip(tr("Redo"));
    actionRedo_->setShortcut(tr("Ctrl+Shift+Z"));
    QIcon icon;
    icon.addPixmap(QPixmap(QString::fromUtf8(":/scmEditor/redo.svg")), QIcon::Normal, QIcon::Off);
    actionRedo_->setIcon(icon);
    actionRedo_->setEnabled(false);

    actionUndo_ = new QAction(this);
    actionUndo_->setObjectName(QString::fromUtf8("actionUndo"));
    actionUndo_->setText(tr("Undo"));
    actionUndo_->setToolTip(tr("Undo"));
    actionUndo_->setShortcut(tr("Ctrl+Z"));
    QIcon icon1;
    icon1.addPixmap(QPixmap(QString::fromUtf8(":/scmEditor/undo.svg")), QIcon::Normal, QIcon::Off);
    actionUndo_->setIcon(icon1);
    actionUndo_->setEnabled(false);

    actionOverwrite_ = new QAction(this);
    actionOverwrite_->setObjectName(QString::fromUtf8("actionOverwrite"));
    actionOverwrite_->setText(tr("Overwrite steps"));
    actionOverwrite_->setToolTip(tr("Overwrite steps with current step"));
    actionOverwrite_->setIcon(QPixmap(QString::fromUtf8(":/scmEditor/overwrite_steps.svg")));

    actionConsistency_ = new QAction(this);
    actionConsistency_->setObjectName(QString::fromUtf8("actionLink"));
    actionConsistency_->setText(tr("Check consistency"));
    actionConsistency_->setToolTip(tr("Check consistency between parameters"));
    actionConsistency_->setCheckable(true);
    actionConsistency_->setChecked(false);
    actionConsistency_->setIcon(QPixmap(QString::fromUtf8(":/scmEditor/check_consistency.svg")));

    connect(actionUndo_, SIGNAL(triggered(bool)),
            this, SLOT(slotUndo(bool)));

    connect(actionRedo_, SIGNAL(triggered(bool)),
            this, SLOT(slotRedo(bool)));

    connect(actionOverwrite_, SIGNAL(triggered(bool)),
            this, SLOT(slotOverwrite(bool)));

    connect(actionConsistency_, SIGNAL(toggled(bool)),
            this, SLOT(slotConsistency(bool)));

    MvQMainWindow::MenuType menuType = MvQMainWindow::EditMenu;
    menuItems_[menuType].push_back(new MvQMenuItem(actionUndo_));
    menuItems_[menuType].push_back(new MvQMenuItem(actionRedo_));
    menuItems_[menuType].push_back(new MvQMenuItem(actionOverwrite_));
    menuItems_[menuType].push_back(new MvQMenuItem(actionConsistency_));
}


void ScmDataEditor::setupAnimationActions()
{
    actionNext_ = new QAction(this);
    actionNext_->setObjectName(QString::fromUtf8("actionRedo"));
    actionNext_->setText(tr("&Next"));
    actionNext_->setToolTip(tr("Next step"));
    actionNext_->setShortcut(tr("Right"));
    QIcon icon;
    icon.addPixmap(QPixmap(QString::fromUtf8(":/scmEditor/player_next.svg")), QIcon::Normal, QIcon::Off);
    actionNext_->setIcon(icon);

    actionPrev_ = new QAction(this);
    actionPrev_->setObjectName(QString::fromUtf8("actionUndo"));
    actionPrev_->setText(tr("&Previous"));
    actionPrev_->setToolTip(tr("Previous step"));
    actionPrev_->setShortcut(tr("Left"));
    QIcon icon1;
    icon1.addPixmap(QPixmap(QString::fromUtf8(":/scmEditor/player_prev.svg")), QIcon::Normal, QIcon::Off);
    actionPrev_->setIcon(icon1);

    connect(actionNext_, SIGNAL(triggered(bool)),
            this, SLOT(slotToNext(bool)));

    connect(actionPrev_, SIGNAL(triggered(bool)),
            this, SLOT(slotToPrev(bool)));

    // Combo box
    // We will add items + signal/slots later in "init"!
    auto* hb = new QHBoxLayout;
    hb->setContentsMargins(10, 2, 10, 2);

    // Combo box for key profile selection
    auto* label = new QLabel(tr("&Steps:"));
    stepCombo_ = new QComboBox;
    stepCombo_->view()->setTextElideMode(Qt::ElideNone);
    stepCombo_->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    label->setBuddy(stepCombo_);

    hb->addWidget(label);
    hb->addWidget(stepCombo_);
    auto* wh = new QWidget;
    wh->setLayout(hb);

    // Slot for the ombo will be added later in init

    MvQMainWindow::MenuType menuType = MvQMainWindow::StepMenu;
    menuItems_[menuType].push_back(new MvQMenuItem(wh));
    menuItems_[menuType].push_back(new MvQMenuItem(actionPrev_));
    menuItems_[menuType].push_back(new MvQMenuItem(actionNext_));
}


void ScmDataEditor::setupViewActions()
{
    //
    actionFileInfo_ = new QAction(this);
    actionFileInfo_->setObjectName(QString::fromUtf8("actionFileInfo"));
    actionFileInfo_->setText(tr("File info"));
    actionFileInfo_->setCheckable(true);
    actionFileInfo_->setChecked(true);
    actionFileInfo_->setToolTip(tr("View file info"));
    QIcon icon;
    icon.addPixmap(QPixmap(QString::fromUtf8(":/examiner/fileInfo.svg")), QIcon::Normal, QIcon::Off);
    actionFileInfo_->setIcon(icon);

    // Define routines
    connect(actionFileInfo_, SIGNAL(toggled(bool)),
            fileInfoLabel_, SLOT(setVisible(bool)));


    QAction* ac;
    ac = new QAction(this);
    ac->setObjectName(QString::fromUtf8("actionShowEditable"));
    ac->setText(tr("Show editable parameters only"));
    ac->setCheckable(true);
    ac->setChecked(false);
    ac->setToolTip(tr("Show editable parameters only"));
    QIcon icon1;
    icon1.addPixmap(QPixmap(QString::fromUtf8(":/scmEditor/show_editable_only.svg")), QIcon::Normal, QIcon::Off);
    ac->setIcon(icon1);

    actionShowEditable_ = ac;

    connect(actionShowEditable_, SIGNAL(toggled(bool)),
            this, SLOT(slotShowEditableParams(bool)));


    MvQMainWindow::MenuType menuType = MvQMainWindow::ViewMenu;
    menuItems_[menuType].push_back(new MvQMenuItem(actionFileInfo_));
    menuItems_[menuType].push_back(new MvQMenuItem(actionShowEditable_));
}

void ScmDataEditor::setupHelpActions()
{
    // About
    auto* actionAbout = new QAction(this);
    actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
    actionAbout->setText(tr("&About Profile Data Editor"));

    connect(actionAbout, SIGNAL(triggered()), this, SLOT(slotShowAboutBox()));

    MvQMainWindow::MenuType menuType = MvQMainWindow::HelpMenu;
    menuItems_[menuType].push_back(new MvQMenuItem(actionAbout, MvQMenuItem::MenuTarget));
}

void ScmDataEditor::init(MvScm* data)
{
    if (data->stepNum() <= 0) {
        data_ = 0;
        return;
    }

    data_ = data;

    // Set actual consistency status in data
    data_->needConsistency(actionConsistency_->isChecked());

    // Get editable variables
    editableVars_ << data_->mlVar(MvScm::TempML) << data_->mlVar(MvScm::WindUML) << data_->mlVar(MvScm::WindVML) << data_->mlVar(MvScm::SpHumML) << data_->mlVar(MvScm::RelHumML) << data_->mlVar(MvScm::CloudLiqML) << data_->mlVar(MvScm::CloudIceML) << data_->mlVar(MvScm::OzoneML);

    // Soil
    editableVars_ << data_->soilVar(MvScm::TempSoil) << data_->soilVar(MvScm::HumSoil);

    // Siurface
    for (auto i : data_->surfaceLevel())
        editableVars_ << i;

    // This method will select a paramater in the table view! This will trigger an uopdate in he profile view as well!
    dataPanel_->init(data, editableVars_);


    QFont editableFont;
    editableFont.setBold(true);
    // Par list widget
    for (int i = 0; i < static_cast<int>(data_->modelLevel().size()); i++) {
        QString name = QString::fromStdString(data_->modelLevel().at(i)->name());
        if (name.size() == 1) {
            name = "   " + name + "   ";
        }
        else if (name.size() == 2) {
            name = "  " + name + "  ";
        }

        auto* item = new QListWidgetItem(name);
        item->setData(Qt::UserRole, i);

        if (editableVars_.contains(data_->modelLevel().at(i))) {
            item->setBackground(MvQTheme::colour("scmview", "params_editable_bg"));
            item->setForeground(MvQTheme::colour("scmview", "params_editable_text"));
            item->setFont(editableFont);
        }
        else {
            item->setBackground(MvQTheme::colour("scmview", "params_bg"));
        }
        parListW_->addItem(item);
    }

    connect(parListW_, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)),
            this, SLOT(slotParamSelectedFromList(QListWidgetItem*, QListWidgetItem*)));

    // Steps
    editPix_ = QPixmap(QString::fromUtf8(":/scmEditor/edit.svg"));
    noEditPix_ = QPixmap(editPix_.size());
    noEditPix_.fill(QColor(255, 255, 255, 0));

    QFont font;
    QFontMetrics fm(font);
    stepCombo_->setIconSize(QSize(16, 16));

    const std::vector<float>& steps = data_->steps();

    for (float step : steps) {
        QString txt = QString::number(step / 3600) + " h      ";
        stepCombo_->addItem(noEditPix_, txt);
        stepCombo_->setItemData(stepCombo_->count() - 1, 0);
    }

    connect(stepCombo_, SIGNAL(currentIndexChanged(int)),
            dataPanel_, SLOT(slotStepChanged(int)));

    stepCombo_->setCurrentIndex(0);
    updateAnimationActionState();

    if (static_cast<int>(steps.size()) <= 1) {
        actionOverwrite_->setEnabled(false);
    }

    // Fileinfo label
    updateFileInfoLabel();
}

void ScmDataEditor::reload()
{
    dataPanel_->reload();
    profilePanel_->reload();
}

void ScmDataEditor::slotParamSelectedFromData(MvScmVar* var, int step)
{
    if (profileData_ && profileData_->var() == var && profileData_->step() == step) {
        return;
    }

    updateAnimationActionState();

    bool editable = (editableVars_.indexOf(var) != -1) ? true : false;

    QList<MvProfileData*> lst;

    profileData_ = new MvScmProfileData(data_, var, step);
    profileData_->setObserver(this);
    lst << profileData_;

    // Temperature
    if (data_->mlVar(MvScm::TempML) == var) {
        // Surf
        MvScmVar* skinT = data_->surfVar(MvScm::TempSurf);
        if (skinT) {
            auto* p = new MvScmProfileData(data_, skinT, step);
            lst << p;
            p->setObserver(this);
        }

        // Soil
        MvScmVar* soilT = data_->soilVar(MvScm::TempSoil);
        if (soilT) {
            auto* p = new MvScmProfileData(data_, soilT, step);
            lst << p;
            p->setObserver(this);
        }
    }

    profilePanel_->setProfile(lst, editable);

    // Set the current index for param list
    updateParListW(var);
}

void ScmDataEditor::slotParamSelectedFromList(QListWidgetItem* current, QListWidgetItem*)
{
    if (ignoreParListW_)
        return;

    if (current) {
        int index = current->data(Qt::UserRole).toInt();
        if (index >= 0 && index < static_cast<int>(data_->modelLevel().size())) {
            slotParamSelectedFromData(data_->modelLevel().at(index), stepCombo_->currentIndex());
            dataPanel_->selectMlParam(data_->modelLevel().at(index));
        }
    }
}

void ScmDataEditor::updateParListW(MvScmVar* var)
{
    if (!var)
        return;

    ignoreParListW_ = true;

    for (int i = 0; i < static_cast<int>(data_->modelLevel().size()); i++) {
        if (data_->modelLevel().at(i) == var) {
            parListW_->setCurrentRow(i);
            break;
        }
    }
    ignoreParListW_ = false;
}

void ScmDataEditor::slotStepChanged(int step)
{
    updateAnimationActionState();

    if (profileData_)
        slotParamSelectedFromData(profileData_->var(), step);
}

void ScmDataEditor::slotShowAboutBox()
{
    MvQAbout about("Profile Data Editor", "", MvQAbout::MetviewVersion);
    about.exec();
}

void ScmDataEditor::updateFileInfoLabel()
{
    fileInfoLabel_->setProfileTextLabel(QString::fromStdString(data_->fileName()), data_->stepNum(),
                                        data_->modelLevelNum(), QString::fromStdString(data_->id()));
}

// Callback from the table view editor
void ScmDataEditor::slotDataEdited(const MvScmProfileChange& item)
{
    profilePanel_->update(item);
    registerValueChange(item);

    updateStepStatus(item.step(), true);
    updateParListW(item.var());

    savePb_->setEnabled(true);
    resetPb_->setEnabled(true);
}

// ProfileObserver method - Callback from curve plot editor, via the data object
void ScmDataEditor::profileEdited(const MvScmProfileChange& item)
{
    dataPanel_->update(item);
    registerValueChange(item);

    updateStepStatus(item.step(), true);
    updateParListW(item.var());

    savePb_->setEnabled(true);
    resetPb_->setEnabled(true);
}

void ScmDataEditor::slotRedo(bool)
{
    const MvScmProfileChange& item = editHistory_.redo();
    if (item.var()) {
        item.var()->setValue(item.step(), item.level(), item.value());
        dataPanel_->update(item);
        profilePanel_->update(item);

        if (item.group() == MvProfileChange::GroupStart ||
            item.group() == MvProfileChange::GroupMember) {
            slotRedo(true);
        }
    }

    updateEditActionStatus();
}

void ScmDataEditor::slotUndo(bool)
{
    const MvScmProfileChange& item = editHistory_.undo();
    if (item.var()) {
        item.var()->setValue(item.step(), item.level(), item.prevValue());
        dataPanel_->update(item);
        profilePanel_->update(item);

        if (item.group() == MvProfileChange::GroupEnd ||
            item.group() == MvProfileChange::GroupMember) {
            slotUndo(true);
        }
    }

    updateEditActionStatus();
}

void ScmDataEditor::registerValueChange(const MvScmProfileChange& item)
{
    editHistory_.add(item);
    updateEditActionStatus();
}

void ScmDataEditor::updateEditActionStatus()
{
    if (editHistory_.canRedo()) {
        actionRedo_->setEnabled(true);
    }
    else {
        actionRedo_->setEnabled(false);
    }

    if (editHistory_.canUndo()) {
        actionUndo_->setEnabled(true);
    }
    else {
        actionUndo_->setEnabled(false);
    }
}

void ScmDataEditor::slotToNext(bool)
{
    int index = stepCombo_->currentIndex();
    if (index < stepCombo_->count() - 1)
        stepCombo_->setCurrentIndex(index + 1);

    updateAnimationActionState();
}

void ScmDataEditor::slotToPrev(bool)
{
    int index = stepCombo_->currentIndex();
    if (index > 0)
        stepCombo_->setCurrentIndex(index - 1);

    updateAnimationActionState();
}

void ScmDataEditor::updateStepStatus(int step, bool edited)
{
    if (step >= 0 && step < stepCombo_->count()) {
        bool act = stepCombo_->itemData(step).toBool();

        if (act != edited) {
            if (edited)
                stepCombo_->setItemIcon(step, editPix_);
            else
                stepCombo_->setItemIcon(step, noEditPix_);
        }
    }
}

void ScmDataEditor::updateAnimationActionState()
{
    if (stepCombo_->count() <= 1) {
        actionPrev_->setEnabled(false);
        actionNext_->setEnabled(false);
    }
    else if (stepCombo_->currentIndex() == 0) {
        actionPrev_->setEnabled(false);
        actionNext_->setEnabled(true);
    }
    else if (stepCombo_->currentIndex() == stepCombo_->count() - 1) {
        actionNext_->setEnabled(false);
        actionPrev_->setEnabled(true);
    }
    else {
        actionNext_->setEnabled(true);
        actionPrev_->setEnabled(true);
    }
}


void ScmDataEditor::slotShowEditableParams(bool showEditable)
{
    if (showEditable) {
        for (int i = 0; i < parListW_->count(); i++) {
            QListWidgetItem* item = parListW_->item(i);
            int index = item->data(Qt::UserRole).toInt();
            if (!editableVars_.contains(data_->modelLevel().at(index))) {
                item->setHidden(true);
            }
        }
    }
    else {
        for (int i = 0; i < parListW_->count(); i++)
            parListW_->item(i)->setHidden(false);
    }

    dataPanel_->showEditableParams(showEditable);
}

void ScmDataEditor::slotOverwrite(bool)
{
    if (data_) {
        int ts = stepCombo_->currentIndex();

        if (QMessageBox::warning(0, QObject::tr("Overwrite steps"), QObject::tr("Do you really want to overwrite all other steps with the data in the current step (") + stepCombo_->currentText().simplified() + ") ?",
                                 QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel) == QMessageBox::Yes) {
            data_->overwrite(ts);

            // Clear the edithistory
            editHistory_.clear();
            updateEditActionStatus();
        }
    }
}

void ScmDataEditor::slotConsistency(bool b)
{
    if (data_)
        data_->needConsistency(b);
}


void ScmDataEditor::slotButtonClicked(QAbstractButton* button)
{
    if (!button)
        return;

    if (buttonBox_->standardButton(button) == QDialogButtonBox::Cancel) {
        closeEditor();
    }
    else if (buttonBox_->standardButton(button) == QDialogButtonBox::Ok) {
        apply();
        closeEditor();
    }
    else if (buttonBox_->standardButton(button) == QDialogButtonBox::Save) {
        apply();

        savePb_->setEnabled(false);
        resetPb_->setEnabled(false);
    }
    else if (buttonBox_->standardButton(button) == QDialogButtonBox::Reset) {
        reset();
        savePb_->setEnabled(false);
        resetPb_->setEnabled(false);
    }
}

void ScmDataEditor::apply()
{
    data_->save();
}

void ScmDataEditor::reset()
{
    data_->reset();
    reload();
    editHistory_.clear();
    updateEditActionStatus();
}

void ScmDataEditor::closeEditor()
{
    close();
}

void ScmDataEditor::writeSettings()
{
    QSettings settings(MVQ_QSETTINGS_DEFAULT_ARGS, "MV4-ScmDataEditor");
    settings.clear();

    settings.beginGroup("mainWindow");
    settings.setValue("geometry", saveGeometry());
    // settings.setValue("mainSplitter",mainSplitter_->saveState());
    settings.setValue("centralSplitter", centralSplitter_->saveState());
    settings.setValue("actionFileInfoStatus", actionFileInfo_->isChecked());
    settings.setValue("actionShowEditableStatus", actionShowEditable_->isChecked());
    settings.setValue("actionConsistencyStatus", actionConsistency_->isChecked());
    settings.endGroup();

    profilePanel_->writeSettings(settings);
}

void ScmDataEditor::readSettings()
{
    QSettings settings(MVQ_QSETTINGS_DEFAULT_ARGS, "MV4-ScmDataEditor");

    settings.beginGroup("mainWindow");
    restoreGeometry(settings.value("geometry").toByteArray());
    // mainSplitter_->restoreState(settings.value("mainSplitter").toByteArray());
    centralSplitter_->restoreState(settings.value("centralSplitter").toByteArray());

    if (settings.value("actionFileInfoStatus").isNull())
        actionFileInfo_->setChecked(true);
    else
        actionFileInfo_->setChecked(settings.value("actionFileInfoStatus").toBool());

    if (settings.value("actionShowEditableStatus").isNull())
        actionShowEditable_->setChecked(false);
    else
        actionShowEditable_->setChecked(settings.value("actionShowEditableStatus").toBool());

    if (settings.value("actionConsistencyStatus").isNull())
        actionConsistency_->setChecked(false);
    else
        actionConsistency_->setChecked(settings.value("actionConsistencyStatus").toBool());

    settings.endGroup();

    profilePanel_->readSettings(settings);
}

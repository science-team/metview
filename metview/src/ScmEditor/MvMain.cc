/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#include <string>

#include "Metview.h"
#include "MvQApplication.h"

#include "ScmDataEditor.h"

int main(int argc, char** argv)
{
    if (argc < 2) {
        marslog(LOG_EROR, "No arguments are specified");
        exit(1);
    }

    MvRequest in;
    in.read(argv[1], false, true);

    marslog(LOG_INFO, "Request:");
    in.print();

    if (!in) {
        marslog(LOG_EROR, "No request could be read from request file=%s", argv[1]);
        exit(1);
    }

    // Get input file name
    std::string fPath;
    if (const char* fPathCh = in("PATH")) {
        fPath = std::string(fPathCh);
    }
    else {
        marslog(LOG_EROR, "No PATH is specified!");
        exit(1);
    }

    MvQApplication::writePidToFile(in);

    // Create the qt application. The appname must be unique!
    std::string appName = MvApplication::buildAppName("ScmDataEditor");
    MvQApplication app(argc, argv, appName.c_str(), {"examiner", "window", "scmEditor"});

    //    QFont font = app.font();
    //    font.setPointSize(9);
    //    app.setFont(font);

    MvScm data(fPath);

    auto* browser = new ScmDataEditor;
    browser->init(&data);
    browser->setAppIcon("SCM_INPUT_DATA");
    browser->show();

    // register for callbacks from Desktop
    app.registerToDesktop(browser);

    // Enter the app loop
    app.exec();
}

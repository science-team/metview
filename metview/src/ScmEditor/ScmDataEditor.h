/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QMainWindow>
#include <QModelIndex>
#include <QPixmap>

#include "MvQMainWindow.h"
#include "MvQMenuItem.h"

#include "MvCommandHistory.h"
#include "MvScm.h"

class QAbstractButton;
class QAction;
class QComboBox;
class QDialogButtonBox;
class QLabel;
class QListWidget;
class QListWidgetItem;
class QPushButton;
class QSplitter;
class QVBoxLayout;

class MvQFileInfoLabel;
class MvQLogBrowser;
class MvQProfileWidget;
class MvQRangeWidget;
class MvQScmDataWidget;

class MvScm;
class MvScmVar;
class MvScmProfileData;


class ScmDataEditor : public MvQMainWindow, public MvScmProfileObserver
{
    Q_OBJECT

public:
    ScmDataEditor(QWidget* parent = 0);
    ~ScmDataEditor();
    void init(MvScm*);
    void updateFileInfoLabel();

    // From MvScmProfileObserver
    void profileEdited(const MvScmProfileChange&);

public slots:
    void slotShowAboutBox();
    void slotParamSelectedFromData(MvScmVar*, int);
    void slotParamSelectedFromList(QListWidgetItem*, QListWidgetItem*);
    void slotStepChanged(int);
    void slotToNext(bool);
    void slotToPrev(bool);
    void slotDataEdited(const MvScmProfileChange&);
    void slotUndo(bool);
    void slotRedo(bool);
    void slotOverwrite(bool);
    void slotConsistency(bool);
    void slotShowEditableParams(bool);
    void slotButtonClicked(QAbstractButton*);

protected:
    void setupEditActions();
    void setupViewActions();
    void setupSettingsActions();
    void setupAnimationActions();
    void setupHelpActions();
    void setupProfileBox();
    void reload();
    void updateParListW(MvScmVar*);

    void registerValueChange(const MvScmProfileChange&);
    void updateEditActionStatus();
    void updateStepStatus(int, bool);
    void updateAnimationActionState();

    void apply();
    void reset();
    void closeEditor();
    void readSettings();
    void writeSettings();

    MvScm* data_;
    MvQMainWindow::MenuItemMap menuItems_;
    MvQFileInfoLabel* fileInfoLabel_;

    QVBoxLayout* mainLayout_;
    QSplitter* mainSplitter_;
    QSplitter* centralSplitter_;
    QAction* actionFileInfo_;
    QAction* actionRedo_;
    QAction* actionUndo_;
    QAction* actionShowEditable_;
    QAction* actionOverwrite_;
    QAction* actionConsistency_;

    QList<MvScmVar*> editableVars_;
    MvCommandHistory<MvScmProfileChange> editHistory_;

    QDialogButtonBox* buttonBox_;
    QPushButton* savePb_;
    QPushButton* okPb_;
    QPushButton* cancelPb_;
    QPushButton* resetPb_;

    MvScmProfileData* profileData_;
    MvQScmDataWidget* dataPanel_;
    QListWidget* parListW_;
    bool ignoreParListW_;
    MvQProfileWidget* profilePanel_;

    QComboBox* stepCombo_;
    QPixmap editPix_;
    QPixmap noEditPix_;
    QAction* actionNext_;
    QAction* actionPrev_;
};

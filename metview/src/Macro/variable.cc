/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <iostream>

#include "macro.h"

Variable::Variable(const char* n, Value v) :
    Node(n),
    value(v),
    export_(0),
    import_(nullptr)
{
}

Variable::Variable(const char* n, Variable* v) :
    Node(n),
    export_(0),
    import_(v)
{
}

void Variable::SetValue(Value& v, int arity, Value* args)
{
    if (import_)
        import_->SetValue(v, arity, args);
    else {
        // If a[1,2] = b
        if (arity)
            value.SetSubValue(v, arity, args);
        else {
            value = v;
        }
    }
}

Value& Variable::GetValue()
{
    if (import_)
        return import_->GetValue();
    else {
        value.Sync();  // Force to synchronise
        return value;
    }
}

void Variable::Dump(int level)
{
    if (import_)
        import_->Dump(level);
    else {
        std::cout << Name() << " = ";
        value.Dump(level);
    }
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "macro.h"

List Context::DictionaryStack;
List Context::DictionaryCache;


class RemoteFunction : public Function
{
    char* service;
    request* args;

public:
    RemoteFunction(const char* n, const char* s, request* r);
    ~RemoteFunction();
    virtual Value Execute(int, Value* arg);
};

RemoteFunction::RemoteFunction(const char* n, const char* s, request* r) :
    Function(n)
{
    service = strcache(s);
    args = clone_all_requests(r);

    int m = count_values(r, "_types");
    auto* t = new vtype[m];
    for (int i = 0; i < m; i++)
        t[i] = (vtype)atoi(get_value(r, "_types", i));
    SetTypes(m, t);

    unset_value(args, "_types");
    unset_value(args, "_info");
    unset_value(args, "_reply");

    info = strcache(get_value(r, "_info", 0));
}

RemoteFunction::~RemoteFunction()
{
    strfree(service);
    //	strfree(info); //-- ~Function() should do this!?
    free_all_requests(args);
}


Value RemoteFunction::Execute(int arity, Value* arg)
{
    request* s = empty_request(Name());
    parameter* p;
    const char* q;
    int i;

    reqcpy(s, args);

    i = 0;
    p = args->params;

    while ((i < arity) && p) {
        if (*p->name != '_') {
            arg[i++].GetValue(q);
            set_value(s, p->name, "%s", q);
        }
        p = p->next;
    }


    Value v(service, s);

    free_all_requests(s);

    // For sync

    v.Sync();

    return v;
}

void Context::PopDictionary(void)
{
    DictionaryStack.Remove(DictionaryStack.Head());
}

void Context::PushDictionary(const char* nam)
{
    List* l = (List*)DictionaryCache.Find(nam);

    if (l == 0) {
        request* r = empty_request("_dictionary");
        Value v(nam, r);
        free_all_requests(r);

        v.Sync();
        v.GetValue(r);

        l = new List(nam);

        while (r) {
            l->Append(new RemoteFunction(r->name, nam, r));
            r = r->next;
        }

        DictionaryCache.Insert(l);
    }

    DictionaryStack.Insert(l);
}

Function* Context::FindInDictionary(const char* nam, bool& nameFound, int arity, Value* arg)
{
    List* l = (List*)DictionaryStack.Head();

    while (l) {
        Function* v = 0;

        while ((v = (Function*)l->Find(nam, v))) {
            nameFound = true;
            if (v->ValidArguments(arity, arg))
                return v;
        }

        l = (List*)l->Next();
    }
    return 0;
}

class PushDictFunction : public Function
{
public:
    PushDictFunction(const char* n) :
        Function(n) {}
    virtual Value Execute(int, Value* arg)
    {
        const char* p;
        arg[0].GetValue(p);
        Owner()->PushDictionary(p);
        return Value(0.0);
    }
};

class PopDictFunction : public Function
{
public:
    PopDictFunction(const char* n) :
        Function(n) {}
    virtual Value Execute(int, Value*)
    {
        Owner()->PopDictionary();
        return Value(0.0);
    }
};


static void install(Context* c)
{
    c->AddFunction(new PushDictFunction(".push.dict"));
    c->AddFunction(new PopDictFunction(".pop.dict"));
}

static Linkage Link(install);

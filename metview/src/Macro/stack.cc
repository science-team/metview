/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "macro.h"
#define STACK 1500

static Value stack[STACK + 1];
static int top = 0;

Value* Context::GetParameters(int count)
{
    if (top - count < 0) {
        Error("Stack is empty (GetParameters)");
        return &stack[0];
    }
    return &stack[top - count];
}

void Context::EmptyStack()
{
    for (int i = 0; i < top; i++)
        stack[i] = Value(0.0);
}

void Context::Push(Value v)
{
    if (top == STACK)
        Error("Stack is full - possibly too many arguments passed to function, only %d allowed", STACK);
    else
        stack[top++] = v;
}

Value Context::Pop()
{
    if (top == 0)
        return Error("Stack is empty (pop)");
    Value v = stack[--top];
    stack[top] = Value(0.0);
    return v;
}

class PStack : public Function
{
public:
    PStack(const char* n) :
        Function(n) {}
    virtual Value Execute(int, Value*)
    {
        for (int i = 0; i < top; i++)
            stack[i].Print();
        return Value(0.0);
    }
};

static void install(Context* c)
{
    c->AddFunction(new PStack("pstack"));
}

static Linkage Link(install);

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//-- pngjpeg.cc  -- Jun05/vk


#include "cpngjpeg.h"

//___________________________________________________________________________

CPngJpeg::CPngJpeg(const char* fileName, const std::string& kind) :
    Content(tany),
    fileName_(fileName),
    kind_(kind)
{
}

//___________________________________________________________________________

void CPngJpeg::ToRequest(request*& r)
{
    r = empty_request(kind_.c_str());
    add_value(r, "PATH", "%s", fileName_.c_str());
}

//___________________________________________________________________________

void CPngJpeg::Print()
{
    std::cout << kind_.c_str();
}

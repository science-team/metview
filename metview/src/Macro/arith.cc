/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "arith.h"


#include "macro.h"
#include <math.h>


double b_add(double a, double b)
{
    return a + b;
}
double b_sub(double a, double b)
{
    return a - b;
}
double b_div(double a, double b)
{
    return a / b;
}
double b_mul(double a, double b)
{
    return a * b;
}
double b_gt(double a, double b)
{
    return a > b;
}
double b_lt(double a, double b)
{
    return a < b;
}
double b_eq(double a, double b)
{
    return a == b;
}
double b_ne(double a, double b)
{
    return a != b;
}
double b_ge(double a, double b)
{
    return a >= b;
}
double b_le(double a, double b)
{
    return a <= b;
}
double b_and(double a, double b)
{
    return a && b;
}
double b_or(double a, double b)
{
    return a || b;
}
double b_mod(double a, double b)
{
    return long(a) % long(b);
}
double b_idiv(double a, double b)
{
    return long(long(a) / long(b));
}
double m_max(double a, double b)
{
    return a > b ? a : b;
}
double m_min(double a, double b)
{
    return a < b ? a : b;
}
double u_neg(double a)
{
    return -a;
}
double u_not(double a)
{
    return !a;
}
double u_ent(double a)
{
    return (long)a;
}
double u_sgn(double a)
{
    if (a > 0.0)
        return 1.0;
    if (a < 0.0)
        return -1.0;
    return 0;
}


binop BinOps[] = {
    {
        "+",
        b_add,
        "",
    },
    {
        "-",
        b_sub,
        "",
    },
    {
        "/",
        b_div,
        "",
    },
    {
        "*",
        b_mul,
        "",
    },
    {
        "^",
        pow,
        "",
    },
    {
        ">",
        b_gt,
        "",
    },
    {
        "<",
        b_lt,
        "",
    },
    {
        ">=",
        b_ge,
        "",
    },
    {
        "<=",
        b_le,
        "",
    },
    {
        "=",
        b_eq,
        "",
    },
    {
        "<>",
        b_ne,
        "",
    },
    {
        "and",
        b_and,
        "",
    },
    {
        "or",
        b_or,
        "",
    },
    {
        "mod",
        b_mod,
        "",
    },
    {
        "div",
        b_idiv,
        "",
    },
    {
        nullptr,
        nullptr,
        nullptr,
    },
};


mulop MulOps[] = {
    {
        "max",
        m_max,
        "",
    },
    {
        "min",
        m_min,
        "",
    },
    {
        "sum",
        b_add,
        "",
    },
    {
        nullptr,
        nullptr,
        nullptr,
    },
};


uniop UniOps[] = {
    {
        "neg",
        u_neg,
        "",
    },
    {
        "not",
        u_not,
        "",
    },
    {
        "sgn",
        u_sgn,
        "",
    },
    {
        "int",
        u_ent,
        "",
    },
    {
        "log10",
        log10,
        "",
    },
    {
        "log",
        log,
        "",
    },
    {
        "exp",
        exp,
        "",
    },
    {
        "sqrt",
        sqrt,
        "",
    },
    {
        "sin",
        sin,
        "",
    },
    {
        "cos",
        cos,
        "",
    },
    {
        "tan",
        tan,
        "",
    },
    {
        "asin",
        asin,
        "",
    },
    {
        "acos",
        acos,
        "",
    },
    {
        "atan",
        atan,
        "",
    },
    {
        "abs",
        fabs,
        "",
    },
    {
        nullptr,
        nullptr,
        nullptr,
    },
};

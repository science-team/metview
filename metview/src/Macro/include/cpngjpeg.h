/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//-- cpngjpeg.h  -- Jun05/vk
//
//  Minimal class to plot png and jpeg images (from a file) in Import View.
//
//  Tested: reading a png file and plotting it in Import View - OK!


#pragma once

#include <string>
#include "Metview.h"
#include "value.h"


class CPngJpeg : public Content
{
public:
    CPngJpeg(const char* fileName, const std::string& kind);

    virtual void ToRequest(request*&);
    virtual void Print();

private:
    std::string fileName_;
    std::string kind_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#define OP_NOP 0
#define OP_PUSH 1
#define OP_POP 2
#define OP_CALL 3
#define OP_GOTO 4
#define OP_TEST 5
#define OP_RETURN 6
#define OP_STORE 7
#define OP_PARAM 8
#define OP_CONVERT 9
#define OP_CONVERT_FOR 10
#define OP_CONVERT_LOOP 11
#define OP_COMMENT 12
#define OP_CONVERT_FN_START 13

#define PUSH_NUMBER 0
#define PUSH_IDENT 1
#define PUSH_STRING 2
#define PUSH_DATE 3
#define PUSH_TIME 4
#define PUSH_NIL 5
#define PUSH_NEGATIVE 6

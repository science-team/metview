/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/
#pragma once


typedef double (*uniproc)(double);
typedef double (*binproc)(double, double);

struct binop
{
    const char* symb;
    binproc proc;
    const char* info;
};
struct mulop
{
    const char* symb;
    binproc proc;
    const char* info;
};
struct uniop
{
    const char* symb;
    uniproc proc;
    const char* info;
};

extern binop BinOps[];
extern mulop MulOps[];
extern uniop UniOps[];


inline bool is_number_nan_d(double d)
{
    // one way to detect a NaN is to compare it with itself - it will be false
    return (d != d);
}

inline bool is_number_nan_f(float d)
{
    // one way to detect a NaN is to compare it with itself - it will be false
    return (d != d);
}

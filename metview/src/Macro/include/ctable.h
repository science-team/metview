/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Cached.h"

#include <MvTable.h>


class CTable : public InPool
{
    MvTable table_;

    request* r;
    virtual void ToRequest(request*&);
    virtual void ToTable(CTable*& x) { x = this; }
    virtual int Write(FILE*);

public:
    int Count(void) { return table_.numColumns(); }
    void load(void);
    MvTableColumn* column(int index) { return table_.column(index); }
    MvTableColumn* column(std::string& name) { return table_.column(name); }
    std::map<std::string, std::string> userMetaData() { return table_.userMetaData(); }


    bool constructedOk(void) { return ok_; }


    /*    void unload(void);
    void sort(){ gpts.sort(); }
    void removeDuplicates(){ gpts.removeDuplicates(); }
    void offset( double dlat, double dlon ){ gpts.offset( dlat, dlon ); }

    void SetSize(int n)        { gpts.count( n );    }
    long Count(void)           { return gpts.count();}
    long indexOfFirstValidPoint(void) {return gpts.indexOfFirstValidPoint();}
    MvGeoP1& operator[](long n){ return gpts[ n ];   }
    void SetFormat( eGeoFormat f ){ gpts.format( f );   }
    eGeoFormat Format(void)    { return gpts.format();  }

    std::string dbSystem()        { return gpts.dbSystem();}
    std::string dbPath()        { return gpts.dbPath();}
    std::string dbColumn(std::string c)  { return gpts.dbColumn(c);}
    std::string dbColumnAlias(std::string c)  { return gpts.dbColumnAlias(c);}
    const std::vector<std::string>&  dbQuery() { return gpts.dbQuery();}

    CGeopts(request  *r);
    CGeopts(long count);
    CGeopts(CGeopts *);
    CGeopts(fieldset*,int);
    CGeopts(CGeopts *,fieldset*,int, bool nearest=false);
    CGeopts(const char *name,int temp = 0);
    ~CGeopts();
*/

    CTable(request* r);
    CTable(const char* name, int temp = 0);
    ~CTable();

private:
    bool loaded_;
    bool ok_;  // used when the constructor detects a problem and the table is not loaded properly
};

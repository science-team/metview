/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <assert.h>
#include "Cached.h"
//#include "MvGeoPointSet.h"
#include "cbufr.h"


class CGeoptSet : public Content
{
    // MvGeoPointSet gptset;
    //  we use a vector of Value instead of CGeopts because it does
    //  the reference counting for us (shared pointer basically)
    std::vector<Value> vgpts;
    eGeoFormat format;

    request* r;
    // void ToRequest(request* &);
    // void ToGeoptSet(CGeoptSet*   &x) { x = this; }
    // int  Write(FILE*);

public:
    // void load(void);
    // void unload(void);
    // long Count(void)           { return gpts.count();}
    Value& operator[](unsigned long n)
    {
        assert(n - 1 >= 0 && n - 1 < vgpts.size());
        return vgpts[n - 1];
    }
    // eGeoFormat Format(void)    { return gpts.format();  }
    // const char *GetFileName();

    CGeoptSet();
    CGeoptSet(const CGeoptSet* src);
    CGeoptSet(const char* p, int temp = 0);
    void Print(void);
    int Write(FILE* f);
    void load(void);
    // CGeoptSet(CGeoptSet &ref);
    CGeoptSet(request* r);
    // CGeopts(long count);
    // CGeopts(CGeopts *);
    // CGeopts(fieldset*,int);
    // CGeopts(CGeopts *,fieldset*,int, bool nearest=false);
    // CGeopts(const char *name,int temp = 0);
    ~CGeoptSet();
    virtual void ToGeoptSet(CGeoptSet*& x) { x = this; }
    // void CopyContentsFrom(const CGeoptSet *src);

    unsigned long Count() { return vgpts.size(); }
    void Add(Value& g) { vgpts.push_back(g); }
};

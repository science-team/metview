/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "script.h"

// Compiler provides an interface to the lex/yacc parser (we have two: one for macro and
// another one for the macro to python converter). The default implementation
// is not doing anything. The concrete compiler is instantiated in the application and
// set on all the Script + derived objects.
class Compiler
{
public:
    Compiler() = default;
    virtual ~Compiler() = default;
    // script becomes the owner
    void setScript(Script* script) {script_=script;}
    virtual int compile(const char* /*filename*/)=0;
    static void compileError(char* msg, int lineNum);

protected:
    int compileCore(const char* filename, int* lineNo, FILE** fpIn);
    virtual void parseIt()=0;
    Script* script_{nullptr};
};

class ScriptCompiler : public Compiler
{
public:
    using Compiler::Compiler;
    int compile(const char* filename) override;
    void parseIt() override;
};

/***************************** LICENSE START ***********************************

 Copyright 2021 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//=============================================================================

#pragma once


#include "Cached.h"
#include "MvGeoPoints.h"


class CBufr : public InPool
{
    request* r;
    virtual void ToRequest(request*&);
    virtual int Write(FILE*);
    virtual void ToBufr(CBufr*& x) { x = this; }

public:
    CBufr(request* r);
    CBufr(const char* name, int temp = 0);
    ~CBufr();

    const char* GetFileName();
};


class CGeopts : public InPool
{
    MvGeoPoints gpts_;

    request* r;
    virtual void ToRequest(request*&);
    virtual void ToGeopts(CGeopts*& x) { x = this; }
    virtual int Write(FILE*);
    void SetSubValue(Value& v, int arity, Value* arg);

public:
    MvGeoPoints& gpts() { return gpts_; }

    void load(void);
    void unload(bool doFree = false);
    void sort() { gpts_.sort(); }
    void removeDuplicates() { gpts_.removeDuplicates(); }
    void offset(double dlat, double dlon) { gpts_.offset(dlat, dlon); }

    void SetSize(size_t n) { gpts_.set_count(n); }
    size_t Count(void) { return gpts_.count(); }
    const char* GetFileName();
    std::string SetColumnValues(eGeoColType col, Value& newVals, bool isList, bool isVector, bool valueIndexProvided, Value* valueIndex);

    bool doesMetadataMatch(const request* r);

    bool isCompatibleForMerging(const CGeopts& in) { return gpts_.isCompatibleForMerging(in.gpts_); }

    CGeopts(request* r);
    CGeopts(long count, int nvals, eGeoFormat efmt, bool init);
    CGeopts(long count, const MvGeoPointColumnInfo& colInfo, eGeoFormat efmt, bool init);
    CGeopts(CGeopts*);
    CGeopts(CGeopts*, fieldset*, int, bool nearest, bool nearestValid, bool storeLocs);
    CGeopts(const char* name, int temp = 0);
    CGeopts(MvGeoPoints& gpts_In);
    ~CGeopts();
};

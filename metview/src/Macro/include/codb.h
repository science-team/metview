/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "Cached.h"
#include "Macro/include/value.h"

class MvAbstractOdb;

class COdb : public InPool
{
    MvAbstractOdb* odb_;

    request* r;
    virtual void ToRequest(request*&);
    virtual void ToOdb(COdb*& x) { x = this; }
    virtual int Write(FILE*);

public:
    void load();
    void unload();
    // void sort(){ gpts.sort(); }
    // void removeDuplicates(){ gpts.removeDuplicates(); }
    // void offset( double dlat, double dlon ){ gpts.offset( dlat, dlon ); }

    MvAbstractOdb* odb() { return odb_; }

    long Count();

    // string dbSystem() 	   { return gpts.dbSystem();}
    // string dbPath() 	   { return gpts.dbPath();}
    // string dbColumn(string c)  { return gpts.dbColumn(c);}
    // string dbColumnAlias(string c)  { return gpts.dbColumnAlias(c);}
    // const std::vector<std::string>&  dbQuery() { return gpts.dbQuery();}

    COdb(request* r);
    // COdb(long count);
    // COdb(CGeopts *);
    // COdb(fieldset*,int);
    // COdb(CGeopts *,fieldset*,int, bool nearest=false);
    COdb(const char* name, int temp = 0);
    ~COdb();
};

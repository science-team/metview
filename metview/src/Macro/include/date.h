/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvDate.h"

class Date
{
    static const char* stringFormat;
    static const char* numberFormat;

    long julian;
    long second;

    int compare(Date& d)
    {
        return julian != d.julian ? julian - d.julian : second - d.second;
    }

public:
    Date(double n = 0);
    Date(long d, long t);
    Date(const Date& d)
    {
        julian = d.julian;
        second = d.second;
    }
    Date(const MvDate& d)
    {
        julian = d._julian();
        second = d._second();
    }
    Date(const char*);

    long _julian() { return julian; }
    long _second() { return second; }

    Date operator-(double&);
    Date operator+(double&);
    double operator-(Date&);
    int operator>(Date& d) { return compare(d) > 0; }
    int operator<(Date& d) { return compare(d) < 0; }
    int operator>=(Date& d) { return compare(d) >= 0; }
    int operator<=(Date& d) { return compare(d) <= 0; }
    int operator==(Date& d) { return compare(d) == 0; }
    int operator!=(Date& d) { return compare(d) != 0; }

    void Print(void);
    int YyMmDd(void) { return mars_julian_to_date(julian, false); }
    int YyyyMmDd(void) { return mars_julian_to_date(julian, true); }

    int Year(void) { return YyyyMmDd() / 10000; }
    int Month(void) { return (YyyyMmDd() / 100) % 100; }
    int Day(void) { return YyyyMmDd() % 100; }
    int Hour(void) { return second / 3600; }
    int Minute(void) { return (second / 60) % 60; }
    int Second(void) { return second % 60; }
    int Julian(void);
    int DayOfWeek(void) { return julian % 7 + 1; }
    void Format(const char*, char*);

    static const char* StringFormat();
    static const char* NumberFormat();
    static const char* MonthName(int, boolean);
    static const char* DayName(int, boolean);
};

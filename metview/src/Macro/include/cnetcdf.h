/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

//-*-c++-*-
#pragma once
#include "MvNetCDF.h"
class request;
class CList;
class MvRequest;
class CNetCDF : public InPool
{
public:
    CNetCDF(request* r);
    CNetCDF(const char* name, int temp = 0, const char mode = 'r');
    ~CNetCDF();
    const char* GetFileName();

    void load(void);
    void unload(void);

    CList* Variables();
    MvRequest Attributes(bool global);

    void Current(int xx) { current_ = xx; }
    bool Current(const char* varName);
    int Current() { return current_; }

    MvNcVar* GetVar()
    {
        load();
        return netCDF_->getVariable(current_ - 1);
    }
    MvNcVar* GetGlobalVar()
    {
        load();
        return netCDF_->getVariable(-1);
    }  // '-1' means 'get me the global variable'
    void Flush(Value* = 0) { netCDF_->sync(); }

private:
    request* r_;
    MvNetCDF* netCDF_;
    int current_;
    char mode_;

    virtual void ToRequest(request*&);
    virtual int Write(FILE*);
    virtual void ToNetCDF(CNetCDF*& x) { x = this; }
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <iostream>

class List;

class Node;
class OpPop;
class OpPush;
class OpStore;
class OpParam;
class OpCall;
class OpTest;
class OpReturn;
class UserFunction;
class ExternFunction;
class OpConvert;
class OpConvertFor;
class OpConvertLoop;
class OpComment;
class OpConvertFnStart;

class NodeVisitor
{
public:
    virtual void visit(Node*) {}
    virtual void visit(OpPop*) {}
    virtual void visit(OpPush*) {}
    virtual void visit(OpStore*) {}
    virtual void visit(OpParam*) {}
    virtual void visit(OpCall*) {}
    virtual void visit(OpTest*) {}
    virtual void visit(OpReturn*) {}
    virtual void visit(UserFunction*) {}
    virtual void visit(ExternFunction*) {}
    virtual void visit(OpConvert*) {}
    virtual void visit(OpConvertFor*) {}
    virtual void visit(OpConvertLoop*) {}
    virtual void visit(OpComment*) {}
    virtual void visit(OpConvertFnStart*) {}
};

class Node
{
    Node* owner;
    char* name;
    Node* next;

public:
    Node(const char* n = nullptr)
    {
        owner = nullptr;
        name = strcache(n);
    }
    virtual ~Node() { strfree(name); }

    virtual void accept(NodeVisitor *v) {v->visit(this);}

    Node* Next() { return next; }
    Node* Owner() { return owner; }
    const char* Name() { return name; }
    void Rename(const char* n)
    {
        strfree(name);
        name = strcache(n);
    }

    const char* FullName(char* buf)
    {
        *buf = 0;
        if (owner) {
            char b[1024];
            strcat(buf, owner->FullName(b));
            strcat(buf, ".");
        }
        strcat(buf, name);
        return buf;
    }

    void SetOwner(Node* s)
    {
        owner = s;
    }

    virtual void Print(void)
    {
        std::cout << "Address: " << this << std::endl;
        if (name != nullptr) {
            std::cout << "Name: " << name << std::endl;
        } else {
            std::cout << "Name: NULL " << std::endl;
        }
    }

    virtual Value ToString(void)
    {
        return Value();
    }

    friend class List;

    void* operator new(size_t s) { return fast_new(s, transient_mem); }
    void operator delete(void* p) { fast_delete(p, transient_mem); }
};

class List : public Node
{
    Node* head;
    Node* tail;

public:
    List(const char* n = nullptr) :
        Node(n) { head = tail = nullptr; }
    void Append(Node* o)
    {
        o->next = nullptr;
        if (tail)
            tail->next = o;
        else
            head = o;
        tail = o;
    }

    void Insert(Node* o)
    {
        o->next = head;
        head = o;
        if (tail == nullptr)
            tail = o;
    }

    void Remove(Node* o)
    {
        Node *p = head, *q = nullptr;
        while (p) {
            if (p == o) {
                if (q)
                    q->next = p->next;
                else
                    head = p->next;
                if (tail == p)
                    tail = q;
            }
            q = p;
            p = p->next;
        }
    }

    Node* Find(const char* n, Node* from = nullptr)
    {
        Node* o = from ? from->next : head;
        while (o) {
            if (n == o->Name())
                return o;
            o = o->next;
        }
        return nullptr;
    }
    void Print(void) override
    {
        Node* o = head;
        while (o) {
            o->Print();
            o = o->next;
        }
    }
    Node* Head() { return head; }
    void Empty(void)
    {
        Node* o = head;
        while (o) {
            Node* p = o->next;
            delete o;
            o = p;
        }
        head = tail = nullptr;
    }

    void Save(List& l)
    {
        l.head = head;
        l.tail = tail;
        head = tail = nullptr;
    }
    void Restore(List& l)
    {
        head = l.head;
        tail = l.tail;
        l.head = l.tail = nullptr;
    }
};

class Cache : public List
{
    class CachedNode : public Node
    {
    public:
        void* data;
        CachedNode(const char* n, void* d) :
            Node(n) { data = d; }
    };

public:
    void* Find(const char* n)
    {
        CachedNode* c = (CachedNode*)List::Find(n);
        return c ? c->data : nullptr;
    }
    void Set(const char* n, void* d)
    {
        CachedNode* c;
        if ((c = (CachedNode*)List::Find(n)))
            Remove(c);
        Append(new CachedNode(n, d));
    }
};

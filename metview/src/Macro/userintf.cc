/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "macro.h"

//=============================================================================

class UiFunction : public Function
{
public:
    UiFunction(char* n) :
        Function(n, 1, trequest)
    {
        info = "Invoke the user interface to open a dialog";
    };
    virtual Value Execute(int arity, Value* arg);
};

Value UiFunction::Execute(int, Value* arg)
{
    request* e = empty_request("EDIT");
    request* r;

    arg[0].GetValue(r);

    e->next = clone_all_requests(r);

    // Get desktop app name
    std::string desktopName;
    char* desktop = getenv("MV_DESKTOP_NAME");
    if (!desktop) {
        std::cout << "Error: MV_DESKTOP_NAME is not defined! Macro exits!" << std::endl;
    }
    else {
        desktopName = std::string(desktop);
    }
    Value v = Value(desktopName.c_str(), e);
    free_all_requests(e);
    return v;
}

//=============================================================================

class DialogFunction : public Function
{
public:
    DialogFunction(char* n) :
        Function(n) { info = "Define a dialog"; };
    virtual Value Execute(int arity, Value* arg);
    virtual int Validate(int arity, Value* arg);
};

int DialogFunction::Validate(int, Value*)
{
    return 1;
}

Value DialogFunction::Execute(int arity, Value* arg)
{
    return Value(0.0);
}

//=============================================================================
static void install(Context* c)
{
    c->AddFunction(new UiFunction("prompt"));
}

static Linkage Link(install);

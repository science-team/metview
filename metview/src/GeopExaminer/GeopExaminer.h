/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>

#include <QMainWindow>
#include <QModelIndex>

#include "MvQMainWindow.h"
#include "MvQMenuItem.h"

class QSplitter;
class QSortFilterProxyModel;

class MvGeoPoints;
class MvQArrowSpinWidget;
class MvQFileInfoLabel;
class MvQGeoPointsDataModel;
class MvQTreeView;
class MvQTreeViewSearchLine;
class MvQLogPanel;


class GeopExaminer : public MvQMainWindow
{
    Q_OBJECT

public:
    GeopExaminer(QWidget* parent = nullptr);
    ~GeopExaminer() override;
    bool initMetaData(const std::string&);
    void updateFileInfoLabel();

public slots:
    void slotShowAboutBox();
    void slotDataRowSpinChanged(int);
    void slotSelectDataRow(const QModelIndex&);

private:
    void setupEditActions();
    void setupViewActions();
    void setupHelpActions();
    void setupDataBox();
    void setupFindBox();
    void updateMetadataLabel();
    void readSettings();
    void writeSettings();

    MvGeoPoints* data_{nullptr};
    MvQGeoPointsDataModel* gpModel_{nullptr};
    QSortFilterProxyModel* gpSortModel_{nullptr};
    MvQMainWindow::MenuItemMap menuItems_;
    MvQFileInfoLabel* fileInfoLabel_{nullptr};
    QLabel* metadataLabel_{nullptr};

    QAction* actionFileInfo_{nullptr};
    QSplitter* mainSplitter_{nullptr};
    MvQTreeView* treeData_{nullptr};
    MvQLogPanel* logPanel_{nullptr};
    QAction* actionLog_{nullptr};
    QLabel* statusMessageLabel_{nullptr};
    bool ignoreDataRowSpinChangeSignal_{false};
    MvQArrowSpinWidget* dataRowSpin_{nullptr};
    QWidget* dataPanel_{nullptr};
};

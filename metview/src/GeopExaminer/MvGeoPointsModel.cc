/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QDebug>

#include "MvGeoPointsModel.h"
#include "MvGeoPoints.h"


MvQGeoPointsDataModel::MvQGeoPointsDataModel(QObject* parent) :
    QAbstractItemModel(parent)
{
}

void MvQGeoPointsDataModel::dataIsAboutToChange()
{
    beginResetModel();
}

void MvQGeoPointsDataModel::setBaseData(MvGeoPoints* data)
{
    data_ = data;

    // Reset the model (views will be notified)
    endResetModel();
}

int MvQGeoPointsDataModel::columnCount(const QModelIndex&) const
{
    if (data_ != nullptr)
        return static_cast<int>(data_->totalcols() + 1);

    return 0;
}

int MvQGeoPointsDataModel::rowCount(const QModelIndex& parent) const
{
    // Non-root
    if (parent.isValid())
        return 0;

    // Root
    if (data_ != nullptr)
        return static_cast<int>(data_->count());

    return 0;
}

QVariant MvQGeoPointsDataModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid() || (role != Qt::DisplayRole && role != Qt::UserRole))
        return {};

    if (index.column() < 0 || index.column() > (int)(data_->totalcols() + 1))
        return {};

    if (index.column() == 0)
        return index.row() + 1;


    // Get a specific row and column element
    if (role == Qt::DisplayRole) {
        // return a nice string to display
        int type = 0;
        return QString::fromStdString(data_->value(index.row(), index.column() - 1, type));
    }
    else if (role == Qt::UserRole) {
        // this role is used for sorting, so return a number (so that [100, 20] is correctly sorted)
        int type = 0;
        QString val = QString::fromStdString(data_->value(index.row(), index.column() - 1, type));
        if (type == eGeoVDouble)
            return val.toDouble();
        else if (type == eGeoVLong)
            return val.toInt();
        else
            return val;
        return val;
    }
    else
        return {};
}

QVariant MvQGeoPointsDataModel::headerData(const int section, const Qt::Orientation orient, const int role) const
{
    if (orient != Qt::Horizontal || role != Qt::DisplayRole)
        return QAbstractItemModel::headerData(section, orient, role);

    if (section < 0 || !data_ || (data_ && section >= (int)(data_->totalcols() + 1)))
        return {};

    if (section == 0)
        return QString(tr("Index"));

    // Get an specific column name
    return QString::fromStdString(data_->colName(section - 1));
}

QModelIndex MvQGeoPointsDataModel::index(int row, int column, const QModelIndex&) const
{
    if (data_ == nullptr || data_->count() <= 0 || column < 0)
        return {};

    return createIndex(row, column, (void*)nullptr);
}


QModelIndex MvQGeoPointsDataModel::parent(const QModelIndex&) const
{
    return {};
}

/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <string>

#include "GeopExaminer.h"
#include "Metview.h"
#include "MvQApplication.h"


int main(int argc, char** argv)
{
    if (argc < 2) {
        marslog(LOG_EROR, "No arguments are specified");
        exit(1);
    }

    MvRequest in;
    in.read(argv[1], false, true);

    marslog(LOG_INFO, "Request:");
    in.print();

    if (!in) {
        marslog(LOG_EROR, "No request could be read from request file=%s", argv[1]);
        exit(1);
    }

    // Get geopints file name
    std::string fgpt;
    if (const char* tmpc = in("PATH")) {
        fgpt = std::string(tmpc);
    }
    else {
        marslog(LOG_EROR, "No PATH is specified!");
        exit(1);
    }

    // Create the qt application. The appname must be unique!
    std::string appName = MvApplication::buildAppName("GeopointsExaminer");
    MvQApplication app(argc, argv, appName.c_str(), {"examiner", "window", "find"});

    // Create the geopoints browser, initialize it and show it
    auto* browser = new GeopExaminer;
    browser->setAppIcon("GEOPOINTS");
    browser->initMetaData(fgpt);
    browser->show();

    // Enter the app loop
    app.exec();
}

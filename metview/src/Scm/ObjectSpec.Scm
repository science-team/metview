################################################################################
#
#	ObjectSpec.Flextra
#

#-----------------------
# Input Data
#-----------------------

object,
	class		= SCM_INPUT_DATA,
	can_be_created  = False,
	check           = False,
	default_object  = False,
	type		= File,
	default_name    = 'Scm Input Data',
	pixmap 		= '$METVIEW_DIR_SHARE/icons/SCM_INPUT_DATA.icon',
	editor_type     = QtScmDataEditor 

#-----------------------
# Output Data
#-----------------------

object,
	class		= SCM_OUTPUT_DATA,
	can_be_created  = False,
	check           = False,
	default_object  = False,
	type		= File,
	default_name    = 'Scm Output Data',
	pixmap 		= '$METVIEW_DIR_SHARE/icons/SCM_INPUT_DATA.icon',
	default_method	= Examine,
	editor_type     = NoEditor 



#-----------------------
# Computation
#-----------------------

object,
	class               = SCM_RUN,
 	can_be_created      = True,
 	definition_file     = '$METVIEW_DIR_SHARE/etc/ScmRunDef',
	rules_file          = '$METVIEW_DIR_SHARE/etc/ScmRunRules',
    default_name        = 'Scm Run',
    help_page           = 'Scm Run',
    type                = Data,
    icon_box            = 'Data processing',
    expand	            = 75,
	macro               = scm_run,
    editor_type         = SimpleEditor,
	pixmap              = '$METVIEW_DIR_SHARE/icons/SCM_RUN.icon'


#-----------------------
# Visualisation
#-----------------------

object,
	class           = SCM_VISUALISER,
 	can_be_created  = True,
 	definition_file = '$METVIEW_DIR_SHARE/etc/ScmVisualiserDef',
	rules_file      = '$METVIEW_DIR_SHARE/etc/ScmVisualiserRules',
	default_name    = 'Scm Visualiser',		
    help_page       = 'Scm Visualiser',
    type            = Data,
    icon_box        = 'Visualisers',
    expand	        = 27,	 # EXPAND_DATE|EXPAND_TIME|expand_2nd_name|EXPAND_LAST_NAME
	macro           = scm_visualiser,
	editor_type	    = SimpleEditor,	
	pixmap          = '$METVIEW_DIR_SHARE/icons/SCM_VIS.icon'


#-----------------------
# States + services
#-----------------------

state,
	class    = SCM_RUN,
	action   = execute,
	service  = ScmRun

state,
	class		 = SCM_RUN,
	output_class = SCM_OUTPUT_DATA,
	service		 = ScmRun


service,
	cmd  = '$METVIEW_CMD $METVIEW_BIN/ScmRun',
	name = 'ScmRun'

state,
	class		    = SCM_OUTPUT_DATA,
	action		    = save,
        service		    = savepool

state,
	class	= SCM_OUTPUT_DATA,
	action	= examine,
    service = UiAppManager

state,
     class   = SCM_VISUALISER,
     action  = execute/visualise/prepare/drop,
     service = ScmVisualiser

service,
	cmd  = '$METVIEW_CMD $METVIEW_BIN/ScmVisualiser',
	name = 'ScmVisualiser'

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MessageControlPanel.h"

#include <QAction>
#include <QHBoxLayout>
#include <QLabel>
#include <QMenu>
#include <QPainter>
#include <QPushButton>
#include <QSpinBox>
#include <QStyleOption>
#include <QToolButton>

#include "MvQArrowSpinWidget.h"
#include "MvMiscellaneous.h"
#include "MvQTheme.h"

MessageControlPanel::MessageControlPanel(bool hasSubset, bool /*hasFilter*/, QWidget* parent) :
    MvQPanel(parent)
{
    QString styleId = "2";

    layout_ = new QHBoxLayout(this);
    layout_->setContentsMargins(4, 2, 4, 2);

    auto* messageTitleLab = new QLabel(tr("&Message:"), this);
    messageTitleLab->setProperty("panelStyle", styleId);
    messageTitleLab->setObjectName("messageTitleLabel");
    messageSpin_ = new MvQArrowSpinWidget(this);
    messageTitleLab->setBuddy(messageSpin_->spin());

    connect(messageSpin_, SIGNAL(valueChanged(int)),
            this, SIGNAL(messageChanged(int)));

    if (hasSubset) {
        subsetTitleLabel_ = new QLabel(tr("&Subset:"), this);
        subsetTitleLabel_->setProperty("panelStyle", styleId);
        subsetTitleLabel_->setObjectName("subsetTitleLabel");
        subsetSpin_ = new MvQArrowSpinWidget(this);
        subsetLabel_ = new QLabel("");
        subsetLabel_->setProperty("panelStyle", styleId);
        subsetTitleLabel_->setBuddy(subsetSpin_->spin());

        connect(subsetSpin_, SIGNAL(valueChanged(int)),
                this, SIGNAL(subsetChanged(int)));
    }

    layout_->addWidget(messageTitleLab);
    layout_->addWidget(messageSpin_);
    if (hasSubset) {
        layout_->addSpacing(10);
        layout_->addWidget(subsetTitleLabel_);
        layout_->addWidget(subsetSpin_);
        layout_->addWidget(subsetLabel_);
    }

    totalLabel_ = new QLabel(this);
    totalLabel_->setProperty("panelStyle", styleId);
    layout_->addSpacing(16);
    layout_->addWidget(totalLabel_);

    layout_->addStretch(1);

    // Options button with menu
    optionsTb_ = new QToolButton(this);
    optionsTb_->setProperty("panelStyle", styleId);
    optionsTb_->setIcon(QPixmap(":/examiner/cogwheel.svg"));
    optionsTb_->setPopupMode(QToolButton::InstantPopup);
    optionsTb_->setToolTip(tr("Options"));

    auto* menu = new QMenu(this);
    menu->setTearOffEnabled(true);
    optionsTb_->setMenu(menu);

    layout_->addWidget(optionsTb_);
}


void MessageControlPanel::setMessageValue(int num, bool /*b*/)
{
    messageSpin_->setValue(num, false);
}

void MessageControlPanel::resetMessageNum(int num, bool /*b*/)
{
    messageSpin_->reset(num, false);

    QString s = "(total number of messages: <b>" + QString::number(num) + "</b>)";
    totalLabel_->setText(s);
}

void MessageControlPanel::resetSubsetNum(int num, bool)
{
    Q_ASSERT(subsetSpin_);
    subsetSpin_->reset(num, false);
}

void MessageControlPanel::setCurrentSubset(int cnt)
{
    if (cnt >= 1 && subsetSpin_->value() != cnt) {
        subsetSpin_->setValue(cnt, true);
    }
}

void MessageControlPanel::setSubsetLabel(QString txt)
{
    Q_ASSERT(subsetLabel_);
    subsetLabel_->setText(txt);
}

void MessageControlPanel::setSubsetTotalLabel(int msgNum, int subsetNum)
{
    QString s = "(total number of messages: <b>" +
                QString::number(msgNum) +
                "</b>, subsets: <b>" + QString::number(subsetNum) + "</b>)";
    totalLabel_->setText(s);
}

void MessageControlPanel::showSubsetControl(bool b)
{
    Q_ASSERT(subsetLabel_);
    subsetSpin_->setVisible(b);
    subsetTitleLabel_->setVisible(b);
}

int MessageControlPanel::messageValue() const
{
    return messageSpin_->value();
}

int MessageControlPanel::subsetValue() const
{
    return subsetSpin_->value();
}

void MessageControlPanel::setFilterInfo(bool filtered, int oriNum, int num)
{
    QString s;
    if (!filtered) {
        s = "(total number of messages: <b>" + QString::number(num) + "</b>)";
    }
    else {
        s = "(total number of messages: <b>" + QString::number(num) +
            "</b>, \
          <font color=\'" +
            MvQTheme::colour("messagecontrol", "filtered_text").name() + "\'><b>filtered </b></font> out of " +
            QString::number(oriNum) + " )";
    }

    totalLabel_->setText(s);
}

void MessageControlPanel::addOptionsAction(QAction* ac)
{
    Q_ASSERT(ac);
    optionsActions_ << ac;
    QMenu* menu = optionsTb_->menu();
    Q_ASSERT(menu);
    menu->addAction(ac);
}

void MessageControlPanel::addButtonAction(QAction* ac)
{
    buttonActions_ << ac;
    Q_ASSERT(ac);
    auto* tb = new QToolButton(this);
    tb->setDefaultAction(ac);
    tb->setProperty("panelStyle", 2);
    layout_->insertWidget(layout_->count() - 1, tb);
}

/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QtGlobal>
#include <QWidget>
//#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
//#include <QtCore5Compat/QRegExp>
//#endif

class QComboBox;
class QLineEdit;

#if QT_VERSION >= QT_VERSION_CHECK(5, 1, 0)
#include <QRegularExpression>
class QRegularExpressionValidator;
#else
#include <QRegExp>
class QRegExpValidator;
#endif


class MvQComboLine : public QWidget
{
    Q_OBJECT

public:
    MvQComboLine(QStringList, QWidget* parent = 0);
    void setItems(QStringList);
#if QT_VERSION >= QT_VERSION_CHECK(5, 1, 0)
    void setRegExps(QList<QRegularExpression>);
#else
    void setRegExps(QList<QRegExp>);
#endif
    void setToolTips(QStringList);
    void setCurrentIndex(int);

signals:
    void currentIndexChanged(int);
    void textChanged(QString);

protected slots:
    void slotCurrentIndexChanged(int);

protected:
    // void resizeEvent(QResizeEvent*);
    QComboBox* combo_;
    QLineEdit* line_;
#if QT_VERSION >= QT_VERSION_CHECK(5, 1, 0)
    QRegularExpressionValidator* validator_;
    QList<QRegularExpression> regExps_;
#else
    QRegExpValidator* validator_;
    QList<QRegExp> regExps_;
#endif
    QStringList toolTips_;
};

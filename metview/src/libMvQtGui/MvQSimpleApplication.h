/***************************** LICENSE START ***********************************

 Copyright 2022 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvFwd.h"

#include <QApplication>
#include <QStringList>


#include "MvAbstractApplication.h"

class MvQSimpleApplication : public QApplication, public MvAbstractApplication
{
public:
    MvQSimpleApplication(int& ac, char** av, const char* name = nullptr, QStringList resorces = {});

    bool notify(QObject* receiver, QEvent* event) override;

protected:
    void writeToLog(const std::string& msg, MvLogLevel level) override;
    void writeToUiLog(const std::string& msg, const std::string& details, MvLogLevel level, bool popup) override;
    void exitWithError() override;
};

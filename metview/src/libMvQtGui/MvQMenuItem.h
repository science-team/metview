/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QMap>
#include <QList>
#include <QString>

class QObject;

class MvQMenuItem
{
public:
    enum Target
    {
        NoTarget = 0x0,
        MenuTarget = 0x1,
        ToolBarTarget = 0x2,
        SubMenuTarget = 0x4
    };
    Q_DECLARE_FLAGS(Targets, Target);

    MvQMenuItem(QObject* o, Targets t = Targets(MenuTarget | ToolBarTarget)) :
        object_(o),
        targets_(t) {}
    QObject* object() { return object_; }
    Targets targets() { return targets_; }

private:
    QObject* object_;
    Targets targets_;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(MvQMenuItem::Targets);

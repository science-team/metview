/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "StatusProgBarHandler.h"

#include <QDebug>
#include <QProgressBar>

StatusProgBarHandler* StatusProgBarHandler::instance_ = nullptr;

StatusProgBarHandler* StatusProgBarHandler::instance()
{
    if (!instance_) {
        instance_ = new StatusProgBarHandler();
    }

    return instance_;
}

void StatusProgBarHandler::init(QProgressBar* progBar)
{
    progBar_ = progBar;
}

void StatusProgBarHandler::progress(int val)
{
    progBar_->setValue(val);
}

void StatusProgBarHandler::start()
{
    progBar_->setVisible(true);
    progBar_->reset();
}

void StatusProgBarHandler::finish()
{
    progBar_->setVisible(false);
}

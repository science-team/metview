/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "ui_CalendarDialog.h"

#include <QDate>
#include <QDialog>

class CalendarDialog : public QDialog, protected Ui::CalendarDialog
{
    Q_OBJECT

public:
    CalendarDialog(QWidget* parent = nullptr);
    void setSelectedDate(QDate);

Q_SIGNALS:
    void dateSelected(QDate);
};

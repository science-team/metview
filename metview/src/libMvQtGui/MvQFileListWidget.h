/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QWidget>

class QFileSystemModel;
class QSplitter;
class QListView;
class QListWidget;
class QTreeView;
class MvQFileList;
class MvQFileListModel;
class MvQFsFilterModel;

class MvQFileListWidget : public QWidget
{
    Q_OBJECT

public:
    MvQFileListWidget(QWidget* parent = nullptr);
    void setFileList(MvQFileList*);
    void selectFile(QString, bool broadcast = true);

protected slots:
    void slotFileSelect(const QModelIndex& idx);

signals:
    void fileSelected(QString);
    void closePanel(bool);
    void clearPanel(bool);

private:
    QSplitter* splitter_{nullptr};
    MvQFileList* fileLst_{nullptr};
    MvQFileListModel* lstModel_{nullptr};
    QListView* lstView_{nullptr};
    QFileSystemModel* fsModel_{nullptr};
    MvQFsFilterModel* fsFilterModel_{nullptr};
    QTreeView* fsView_{nullptr};
};

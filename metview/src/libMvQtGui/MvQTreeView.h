/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QPen>
#include <QBrush>
#include <QPoint>
#include <QTreeView>
#include <QStyledItemDelegate>


class MvQTreeViewDelegate : public QStyledItemDelegate
{
    friend class MvQTreeView;

public:
    explicit MvQTreeViewDelegate(QTreeView* parent);
    void setStyleId(int id);
    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const override;
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const override;

private:
    void setColours();

    QPen textPen_;
    QPen subTextPen_;
    QPen selectTextPen_;
    QPen selectSubTextPen_;
    QBrush selectBrush_;
    QPen errorSelectPen_;
    QBrush errorSelectBrush_;
    QBrush errorBrush_;
    QBrush blockBrush_;
    QPen blockTextPen_;
    QBrush selectBrushBlock_;
    QPen borderPen_;
    QTreeView* view_;
};

class MvQTreeView : public QTreeView
{
    Q_OBJECT

public:
    MvQTreeView(QWidget* parent = nullptr, bool useDelegate = false);
    void setStyleId(int);
    void keyboardSearch(const QString&) override;
    void setActvatedByKeyNavigation(bool b) { activatedByKeyNavigation_ = b; }
    void setColumnToDrag(int i) { columnToDrag_ = i; }

signals:
    void selectionChanged(const QModelIndex&);

protected:
    void keyReleaseEvent(QKeyEvent*) override;
    void selectionChanged(const QItemSelection& selected, const QItemSelection& deselected) override;
    void drawBranches(QPainter* painter, const QRect& rect, const QModelIndex& index) const override;

    bool dragEnabled();
    bool dropEnabled();

    MvQTreeViewDelegate* delegate_{nullptr};
    bool activatedByKeyNavigation_;
    int columnToDrag_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

class QWebView;

#include "AbstractSearchLine.h"

class MvQWebViewSearchLine : public AbstractSearchLine
{
    Q_OBJECT

public:
    MvQWebViewSearchLine(QWebView*, QString);
    ~MvQWebViewSearchLine();

public slots:
    void slotFind(QString);
    void slotFindNext();
    void slotFindPrev();

protected:
    QWebView* view_;
    QString txt_;
};

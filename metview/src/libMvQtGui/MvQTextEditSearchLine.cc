/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QColor>
#include <QDebug>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTextEdit>

#include "MvQTextEditSearchLine.h"


MvQTextEditSearchLine::MvQTextEditSearchLine(QTextEdit *edit,QString searchLabelText) :
    AbstractSearchLinearchLabelText), editor_(edit)
{
}

MvQTextEditSearchLine::~MvQTextEditSearchLine()
{
}

void MvQTextEditSearchLine::slotFind(QString txt)
{
    QTextCursor cursor(editor_->textCursor());
    cursor.movePosition(QTextCursor::StartOfWord);
    editor_->setTextCursor(cursor);

    if (editor_->find(txt) == false) {
        cursor = editor_->textCursor();
        cursor.movePosition(QTextCursor::Start);
        editor_->setTextCursor(cursor);
        updateButtons(editor_->find(txt));
    }
    else {
        updateButtons(true);
    }

    // QTextDocument::FindFlags flags;

    /*if(checkBox_backward->checkState() == Qt::Checked)
    {
        flags = QTextDocument::FindBackward;
    }
    if(checkBox_case->checkState() == Qt::Checked)
    {
        flags = flags | QTextDocument::FindCaseSensitively;
    }
    if(checkBox_word->checkState() == Qt::Checked)
    {
        flags = flags | QTextDocument::FindWholeWords;
    }*/

    /*if(editor_->find(txt))
        {
            //statusMessage("", 0);
        }
        else
        {
        if (flags & QTextDocument::FindBackward)
            statusMessage(tr("Reached top of file"),    5000);
        else
            statusMessage(tr("Reached bottom of file"), 5000);
    }*/
}

void MvQTextEditSearchLine::slotFindNext()
{
    if (status_ == true) {
        if (editor_->find(searchLine_->text()) == false) {
            QTextCursor cursor(editor_->textCursor());
            cursor.movePosition(QTextCursor::Start);
            editor_->setTextCursor(cursor);
            editor_->find(searchLine_->text());
        }
    }
}

void MvQTextEditSearchLine::slotFindPrev()
{
    QTextDocument::FindFlags flags = QTextDocument::FindBackward;

    if (status_ == true) {
        if (editor_->find(searchLine_->text(), flags) == false) {
            QTextCursor cursor(editor_->textCursor());
            cursor.movePosition(QTextCursor::End);
            editor_->setTextCursor(cursor);
            editor_->find(searchLine_->text(), flags);
        }
    }
}

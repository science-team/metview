/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QList>
#include <QObject>
#include <QString>

class QAction;
class QTabWidget;
class QWidget;

class MvQTabManager : public QObject
{
    Q_OBJECT

public:
    MvQTabManager(QTabWidget*);
    void addItem(QWidget*, QString, QAction* action = 0);
    void setAction(int, QAction*);

public slots:
    void slotChangeViewStatus(bool);
    void slotRemoveTab(int);

protected:
    QTabWidget* tab_;
    QList<QWidget*> widget_;
    QList<QAction*> action_;
    QList<QString> name_;
};

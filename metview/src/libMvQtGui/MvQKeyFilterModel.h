/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QStyledItemDelegate>
#include <QStyleOptionViewItem>

#include "MvQKeyProfileModel.h"

class GribMetaData;
class MvKeyProfile;


class MvQKeyFilterDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    MvQKeyFilterDelegate(QWidget* parent = 0);
    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const;
    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
                          const QModelIndex& index) const;
    void setEditorData(QWidget* editor, const QModelIndex& index) const;
    void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index);

    void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;

private slots:
    void commmitAndCloseEditor();
};

class MvQKeyFilterModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    MvQKeyFilterModel();

    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    int rowCount(const QModelIndex& parent = QModelIndex()) const;

    Qt::ItemFlags flags(const QModelIndex& index) const;
    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex& index, const QVariant& value, int role);
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex&) const;

    MvKeyProfile* keyProfile() { return profile_; }
    void setKeyProfile(MvKeyProfile*);

    MvQKeyFilter keyFilter() { return filter_; }
    void setKeyFilter(MvQKeyFilter);

signals:
    void filterUpdate(MvQKeyFilter&);

protected:
    QString label(const int, const int) const;
    void setupKeyFilter();
    void loadKeyFilter();

private:
    bool isDataSet() const;
    MvKeyProfile* profile_;
    MvQKeyFilter filter_;
};

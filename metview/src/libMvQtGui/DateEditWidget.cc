#include "DateEditWidget.h"

#include <QCalendarWidget>
#include <QDateEdit>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QStyle>
#include <QToolButton>

DateEditWidget::DateEditWidget(QWidget* parent) :
    QWidget(parent)
{
    auto* hb = new QHBoxLayout(this);
    hb->setContentsMargins(0, 0, 0, 0);
    hb->setSpacing(0);

    dateEdit_ = new QDateEdit(this);
    dateEdit_->setCalendarPopup(true);
    dateEdit_->setSpecialValueText("ANY");

    clearTb_ = new QToolButton(this);
    clearTb_->setIcon(QPixmap(":/examiner/clear_left.svg"));
    clearTb_->setToolTip("Clear");
    clearTb_->setAutoRaise(true);

    connect(clearTb_, SIGNAL(clicked()),
            this, SLOT(slotClear()));

    hb->addWidget(dateEdit_);
    hb->addWidget(clearTb_);

    slotClear();
}

void DateEditWidget::slotClear()
{
    dateEdit_->setDate(dateEdit_->minimumDate());
    // dateEdit_->calendarWidget()->setSelectedDate(QDate::currentDate());
}

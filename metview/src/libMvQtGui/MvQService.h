/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QObject>

#include "MvRequest.h"
#include "MvProtocol.h"
#include "MvService.h"

class QSocketNotifier;

class MvQService : public QObject, public MvService
{
    Q_OBJECT

public:
    MvQService(const char* name = nullptr);
    virtual ~MvQService();
    void serve(MvRequest& /*in*/, MvRequest& /*out*/) override {}

public slots:
    void slotDataReceived(int);

protected:
    void setupSocketNotifier();

private:
    // No copy allowed
    MvQService(const MvQService&);
    MvQService& operator=(const MvQService&) { return *this; }

    QSocketNotifier* socketNotifier_;
};

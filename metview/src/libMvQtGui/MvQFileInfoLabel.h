/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QLabel>

class MvQFileInfoLabel : public QLabel
{
public:
    MvQFileInfoLabel(QWidget* parent = nullptr);
    void setTextLabel(QString);
    void setGribTextLabel(QString, int, bool, int, bool);
    void setBufrTextLabel(QString, int, bool, int, bool);
    void setOdbTextLabel(QString, QString);
    void setGeopTextLabel(QString, QString, long);
    void setFlextraTextLabel(QString, QString, int);
    void setProfileTextLabel(QString, int, int, QString);

protected:
    QString buildTextLabel(QString, QString fileExtraText = QString());
};

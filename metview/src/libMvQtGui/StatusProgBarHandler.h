/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once


#include <QString>
#include <QTime>

class QProgressBar;

class StatusProgBarHandler
{
public:
    ~StatusProgBarHandler() = default;
    static StatusProgBarHandler* instance();

    void init(QProgressBar*);
    void progress(int);
    void start();
    void finish();

protected:
    StatusProgBarHandler() :
        progBar_(nullptr) {}

private:
    static StatusProgBarHandler* instance_;

    QProgressBar* progBar_;
    QTime timer_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>

class MvQFileList;

class MvQFileListModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    MvQFileListModel(QObject* parent = nullptr);

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;
    Qt::ItemFlags flags(const QModelIndex&) const override;

    void setFileList(MvQFileList*);

    QString indexToPath(const QModelIndex&) const;
    QModelIndex pathToIndex(QString) const;

public slots:
    void slotDataAddRemoveBegin();
    void slotDataAddRemoveEnd();

private:
    MvQFileList* data_{nullptr};
};

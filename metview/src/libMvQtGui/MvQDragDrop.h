/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QStringList>
#include <QMap>

class QDropEvent;
class QMimeData;


class QDropEvent;

class MvQDrop
{
public:
    enum DataType
    {
        NoData,
        IconData,
        FeatureData
    };
    MvQDrop(QDropEvent*);
    MvQDrop(QMimeData*);
    bool hasData() const { return dataType_ == IconData; }
    int iconNum() const { return (hasData()) ? iconPath_.count() : 0; }
    QString iconPath(int) const;
    QString iconClass(int) const;
    QString value(QString key) const { return values_.value(key); }
    DataType dataType() const { return dataType_; }

protected:
    void parseMimeData(const QMimeData* data);

    DataType dataType_;
    QStringList iconPath_;
    QStringList iconClass_;
    QMap<QString, QString> values_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QDialog>
#include <QSettings>

#include "MvQConfigDialog.h"
#include "MvQKeyManager.h"

class QComboBox;
class QGridLayout;
class QLabel;
class QLineEdit;
class QListWidget;
class QModelIndex;
class QPushButton;
class QSplitter;
class QToolButton;
class QTreeView;

class MvQKeyModel;
class MvQTreeView;

class MvKey;
class MvKeyProfile;

class MvQKeyListPage;
class MvQKeyProfileDialog;

class MvQKeyProfileDialog : public QWidget
{
    Q_OBJECT

public:
    MvQKeyProfileDialog(QString title, MvQKeyManager*, int, QWidget* parent = nullptr);
    ~MvQKeyProfileDialog() override = default;

public slots:
    void slotUpdate();
    void slotSelectProfile(int);
    void slotAddProfile();
    void slotRenameProfile();
    void slotDeleteProfile();
    void slotDuplicateProfile();

signals:
    void dataChanged();
    void currentProfileChanged(int);

private:
    void init(int);

    QListWidget* profList_{nullptr};
    MvQKeyManager* manager_{nullptr};
    QPushButton* deletePb_{nullptr};
    QPushButton* renamePb_{nullptr};
};

class MvQKeyListPage : public QWidget
{
    Q_OBJECT

public:
    MvQKeyListPage(QString title, MvQKeyManager*, QList<MvKeyProfile*>, QWidget* parent = nullptr);
    ~MvQKeyListPage() override = default;
    void init();
    void setAllKeyList(QList<MvKeyProfile*>);

    void writeSettings(QSettings&);
    void readSettings(QSettings&);

public slots:
    void slotProfilesChanged();
    void slotChangeCurrentProfile(int);
    void slotSelectProfile(int);
    void slotAddKey();
    void slotDeleteKey();
    void slotMoveUpKey();
    void slotMoveDownKey();
    void slotEditKey();
    void slotEditTree(const QModelIndex&);
    void slotAddKeyToProf();
    void slotSelectAllKeyGroup(int);

private:
    QTreeView* keyTree_{nullptr};
    MvQKeyModel* keyModel_{nullptr};

    MvQKeyManager* manager_{nullptr};

    QSplitter* splitter_{nullptr};
    QWidget* allKeyWidget_{nullptr};
    QList<MvKeyProfile*> allKey_;
    MvQKeyModel* allKeyModel_{nullptr};
    QTreeView* allKeyTree_{nullptr};
    QComboBox* allKeyCombo_{nullptr};

    QComboBox* profCombo_{nullptr};

    QToolButton* addKeyTb_{nullptr};
    QToolButton* deleteKeyTb_{nullptr};
    QToolButton* moveUpKeyTb_{nullptr};
    QToolButton* moveDownKeyTb_{nullptr};
    QToolButton* editKeyTb_{nullptr};
    QPushButton* addKeyToProfTb_{nullptr};
};

class MvQKeyEditDialog : public QDialog
{
    Q_OBJECT

public:
    MvQKeyEditDialog(MvKey*, QWidget* parent = nullptr);
    ~MvQKeyEditDialog() override;

public slots:
    void accept() override;
    void reject() override;

private:
    MvKey* key_{nullptr};
    QLineEdit* nameLe_{nullptr};
    QLineEdit* shortLe_{nullptr};
    QLineEdit* descLe_{nullptr};
    QMap<std::string, QLineEdit*> metaLe_;
};


class MvQKeyImportPage : public QWidget
{
    Q_OBJECT

public:
    MvQKeyImportPage(QString title, MvQKeyManager*, QWidget* parent = nullptr);
    ~MvQKeyImportPage() override;

public slots:
    void slotSelectAll();
    void slotUnselectAll();
    void slotImport();

signals:
    void dataChanged();

private:
    void init();

    QListWidget* profList_{nullptr};
    MvQKeyManager* manager_{nullptr};
    MvQKeyManager* inManager_{nullptr};
};


class MvQKeyDialog : public MvQConfigDialog
{
    Q_OBJECT

public:
    MvQKeyDialog(QString title, MvQKeyManager::KeyType,
                 MvQKeyManager*, int, QList<MvKeyProfile*>, QWidget* parent = nullptr);
    ~MvQKeyDialog() override;

public slots:
    void accept() override;
    void reject() override;

private:
    void writeSettings();
    void readSettings();

    MvQKeyManager* manager_{nullptr};
    MvQKeyProfileDialog* profPage_{nullptr};
    MvQKeyListPage* keyPage_{nullptr};
    QString settingsName_;
};

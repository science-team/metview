/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QtGlobal>
#include <QMimeData>
#include <QStringList>

class MvKey;
class QAbstractItemModel;

class MvQKeyMimeData : public QMimeData
{
    Q_OBJECT

public:
    MvQKeyMimeData(const QAbstractItemModel* model) :
        model_(model) {}

    bool hasFormat(const QString& mimeType) const override;
    QStringList formats() const override;

    void addKey(MvKey* key, int row) { keys_[row] = key; }
    const QMap<int, MvKey*>& keys() const { return keys_; }
    const QAbstractItemModel* model() const { return model_; }

protected:
    // TODO: implement this method
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    QVariant retrieveData(const QString& /*mimeType*/, QMetaType /*type*/) const override
    {
        return {};
    }
#else
    QVariant retrieveData(const QString& /*mimeType*/, QVariant::Type /*type*/) const override
    {
        return {};
    }
#endif
private:
    const QAbstractItemModel* model_{nullptr};
    QMap<int, MvKey*> keys_;
};

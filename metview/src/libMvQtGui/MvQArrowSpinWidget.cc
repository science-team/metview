/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQArrowSpinWidget.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QToolButton>

MvQArrowSpinWidget::MvQArrowSpinWidget(QWidget* parent) :
    QWidget(parent)
{
    auto* hb = new QHBoxLayout();
    hb->setContentsMargins(4, 0, 4, 0);
    hb->setSpacing(0);

    setLayout(hb);
    spin_ = new QSpinBox(this);

    prevTb_ = new QToolButton(this);
    prevTb_->setIcon(QPixmap(":/examiner/triangle_left_orange.svg"));
    prevTb_->setAutoRaise(true);

    nextTb_ = new QToolButton(this);
    nextTb_->setIcon(QPixmap(":/examiner/triangle_right_orange.svg"));
    nextTb_->setAutoRaise(true);

    hb->addWidget(prevTb_);
    hb->addSpacing(0);
    hb->addWidget(spin_);
    hb->addSpacing(0);
    hb->addWidget(nextTb_);

    connect(spin_, SIGNAL(valueChanged(int)),
            this, SLOT(slotSpinChanged(int)));

    connect(prevTb_, SIGNAL(clicked(bool)),
            this, SLOT(slotPrev(bool)));

    connect(nextTb_, SIGNAL(clicked(bool)),
            this, SLOT(slotNext(bool)));
}

int MvQArrowSpinWidget::value() const
{
    return spin_->value();
}

void MvQArrowSpinWidget::setValue(int val, bool broadcast)
{
    if (!broadcast)
        broadcast_ = false;

    if (val != spin_->value()) {
        spin_->setEnabled(true);
        spin_->setValue(val);
    }

    if (!broadcast)
        broadcast_ = true;

    checkState();
}

void MvQArrowSpinWidget::clear(bool broadcast)
{
    if (!broadcast)
        broadcast_ = false;

    spin_->setMinimum(0);
    spin_->setMaximum(0);
    spin_->setEnabled(false);

    if (!broadcast)
        broadcast_ = true;

    checkState();
}

void MvQArrowSpinWidget::reset(int maxVal, bool broadcast)
{
    if (!broadcast)
        broadcast_ = false;

    if (maxVal <= 0) {
        spin_->setMinimum(0);
        spin_->setMaximum(0);
        spin_->setEnabled(false);
    }
    else {
        spin_->setEnabled(true);
        int current = spin_->value();
        spin_->setMinimum(1);
        spin_->setMaximum(maxVal);
        if (current >= maxVal) {
            spin_->setValue(1);
        }

        if (spin_->maximum() == spin_->minimum())
            spin_->setEnabled(false);
    }

    if (!broadcast)
        broadcast_ = true;

    checkState();
}

void MvQArrowSpinWidget::slotSpinChanged(int val)
{
    if (broadcast_)
        emit valueChanged(val);

    checkState();
}

void MvQArrowSpinWidget::slotPrev(bool)
{
    if (spin_->value() > spin_->minimum()) {
        spin_->setValue(spin_->value() - 1);
        checkState();
    }
}

void MvQArrowSpinWidget::slotNext(bool)
{
    if (spin_->value() < spin_->maximum()) {
        spin_->setValue(spin_->value() + 1);
        checkState();
    }
}

void MvQArrowSpinWidget::checkState()
{
    prevTb_->setEnabled(spin_->value() != spin_->minimum());
    nextTb_->setEnabled(spin_->value() != spin_->maximum());
}

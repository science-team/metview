/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QComboBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QPainter>
#include <QStackedLayout>
#include <QToolButton>

#include "MvQSearchLinePanel.h"

#include "AbstractSearchLine.h"


//============================================
//
//  MvQSearchLinePanel
//
//=============================================

MvQSearchLinePanel::MvQSearchLinePanel()
{
    auto* layout = new QHBoxLayout;
    // layout->setContentsMargins(0,0,0,0);
    setLayout(layout);

    auto* closeTb = new QToolButton(this);
    QIcon icon(QPixmap(QString::fromUtf8(":/find/close.svg")));
    closeTb->setIcon(icon);
    closeTb->setAutoRaise(true);

    connect(closeTb, SIGNAL(clicked(bool)),
            this, SLOT(setVisible(bool)));

    itemLayout_ = new QStackedLayout;

    layout->addWidget(closeTb);
    layout->addLayout(itemLayout_);
}

void MvQSearchLinePanel::addSearchLine(AbstractSearchLine* item, QWidget* itemId)
{
    if (item && itemId) {
        items_[itemId] = item;
        itemLayout_->addWidget(item);
        setCurrentSearchLineById(itemId);
    }
}

void MvQSearchLinePanel::slotFindNext(bool)
{
    if (isVisible() == false)
        return;

    QWidget* w = itemLayout_->currentWidget();
    if (w) {
        auto* item = dynamic_cast<AbstractSearchLine*>(w);
        if (item) {
            item->slotFindNext();
        }
    }
}

void MvQSearchLinePanel::slotFindPrev(bool)
{
    if (isVisible() == false)
        return;

    QWidget* w = itemLayout_->currentWidget();
    if (w) {
        auto* item = dynamic_cast<AbstractSearchLine*>(w);
        if (item) {
            item->slotFindPrev();
        }
    }
}

void MvQSearchLinePanel::setCurrentSearchLineById(QWidget* itemId)
{
    if (!itemId) {
        setEnabled(false);
        return;
    }
    else if (items_.find(itemId) != items_.end()) {
        setEnabled(true);
        itemLayout_->setCurrentWidget(items_[itemId]);
    }
    else {
        setEnabled(false);
    }
}

//============================================
//
//  MvQDualSearchLinePanel
//
//=============================================

MvQDualSearchLinePanel::MvQDualSearchLinePanel() :
    currentLeftItem_(nullptr),
    currentRightItem_(nullptr)
{
    auto* layout = new QHBoxLayout;
    // layout->setContentsMargins(0,0,0,0);
    setLayout(layout);

    auto* closeTb = new QToolButton(this);
    QIcon icon(QPixmap(QString::fromUtf8(":/find/close.svg")));
    closeTb->setIcon(icon);
    closeTb->setAutoRaise(true);

    connect(closeTb, SIGNAL(clicked(bool)),
            this, SLOT(setVisible(bool)));

    itemLayout_ = new QStackedLayout;

    auto* label = new QLabel(tr("&Scope: "), this);
    searchModeCb_ = new QComboBox;
    label->setBuddy(searchModeCb_);

    QPixmap leftPix(13, 13);
    QPainter leftPainter(&leftPix);
    leftPainter.fillRect(0, 0, 13, 13, Qt::black);
    leftPainter.fillRect(1, 1, 11, 11, QColor(255, 230, 191));

    QPixmap rightPix(13, 13);
    QPainter rightPainter(&rightPix);
    rightPainter.fillRect(0, 0, 13, 13, Qt::black);
    rightPainter.fillRect(1, 1, 11, 11, QColor(194, 221, 255));


    searchModeCb_->addItem(QIcon(leftPix), tr("Left Panel"));
    searchModeCb_->addItem(QIcon(rightPix), tr("Right Panel"));

    searchModeCb_->setCurrentIndex(1);

    layout->addWidget(closeTb);
    layout->addWidget(label);
    layout->addWidget(searchModeCb_);
    layout->addSpacing(10);
    layout->addLayout(itemLayout_);
    // layout->addSpacing(20);

    connect(searchModeCb_, SIGNAL(activated(int)),
            this, SLOT(slotSearchModeChanged(int)));
}

void MvQDualSearchLinePanel::addSearchLineToLeft(AbstractSearchLine* item, QWidget* itemId)
{
    if (item && itemId) {
        leftItems_[itemId] = item;
        itemLayout_->addWidget(item);
        setCurrentSearchLineById(itemId);
    }
}

void MvQDualSearchLinePanel::addSearchLineToRight(AbstractSearchLine* item, QWidget* itemId)
{
    if (item && itemId) {
        rightItems_[itemId] = item;
        itemLayout_->addWidget(item);
        setCurrentSearchLineById(itemId);
    }
}

void MvQDualSearchLinePanel::slotFindNext(bool)
{
    if (isVisible() == false)
        return;

    QWidget* w = itemLayout_->currentWidget();
    if (w) {
        auto* item = dynamic_cast<AbstractSearchLine*>(w);
        if (item) {
            item->slotFindNext();
        }
    }
}

void MvQDualSearchLinePanel::slotFindPrev(bool)
{
    if (isVisible() == false)
        return;

    QWidget* w = itemLayout_->currentWidget();
    if (w) {
        auto* item = dynamic_cast<AbstractSearchLine*>(w);
        if (item) {
            item->slotFindPrev();
        }
    }
}

void MvQDualSearchLinePanel::slotSearchModeChanged(int index)
{
    if (index == SearchLeftItems && currentLeftItem_) {
        itemLayout_->setCurrentWidget(currentLeftItem_);
    }
    else if (index == SearchRightItems && currentRightItem_) {
        itemLayout_->setCurrentWidget(currentRightItem_);
    }
}


void MvQDualSearchLinePanel::setCurrentSearchLineById(QWidget* itemId)
{
    if (!itemId) {
        setEnabled(false);
        return;
    }
    else {
        setEnabled(true);
    }

    if (leftItems_.find(itemId) != leftItems_.end()) {
        currentLeftItem_ = leftItems_[itemId];
        if (searchModeCb_->currentIndex() == SearchLeftItems) {
            itemLayout_->setCurrentWidget(currentLeftItem_);
        }
    }
    else if (rightItems_.find(itemId) != rightItems_.end()) {
        currentRightItem_ = rightItems_[itemId];
        if (searchModeCb_->currentIndex() == SearchRightItems) {
            itemLayout_->setCurrentWidget(currentRightItem_);
        }
    }
}

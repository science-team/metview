/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QLabel>
#include <QPen>

class MvQDropTarget : public QLabel
{
public:
    void reset(QString, bool, QString prefix = QString());
    void reset(QString, QString, QString prefix = QString());

    static MvQDropTarget* Instance();

protected:
    MvQDropTarget();
    void createPixmap(QString);

    static MvQDropTarget* instance_;

    bool move_;
    QString target_;
    QString text_;
    int xPadding_;
    int yPadding_;
    QPen bgPen_;
    QBrush bgBrush_;
    QPixmap decorPix_;
    QPixmap pix_;
    QFont font_;
};

/***************************** LICENSE START ***********************************

 Copyright 2017 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "ui_LocationWidget.h"

#include <QWidget>

class BufrLocationCollector;
class MvKeyProfile;
class MvQKeyProfileModel;
class MvQKeyProfileSortFilterModel;

class LocationWidget : public QWidget, protected Ui::LocationWidget
{
    Q_OBJECT

public:
    explicit LocationWidget(QWidget* parent = nullptr);
    ~LocationWidget() override = default;

    BufrLocationCollector* makeCollector();
    void update(BufrLocationCollector*);
    void update(MvKeyProfile*);
    void updateMessage(int);
    void clear();
    int maxNum() const { return maxNum_; }

    enum Mode
    {
        LocationMode,
        DataMode
    };
    void setMode(Mode mode) { mode_ = mode; }

protected Q_SLOTS:
    void slotCurrentChangedInMap(int index);
    void slotCurrentChangedInTree(const QModelIndex&);

Q_SIGNALS:
    void messageSelected(int);

private:
    void updateNumLabel(int, int);

    Mode mode_{LocationMode};
    MvQKeyProfileModel* model_{nullptr};
    MvQKeyProfileSortFilterModel* sortModel_{nullptr};
    MvKeyProfile* prof_{nullptr};
    int maxNum_{500000};
    bool inChangeFromMap_{false};
    bool inChangeFromTree_{false};
    bool inUpdateMessage_{false};
};

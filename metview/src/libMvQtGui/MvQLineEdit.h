/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QLineEdit>

class QLabel;
class QToolButton;


class MvQLineEdit : public QLineEdit
{
    Q_OBJECT

public:
    MvQLineEdit(QWidget* parent = 0);
    void setDecoration(QPixmap);

public slots:
    void slotClear();

signals:
    void textCleared();

protected:
    void adjustSize();
    void resizeEvent(QResizeEvent*);

    QToolButton* clearTb_{nullptr};
    QLabel* iconLabel_{nullptr};
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "ui_GotoLineDialog.h"


class GotoLineDialog : public QDialog, private Ui::GotoLineDialog
{
    Q_OBJECT

public:
    explicit GotoLineDialog(QWidget* parent = nullptr);
    ~GotoLineDialog() override;
    void setupUIBeforeShow();

Q_SIGNALS:
    void gotoLine(int line);  // emitted when the user says 'ok'


public Q_SLOTS:
    void slotDone();
    void setButtonStatus();
};

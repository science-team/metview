/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QMainWindow>
#include "MvQMenuItem.h"

class QAction;
class QLabel;
class QMenu;
class QObject;
class QToolBar;


class MvQMainWindow : public QMainWindow
{
public:
    enum ActionType
    {
        AboutAction,
        CloseAction,
        LogAction,
        ConfigureAction,
        FindAction,
        FindNextAction,
        FindPreviousAction,
        QuitAction
    };
    enum MenuType
    {
        AnimationMenu,
        BookmarksMenu,
        FileMenu,
        FilterMenu,
        EditMenu,
        HelpMenu,
        HistoryMenu,
        NavigateMenu,
        ProfilesMenu,
        SelectionMenu,
        SettingsMenu,
        StepMenu,
        ToolsMenu,
        ViewMenu,
        ZoomMenu,
        ShareMenu,
        ProgressBarMenu,
        EditorMenu
    };

    enum MenuOption
    {
        NoOption,
        ToolBarOnlyOption
    };

    using MenuItemMap = QMap<MenuType, QList<MvQMenuItem*>>;

    MvQMainWindow(QWidget* parent = nullptr);
    QRect screenGeometry();
    void setInitialSize(int, int);
    static QAction* createAction(ActionType, QObject*);
    static QAction* createAction(QString, QString, QString, QObject* parent = nullptr);
    static QAction* createSeparator(QObject* parent = nullptr);
    void setMovableToolBars(bool b) { movableToolBars_ = b; }
    void setAppIcon(QString name);

protected:
    void setupMenus(MenuItemMap, MenuOption option = NoOption);
    QMenu* createMenu(QString, QString, QList<MvQMenuItem*>);
    QMenu* createMenu(QString, QString);
    QToolBar* createToolBar(QString, QString, QList<MvQMenuItem*>, Qt::ToolBarAreas = Qt::AllToolBarAreas);
    QToolBar* createToolBar(QString, QString, Qt::ToolBarAreas = Qt::AllToolBarAreas);
    bool isToolBarItemPresent(QList<MvQMenuItem*>);
    void populate(QMenu*, QToolBar*, QList<MvQMenuItem*>);
    QAction* findAction(QList<QAction*>, QString);
    bool isActionPresent(QList<MvQMenuItem*>, QString);
    bool isMenuNeeded(QList<MvQMenuItem*>);
    QMenu* findMenu(MvQMainWindow::MenuType);
    bool isToolBarNeeded(QList<MvQMenuItem*>);
    QToolBar* findToolBar(MvQMainWindow::MenuType);

    QMap<MenuType, QString> menuName_;
    QList<MenuType> menuOrder_;
    bool movableToolBars_;
};

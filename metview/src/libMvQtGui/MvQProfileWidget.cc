/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QtGlobal>
#include <QDebug>
#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QVBoxLayout>

#include "MvQProfileWidget.h"
#include "MvQProfileView.h"
#include "MvQRangeWidget.h"

#include "MvProfileData.h"

MvQProfileWidget::MvQProfileWidget(QWidget* parent) :
    QWidget(parent),
    startLevelIndex_(-1),
    endLevelIndex_(-1),
    editRadius_(0)
{
    auto* mainHb = new QHBoxLayout(this);
    mainHb->setContentsMargins(0, 0, 0, 1);
    mainHb->setSpacing(1);


    auto* profLayout = new QVBoxLayout;
    mainHb->addLayout(profLayout, 1);

    profileLabel_ = new QLabel;
    profileLabel_->setAutoFillBackground(true);
    profileLabel_->setObjectName(QString::fromUtf8("fileInfoLabel"));
    profileLabel_->setMargin(2);
    profLayout->addWidget(profileLabel_);

    profileView_ = new MvQProfileView(this);
    profLayout->addWidget(profileView_);

    connect(profileView_, SIGNAL(cursorData(QString)),
            profileLabel_, SLOT(setText(QString)));

    /*QHBoxLayout* levHb=new QHBoxLayout;
    profLayout->addLayout(levHb);

    QLabel *label;
    label=new QLabel(tr("Edit radius:"));
    levHb->addWidget(label);
    editRadiusSpin_=new QSpinBox(this);
        editRadiusSpin_->setSuffix(" level");
    levHb->addWidget(editRadiusSpin_);

    //Edit radius spinbox
    editRadiusSpin_->setRange(1,5);
    editRadiusSpin_->setValue(3);

    connect(editRadiusSpin_,SIGNAL(valueChanged(int)),
        this, SLOT(slotEditRadius(int)));

    //Range widget
    levHb->addStretch(1);*/

    rangeWidget_ = new MvQRangeWidget(this);
    mainHb->addWidget(rangeWidget_);

    connect(rangeWidget_, SIGNAL(valueChanged(int, int)),
            this, SLOT(slotLevelRange(int, int)));
}

MvQProfileWidget::~MvQProfileWidget() = default;

void MvQProfileWidget::setProfile(QList<MvProfileData*> profData, bool editable)
{
    profileView_->setProfile(profData, editable, startLevelIndex_, endLevelIndex_);

    std::vector<float> levels;
    profData[0]->levels(levels);
#if QT_VERSION >= QT_VERSION_CHECK(5, 14, 0)
    rangeWidget_->reset(QVector<float>(levels.begin(), levels.end()));
#else
    rangeWidget_->reset(QVector<float>::fromStdVector(levels));
#endif
}

void MvQProfileWidget::update(const MvProfileChange& item)
{
    profileView_->update(item);
}

void MvQProfileWidget::reload()
{
    profileView_->reload();
}

void MvQProfileWidget::slotLevelRange(int startIndex, int endIndex)
{
    startLevelIndex_ = startIndex;
    endLevelIndex_ = endIndex;
    profileView_->setLevelRange(startIndex, endIndex);
}

void MvQProfileWidget::slotEditRadius(int rad)
{
    editRadius_ = rad;
    profileView_->setEditRadius(rad);
}

void MvQProfileWidget::writeSettings(QSettings& settings)
{
    settings.beginGroup("profile");
    settings.setValue("startLevelIndex", startLevelIndex_);
    settings.setValue("endLevelIndex", endLevelIndex_);
    settings.setValue("editRadius", editRadius_);
    settings.endGroup();
}

void MvQProfileWidget::readSettings(QSettings& settings)
{
    settings.beginGroup("profile");

    if (!settings.value("startLevelIndex").isNull() && !settings.value("endLevelIndex").isNull()) {
        int sv = settings.value("startLevelIndex").toInt();
        int ev = settings.value("endLevelIndex").toInt();
        rangeWidget_->setRange(sv, ev);
        slotLevelRange(sv, ev);
    }

    if (!settings.value("editRadius").isNull())
        slotEditRadius(settings.value("editRadius").toInt());

    settings.endGroup();
}

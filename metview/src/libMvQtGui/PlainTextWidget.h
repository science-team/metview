//============================================================================
// Copyright 2014 ECMWF.
// This software is licensed under the terms of the Apache Licence version 2.0
// which can be obtained at http://www.apache.org/licenses/LICENSE-2.0.
// In applying this licence, ECMWF does not waive the privileges and immunities
// granted to it by virtue of its status as an intergovernmental organisation
// nor does it submit to any jurisdiction.
//
//============================================================================

#pragma once

#include <QWidget>
#include <QSettings>

#include "ui_PlainTextWidget.h"

class PlainTextWidget : public QWidget, protected Ui::PlainTextWidget
{
    Q_OBJECT

public:
    explicit PlainTextWidget(QWidget* parent = nullptr);
    ~PlainTextWidget() override;

    PlainTextEdit* editor() const;
    MessageLabel* messageLabel() const;
    void clear();
    void readSettings(QSettings& settings, QString);
    void writeSettings(QSettings& settings, QString);

protected Q_SLOTS:
    void on_searchTb__clicked();
    void on_gotoLineTb__clicked();
    void on_fontSizeUpTb__clicked();
    void on_fontSizeDownTb__clicked();
    void showLineNumbersClicked(bool);

Q_SIGNALS:
    void editorFontSizeChanged();

protected:
    void removeSpacer();
};

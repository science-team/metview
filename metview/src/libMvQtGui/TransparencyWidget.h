/***************************** LICENSE START ***********************************

 Copyright 2021 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QWidget>

class QLabel;
class QSlider;
class QSpinBox;
class QToolButton;

// Values are between 0-100
class TransparencyWidget : public QWidget
{
    Q_OBJECT
public:
    TransparencyWidget(QWidget* parent, QFont f = QFont());
    void init(int);
    void initAlpha(int);
    int value() const;
    int alphaValue() const;
    void setUseLabel(bool);
    void setUseSpin(bool);

protected Q_SLOTS:
    void sliderChanged(int value);
    void sliderReleased();
    void spChanged(int value);
    void resetValue();

signals:
    void valueChanged(int);

protected:
    int trToAlpha(int tr) const;
    int alphaToTr(int a) const;

    QSlider* slider_;
    QSpinBox* sp_;
    QLabel* label_;
    QToolButton* resetTb_;
    bool ignoreChange_{false};
    bool useSp_{false};
    bool useLabel_{true};
};

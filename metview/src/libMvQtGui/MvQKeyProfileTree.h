/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once


#include "MvKeyProfile.h"

class MvQKeyManager;
class MvQKeyProfileModel;
class MvQKeyProfileSortFilterModel;

#include "MvQTreeView.h"

class MvQKeyProfileTree : public MvQTreeView
{
    Q_OBJECT

public:
    MvQKeyProfileTree(QWidget* parent);
    ~MvQKeyProfileTree() override = default;
    void setEditable(bool);
    bool editable() const { return editable_; }
    void setPredefinedKeysOnly(bool b) { predefinedKeysOnly_ = b; }
    void setModels(MvQKeyProfileModel* messageModel,
                   MvQKeyProfileSortFilterModel* sortModel);

public slots:
    void slotMessageTreeContextMenu(const QPoint&);
    void slotMessageTreeColumnMoved(int, int, int);

protected slots:
    void selectionChanged(const QItemSelection& selected, const QItemSelection& deselected) override;
    void insertKeys(QList<MvKey*>, int);

signals:
    void profileChanged(bool reaload, int selectedRow);
    void selectionChanged(const QModelIndex&);

private:
    void insertMessageTreeColumn(int, bool before);
    void renameMessageTreeHeader(int);
    void deleteMessageTreeColumn(int);
    void editMessageTreeHeader(int);

    MvQKeyProfileModel* messageModel_{nullptr};
    MvQKeyProfileSortFilterModel* messageSortModel_{nullptr};
    bool editable_{false};
    bool predefinedKeysOnly_{false};
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QMap>

#include "MvQOdbMetaData.h"

class MvAbstractOdb;

class MvQOdbVarModel : public QAbstractItemModel
{
public:
    MvQOdbVarModel();

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    void setBaseData(QList<MvQOdbVar*>&);

protected:
    QString label(MvQOdbVar*, const int) const;

private:
    QList<MvQOdbVar*> data_;
};

class MvQOdbColumnModel : public QAbstractItemModel
{
public:
    MvQOdbColumnModel(bool);

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    void setBaseData(const QList<MvQOdbColumn*>&, MvQOdbMetaData::OdbVersion);

protected:
    QVariant label(MvQOdbColumn*, const int) const;
    QString label(MvQOdbBitfieldMember*, const int) const;
    int idToLevel(int) const;
    int idToParentRow(int) const;

private:
    QMap<MvQOdbColumn::OdbColumnType, QString> typeMap_;
    QList<MvQOdbColumn*> data_;
    MvQOdbMetaData::OdbVersion odbVersion_;
    bool showTableColumn_;
};


class MvQOdbDataModel : public QAbstractItemModel
{
public:
    MvQOdbDataModel();

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    void dataIsAboutToChange();
    void setBaseData(MvAbstractOdb*);
    void setFilter(const std::vector<int>&);
    void setTmpFilter(const std::vector<int>&);
    void clearTmpFilter();
    bool filtered() const;
    bool hasTmpFilter() const { return hasTmpFilter_; }
    int filterCount() const { return static_cast<int>(filter_.size()); }
    const std::vector<int>& tmpFilter() const { return tmpFilter_; }

private:
    bool hasFilter() const;

    MvAbstractOdb* data_;
    std::vector<int> filter_;
    std::vector<int> tmpFilter_;
    bool hasTmpFilter_;
    QList<int> columnOrder_;
};

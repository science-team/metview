/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QList>
#include <QString>

#include <string>
#include <vector>

class QModelIndex;
class QTreeView;

class ExpandNode
{
    friend class TreeNodeView;

public:
    explicit ExpandNode(QString name) :
        name_(name) {}
    ExpandNode() :
        name_("") {}
    ~ExpandNode();

    void clear();
    ExpandNode* add(QString);

    QList<ExpandNode*> children_;
    QString name_;
};

class MvQTreeExpandState
{
    friend class TreeNodeView;

public:
    explicit MvQTreeExpandState(QTreeView* view);
    ~MvQTreeExpandState();

    void clear();
    void save();
    void restore();
    void init(QStringList topLevelNodes);

protected:
    void saveExpand(ExpandNode* parentExpand, const QModelIndex& idx, const QModelIndex& selIdx);
    void restoreExpand(ExpandNode* expand, const QModelIndex& idx);

    ExpandNode* setRoot(QString);
    ExpandNode* root() const { return root_; }

    ExpandNode* root_{nullptr};
    QTreeView* view_{nullptr};
    ExpandNode* selectionParent_{nullptr};
    QString selectionName_;
};

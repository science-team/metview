/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QTextDocument>
#include <QTimer>

#include "AbstractSearchLine.h"

class AbstractTextEditSearchInterface;

class TextEditSearchLine : public AbstractSearchLine
{
    Q_OBJECT

public:
    explicit TextEditSearchLine(QWidget* parent);
    ~TextEditSearchLine() override;
    void setSearchInterface(AbstractTextEditSearchInterface*);
    void searchOnReload(bool userClickedReload);

public Q_SLOTS:
    void slotFind(QString) override;
    void slotFindNext() override;
    void slotFindPrev() override;
    void slotFindNext(bool) { slotFindNext(); }
    void slotFindPrev(bool) { slotFindPrev(); }
    void matchModeChanged(int newIndex);
    void on_actionCaseSensitive__toggled(bool) override;
    void on_actionWholeWords__toggled(bool) override;
    void on_actionHighlightAll__toggled(bool) override;
    void slotClose() override;
    void slotHighlight();

protected:
    QTextDocument::FindFlags findFlags();
    bool findString(QString str, bool highlightAll, QTextDocument::FindFlags extraFlags, QTextCursor::MoveOperation move, int iteration);
    void refreshSearch();
    void highlightMatches(QString txt);
    void clearHighlights();
    void disableHighlights();
    bool lastFindSuccessful() { return lastFindSuccessful_; }

    AbstractTextEditSearchInterface* interface_;
    QTimer highlightAllTimer_;
    QColor highlightColour_;
    bool lastFindSuccessful_;
};

/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "CalendarDialog.h"

CalendarDialog::CalendarDialog(QWidget* parent) :
    QDialog(parent)
{
    setupUi(this);

    connect(cal_, SIGNAL(clicked(QDate)),
            this, SIGNAL(dateSelected(QDate)));
}

void CalendarDialog::setSelectedDate(QDate dd)
{
    cal_->setSelectedDate(dd);
}

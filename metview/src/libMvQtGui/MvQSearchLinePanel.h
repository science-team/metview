/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QMap>
#include <QWidget>

class QComboBox;
class QStackedLayout;

class AbstractSearchLine;

class MvQSearchLinePanel : public QWidget
{
    Q_OBJECT

public:
    MvQSearchLinePanel();
    void addSearchLine(AbstractSearchLine*, QWidget*);
    void setCurrentSearchLineById(QWidget*);

public slots:
    void slotFindNext(bool);
    void slotFindPrev(bool);

protected:
    QStackedLayout* itemLayout_;
    QMap<QWidget*, AbstractSearchLine*> items_;
};


class MvQDualSearchLinePanel : public QWidget
{
    Q_OBJECT

public:
    MvQDualSearchLinePanel();
    void addSearchLineToLeft(AbstractSearchLine*, QWidget*);
    void addSearchLineToRight(AbstractSearchLine*, QWidget*);
    void setCurrentSearchLineById(QWidget*);

public slots:
    void slotFindNext(bool);
    void slotFindPrev(bool);
    void slotSearchModeChanged(int);

protected:
    enum SearchMode
    {
        SearchLeftItems = 0,
        SearchRightItems = 1
    };
    QStackedLayout* itemLayout_;
    AbstractSearchLine* currentLeftItem_;
    AbstractSearchLine* currentRightItem_;
    QMap<QWidget*, AbstractSearchLine*> leftItems_;
    QMap<QWidget*, AbstractSearchLine*> rightItems_;
    QComboBox* searchModeCb_;
};

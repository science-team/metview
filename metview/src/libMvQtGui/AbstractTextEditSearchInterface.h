/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "StringMatchMode.h"
#include "MvQProperty.h"

#include <QColor>
#include <QTextDocument>
#include <QTextCursor>

class AbstractTextEditSearchInterface
{
public:
    AbstractTextEditSearchInterface();
    virtual ~AbstractTextEditSearchInterface() = default;

    virtual bool findString(QString str, bool highlightAll, QTextDocument::FindFlags findFlags,
                            QTextCursor::MoveOperation move, int iteration, StringMatchMode::Mode matchMode) = 0;
    virtual void automaticSearchForKeywords(bool) = 0;
    virtual void refreshSearch() = 0;
    virtual void clearHighlights() = 0;
    virtual void disableHighlights() = 0;
    virtual void enableHighlights() = 0;
    virtual bool highlightsNeedSearch() = 0;
    virtual void gotoLastLine() = 0;

protected:
    void setColours();

    QColor highlightColour_;
    MvQProperty* vpPerformAutomaticSearch_{nullptr};
    MvQProperty* vpAutomaticSearchMode_{nullptr};
    MvQProperty* vpAutomaticSearchText_{nullptr};
    MvQProperty* vpAutomaticSearchFrom_{nullptr};
    MvQProperty* vpAutomaticSearchCase_{nullptr};
};

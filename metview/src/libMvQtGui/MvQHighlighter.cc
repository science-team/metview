/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QStringList>
#include <QRegularExpressionMatch>

#include "MvQHighlighter.h"

MvQHighlighter::MvQHighlighter(QTextDocument* parent) :
    QSyntaxHighlighter(parent)
{
}

void MvQHighlighter::highlightBlock(const QString& text)
{
    foreach (HighlightingRule rule, highlightingRules) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
        QRegularExpression expression(rule.pattern);
        QRegularExpressionMatch rmatch;
        int index = text.indexOf(expression, 0, &rmatch);
        while (index >= 0) {
            int length = rmatch.capturedLength();
            setFormat(index, length, rule.format);
            index = text.indexOf(expression, index + length, &rmatch);
        }
#else
        QRegExp expression(rule.pattern);
        int index = text.indexOf(expression);
        while (index >= 0) {
            int length = expression.matchedLength();
            setFormat(index, length, rule.format);
            index = text.indexOf(expression, index + length);
        }
#endif
    }
    setCurrentBlockState(0);
}

MvQOdbSqlHighlighter::MvQOdbSqlHighlighter(QTextDocument* parent, QStringList columns, QStringList /*functions*/) :
    MvQHighlighter(parent)
{
    HighlightingRule rule;

    // Keywords
    QTextCharFormat keywordFormat;

    keywordFormat.setForeground(Qt::darkBlue);
    keywordFormat.setFontWeight(QFont::Bold);

    QStringList keywordPatterns;
    keywordPatterns << "select"
                    << "from"
                    << "where"
                    << "orderby"
                    << "uniqueby"
                    << "distinct";

    foreach (QString pattern, keywordPatterns) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
        rule.pattern = QRegularExpression(pattern);
        rule.pattern.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
#else
        rule.pattern = QRegExp(pattern, Qt::CaseInsensitive);
#endif
        // QString s=pattern + "(?==)";
        // rule.pattern = QRegExp(s,Qt::CaseInsensitive);
        rule.format = keywordFormat;
        highlightingRules.append(rule);
    }

    // Columns
    setColumns(columns);
}

void MvQOdbSqlHighlighter::setColumns(QStringList columns)
{
    if (columns.count() == 0)
        return;

    // Here we should remove the previous settings!!!!!!

    HighlightingRule rule;

    rule.id = "column";

    QTextCharFormat columnFormat;

    columnFormat.setForeground(Qt::red);
    columnFormat.setFontWeight(QFont::Bold);

    foreach (QString pattern, columns) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
        rule.pattern = QRegularExpression(pattern);
        rule.pattern.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
#else
        rule.pattern = QRegExp(pattern, Qt::CaseInsensitive);
#endif
        // QString s=pattern + "(?==)";
        // rule.pattern = QRegExp(s,Qt::CaseInsensitive);
        rule.format = columnFormat;
        highlightingRules.append(rule);
    }
}

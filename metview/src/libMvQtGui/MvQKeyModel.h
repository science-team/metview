/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QMimeData>
#include <QPixmap>
#include <QStringList>

class MvQPixmapCache;

class MvKeyProfile;
class MvKey;

class MvQKeyModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    enum DisplayMode
    {
        ProfileContentsMode,
        AllKeysMode,
        ShortNameMode
    };

    MvQKeyModel(DisplayMode mode, QObject* parent);
    MvQKeyModel(MvKeyProfile*, DisplayMode mode, QObject* parent);

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex&, const QVariant&, int role = Qt::EditRole) override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    void profileIsAboutToChange();
    MvKeyProfile* profile() { return profile_; }
    void setKeyProfile(MvKeyProfile*);
    void moveUp(const QModelIndex& index);
    void moveDown(const QModelIndex& index);

    Qt::ItemFlags flags(const QModelIndex&) const override;

    Qt::DropActions supportedDropActions() const override;
    QStringList mimeTypes() const override;
    QMimeData* mimeData(const QModelIndexList&) const override;
    bool dropMimeData(const QMimeData* data,
                      Qt::DropAction action, int row, int column,
                      const QModelIndex& parent) override;

    void setEditable(bool b) { editable_ = b; }
    bool editable() { return editable_; }

    void setPixmapCache(MvQPixmapCache* p) { pixmaps_ = p; }


protected:
    virtual QString label(MvKey*, const int, const int, int) const;
    void moveRow(int, int);

    MvKeyProfile* profile_{nullptr};
    DisplayMode mode_{ProfileContentsMode};
    bool editable_{false};
    MvQPixmapCache* pixmaps_{nullptr};
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QDialog>
#include <QMap>
#include <QString>

class MvQAbout : public QDialog
{
public:
    enum Version
    {
        NoVersion = 0x0,
        MetviewVersion = 0x1,
        GribApiVersion = 0x2,
        EmosVersion = 0x4,
        MagicsVersion = 0x8,
        NetcdfVersion = 0x10,
        OdcVersion = 0x20,
        InterpolationPackage = 0x40
    };
    Q_DECLARE_FLAGS(Versions, Version);

    using VersionTextMap = QMap<Version, QString>;
    MvQAbout(QString, QString, Versions, VersionTextMap text = VersionTextMap());
};

Q_DECLARE_OPERATORS_FOR_FLAGS(MvQAbout::Versions);

/***************************** LICENSE START ***********************************

 Copyright 2018 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QColor>
#include <QStyledItemDelegate>

class MvQStyleDbItem;
class QTreeView;
class QTextEdit;
class QSplitter;

class MvQStyleDelegate : public QStyledItemDelegate
{
public:
    MvQStyleDelegate(QWidget* parent = 0);
    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const;
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const;

protected:
    QColor borderCol_;
    int textHeight_;
    int pixHeight_;
};


class MvQStyleTreeWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MvQStyleTreeWidget(QWidget* parent = 0);

    void setStyle(MvQStyleDbItem* item);
    QTreeView* tree() const { return tree_; }

protected slots:
    void slotCopyStyleName();

protected:
    void toClipboard(QString txt) const;

    QSplitter* splitter_;
    QTreeView* tree_;
    QTextEdit* te_;
};

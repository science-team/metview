/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QSettings>
#include <QWidget>

class QAction;
class QCheckBox;
class MvQLogBrowser;

class MvQLogPanel : public QWidget
{
    Q_OBJECT
public:
    explicit MvQLogPanel(QWidget* parent = nullptr);

    void setClearOptionButtonText(QString txt);
    void setViewAction(QAction*);
    void readSettings(QSettings&);
    void writeSettings(QSettings&);

public slots:
    void clearLog();
    void newMessageLoaded(int);

private slots:
    void closePanel();

private:
    MvQLogBrowser* logBrowser_;
    QAction* viewAction_;
    QCheckBox* clearMsgCb_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQStackedEditor.h"

#include <QDialogButtonBox>
#include <QHBoxLayout>
#include <QListWidget>
#include <QPushButton>
#include <QStackedWidget>
#include <QVBoxLayout>

MvQStackedEditor::MvQStackedEditor(QString title, QWidget* parent) :
    QDialog(parent)
{
    list_ = new QListWidget(this);
    // list_->setFlow(QListView::LeftToRight);
    // list_->setViewMode(QListView::IconMode);
    // list_->setIconSize(QSize(32, 32));
    // list_->setGridSize(QSize(48, 48));
    list_->setMovement(QListView::Static);
    list_->setMaximumWidth(150);
    // list_->setSpacing(5);

    page_ = new QStackedWidget;
    /*foreach(MvQPageDefinition* p,pageLst)
    {
            addPage(p->widget,p->icon,p->title);
    }

        list_->setCurrentRow(0);*/


    // Buttonbox
    auto* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    // QPushButton *closePb = new QPushButton(tr("Close"));
    // connect(closePb, SIGNAL(clicked()), this, SLOT(close()));

    // QHBoxLayout *horizontalLayout = new QHBoxLayout;
    // horizontalLayout->addWidget(list_);
    // horizontalLayout->addWidget(page_, 1);

    /*QHBoxLayout *buttonsLayout = new QHBoxLayout;
        buttonsLayout->addStretch(1);
        buttonsLayout->addWidget(closePb);*/

    auto* mainLayout = new QVBoxLayout;
    auto* hb = new QHBoxLayout;

    hb->addWidget(list_);
    hb->addWidget(page_, 1);
    // mainLayout->addStretch(1);
    // mainLayout->addSpacing(12);
    mainLayout->addLayout(hb);

    mainLayout->addWidget(buttonBox);
    setLayout(mainLayout);

    setWindowTitle(title);

    connect(list_, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)),
            this, SLOT(slotChangePage(QListWidgetItem*, QListWidgetItem*)));
}

void MvQStackedEditor::addPage(QWidget* w, QIcon /*icon*/, QString txt)
{
    auto* item = new QListWidgetItem(list_);
    // item->setIcon(icon);
    item->setText(txt);
    // item->setTextAlignment(Qt::AlignHCenter);
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

    page_->addWidget(w);
}

void MvQStackedEditor::slotChangePage(QListWidgetItem* current, QListWidgetItem* previous)
{
    if (!current)
        current = previous;

    page_->setCurrentIndex(list_->row(current));
}

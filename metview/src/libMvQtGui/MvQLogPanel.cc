/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQLogPanel.h"

#include <QAction>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QToolButton>
#include <QVBoxLayout>

#include "MvQLogBrowser.h"
#include "MvQPanel.h"

MvQLogPanel::MvQLogPanel(QWidget* parent) :
    QWidget(parent),
    viewAction_(nullptr)
{
    auto* logLayout = new QVBoxLayout;
    logLayout->setContentsMargins(0, 0, 0, 0);
    logLayout->setSpacing(1);

    // logPanel_=new QWidget(this);
    setMinimumHeight(110);
    setLayout(logLayout);

    auto headerW = new MvQPanel(this);
    auto* hb = new QHBoxLayout(headerW);
    hb->setContentsMargins(0, 0, 0, 0);
    logLayout->addWidget(headerW);

    // Label
    auto* label = new QLabel(tr("Log"), this);
    label->setProperty("panelStyle", "2");

    hb->addSpacing(4);
    hb->addWidget(label, 1);

    // Clear if new message button
    clearMsgCb_ = new QCheckBox(tr("Clear log on new message"));
    clearMsgCb_->setCheckState(Qt::Checked);
    clearMsgCb_->setProperty("panelStyle", "2");
    hb->addWidget(clearMsgCb_);

    // Clear button
    auto* clearTb = new QToolButton(this);
    clearTb->setAutoRaise(true);
    clearTb->setIcon(QPixmap(":/examiner/wastebasket_orange.svg"));
    clearTb->setProperty("panelStyle", "2");
    clearTb->setToolTip(tr("Clear log"));
    hb->addWidget(clearTb);

    auto* closeTb = new QToolButton(this);
    closeTb->setAutoRaise(true);
    closeTb->setIcon(QPixmap(":/examiner/panel_close.svg"));
    closeTb->setToolTip(tr("Close log panel"));
    closeTb->setProperty("panelStyle", "2");
    hb->addWidget(closeTb);

    // Log browser
    logBrowser_ = new MvQLogBrowser(this);
    logLayout->addWidget(logBrowser_);

    connect(clearTb, SIGNAL(clicked()),
            this, SLOT(clearLog()));

    connect(closeTb, SIGNAL(clicked()),
            this, SLOT(closePanel()));
}

void MvQLogPanel::setClearOptionButtonText(QString txt)
{
    clearMsgCb_->setText(txt);
}

void MvQLogPanel::setViewAction(QAction* ac)
{
    Q_ASSERT(ac);
    viewAction_ = ac;

    connect(viewAction_, SIGNAL(toggled(bool)),
            this, SLOT(setVisible(bool)));
}

void MvQLogPanel::clearLog()
{
    logBrowser_->clear();
}

void MvQLogPanel::closePanel()
{
    hide();
    if (viewAction_) {
        viewAction_->setChecked(false);
    }
}

void MvQLogPanel::newMessageLoaded(int)
{
    if (clearMsgCb_->isChecked())
        logBrowser_->clear();
}

void MvQLogPanel::writeSettings(QSettings& settings)
{
    settings.beginGroup("log");
    settings.setValue("clearLogForNewMsg", clearMsgCb_->isChecked());
    settings.endGroup();
}

void MvQLogPanel::readSettings(QSettings& settings)
{
    settings.beginGroup("log");
    if (settings.contains("clearLogForNewMsg")) {
        bool b = settings.value("clearLogForNewMsg").toBool();
        clearMsgCb_->setChecked(b);
    }
    settings.endGroup();
}

/***************************** LICENSE START ***********************************

 Copyright 2021 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "TransparencyWidget.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QSlider>
#include <QSpinBox>
#include <QToolButton>

TransparencyWidget::TransparencyWidget(QWidget* parent, QFont font) :
    QWidget(parent)
{
    auto hb = new QHBoxLayout(this);
    hb->setContentsMargins(0, 0, 0, 0);
    hb->setSpacing(1);

    slider_ = new QSlider(this);
    slider_->setOrientation(Qt::Horizontal);

    sp_ = new QSpinBox(this);

    label_ = new QLabel(this);
    label_->setFont(font);

    resetTb_ = new QToolButton(this);
    resetTb_->setAutoRaise(true);
    resetTb_->setIcon(QPixmap(":/edit/reset_line.svg"));
    resetTb_->setToolTip(tr("Reset transparency to 0"));

    hb->addWidget(slider_);
    hb->addWidget(sp_);
    hb->addWidget(label_);
    hb->addWidget(resetTb_);

    slider_->setMinimum(0);
    slider_->setMaximum(100);

    sp_->setMinimum(0);
    sp_->setMaximum(100);

    connect(slider_, SIGNAL(valueChanged(int)),
            this, SLOT(sliderChanged(int)));

    connect(sp_, SIGNAL(valueChanged(int)),
            this, SLOT(spChanged(int)));

    connect(resetTb_, SIGNAL(clicked()),
            this, SLOT(resetValue()));

    sp_->setVisible(useSp_);
    label_->setVisible(useLabel_);
}

void TransparencyWidget::setUseLabel(bool b)
{
    useLabel_ = b;
    label_->setVisible(b);
}

void TransparencyWidget::setUseSpin(bool b)
{
    useSp_ = b;
    sp_->setVisible(b);
}

void TransparencyWidget::sliderChanged(int value)
{
    if (!ignoreChange_) {
        ignoreChange_ = true;
        sp_->setValue(value);
        label_->setText(QString::number(value));
        Q_EMIT valueChanged(value);
        ignoreChange_ = false;
    }
}

void TransparencyWidget::sliderReleased()
{
    if (!ignoreChange_) {
        auto value = slider_->value();
        Q_EMIT valueChanged(value);
    }
}

void TransparencyWidget::spChanged(int value)
{
    if (!ignoreChange_) {
        slider_->setValue(value);
        label_->setText(QString::number(value));
        Q_EMIT valueChanged(value);
    }
}

void TransparencyWidget::resetValue()
{
    ignoreChange_ = true;
    int value = 0;
    sp_->setValue(value);
    slider_->setValue(value);
    label_->setText(QString::number(value));
    Q_EMIT valueChanged(0);
    ignoreChange_ = false;
}

int TransparencyWidget::value() const
{
    return slider_->value();
}

int TransparencyWidget::alphaValue() const
{
    auto v = static_cast<float>(100 - slider_->value());
    int r = static_cast<int>(v * 2.55);
    if (r < 0) {
        r = 0;
    }
    else if (r > 255) {
        r = 255;
    }
    return r;
}

int TransparencyWidget::trToAlpha(int tr) const
{
    auto v = static_cast<float>(100 - tr);
    int r = static_cast<int>(v * 2.55);
    if (r < 0) {
        r = 0;
    }
    else if (r > 255) {
        r = 255;
    }
    return r;
}

int TransparencyWidget::alphaToTr(int a) const
{
    float v = static_cast<float>(a) / 2.55;
    int r = static_cast<int>(v);
    if (r < 0) {
        r = 0;
    }
    else if (r > 100) {
        r = 100;
    }
    return 100 - r;
}

void TransparencyWidget::init(int value)
{
    if (value >= 0 && value <= 100) {
        ignoreChange_ = true;
        slider_->setValue(value);
        sp_->setValue(value);
        label_->setText(QString::number(value));
        ignoreChange_ = false;
    }
}

void TransparencyWidget::initAlpha(int a)
{
    init(alphaToTr(a));
}

/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QPlainTextEdit>

#include "MvQProperty.h"

class LineNumberArea;
class GotoLineDialog;

class PlainTextEdit : public QPlainTextEdit, public MvQPropertyObserver
{
    Q_OBJECT

public:
    explicit PlainTextEdit(QWidget* parent = nullptr);
    ~PlainTextEdit() override;

    void lineNumberAreaPaintEvent(QPaintEvent* event);
    int lineNumberAreaWidth();

    bool showLineNumbers() { return showLineNum_; }
    void setShowLineNumbers(bool b);

    void cursorRowCol(int* row, int* col);
    QChar characterBehindCursor(QTextCursor* cursor = nullptr);

    int numLinesSelected();
    bool findString(const QString&, QTextDocument::FindFlags, bool replace = false, const QString& r = emptyString_);

    void setFontProperty(MvQProperty* p);
    void updateFont();
    void notifyChange(MvQProperty* p) override;
    bool isHyperlinkEnabled() { return hyperlinkEnabled_; }
    void setHyperlinkEnabled(bool h);

public Q_SLOTS:
    void gotoLine();
    void slotZoomIn();
    void slotZoomOut();

private Q_SLOTS:
    void updateLineNumberAreaWidth(int newBlockCount);
    void updateLineNumberArea(const QRect&, int);
    void gotoLine(int line);

Q_SIGNALS:
    void focusRegained();
    void focusLost();
    void fontSizeChangedByWheel();
    void hyperlinkActivated(QString link);

protected:
    void resizeEvent(QResizeEvent* event) override;
    void focusInEvent(QFocusEvent* event) override;
    void focusOutEvent(QFocusEvent* event) override;
    void wheelEvent(QWheelEvent* event) override;
    void mousePressEvent(QMouseEvent* e) override;
    void mouseReleaseEvent(QMouseEvent* e) override;
    void mouseMoveEvent(QMouseEvent* e) override;

private:
    void fontSizeChangedByZoom();
    void setColours();

    bool showLineNum_;
    QWidget* lineNumArea_;
    int rightMargin_;
    bool hyperlinkEnabled_;
    QString lastFindString_;
    QString currentLink_;
    QTextDocument::FindFlags lastFindFlags_;
    GotoLineDialog* gotoLineDialog_;
    static QString emptyString_;

    QColor numAreaBgCol_;
    QColor numAreaFontCol_;
    QColor numAreaSeparatorCol_;
    QColor numAreaCurrentCol_;
    MvQProperty* fontProp_;
};


class LineNumberArea : public QWidget
{
public:
    explicit LineNumberArea(PlainTextEdit* editor) :
        QWidget(editor) { textEditor = editor; }
    QSize sizeHint() const override { return {textEditor->lineNumberAreaWidth(), 0}; }

protected:
    void paintEvent(QPaintEvent* event) override { textEditor->lineNumberAreaPaintEvent(event); }

private:
    PlainTextEdit* textEditor;
};

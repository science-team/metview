/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

#include "AbstractTextEditSearchInterface.h"

class QPlainTextEdit;

class PlainTextSearchInterface : public AbstractTextEditSearchInterface
{
public:
    PlainTextSearchInterface();
    void setEditor(QPlainTextEdit* e) { editor_ = e; }

    bool findString(QString str, bool highlightAll, QTextDocument::FindFlags findFlags,
                    QTextCursor::MoveOperation move, int iteration, StringMatchMode::Mode matchMode) override;

    void automaticSearchForKeywords(bool) override;
    void refreshSearch() override;
    void clearHighlights() override;
    void disableHighlights() override;
    void enableHighlights() override {}
    bool highlightsNeedSearch() override { return true; }
    void gotoLastLine() override;

protected:
    QPlainTextEdit* editor_;
};

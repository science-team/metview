/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQFileInfoLabel.h"
#include "MvQFileInfo.h"
#include "MvQTheme.h"
#include "MvQMethods.h"

MvQFileInfoLabel::MvQFileInfoLabel(QWidget* parent) :
    QLabel(parent)
{
    setProperty("type", "info");

    // Set size policy
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(this->sizePolicy().hasHeightForWidth());
    setSizePolicy(sizePolicy);

    setMargin(4);
    setAlignment(Qt::AlignLeft | Qt::AlignVCenter);

    // Other settings
    setAutoFillBackground(true);
    setTextInteractionFlags(Qt::LinksAccessibleByMouse | Qt::TextSelectableByKeyboard | Qt::TextSelectableByMouse);
}

QString MvQFileInfoLabel::buildTextLabel(QString fileName, QString fileExtraText)
{
    QString s;
    MvQFileInfo info(fileName);

    auto subCol = MvQTheme::subText();
    auto bold = MvQTheme::boldFileInfoTitle();
    s = MvQ::formatText("File: ", subCol, bold) + info.absoluteFilePath();

    if (!fileExtraText.isEmpty()) {
        s += " " + fileExtraText;
    }
    s += "<br>";

    if (info.isSymLink()) {
        s += MvQ::formatText("Symlink target: ", subCol) + info.symLinkTarget() + "<br>";
    }

    s += MvQ::formatText("Permissions: ", subCol, bold) + info.formatPermissions();
    s += MvQ::formatText(" Owner: ", subCol, bold) + info.owner();
    s += MvQ::formatText(" Group: ", subCol, bold) + info.group();
    s += MvQ::formatText(" Size: ", subCol, bold) + info.formatSize();
    s += MvQ::formatText(" Modified: ", subCol, bold) + info.formatModDate();

    return s;
}

void MvQFileInfoLabel::setTextLabel(QString fileName)
{
    QString s = buildTextLabel(fileName);
    setText(s);
}

void MvQFileInfoLabel::setGribTextLabel(QString fileName, int messageNum, bool filtered, int filteredMessageNum, bool hasMultiMessage)
{
    QString s = buildTextLabel(fileName);
    s += "<br>";

    auto subCol = MvQTheme::subText();
    auto accentCol = MvQTheme::accentText();
    auto bold = MvQTheme::boldFileInfoTitle();

    if (filtered) {
        s += MvQ::formatText("Number of filtered messages: ", subCol, bold) +
             QString::number(filteredMessageNum) +
             " (out of " + QString::number(messageNum) + ")";
    }
    else {
        s += MvQ::formatText("Total number of messages: ", subCol, bold) +
             QString::number(messageNum);

        if (hasMultiMessage) {
            s += MvQ::formatText(" (some messages contain multiple fields!) ", accentCol);
        }
    }
    setText(s);
}

void MvQFileInfoLabel::setBufrTextLabel(QString fileName, int messageNum, bool filtered, int filteredMessageNum, bool hasSubset)
{
    QString s = buildTextLabel(fileName);
    s += "<br>";

    auto subCol = MvQTheme::subText();
    auto accentCol = MvQTheme::accentText();
    auto bold = MvQTheme::boldFileInfoTitle();

    if (filtered) {
        s += MvQ::formatText("Number of filtered messages: ", subCol, bold) +
             MvQ::formatText(QString::number(filteredMessageNum) +
                                 " (out of " + QString::number(messageNum) + ")",
                             accentCol);
    }
    else {
        s += MvQ::formatText("Total number of messages: ", subCol, bold) +
             QString::number(messageNum);

        if (hasSubset) {
            s += MvQ::formatText(" (some messages contain subsets!) ", accentCol);
        }
    }
    setText(s);
}

void MvQFileInfoLabel::setOdbTextLabel(QString fileName, QString)
{
    QString s = buildTextLabel(fileName);
    setText(s);
}

void MvQFileInfoLabel::setGeopTextLabel(QString fileName, QString format, long itemNum)
{
    auto subCol = MvQTheme::subText();
    auto bold = MvQTheme::boldFileInfoTitle();
    QString s = buildTextLabel(fileName);
    s += "<br>";
    s += MvQ::formatText("Format: ", subCol, bold) + format + "<br>" +
         MvQ::formatText(" Total number of points: ", subCol, bold) + QString::number(itemNum);
    setText(s);
}

void MvQFileInfoLabel::setFlextraTextLabel(QString fileName, QString iconName, int itemNum)
{
    auto subCol = MvQTheme::subText();
    auto bold = MvQTheme::boldFileInfoTitle();
    QString s;
    if (!iconName.isEmpty()) {
        s = MvQ::formatText("Icon: ", subCol, bold) + iconName + "<br>";
    }
    s += buildTextLabel(fileName) + "<br>" +
         MvQ::formatText("Total number of groups: ", subCol, bold) + QString::number(itemNum);
    setText(s);
}

void MvQFileInfoLabel::setProfileTextLabel(QString fileName, int stepNum, int levNum, QString id)
{
    auto subCol = MvQTheme::subText();
    auto bold = MvQTheme::boldFileInfoTitle();
    QString s = buildTextLabel(fileName);
    s += "<br>";
    s += MvQ::formatText("Steps: ", subCol, bold) + QString::number(stepNum) +
         MvQ::formatText(" Model levels: ", subCol, bold) + QString::number(levNum) +
         MvQ::formatText(" Data type: ", subCol, bold) + id;
    setText(s);
}

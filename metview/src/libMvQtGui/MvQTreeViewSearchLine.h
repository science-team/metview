/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "AbstractSearchLine.h"

#include <QModelIndex>

class QTreeView;
class TreeModelMatchCollector;

class TreeViewSearchLine : public AbstractSearchLine
{
    Q_OBJECT

public:
    explicit TreeViewSearchLine(QWidget* parent);
    ~TreeViewSearchLine() override;
    void setView(QTreeView* view);
    void setColumns(QVector<int> columns);

public Q_SLOTS:
    void slotFind(QString) override;
    void slotFindNext() override;
    void slotFindPrev() override;
    void slotFindNext(bool) { slotFindNext(); }
    void slotFindPrev(bool) { slotFindPrev(); }
    void slotSortHappened(int, Qt::SortOrder);
    void slotUpdate();
    void slotUpdate(const QModelIndex&, const QModelIndex&);
    void matchModeChanged(int newIndex);
    void on_actionCaseSensitive__toggled(bool) override;
    void on_actionWholeWords__toggled(bool) override;
    void slotClose() override;

protected:
    void refreshSearch();
    void selectIndex(const QModelIndex& index);
    void clearRequested() {}

    QTreeView* view_{nullptr};
    QVector<int> columns_{0};
    int currentResultItem_{-1};
    TreeModelMatchCollector* collector_;
};

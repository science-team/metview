/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "StringMatchMode.h"

std::map<StringMatchMode::Mode, std::string> StringMatchMode::matchOper_;

StringMatchMode::StringMatchMode() :
    mode_(WildcardMatch)
{
    init();
}

StringMatchMode::StringMatchMode(Mode mode) :
    mode_(mode)
{
    init();
}

void StringMatchMode::init()
{
    if (matchOper_.empty()) {
        matchOper_[ContainsMatch] = "~";
        matchOper_[WildcardMatch] = "=";
        matchOper_[RegexpMatch] = "=~";
    }
}

const std::string& StringMatchMode::matchOperator() const
{
    static std::string emptyStr;
    auto it = matchOper_.find(mode_);
    if (it != matchOper_.end())
        return it->second;

    return emptyStr;
}

StringMatchMode::Mode StringMatchMode::operToMode(const std::string& op)
{
    for (auto& it : matchOper_) {
        if (op == it.second)
            return it.first;
    }
    return InvalidMatch;
}

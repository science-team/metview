/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQLineEdit.h"

#include <QtGlobal>
#include <QLabel>
#include <QResizeEvent>
#include <QStyle>
#include <QToolButton>

MvQLineEdit::MvQLineEdit(QWidget* parent) :
    QLineEdit(parent)
{
#if QT_VERSION >= QT_VERSION_CHECK(5, 2, 0)
    setClearButtonEnabled(true);
#endif
}

void MvQLineEdit::setDecoration(QPixmap)
{
}

void MvQLineEdit::adjustSize()
{
    int frame = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    QSize tbSize = clearTb_->sizeHint();
    setStyleSheet(QString("QLineEdit { padding-right: %1px; } ").arg(tbSize.width() + frame + 1));

    int leftWidth = 0;
    if (iconLabel_) {
        QSize icSize = iconLabel_->sizeHint();
        setStyleSheet(QString("QLineEdit { padding-left: %1px; } ").arg(icSize.width() + frame + 1));
        leftWidth = +icSize.width();
    }
    QSize minSize = minimumSizeHint();
    setMinimumSize(qMax(minSize.width(), leftWidth + tbSize.width() + frame * 2 + 2),
                   qMax(minSize.height(), tbSize.height() + frame * 2 + 2));
}


void MvQLineEdit::resizeEvent(QResizeEvent* e)
{
    QLineEdit::resizeEvent(e);
}


void MvQLineEdit::slotClear()
{
    clear();
    emit textCleared();
}

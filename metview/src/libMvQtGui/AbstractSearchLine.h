/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "ui_SearchLineWidget.h"

#include <QSettings>
#include <QString>
#include <QWidget>

class AbstractSearchLine : public QWidget, protected Ui::SearchLineWidget
{
    Q_OBJECT

public:
    explicit AbstractSearchLine(QWidget* parent = nullptr);
    ~AbstractSearchLine() override;
    virtual void clear();
    virtual bool isEmpty();
    void selectAll();
    void setConfirmSearch(bool);
    bool confirmSearch() const { return confirmSearch_; }
    QString confirmSearchText() const;
    void setCompleter(QSet<QString> coVals);

    bool caseSensitive() { return caseSensitive_; }
    bool wholeWords() { return wholeWords_; }
    bool highlightAll() { return highlightAll_; }

    void writeSettings(QSettings& settings);
    void readSettings(QSettings& settings);

public Q_SLOTS:
    virtual void slotFind(QString) = 0;
    virtual void slotFindNext() = 0;
    virtual void slotFindPrev() = 0;
    virtual void slotClose();
    virtual void on_actionCaseSensitive__toggled(bool);
    virtual void on_actionWholeWords__toggled(bool);
    virtual void on_actionHighlightAll__toggled(bool);

Q_SIGNALS:
    void visibilityChanged();

protected:
    void setColours();
    void updateButtons(bool);
    void toDefaultState();
    void hideEvent(QHideEvent* event) override;
    void showEvent(QShowEvent* event) override;
    void paintEvent(QPaintEvent*) override;

    bool status_;
    bool caseSensitive_;
    bool wholeWords_;
    bool highlightAll_;
    StringMatchMode matchMode_;

    QString oriSh_;
    QString notFoundSh_;

    bool confirmSearch_;
    bool useToolTip_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <string>
#include <map>

class StringMatchMode
{
public:
    enum Mode
    {
        InvalidMatch = -1,
        ContainsMatch = 0,
        WildcardMatch = 1,
        RegexpMatch = 2
    };

    StringMatchMode();
    StringMatchMode(Mode m);
    StringMatchMode(const StringMatchMode& r) { mode_ = r.mode_; }

    Mode mode() const { return mode_; }
    void setMode(Mode m) { mode_ = m; }
    const std::string& matchOperator() const;
    int toInt() const { return static_cast<int>(mode_); }

    static Mode operToMode(const std::string&);

private:
    void init();

    Mode mode_;
    static std::map<Mode, std::string> matchOper_;
};

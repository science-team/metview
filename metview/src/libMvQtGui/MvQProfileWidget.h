/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QSettings>
#include <QWidget>

class QLabel;
class QSpinBox;

class MvProfileData;
class MvProfileChange;

class MvQProfileView;
class MvQRangeWidget;


class MvQProfileWidget : public QWidget
{
    Q_OBJECT

public:
    MvQProfileWidget(QWidget* parent = 0);
    ~MvQProfileWidget();

    void setProfile(QList<MvProfileData*>, bool);
    void update(const MvProfileChange&);
    void reload();
    void readSettings(QSettings&);
    void writeSettings(QSettings&);

public slots:
    void slotLevelRange(int, int);
    void slotEditRadius(int);

protected:
    QLabel* profileLabel_;
    MvQProfileView* profileView_;
    QSpinBox* editRadiusSpin_;
    MvQRangeWidget* rangeWidget_;

    int startLevelIndex_;
    int endLevelIndex_;
    int editRadius_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QtWidgets/QApplication>
#include <QWidget>
#include <QStringList>

#include <MvRequest.h>
#include <MvProtocol.h>
#include <MvApplication.h>
#include <qsocketnotifier.h>

#include "MvQDragDrop.h"


class QMainWindow;

class MvQApplication : public QApplication, public MvApplication
{
    Q_OBJECT

public:
    MvQApplication(int& ac, char** av, const char* name = nullptr, QStringList resorces = {});
    ~MvQApplication();
    MvQApplication(const MvQApplication&) = delete;
    MvQApplication& operator=(const MvQApplication&) = delete;

    bool notify(QObject* receiver, QEvent* event) override;
    void registerToDesktop(QMainWindow* win);
    static void writePidToFile(const MvRequest& r);

public slots:
    void slotDataReceived(int);
    void processDropRequest(MvRequest*);

protected:
    virtual void messageCallback(svcid* id, request* r);
    void writeToUiLog(const std::string& msg, const std::string& details, MvLogLevel level, bool popup) override;

private:
    static void messageCallbackCore(svcid* id, request* r, void* obj);

    QSocketNotifier* sn_{nullptr};
    QMainWindow* win_{nullptr};
    char* desktopName_;
};

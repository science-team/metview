/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QList>
#include <QStringList>
#include <QComboBox>
#include <QStyleOptionViewItem>
#include <QStyledItemDelegate>

class MvQCheckBoxComboDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    MvQCheckBoxComboDelegate(QWidget* parent = 0);
    void paint(QPainter* painter, const QStyleOptionViewItem& option,
               const QModelIndex& index) const;
    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
                          const QModelIndex& index) const;
    void setEditorData(QWidget* editor, const QModelIndex& index) const;
    void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const;

    void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;

signals:
    void dataChanged() const;

public slots:
    void slotCheckBoxStateChanged(int);

private slots:
    void commmitAndCloseEditor();
};


class MvQCheckBoxCombo : public QComboBox
{
    Q_OBJECT;

public:
    MvQCheckBoxCombo(QWidget* widget = 0);
    virtual ~MvQCheckBoxCombo();

    bool eventFilter(QObject* object, QEvent* event);
    virtual void paintEvent(QPaintEvent*);

    // Get selected values
    QStringList getSelectedValues();

signals:
    void selectionChanged() const;
};

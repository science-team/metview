/***************************** LICENSE START ***********************************

 Copyright 2021 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <vector>

#include <QtGlobal>
#include <QAbstractItemModel>
#include <QSet>
#include <QSortFilterProxyModel>
#include <QStringList>
#include <QVector>
#include <QWidget>

class MvQTreeView;
class MvQComboLine;


class MvQFilterTreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    MvQFilterTreeModel(QObject* parent = 0);

    void load(QStringList headerData, QVector<QStringList> valueData, bool multi, int valueColumnIndex);

    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    int rowCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex& idx, const QVariant& value, int role);
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex&) const;

    Qt::ItemFlags flags(const QModelIndex& index) const;

    void updateValueChecked(QStringList);
    void queryValueChecked(QStringList&, QVector<bool>&) const;
    QModelIndex indexToValue(QString s) const;

signals:
    void valueCheckedChanged(QString, bool);
    void repaintRequested();

protected:
    bool hasData() const;

    bool multi_{true};
    QStringList headerData_;
    QVector<QStringList> valueData_;
    QVector<bool> checked_;
    int valueColumnIndex_{-1};
};

class MvQFilterTreeProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    MvQFilterTreeProxyModel(QObject* parent = 0);
    int filterColumn() const { return filterColumn_; }

public slots:
    void slotFilterColumnChanged(int index);
    void slotFilterTextChanged(QString);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const;

    int filterColumn_{0};
    QString filterText_;
};


class MvQFilterTreeWidget : public QWidget
{
    Q_OBJECT

public:
    MvQFilterTreeWidget(QStringList headerData, QVector<QStringList> valueData, int selectionColumnIndex, bool multi, QWidget* parent = 0);
    void updateValueChecked(QStringList);
    void queryValueChecked(QStringList&, QVector<bool>&) const;
    void setCurrentValue(QString);
    void adjustHeight();

protected slots:
    void slotSelected(const QModelIndex&);
    void slotReqpaintTree();

signals:
    void selected(QString);
    void valueCheckedChanged(QString, bool);

protected:
    MvQFilterTreeModel* model_;
    MvQFilterTreeProxyModel* sortModel_;
    MvQTreeView* tree_;
    MvQComboLine* filterCb_;
    bool multi_{false};
    int selectionColumnIndex_{-1};
};

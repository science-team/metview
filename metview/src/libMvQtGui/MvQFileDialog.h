/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QFileSystemModel>
#include <QFileIconProvider>
#include <QFileDialog>
#include <QFileInfo>
#include <QModelIndex>
#include <QSortFilterProxyModel>
#include <QString>


class MvQPixmapCache;

class MvQFileIconProvider : public QFileIconProvider
{
public:
    MvQFileIconProvider();
    QIcon icon(const QFileInfo&) const override;

private:
    QString path_;
    MvQPixmapCache* pixCache_{nullptr};
    QString mvHome_;
};


class MvQFileDialogFilterModel : public QSortFilterProxyModel
{
public:
    MvQFileDialogFilterModel(QObject* parent = nullptr);
    bool filterAcceptsRow(int, const QModelIndex&) const override;
    bool lessThan(const QModelIndex&, const QModelIndex&) const override;
};

class MvQFileDialog : public QFileDialog
{
public:
    MvQFileDialog(QString, QString, QWidget* parent = nullptr);
    ~MvQFileDialog() override;

    static void updateSidebar(QFileDialog* dg, QString startDir);
    static void initTargetFile(QFileDialog*, QString startDir);
    static void initTargetDir(QFileDialog*, QString startDir);
    static void writeLastGlobalSavedFile(QFileDialog* dg);

protected:
    static bool useTargetDirFromConfig();
    static QString readLastGlobalSavedFile();
    void readSettings();
    void writeSettings();
};

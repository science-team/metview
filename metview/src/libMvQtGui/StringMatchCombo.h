/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QComboBox>
#include <QToolButton>

#include "StringMatchMode.h"

class StringMatchTb : public QToolButton
{
public:
    StringMatchTb(QWidget* parent = nullptr);
};

class StringMatchCombo : public QComboBox
{
public:
    explicit StringMatchCombo(QWidget* parent = nullptr);

    StringMatchMode::Mode matchMode(int) const;
    StringMatchMode::Mode currentMatchMode() const;
    void setMatchMode(const StringMatchMode& mode);
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "GotoLineDialog.h"

#include <QPushButton>

GotoLineDialog::GotoLineDialog(QWidget* parent) :
    QDialog(parent)
{
    setupUi(this);  // this sets up GUI// setupFileMenu();


    connect(buttonBox, SIGNAL(accepted()), this, SLOT(slotDone()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(lineEdit, SIGNAL(textChanged(const QString&)), this, SLOT(setButtonStatus()));
}


GotoLineDialog::~GotoLineDialog() = default;


// ---------------------------------------------------------------------------
// GotoLineDialog::setButtonStatus
// if there is text in the input box, then we can activate the 'OK' button,
// otherwise we should disable it. This function is called each time the text
// in the box is changed.
// ---------------------------------------------------------------------------

void GotoLineDialog::setButtonStatus()
{
    QPushButton* okButton = buttonBox->button(QDialogButtonBox::Ok);

    if (lineEdit->text().isEmpty()) {
        okButton->setEnabled(false);
    }
    else {
        okButton->setEnabled(true);
    }
}


// ---------------------------------------------------------------------------
// GotoLineDialog::setupUIBeforeShow
// sets up UI elements before the dialog is displayed.
// ---------------------------------------------------------------------------

void GotoLineDialog::setupUIBeforeShow()
{
    lineEdit->setFocus(Qt::OtherFocusReason);
    buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);

    setButtonStatus();
}


// ---------------------------------------------------------------------------
// GotoLineDialog::accept
// called when the user clicks the 'OK' button - emits a signal to tell the
// text editor to go to the chosen line
// ---------------------------------------------------------------------------

void GotoLineDialog::slotDone()
{
    int line = lineEdit->text().toInt();
    Q_EMIT gotoLine(line);
    close();
}

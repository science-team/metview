/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "PlainTextSearchLine.h"
#include "PlainTextSearchInterface.h"

#include <cassert>

PlainTextSearchLine::PlainTextSearchLine(QWidget* parent) :
    TextEditSearchLine(parent)
{
    interface_ = new PlainTextSearchInterface;
    TextEditSearchLine::setSearchInterface(interface_);
}

PlainTextSearchLine::~PlainTextSearchLine()
{
    delete interface_;
}

void PlainTextSearchLine::setEditor(QPlainTextEdit* e)
{
    auto* pti = static_cast<PlainTextSearchInterface*>(interface_);
    assert(pti);
    pti->setEditor(e);
}

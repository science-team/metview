/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QDialog>
#include <QIcon>

class QListWidget;
class QListWidgetItem;
class QStackedWidget;

class MvQStackedEditor : public QDialog
{
    Q_OBJECT

public:
    MvQStackedEditor(QString, QWidget* parent = nullptr);

public slots:
    void slotChangePage(QListWidgetItem* current, QListWidgetItem* previous);

    void addPage(QWidget*, QIcon, QString);

protected:
    QListWidget* list_;
    QStackedWidget* page_;
};

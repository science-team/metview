/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQTextDialog.h"

#include <QApplication>
#include <QDialogButtonBox>
#include <QLabel>
#include <QPushButton>
#include <QStyle>
#include <QTextEdit>
#include <QVBoxLayout>

MvQTextDialog::MvQTextDialog(QString title, QString labelText, QString customOkLabel, QWidget* parent) :
    QDialog(parent)
{
    edit_ = new QTextEdit(this);

    auto* label = new QLabel(labelText);

    auto* buttonBox = new QDialogButtonBox;

    if (!customOkLabel.isEmpty()) {
        auto* okPb = new QPushButton(customOkLabel);
        okPb->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogOkButton));

        buttonBox->addButton(okPb, QDialogButtonBox::AcceptRole);
    }
    else {
        buttonBox->addButton(QDialogButtonBox::Ok);
    }
    buttonBox->addButton(QDialogButtonBox::Cancel);

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    auto* mainLayout = new QVBoxLayout;
    mainLayout->addWidget(label);
    mainLayout->addWidget(edit_);
    mainLayout->addWidget(buttonBox);
    setLayout(mainLayout);

    setWindowTitle(title);
}

QString MvQTextDialog::text()
{
    return edit_->toPlainText();
}

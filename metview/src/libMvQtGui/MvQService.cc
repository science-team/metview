/***************************** LICENSE START ***********************************

 Copyright 2015 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QDebug>
#include <QSocketNotifier>
#include <QCoreApplication>
#include <QMainWindow>

#include "MvQService.h"
#include "MvApplication.h"

#define MVQSERVICE_DEBUG__

MvQService::MvQService(const char* name) :
    MvService(name),
    socketNotifier_(nullptr)
{
    saveToPool(false);  // interactive modules are not caching data (always true?)
}

MvQService::~MvQService() = default;

// The MARS event loop is not running in the forked MvService instances. Using this function
// we are able to receive data from Event and process it accordingly.
void MvQService::setupSocketNotifier()
{
    if (socketNotifier_)
        return;

    // Create socket notifier
    // The dataReceived function will be called everytime
    // there is data available in the socket
    socketNotifier_ = new QSocketNotifier(Id->s->soc, QSocketNotifier::Read, this);
    QObject::connect(socketNotifier_, SIGNAL(activated(int)),
                     this, SLOT(slotDataReceived(int)));
}

void MvQService::slotDataReceived(int)
{
    process_service(Id->s);
}

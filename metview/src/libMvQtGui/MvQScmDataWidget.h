/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QTabWidget>
#include <QWidget>
#include <QModelIndex>

class QAction;
class QHeaderView;
class QPushButton;
class QTableView;
class QStandardItemModel;
class QVBoxLayout;

class MvQScmDataModel;
class MvQScmProfileModel;
class MvQScmProfileFilterModel;
class MvQScmSurfaceModel;

class MvScm;
class MvScmVar;
class MvScmProfileChange;


class MvQScmTab : public QTabWidget
{
public:
    MvQScmTab(QWidget* parent = 0);
};

class MvQScmDataPanel : public QWidget
{
    Q_OBJECT
public:
    MvQScmDataPanel(QWidget* parent = 0);
    virtual ~MvQScmDataPanel() = default;

    void setMasterParam(MvScmVar*, MvScmVar*);

    void setStep(int);
    void reselectIndex(bool);
    bool selectParam(MvScmVar*);
    void showEditableOnly(bool);
    bool update(const MvScmProfileChange&);
    void reload();

    virtual void init(const std::vector<MvScmVar*>&, int, const std::vector<float>&, const std::string&,
                      const std::vector<MvScmVar*>&) {}
    virtual void init(const std::vector<MvScmVar*>&, int, const std::vector<MvScmVar*>&) {}

signals:
    void paramSelected(MvScmVar*, int);
    void stepChanged(int);
    void dataEdited(const MvScmProfileChange&);
    void selectParamFromMasterPanel();

public slots:
    void slotParamSelected(int);
    void slotParamSelected(const QModelIndex&);

protected:
    std::vector<MvScmVar*> vars_;
    QTableView* view_;
    MvQScmDataModel* model_;
    MvQScmProfileFilterModel* filterModel_;
    QModelIndex prevIndex_;
    std::map<MvScmVar*, MvScmVar*> masterParams_;
    bool master_;
};

class MvQScmProfileDataPanel : public MvQScmDataPanel
{
    Q_OBJECT

public:
    MvQScmProfileDataPanel(QWidget* parent = 0);
    void init(const std::vector<MvScmVar*>&, int, const std::vector<float>&, const std::string&,
              const std::vector<MvScmVar*>&);

public slots:
    void slotHeaderContextMenu(const QPoint&);

protected:
    void assignValue(MvScmVar*);
    void assignValue(MvScmVar*, double);

    QHeaderView* header_;
    std::vector<MvScmVar*> editableVars_;
};

class MvQScmSurfaceDataPanel : public MvQScmDataPanel
{
public:
    MvQScmSurfaceDataPanel(QWidget* parent = 0);
    void init(const std::vector<MvScmVar*>&, int, const std::vector<MvScmVar*>&);
};


class MvQScmDataWidget : public QWidget
{
    Q_OBJECT

public:
    MvQScmDataWidget(QWidget* parent = 0);
    ~MvQScmDataWidget();
    void init(MvScm*, QList<MvScmVar*>);
    void update(const MvScmProfileChange&);
    void reload();
    void showEditableParams(bool);
    void selectMlParam(MvScmVar*);

public slots:
    void slotStepChanged(int);
    void slotTabIndexChanged(int);

signals:
    void dataEdited(const MvScmProfileChange&);
    void paramSelected(MvScmVar*, int);
    void stepChanged(int);

protected:
    enum PanelType
    {
        ModelLevPanel = 0,
        PressureLevPanel = 1,
        SoilLevPanel = 2,
        SurfaceLevPanel = 3
    };

    void clearPanels();
    void addPanelToTab(MvQScmDataPanel*, MvQScmDataWidget::PanelType, QString);
    void readSettings();
    void writeSettings();

    MvScm* data_;
    QTabWidget* tab_;
    std::map<PanelType, MvQScmDataPanel*> panels_;
    MvQScmDataPanel* masterPanel_;
    std::map<PanelType, int> panelToTab_;
    std::map<int, PanelType> tabToPanel_;
};

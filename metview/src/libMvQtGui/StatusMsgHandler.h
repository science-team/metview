
/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QElapsedTimer>
#include <QString>
#include <QTime>

class QLabel;

class StatusMsgHandler
{
public:
    ~StatusMsgHandler() { clear(); }
    static StatusMsgHandler* instance();

    void init(QLabel* label);
    void show(QString, bool replace = false);
    void done(QString);
    void failed(QString);
    void task(QString);
    void failed();
    void done();
    void clear();

protected:
    StatusMsgHandler();

private:
    static StatusMsgHandler* instance_;

    QLabel* label_{nullptr};
    QElapsedTimer timer_;
    const int period_{3000};
};

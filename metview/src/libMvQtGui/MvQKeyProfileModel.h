/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QAbstractItemModel>
#include <QList>
#include <QPair>
#include <QSortFilterProxyModel>
#include <QStringList>

class GribMetaData;

#include "MvKeyProfile.h"

using MvQKeyFilter = QMap<QString, QStringList>;

class MvQKeyProfileSortFilterModel : public QSortFilterProxyModel
{
public:
    MvQKeyProfileSortFilterModel(QObject* parent = nullptr);
    void setColumnFilter(QStringList);
    void setColumnFilter(QList<int>);
    // bool hasColumnFilter() const;

private:
    enum FilterMode
    {
        StringFilter,
        PositionFilter
    };
    // bool lessThan ( const QModelIndex &, const QModelIndex &) const;
    bool filterAcceptsColumn(int source_column, const QModelIndex& source_parent) const override;

    FilterMode filterMode_{StringFilter};
    QStringList filterStr_;
    QList<int> filterPos_;
};

class MvQKeyProfileModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    MvQKeyProfileModel(QObject* parent);
    ~MvQKeyProfileModel() override;

    enum CustomItemRole
    {
        SortRole = Qt::UserRole + 1,
        ErrorRole = Qt::UserRole + 2,
        SubTextRole = Qt::UserRole + 3
    };

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    void keyProfileIsAboutToChange();
    void keyProfileChangeFinished();

    MvKeyProfile* keyProfile() const { return profile_; }
    void setKeyProfile(MvKeyProfile*);
    void reloadKeyProfile();
    virtual void clear();

    MvQKeyFilter keyFilter() const { return filter_; }
    void setKeyFilter(MvQKeyFilter);

    Qt::ItemFlags flags(const QModelIndex&) const override;
    Qt::DropActions supportedDropActions() const override;
    QStringList mimeTypes() const override;
    QMimeData* mimeData(const QModelIndexList&) const override;
    bool dropMimeData(const QMimeData* data,
                      Qt::DropAction action, int row, int column,
                      const QModelIndex& parent) override;
    void setShadeFirstColumn(bool b) { shadeFirstColumn_ = b; }

signals:
    void keysInserted(QList<MvKey*>, int);

protected:
    QString label(const int, const int) const;
    bool isPossibleStringKey(const std::string& name) const;
    void loadKeyFilter();

    bool isDataSet() const;
    MvKeyProfile* profile_{nullptr};
    MvQKeyFilter filter_;
    QList<bool> messageFilterStatus_;
    bool shadeFirstColumn_{false};
    QStringList possibleStringKeys_;
};

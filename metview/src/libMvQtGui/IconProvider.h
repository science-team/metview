/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QPixmap>

#include <map>

class IconItem
{
public:
    explicit IconItem(QString);
    virtual ~IconItem() = default;

    QPixmap pixmap(int);
    int id() const { return id_; }
    QString path() const { return path_; }

protected:
    static void greyOut(QImage&);
    virtual QPixmap unknown(int);

    QString path_;
    std::map<int, QPixmap> pixmaps_;
    int id_;
};

class UnknownIconItem : public IconItem
{
public:
    explicit UnknownIconItem(QString);

protected:
    QPixmap unknown(int) override;
};

class FailedIconItem : public IconItem
{
public:
    explicit FailedIconItem(QString);

protected:
    QPixmap unknown(int) override;
};


class IconProvider
{
public:
    IconProvider();

    static int add(QString path, QString name);

    static QString path(int id);
    static QPixmap pixmap(QString name, int size);
    static QPixmap pixmap(int id, int size);

    static QPixmap lockPixmap(int);
    static QPixmap failedPixmap();

private:
    static IconItem* icon(QString name);
    static IconItem* icon(int id);

    static std::map<QString, IconItem*> icons_;
    static std::map<int, IconItem*> iconsById_;
};

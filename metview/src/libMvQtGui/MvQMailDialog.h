/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QDialog>
#include <string>

class QLineEdit;
class QPlainTextEdit;

class MvQMailDialog : public QDialog
{
    Q_OBJECT

public:
    MvQMailDialog(const std::string&, const std::string&);
    QString to() const;
    QString cc() const;
    QString subject() const;
    QString message() const;

public slots:
    void accept();
    void reject();

protected:
    void closeEvent(QCloseEvent*);
    QString formatFileSize(qint64 size) const;
    void readSettings();
    void writeSettings();

    QLineEdit* toLe_;
    QLineEdit* ccLe_;
    QLineEdit* subjLe_;
    QPlainTextEdit* msgTe_;
    QStringList toLst_;
};

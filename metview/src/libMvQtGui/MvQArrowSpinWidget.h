/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QWidget>

class QLabel;
class QToolButton;
class QSpinBox;

class MvQArrowSpinWidget : public QWidget
{
    Q_OBJECT

    // Indexing starts from 1

public:
    explicit MvQArrowSpinWidget(QWidget* parent = nullptr);
    void clear(bool broadcast);
    void reset(int maxVal, bool broadcast);
    int value() const;
    void setValue(int val, bool broadcast);
    QSpinBox* spin() const { return spin_; }

protected slots:
    void slotSpinChanged(int);
    void slotPrev(bool);
    void slotNext(bool);

signals:
    void valueChanged(int);

private:
    void checkState();

    QToolButton* nextTb_{nullptr};
    QToolButton* prevTb_{nullptr};
    QSpinBox* spin_{nullptr};
    bool broadcast_{true};
};

/***************************** LICENSE START ***********************************

 Copyright 2020 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QLabel>
#include <QPixmap>

class QTextDocument;

class CustomLabel : public QWidget
{
public:
    CustomLabel(QWidget* parent = nullptr) :
        QWidget(parent) {}
    void setPixmap(QPixmap);
    void setText(QString);
    void setTextAndPixmap(QString, QPixmap);
    void clear();

protected:
    enum Mode
    {
        TextMode,
        PixmapMode,
        TextAndPixmapMode
    };
    void paintEvent(QPaintEvent*);
    void resizeEvent(QResizeEvent*);
    void makePix();
    void makeDoc();

    Mode mode_{TextMode};
    QPixmap pixOri_;
    QPixmap pix_;
    QString txt_;
    QTextDocument* doc_{nullptr};
};

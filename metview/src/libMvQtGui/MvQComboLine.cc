/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQComboLine.h"

#include <QtGlobal>
#include <QDebug>
#include <QComboBox>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QStyle>

#if QT_VERSION >= QT_VERSION_CHECK(5, 1, 0)
#include <QRegularExpressionValidator>
#else
#include <QRegExpValidator>
#endif

MvQComboLine::MvQComboLine(QStringList lst, QWidget* parent) :
    QWidget(parent),
    validator_(nullptr)
{
    auto* layout = new QHBoxLayout(this);
    layout->setSpacing(0);
    layout->setContentsMargins(1, 1, 1, 1);

    combo_ = new QComboBox(this);
    layout->addWidget(combo_);

    combo_->addItems(lst);

    connect(combo_, SIGNAL(currentIndexChanged(int)),
            this, SLOT(slotCurrentIndexChanged(int)));

    line_ = new QLineEdit(this);
    line_->setPlaceholderText("Filter ...");

#if QT_VERSION >= QT_VERSION_CHECK(5, 2, 0)
    line_->setClearButtonEnabled(true);
#endif

    layout->addWidget(line_);


    // line_->setStyleSheet(QString("QLineEdit {background: url(:/desktop/filter.svg); background-position: right; background-repeat: no-repeat} "));

    connect(line_, SIGNAL(textChanged(QString)),
            this, SIGNAL(textChanged(QString)));
}

void MvQComboLine::setItems(QStringList lst)
{
    combo_->clear();
    combo_->addItems(lst);
}

#if QT_VERSION >= QT_VERSION_CHECK(5, 1, 0)
void MvQComboLine::setRegExps(QList<QRegularExpression> lst)
{
    if (lst.count() == combo_->count())
        regExps_ = lst;
    else
        return;

    if (!validator_) {
        validator_ = new QRegularExpressionValidator(this);
        line_->setValidator(validator_);
    }

    if (combo_->currentIndex() != -1)
        validator_->setRegularExpression(regExps_.at(combo_->currentIndex()));
}
#else
void MvQComboLine::setRegExps(QList<QRegExp> lst)
{
    if (lst.count() == combo_->count())
        regExps_ = lst;
    else
        return;

    if (!validator_) {
        validator_ = new QRegExpValidator(this);
        line_->setValidator(validator_);
    }

    if (combo_->currentIndex() != -1)
        validator_->setRegExp(regExps_.at(combo_->currentIndex()));
}
#endif

void MvQComboLine::setToolTips(QStringList lst)
{
    if (lst.count() == combo_->count())
        toolTips_ = lst;
    else
        return;

    if (combo_->currentIndex() != -1)
        line_->setToolTip(toolTips_.at(combo_->currentIndex()));
}

void MvQComboLine::setCurrentIndex(int index)
{
    combo_->setCurrentIndex(index);
}

void MvQComboLine::slotCurrentIndexChanged(int index)
{
    if (validator_ && index != -1)
#if QT_VERSION >= QT_VERSION_CHECK(5, 1, 0)
        validator_->setRegularExpression(regExps_.at(index));
#else
        validator_->setRegExp(regExps_.at(index));
#endif

    if (toolTips_.count() == combo_->count())
        line_->setToolTip(toolTips_.at(index));

    emit currentIndexChanged(index);
}

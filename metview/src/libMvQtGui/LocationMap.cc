/***************************** LICENSE START ***********************************

 Copyright 2017 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END ***********************************/

#include "LocationMap.h"

#include <QDebug>

LocationMap::LocationMap(QWidget* parent) :
    QWidget(parent),
    ignoreSlider_(false)
{
    setupUi(this);

    zoomSlider_->setMinimum(view_->minZoomLevel());
    zoomSlider_->setMaximum(view_->maxZoomLevel());
    zoomSlider_->setSingleStep(1);

    // init
    zoomLevelChangedInView(view_->zoomLevel());

    connect(view_, SIGNAL(zoomLevelChanged(int)),
            this, SLOT(zoomLevelChangedInView(int)));

    checkButtonState();
}

LocationView* LocationMap::view()
{
    return view_;
}

void LocationMap::on_fullMapTb__clicked()
{
    view_->showFullMap();
}

void LocationMap::on_dataZoomTb__clicked()
{
    view_->zoomToData();
}

void LocationMap::on_zoomInTb__clicked()
{
    view_->zoomIn();
}

void LocationMap::on_zoomOutTb__clicked()
{
    view_->zoomOut();
}

void LocationMap::on_zoomSlider__valueChanged(int val)
{
    if (!ignoreSlider_) {
        view_->setZoomLevel(val);
        checkButtonState();
    }
}

void LocationMap::zoomLevelChangedInView(int level)
{
    if (zoomSlider_->value() != level) {
        ignoreSlider_ = true;
        zoomSlider_->setValue(level);
        checkButtonState();
        ignoreSlider_ = false;
    }
}

void LocationMap::checkButtonState()
{
    bool in = true, out = true;
    if (zoomSlider_->value() == zoomSlider_->maximum()) {
        in = false;
    }
    if (zoomSlider_->value() == zoomSlider_->minimum()) {
        out = false;
    }
    zoomInTb_->setEnabled(in);
    zoomOutTb_->setEnabled(out);
}

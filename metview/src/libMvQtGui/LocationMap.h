/***************************** LICENSE START ***********************************

 Copyright 2017 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "ui_LocationMap.h"

#include <QWidget>

class LocationMap;

class LocationMap : public QWidget, protected Ui::LocationMap
{
    Q_OBJECT

public:
    explicit LocationMap(QWidget* parent = nullptr);
    ~LocationMap() override = default;

    LocationView* view();

protected Q_SLOTS:
    void on_fullMapTb__clicked();
    void on_dataZoomTb__clicked();
    void on_zoomInTb__clicked();
    void on_zoomOutTb__clicked();
    void on_zoomSlider__valueChanged(int val);
    void zoomLevelChangedInView(int level);

protected:
    void checkButtonState();

    bool ignoreSlider_;
};

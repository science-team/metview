/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QStackedWidget>
#include <QMap>

//#include "VFile.hpp"

class Highlighter;
class MessageLabel;
class TextEditSearchLine;
class PlainTextEdit;
class PlainTextSearchInterface;
class TextPagerWidget;
class TextPagerSearchInterface;
class MvQProperty;

class OutputBrowser;

class CursorCacheItem
{
    friend class OutputBrowser;

public:
    CursorCacheItem() :
        pos_(0),
        verticalScrollValue_(0),
        viewportHeight_(0) {}

protected:
    int pos_;
    int verticalScrollValue_;
    int viewportHeight_;
};


class OutputBrowser : public QWidget
{
    Q_OBJECT

public:
    explicit OutputBrowser(QWidget* parent);
    ~OutputBrowser() override;

    void clear();
    // void loadFile(VFile_ptr file);
    void loadText(QString text, QString fileName, bool resetFile = true);
    void adjustHighlighter(QString fileName);
    void setFontProperty(MvQProperty* p);
    void updateFont();
    void gotoLine();
    void showSearchLine();
    void searchOnReload(bool userClickedReload);
    void zoomIn();
    void zoomOut();
    void clearCursorCache() { cursorCache_.clear(); }

protected Q_SLOTS:
    void showConfirmSearchLabel();

private:
    enum IndexType
    {
        BasicIndex = 0,
        PagerIndex = 1
    };
    void changeIndex(IndexType indexType, qint64 fileSize);
    bool isJobFile(QString fileName);
    void loadFile(QString fileName);
    void updateCursorFromCache(const std::string&);
    void setCursorPos(qint64 pos);

    QStackedWidget* stacked_;
    PlainTextEdit* textEdit_;
    TextPagerWidget* textPager_;
    TextEditSearchLine* searchLine_;
#if 0
    Highlighter* jobHighlighter_;
#endif
    PlainTextSearchInterface* textEditSearchInterface_;
    TextPagerSearchInterface* textPagerSearchInterface_;
    MessageLabel* confirmSearchLabel_;

    // we keep a reference to it to make sure that it does not get deleted while
    // it is being displayed
#if 0
    VFile_ptr file_;
#endif
    int lastPos_;
    std::string currentSourceFile_;

    QMap<std::string, CursorCacheItem> cursorCache_;

    static int minPagerTextSize_;
    static int minPagerSparseSize_;
    static int minConfirmSearchSize_;
};

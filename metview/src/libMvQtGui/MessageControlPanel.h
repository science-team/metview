/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvQPanel.h"


class QHBoxLayout;
class QLabel;
class QToolButton;
class MvQArrowSpinWidget;

class MessageControlPanel : public MvQPanel
{
    Q_OBJECT

public:
    MessageControlPanel(bool hasSubset, bool hasFilter, QWidget* parent = nullptr);
    void setMessageValue(int num, bool b);
    void resetMessageNum(int num, bool b);
    void resetSubsetNum(int num, bool b);
    void setCurrentSubset(int currentSubset);
    void setSubsetLabel(QString txt);
    void setSubsetTotalLabel(int msgNum, int subsetNum);
    void showSubsetControl(bool b);
    int messageValue() const;
    int subsetValue() const;
    void setFilterInfo(bool filtered, int oriNum, int num);
    void addOptionsAction(QAction*);
    void addButtonAction(QAction*);

signals:
    void messageChanged(int);
    void subsetChanged(int);

private:
    QHBoxLayout* layout_{nullptr};
    MvQArrowSpinWidget* messageSpin_{nullptr};
    QLabel* messageLabel_{nullptr};
    MvQArrowSpinWidget* subsetSpin_{nullptr};
    QLabel* subsetLabel_{nullptr};
    QLabel* subsetTitleLabel_{nullptr};
    QLabel* totalLabel_{nullptr};
    QToolButton* optionsTb_{nullptr};
    QList<QAction*> optionsActions_;
    QList<QAction*> buttonActions_;
};

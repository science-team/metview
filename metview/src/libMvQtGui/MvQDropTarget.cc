/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQDropTarget.h"

//#include <QDebug>
#include <QPainter>
#include "MvQMethods.h"


MvQDropTarget* MvQDropTarget::instance_ = nullptr;

MvQDropTarget* MvQDropTarget::Instance()
{
    if (!instance_)
        instance_ = new MvQDropTarget;
    return instance_;
}

MvQDropTarget::MvQDropTarget() :
    QLabel(nullptr),
    move_(true),
    xPadding_(4),
    yPadding_(4)
{
    setWindowFlags(Qt::FramelessWindowHint | Qt::X11BypassWindowManagerHint);
    setFocusPolicy(Qt::NoFocus);

    QLinearGradient hoverGrad;
    hoverGrad.setCoordinateMode(QGradient::ObjectBoundingMode);
    hoverGrad.setStart(0, 0);
    hoverGrad.setFinalStop(0, 1);
    hoverGrad.setColorAt(0, QColor(254, 254, 254));
    hoverGrad.setColorAt(1, QColor(240, 240, 240));
    bgBrush_ = QBrush(hoverGrad);
    bgPen_ = QPen(QColor(127, 154, 191));

    QFontMetrics fm(font_);

    int size = fm.height();
    decorPix_ = QPixmap::fromImage(QPixmap(":/desktop/arrow_drop_target.svg").toImage().scaled(QSize(size, size), Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
}

void MvQDropTarget::reset(QString target, bool move, QString prefix)
{
    if (pix_.isNull() || target_ != target || move_ != move) {
        target_ = target;
        move_ = move;
        text_ = (move_) ? "Move to " : "Copy to ";
        createPixmap(prefix);
        setPixmap(pix_);
    }

    show();
}

void MvQDropTarget::reset(QString target, QString text, QString prefix)
{
    if (pix_.isNull() || target_ != target || text_ != text) {
        target_ = target;
        text_ = text;
        createPixmap(prefix);
        setPixmap(pix_);
    }

    show();
}

void MvQDropTarget::createPixmap(QString prefix)
{
    QString text1 = text_;
    QString text2 = target_;

    if (!prefix.isEmpty())
        text1 += prefix + " ";

    int textOffset = decorPix_.width() + 4;

    QFontMetrics fm(font_);
    QRect textRect(0, 0, MvQ::textWidth(fm, text1 + text2), fm.height());
    QRect rect(0, 0, 2 * xPadding_ + textOffset + textRect.width(), 2 * yPadding_ + textRect.height());

    pix_ = QPixmap(rect.size());

    QPainter painter(&pix_);

    painter.setPen(bgPen_);
    painter.setBrush(bgBrush_);
    // painter.drawRoundedRect(rect,2,2);
    painter.drawRect(rect.adjusted(0, 0, -1, -1));
    painter.setBrush(Qt::NoBrush);

    painter.drawPixmap(QPointF(xPadding_, yPadding_), decorPix_);

    QRect tRect(xPadding_ + textOffset, yPadding_, MvQ::textWidth(fm, text1), fm.height());
    painter.setPen(Qt::blue);
    painter.drawText(tRect, Qt::AlignLeft, text1);

    tRect = QRect(xPadding_ + textOffset + MvQ::textWidth(fm, text1), yPadding_, MvQ::textWidth(fm, text2), fm.height());
    painter.setPen(Qt::black);
    painter.drawText(tRect, Qt::AlignLeft, text2);

    //
    resize(pix_.size());
}

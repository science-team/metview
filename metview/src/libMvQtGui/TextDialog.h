/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "ui_TextDialog.h"


class TextDialog : public QDialog, private Ui::TextDialog
{
    Q_OBJECT

public:
    explicit TextDialog(QWidget* parent = 0);
    ~TextDialog() {}
    void setTitle(QString);
    void setText(QString);

public slots:
    void accept();

protected:
    void closeEvent(QCloseEvent* event);
    void readSettings();
    void writeSettings();
};

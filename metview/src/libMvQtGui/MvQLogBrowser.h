/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QColor>
#include <QMap>
#include <QTextBrowser>

#include "LogHandler.h"

class MvQLogBrowser : public QTextBrowser, public LogObserver
{
    Q_OBJECT

public:
    MvQLogBrowser(QWidget* parent = nullptr);
    ~MvQLogBrowser() override;

    void update();
    void update(const std::string&, metview::Type) override;

protected slots:
    void slotTextChanged();

private:
    void parseTask(QString, QString&, QString&, QString&);
    QString creTitle(QString);
    // QString creIconCell(int iconSize,QString iconPath);
    // QString creError();

    bool hasOutput_;
    static QMap<QString, QString> keys_;
};

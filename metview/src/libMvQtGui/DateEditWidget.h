
#pragma once

#include <QWidget>

class QDateEdit;
class QToolButton;

class DateEditWidget : public QWidget
{
    Q_OBJECT

public:
    DateEditWidget(QWidget* parent = nullptr);

protected Q_SLOTS:
    void slotClear();

protected:
private:
    QDateEdit* dateEdit_;
    QToolButton* clearTb_;
};

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "StringMatchCombo.h"

#include <QMenu>
#include <QVariant>

StringMatchTb::StringMatchTb(QWidget* parent) :
    QToolButton(parent)
{
    QIcon ic(QPixmap(":/viewer/edit.svg"));
    setIcon(ic);
    setAutoRaise(true);
    auto* menu = new QMenu(this);
    menu->addAction("Contains");  //,StringMatchMode::ContainsMatch);
    menu->addAction("Matches");   //,StringMatchMode::WildcardMatch);
    menu->addAction("Regexp");    //,StringMatchMode::RegexpMatch);
    setMenu(menu);
    setPopupMode(QToolButton::InstantPopup);
}

StringMatchCombo::StringMatchCombo(QWidget* parent) :
    QComboBox(parent)
{
    addItem("Contains", StringMatchMode::ContainsMatch);
    addItem("Matches", StringMatchMode::WildcardMatch);
    addItem("Regexp", StringMatchMode::RegexpMatch);

    setCurrentIndex(1);
}

StringMatchMode::Mode StringMatchCombo::matchMode(int index) const
{
    if (index >= 0 && index < count()) {
        return static_cast<StringMatchMode::Mode>(itemData(index).toInt());
    }
    return StringMatchMode::WildcardMatch;
}

StringMatchMode::Mode StringMatchCombo::currentMatchMode() const
{
    return matchMode(currentIndex());
}

void StringMatchCombo::setMatchMode(const StringMatchMode& mode)
{
    int im = mode.toInt();
    for (int i = 0; i < count(); i++) {
        if (itemData(i).toInt() == im) {
            setCurrentIndex(i);
        }
    }
}

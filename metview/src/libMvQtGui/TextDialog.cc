/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "TextDialog.h"
#include <QSettings>

#include "MvQMethods.h"

TextDialog::TextDialog(QWidget* parent) :
    QDialog(parent)
{
    setupUi(this);
    setModal(true);

    titleLabel_->hide();

    te_->setReadOnly(true);
    te_->setFont(MvQ::findMonospaceFont());

    readSettings();
}

void TextDialog::setTitle(QString title)
{
    setWindowTitle(title);
}

void TextDialog::setText(QString txt)
{
    te_->setPlainText(txt);
}

void TextDialog::accept()
{
    writeSettings();
    QDialog::accept();
}

void TextDialog::closeEvent(QCloseEvent* event)
{
    writeSettings();
    event->accept();
}

void TextDialog::readSettings()
{
    QSettings settings(MVQ_QSETTINGS_DEFAULT_ARGS, "MV-TextDialog");

    settings.beginGroup("main");
    if (settings.contains("size")) {
        resize(settings.value("size").toSize());
    }
    else {
        resize(QSize(500, 360));
    }

    settings.endGroup();
}

void TextDialog::writeSettings()
{
    QSettings settings(MVQ_QSETTINGS_DEFAULT_ARGS, "MV-TextDialog");

    settings.clear();

    settings.beginGroup("main");
    settings.setValue("size", size());
    settings.endGroup();
}

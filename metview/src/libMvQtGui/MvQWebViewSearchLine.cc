/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <QColor>
#include <QDebug>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QWebView>
#include <QWebPage>

#include "MvQWebViewSearchLine.h"


MvQWebViewSearchLine::MvQWebViewSearchLine(QWebView* view, QString searchLabelText) :
    AbstractSearchLine(searchLabelText),
    view_(view)
{
}

MvQWebViewSearchLine::~MvQWebViewSearchLine()
{
}

void MvQWebViewSearchLine::slotFind(QString txt)
{
    QWebPage::FindFlags flags = QWebPage::FindWrapsAroundDocument;
    updateButtons(view_->findText(txt, flags));
    // keep the search term
    txt_ = txt;
}

void MvQWebViewSearchLine::slotFindNext()
{
    QWebPage::FindFlags flags = QWebPage::FindWrapsAroundDocument;
    updateButtons(view_->findText(txt_, flags));
}

void MvQWebViewSearchLine::slotFindPrev()
{
    QWebPage::FindFlags flags = QWebPage::FindBackward | QWebPage::FindWrapsAroundDocument;
    updateButtons(view_->findText(txt_, flags));
}

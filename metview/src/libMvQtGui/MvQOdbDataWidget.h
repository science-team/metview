/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QMainWindow>
#include <QModelIndex>

#include "MvQMainWindow.h"
#include "MvQMenuItem.h"

class QIntValidator;
class QLabel;
class QLineEdit;
class QTreeView;
class QSortFilterProxyModel;
class QToolButton;

class MvQOdbDataModel;

class MvAbstractOdb;


class MvQOdbDataWidget : public QWidget
{
    Q_OBJECT

public:
    MvQOdbDataWidget(QWidget* parent = 0);
    ~MvQOdbDataWidget();
    bool init(QString);
    bool init(QString, const std::vector<int>&);
    bool initialised() { return dataInitialised_; }
    void setMaxChunkSizeInMb(int);
    int maxChunkSizeInMb() { return maxChunkSizeInMb_; }
    QTreeView* dataTree() { return dataTree_; }
    void setFilter(const std::vector<int>&);
    void setTmpFilter(const std::vector<int>&);
    void clearTmpFilter();
    void highlightRow(int);
    QStringList visibleColumns();
    void setVisibleColumns(QStringList);
    void setTotalNumDisplay();

public slots:
    void slotToPrevChunk();
    void slotToNextChunk();
    void slotChunkEdited();
    void slotHeaderContextMenu(const QPoint&);
    void slotRowSelected(const QModelIndex&);

signals:
    void dataRowSelected(int);

protected:
    void loadData();
    void setCurrentChunk(int);
    void initCurrentChunk();
    void loadStarted();
    void loadFinished();

    QTreeView* dataTree_;
    MvQOdbDataModel* dataModel_;
    QSortFilterProxyModel* dataSortModel_;
    MvAbstractOdb* data_;

    bool useChunks_;
    int maxChunkSizeInMb_;  // max size in Mb of one chunk
    int chunkSize_;         // current number of row in a chunk
    int chunkNum_;          // nuber of chunks
    int currentChunk_;
    QLabel* rowNumLabel_;
    QLabel* chunkSizeLabel_;
    QLabel* dataBlockLabel_;
    QToolButton* chunkPrevTb_;
    QToolButton* chunkNextTb_;
    QLineEdit* chunkEdit_;
    QLabel* chunkNumLabel_;
    QIntValidator* chunkEditValidator_;

    bool dataInitialised_;
};

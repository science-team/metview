/***************************** LICENSE START ***********************************

 Copyright 2013 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <QtGlobal>
#include <QAbstractItemModel>
#include <QSortFilterProxyModel>
#include <QWidget>

#if QT_VERSION >= QT_VERSION_CHECK(5, 1, 0)
#include <QRegularExpression>
#else
#inclue < QRegExp>
#endif

class QTreeView;
class MvQComboLine;

typedef struct
{
    long lat;
    long lon;
    char flags[13];
    int station_height;
} MvStationPosition;


typedef struct
{
    int ident;
    MvStationPosition pos;
    int barometre_height;
    int pressure_level;
    char name[1024];
} MvStationData;


class MvQStationsModel : public QAbstractItemModel
{
public:
    MvQStationsModel(QObject* parent = 0);

    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    int rowCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;

    QModelIndex index(int, int, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex&) const;

protected:
    void loadDb();
    static QList<MvStationData*> data_;
};

class MvQStationsFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    MvQStationsFilterModel(QObject* parent = 0);
    QStringList filterLabels() const;
#if QT_VERSION >= QT_VERSION_CHECK(5, 1, 0)
    QList<QRegularExpression> filterRegExps() const;
#else
    QList<QRegExp> filterRegExps() const;
#endif
    QStringList filterToolTips() const;
    int filterModeIndex() const;

public slots:
    void slotFilterModeChanged(int index);
    void slotFilterTextChanged(QString);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const;

    enum FilterMode
    {
        NameMode,
        IdMode,
        WmoMode,
        PositionMode,
        AreaMode
    };

    struct FilterDef
    {
#if QT_VERSION >= QT_VERSION_CHECK(5, 1, 0)
        FilterDef(QString lab, FilterMode m, QString d, QRegularExpression r) :
#else
        FilterDef(QString lab, FilterMode m, QString d, QRegExp r) :
#endif
            label(lab),
            mode(m),
            desc(d),
            regExp(r)
        {
        }
        QString label;
        FilterMode mode;
        QString desc;
#if QT_VERSION >= QT_VERSION_CHECK(5, 1, 0)
        QRegularExpression regExp;
#else
        QRegExp regExp;
#endif
    };

    FilterMode filterMode_;
    QString filterText_;
    QList<FilterDef> filters_;
};


class MvQStationsWidget : public QWidget
{
    Q_OBJECT

public:
    enum SelectionMode
    {
        NameMode,
        IdMode,
        WmoMode
    };

    MvQStationsWidget(SelectionMode, QWidget* parent = 0);

protected slots:
    void slotSelected(const QModelIndex&);

signals:
    void selected(QString);

protected:
    MvQStationsModel* model_;
    MvQStationsFilterModel* sortModel_;
    QTreeView* tree_;
    MvQComboLine* filterCb_;
    SelectionMode selectionMode_;
};

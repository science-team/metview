/***************************** LICENSE START ***********************************

 Copyright 2014 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/


#pragma once

#include <QPlainTextEdit>

#include "TextEditSearchLine.h"

class AbstractTextSearchInterface;

class PlainTextSearchLine : public TextEditSearchLine
{
public:
    explicit PlainTextSearchLine(QWidget* parent = nullptr);
    ~PlainTextSearchLine() override;
    void setEditor(QPlainTextEdit*);

private:
    // The interface is set internally
    void setSearchInterface(AbstractTextSearchInterface*) {}
};

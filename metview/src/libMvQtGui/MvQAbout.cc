/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include "MvQAbout.h"

#include <QDialogButtonBox>
#include <QDate>
#include <QLabel>
#include <QTabWidget>
#include <QTextStream>
#include <QVBoxLayout>
#include <QFile>
#include <QDate>
#include <QPlainTextEdit>

#include "MvVersionInfo.h"

#ifdef METVIEW
#include "MvPath.hpp"
#endif

#define QUOTE_PP(P) #P
#define QUOTE_PPP(P) QUOTE_PP(P)


MvQAbout::MvQAbout(QString title, QString description, Versions version,
                   QMap<Version, QString> text) :
    QDialog(nullptr)
{
    setWindowTitle(tr("About ") + title);

    QString versionTxt;
    QTextStream str(&versionTxt);

    QString mvVersionTxt;
    QTextStream mvStr(&mvVersionTxt);

    resize(QSize(500, 300));

#ifdef METVIEW
    if (version.testFlag(MetviewVersion)) {
        str << "<p><b>Metview version:</b> ";

        if (text.contains(MetviewVersion)) {
            str << text[MetviewVersion];
            mvStr << text[MetviewVersion];
        }
        else {
            MvVersionInfo mvInfo;  // the constructor populates the class with information

            str << mvInfo.majorVersion() << "." << mvInfo.minorVersion() << "." << mvInfo.revision() << " (" << mvInfo.version() << ", " << (sizeof(int*) * 8) << "-bit)";  // could be any pointer, just to get the size


            mvStr << mvInfo.majorVersion() << "." << mvInfo.minorVersion() << "." << mvInfo.revision() << " (" << mvInfo.version() << ", " << (sizeof(int*) * 8) << "-bit)";


            const char* dev = getenv("METVIEW_VERSION");  // only add dir if using dev version
            if (dev && (!strcmp(dev, "dev")))
                str << "<p><b>Metview dir:</b> " << QString(mvInfo.installDir().c_str());
        }
    }

    if (version.testFlag(MagicsVersion)) {
        if (text.contains(MagicsVersion)) {
            str << "<p><b>Magics version:</b> " << text[MagicsVersion];
        }
    }
#endif

    if (version.testFlag(GribApiVersion)) {
        str << "<p><b>" QUOTE_PPP(GRIB_HANDLING_PACKAGE) " version:</b> ";

        if (text.contains(GribApiVersion)) {
            str << text[GribApiVersion];
        }
        else {
            MvGribApiVersionInfo mvInfo;
            /*long gribapiVersionNumber =  grib.gribapiVersionNumber();
                long gribapiMajorVersion  = gribapiVersionNumber / 10000;
                long gribapiMinorVersion  = (gribapiVersionNumber - (gribapiMajorVersion * 10000)) / 100;
                long gribapiRevisVersion  = gribapiVersionNumber % 100;*/

            str << mvInfo.majorVersion() << "." << mvInfo.minorVersion() << "." << mvInfo.revision() << " (" << mvInfo.version() << ")";
        }
    }

#ifdef METVIEW
    if (version.testFlag(OdcVersion)) {
        if (text.contains(OdcVersion)) {
            str << "<p><b>ODC version:</b> " << text[OdcVersion];
        }
    }

    if (version.testFlag(NetcdfVersion)) {
        str << "<p><b>NetCDF version:</b> ";
        if (text.contains(NetcdfVersion)) {
            str << text[NetcdfVersion];
        }
        else {
            MvNetcdfVersionInfo mvInfo;  // the constructor populates the class
            str << mvInfo.majorVersion() << "." << mvInfo.minorVersion() << "." << mvInfo.revision() << " (" << mvInfo.version() << ")";
        }
    }
#endif

#ifdef METVIEW
    if (version.testFlag(InterpolationPackage)) {
        const char* interp = getenv("MARS_PPROC_BACKEND");
        if (interp) {
            str << "<p><b>Default interpolation package:</b> ";
            str << interp;
        }
    }
#endif


    const char* qtv = qVersion();
    if (qtv) {
        str << "<p><b>Qt version: </b>" + QString(qtv);
    }

    auto* vb = new QVBoxLayout(this);
    setLayout(vb);

    // Logo + text

    auto* logo = new QLabel(this);
    logo->setProperty("type", "title");
    // logo->setFrameStyle(QFrame::StyledPanel | QFrame::Plain);
    // logo->setAutoFillBackground(true);
    //     QPalette pal = logo->palette();
    //     pal.setBrush(QPalette::Window, Qt::white);
    //     logo->setPalette(pal);

    QString logoTxt;
    logoTxt = "<table><tr>";

#ifdef METVIEW
    logoTxt += "<td><img src=\":/window/metview_logo.png\"></td>";
    logoTxt += "<td align=\"left\">";
    logoTxt += "<h3>&nbsp;&nbsp;" + title + "</h3>";
    if (!description.isEmpty()) {
        logoTxt += "<p>" + description + "</p>";
    }
    if (!mvVersionTxt.isEmpty()) {
        logoTxt += "<p>&nbsp;&nbsp;Metview version: " + mvVersionTxt + "</p>";
    }
    logoTxt += "</td></tr></table>";


#elif defined ECCODES_UI

    logoTxt += "<td><img src=\":/codes_ui/codes_ui_logo.png\"></td>&nbsp;&nbsp;&nbsp;&nbsp;<td></td><td>";
    logoTxt += "<h3>" + title + "</h3>";
    if (!description.isEmpty()) {
        logoTxt += "<p>" + description + "</p>";
    }

    CodesUiVersionInfo cuInfo;
    logoTxt += "<p>Version: " + QString::number(cuInfo.majorVersion()) + "." +
               QString::number(cuInfo.minorVersion()) + "." + QString::number(cuInfo.revision()) +
               " (" + QString::number(cuInfo.version()) + ", " +
               QString::number(sizeof(int*) * 8) + "-bit)";

    logoTxt += "<br><small>Copyright (C) ECMWF " + QString::fromStdString(cuInfo.period()) + "</small><br></p>";

    if (!mvVersionTxt.isEmpty()) {
        logoTxt += "<p>" + mvVersionTxt + "</p>";
    }
    logoTxt += "</td></tr></table>";
#endif

    logo->setText(logoTxt);

    vb->addWidget(logo);

    // tab
    auto* tw = new QTabWidget(this);

    // Version
    auto* versionLabel = new QLabel(this);
    versionLabel->setIndent(10);
    versionLabel->setText(versionTxt);
    versionLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    tw->addTab(versionLabel, tr("Versions"));

    // Licence
    auto* licenceTe = new QPlainTextEdit(this);
    licenceTe->setReadOnly(true);
    licenceTe->setWordWrapMode(QTextOption::NoWrap);

    QString licenceTxt;
#ifdef METVIEW
    std::string licenceTextPath = MakeSystemEtcPath("licence_for_about_box.txt");
    QFile file(licenceTextPath.c_str());
    if (file.open(QIODevice::ReadOnly)) {
        QTextStream ts(&file);
        licenceTxt = ts.readAll();
        licenceTxt.replace("Copyright 2016",
                           "Copyright " + QString::number(QDate::currentDate().year()));
    }
#else
    licenceTxt = "Copyright " + QString::number(cuInfo.year()) +
                 " ECMWF and INPE. This software is distributed under the terms\n\
of the Apache License version 2.0. In applying this license, ECMWF does not\n\
waive the privileges and immunities granted to it by virtue of its status as\n\
an Intergovernmental Organization or submit itself to any jurisdiction.\n";
#endif
    licenceTe->setPlainText(licenceTxt);
    tw->addTab(licenceTe, tr("Licence"));

    vb->addWidget(tw);

    // Buttonbox
    auto* buttonBox = new QDialogButtonBox(QDialogButtonBox::Close, Qt::Horizontal, this);
    vb->addWidget(buttonBox);

    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}

/***************************** LICENSE START ***********************************

 Copyright 2020 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include "MvRequest.h"
#include "MvProtocol.h"
#include "MvService.h"

#include <memory>
#include <map>
#include <vector>

struct MvUiServiceAppItem
{
    MvUiServiceAppItem() {}
    MvUiServiceAppItem(const std::string& appName, const std::string& action, bool hasOutput) :
        appName_(appName), action_(action), hasOutput_(hasOutput) {}
    std::string appName_;
    std::string action_;
    bool hasOutput_{false};
};

class MvUiService : public MvService
{
public:
    MvUiService(const char* name);
    virtual ~MvUiService();
    void serve(MvRequest& in, MvRequest& out) override;
    void addApp(const std::string& appName, const std::vector<std::string>& actions, bool hasOutReq = false);

private:
    // No copy allowed
    MvUiService(const MvUiService&);
    MvUiService& operator=(const MvUiService&) { return *this; }

    std::map<std::string, MvUiServiceAppItem> apps_;
};


typedef std::shared_ptr<MvUiService> MvUiService_ptr;

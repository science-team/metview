/***************************** LICENSE START ***********************************

 Copyright 2016 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <map>
#include <string>
#include <vector>

#include "Metview.h"
#include "MvUiService.h"
#include "MvMiscellaneous.h"


int main(int argc, char** argv)
{
    MvApplication theApp(argc, argv);

    // MvUiService is non-copiable!
    std::map<std::string, MvUiService_ptr> services;
    MvUiService_ptr p;

    MvRequest r;
    r.read(metview::etcDirFile("UiServiceTable").c_str());
    if (r) {
        print_all_requests(r);
        do {
            const char* verbCh = r("class");
            const char* appNameCh = r("app");
            if (verbCh && appNameCh) {
                std::string verb(verbCh);
                std::string appName(appNameCh);

                auto it = services.find(verb);
                MvUiService_ptr p;
                if (it == services.end()) {
                    p = std::make_shared<MvUiService>(verbCh);
                    services[verb] = p;
                }
                else {
                    p = it->second;
                }

                bool hasOutput = false;
                if (const char* outputCh = r("output")) {
                    if (strcmp(outputCh, "1") == 0) {
                        hasOutput = true;
                    }
                }

                std::vector<std::string> actions;
                r.getValue("action", actions, true);
                if (actions.empty()) {
                    actions.push_back("any");
                }

                p->addApp(appName, actions, hasOutput);

                //                marslog(LOG_DBUG,"add sevice %s %s %s",
                //                        verb.c_str(), appName.c_str(), action.c_str());
            }
        } while (r.advance());
    }

    theApp.run();
}

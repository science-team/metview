/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#include <stdio.h>
#include <string.h>
#include <ctype.h>


char* getnchar(char* line, int n)
{
    static char buf[1024];
    strncpy(buf, line, n);
    buf[n] = 0;
    return buf;
}

char station[1024] = "";

void output(char* p)
{
    int n = strlen(p) - 1;
    while (n > 0 && isspace(p[n])) {
        p[n] = 0;
        n--;
    }
    while (*p && isspace(*p))
        p++;

    if (*station) {
        if (station[strlen(station) - 1] == '-')
            station[strlen(station) - 1] = 0;
        else
            strcat(station, " ");
    }
    strcat(station, p);
}

void flush(void)
{
    char* p = station;
    int nospace = 0;
    int space = 0;

    while (*p) {
        switch (*p) {
            case '(':
                if (!space)
                    putchar('_');
                putchar(*p);
                break;

            case '/':
                putchar('-');
                nospace = 1;
                space = 0;
                break;

            case ',':
            case '.':
                /* putchar(*p); */
                if (p[1] != ')' && p[1] != '"')
                    putchar('.');
                else
                    putchar(*p);
                space = 1;
                nospace = 1;
                break;

            case ' ':
                if (*p)
                    switch (p[1]) {
                        case '.':
                        case ',':
                        case '(':
                        case '/':
                        case '"':
                            nospace = 1;
                            break;
                    }
                if (!nospace) {
                    putchar('_');
                    space = 1;
                };
                nospace = 1;
                break;

            default:
                if (islower(*p))
                    putchar(toupper(*p));
                else
                    putchar(*p);
                nospace = 0;
                space = 0;
                break;
        }

        p++;
    }
    putchar('\n');
    *station = 0;
}

main(argc, argv) int argc;
char* argv[];
{
    unsigned pilot = 0;
    unsigned temp = 0;
    unsigned synop = 0;
    unsigned f = 0;
    char* p;
    char* ptr;
    char st[256];
    int i;
    char buff[1024];
    long n, m;
    int first = 1;
    ptr = buff;

    gets(buff);
    gets(buff);

    while (gets(buff)) {
        temp = 0;
        pilot = 0;
        synop = 0;

        p = getnchar(buff + 1, 5);


        if (isdigit(*p)) {
            if (!first)
                flush();
            first = 0;

            printf("%s ", p);


            /* decode station name */

            /* decode latitude */

            n = atol(getnchar(ptr + 30, 2));

            m = atol(getnchar(ptr + 33, 2));

            if (ptr[35] == 'S')
                printf("%d ", (int)((float)((-n + (-m / 60.0)) * 100.0)));
            else
                printf("%d ", (int)((float)((n + (m / 60.0)) * 100.0)));

            /* decode longitude */

            n = atol(getnchar(ptr + 37, 3));

            m = atol(getnchar(ptr + 41, 2));

            if (ptr[43] == 'W')
                printf("%d ", (int)((float)((-n + (-m / 60.0)) * 100.0)));
            else
                printf("%d ", (int)((float)((n + (m / 60.0)) * 100.0)));

            /* decode  barometre height */

            p = getnchar(ptr + 47, 4);
            printf("%d ", atoi(p));

            /* decode  station height */

            p = getnchar(ptr + 52, 4);
            printf("%d ", atoi(p));

            /* decode  pressure_level */
            p = getnchar(ptr + 57, 4);
            printf("%d ", atoi(p));

            synop = pilot = temp = 0;

            for (i = 0; i < 12; i++) {
                switch (ptr[i + 67]) {
                    case 'T':
                        temp |= (1 << i);
                        break;

                    case 'P':
                        pilot |= (1 << i);
                        break;

                    case 'X':
                        synop |= 1 << (i - 4);
                        break;

                    case ' ':
                        break;

                    default:
                        printf("bad value...\n");
                        exit(1);
                        break;
                }
            }

            f = (ptr[7] == 'P');

            printf("%d ", (f << 24) + (temp << 16) + (pilot << 8) + synop);
        }
        output(getnchar(ptr + 9, 20));
    }
    flush();
    exit(0);
}

/***************************** LICENSE START ***********************************

 Copyright 2012 ECMWF and INPE. This software is distributed under the terms
 of the Apache License version 2.0. In applying this license, ECMWF does not
 waive the privileges and immunities granted to it by virtue of its status as
 an Intergovernmental Organization or submit itself to any jurisdiction.

 ***************************** LICENSE END *************************************/

#pragma once

#include <iosfwd>


extern "C" {
extern GDBM_FILE db;
}

/* utils.cc */
int same(const char* p, const char* q);
int inbox(double lat, double lon, double north, double west, double south, double east);

char* nice_name(const char* p);
int open_database(char c);

int open_EPS_stationfile(std::ifstream*& EPS_file);
int open_cached_EPS_stationfile(std::ifstream*& EPS_file);
int read_one_station(std::ifstream*& EPS_file, station& st);

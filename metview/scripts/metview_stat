
# **************************** LICENSE START ***********************************
#
# Copyright 2012 ECMWF and INPE. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

###
#   This is an ECMWF specific script to collect Metview usage
#   statistics. Every time Metview is started a line about
#   the user is appended into usage statistics file.
#
#   If your site wants usage statistics then:
#     1) create an empty file with write permissions (touch + chmod)
#     2) when configuring with CMake, add -DENABLE_USAGE_LOG=on -DLOG_DIR=<path-to-log-dir>
#
#   If you do not want to collect usage statistics then leave
#   this file as is.
###

#-- do not let this script crash the master script!
set +e


if [ $MV_USAGE_LOG != OFF ]
then
   #-- first letter of runmode: 'i' or 'b'
   MV_MODE=`echo $METVIEW_MODE | cut -c1`

   #-- (2) modify here the path to your own usage file
   MV_USAGE_FILE="${MV_LOG_DIR}/Metview_usage_${METVIEW_RELEASE}_$MV_MODE"

   if [ -f $MV_USAGE_FILE ]
   then
      #-- try $USER first, if nothing then try $LOGNAME
      MVUSERID=$USER
      [ "$MVUSERID" = "" ] && MVUSERID=$LOGNAME
      [ "$MVUSERID" = "" ] && MVUSERID="???"
      echo "`date -u '+%d.%m.%Y %R'`; $MVUSERID; $MV_MODE; `hostname`; $METVIEW_VERSION_TITLE" >> $MV_USAGE_FILE
   else
      info "Common statistics file not found: $MV_USAGE_FILE"
   fi
fi

set -e

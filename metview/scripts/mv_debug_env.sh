
# **************************** LICENSE START ***********************************
#
# Copyright 2021- ECMWF and INPE. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

echo " METVIEW debug shell - available functions:"
echo ""
echo " mscape     - TotalView's MemoryScape debugger"
echo ""
echo " val_mem   - valgrind memory debugging"
echo " val_heap  - valgrind heap debugging"
echo " val_call  - profiling with callgrind & kcachegrind "
echo ""
echo " Usage: val_call bin/MvModuleName -nofork"
echo ""

ECMWFHOME=/usr/local/share/ecmwf

module load totalview

alias mscape='/usr/local/apps/totalview/tv8.11.0-0/memoryscape.3.3.0-0/bin/memscape'

echo ""

#
# F U N C T I O N S
#
val_mem()
{
 valgrind --tool=memcheck --leak-check=full --track-fds=yes -v --log-file=memcheck.out $*
 $EDITOR memcheck.out &
}

val_heap()
{
 valgrind --tool=massif --massif-out-file=massif.out --time-unit=ms $*
 ms_print massif.out | more
}

val_call()
{
 valgrind --tool=callgrind --callgrind-out-file=callgrind.out --dump-instr=yes --trace-jump=yes $*
 kcachegrind callgrind.out &
}

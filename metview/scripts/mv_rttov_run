#!/bin/bash

# **************************** LICENSE START ***********************************
#
# Copyright 2013 ECMWF and INPE. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************


# ---------------------------------------------------------
# Script to run RTTOV from within Metview
# ---------------------------------------------------------

set -x

print_err()
{
	echo ${text_ERR}  $* >> "${f_LOG}"
} 

text_ERR="Script `basename $0` FAILED> "

#Get args
if [ $# -ne 9 ] ; then
    echo "Invalid number of arguments specified for script $0 ! (" $# " instead of 9)"
    exit 1
fi

d_WORK=$1
f_EXE=$2
f_INPUT=$3
NLEV=$4
SENSOR=$5
f_CHANNEL=$6
f_COEFF=$7
f_LOG=$8
f_RES=$9


#Some init
do_SOLAR=0  # 0 = solar off / 1 = solar on
do_OZONE=0   # 0 = microwave   1 = infrared ozone input

#Define executable
if [ "$f_EXE" = "_UNDEF_" ] ; then
	exe_RTTOV=${MV_RTTOV_EXE}
else
	exe_RTTOV=${f_EXE}
fi

#-------------------------------------------
# Set-up predefined channels and coeff
#-------------------------------------------

if [ "$SENSOR"  != "CUSTOM" ] ; then
	RTTOV_CHANNELS_FILES_PATH="${RTTOV_CHANNELS_FILES_PATH:=/home/graphics/cgx/model_files/rttov}"
	case "$SENSOR" in
	"AMSUA")
		f_CHANNEL="${RTTOV_CHANNELS_FILES_PATH}/amsua_channels"		
		f_COEFF="${RTTOV_CHANNELS_FILES_PATH}/rtcoef_metop_2_amsua.dat"
		do_SOLAR=0
		do_OZONE=0
		;;
	"IASI")
		f_CHANNEL="${RTTOV_CHANNELS_FILES_PATH}/iasi_channels"		
		f_COEFF="${RTTOV_CHANNELS_FILES_PATH}/rtcoef_metop_2_iasi.dat"
		do_SOLAR=0
		do_OZONE=1
		;;
	*) print_err "Unsupported sensor type:" "$SENSOR"; exit 1 ;;
		
	esac

fi

#-------------------------------
# Go to working directory 
#-------------------------------

if [ ! -d "$d_WORK" ] ; then   
   print_err "No working directory found: " $d_WORK
   exit 1
fi

cd $d_WORK

#-------------------------------
# Checks
#-------------------------------

if [ x"$exe_RTTOV" = "x" ] ; then   
   print_err "No RTTOV executable is defined. Please define it via env variable MV_RTTOV_EXE."
   exit 1
fi

if [ ! -f "$exe_RTTOV" ] ; then   
   print_err "No RTTOV executable found: " $exe_RTTOV
   exit 1
fi

if [ ! -x "$exe_RTTOV" ] ; then   
   print_err "RTTOV executable cannot be run! Permission is missing. " $exe_RTTOV 
   exit 1
fi

if [ ! -f "$f_INPUT" ] ; then   
   print_err "No RTTOV input profile file is found: " $f_INPUT
   exit 1
fi

if [ ! -r "$f_INPUT" ] ; then   
   print_err "RTTOV input profile file cannot be read! Permission is missing. " $f_INPUT
   exit 1
fi

if [ ! -f "$f_CHANNEL" ] ; then   
   print_err  "No RTTOV channels file is found: " $f_CHANNEL
   exit 1 
fi

if [ ! -r "$f_CHANNEL" ] ; then   
   print_err "RTTOV channels file cannot be read! Permission is missing. " $f_CHANNEL
   exit 1
fi

if [ ! -f "$f_COEFF" ] ; then   
   print_err  "No RTTOV coefficient file is found: " $f_COEFF
   exit 1 
fi

if [ ! -r "$f_COEFF" ] ; then   
   print_err "RTTOV coefficient file cannot be read! Permission is missing. " $f_COEFF
   exit 1
fi

#-------------------------------
# Prepare input profile
#-------------------------------

ln -sf "${f_INPUT}" rttov_input_data.txt

#--------------------------------------
# Prepare channels and coefficient file
#---------------------------------------

ln -sf "${f_CHANNEL}" channel
ln -sf "${f_COEFF}" coeff

#-------------------------------
# Prepare option input files
#-------------------------------

cat > options_input << EOF
coeff, Coefficient filename
rttov_input_data.txt      , Input profile filename
1                 , Number of profiles
${NLEV}         , Number of levels
${do_SOLAR}     , Turn solar radiation on/off
${do_OZONE}     , Turn ozone radiation on/off
EOF
cat options_input channel > input

#-------------------------------
# Run RTTOV
#-------------------------------

$exe_RTTOV < input > "${f_LOG}" 2>&1 
outCode=$?

#-----------------------------------
#  Check log
#-----------------------------------

if [ -f "${f_LOG}" ] ; then
  if [ `grep -c -i WARNING "$f_LOG"` -ne 0 ] ; then
	outCode=255 
  elif [ `grep -c -i ERROR "$f_LOG"` -ne 0 ] ; then
	outCode=1
  elif [ $outCode -ne 0 ] ; then
	outCode=$outCode
  fi  
fi

#-----------------------------------
#  Check results
#-----------------------------------

f_OUT=output_example_fwd.dat

if [ ! -f $f_OUT ] ; then
   print_err "RTTOV ouput file was not generated!"
   exit 1	 
fi

cp -f ${f_OUT} "${f_RES}"

exit $outCode

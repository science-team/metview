#!/bin/bash

# **************************** LICENSE START ***********************************
#
# Copyright 2014 ECMWF and INPE. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************


# Script to compile external inline macro functions
#
# The upper part is created by Make in order to copy
# site and platform specific compiler macros from
# Metview configuration files. The rest of the script
# - below - should be site and platform independent.
#

#-- MACRO_EXTRA_LIBS can be set externally to link with custom libs!
#-- MACRO_EXTRA_F77_FLAGS can be set externally for extra Fortran 77 flags!
#-- MACRO_EXTRA_F90_FLAGS can be set externally for extra Fortran 90 flags!
#-- MACRO_EXTRA_CC_FLAGS can be set externally for extra C/C++ flags!
#-- MACRO_EXTRA_INCS is reserved for C programs, for extra -I flags!

# NOTE that we use environment variables METVIEW_DDIR and METVIEW_VVERSION instead
# of METVIEW_DIR and METVIEW_VERSION. We set these in extern.cc when we call the
# popen() routine; we can't always use the normal env vars because these can be
# overwritten by the default modules command at ECMWF.


set -e
CC=@CMAKE_C_COMPILER@
CC_FLAGS="@METVIEW_INLINE_DEFINITIONS@ @CXX_DEFINES@ @CMAKE_C_FLAGS@"
CXX=@CMAKE_CXX_COMPILER@
CXX_FLAGS="@METVIEW_INLINE_DEFINITIONS@ @CMAKE_CXX_FLAGS@"
F77=@CMAKE_Fortran_COMPILER@
F77_FLAGS="@CMAKE_Fortran_FLAGS@"
F90=@CMAKE_Fortran_COMPILER@
F90_FLAGS="@CMAKE_Fortran_FLAGS@"
FC_LIBS="@FLIBS@"
LIBNAME="@INSTALL_LIB_DIR@"
GRIBAPI_LIB="@GRIB_API_LIBRARIES_STR@"
GRIBAPI_INC="@GRIB_API_INCLUDE_DIRS_STR@"
MV_EMOSLIB="@EMOS_LIBRARIES_STR@"
LDFLAGS_INLINE="@LDFLAGS_INLINE@"
IMP_FORT_LIBS="@IMPLICIT_FORTRAN_LIBS@"
MARSCLIENT_LIB_PATH="@mars_client_BASE_DIR@/@CMAKE_INSTALL_LIBDIR@"
DTAGS="-Wl,--disable-new-dtags"  # to set RPATH instead of RUNPATH in the executable

# installed version with own-built GRIB_API - make sure we pick up the right GRIB libs
case $METVIEW_VVERSION in
 prod)

    if [ "$MV_BUNDLE_CONTAINS_ECCODES" != "x" ]
    then
        GRIBAPI_INC="-I$METVIEW_DDIR/include $GRIBAPI_INC"
        GRIBAPI_LIB="-L$METVIEW_DDIR/$LIBNAME -Wl,-rpath,/$METVIEW_DDIR/$LIBNAME $GRIBAPI_LIB"
    fi
	;;
esac



#-- Note that we end up with a duplicated '-lgrib_api' in $FLIBS, but this is because this needs to
#-- come after the fortran library (and it also exists within $GRIBAPI_LIB).

FLIB="-L$METVIEW_DDIR/${LIBNAME} ${DTAGS} -Wl,-rpath,$METVIEW_DDIR/${LIBNAME}:${MARSCLIENT_LIB_PATH} -lmacro_api_f90 -lmacro_api_c -L${MARSCLIENT_LIB_PATH} -lmarsclient @FDB_LIBRARIES_STR@ $MACRO_EXTRA_LIBS $MV_EMOSLIB $GRIBAPI_INC $GRIBAPI_LIB  $LDFLAGS_INLINE -lm -lc "
CLIB="$MACRO_EXTRA_LIBS $GRIBAPI_INC ${DTAGS} -L$METVIEW_DDIR/${LIBNAME} -Wl,-rpath,$METVIEW_DDIR/${LIBNAME}:${MARSCLIENT_LIB_PATH} -lmacro_api_c -L${MARSCLIENT_LIB_PATH} -lmarsclient @FDB_LIBRARIES_STR@ $MV_EMOSLIB $GRIBAPI_LIB $LDFLAGS_INLINE -lm $FC_LIBS   $EMOSLIB_TPLS $IMP_FORT_LIBS"
CINC="$MACRO_EXTRA_INCS $GRIBAPI_INC -I$METVIEW_DDIR/include"
MX77="$MACRO_EXTRA_F77_FLAGS"
MX90="$MACRO_EXTRA_F90_FLAGS"
MXCC="$MACRO_EXTRA_CC_FLAGS"

cd $METVIEW_TMPDIR

#-- to set trace use macro function 'trace(n)' where n>1

trace=0

if [ "$1" = "-d" ]
then
	trace=1
	set -x
	shift
fi

type="$1"
in="$2"
out="$3"

#-- uncomment if you need to debug...
#set -x

case "$type" in

	shell)
		## Simply copy input to output and make executable
		ln -f "$in" "$out"
		chmod +x "$out"
		;;

	fortran|f77)
		ln -s -f "$in" compile$$.f
		$F77 -o "$out" $F77_FLAGS $MX77 compile$$.f $FLIB
		[ x$MACRO_FORTRAN_DBG != x ] ||  rm -f compile$$.f
		;;

	fortran90|f90)
		ln -s -f "$in" compile$$.f90
		$F90 -o "$out" $F90_FLAGS $MX90 compile$$.f90 $FLIB
		[ x$MACRO_FORTRAN_DBG != x ] || rm -f compile$$.f90
		;;

	C|c)
		ln -s -f "$in" compile$$.c
		$CXX -o "$out" $CC_FLAGS $MXCC $CINC compile$$.c $CLIB
		[ x$MACRO_FORTRAN_DBG != x ] || rm -f compile$$.c
		;;

	C++|c++)
		ln -s -f "$in" compile$$.cc
		$CXX -o "$out" $CXX_FLAGS $MXCC $CINC compile$$.cc $CLIB
		[ x$MACRO_FORTRAN_DBG != x ] || rm -f compile$$.c
		;;

	*)
		# Consider the string as a command
		cat <<@ > "$out"
#!/bin/bash


cat <<\end_of_inline_macro$$ > $METVIEW_TMPDIR/\$\$
@
		cat "$in" >> "$out"
		cat <<@ >> "$out"
end_of_inline_macro$$
$type $METVIEW_TMPDIR/\$\$
e=\$?
rm -f $METVIEW_TMPDIR/\$\$
exit \$e
@
		;;

esac

# Copyright 2012 ECMWF and INPE. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.

# -*- coding: utf-8 -*-
import json
import sys
from webmars import fetch
#from beautify import beautify

# MARS catalog is a tree and everything is a node. The abstraction
# of that in our case is that everything is an instance of the class
# PSimpleNode.

# returns root node if fetch is called with no arguments
a=sys.stdin.readlines()

#print a

path    = json.loads("\n".join(a))
choices = {}
tree    = []

#print path
#sys.exit(0)

#def navigate(node):
#    if node.has_kids():
#       if node.name in path:
#          value = path[node.name]
#          tree.append([node.name, value])
#          choices[node.name] = node.kids.keys()
#          return navigate(fetch(node, value))
#    return node


output ={"axis":[], "tree":[], "choices":{}}
fetch().navigate(path,output)
#print fetch().kids.keys()
print json.dumps(output, indent = 4)

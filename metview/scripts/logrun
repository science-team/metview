#!/bin/bash

# **************************** LICENSE START ***********************************
#
# Copyright 2012 ECMWF and INPE. This software is distributed under the terms
# of the Apache License version 2.0. In applying this license, ECMWF does not
# waive the privileges and immunities granted to it by virtue of its status as
# an Intergovernmental Organization or submit itself to any jurisdiction.
#
# ***************************** LICENSE END ************************************

#                                                (vk May-98, rev 980507)
#
# This script is a simplified "log-only" version of ECMWF script 'mvrun',
# to be used (through 'metview_local.???') if log files are needed.
#
# Log files are created on $SCRATCH ($SCRATCH needs to be defined!!!).
#

set +e
Module=`basename $1`
start_date=`date`
MYLOG="$SCRATCH/$METVIEW_LOG.$Module.log"

if [ $METVIEW_LOG = no_log -o "$METVIEW_LOG" = "-" ]
then
  # -- no log file --
  "$@"
  SUCCESS_STATUS=$?
else
  # -- create log file --
  echo "[logrun info] process $$ starting `date` with cmd:" > $MYLOG
  echo "[logrun info] $@"                        >> $MYLOG
  echo "[logrun info] mode is $METVIEW_MODE\n"   >> $MYLOG

  "$@"                                           >> $MYLOG
  SUCCESS_STATUS=$?

  echo "\n[logrun info] process $$ ended `date`" >> $MYLOG
  echo "[logrun info] exit status: $SUCCESS_STATUS" >> $MYLOG
fi

exit $SUCCESS_STATUS
